<?php
return [
    'modules'=>array(
        "users",
        "settings",
        "reports",
        "pilgrim",
        "dashboard"
    ),
];