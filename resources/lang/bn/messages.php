<?php

return [
    'welcome' => 'Logged in successfully, Welcome to BEZA platform ',
    'app_title' => 'BEZA',
//  For Application Module
    'app_list' => 'Application List',
    'delegation_app_list' => 'Delegation Application List',
    'app_form' => 'Add New Application ',
    'app_form_title' => 'New Application Data Entry',

//  Settings Module
    //   For Area Settings    'document' => 'Document ',
    'doc_list' => 'Document List',
    'area' => 'এলাকা ',
    'area_form' => 'এলাকা ',
    'area_edit' => 'এলাকা হালনাগাদ',
    'area_list' => 'এলাকার তালিকা',
    'new_area' => 'নতুন এলাকা',
    'district' => 'District',
    //For Demo Group Settings
    'demo_group_list' => 'জনতাত্ত্বিক গ্রুপ',
    'new_demo_group' => 'নতুন জনতাত্ত্বিক গ্রুপ',
    'demo_group_form' => 'নতুন জনতাত্ত্বিক গ্রুপ',
    'demo_group_edit' => 'জনতাত্ত্বিক গ্রুপ পরিবর্তন',
    // For Configuration
    'list_config' => 'কনফিগারেশন লিস্ট',
    'new_config' => 'নতুন কনফিগারেশন',
    'edit_config' => 'কনফিগারেশন পরিবর্তন',
    // For Notification
    'list_notification' => 'নোটিফিকেশন লিস্ট',
    'new_notification' => 'নতুন নোটিফিকেশন',
    'edit_notification' => 'নোটিফিকেশন পরিবর্তন',
    'view_notification' => 'নোটিফিকেশনের বিস্তারিত',
    //   For FAQ Settings
    'faq_form' => 'প্রশ্নসমূহ ',
    'faq_edit' => 'প্রশ্ন পরিবর্তন',
    'faq_list' => 'প্রশ্নের তালিকা',
    'new_faq' => 'নতুন প্রশ্ন যোগ',
    //   For FAQ Category Settings
    'faq_cat_form' => 'নতুন জ্ঞানকোষ',
    'faq_cat_edit' => 'জ্ঞানকোষ পরিবর্তন',
    'faq_cat_list' => 'জ্ঞানকোষ',
    'new_faq_cat' => 'জ্ঞানকোষ তৈরি',
    // For settings->User Type
    'list_user_type' => 'ব্যবহারকারীর ধরণ',
    'edit_user_type' => 'ব্যবহারকারীর ধরণ পরিবর্তন',
//For Sidebar Menu
    'dashboard' => 'ড্যাশবোর্ড',
    'search' => 'অনুসন্ধান',
    'pilgrim' => 'পিলগ্রিম',
    'users' => 'ব্যবহারকারী',
    'settings' => 'সেটিংস',
    'bank' => 'ব্যাংক',
    'agency' => 'এজেন্সি',
    'uisc' => 'ইউনিয়ন ডিজিটাল সেন্টার',
    'bank_payment' => 'ব্যাংক পেমেন্ট',
    'pre_registered' => 'আবেদনপত্র',
    'registered' => 'নিবন্ধনকৃত',
    'group_voucher' => 'টাকা জমা',
    'demographic_group' => 'জনতাত্ত্বিক গ্রুপ',
    'config' => 'কনফিগারেশন',
    'notify' => 'নোটিফিকেশন',
    'faq' => 'জ্ঞানকোষ',
    'faq_cat' => 'জ্ঞানকোষ ক্যাটাগরি',
    'feedback' => 'সহায়তা',
    //For User module
    'user_list' => 'ব্যবহারকারীর তালিকা',
    'user_view' => 'ব্যবহারকারীর বিস্তারিত তথ্য',
    'new_user' => 'নতুন ব্যবহারকারী',
    //For Dashboard
    'group_total' => 'মোট গ্রুপ',
    'registered_application' => 'নিবন্ধনকৃত অ্যাপ্লিকেশন ',
    'unpaid_application' => 'অপরিশোধিত অ্যাপ্লিকেশন',
    'total_registered' => 'মোট নিবন্ধন',
    'monthly_registered' => 'প্রাক্‌-নিবন্ধনের বর্তমান অবস্থা',
    'age_wise_registration' => 'বয়স ভিত্তিক পিলগ্রিম নিবন্ধন',
    'pilgrim_payment_chart' => 'পিলগ্রিম পেমেন্ট চার্ট',
    'pilgrim_chart' => 'পিলগ্রিম চার্ট',
//For Profile
    'profile' => 'প্রোফাইল তথ্য',
    'first_name' => 'নাম',
    'specification' => 'ব্যবহারকারী স্পেসিফিকেশন',
    'nid' => 'জাতীয় আইডি নং',
    'dob' => 'জন্ম তারিখ',
    'mobile' => 'মোবাইল নম্বর',
    'email' => 'ইমেল ঠিকানা',
    'user_name' => 'ইউজার নাম',
    'user_type' => 'ইউজার টাইপ',
    'details' => 'বিস্তারিত দেখুন ',
    //For Report Module
    'reports' => 'রিপোর্ট',
    //For Group Voucher Module
    'voucher_list' => 'ভাউচারের তালিকা',
    'new_voucher' => 'নতুন ভাউচার',
    //For SB
    'sb' => 'পিলগ্রিম',
    'sb_report' => 'স্পেশাল ব্রাঞ্চ রিপোর্ট',
    'new_sb_report' => 'নতুন স্পেশাল ব্রাঞ্চ রিপোর্ট',
    'pilgrim_list_sb' => 'পিলগ্রিমের ভেরিফিকেশন স্ট্যাটাস',
    //For Group
    'group' => 'গ্রুপসমূহ',
    'group_list' => 'গ্রুপ তালিকা',
    'group_form' => 'নতুন গ্রুপ',
    //  For Report Module
    'report_list' => 'রিপোর্ট তালিকা',
    'report_form' => 'নতুন রিপোর্ট',
    'report_view' => 'রিপোর্ট পরিদর্শন',
    'report_edit' => 'রিপোর্ট হালনাগাদ',
    //For Agency Module
    // For session
    'session' => 'বর্ষ',
    'session_form' => 'নতুন বর্ষ',
    'session_edit' => 'বর্ষ হালনাগাদ করুন',
    'session_list' => 'বর্ষ তালিকা',
    'session_view' => 'বর্ষ বিস্তারিত',
    // For Pilgrim search for Payment
    'new_payment_and_group_payment' => 'পেমেন্টের জন্য ভাউচার/রশিদ সার্চ',
    // For Search
    'search_list' => 'অনুসন্ধান করুন',
    // For Support
    'support' => 'Support',
    'sms_status' => 'SMS Status',
    'pdf_status' => 'PDF Status',
    //For Security
    'security' => 'সিকিউরিটি প্রোফাইল',
    'security_list' => 'নিরাপত্তা তালিকা',
    'security_form' => 'নতুন নিরাপত্তা',
    //   For Feedback (Support)
    'feedback_form_title' => 'সহায়তা',
    'feedback_form' => 'নতুন সাপোর্ট টিকেট',
    'feedback_edit' => 'সাপোর্ট টিকেট পরিবর্তন',
    'feedback_view' => 'সাপোর্ট টিকেট বিস্তারিত',
    'feedback_list' => 'সাপোর্ট টিকেট',
    'feedback' => 'সাপোর্ট টিকেট',
    'new_feedback' => 'সাপোর্ট টিকেট তৈরি',
];
