@extends ('layouts.plane')

@section ('body')
<style>
    body {
        background: url('assets/images/top_bg.jpg') no-repeat scroll 0 0;
    }
    .header-cls{
        width: 100%; height: auto; opacity:0.7;
    }
    .box-div {
        background: #53b67f  none repeat scroll 0 0;//#84B685  none repeat scroll 0 0;
        border-radius: 8px;
        border: 1px solid #c0dfc0;
    }
    .hr{
        border-top: 1px solid #d3d3d3;
        box-shadow: 0 1px #fff inset;
        margin: 0px;
        padding: 0px;

    }
    .btn-beta{
        background-color: #F69522 !important;
        padding: 0px 2px;
        font-size: 16px;
        line-height: 16px;
    }
    .company-info {
        color: #333;
        font-size: 12px;
        font-weight: normal;
        padding-bottom: 3px;
        padding-top: 2px;
        text-align: center;
    }
    .less-padding {
        padding: 1px;
        margin: 0px !important;
    }
    .top-border{
        border-top: 3px steelblue solid !important;
        padding-bottom: 5px !important;
        margin-bottom: 5px !important;
        margin-top: 0px !important;
    }
</style>
<script>
    function changeCaptcha() {
        $.ajax({
            type: "GET",
            url: '<?php echo url(); ?>/re-captcha',
            success: function (data) {
                $("#rowCaptcha").html(data);
            }
        });
    }
</script>



<header style="width: 100%; height: auto; opacity:0.7;">
    <div class="col-md-12 text-center">
        <div class="col-md-4"></div>
        <div class="col-md-4"  style="margin-top:5px;">

            {!! Html::image('assets/images/logo_beza.png') !!}
            </br>
            <h4 class="less-padding" style="color: #119213;">Welcome to One Stop Service (OSS) Desk <span class="btn btn-warning btn-xs btn-beta"><strong>Beta</strong></span></h4>

            </br>
            <hr class="hr" />

        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="clearfix"> <br></div>
</header>

<div class="col-md-12">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <hr class="top-border"/>
    </div>
    <div class="col-md-1"></div>
</div>

<div class="container" style="margin-top:20px;">

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
                <div class="text-center">
                    <!--<h3> .....::Learn or share something::.....</h3>-->
                    <?php
                    /*foreach ($DeshboardObject as $row) {
                        $div = 'dbobj_' . $row->db_obj_id;
                        ?>
                        <div class="col-md-4" style="margin-left:60px; ">
                            <?php
                            $para1 = DB::select(DB::raw($row->db_obj_para1));

                            switch ($row->db_obj_type) {
                                case 'SCRIPT':
                                    ?>
                                    <div id="<?php echo $div; ?>" style="width: 380px; height: 350px; text-align:center;"><br /><br />Chart will be loading in 5 sec...</div>
                                    <?php
                                    $script = $row->db_obj_para2;
                                    $datav['charttitle'] = $row->db_obj_title;
                                    $datav['chartdata'] = json_encode($para1);
                                    $datav['baseurl'] = url();
                                    $datav['chartediv'] = $div;
                                    echo '<script type="text/javascript">' . CommonFunction::updateScriptPara($script, $datav) . '</script>';
                                    break;
                                //case 'LIST':
                                //  $data= $para1;
                                // report_gen($row->db_obj_id,$data,$row->db_obj_title,$row->db_obj_para2);
                                //  break;
                                default:
                                    break;
                            }
                            ?>
                        </div>
                        <?php
                    }*/
                    ?>
                </div><!-- /.panel-heading -->
                <div class="panel panel-body">
                    @include('login-dashboard')
                </div><!-- /.panel-body -->
            </div>
            {{--<div class="col-md-2"></div>--}}
            <div class="col-md-3 box-div">
                <div class="text-center">
                    <h3>User Access</h3>
                </div>
                <div class="pannel">
                    @if (count($errors))
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <li>{!!$error!!}</li>
                        @endforeach
                    </ul>
                    @endif
                    {!!session()->has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'.session('success') .'</div>' : '' !!}
                    {!!session()->has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. session('error') .'</div>' : '' !!}
                    {!! Form::open(array('url' => 'login/check','method' => 'post', 'class' => '')) !!}
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="E-mail" required name="email" value="{{old('email')}}" type="email" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" required name="password" type="password">
                        </div>
                        <?php if (Session::get('hit') >= 3) { ?>
                            <div class="form-group">
                                <span id="rowCaptcha"><?php echo Captcha::img(); ?></span> <img onclick="changeCaptcha();" src="assets/images/refresh.png" class="reload" alt="Reload" />
                            </div>
                            <div class="form-group" style="margin-top: 15px;">
                                <input class="form-control required" required placeholder="Enter captcha code" name="captcha" type="text">
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary  btn-block"><b>Login to your account!</b></button>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <span class="pull-right">
                                    {!! link_to('forget-password', 'Forget Password?', array("class" => "text-right")) !!} 
                                </span>
                                <br/>
                                <span class="pull-right">
                                    New User? {!! link_to('users/create', 'Sign Up', array("class" => "text-right ")) !!}
                                </span>
                                <br/>
                                <div class="pull-right">
                                    {!! link_to('users/support', 'Need Help?', array("class" => "text-right")) !!}
                                </div>
                                <br/>
                                <br/>
                                <span class="pull-right">
                                    {!! link_to('users/create/va', 'For Visa Assistance Sign Up Here', array("class" => "text-right btn btn-danger")) !!}
                                </span>
                                <br/>
                                <br/>
                            </div>
                        </div>

                        <div class="text-right">
                            <span style="font-size: smaller">Powered By</span>
                            <br/>
                            {!!  Html::image('assets/images/business_automation.png','Business Automation logo',['width'=>'80']) !!}
                            <br/> <br/>
                        </div>
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div>
        <br/>
        <hr class=" less-padding"/>
        <p class="company-info">
            <em>Powered by  <a href="http://www.batworld.com">Business Automation Ltd. </a> in association with  <a href="http://ocpl.com.bd">OCPL</a>. </em>
        </p>
    </div>
</div>
@stop

@section ('footer-script')
<script type="text/javascript">
    $(function () {
        $('.notice_heading').click(function () {
            $(this).parent().parent().find('.details').fadeToggle();
            return false;
        });
    });
</script>
@endsection