<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>{!!trans('messages.app_title')!!}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>

    <link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
    <!--/*comment by jakir*/<link rel="stylesheet" href="{{ asset("assets/stylesheets/formWizerd.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}" />-->


    <link rel="stylesheet" href="{{ asset("assets/stylesheets/print.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/scripts/datatable/dataTables.bootstrap.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/scripts/datatable/responsive.bootstrap.min.css") }}" />

    <link rel="stylesheet" href="{{ asset("assets/stylesheets/bootstrap-datetimepicker.css") }}" />

    <link rel="stylesheet" href="{{ asset("assets/stylesheets/custom.css") }}" />

    <script src="{{ asset("assets/scripts/jquery.min.js") }}"  type="text/javascript"></script>

</head>
<body>
	@yield('body')
	@include('layouts.image_config')
 <!-- jQuery -->
    <script src="{{ asset("assets/scripts/jquery.min.js") }}"  type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset("assets/scripts/bootstrap.min.js") }}" type="text/javascript"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset("assets/scripts/metis-menu.min.js") }}" type="text/javascript"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset("assets/scripts/raphael-min.js") }}" type="text/javascript"></script>
    
    <script src="{{ asset("assets/scripts/metis-menu.min.js") }}" type="text/javascript"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset("assets/scripts/sb-admin-2.js") }}" type="text/javascript"></script>


    <script src="{{ asset("assets/scripts/jquery.validate.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

    <script src="{{ asset("assets/scripts/image-processing.js") }}" type="text/javascript"></script>
	

    @yield('footer-script')
</body>
</html>