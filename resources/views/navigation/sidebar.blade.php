<?php
$user_type = Auth::user()->user_type;
$type = explode('x', $user_type);
?>
@if(Auth::user()->user_status='active' && (Auth::user()->is_approved == 1 or Auth::user()->is_approved == true))
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="{{ (Request::is('/dashboard') ? 'active' : '') }}">
                <a href="{{ url ('/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i>
                    {!!trans('messages.dashboard')!!}
                </a>
            </li>
            @if($type[0] !=11) <!-- Bank user -->
            <li class="{{ (Request::is('/') ? 'active' : '') }}">
                <a href="{{ url ('/#') }}"><i class="fa fa-paperclip fa-fw"></i>
                    Application
                    <span class="fa arrow"></span></a>
                <?php
                $pcActive = '';
                $visaRecoActive = '';
                $workPermitActive = '';
                $exportActive = '';
                $importActive = '';
                $visaAssisActive = '';
                $sampleDocActive = '';
                $colpseIn = '';
                if (Request::is('project-clearance/list') || Request::is('project-clearance/create-form') || Request::is('project-clearance/edit-form/*') || Request::is('project-clearance/view/*')) {
                    $pcActive = 'active';
                    $colpseIn = 'in';
                }
                if (Request::is('sample-doc/list')) {
                    $sampleDocActive = 'active';
                    $colpseIn = 'in';
                }
                if (Request::is('visa-assistance/list') || Request::is('visa-assistance/form') || Request::is('visa-assistance/edit-form/*') || Request::is('visa-assistance/view/*')) {
                    $visaAssisActive = 'active';
                    $colpseIn = 'in';
                }
                if (Request::is('visa-recommend/list') || Request::is('visa-recommend/form') || Request::is('visa-recommend/edit-form/*') || Request::is('visa-recommend/view/*')) {
                    $visaRecoActive = 'active';
                    $colpseIn = 'in';
                }
                if (Request::is('work-permit/list') || Request::is('work-permit/form') || Request::is('work-permit/edit-form/*') || Request::is('work-permit/view/*')) {
                    $workPermitActive = 'active';
                    $colpseIn = 'in';
                }
                if (Request::is('export-permit/list') || Request::is('export-permit/form') || Request::is('export-permit/edit-form/*') || Request::is('export-permit/view/*')) {
                    $exportActive = 'active';
                    $colpseIn = 'in';
                }
                if (Request::is('import-permit/list') || Request::is('import-permit/form') || Request::is('import-permit/edit-form/*') || Request::is('import-permit/view/*')) {
                    $importActive = 'active';
                    $colpseIn = 'in';
                }
                ?>
                @if(Auth::user()->is_approved == 1 && (! (Auth::user()->first_login == 0 AND in_array($type[0], [5,6]))))
                <ul class="nav nav-second-level in">

                    @if($type[0] == '1' || $type[0] == '2' || $type[0] == '4' || $type[0] == '5' || $type[0] == '7' || $type[0]=='8' || Auth::user()->desk_id == 5 || Auth::user()->desk_id == 6)
                    <li class="{{ $pcActive }}">
                        <a class="{{$pcActive}}" href="{{ url ('project-clearance/list-of-clearance/project') }}">
                            <i class="fa fa-file-text-o fa-fw"></i> Project Clearance
                        </a>
                    </li>
                    @endif

                    @if($type[0] == '1' || $type[0] == '2' || $type[0] == '4' || $type[0] == '6' || $type[0] == '7' || $type[0]=='8' || Auth::user()->desk_id == 5 || Auth::user()->desk_id == 6)
                    <li class="{{ $visaAssisActive }}"> <!-- visa-assistance/list -->
                        <a class="{{$visaAssisActive}}" href="{{ url ('visa-assistance/list') }}">
                            <i class="fa fa-file-text-o fa-fw"></i> Visa Assistance
                        </a>
                    </li>
                    @endif
                    @if($type[0] == '1' || $type[0] == '2' || $type[0] == '4' || $type[0] == '5' || $type[0] == '7' || $type[0]=='8' || Auth::user()->desk_id == 5 || Auth::user()->desk_id == 6)
                    <li class="{{$visaRecoActive}}"> <!-- visa-recommend/list -->
                        <a class="{{$visaRecoActive}}" href="{{ url ('visa-recommend/list') }}">
                            <i class="fa fa-file-text-o fa-fw"></i> Visa Recommendation
                        </a>
                    </li>
                    @endif

                    @if($type[0] == '1' || $type[0] == '2' || $type[0] == '4' || $type[0] == '5' || $type[0] == '7' || $type[0]=='8' || Auth::user()->desk_id == 5 || Auth::user()->desk_id == 6)
                    <li class="{{$workPermitActive}}"> <!-- work-permit/list -->
                        <a class="{{$workPermitActive}}" href="{{ url ('work-permit/list') }}">
                            <i class="fa fa-file-text-o fa-fw"></i> Work Permit
                        </a>
                    </li>
                    @endif
                    @if($type[0] == '1' || $type[0] == '2' || $type[0] == '3' ||  $type[0] == '4' || $type[0] == '5' || $type[0] == '7' || $type[0]=='8' || $type[0]=='9' ||
                    Auth::user()->desk_id == 5 || Auth::user()->desk_id == 6)
                    <li class="{{ $exportActive }}"> <!-- export-permit/list -->
                        <a class="{{$exportActive}}" href="{{ url ('export-permit/list') }}">
                            <i class="fa fa-file-text-o fa-fw"></i> Export Permit
                        </a>
                    </li>
                    @endif
                    @if($type[0] == '1' || $type[0] == '2' || $type[0] == '3' || $type[0] == '4' || $type[0] == '5' || $type[0] == '7' || $type[0]=='8' || $type[0]=='9' ||
                    Auth::user()->desk_id == 5 || Auth::user()->desk_id == 6)
                    <li class="{{ $importActive }}"> <!-- import-permit/list -->
                        <a class="{{$importActive}}" href="{{ url ('import-permit/list') }}">
                            <i class="fa fa-file-text-o fa-fw"></i> Import Permit
                        </a>
                    </li>
                    @endif
                </ul>
                @endif
            </li>
            @endif


            @if(Auth::user()->is_approved == 1 && (!(Auth::user()->first_login == 0 AND (in_array($type[0], [5,6])))))
            @if($type[0] ==1)   {{-- For System Admin --}}
            {{--<li class="{{ (Request::is('process-path') ? 'active' : '') }}">
            <a href="{{ url ('/#') }}"><i class="fa fa-sitemap fa-fw"></i>
                Process Path
                <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li class="{{ (Request::is('process-path/project-clearance/update-form') ? 'active' : '') }}">
                    <a href="{{ url ('process-path/project-clearance/update-form') }}">
                        <i class="fa fa-road fa-fw"></i> Project Clearance
                    </a>
                </li>
                <li class="{{ (Request::is('process-path/visa-assistance/update-form') ? 'active' : '') }}">
                    <a href="{{ url ('process-path/visa-assistance/update-form') }}">
                        <i class="fa fa-road fa-fw"></i> Visa Assistance
                    </a>
                </li>
                <li class="{{ (Request::is('process-path/visa-recommendation/update-form') ? 'active' : '') }}">
                    <a href="{{ url ('process-path/visa-recommendation/update-form') }}">
                        <i class="fa fa-road fa-fw"></i> Visa Recommendation
                    </a>
                </li>
                <li class="{{ (Request::is('process-path/work-permit/update-form') ? 'active' : '') }}">
                    <a href="{{ url ('process-path/work-permit/update-form') }}">
                        <i class="fa fa-road fa-fw"></i> Work Permit
                    </a>
                </li>
                <li class="{{ (Request::is('process-path/export-permit/update-form') ? 'active' : '') }}">
                    <a href="{{ url ('process-path/export-permit/update-form') }}">
                        <i class="fa fa-road fa-fw"></i> Export Permit
                    </a>
                </li>
                <li class="{{ (Request::is('process-path/import-permit/update-form') ? 'active' : '') }}">
                    <a href="{{ url ('process-path/import-permit/update-form') }}">
                        <i class="fa fa-road fa-fw"></i> Import Permit
                    </a>
                </li>
            </ul>
            </li>--}}

            @endif
            @endif

            @if(Auth::user()->is_approved == 1 && Auth::user()->first_login != 0 AND  (in_array($type[0], [5,6])) )
            <li>
                <a class="@if (Request::is('cer-doc') || Request::is('cer-doc/*')) active @endif" href="{{ url ('/cer-doc')}}">
                    <i class="fa fa-file-pdf-o fa-fw"></i> Associated Documents
                </a>
            </li>
            @endif {{-- checking approved old applicant --}}

            @if(Auth::user()->is_approved == 1 && Auth::user()->first_login != 0 AND  (in_array($type[0], [1,5,6])) )
                <li>
                    <a class="@if (Request::is('bill-generation/bill-list') || Request::is('bill-generation/bill-list/*')) active @endif" 
                       href="{{ url ('/bill-generation/bill-list')}}">
                        <i class="fa fa-file-pdf-o fa-fw"></i> Bills
                    </a>
                </li>
            @endif {{-- checking approved old applicant --}}



            @if(Auth::user()->is_approved == 1 && (!(Auth::user()->first_login == 0 AND (in_array($type[0], [5,6]))))) {{-- not applicants --}}

         @if($type[0] ==11) {{-- For Bank users --}}
            <li>
                <a class="@if (Request::is('/bank-payment') || Request::is('/bank-payment/*')) active @endif" href="{{ url ('/bank-payment') }}">
                    <i class="fa fa-money fa-fw"></i> Payment
                </a>
            </li>
            @endif {{-- checking bank users --}}
            
            <li>
                <a class="@if (Request::is('reports') || Request::is('reports/*')) active @endif" href="{{ url ('/reports ')}}">
                    <i class="fa fa-book fa-fw"></i> {!! trans('messages.reports') !!}
                </a>
            </li>

            @if($type[0] ==1) {{-- For System Admin --}}
            <li>
                <a class="@if (Request::is('users') || Request::is('users/create-new-user')) active @endif" href="{{ url ('/users/lists') }}">
                    <i class="fa fa-users fa-fw"></i> {!!trans('messages.users')!!}
                </a>
            </li>

            <li class="{{ (Request::is('settings/*') ? 'active' : '') }}">
                <a href="{{ url ('/settings') }}"><i class="fa fa-gear fa-fw"></i>
                    <!--Settings--> {!!trans('messages.settings')!!}
                    <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class="@if(Request::is('settings/area-list') || Request::is('settings/create-area') || Request::is('settings/edit-area/*')) active @endif" href="{{ url ('/settings/area-list') }}">
                            <i class="fa fa-map-marker fa-fw"></i> {!!trans('messages.area')!!}
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('/cer-doc/list-types') || Request::is('/cer-doc/create-cer-type') || Request::is('cer-doc/edit-cer-type/*')) 
                           active @endif" href="{{ url ('/cer-doc/list-types') }}">
                            <i class="fa fa-file-archive-o fa-fw"></i> Associated Documents
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/bank-list') || Request::is('settings/create-bank') || Request::is('settings/view-bank/*')) active @endif" href="{{ url ('/settings/bank-list') }}">
                            <i class="fa fa-bank  fa-fw"></i> Bank
                        </a>
                    </li>

                    <li>
                        <a class="@if(Request::is('settings/branch-list') || Request::is('settings/create-branch') || Request::is('settings/view-branch/*')) active @endif" href="{{ url ('/settings/branch-list') }}">
                            <i class="fa fa-bank  fa-fw"></i> Bank Branch
                        </a>
                    </li>

                    <li>
                        <a class="@if(Request::is('settings/currency') || Request::is('settings/create-currency') || Request::is('settings/edit-currency/*')) active @endif" href="{{ url ('/settings/currency') }}">
                            <i class="fa fa-money  fa-fw"></i> Currency
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/document') || Request::is('settings/create-document') || Request::is('settings/edit-document/*')) active @endif"
                           href="{{ url ('/settings/document') }}">
                            <i class="fa fa-file-text fa-fw" aria-hidden="true"></i> <span>Documents</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/eco-zones') OR Request::is('settings/create-eco-zone') OR Request::is('settings/edit-eco-zone/*')) active @endif" href="{{ url ('/settings/eco-zones') }}">
                            <i class="fa fa-tree fa-fw" aria-hidden="true"></i> <span>Economic Zones</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/high-commission') || Request::is('settings/create-high-commission') ||
                           Request::is('settings/edit-high-commission/*')) active @endif" href="{{ url ('/settings/high-commission') }}">
                            <i class="fa fa-building fa-fw" aria-hidden="true"></i> <span>High Commission</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/hs-codes') || Request::is('settings/create-hs-code') ||
                           Request::is('settings/edit-hs-code/*')) active @endif" href="{{ url ('/settings/hs-codes') }}">
                            <i class="fa fa-codepen fa-fw" aria-hidden="true"></i> <span>HS Code</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/indus-cat') || Request::is('settings/create-indus-cat') ||
                           Request::is('settings/edit-indus-cat/*')) active @endif" href="{{ url ('/settings/indus-cat') }}">
                            <i class="fa fa-indent" aria-hidden="true"></i> <span>Industrial Category</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/notice') || Request::is('settings/create-notice') || Request::is('settings/edit-notice/*')) active @endif" href="{{ url ('/settings/notice') }}">
                            <i class="fa fa-list-alt fa-fw" aria-hidden="true"></i> <span>Notice</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/ports') || Request::is('settings/create-port') || Request::is('settings/edit-port/*')) active @endif" href="{{ url ('/settings/ports') }}">
                            <i class="fa fa-support fa-fw" aria-hidden="true"></i> <span>Ports</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/security') || Request::is('settings/edit-security/*')) active @endif" href="{{ url ('/settings/security') }}" href="{{ url ('/settings/security') }}">
                            <i class="fa fa-key fa-fw" aria-hidden="true"></i> <span>Security Profile</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('first_loginsettings/units') || Request::is('settings/create-unit') || Request::is('settings/edit-unit/*')) active @endif"
                           href="{{ url ('/settings/units') }}">
                            <i class="fa fa-beer fa-fw"></i> {!!trans('messages.units')!!}
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/user-types') || Request::is('settings/edit-user-type/*')) active @endif" href="{{ url ('/settings/user-type') }}">
                            <i class="fa fa-user fa-fw"></i> {!!trans('messages.user_type')!!}
                        </a>
                    </li>
                </ul>
            </li>

            @endif {{-- For System Admin --}}
            @endif

        </ul>

        <div class="panel panel-default sidefooter">

            <div class="panel-header text-center">
                <br/>Implemented by<br/><br/>
                {!!  Html::image('assets/images/business_automation.png','Business Automation logo',['width'=>'75']) !!}<br/>
                <br/>Supported by <br/><br/>
                <div class="">
                </div>
            </div>

        </div>
        <div class="panel-body">
            <small>Developed By <a href="http://batworld.com">Business Automation Ltd</a> in association with
                <a href="http://ocpl.com.bd/">OCPL</a>.</small>
        </div>

    </div><!-- /.sidebar-collapse -->
</div><!-- /.navbar-static-side -->
@endif {{--  user is active --}}
