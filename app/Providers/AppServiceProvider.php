<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


        $this->app['validator']->extend('alphaspace', function ($attribute, $value, $parameters)
        {
            // echo '<pre>';print_r($value);exit;
            if(!preg_match('/^[a-zA-Z ]*$/', $value)){
                return false;
            }
            return true;
        });

        $this->app['validator']->extend('numericarray', function ($attribute, $value, $parameters)
        {
            foreach ($value as $v) {
                if (!is_int($v)) {
                    return false;
                }
            }
            return true;
        });

        $this->app['validator']->extend('alphaspacearray', function ($attribute, $value, $parameters)
        {
            foreach ($value as $v) {
                if(!preg_match('/^[a-zA-Z ]*$/', $v)){
                    return false;
                }
            }
            return true;
        });

        $this->app['validator']->extend('requiredarray', function ($attribute, $value, $parameters)
        {
            foreach ($value as $v) {
                if(empty($v)){
                    return false;
                }
            }
            return true;
        });


        $this->app['validator']->extend('numberdotarray', function ($attribute, $value, $parameters)
        {
            foreach ($value as $v) {
                if(!preg_match('/^\d*(?:\.{1}\d+)?$/', $v)){
                    return false;
                }
            }
            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
