<?php

namespace App\Libraries;

use App\AuditLog;
use App\Modules\apps\Models\pdfSignatureQrcode;
use App\Modules\Dashboard\Models\Services;
use App\Modules\Files\Controllers\FilesController;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\BankBranch;
use App\Modules\Settings\Models\Configuration;
use App\Modules\Users\Models\UsersModel;
use App\Modules\Users\Models\UserTypes;
use App\Modules\Users\Models\UserDesk;
use App\Modules\visaAssistance\Models\VisaAssistance;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommonFunction {

    /**
     * @param Carbon|string $updated_at
     * @param string $updated_by
     * @return string
     * @internal param $Users->id /string $updated_by
     */
    public static function showAuditLog($updated_at = '', $updated_by = '') {
        $update_was = 'Unknown';
        if ($updated_at && $updated_at > '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }

        $user_name = 'Unknown';
        if ($updated_by) {
            $name = User::where('id', $updated_by)->first();
            if ($name) {
                $user_name = $name->user_full_name;
            }
        }
        return '<span class="help-block">Last updated : <i>' . $update_was . '</i> by <b>' . $user_name . '</b></span>';
    }

    public static function updatedOn($updated_at = '') {
        $update_was = '';
        if ($updated_at && $updated_at > '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }
        return $update_was;
    }

    public static function updatedBy($updated_by = '') {
        $user_name = 'Unknown';
        if ($updated_by) {
            $name = User::find($updated_by);
            if ($name) {
                $user_name = $name->user_full_name;
            }
        }
        return $user_name;
    }

    public static function getUserId() {

        if (Auth::user()) {
            return Auth::user()->id;
        } else {
            return 'Invalid Login Id';
        }
    }

    public static function getUserType() {

        if (Auth::user()) {
            return Auth::user()->user_type;
        } else {
            dd('Invalid User Type');
        }
    }

    public static function getUserTypeWithZero() {

        if (Auth::user()) {
            return Auth::user()->user_type;
        } else {
            return 0;
        }
    }

    public static function getUserSubTypeWithZero() {

        if (Auth::user()) {
            return Auth::user()->user_sub_type;
        } else {
            return 0;
        }
    }

    public static function getDeskId() {
        if (Auth::user()) {
            return Auth::user()->desk_id;
        } else {
            CommonFunction::redirectToLogin();
        }
    }

    public static function getRootAppId() {
        $userId = Auth::user()->id;
        $userType = Auth::user()->user_type;
        $rootAppId = 0;
        if ($userType == '5x505') { // applicant
            $rootAppId = ProjectClearance::where('status_id', 23)->where('created_by', $userId)->pluck('id'); // completed
        } else if ($userType == '6x606') { // visa assistance applicant
            $rootAppId = VisaAssistance::where('status_id', 23)->where('created_by', $userId)->pluck('id'); // completed
        }
        return $rootAppId;
    }

    public static function redirectToLogin() {
        echo "<script>location.replace('users/login');</script>";
    }

    public static function formateDate($date = '') {
        return date('d.m.Y', strtotime($date));
    }

    public static function getUserStatus() {

        if (Auth::user()) {
            return Auth::user()->user_status;
        } else {
            // return 1;
            dd('Invalid User Type');
        }
    }

    public static function convertUTF8($string) {
//        $string = 'u0986u09a8u09c7u09beu09dfu09beu09b0 u09b9u09c7u09beu09b8u09beu0987u09a8';
        $string = preg_replace('/u([0-9a-fA-F]+)/', '&#x$1;', $string);
        return html_entity_decode($string, ENT_COMPAT, 'UTF-8');
    }

    public static function showDate($updated_at = '') {
        if ($updated_at && $updated_at > '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }

        return '<span class="help-block"><i>' . $update_was . '</i></span>';
    }

    public static function checkUpdate($model, $id, $updated_at) {
        if ($model::where('updated_at', $updated_at)->find($id)) {
            return true;
        } else {
            return false;
        }
    }

    /* This function determines if an user is an admin or sub-admin
     * Based On User Type
     *  */

    public static function isAdmin() {
        $user_type = Auth::user()->user_type;
        /*
         * 1_101 for System Admin
         * 5_506 for UNO Admin
         * 10x411 for DC Office Admin
         * 7x712 for SB Admin
         * Desk id 6 = executive
         */
        if ($user_type == '1x101' || $user_type == '2x202' || Auth::user()->desk_id == 6 || Auth::user()->desk_id == 4) {
            return true;
        } else {
            return false;
        }
    }

    public static function isBank() {
        $user_type = Auth::user()->user_type;
        if ($user_type == '11x421' || $user_type == '11x422') {
            return true;
        } else {
            return false;
        }
    }

    public static function changeDateFormat($dateicker, $mysql = false) {
        if ($mysql) {
            return Carbon::createFromFormat('d-M-Y', $dateicker)->format('Y-m-d');
        } else {
            return Carbon::createFromFormat('Y-m-d', $dateicker)->format('d-M-Y');
        }
    }

    public static function age($birthDate) {
        $year = '';
        if ($birthDate) {
            $year = Carbon::createFromFormat('Y-m-d', $birthDate)->diff(Carbon::now())->format('%y years, %m months and %d days');
        }
        return $year;
    }

    public static function getFieldName($id, $field, $search, $table) {

        if ($id == NULL || $id == '') {
            return '';
        } else {
            return DB::table($table)->where($field, $id)->pluck($search);
        }
    }

    public static function getEcoId() {
        $eco_zone_id = ProjectClearance::where('created_by', Auth::user()->id)
                ->where('status_id', 23)
                ->first(['eco_zone_id']);
        if (!empty($eco_zone_id->eco_zone_id)) {
            return $eco_zone_id->eco_zone_id;
        } else {
            return 0;
        }
    }

    public static function getPicture($type, $ref_id) {
        $files = new FilesController();
        $img_data = $files->getFile(['type' => $type, 'ref_id' => $ref_id]);
        $json_data = json_decode($img_data->getContent());
        if ($json_data->responseCode == 1) {
            $base64 = $json_data->data;
        } else {
            $path = 'assets/images/no_image.png';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        return $base64;
    }

    public static function convert2Bangla($eng_number) {
        $eng = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $ban = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];
        return str_replace($eng, $ban, $eng_number);
    }

    public static function convert2English($ban_number) {
        $eng = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $ban = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];
        return str_replace($ban, $eng, $ban_number);
    }

    public static function generateTrackingID($prefix, $id) {
        $prefix = strtoupper($prefix);
        $str = $id . date('Y') . mt_rand(0, 9);
        if ($prefix == 'M' || $prefix == 'N') {
            if (strlen($str) > 12) {
                $str = substr($str, strlen($str) - 12);
            }
        } elseif ($prefix == 'B') {
            if (strlen($str) > 10) {
                $str = substr($str, strlen($str) - 10);
            }
        } else {
            if (strlen($str) > 14) {
                $str = substr($str, strlen($str) - 14);
            }
        }
        return $prefix . dechex($str);
    }

    public static function getImageConfig($type) {
        extract(CommonFunction::getImageDocConfig());
        $config = Configuration::where('caption', $type)->pluck('details');
        $reportHelper = new ReportHelper();
//        [File Format: *.jpg / *.png Dimension: {$height}x{$width}px File size($filesize)KB]
        if ($type == 'IMAGE_SIZE') {
            $data['width'] = ($IMAGE_WIDTH - ($IMAGE_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($IMAGE_WIDTH + ($IMAGE_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['height'] = ($IMAGE_HEIGHT - ($IMAGE_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($IMAGE_HEIGHT + ($IMAGE_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['variation'] = $IMAGE_DIMENSION_PERCENT;
            $data['filesize'] = $IMAGE_SIZE;
        } elseif ($type == 'DOC_IMAGE_SIZE') {
            $data['width'] = ($DOC_WIDTH - ($DOC_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($DOC_WIDTH + ($DOC_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['height'] = ($DOC_HEIGHT - ($DOC_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($DOC_HEIGHT + ($DOC_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['variation'] = $DOC_DIMENSION_PERCENT;
            $data['filesize'] = $DOC_SIZE;
        }
        $string = $reportHelper->ConvParaEx($config, $data);
        return $string;
    }

    public static function validateMobileNumber($mobile_no) {
        $mobile_validation_err = '';
        $first_digit = substr($mobile_no, 0, 1);
        $first_two_digit = substr($mobile_no, 0, 2);
        $first_four_digit = substr($mobile_no, 0, 5);
        // if first two digit is 01
        if (strlen($mobile_no) < 11) {
            $mobile_validation_err = 'Mobile number should be minimum 11 digit';
        } elseif ($first_two_digit == '01') {
            if (strlen($mobile_no) != 11) {
                $mobile_validation_err = 'Mobile number should be 11 digit';
            }
        }
        // if first two digit is +880
        else if ($first_four_digit == '+8801') {
            if (strlen($mobile_no) != 14) {
                $mobile_validation_err = 'Mobile number should be 14 digit';
            }
        }
        // if first digit is only
        else if ($first_digit == '+') {
            // Mobile number will be ok
        } else {
            $mobile_validation_err = 'Please enter valid Mobile number';
        }

        if (strlen($mobile_validation_err) > 0) {
            return $mobile_validation_err;
        } else {
            return 'ok';
        }
    }

    public static function getNotice($flag = 0) {
        if ($flag == 1) {
            $list = DB::select(DB::raw("SELECT date_format(updated_at,'%d %M, %Y') `Date`,heading,details,importance,id, case when importance='Top' then 1 else 0 end Priority FROM notice where status='public' or status='private' order by Priority desc, updated_at desc LIMIT 10"));
        } else {
            $list = DB::select(DB::raw("SELECT date_format(updated_at,'%d %M, %Y') `Date`,heading,details,importance,id, case when importance='Top' then 1 else 0 end Priority FROM notice where status='public' order by Priority desc, updated_at desc LIMIT 10"));
        }
        return $list;
    }

    public static function getImageDocConfig() {
        $config = array();
        $config['IMAGE_DIMENSION'] = Configuration::where('caption', 'IMAGE_SIZE')->pluck('value');
        $config['IMAGE_SIZE'] = Configuration::where('caption', 'IMAGE_SIZE')->pluck('value2');

        // Image size
        $split_img_size = explode('-', $config['IMAGE_SIZE']);
        $config['IMAGE_MIN_SIZE'] = $split_img_size[0];
        $config['IMAGE_MAX_SIZE'] = $split_img_size[1];

        // image dimension
        $split_img_dimension = explode('x', $config['IMAGE_DIMENSION']);
        $split_img_variation = explode('~', $split_img_dimension[1]);
        $config['IMAGE_WIDTH'] = $split_img_dimension[0];
        $config['IMAGE_HEIGHT'] = $split_img_variation[0];
        $config['IMAGE_DIMENSION_PERCENT'] = $split_img_variation[1];

        //image max/min width and height
        $config['IMAGE_MIN_WIDTH'] = $split_img_dimension[0] - (($split_img_dimension[0] * $split_img_variation[1]) / 100);
        $config['IMAGE_MAX_WIDTH'] = $split_img_dimension[0] + (($split_img_dimension[0] * $split_img_variation[1]) / 100);

        $config['IMAGE_MIN_HEIGHT'] = $split_img_variation[0] - (($split_img_variation[0] * $split_img_variation[1]) / 100);
        $config['IMAGE_MAX_HEIGHT'] = $split_img_variation[0] + (($split_img_variation[0] * $split_img_variation[1]) / 100);

        //========================= image config end =====================
        // for doc file
        $config['DOC_DIMENSION'] = Configuration::where('caption', 'DOC_IMAGE_SIZE')->pluck('value');
        $config['DOC_SIZE'] = Configuration::where('caption', 'DOC_IMAGE_SIZE')->pluck('value2');

        // Doc size
        $split_doc_size = explode('-', $config['DOC_SIZE']);
        $config['DOC_MIN_SIZE'] = $split_doc_size[0];
        $config['DOC_MAX_SIZE'] = $split_doc_size[1];

        // doc dimension
        $split_doc_dimension = explode('x', $config['DOC_DIMENSION']);
        $split_doc_variation = explode('~', $split_doc_dimension[1]);
        $config['DOC_WIDTH'] = $split_doc_dimension[0];
        $config['DOC_HEIGHT'] = $split_doc_variation[0];
        $config['DOC_DIMENSION_PERCENT'] = $split_doc_variation[1];

        //doc max/min width and height
        $config['DOC_MIN_WIDTH'] = $split_doc_dimension[0] - (($split_doc_dimension[0] * $split_doc_variation[1]) / 100);
        $config['DOC_MAX_WIDTH'] = $split_doc_dimension[0] + (($split_doc_dimension[0] * $split_doc_variation[1]) / 100);

        $config['DOC_MIN_HEIGHT'] = $split_doc_variation[0] - (($split_doc_variation[0] * $split_doc_variation[1]) / 100);
        $config['DOC_MAX_HEIGHT'] = $split_doc_variation[0] + (($split_doc_variation[0] * $split_doc_variation[1]) / 100);

        return $config;
    }

    public static function updateScriptPara($sql, $data) {
        $start = strpos($sql, '{$');
        while ($start > 0) {
            $end = strpos($sql, '}', $start);
            if ($end > 0) {
                $filed = substr($sql, $start + 2, $end - $start - 2);
                $sql = substr($sql, 0, $start) . $data[$filed] . substr($sql, $end + 1);
            }
            $start = strpos($sql, '{$');
        }
        return $sql;
    }

    public static function getUserTypeName() {
        if (Auth::user()) {
            $user_type_id = Auth::user()->user_type;
            $user_type_name = UserTypes::where('id', $user_type_id)
                    ->pluck('type_name');
            return $user_type_name;
        } else {
            CommonFunction::redirectToLogin();
        }
    }

    public static function getUserDeskName() {
        if (Auth::user()) {
            $desk_id = Auth::user()->desk_id;
            $desk_name = UserDesk::where('desk_id', $desk_id)->pluck('desk_name');
            return $desk_name;
        } else {
            return '';
        }
    }

    public static function getDeskName($desk_id) {
        if (Auth::user()) {
            $desk_name = UserDesk::where('desk_id', $desk_id)->pluck('desk_name');
            return $desk_name;
        } else {
            return '';
        }
    }

    public static function getStatusListForSBNSI() {
        $list = array('0' => 'Pending', '-1' => 'Aborted', '1' => 'Positive', '2' => 'Negative', '3' => 'Rejected');
        return $list;
    }

//    send sms or email
    public static function sendMessageFromSystem($param) {

        $mobileNo = $param[0]['mobileNo'] == '' ? '0' : $param[0]['mobileNo'];
        $smsYes = $param[0]['smsYes'] == '' ? '0' : $param[0]['smsYes'];
        $smsBody = $param[0]['smsBody'] == '' ? '' : $param[0]['smsBody'];
        $emailYes = $param[0]['emailYes'] == '' ? '1' : $param[0]['emailYes'];
        $emailBody = $param[0]['emailBody'] == '' ? '' : $param[0]['emailBody'];
        $emailHeader = $param[0]['emailHeader'] == '' ? '0' : $param[0]['emailHeader'];
        $emailAdd = $param[0]['emailAdd'] == '' ? 'base@gmail.com' : $param[0]['emailAdd'];
        $template = $param[0]['emailTemplate'] == '' ? '' : $param[0]['emailTemplate'];
        $emailSubject = $param[0]['emailSubject'] == '' ? '' : $param[0]['emailSubject'];
        $response_mail = '{"is_success":"0","trn_id":"0","error_code":"1","message":"0"}';
        if ($emailYes == 1) {
            $email = $emailAdd;
            $data = array(
                'header' => $emailHeader,
                'param' => $emailBody
            );
            \Mail::send($template, $data, function($message) use ($email, $emailSubject) {
                $message->from('no-reply@beza.com', 'BEZA');
                $message->to($email);
                $message->subject($emailSubject);
            });

            $response_mail = '{"is_success":"1","trn_id":"100","error_code":"0","message":"0"}';
        }

//        $smsYes = 1;
        $response_sms = '{"is_success":"0","trn_id":"0","error_code":"1","message":"0"}';
        if ($smsYes == 1) {
            $sms = $smsBody;
            $sms = str_replace(" ", "+", $sms);
            //        $sms = str_replace("<br>", "%0a", $sms);
            $mobileNo = str_replace("+88", "", "$mobileNo");
            $url = "http://202.4.119.45:777/syn_sms_gw/index.php?txtMessage=$sms&msisdn=$mobileNo&usrname=business_automation&password=bus_auto@789_admin";
//            echo $url;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($curl);
            curl_close($curl);
        }
        $mergeJson = '{"email":' . $response_mail . ',"sms":' . $response_sms . '}';

        return $mergeJson;
    }

    public static function showErrorPublic($param, $msg = 'Sorry! Something went wrong! Please try again later..') {
        $j = strpos($param, '(SQL:');
        if ($j > 15) {
            $param = substr($param, 8, $j - 9);
        } else {
            //
        }
        return $msg . $param;
    }

    public static function appInDesks() {
        $appsInDesk = Services::leftJoin('process_list', function($join) {
                    $join->on('service_info.id', '=', 'process_list.service_id');
                    if (Auth::user()->user_type != '1x101') {
                        $join->on('process_list.desk_id', '=', DB::raw(Auth::user()->desk_id));
                    }
                    $join->on('process_list.status_id', '!=', DB::raw(-1));
                })
                ->orderBy('service_info.name')
                ->groupBy('process_list.service_id')
                ->get(['name', 'short_name', 'url', 'panel', DB::raw('count(track_no) AS totalApplication')]);

        return $appsInDesk;
    }

    /**
     * @This function should be used to generate certificate from pdf server
      in time of processing the application if required.

     * @It will request to pdf server to insert a new record
     */
    public function certificateGenForUpdateBatch($service_id = 0, $app_id = 0, $pdf_type = "", $reg_key = "") {

        $data = array();
        $data['data'] = array(
            'reg_key' => $reg_key, // Secret authentication key
            'pdf_type' => $pdf_type, // letter type
            'ref_id' => $app_id, // app_id
            'param' => array(
                'app_id' => $app_id // app_id
            )
        );

        // Its should be needed for officer signature

        $user_data = UsersModel::where('id', '=', 1)->first();

        // File path URL comes from env url variable
        $signature_url = env('sign_url') . $user_data->signature;
        if (!empty($signature_url)) {
            $signature = ''; //file_get_contents($signature_url);
        } else {
            $signature = 'No signature found';
        }

        $get_pdf_signature = DB::table('pdf_signature_qrcode')->where('app_id', $app_id)->first();
        $qrCodeGenText = "test qr code";
        $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
        $url = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
            echo "\n<br />";
            $response = '';
        } else {
            curl_close($ch);
        }


        if ($get_pdf_signature) {
            // It will use in pdf server for retrieve signature and user info
            DB::table('pdf_signature_qrcode')->where('app_id', $app_id)->update([
                'signature' => $signature,
                'app_id' => $app_id,
                'user_id' => $user_data->id,
                'desk_id' => $user_data->desk_id
            ]);
        } else {
            // It will use in pdf server for retrieve signature and user info
            $pdfSinaQr = new pdfSignatureQrcode();
            $pdfSinaQr->signature = $signature;
            $pdfSinaQr->app_id = $app_id;
            $pdfSinaQr->qr_code = $response;
            $pdfSinaQr->user_id = $user_data->id;
            $pdfSinaQr->desk_id = $user_data->desk_id;
            $pdfSinaQr->save();
        }
        // End of the signature function

        $encode_data = json_encode($data);


        switch ($pdf_type) {

            case 'beza.certificate.local':
            case 'beza.certificate.uat':
            case 'beza.certificate.dev':
                // Request send to the pdf server
                $this->curlNewRequest($encode_data);
                break;

            case 'beza.undertaking.local':
            case 'beza.undertaking.uat':
            case 'beza.undertaking.dev':
                // Request send to the pdf server
                $this->curlNewRequest($encode_data);
                break;

            default:
        }

        return true; // return true for success
    }

    public function curlNewRequest($data) {
        // CURL request send to pdf server
        $url = "https://devpdfservice.pilgrimdb.org:8092/api/new-job?requestData=$data";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
            echo "\n<br />";
            $response = '';
        } else {
            curl_close($ch);
        }

        if (!is_string($response) || !strlen($response)) {
            echo "Failed to get contents.";
            $response = '';
        }
        // CURL request send to pdf server
        return true;
    }

    public static function bankTransactionStatus($bank_branch_id) {
        $status = BankBranch::join('bank', 'bank.id', '=', 'bank_branches.bank_id')
                ->where('bank_branches.id', $bank_branch_id)
                ->first(['bank.transaction_status']);
        return $status->transaction_status > 0 ? true : false;
    }
    
    public static function isArrears($bank_branch_id) {
        $status = BankBranch::join('bank', 'bank.id', '=', 'bank_branches.bank_id')
                ->where('bank_branches.id', $bank_branch_id)
                ->first(['bank.transaction_status']);
        return $status->transaction_status > 0 ? true : false;
    }

    public static function createAuditLog($module, $request, $id = '') {
        $data = $request->all();
        if ($id) {
            $data['id'] = $id;
        }
        try {
            unset($data['_token']);
            unset($data['_method']);
            unset($data['selected_file']);
            unset($data['TOKEN_NO']);
        } catch (\Exception $e) {
            echo 'Something went wrong in audit log! [CF0001]';
        }
        $details = json_encode($data);
        try {
            AuditLog::create([
                'remote_ip' => $request->ip(),
                'module' => $module,
                'details' => $details
            ]);
        } catch (\Exception $e) {
            echo 'Something went wrong in audit log! [CF0002]';
        }
    }

    public static function convert_number_to_words($number) {
        $common = new CommonFunction;
        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . $common->convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $common->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $common->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $common->convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    //   ConvParaEx function imported from Report Helper Libraries
    public static function ConvParaEx($sql, $data, $sm = '{$', $em = '}', $optional = false) {
        $sql = ' ' . $sql;
        $start = strpos($sql, $sm);
        $i = 0;
        while ($start > 0) {
            if ($i++ > 20) {
                return $sql;
            }
            $end = strpos($sql, $em, $start);
            if ($end > $start) {
                $filed = substr($sql, $start + 2, $end - $start - 2);
                if (strtolower(substr($filed, 0, 8)) == 'optional') {
                    $optionalCond = self::ConvParaEx(substr($filed, 9), $data, '[$', ']', true);
                    $sql = substr($sql, 0, $start) . $optionalCond . substr($sql, $end + 1);
                } else {
                    $inputData = self::getData($filed, $data, substr($sql, 0, $start));
                    if ($optional && (($inputData == '') || ($inputData == "''"))) {
                        $sql = '';
                        break;
                    } else {
                        $sql = substr($sql, 0, $start) . $inputData . substr($sql, $end + 1);
                    }
                }
            }
            $start = strpos($sql, $sm);
        }
        return trim($sql);
    }
    
        public static function getData($filed, $data, $prefix = null)
    {
        $filedKey = explode('|', $filed);
        $val = trim($data[$filedKey[0]]);
        if (!is_numeric($val)) {
            if ($prefix) {
                $prefix = strtoupper(trim($prefix));
                if (substr($prefix, strlen($prefix) - 3) == 'IN(') {
                    $vals = explode(',', $val);
                    $val = '';
                    for ($i = 0; $i < count($vals); $i++) {
                        if (is_numeric($vals[$i])) {
                            $val .= (strlen($val) > 0 ? ',' : '') . $vals[$i];
                        } else {
                            $val .= (strlen($val) > 0 ? ',' : '') . "'" . $vals[$i] . "'";
                        }
                    }
                } elseif (!(substr($prefix, strlen($prefix) - 1) == "'" || substr($prefix, strlen($prefix) - 1) == "%")) {
                    $val = "'" . $val . "'";
                }
            }
        }
        if ($val == '') $val = "''";
        return $val;
    }


    /*     * ****************************End of Class***************************** */
}
