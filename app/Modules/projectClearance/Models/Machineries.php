<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class Machineries  extends Model {

    protected $table = 'clr_machineries';
    protected $fillable = array(
        'id',
        'clearance_id',
        'm_details',
        'm_country',
        'm_vendor',
        'm_currency',
        'm_value',
        'm_purchase_state',
        'm_age',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
