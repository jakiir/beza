<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SponsorsDirectors extends Model {

    protected $table = 'clr_sponsors';
    protected $fillable = array(
        'id',
        'clearance_id',
        'sponsor_name',
        'sponsor_address',
        'sponsor_nationality',
        'sponsor_status',
        'sponsor_share_ext',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
