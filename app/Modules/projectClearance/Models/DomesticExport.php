<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class DomesticExport extends Model {

    protected $table = 'clr_domestic_export';
    protected $fillable = array(
        'id',
        'clearance_id',
        'pro_dom_desc',
        'pro_dom_1st',
        'pro_dom_2nd',
        'pro_dom_3rd',
        'pro_dom_4th',
        'pro_dom_5th',
        'pro_dom_total',
        'pro_dom_unit',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
