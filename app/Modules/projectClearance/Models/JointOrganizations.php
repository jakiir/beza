<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JointOrganizations extends Model {

    protected $table = 'clr_joint_orgs';
    protected $fillable = array(
        'id',
        'clearance_id',
        'joint_company',
        'joint_company_address',
        'joint_com_country',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
