<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class ProductionCost extends Model {

    protected $table = 'clr_production_cost';
    protected $fillable = array(
        'id',
        'clearance_id',
        'production_cost',
        'raw_cost_bd',
        'raw_cost_other',
        'pac_cost_bd',
        'pac_cost_other',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
