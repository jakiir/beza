<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Export extends Model {

    protected $table = 'clr_export';
    protected $fillable = array(
        'id',
        'clearance_id',
        'pro_ext_desc',
        'pro_ext_1st',
        'pro_ext_2nd',
        'pro_ext_3rd',
        'pro_ext_4th',
        'pro_ext_5th',
        'pro_ext_total',
        'pro_ext_unit',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
