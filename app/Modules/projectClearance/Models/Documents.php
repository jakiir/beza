<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class Documents extends Model {

    protected $table = 'document';
    protected $fillable = array(
        'id',
        'service_id',
        'app_id',
        'doc_name',
        'doc_file',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

/*******************************End of Model Class***********************************/
}
