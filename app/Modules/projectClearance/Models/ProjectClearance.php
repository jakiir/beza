<?php

namespace App\Modules\projectClearance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectClearance extends Model {

    protected $table = 'project_clearance';
    protected $fillable = array(
        'id',
        'tracking_number',
        'status_id',
        'bill_id',
        'bill_month',
        'payment_status_id',
        'applicant_name',
        'country',
        'district',
        'division',
        'state',
        'province',
        'road_no',
        'road_no',
        'sb_gk_verification_status',
        'sb_gk_verification_remarks',
        'nsi_gk_verification_status',
        'nsi_gk_verification_remarks',
        'house_no',
        'post_code',
        'phone',
        'fax',
        'email',
        'website',        
        'correspondent_country',
        'correspondent_division',
        'correspondent_district',
        'correspondent_state',
        'correspondent_province',
        'correspondent_road_no',
        'correspondent_house_no',
        'correspondent_post_code',
        'correspondent_phone',
        'correspondent_fax',
        'correspondent_email',
        
        'zone_type',
        'eco_zone_id',
        'proposed_name',
        'company_logo',
        'business_type',
        'organization_type',
        'industry_cat_id',
        'industry_type',
        'construction_start',
        'construction_end',
        'construction_duration',
        'cod_date',
        'auth_capital_to',
        'paid_capital_to',
        'ext_borrow_to',
        'paid_cap_amount',
        'paid_cap_nature',
        'paid_cap_percentage',        
        'agreed_land',
        'sfb_plot_address',
        'remarks',
        'product_name',
        'product_usage',
        'manufacture_process',
        'project_cost',
        
        'mp_year_1',
        'for_man_1',
        'for_skill_1',
        'for_unskill_1',
        'for_total_1',
        'loc_man_1',
        'loc_skill_1',
        'loc_unskill_1',
        'loc_total_1',
        'gr_total_1',
         
        'mp_year_2',
        'for_man_2',
        'for_skill_2',
        'for_unskill_2',
        'for_total_2',
        'loc_man_2',
        'loc_skill_2',
        'loc_unskill_2',
        'loc_total_2',
        'gr_total_2',
        
         'mp_year_3',
        'for_man_3',
        'for_skill_3',
        'for_unskill_3',
        'for_total_3',
        'loc_man_3',
        'loc_skill_3',
        'loc_unskill_3',
        'loc_total_3',
        'gr_total_3',
        
        'mp_year_4',
        'for_man_4',
        'for_skill_4',
        'for_unskill_4',
        'for_total_4',
        'loc_man_4',
        'loc_skill_4',
        'loc_unskill_4',
        'loc_total_4',
        'gr_total_4',
        
        'mp_year_5',
        'for_man_5',
        'for_skill_5',
        'for_unskill_5',
        'for_total_5',
        'loc_man_5',
        'loc_skill_5',
        'loc_unskill_5',
        'loc_total_5',
        'gr_total_5',
        
        'sales_export',
        'sales_exp_oriented',
        'sales_domestic',
        'sales_total',        
        'land_ini',
        'land_reg',
        'power_ini',
        'power_reg',
        'gas_ini',
        'gas_reg',
        'challan_no',
        'challan_amount',
        'challan_branch',
        'challan_date',
        'challan_file',
        'bank_name',
        'water_ini',
        'water_reg',
        'etp_ini',
        'etp_reg',
        'is_draft',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    function update_method($app_id, $data) {
        DB::table($this->table)
            ->where('id', $app_id)
            ->update($data);
    }
    function update_app($_id, $data) {
        DB::table($this->table)
            ->where('record_id', $_id)
            ->where('service_id', 1)
            ->update($data);
    }
    
/*******************************End of Model Class***********************************/
}
