<?php

Route::group(array('module' => 'projectClearance', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'], 'namespace' => 'App\Modules\projectClearance\Controllers'), function() {

    // search
    Route::get('project-clearance/search', 'projectClearanceController@search');
    Route::post('project-clearance/search-result', 'projectClearanceController@searchResult');
    Route::post('project-clearance/service-wise-result', 'projectClearanceController@serviceWiseStatus');
    Route::post('project-clearance/search-view/{param}', 'projectClearanceController@searchView');

    //app form
    Route::get('project-clearance/create-form', 'projectClearanceController@appForm');
    Route::get('project-clearance/edit-form/{id}', 'projectClearanceController@appFormEdit');
    Route::get('project-clearance/view/{id}', 'projectClearanceController@appFormView');
    Route::post('project-clearance/store-app', "projectClearanceController@appStore");

    Route::post('project-clearance/save-as-draft', "projectClearanceController@saveAsDraft");
    Route::any('project-clearance/upload-document', 'projectClearanceController@uploadDocument');
    Route::get('project-clearance/list-of-clearance/project', 'projectClearanceController@listOfClearance');
    Route::get('project-clearance/list-of-clearance/project/{ind}/{zone}', 'projectClearanceController@listOfClearance');

// Process
    Route::post('project-clearance/ajax/{param}', 'projectClearanceController@ajaxRequest');
    Route::patch('project-clearance/update-batch', "projectClearanceController@updateBatch");

    //Challan
    Route::post('project-clearance/challan-store/{id}', "projectClearanceController@challanStore");

    // PDF Generation   
    Route::get('project-clearance/certificate_gen/{id}', "projectClearanceController@certificate_gen");
    Route::get('project-clearance/project-cer-re-gen/{id}', "projectClearanceController@certificate_re_gen");

    Route::get('project-clearance/preview', 'projectClearanceController@preview');
    Route::get('project-clearance/re-download/{id}', 'projectClearanceController@appsDownloadPDF');

    Route::get('project-clearance/view-cer/{id}', 'projectClearanceController@viewAssociateCertificates');
    Route::get('project-clearance/discard-certificate/{id}', 'projectClearanceController@discardCertificate');

    // get color
    Route::get('project-clearance/colour-change', 'projectClearanceController@colorChange');
    // get Industrial list
    Route::post('project-clearance/get-companies', 'projectClearanceController@getValidCompanyList');

    Route::resource('project-clearance', 'projectClearanceController');

    /*     * ********************************End of Route group****************************** */
});
