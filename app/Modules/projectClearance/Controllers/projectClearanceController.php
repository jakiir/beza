<?php

namespace App\Modules\projectClearance\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\Apps\Models\ClearingCertificate;
use App\Modules\apps\Models\pdfQueue;
use App\Modules\apps\Models\pdfSignatureQrcode;
use App\Modules\Company\Models\Company;
use App\Modules\Dashboard\Models\Services;
use App\Modules\projectClearance\Models\Apps;
use App\Modules\apps\Models\docInfo;
use App\Modules\apps\Models\Document;
use App\Modules\Apps\Models\Status;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\Certificate\Models\DocCertificate;
use App\Modules\Certificate\Models\UploadedCertificates;
use App\Modules\ProcessPath\Models\ProcessHistory;
use App\Modules\projectClearance\Models\Colors;
use App\Modules\projectClearance\Models\DomesticExport;
use App\Modules\projectClearance\Models\Export;
use App\Modules\projectClearance\Models\IndustryCategories;
use App\Modules\projectClearance\Models\JointOrganizations;
use App\Modules\projectClearance\Models\Machineries;
use App\Modules\projectClearance\Models\Production;
use App\Modules\projectClearance\Models\ProductionCost;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\projectClearance\Models\SponsorsDirectors;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Settings\Models\Units;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\Users;
use App\Modules\Users\Models\UsersModel;
use App\Modules\workPermit\Models\Processlist;
use Carbon\Carbon;
use DB;
use Encryption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use mPDF;
use Session;

class projectClearanceController extends Controller {

    protected $service_id;

    public function __construct() {
        // Globally declaring service ID
        $this->service_id = 1; // 1 is Project Clearance
    }

    public function index() {
        if (!ACL::getAccsessRight('projectClearance', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $userId = CommonFunction::getUserId();
        $getList['result'] = Processlist::where('initiated_by', $userId)->get();
        return view("projectClearance::list", ['getList' => $getList]);
    }

    public function listOfClearance($ind = 0, $zone = 0) {
        if (!ACL::getAccsessRight('projectClearance', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            Session::put('sess_user_id', Auth::user()->id);

            $getList = Apps::getClearanceList();
            $appStatus = Status::where('service_id', 1)->get();
            $statusList = array();
            foreach ($appStatus as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
                $statusList[$v->status_id . 'color'] = $v->color;
            }
            $statusList[-1] = "Draft";
            $statusList['-1' . 'color'] = '#AA0000';

            $desks = UserDesk::all();
            $deskList = array();
            foreach ($desks as $k => $v) {
                $deskList[$v->desk_id] = $v->desk_name;
            }

            //for advance search
            $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
            $validCompanies = Processlist::leftJoin('project_clearance as pc', 'pc.id', '=', 'process_list.record_id')
                ->where('process_list.status_id', 23)
                ->where('pc.status_id', 23)
                ->orderBy('pc.applicant_name', 'ASC')
                ->lists('pc.applicant_name', 'pc.tracking_number')->all();
            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                ->orderBy('zone')->lists('zone', 'id');
            $mode = 'V';

            return view("projectClearance::list", compact('status', 'deskList', 'statusList', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'mode', 'validCompanies', 'economicZone', 'ind', 'zone'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appForm() {
        if (!ACL::getAccsessRight('projectClearance', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $statusArr = array(8, 22, '-1'); //8 is Discard, 22 is Rejected Application and -1 is draft
            $alreadyExistApplicant = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', $this->service_id)
                ->whereNotIn('process_list.status_id', $statusArr)
                ->where('project_clearance.created_by', $authUserId)
                ->first();
            if ($alreadyExistApplicant) {
                Session::flash('error', "You have already applied project clearance! Your tracking no is : " . $alreadyExistApplicant->tracking_number);
                return redirect()->back();
            }

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $zoneType = ['' => 'Select One',
                'Private Owned Economic Site' => 'Private Owned Economic Site',
                'Government Owned Economic Site' => 'Government Owned Economic Site'];

            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                ->orderBy('zone')->lists('zone', 'id');

            $businessIndustryServices = ['' => 'Select One',
                'Industry (Manufacturing)' => 'Industry (Manufacturing)',
                'Service Oriented Business' => 'Service Oriented Business'];
            $typeofOrganizations = ['' => 'Select One',
                'Private Limited' => 'Private Limited',
                'Public Limited' => 'Public Limited',
                'Joint Venture' => 'Joint Venture'];
            $typeofIndustry = ['' => 'Select One',
                'A - Foreign Oriented' => 'A - Foreign Oriented',
                'B - Joint Venture' => 'B - Joint Venture',
                'C - Local Oriented' => 'C - Local Oriented'];

            $units = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();
            $industry_cat = IndustryCategories::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            if ($alreadyExistApplicant) {
                $jointOrganizations = JointOrganizations::where('clearance_id', $alreadyExistApplicant->id)->get();
                $sponsorDirections = SponsorsDirectors::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_production = Production::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_product_cost = ProductionCost::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_domestic_export = DomesticExport::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_export = Export::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_machinary = Machineries::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
            } else {
                $jointOrganizations = [];
                $sponsorDirections = [];
                $clr_production = [];
                $clr_product_cost = [];
                $clr_domestic_export = [];
                $clr_machinary = [];
                $clr_export = [];
                $clrDocuments = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $colors = Colors::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $code = Colors::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('code');
            $viewMode = 'off';
            $mode = 'A';
            return view("projectClearance::application-form", compact('countries', 'colors', 'code', 'currencies', 'divition_eng', 'district_eng', 'economicZone', 'zoneType', 'units', 'businessIndustryServices', 'typeofOrganizations', 'typeofIndustry', 'document', 'alreadyExistApplicant', 'logged_user_info', 'jointOrganizations', 'clr_machinary', 'sponsorDirections', 'clr_production', 'clr_product_cost', 'clr_export', 'clr_domestic_export', 'clrDocuments', 'nationality', 'viewMode', 'mode', 'industry_cat'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function challanStore($id, Request $request) {
        $app_id = Encryption::decodeId($id);
//        if (!ACL::getAccsessRight('projectClearance', 'E',$app_id))
//            die('You have no access right! Please contact with system admin if you have any query.');

        $this->validate($request, [
            'payorder_no' => 'required',
            'bank_name' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'branch' => 'required',
            'payorder_file' => 'required|mimes:pdf',
        ]);

        $file = $request->file('payorder_file');
        $original_file = $file->getClientOriginalName();
        $file->move('uploads/', time() . $original_file);
        $filename = 'uploads/' . time() . $original_file;

        Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->update([
            'status_id' => 26, ///This is for challan re-submitted
            'desk_id' => 6,
        ]);
        ProjectClearance::find($app_id)->update([
            'challan_no' => $request->get('payorder_no'),
            'status_id' => 26, ///This is for challan re-submitted
            'bank_name' => $request->get('bank_name'),
            'challan_amount' => $request->get('amount'),
            'challan_branch' => $request->get('branch'),
            'challan_date' => Carbon::createFromFormat('d-M-Y', $request->get('date'))->format('Y-m-d'),
            'challan_file' => $filename,
            'updated_by' => CommonFunction::getUserId(),
        ]);
        \Session::flash('success', "Pay order information has been successfully updated!");
        return redirect()->back();
    }

    public function appFormEdit($id) {
        $app_id = Encryption::decodeId($id);
        if (!ACL::getAccsessRight('projectClearance', 'E', $app_id)) {
            abort('400',"You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = ProjectClearance::where('id', $app_id)->first();
            if ($alreadyExistApplicant) {
                $alreadyExistProcesslist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
                if ($alreadyExistProcesslist) {
                    if ($alreadyExistProcesslist->status_id != '-1' && $alreadyExistProcesslist->status_id != 5) {
                        Session::flash('error', "You have no access right! Please contact system administration for more information.");
                        $listOfVisaAssistant = DB::table('service_info')->where('id', 1)->pluck('url');
                        return redirect($listOfVisaAssistant);
                    }
                }
            }

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $zoneType = ['' => 'Select One',
                'Private Owned Economic Site' => 'Private Owned Economic Site',
                'Government Owned Economic Site' => 'Government Owned Economic Site'];

            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                ->orderBy('zone')->lists('zone', 'id');

            $businessIndustryServices = ['' => 'Select One',
                'Industry (Manufacturing)' => 'Industry (Manufacturing)',
                'Service Oriented Business' => 'Service Oriented Business'];
            $typeofOrganizations = ['' => 'Select One',
                'Private Limited' => 'Private Limited',
                'Public Limited' => 'Public Limited',
                'Joint Venture' => 'Joint Venture'];

            $typeofIndustry = ['' => 'Select One',
                'A - Foreign Oriented' => 'A - Foreign Oriented',
                'B - Joint Venture' => 'B - Joint Venture',
                'C - Local Oriented' => 'C - Local Oriented'];

            $units = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

            if ($alreadyExistApplicant) {
                $jointOrganizations = JointOrganizations::where('clearance_id', $alreadyExistApplicant->id)->get();
                $sponsorDirections = SponsorsDirectors::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_production = Production::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_product_cost = ProductionCost::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_domestic_export = DomesticExport::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_export = Export::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_machinary = Machineries::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
//            dd($clrDocuments);
                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $jointOrganizations = [];
                $sponsorDirections = [];
                $clr_production = [];
                $clr_product_cost = [];
                $clr_domestic_export = [];
                $clr_machinary = [];
                $clrDocuments = [];
                $processlistExist = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $industry_cat = IndustryCategories::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $colors = Colors::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $code = Colors::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('code');
            // $colors = DB::table('colors')->where('is_active', 1)->where('is_archieved', 0)->lists('name','code');
            $viewMode = 'off';
            $mode = 'E';
            return view("projectClearance::application-form", compact('countries', 'currencies', 'divition_eng', 'district_eng', 'economicZone', 'zoneType', 'units', 'businessIndustryServices', 'typeofOrganizations', 'typeofIndustry', 'document', 'alreadyExistApplicant', 'logged_user_info', 'jointOrganizations', 'clr_machinary', 'sponsorDirections', 'clr_production', 'clr_product_cost', 'clr_export', 'clr_domestic_export', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'mode', 'colors', 'code', 'industry_cat'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormView($id) {
        if (!ACL::getAccsessRight('projectClearance', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            $app_id = Encryption::decodeId($id);
//        breadcrumb start
            $form_data = ClearingCertificate::where('id', $app_id)->first();
            $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
            $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end

            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = ProjectClearance::where('id', $app_id)->first();
            $industryCatInfo = ProjectClearance::where('industry_cat_id', $alreadyExistApplicant->industry_cat_id)
                ->leftJoin('industry_categories', 'industry_categories.id', '=', 'project_clearance.industry_cat_id')
                ->leftJoin('colors', 'industry_categories.color_id', '=', 'colors.id')
                ->first(['industry_categories.name as industry_name', 'colors.name as color_name', 'colors.code as color_code']);

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();

            $zoneType = ['' => 'Select One',
                'Private Owned Economic Site' => 'Private Owned Economic Site',
                'Government Owned Economic Site' => 'Government Owned Economic Site'];

            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                ->orderBy('zone')->lists('zone', 'id');
            $businessIndustryServices = ['' => 'Select One',
                'Industry (Manufacturing)' => 'Industry (Manufacturing)',
                'Service Oriented Business' => 'Service Oriented Business'];
            $typeofOrganizations = ['' => 'Select One',
                'Private Limited' => 'Private Limited',
                'Public Limited' => 'Public Limited',
                'Joint Venture' => 'Joint Venture'];
            $typeofIndustry = ['' => 'Select One',
                'A - Foreign Oriented' => 'A - Foreign Oriented',
                'B - Joint Venture' => 'B - Joint Venture',
                'C - Local Oriented' => 'C - Local Oriented'];

            $units = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            $industry_cat = IndustryCategories::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            if ($alreadyExistApplicant) {
                $applicantInfo = Users::where('id', $alreadyExistApplicant->created_by)->first(['authorization_file']);
                $jointOrganizations = JointOrganizations::where('clearance_id', $alreadyExistApplicant->id)->get();
                $sponsorDirections = SponsorsDirectors::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_production = Production::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_product_cost = ProductionCost::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_domestic_export = DomesticExport::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_export = Export::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_machinary = Machineries::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $applicantInfo = '';
                $jointOrganizations = [];
                $sponsorDirections = [];
                $clr_production = [];
                $clr_product_cost = [];
                $clr_domestic_export = [];
                $clr_export = [];
                $clr_machinary = [];
                $clrDocuments = [];
                $processlistExist = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $colors = Colors::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $code = Colors::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('code');
            $viewMode = 'on';
            $mode = 'V';

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));
            return view("projectClearance::application-form", compact('countries', 'code', 'colors', 'currencies', 'divition_eng', 'district_eng', 'economicZone', 'zoneType', 'units', 'businessIndustryServices', 'typeofOrganizations', 'typeofIndustry', 'document', 'alreadyExistApplicant', 'logged_user_info', 'jointOrganizations', 'clr_machinary', 'sponsorDirections', 'clr_production', 'clr_product_cost', 'clr_export', 'clr_domestic_export', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'process_data', 'statusArray', 'banks', 'mode', 'applicantInfo', 'industry_cat', 'industryCatInfo'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appStore(Request $request, ProjectClearance $projectClearance, Processlist $processlist) {
      
        $authUserId = CommonFunction::getUserId();
        $statusArr = array(5, 8, 22, '-1'); // 5 is shortfall, 8 is Discard, 22 is Rejected Application and -1 is draft
        $alreadyExistApplicant = ProjectClearance::leftJoin('process_list', function($join) {
            $join->on('process_list.record_id', '=', 'project_clearance.id');
            $join->on('process_list.service_id', '=', DB::raw($this->service_id));
        })
            ->where('process_list.service_id', $this->service_id)
            ->whereNotIn('process_list.status_id', $statusArr)
            ->where('project_clearance.created_by', $authUserId)
            ->first();
        if ($alreadyExistApplicant) {
            Session::flash('error', "You have already submitted project clearance! Your tracking no is : " . $alreadyExistApplicant->tracking_number);
            return redirect()->back();
        }
        try {
            $app_id = (!empty($request->get('app_id')) ? Encryption::decodeId($request->get('app_id')) : '');
            $alreadyExistApplicant = ProjectClearance::where('created_by', $authUserId)->where('id', $app_id)->orderBy('id', 'ASC')->first();
            if ($alreadyExistApplicant) {
                if (!ACL::getAccsessRight('projectClearance', 'E', $app_id))
                    die('You have no access right! Please contact with system admin if you have any query.');
                $projectClearance = $alreadyExistApplicant;
            }else {
                if (!ACL::getAccsessRight('projectClearance', 'A'))
                    die('You have no access right! Please contact with system admin if you have any query.');
            }

            $projectClearance->applicant_name = $request->get('applicant_name');
            $projectClearance->country = $request->get('country');
            $projectClearance->division = $request->get('division');
            $projectClearance->district = $request->get('district');
            $projectClearance->state = $request->get('state');
            $projectClearance->province = $request->get('province');
            $projectClearance->road_no = $request->get('road_no');
            $projectClearance->house_no = $request->get('house_no');
            $projectClearance->post_code = $request->get('post_code');
            $projectClearance->phone = $request->get('phone');
            $projectClearance->fax = $request->get('fax');
            $projectClearance->email = $request->get('email');
            $projectClearance->website = $request->get('website');
            if ($request->get('same_as_authorized') != '') {
                $projectClearance->same_as_authorized = $request->get('same_as_authorized');
            }
            $projectClearance->correspondent_name = $request->get('correspondent_name');
            $projectClearance->correspondent_nationality = $request->get('correspondent_nationality');
            $projectClearance->correspondent_passport = $request->get('correspondent_passport');
            $projectClearance->correspondent_nid = $request->get('correspondent_nid');
            $projectClearance->correspondent_country = $request->get('correspondent_country');
            $projectClearance->correspondent_division = $request->get('correspondent_division');
            $projectClearance->correspondent_district = $request->get('correspondent_district');
            $projectClearance->correspondent_state = $request->get('correspondent_state');
            $projectClearance->correspondent_province = $request->get('correspondent_province');
            $projectClearance->correspondent_road_no = $request->get('correspondent_road_no');
            $projectClearance->correspondent_house_no = $request->get('correspondent_house_no');
            $projectClearance->correspondent_post_code = $request->get('correspondent_post_code');
            $projectClearance->correspondent_phone = $request->get('correspondent_phone');
            $projectClearance->correspondent_fax = $request->get('correspondent_fax');
            $projectClearance->correspondent_email = $request->get('correspondent_email');

            $projectClearance->zone_type = $request->get('zone_type');
            $projectClearance->eco_zone_id = $request->get('eco_zone_id');
            $projectClearance->proposed_name = $request->get('proposed_name');

            $prefix = date('Y_');
            $company_logo = $request->file('company_logo');
            $path = "uploads/" . date("Y") . "/" . date("m");

            if ($request->hasFile('company_logo')) {

                $img_file = trim(sprintf("%s", uniqid($prefix, true))) . $company_logo->getClientOriginalName();
                $mime_type = $company_logo->getClientMimeType();
                if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {

                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                        $myfile = fopen($path . "/index.html", "w");
                        fclose($myfile);
                    }

                    $company_logo->move($path, $img_file);
                    $projectClearance->company_logo = $path . '/' . $img_file;
                } else {
                    \Session::flash('error', 'Company type must be png or jpg or jpeg format');
                    return redirect('project-clearance/list-of-clearance/project');
                }
            }

            $projectClearance->business_type = $request->get('business_type');
            $projectClearance->organization_type = $request->get('organization_type');
            $projectClearance->industry_type = $request->get('industry_type');
            $projectClearance->industry_cat_id = $request->get('industry_cat_id');
            if ($request->get('construction_start') != '')
                $projectClearance->construction_start = CommonFunction::changeDateFormat($request->get('construction_start'), true);
            if ($request->get('construction_end') != '')
                $projectClearance->construction_end = CommonFunction::changeDateFormat($request->get('construction_end'), true);
            $projectClearance->construction_duration = $request->get('construction_duration');
            if ($request->get('cod_date') != '')
                $projectClearance->cod_date = CommonFunction::changeDateFormat($request->get('cod_date'), true);
            $projectClearance->auth_capital_to = $request->get('auth_capital_to');
            $projectClearance->paid_capital_to = $request->get('paid_capital_to');
            $projectClearance->ext_borrow_to = $request->get('ext_borrow_to');
            $projectClearance->paid_cap_amount = $request->get('paid_cap_amount');
            $projectClearance->paid_cap_nature = $request->get('paid_cap_nature');
            $projectClearance->paid_cap_percentage = $request->get('paid_cap_percentage');
            $projectClearance->agreed_land = $request->get('agreed_land');
            $projectClearance->sfb_plot_address = $request->get('sfb_plot_address');

            $projectClearance->product_name = $request->get('product_name');
            $projectClearance->product_usage = $request->get('product_usage');
            $projectClearance->manufacture_process = $request->get('manufacture_process');
            $projectClearance->project_cost = $request->get('project_cost');

            $projectClearance->mp_year_1 = $request->get('mp_year_1');
            $projectClearance->for_man_1 = $request->get('for_man_1');
            $projectClearance->for_skill_1 = $request->get('for_skill_1');
            $projectClearance->for_unskill_1 = $request->get('for_unskill_1');
            $projectClearance->for_total_1 = $request->get('for_total_1');
            $projectClearance->loc_man_1 = $request->get('loc_man_1');
            $projectClearance->loc_skill_1 = $request->get('loc_skill_1');
            $projectClearance->loc_unskill_1 = $request->get('loc_unskill_1');
            $projectClearance->loc_total_1 = $request->get('loc_total_1');
            $projectClearance->gr_total_1 = $request->get('gr_total_1');

            $projectClearance->mp_year_2 = $request->get('mp_year_2');
            $projectClearance->for_man_2 = $request->get('for_man_2');
            $projectClearance->for_skill_2 = $request->get('for_skill_2');
            $projectClearance->for_unskill_2 = $request->get('for_unskill_2');
            $projectClearance->for_total_2 = $request->get('for_total_2');
            $projectClearance->loc_man_2 = $request->get('loc_man_2');
            $projectClearance->loc_skill_2 = $request->get('loc_skill_2');
            $projectClearance->loc_unskill_2 = $request->get('loc_unskill_2');
            $projectClearance->loc_total_2 = $request->get('loc_total_2');
            $projectClearance->gr_total_2 = $request->get('gr_total_2');

            $projectClearance->mp_year_3 = $request->get('mp_year_3');
            $projectClearance->for_man_3 = $request->get('for_man_3');
            $projectClearance->for_skill_3 = $request->get('for_skill_3');
            $projectClearance->for_unskill_3 = $request->get('for_unskill_3');
            $projectClearance->for_total_3 = $request->get('for_total_3');
            $projectClearance->loc_man_3 = $request->get('loc_man_3');
            $projectClearance->loc_skill_3 = $request->get('loc_skill_3');
            $projectClearance->loc_unskill_3 = $request->get('loc_unskill_3');
            $projectClearance->loc_total_3 = $request->get('loc_total_3');
            $projectClearance->gr_total_3 = $request->get('gr_total_3');

            $projectClearance->mp_year_4 = $request->get('mp_year_4');
            $projectClearance->for_man_4 = $request->get('for_man_4');
            $projectClearance->for_skill_4 = $request->get('for_skill_4');
            $projectClearance->for_unskill_4 = $request->get('for_unskill_4');
            $projectClearance->for_total_4 = $request->get('for_total_4');
            $projectClearance->loc_man_4 = $request->get('loc_man_4');
            $projectClearance->loc_skill_4 = $request->get('loc_skill_4');
            $projectClearance->loc_unskill_4 = $request->get('loc_unskill_4');
            $projectClearance->loc_total_4 = $request->get('loc_total_4');
            $projectClearance->gr_total_4 = $request->get('gr_total_4');

            $projectClearance->mp_year_5 = $request->get('mp_year_5');
            $projectClearance->for_man_5 = $request->get('for_man_5');
            $projectClearance->for_skill_5 = $request->get('for_skill_5');
            $projectClearance->for_unskill_5 = $request->get('for_unskill_5');
            $projectClearance->for_total_5 = $request->get('for_total_5');
            $projectClearance->loc_man_5 = $request->get('loc_man_5');
            $projectClearance->loc_skill_5 = $request->get('loc_skill_5');
            $projectClearance->loc_unskill_5 = $request->get('loc_unskill_5');
            $projectClearance->loc_total_5 = $request->get('loc_total_5');
            $projectClearance->gr_total_5 = $request->get('gr_total_5');

            $projectClearance->sales_export = $request->get('sales_export');
            $projectClearance->sales_exp_oriented = $request->get('sales_exp_oriented');
            $projectClearance->sales_domestic = $request->get('sales_domestic');
            $projectClearance->sales_total = $request->get('sales_total');

            $projectClearance->land_ini = $request->get('land_ini');
            $projectClearance->land_reg = $request->get('land_reg');
            $projectClearance->power_ini = $request->get('power_ini');
            $projectClearance->power_reg = $request->get('power_reg');
            $projectClearance->gas_ini = $request->get('gas_ini');
            $projectClearance->gas_reg = $request->get('gas_reg');
            $projectClearance->water_ini = $request->get('water_ini');
            $projectClearance->water_reg = $request->get('water_reg');
            $projectClearance->etp_ini = $request->get('etp_ini');
            $projectClearance->etp_reg = $request->get('etp_reg');
            $acceptTerms = (!empty($request->get('acceptTerms')) ? 1 : 0);
            $projectClearance->acceptance_of_terms = $acceptTerms;

            $projectClearance->created_by = $authUserId;
            $projectClearance->save();


            if (empty($request->get('sv_draft'))) {
                /* if applicant submit the application without saving as draft or from draft submit an application track no will be generated
                  // if status id = 5 or shortfall, tracking id will not be regenerated */
                if ((!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number == '' && $alreadyExistApplicant->status_id != 5) ||
                    empty($alreadyExistApplicant)) {
                    $tracking_number = 'PC-' . date("dMY") . $this->service_id . str_pad($projectClearance->id, 6, '0', STR_PAD_LEFT);
                } else {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                }
            } else {
                if (!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number != '' && $alreadyExistApplicant->status_id == 5) {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                } else {
                    $tracking_number = '';
                }
            }

            if (empty($request->get('sv_draft'))) {
                $projectClearance->status_id = 1;
                $projectClearance->is_draft = 0;
            } else {
                $projectClearance->status_id = -1;
                $projectClearance->is_draft = 1;
            }

            $projectClearance->tracking_number = $tracking_number;
            $projectClearance->save();

            ///For Tracking ID generating and update
            /* if (!$alreadyExistApplicant) {
              $tracking_number = 'PC-' . date("dMY") . $this->service_id . str_pad($projectClearance->id, 6, '0', STR_PAD_LEFT);
              $projectClearance->tracking_number = $tracking_number;
              $projectClearance->save();
              } else {
              $tracking_number = $projectClearance->tracking_number;
              } */

            $data = $request->all();
            $jointIds = [];
            foreach ($data['joint_company'] as $key => $jointCompany) {
                if (empty($data['joint_id'][$key])) {
                    $jointOrganizations = new JointOrganizations();
                } else {
                    $sponsorId = $data['joint_id'][$key];
                    $jointOrganizations = JointOrganizations::where('id', $sponsorId)->first();
                }
                $jointOrganizations->clearance_id = $projectClearance->id;
                $jointOrganizations->joint_company = $data['joint_company'][$key];
                $jointOrganizations->joint_company_address = $data['joint_company_address'][$key];
                $jointOrganizations->joint_com_country = $data['joint_com_country'][$key];
                $jointOrganizations->save();
                $jointIds[] = $jointOrganizations->id;
            }

            if (!empty($jointIds))
                JointOrganizations::where('clearance_id', $projectClearance->id)->whereNotIn('id', $jointIds)->delete();

            $sponsorIds = [];
            foreach ($data['sponsor_name'] as $key => $sponsor_name) {
                if (empty($data['sponsor_id'][$key])) {
                    $sponsorsDirectors = new SponsorsDirectors();
                } else {
                    $sponsorId = $data['sponsor_id'][$key];
                    $sponsorsDirectors = SponsorsDirectors::where('id', $sponsorId)->first();
                }
                $sponsorsDirectors->clearance_id = $projectClearance->id;
                $sponsorsDirectors->sponsor_name = $data['sponsor_name'][$key];
                $sponsorsDirectors->sponsor_address = $data['sponsor_address'][$key];
                $sponsorsDirectors->sponsor_nationality = $data['sponsor_nationality'][$key];
                $sponsorsDirectors->sponsor_status = $data['sponsor_status'][$key];
                $sponsorsDirectors->sponsor_share_ext = $data['sponsor_share_ext'][$key];
                $sponsorsDirectors->save();
                $sponsorIds[] = $sponsorsDirectors->id;
            }
            if (!empty($sponsorIds)) {
                SponsorsDirectors::where('clearance_id', $projectClearance->id)->whereNotIn('id', $sponsorIds)->delete();
            }
            $productionIds = [];
            foreach ($data['production_desc'] as $key => $production_desc) {
                if (empty($data['production_id'][$key])) {
                    $productions = new Production();
                    $productions->clearance_id = $projectClearance->id;
                } else {
                    $productionId = $data['production_id'][$key];
                    $productions = Production::where('id', $productionId)->first();
                }
                $productions->production_desc = $data['production_desc'][$key];
                $productions->production_1st = $data['production_1st'][$key];
                $productions->production_2nd = $data['production_2nd'][$key];
                $productions->production_3rd = $data['production_3rd'][$key];
                $productions->production_4th = $data['production_4th'][$key];
                $productions->production_5th = $data['production_5th'][$key];
                $productions->production_total = $data['production_total'][$key];
                $productions->production_unit = $data['production_unit'][$key];
                $productions->save();
                $productionIds[] = $productions->id;
            }
            if (!empty($productionIds)) {
                Production::where('clearance_id', $projectClearance->id)->whereNotIn('id', $productionIds)->delete();
            }
            $pro_extIds = [];
            foreach ($data['pro_ext_desc'] as $key => $pro_ext_desc) {
                if (empty($data['pro_ext_id'][$key])) {
                    $exports = new Export();
                } else {
                    $exportsId = $data['pro_ext_id'][$key];
                    $exports = Export::where('id', $exportsId)->first();
                }
                $exports->clearance_id = $projectClearance->id;
                $exports->pro_ext_desc = $data['pro_ext_desc'][$key];
                $exports->pro_ext_1st = $data['pro_ext_1st'][$key];
                $exports->pro_ext_2nd = $data['pro_ext_2nd'][$key];
                $exports->pro_ext_3rd = $data['pro_ext_3rd'][$key];
                $exports->pro_ext_4th = $data['pro_ext_4th'][$key];
                $exports->pro_ext_5th = $data['pro_ext_5th'][$key];
                $exports->pro_ext_total = $data['pro_ext_total'][$key];
                $exports->pro_ext_unit = $data['pro_ext_unit'][$key];
                $exports->save();
                $pro_extIds[] = $exports->id;
            }
            if (!empty($pro_extIds)) {
                Export::where('clearance_id', $projectClearance->id)->whereNotIn('id', $pro_extIds)->delete();
            }
            $domestic_extIds = [];
            foreach ($data['pro_dom_desc'] as $key => $pro_dom_desc) {
                if (empty($data['pro_dom_id'][$key])) {
                    $dom_export = new DomesticExport();
                } else {
                    $dom_exportId = $data['pro_dom_id'][$key];
                    $dom_export = DomesticExport::where('id', $dom_exportId)->first();
                }
                $dom_export->clearance_id = $projectClearance->id;
                $dom_export->pro_dom_desc = $data['pro_dom_desc'][$key];
                $dom_export->pro_dom_1st = $data['pro_dom_1st'][$key];
                $dom_export->pro_dom_2nd = $data['pro_dom_2nd'][$key];
                $dom_export->pro_dom_3rd = $data['pro_dom_3rd'][$key];
                $dom_export->pro_dom_4th = $data['pro_dom_4th'][$key];
                $dom_export->pro_dom_5th = $data['pro_dom_5th'][$key];
                $dom_export->pro_dom_total = $data['pro_dom_total'][$key];
                $dom_export->pro_dom_unit = $data['pro_dom_unit'][$key];
                $dom_export->save();
                $domestic_extIds[] = $dom_export->id;
            }
            if (!empty($domestic_extIds)) {
                DomesticExport::where('clearance_id', $projectClearance->id)->whereNotIn('id', $domestic_extIds)->delete();
            }
            $productionCostIds = [];
            foreach ($data['production_cost'] as $key => $product_cost) {
                if (empty($data['pro_cost_id'][$key])) {
                    $pro_cost = new ProductionCost();
                } else {
                    $proCostId = $data['pro_cost_id'][$key];
                    $pro_cost = ProductionCost::where('id', $proCostId)->first();
                }
                $pro_cost->clearance_id = $projectClearance->id;
                $pro_cost->production_cost = $data['production_cost'][$key];
                $pro_cost->raw_cost_bd = $data['raw_cost_bd'][$key];
                $pro_cost->raw_cost_other = $data['raw_cost_other'][$key];
                $pro_cost->pac_cost_bd = $data['pac_cost_bd'][$key];
                $pro_cost->pac_cost_other = $data['pac_cost_other'][$key];
                $pro_cost->save();
                $productionCostIds[] = $pro_cost->id;
            }
            if (!empty($productionCostIds)) {
                ProductionCost::where('clearance_id', $projectClearance->id)->whereNotIn('id', $productionCostIds)->delete();
            }

            $machineryIds = [];
            foreach ($data['m_details'] as $key => $machine_desc) {
                if (empty($data['machine_id'][$key])) {
                    $machine = new Machineries();
                } else {
                    $machineId = $data['machine_id'][$key];
                    $machine = Machineries::where('id', $machineId)->first();
                }
                $machine->clearance_id = $projectClearance->id;
                $machine->m_details = $data['m_details'][$key];
                $machine->m_country = $data['m_country'][$key];
                $machine->m_currency = $data['m_currency'][$key];
                $machine->m_value = $data['m_value'][$key];
                $machine->m_vendor = $data['m_vendor'][$key];
                $machine->m_purchase_state = $data['m_purchase_state'][$key];
                $machine->m_age = $data['m_age'][$key];
                $machine->save();
                $machineryIds[] = $machine->id;
            }
            if (!empty($machineryIds)) {
                Machineries::where('clearance_id', $projectClearance->id)->whereNotIn('id', $machineryIds)->delete();
            }

            $doc_row = docInfo::where('service_id', 1) // 1 is for Project Clearance
            ->get(['doc_id', 'doc_name']);

            ///Start file uploading
            if (isset($doc_row)) {
                foreach ($doc_row as $docs) {
//                if ($request->get('validate_field_' . $docs->doc_id) != '') {
                    $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ? $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                    $documnent_id = $docs->doc_id;
                    if ($request->get('document_id_' . $docs->doc_id) == '') {
                        Document::create([
                            'service_id' => 1, // 1 is for Project Clearance
                            'app_id' => $projectClearance->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    } else {
                        $documentId = $request->get('document_id_' . $docs->doc_id);

//                            dd($documentId);
//                        dd($request->get('validate_field_' . $docs->doc_id));

                        Document::where('id', $documentId)->update([
                            'service_id' => 1, // 1 is for Project Clearance
                            'app_id' => $projectClearance->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    }
//                }
                }
            }
            ///End file uploading
            ///Save data to process_list table

            $processlistExist = Processlist::where('record_id', $projectClearance->id)->where('service_id', $this->service_id)->first();
            $deskId = 0;
            if (!empty($request->get('sv_draft'))) {
                $statusId = -1;
                $deskId = 0;
            } else {
                $statusId = 1;
                $deskId = 3; // 3 is RD1
            }

            if (count($processlistExist) < 1) {
                $process_list_insert = Processlist::create([
                    'track_no' => $tracking_number,
                    'reference_no' => '',
                    'company_id' => '',
                    'service_id' => $this->service_id,
                    'initiated_by' => CommonFunction::getUserId(),
                    'closed_by' => 0,
                    'status_id' => $statusId,
                    'desk_id' => $deskId,
                    'record_id' => $projectClearance->id,
                    'eco_zone_id' => $request->get('eco_zone_id'),
                    'process_desc' => '',
                    'updated_by' => CommonFunction::getUserId()
                ]);
            } else {

                if ($processlistExist->status_id > -1) {
                    if (empty($request->get('sv_draft'))) {
                        $statusId = 10; // resubmitted
                    } else {
                        $statusId = $processlistExist->status_id; // if drafted, older status will remain
                    }
                }
                $processlisUpdate = array(
                    'track_no' => $tracking_number,
                    'service_id' => $this->service_id,
                    'status_id' => $statusId,
                    'desk_id' => $deskId,
                    'eco_zone_id' => $request->get('eco_zone_id'),
                );
                $processlist->update_app_for_pc($projectClearance->id, $processlisUpdate);
            }
            if (empty($request->get('sv_draft'))) {
                $flassMsg = "Your application has been submitted with tracking no: <strong>" . $tracking_number . "</strong>";
            } else {
                $flassMsg = "Your application has been drafted successfully";
            }

            Session::flash('success', $flassMsg);
            $listOfVisaAssistant = DB::table('service_info')->where('id', 1)->pluck('url');
            return redirect($listOfVisaAssistant);
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appsDownloadPDF($id) {
        try {
            $app_id = Encryption::decodeId($id);

            $form_data = ClearingCertificate::where('id', $app_id)->first();
            $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();

            $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');

            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = ProjectClearance::where('id', $app_id)->first();

            $industryCatInfo = ProjectClearance::where('industry_cat_id', $alreadyExistApplicant->industry_cat_id)
                ->leftJoin('industry_categories', 'industry_categories.id', '=', 'project_clearance.industry_cat_id')
                ->leftJoin('colors', 'industry_categories.color_id', '=', 'colors.id')
                ->first(['industry_categories.name as industry_name', 'colors.name as color_name', 'colors.code as color_code']);

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $districtName = AreaInfo::where('area_type', 2)->where('area_id', $alreadyExistApplicant->district)->first(['area_nm']);

            $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();

            $zoneType = ['' => 'Select One',
                'Private Owned Economic Site' => 'Private Owned Economic Site',
                'Government Owned Economic Site' => 'Government Owned Economic Site'];

            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                ->orderBy('zone')->lists('zone', 'id');

            $businessIndustryServices = ['' => 'Select One',
                'Industry (Manufacturing)' => 'Industry (Manufacturing)',
                'Service Oriented Business' => 'Service Oriented Business'];
            $typeofOrganizations = ['' => 'Select One',
                'Private Limited' => 'Private Limited',
                'Public Limited' => 'Public Limited',
                'Joint Venture' => 'Joint Venture'];

            $units = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

            if ($alreadyExistApplicant) {
                $jointOrganizations = JointOrganizations::where('clearance_id', $alreadyExistApplicant->id)->get();
                $sponsorDirections = SponsorsDirectors::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_production = Production::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_product_cost = ProductionCost::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_domestic_export = DomesticExport::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_export = Export::where('clearance_id', $alreadyExistApplicant->id)->get();
                $clr_machinary = Machineries::where('clearance_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_machinary as $machinary) {
                    $currencie_arr[$machinary->m_currency] = Currencies::where('is_active', 1)->where('id', $machinary->m_currency)->first(['code']);
                }
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $jointOrganizations = [];
                $sponsorDirections = [];
                $clr_production = [];
                $clr_product_cost = [];
                $clr_domestic_export = [];
                $clr_export = [];
                $clr_machinary = [];
                $clrDocuments = [];
                $processlistExist = [];
                $currencie_arr = [];
            }

            $logged_user_info = Users::where('id', $authUserId)->first();
            $viewMode = 'on';
            $mode = 'V';

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

            $contents = view("projectClearance::application-form-html", compact('countries', 'currencie_arr', 'divition_eng', 'economicZone', 'zoneType', 'units', 'businessIndustryServices', 'typeofOrganizations', 'document', 'alreadyExistApplicant', 'logged_user_info', 'jointOrganizations', 'clr_machinary', 'sponsorDirections', 'clr_production', 'clr_product_cost', 'clr_export', 'clr_domestic_export', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'process_data', 'statusArray', 'banks', 'mode', 'districtName', 'industryCatInfo'))->render();

//        return $contents;

            $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                12, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                15, // margin bottom
                10, // margin header
                9, // margin footer
                'P'
            );

            $mpdf->Bookmark('Start of the document');
            $mpdf->useSubstitutions;
            $mpdf->SetProtection(array('print'));
            $mpdf->SetDefaultBodyCSS('color', '#000');
            $mpdf->SetTitle("BEZA");
            $mpdf->SetSubject("Subject");
            $mpdf->SetAuthor("Business Automation Limited");
            $mpdf->autoScriptToLang = true;
            $mpdf->baseScript = 1;
            $mpdf->autoVietnamese = true;
            $mpdf->autoArabic = true;

            $mpdf->autoLangToFont = true;
            $mpdf->SetDisplayMode('fullwidth');
            $mpdf->setFooter('{PAGENO} / {nb}');
            $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
            $mpdf->setAutoTopMargin = 'stretch';
            $mpdf->setAutoBottomMargin = 'stretch';
            $mpdf->WriteHTML($stylesheet, 1);

            $mpdf->WriteHTML($contents, 2);

            $mpdf->defaultfooterfontsize = 10;
            $mpdf->defaultfooterfontstyle = 'B';
            $mpdf->defaultfooterline = 0;

            $mpdf->SetCompression(true);
            $mpdf->Output($process_data->track_no . '.pdf', 'I');   // Saving pdf "F" for Save only, "I" for view only.
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function uploadDocument() {
        return View::make('projectClearance::ajaxUploadFile');
    }

    public function colorChange(Request $request) {
        $industry_cat_id = $request->get('industry_cat_id');

        $color_code = DB::table('industry_categories')
            ->leftJoin('colors', 'industry_categories.color_id', '=', 'colors.id')
            ->where('industry_categories.id', $industry_cat_id)
            ->select('colors.code', 'colors.name')->first();
        echo json_encode($color_code);
        exit();
    }

    public function preview() {
        return view("projectClearance::preview");
    }

    /**
     * @param $param request type for ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');

        if ($param == 'process') {

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;

            $verifiedInfo = ClearingCertificate::where('id', $curr_app_id)->first();
            $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM app_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            // Get all applications' id
            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition
            $statusId_wz = sprintf("%02d", $statusId);

            //echo $sql;exit;
            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM app_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";

            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {

            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-status-list') {

            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');

            $verifiedInfo = ClearingCertificate::where('id', $curr_app_id)->first();
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM app_process_path APP WHERE APP.status_from = '$statusId' $cond))
                        AND APS.service_id = 1
                        order by APS.status_name
                        ";

            $statusList = \DB::select(DB::raw($sql));

            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @param Processlist $process_model
     * @param ProjectClearance $ProjectClearance_model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateBatch(Request $request, Processlist $process_model, ProjectClearance $ProjectClearance_model, CommonFunction $common) {

        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $status_id = $request->get('status_id');
        $attach_file = $request->file('attach_file');
        $service_id = 1;
        $onbehalf = $request->get('on_behalf_of');
        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }

        foreach ($apps_id as $app_id) {

            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->service_id = $this->service_id;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }

            $appInfo = Processlist::where('record_id', $app_id)->where('service_id', '=', $service_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;

            if (empty($desk_id)) {
                $whereCond = "select * from app_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'
                        AND status_to REGEXP '^([0-9]*[,]+)*$status_id([,]+[,0-9]*)*$'";

                $processPath = DB::select(DB::raw($whereCond));
                if ($processPath[0]->desk_to == '0')  // Sent to Applicant
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')   // Keep in same desk
                    $desk_id = $deskFrom;
            }

            $app_data = array(
                'status_id' => $status_id,
                'remarks' => $remarks,
                'updated_at' => date('y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            );


            $info_data = array(
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8 || $status_id == 14) {
                $info_data['closed_by'] = Auth::user()->id;
            }
            $process_model->update_app($app_id, $info_data);

            $ProjectClearance_model->update_method($app_id, $app_data);
            ProjectClearance::where('id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);

            if (in_array($status_id, array(5, 8, 23, 21, 22, 25, 27))) {
                $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $service_id)->first();

//            Notification
                $body_msg = '<span style="color:#000;text-align:justify;"><b>';
                if ($status_id == 23) {
                    $smsbody = "Will be given by BEZA later";
                    $applicantMobile = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_phone', 'users');

                    $params = array([
                        'emailYes' => '0',
                        'emailTemplate' => 'users::message',
                        'emailSubject' => 'Application has been accepted',
                        'emailBody' => $smsbody,
                        'emailHeader' => 'Successfully Saved the Application header',
                        'emailAdd' => 'shahin.fci@gmail.com',
                            'mobileNo' => $applicantMobile,
                        'smsYes' => '0',
                        'smsBody' => 'Your application has been submitted with tracking id: ' . $process_data->track_no . ' received. Please fill up your Pay order information!',
                    ]);
                    $returnMsg = CommonFunction::sendMessageFromSystem($params);
                    $body_msg .= 'Your application for ' . $process_data->track_no . ' has been ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status') . '.';
                } else {
                    $body_msg .= 'Your application for project clearance with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' . CommonFunction::getFieldName($status_id, 'status_id', 'status_name', 'app_status') . '</b>';
                }

//            $body_msg .= 'Your application for ' . $process_data->track_no . ' has been ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status');

                $body_msg .= '</span>';
                $body_msg .= '<br/><br/><br/>Thanks<br/>';
                $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                $data = array(
                    'header' => 'Application Update',
                    'param' => $body_msg
                );

                $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');
                $certificate = '';
                if ($status_id == 23) {

                    // pdf server code
                    $pdf_type = 'beza.pcl.'.env('server_type');
                    $reg_key = 'b5z1-p18j5320-312e1185143e2016';
                    $this->certificateGenForUpdateBatch($this->service_id,$app_id,$pdf_type,$reg_key);

                    
                    $certificate = $this->certificate_gen($app_id);   //Certificate Generation after payment accepted


                    $email_content = <<<HERE
       
           <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Ministry of Home Affairs</title>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
            <style type="text/css">
                *{
                    font-family: Vollkorn;
                } 
            </style>
    </head>


    <body>
        <table width="80%" style="background-color:#DFF0D8;margin:0 auto; height:50px; border-radius: 4px;">
            <thead>
                <tr>
                    <td style="padding: 10px; border-bottom: 1px solid rgba(0, 102, 255, 0.21);">
                        <img style="margin-left: auto; margin-right: auto; display: block;" src="http://dev-beza.eserve.org.bd/assets/images/logo_beza_single.png" width="80px"
                             alt="BEZA"/>
                            <h4 style="text-align:center">
                                Bangladesh Economic Zones Authority (BEZA)
                            </h4>
                    </td>
                </tr>
            </thead>
            
            <tbody>
                <tr>
                    <td style="margin-top: 20px; padding: 15px;">
                        <!--Dear Applicant,-->
                        Dear Applicant ,
                        <br/><br/>
                                               $body_msg

                        <br/><br/>
                    </td>
                </tr>
                <tr style="margin-top: 15px;">
                      <td style="padding: 1px; border-top: 1px solid rgba(0, 102, 255, 0.21);">
                        <h5 style="text-align:center">All right reserved by BEZA 2017</h5>
                    </td>
                </tr>
            </tbody>  
        </table>
    </body>
</html>
HERE;
                    $pdfQueue = new pdfQueue();
                    $pdfQueue->service_id = $service_id;
                    $pdfQueue->app_id = $app_id;
                    $pdfQueue->secret_key = $reg_key;
                    $pdfQueue->pdf_type = $pdf_type;
                    $pdfQueue->email_to = $fetched_email_address;
                    $pdfQueue->email_cc = 'shahin@batworld.com';
                    $pdfQueue->email_content = $email_content;

                    $pdfQueue->save();
                    // pdf server code end
                }
                $billMonth = date('Y-m');


                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $billMonth, $app_id, $certificate, $status_id) {

                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->cc('jakir.ocpl@batworld.com')
                        ->subject('Application Update Information for project clearance');
                    if ($status_id == 23) {
                        $message->attach($certificate);
                        ProjectClearance::where('id', $app_id)->update(['certificate' => $certificate,'bill_month'=>$billMonth]);
                    }
                });

//                    $increment++;
            }
        }

        //         for previous and present status
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        Session::flash('success', "Application status updated Previous status: $statusList[$status_from] || Present Status: $statusList[$status_id]");
        return redirect()->back();
    }

    public function certificate_re_gen($id) {
        $app_id = Encryption::decodeId($id);
        $processlistExist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        $certificate = '';
        if ($status_id == 23) {
            $certificate = $this->certificate_gen($app_id);   //Certificate Generation after payment accepted
            ProjectClearance::where('id', $app_id)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function certificate_gen($appID) {
        ini_set('memory_limit', '99M');

        $mpdf = new mPDF(
            'utf-8', // mode - default ''
            'A4', // format - A4, for example, default ''
            9, // font size - default 0
            'Times New Roman', // default font family
            17, // margin_left
            10, // margin right
            30, // margin top
            10, // margin bottom
            9, // margin header
            9, // margin footer
            'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');
        $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;
        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("beza_" . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');


        $alreadyExistApplicant = Processlist::leftJoin('project_clearance', 'project_clearance.id', '=', 'process_list.record_id')
            ->where('process_list.service_id', $this->service_id)
            ->where('process_list.record_id', $appID)
            ->first();

        /*         * **Start of commenting code for Static RD3 as instructed by BEZA ***************** */
//        $approver = Users::leftJoin('user_desk', 'users.desk_id', '=', 'user_desk.desk_id')
//                ->where('users.eco_zone_id', $alreadyExistApplicant->eco_zone_id)
//                ->where('users.desk_id', 5) // 5 is RD3 approver
//                ->first(['user_desk.desk_name as desk', 'user_full_name', 'signature']);
//        
//        $signature = (!empty($approver->signature) && file_exists($directory.$approver->signature)) ?
//                '<img src="users/signature/' . $approver->signature . '" alt="(signature)" width="150px"/>' : '';        
        /*         * **End of commenting code for Static RD3 as instructed by BEZA ***************** */

        $directory = 'users/signature/';
        $approver = 'Mohammed Ayub';
        $signature = ( file_exists($directory . 'rd3-sign.jpg')) ?
            'users/signature/rd3-sign.jpg' : '';

        if (!$alreadyExistApplicant) {
            return '';
        } else {

            $track_no = (!empty($alreadyExistApplicant->track_no) ? $alreadyExistApplicant->track_no : '');

            $ApproveData = ProcessHistory::where('process_id', $alreadyExistApplicant->process_id)->where('record_id', $appID)
                ->where('status_id', 23) // 23 = approved
                ->orderBy('p_hist_id', 'desc')->first();

            $formatted_date = '';
            if (!empty($ApproveData->created_at)) {
                $formatted_date = date_format($ApproveData->created_at, "d-M-Y");
            }
            $dateNow = !empty($formatted_date) ? $formatted_date : '';

            $ReceivedData = ProcessHistory::where('process_id', $alreadyExistApplicant->process_id)->where('record_id', $appID)
                ->where('status_id', 1) // 1 = submitted
                ->orderBy('p_hist_id', 'desc')->first();

            $app_received_date = '';
            if (!empty($ReceivedData->created_at)) {
                $app_received_date = date_format($ReceivedData->created_at, "d-M-Y");
            }

            $proposed_name = (!empty($alreadyExistApplicant->proposed_name) ? $alreadyExistApplicant->proposed_name : '');
            $product_name = (!empty($alreadyExistApplicant->product_name) ? $alreadyExistApplicant->product_name : '');

            $eco_zone_id = (!empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : '');
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);

            $organization_type = (!empty($alreadyExistApplicant->organization_type) ? $alreadyExistApplicant->organization_type : '');
            $business_type = (!empty($alreadyExistApplicant->business_type) ? $alreadyExistApplicant->business_type : '');
            $industry_type = (!empty($alreadyExistApplicant->industry_type) ? $alreadyExistApplicant->industry_type : '');

            $production_capacity_arr = Production::leftJoin('units', 'clr_production.production_unit', '=', 'units.id')
                ->where('clearance_id', $appID)->get(['production_desc', 'production_5th', 'units.name as unit_name']);
            $production_capacity_str = array();
            foreach ($production_capacity_arr as $row) {
                $production_capacity_str[] = $row->production_desc . ' ' . $row->production_5th . ' ' . $row->unit_name;
            }
            $production_capacity = implode(', ', $production_capacity_str);

            $production_cost_arr = ProductionCost::where('clearance_id', $appID)->get(['production_cost']);
            $production_cost = 0;
            foreach ($production_cost_arr as $row) {
                $production_cost += $row->production_cost;
            }

            $machineries_cost_arr = Machineries::where('clearance_id', $appID)->get(['m_value', 'm_purchase_state']);
            $machineries_cost = 0;
            $machineries_state = array();
            foreach ($machineries_cost_arr as $row) {
                $machineries_cost += $row->m_value;
                $machineries_state[] = $row->m_purchase_state;
            }

            if (in_array('New', $machineries_state) && in_array('Old', $machineries_state)) {
                $m_state = "brand new and old both";
            } else if (in_array('New', $machineries_state) && !in_array('Old', $machineries_state)) {
                $m_state = 'brand new';
            } else if (in_array('Old', $machineries_state) && !in_array('New', $machineries_state)) {
                $m_state = 'old';
            } else {
                $m_state = "brand new and old both";
            }

            $export_projection_arr = Export::where('clearance_id', $appID)->get(['pro_ext_5th']); // have to take 5th year data
            $total_export_projection = 0;
            foreach ($export_projection_arr as $row) {
                $total_export_projection += $row->pro_ext_5th;
            }

            $domestic_projection_arr = DomesticExport::where('clearance_id', $appID)->get(['pro_dom_5th']); // have to take 5th year data
            $total_domestic_projection = 0;
            foreach ($domestic_projection_arr as $row) {
                $total_domestic_projection += $row->pro_dom_5th;
            }

            $sum_export_domestic = $total_export_projection + $total_domestic_projection;
            if ($sum_export_domestic > 0) {
                $export_percentage = number_format(($total_export_projection / $sum_export_domestic) * 100, 2, '.', ','); // percentage of export
                $domestic_percentage = number_format(($total_domestic_projection / $sum_export_domestic) * 100, 2, '.', ','); // percentage of domestic
            } else {
                $export_percentage = 0;
                $domestic_percentage = 0;
            }

            $total_employee = $alreadyExistApplicant->gr_total_5; // have to consider 5th year only according to new changes
            $foreign_employee = $alreadyExistApplicant->for_total_5; // have to consider 5th year only according to new changes

            $qrCodeGenText = $alreadyExistApplicant->tracking_number . '-' . $alreadyExistApplicant->proposed_name . '-' .
                $economicZones->name . '-' . $dateNow;
            $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
            $url = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";

            $pdf_body = View::make("projectClearance::approval-html", compact('track_no', 'dateNow', 'proposed_name', 'alreadyExistApplicant', 'economicZones', 'business_type', 'app_received_date', 'product_name', 'export_percentage', 'domestic_percentage', 'industry_type', 'production_capacity', 'total_employee', 'foreign_employee', 'organization_type', 'machineries_cost', 'm_state', 'url', 'signature', 'approver'))
                ->render();

            $mpdf->SetCompression(true);
            $mpdf->WriteHTML($pdf_body);
            $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
            $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
            return $pdfFilePath;
        }
    }

    public function search() {
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', 1)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = Status::where('service_id', 1)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('projectClearance::search-view', compact('organization', 'resultList', 'statusList', 'services'));
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

        $valid_track_no = $request->get('valid_applicant_name');
        $economic_zone = $request->get('eco_zone_id');

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $valid_track_no, $economic_zone);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('projectClearance::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();

// Code to count the total number of application for different services as a whole
//        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, pl.eco_zone_id, industry_cat_id
//                                            from service_info si
//                                            left join process_list pl on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id !='-1'
//                                            left join (select industry_cat_id, created_by
//                                                    from project_clearance
//                                                    where industry_cat_id = '$industrial_category'
//                                                     limit 1) pc
//                                             on pl.initiated_by = pc.created_by
//                                            group by si.id;");

        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, 
                                                pl.eco_zone_id, pl.tracking_number, pl.created_by, pl.initiated_by
                                                from service_info si
                                                left join 
                                (
                                select pl.process_id, pl.eco_zone_id, pc.tracking_number, pc.created_by, pl.initiated_by, pl.service_id, pl.status_id  
                                from process_list pl 
                                inner join (
                                                select tracking_number, created_by 
                                                    from project_clearance 
                                                    where tracking_number = '$valid_track_no' 
                                                   ) pc
                                on pl.initiated_by = pc.created_by
                                ) pl 
                                on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id != -1
                                group by si.id;");

        $str = "";
        foreach ($sql as $sql_data) {
            $short_name = $sql_data->short_name;
            $id = $sql_data->id;
            $industry_cat_id = $sql_data->tracking_number;
            $application_number = $sql_data->application_number;
            $eco_zone_id = $sql_data->eco_zone_id;
            $str.="$short_name:$application_number:$industry_cat_id:$eco_zone_id==";
        }
        $data = ['responseCode' => 1, 'data' => $contents, 'totalApps' => $str]; // pattern= Module:AppsCount:Ind:zone
        // End of code to count total number of applications

        return response()->json($data);
    }

    public function discardCertificate($id) {
        $app_id = Encryption::decodeId($id);
        if (Auth::user()->user_type == '1x101') {
            $appInfo = ProjectClearance::find($app_id);
            if ($appInfo->status_id == 23) {
                $appInfo->status_id = 40;
                $appInfo->certificate = '';
                $appInfo->save();

                Processlist::where(['record_id' => $app_id, 'service_id' => 1])
                    ->update(['desk_id' => 0, 'status_id' => 40]);
                Session::flash('success', 'Certificate Discard');
                return redirect()->back();
            } else {
                Session::flash('error', 'Certificate discard is not possible [PC9002]');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'You are not authorized to discard certificate [PC9001]');
            return redirect()->back();
        }
    }

    /*     * *To view associated documents or certificate of the users, takes user_id as input ** */

    public function viewAssociateCertificates($_id) {
        $user_id = Encryption::decodeId($_id);
        $docCertificate = DocCertificate::where('is_archieved', 0)->orderBy('order')->get();

        $assoCertificates = UploadedCertificates::where('is_archieved', 0)->where('created_by', $user_id)->get();
        foreach ($assoCertificates as $documents) {
            $assoCert[$documents->doc_id]['doucument_id'] = $documents->id;
            $assoCert[$documents->doc_id]['file'] = $documents->doc_file;
            $assoCert[$documents->doc_id]['doc_name'] = $documents->doc_name;
        }

        return view("projectClearance::associateDoc.doc-cer", compact('assoCert', 'docCertificate', 'user_id'));
    }

    public function getValidCompanyList(Request $request) {
        $eco_zone_id = $request->get('eco_zone_id');

        $getAppByEcoZone = Processlist::leftJoin('project_clearance as pc', 'pc.id', '=', 'process_list.record_id')
            ->where('process_list.status_id', 23)
            ->where('pc.status_id', 23)
            ->where('process_list.eco_zone_id', $eco_zone_id)
            ->orderBy('pc.applicant_name', 'ASC')
            ->lists('applicant_name', 'tracking_number');

        if (!empty($getAppByEcoZone)) {
            $data = ['responseCode' => 1, 'data' => $getAppByEcoZone];
        } else {
            $data = ['responseCode' => 0, 'data' => ''];
        }
        return response()->json($data);
    }



    /**
     * @This function should be used to generate certificate from pdf server
    in time of processing the application if required.

     * @It will request to pdf server to insert a new record
     */
    public function certificateGenForUpdateBatch($service_id = 0, $app_id = 0, $pdf_type = "", $reg_key = "") {

        $data = array();
        $data['data'] = array(
            'reg_key' => $reg_key, // Secret authentication key
            'pdf_type' => $pdf_type, // letter type
            'ref_id' => $app_id, // app_id
            'param' => array(
                'app_id' => $app_id // app_id
            )
        );

        // Its should be needed for officer signature

        $user_data = UsersModel::where('id', '=', 356)->first();

        // File path URL comes from env url variable
        $signature_url = env('sign_url') . $user_data->signature;
        if (!empty($signature_url)) {
            $signature = ''; //file_get_contents($signature_url);
        } else {
            $signature = 'No signature found';
        }

        $get_pdf_signature = DB::table('pdf_signature_qrcode')->where('app_id', $app_id)->first();
        $qrCodeGenText = "test qr code";
        $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
        $url = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
            echo "\n<br />";
            $response = '';
        } else {
            curl_close($ch);
        }


        if ($get_pdf_signature) {
            // It will use in pdf server for retrieve signature and user info
            DB::table('pdf_signature_qrcode')->where('app_id', $app_id)->update([
                'signature' => $signature,
                'app_id' => $app_id,
                'user_id' => $user_data->id,
                'desk_id' => $user_data->desk_id
            ]);
        } else {
            // It will use in pdf server for retrieve signature and user info
            $pdfSinaQr = new pdfSignatureQrcode();
            $pdfSinaQr->signature = $signature;
            $pdfSinaQr->app_id = $app_id;
            $pdfSinaQr->qr_code = $response;
            $pdfSinaQr->user_id = $user_data->id;
            $pdfSinaQr->desk_id = $user_data->desk_id;
            $pdfSinaQr->save();
        }
        // End of the signature function

        $encode_data = json_encode($data);


        switch ($pdf_type) {

            case 'beza.certificate.local':
            case 'beza.certificate.l':
            case 'beza.pcl.d':
                // Request send to the pdf server
                $this->curlNewRequest($encode_data);
                break;

            case 'beza.undertaking.local':
            case 'beza.undertaking.uat':
            case 'beza.undertaking.dev':
                // Request send to the pdf server
                $this->curlNewRequest($encode_data);
                break;

            default:
        }

        return true; // return true for success
    }

    public function curlNewRequest($data) {
        // CURL request send to pdf server
        $url = env('pdf_server_url')."api/new-job?requestData=$data";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
            echo "\n<br />";
            $response = '';
        } else {
            curl_close($ch);
        }

        if (!is_string($response) || !strlen($response)) {
            echo "Failed to get contents.";
            $response = '';
        }
        // CURL request send to pdf server
        return true;
    }

    /*     * ********************************************End of Controller Class************************************************* */
}
