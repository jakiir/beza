@extends('layouts.admin')

@section('content')

<div class="col-md-12">
<div class="panel panel-primary">
    <div class="panel-heading">
        Associated Documents of the applicant
    </div>
    <div class="panel-body">
        <div id="docTabs" style="margin:10px;">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                  @if( in_array(Auth::user()->desk_id,array(3,4,5,6)) || Auth::user()->id == $user_id)
                        <?php $j = 1; ?>
                       @foreach($docCertificate as $row)
                           <li role="presentation"  class="<?php if($j == 1){ echo 'active'; } ?>">
                               <a href="#doc-cer{{$j}}" data-toggle="tab"> {{$row->doc_name}}</a>
                           </li>
                           <?php $j++; ?>
                       @endforeach
                @endif
                
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                  @if( in_array(Auth::user()->desk_id,array(3,4,5,6)) || $user_id)
                        <?php $j = 1; ?>
                        @foreach($docCertificate as $row)
                                <div role="tabpanel"class="tab-pane <?php if($j == 1){ echo 'active'; }?>" id="doc-cer{{$j}}">
                                    @if(!empty($assoCert[$row->doc_id]['file']))
                                        <h4 style="text-align: left;">{{$assoCert[$row->doc_id]['doc_name']}}</h4>
                                        <?php 
                                        $fileUrl = public_path().'/uploads/'.$assoCert[$row->doc_id]['file'];
                                        if(file_exists($fileUrl)) { 
                                        ?>
                                        <object style="display: block; margin: 0 auto;" width="1000" height="1260" type="application/pdf" 
                                                data="/uploads/<?php echo $assoCert[$row->doc_id]['file'] 
                                                        ?>#toolbar=1&amp;navpanes=0&amp;scrollbar=1&amp;page=1&amp;view=FitH"></object>
                                        <?php } else { ?>
                                            <div class="">No such file is existed!</div>
                                        <?php } ?> {{-- checking file is existed --}}

                                   @else
                                        <div class="">No file found!</div>
                                    @endif
                                </div>
                            <?php $j++; ?>
                        @endforeach
                @endif
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection