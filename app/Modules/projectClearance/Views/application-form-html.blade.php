@extends('layouts.pdfGen')
@section('content')
<div class="row">
    <section id="projectClearanceForm" class="content">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <img src="assets/images/logo_beza_single.png"/><br/>
                            BEZA::Bangladesh Economic Zones Authority<br/>
                            Application for Project Clearance
                        </div>
                    </div>
                    <div id="inputForm" class="panel panel-red">

                        <div class="panel-heading">

                        </div>
                        <div class="panel-body">


                            <table width="100%">
                                <tr>
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Tracking no. : </strong><span style="font-size: 10px;">{{ $process_data->track_no  }}</span></td>
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Date of Submission: </strong> <span style="font-size: 10px;">{{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }}</span></td>
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Status : </strong> <span style="font-size: 10px;">@if(isset($form_data) && $form_data->status_id == -1) Draft
                                            @else {!! isset($form_data->status_id) ? $statusArray[$form_data->status_id] : '' !!}
                                            @endif</span></td>
                                    @if($process_data->desk_id != 0)
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Desk :</strong> <span style="font-size: 10px;">{{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}</span></td>
                                    @else
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Desk :</strong> <span style="font-size: 10px;">Applicant</span></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td style="padding:5px; font-size:10px">
                                        <?php if (isset($form_data) && $form_data->status_id == 23 && isset($form_data->certificate)) { ?>
                                            <a href="{{ url($form_data->certificate) }}"
                                               title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png"
                                                alt="Download Certificate" /> <b>Download Certificate</b></a>
                                            <?php } ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            @if(isset($alreadyExistApplicant->challan_no) && isset($alreadyExistApplicant->bank_name) && isset($alreadyExistApplicant->challan_amount))
                            <div id="ep_form" class="panel panel-primary">
                                <div class="panel-heading">Pay order related information</div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Pay Order No : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->challan_no) ? $alreadyExistApplicant->challan_no : '') }}</span></td>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Date : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->challan_date) ? $alreadyExistApplicant->challan_date : '') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Bank Name : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->bank_name) ? $banks[$alreadyExistApplicant->bank_name] : '') }}</span></td>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Branch Name : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->challan_branch) ? $alreadyExistApplicant->challan_branch : '') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Amount : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->challan_amount) ? $alreadyExistApplicant->challan_amount : '') }}</span></td>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Pay order copy : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">
                                                    <a href="{{url($alreadyExistApplicant->challan_file)}}">
                                                        <img width="10" height="10" src="assets/images/pdf.png" alt="Pay Order Copy" />
                                                        Download</a></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            @else
                            {!! '' !!}
                            @endif
                            <fieldset style="display: block;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading margin-for-preview"><strong>1. Applicant Information</strong></div>
                                    <div class="panel-body">
                                        <table width="100%">
                                            <tr>
                                                <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Applying firm or Company :</strong> </td>
                                                <td width="25%" style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : 'N/A') }}</span></td>
                                                <td width="25%"></td>
                                                <td width="25%"></td>
                                            </tr>
                                        </table>

                                        <table width="100%">
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Full Address of Registered Head Office of Applicant / Applying Firm or Company :</strong></td>
                                            </tr>
                                        </table>

                                        <table width="100%">
                                            <tr>
                                                <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Country :</strong> </td>
                                                <td width="25%" style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->country) ? $countries[$alreadyExistApplicant->country] : 'N/A') }}</span></td>
                                                <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Division :</strong> </td>
                                                <td width="25%" style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->division) && $alreadyExistApplicant->division != 0)? 
                                                    $divition_eng[$alreadyExistApplicant->division] : 'N/A' }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">District :</strong> </td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($districtName->area_nm) ? $districtName->area_nm : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Address Line 1 :</strong> </td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no :'N/A') }}</span></td>
                                            </tr>

                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Address Line 2 :</strong> </td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Post Code :</strong> </td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : 'N/A') }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Phone No : </strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Fax No : </strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : 'N/A') }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Email : </strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Website : </strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : 'N/A') }}</span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <strong style="display: inline-block;width:60%;">2. Authorized Information</strong>
                                        <span style="width: 38%; display: none;" id="full_same_as_authorized" class="text-right">
                                            <input type="checkbox" value="Yes" name="same_as_authorized" checked="checked" id="same_as_authorized" onclick="editAuthorizeInfo(this)" class="text-left">
                                            <label style="font-size:13px;" class="text-left" for="same_as_authorized">Same as Authorized Person</label>
                                        </span>

                                    </div>
                                    <div class="panel-body">
                                        <table width="100%">
                                            <tr>
                                                <?php $authCorrespondent_name = (isset($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : 'N/A'); ?>
                                                <td width="35%" style="padding:5px;"><strong style="font-size:10px;">Name of the Correspondent Applicant Name :</strong></td>
                                                <td width="65%" style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name) }}</span></td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Nationality :</strong></td>
                                                <td width="25%" style="padding:5px;">
                                                    <span style="font-size:10px;">
                                                        <?php $authNationality = (isset($logged_user_info->nationality) && $logged_user_info->nationality != '') ? $logged_user_info->nationality : 'N/A'; ?>
                                                        {{ (isset($alreadyExistApplicant->correspondent_nationality)  && $alreadyExistApplicant->correspondent_nationality != '') 
                                                            ? $nationality[$alreadyExistApplicant->correspondent_nationality] : $authNationality }}
                                                    </span>
                                                </td>
                                                <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Passport : </strong></td>
                                                <td width="25%" style="padding:5px;">
                                                    <span style="font-size:10px;">
                                                        <?php $authPassport_no = (isset($logged_user_info->passport_no) ? $logged_user_info->passport_no : 'N/A'); ?>
                                                        {{ (isset($alreadyExistApplicant->correspondent_passport) ? $alreadyExistApplicant->correspondent_passport : $authPassport_no) }}
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Correspondent Address & Contact Details :</strong></td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td width="25%" style="padding: 5px;"><strong style="font-size:10px;">Country : </strong></td>
                                                <td width="25%" style="padding:5px;">
                                                    <span style="font-size:10px;">
                                                        <?php $authCountry = (isset($logged_user_info->country) ? $logged_user_info->country : 'N/A'); ?>
                                                        {{ (isset($alreadyExistApplicant->correspondent_country) ? $countries[$alreadyExistApplicant->correspondent_country] : $authCountry) }}
                                                    </span>
                                                </td>
                                                <td width="25%" style="padding:5px;"><strong style="font-size:10px;">State : </strong></td>
                                                <?php $authState = (isset($logged_user_info->state) ? $logged_user_info->state : 'N/A'); ?>
                                                <td width="25%" style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_state) ? $alreadyExistApplicant->correspondent_state : $authState) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Province : </strong></td>
                                                <?php $authProvince = (isset($logged_user_info->province) ? $logged_user_info->province : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince) }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Address Line 1 : </strong></td>
                                                <?php $authRoad_no = (isset($logged_user_info->road_no) ? $logged_user_info->road_no : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_road_no) ? $alreadyExistApplicant->correspondent_road_no : $authRoad_no) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Address Line 2 : </strong></td>
                                                <?php $authHouse_no = (isset($logged_user_info->house_no) ? $logged_user_info->house_no : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_house_no) ? $alreadyExistApplicant->correspondent_house_no :
                                                            $authHouse_no) }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Post Code : </strong></td>
                                                <?php $authPost_code = (isset($logged_user_info->post_code) ? $logged_user_info->post_code : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_post_code) ? $alreadyExistApplicant->correspondent_post_code : $authPost_code) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Phone No : </strong></td>
                                                <?php $authUser_phone = (isset($logged_user_info->user_phone) ? $logged_user_info->user_phone : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_phone) ?
                                                            $alreadyExistApplicant->correspondent_phone : $authUser_phone) }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Fax No : </strong></td>
                                                <?php $authUser_fax = (isset($logged_user_info->user_fax) ? $logged_user_info->user_fax : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Email : </strong></td>
                                                <?php $authUser_email = (isset($logged_user_info->user_email) ? $logged_user_info->user_email : 'N/A'); ?>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email) }}</span></td>
                                                <td style="padding:5px;"></td>
                                                <td style="padding:5px;"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset style="display: block;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>3. Proposed Project</strong></div>
                                    <div class="panel-body">
                                        <table width="100%">
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Type of Economic Zone where business to be set :</strong></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Economic Zone Name :</strong></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->zone_type) ? $alreadyExistApplicant->zone_type : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($economicZone[$alreadyExistApplicant->eco_zone_id]) ? $economicZone[$alreadyExistApplicant->eco_zone_id] : 'N/A') }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Proposed Project / Company Name which will carry out the Business :</strong></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Company Logo :</strong></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->proposed_name) ? $alreadyExistApplicant->proposed_name : 'N/A') }}</span></td>
                                                <td style="padding:5px;" rowspan="3">
                                                    <img style="background-color: gray;" width="80" height="80" src="{{ (isset($alreadyExistApplicant->company_logo)? $alreadyExistApplicant->company_logo : 'assets/images/company_logo.png')  }}" alt="Company Logo" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Type of Business / Industry or Services :</strong></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->business_type) ? $alreadyExistApplicant->business_type : 'N/A') }}</span></td>
                                            </tr>

                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Type of Industry :</strong></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Type of Organization :</strong></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->industry_type) ?
                                                $alreadyExistApplicant->industry_type : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->organization_type) ?
                                                $alreadyExistApplicant->organization_type : 'N/A') }}</span></td>
                                            </tr>

                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;"></strong></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Industry Category : </strong></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;"></span></td>
                                                <td style="padding:5px;">
                                                    <span style="font-size:10px;">
                                                        {{ (isset($industryCatInfo->industry_name) ? $industryCatInfo->industry_name : 'N/A') }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"></td>
                                                <td style="padding:5px;">
                                                    <table width="100%" >
                                                        <tr>
                                                            <td>
                                                                <strong style="font-size:10px;">
                                                                    {{ (isset($industryCatInfo->color_name) ? $industryCatInfo->color_name : '') }}
                                                                </strong>
                                                            </td>
                                                            <td>
                                                                <?php (isset($industryCatInfo->color_code) ? $color = $industryCatInfo->color_code : $color = '#FFFFFF') ?>
                                                                <strong style="background-color: {{ $color }}; color:{{ $color }}; width: 150px; height: 50px;">
                                                                    Colors Codes
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>




                                        </table>
                                        <div style="<?php
                                        if (isset($alreadyExistApplicant->organization_type) && $alreadyExistApplicant->organization_type != 'Joint Venture') {
                                            echo 'display:none;';
                                        }
                                        ?>">
                                            @if($jointOrganizations)
                                            <table class="table-bordered" width="100%">
                                                <tr class="alert alert-success">
                                                    <td style="font-size:10px; padding:5px;"><strong>Company Name</strong></td>
                                                    <td style="font-size:10px; padding:5px;"><strong>Company Address</strong></td>
                                                    <td style="font-size:10px; padding:5px;"><strong>Country</strong></td>
                                                </tr>
                                                <?php $inc = 0; ?>
                                                @foreach($jointOrganizations as $eachJointOrg)
                                                <tr>
                                                    <td style="font-size:10px; padding:5px;"><span>{{ (isset($eachJointOrg->joint_company)?$eachJointOrg->joint_company:'N/A') }}</span></td>
                                                    <td style="font-size:10px; padding:5px;"><span> {{ (isset($eachJointOrg->joint_company_address)?$eachJointOrg->joint_company_address:'N/A') }}</span></td>
                                                    <td style="font-size:10px; padding:5px;"><span>{{ (isset($countries[$eachJointOrg->joint_com_country])?$countries[$eachJointOrg->joint_com_country]:'N/A') }}</span></td>
                                                </tr>
                                                <?php $inc++; ?>
                                                @endforeach
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>4. Construction Schedule</strong></div>
                                    <div class="panel-body">
                                        <table width="100%">
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Start Time : </strong></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">End Time : </strong></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Duration (in days) : </strong></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->construction_start) ? App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->construction_start, 0, 10)) : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->construction_end) ? App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->construction_end, 0, 10)) : 'N/A') }}</span></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->construction_duration) ? $alreadyExistApplicant->construction_duration : 0) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Commercial Operation Date (COD) :</strong></td>
                                                <td style="padding:5px;"></td>
                                                <td style="padding:5px;"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->cod_date) ?
                                                                App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->cod_date, 0, 10)) : '') }}</span></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->import_cost) ? $alreadyExistApplicant->import_cost : '') }}</span></td>
                                                <td style="padding:5px;"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br/>
                                <div class="panel panel-primary" style="page-break-inside:avoid !important;">
                                    <div class="panel-heading"><strong>5. Carry of the Business (BDT) </strong></div>
                                    <div class="panel-body">

                                        <div style="clear:both" class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="">
                                                            <thead class="alert alert-info">
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;">Capital Structure </th>
                                                                    <th style="padding:5px; font-size:10px;">Total </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;">Authorized Capital :</th>
                                                                    <td style="padding:5px; font-size:10px;"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->auth_capital_to) && isset($alreadyExistApplicant->auth_capital_to) && is_numeric($alreadyExistApplicant->auth_capital_to) ? number_format($alreadyExistApplicant->auth_capital_to) : 0) }}</span>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;">Paid-up Capital : </th>
                                                                    <td style="padding:5px; font-size:10px;"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->paid_capital_to) && isset($alreadyExistApplicant->paid_capital_to) && is_numeric($alreadyExistApplicant->paid_capital_to) ? number_format($alreadyExistApplicant->paid_capital_to) : 0) }}</span>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;">Borrowing / Loan : </th>
                                                                    <td style="padding:5px; font-size:10px;"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->ext_borrow_to) && isset($alreadyExistApplicant->ext_borrow_to) && is_numeric($alreadyExistApplicant->ext_borrow_to) ? number_format($alreadyExistApplicant->ext_borrow_to) : 0) }}</span>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>6. Contribution in Paid-up Capital Among Shareholders</strong></div>
                                    <div class="panel-body">

                                        <div style="clear:both" class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="">
                                                            <thead class="alert alert-warning">
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;">Name of Shareholder </th>
                                                                    <th style="padding:5px; font-size:10px;">Local Share</th>
                                                                    <th style="padding:5px; font-size:10px;">Foreign Share </th>
                                                                    <th style="padding:5px; font-size:10px;">Total Share  </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;">Paid-up Capital (%)</th>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->paid_cap_amount) ? $alreadyExistApplicant->paid_cap_amount : 'N/A') }}</span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->paid_cap_nature) ? $alreadyExistApplicant->paid_cap_nature : 'N/A') }}</span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                            {{ (isset($alreadyExistApplicant->paid_cap_percentage) ? $alreadyExistApplicant->paid_cap_percentage : 'N/A') }}</span>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>7.Sponsor of Shareholder</strong></div>
                                    <div class="panel-body">
                                        <div style="clear:both" class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="directors_list">
                                                            <thead class="alert alert-success">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:10px;">Name </th>
                                                                    <th style="padding:5px; font-size:10px;">Address </th>
                                                                    <th style="padding:5px; font-size:10px;">Nationality</th>
                                                                    <th style="padding:5px; font-size:10px;">Status in the proposed company </th>
                                                                    <th style="padding:5px; font-size:10px;">Extent of share Holding (%) </th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>
                                                                @if(count($sponsorDirections) > 0)
                                                                <?php $inc = 0; ?>
                                                                @foreach($sponsorDirections as $eachSponsor)
                                                                <tr id="templateRow {{$inc}}">
                                                                    <td style="padding:5px; font-size:10px">

                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachSponsor->sponsor_name) ? $eachSponsor->sponsor_name : 'N/A') }}</span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachSponsor->sponsor_address) ? $eachSponsor->sponsor_address : 'N/A') }}</span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">

                                                                        <span style="width:100%;padding:6px; display:block;">
                                                                            {{ (isset($eachSponsor->sponsor_nationality) && $eachSponsor->sponsor_nationality != '')?
                                                                                $nationality[$eachSponsor->sponsor_nationality]: 'N/A' }}
                                                                        </span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachSponsor->sponsor_status)? $eachSponsor->sponsor_status : 'N/A') }}</span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachSponsor->sponsor_share_ext)? $eachSponsor->sponsor_share_ext : 'N/A') }}</span>

                                                                    </td>
                                                                </tr>

                                                                <?php $inc++; ?>
                                                                @endforeach
                                                                @else
                                                                <tr id="templateRow0">
                                                                    <td style="padding:5px; font-size:10px">

                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;padding:6px; display:block;"></span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">

                                                                    </td>
                                                                </tr>
                                                                @endif
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>8. Area of Land / SFB to be allotted</strong></div>
                                    <div class="panel-body">
                                        <div style="clear:both" class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="land_sfb_tbl">
                                                            <thead class="alert alert-info">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:10px;">Agreed land by developers (square meter) </th>
                                                                    <th style="padding:5px; font-size:10px;">Plot Address </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr id="land_sfb_tbl_row">
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->agreed_land) ? $alreadyExistApplicant->agreed_land : 'N/A') }}</span>

                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->sfb_plot_address) ? $alreadyExistApplicant->sfb_plot_address : 'N/A') }}</span>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--/panel-body-->
                                </div><!--/panel-->

                            </fieldset>


                            <fieldset style="display: block;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>9. Service/Products</strong></div>
                                    <div class="panel-body">
                                        <table width="100%">
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">a) Name / description of the product(s) : </strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->product_name) ? $alreadyExistApplicant->product_name : 'N/A') }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">b) Usage of the product(s) :</strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->product_usage) ? $alreadyExistApplicant->product_usage : 'N/A') }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">c) Manufacturing process :</strong></td>
                                                <td style="padding:5px;"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->manufacture_process) ? $alreadyExistApplicant->manufacture_process : 'N/A') }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">d) Cost of the project (in US$) :</strong></td>
                                                <td style="padding:5px;"><span class="onlyNumber" style="font-size:10px;">{{ (isset($alreadyExistApplicant->project_cost) ? $alreadyExistApplicant->project_cost : 'N/A') }}</span></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--/panel-body-->
                                </div><!--/panel-->

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>10. Production Programme</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="productionPrgTbl">
                                                        <thead class="alert alert-success">
                                                            <tr class="text-center">
                                                                <th style="padding:5px; font-size:10px;" class="valign-middle text-center"> Description</th>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center"> Unit </th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">1st Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">2nd Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">3rd Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">4th Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">5th Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center"> Total </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count($clr_production) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_production as $eachProduction)

                                                            <tr id="rowProductionCount {{ $inc }}">
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachProduction->production_desc)? $eachProduction->production_desc : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <?php $program_unit = (isset($units[$eachProduction->production_unit])) ? $units[$eachProduction->production_unit] : 'N/A'; ?>
                                                                    <span style="width:100%;padding:6px; display:block;">{{ (($eachProduction->production_unit != null)? $program_unit : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ ( isset($eachProduction->production_1st) ? $eachProduction->production_1st : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ ( isset($eachProduction->production_2nd) ?  $eachProduction->production_2nd : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ ( isset($eachProduction->production_3rd) ? $eachProduction->production_3rd : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ ( isset($eachProduction->production_4th) ? $eachProduction->production_4th : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ ( isset($eachProduction->production_5th) ? $eachProduction->production_5th : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;">{{ ( isset($eachProduction->production_total) && is_numeric($eachProduction->production_total) ? number_format($eachProduction->production_total,2) : 0) }}</span>
                                                                </td>
                                                            </tr>
                                                            <?php $inc++; ?>
                                                            @endforeach
                                                            @else
                                                            <tr id="rowProductionCount">
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;padding:6px; display:block;">{{ $units }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:none"></span>
                                                                </td>
                                                            </tr>
                                                            @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>11. Projection of Export</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="proExportTbl">
                                                        <thead class="alert alert-info">
                                                            <tr>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center">Description </th>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center">Unit</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">1st Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">2nd Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">3rd Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">4th Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">5th Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center">Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count($clr_export) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_export as $eachExport)
                                                            <tr id="rowProExportCount {{ $inc }}">
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachExport->pro_ext_desc)? $eachExport->pro_ext_desc : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <?php $export_unit = (isset($units[$eachExport->pro_ext_unit]) ? $units[$eachExport->pro_ext_unit] : 'N/A'); ?>
                                                                    <span style="width:100%;padding:6px; display:block;">{{ (($eachExport->pro_ext_unit = !null)? $export_unit : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachExport->pro_ext_1st) ? $eachExport->pro_ext_1st : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachExport->pro_ext_2nd) ? $eachExport->pro_ext_2nd : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachExport->pro_ext_3rd) ? $eachExport->pro_ext_3rd : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachExport->pro_ext_4th) ? $eachExport->pro_ext_4th : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachExport->pro_ext_5th) ? $eachExport->pro_ext_5th : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber"  style="width:100%;display:block;margin:2px;padding:6px;">{{ (isset($eachExport->pro_ext_total) && is_numeric($eachExport->pro_ext_total) ? number_format($eachExport->pro_ext_total,2) : 0) }}</span>
                                                                </td>
                                                            </tr>
                                                            <?php $inc++ ?>

                                                            @endforeach
                                                            @else
                                                            <tr id="rowProExportCount">
                                                                <td style="padding:5px; font-size:10px">

                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;padding:6px; display:block;">{{ $units }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>

                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:none"></span>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>12. Projection of Domestic</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="proDomesticTbl">
                                                        <thead class="alert alert-warning">
                                                            <tr>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center">Description </th>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center"> Unit </th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">1st Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">2nd Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">3rd Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">4th Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="text-center">5th Year  <br>Qty</th>
                                                                <th style="padding:5px; font-size:10px;" class="valigh-middle text-center"> Total </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count($clr_domestic_export) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_domestic_export as $eachDomEx)
                                                            <tr id="rowProDomesticCount {{ $inc }}">
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachDomEx->pro_dom_desc)? $eachDomEx->pro_dom_desc : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <?php $domestic_unit = (isset($units[$eachDomEx->pro_dom_unit])) ? $units[$eachDomEx->pro_dom_unit] : 'N/A'; ?>
                                                                    <span style="width:100%;padding:6px; display:block;">{{ (($eachDomEx->pro_dom_unit = !null)? $domestic_unit : 'N/A') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachDomEx->pro_dom_1st) ? $eachDomEx->pro_dom_1st : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachDomEx->pro_dom_2nd) ? $eachDomEx->pro_dom_2nd : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachDomEx->pro_dom_3rd) ? $eachDomEx->pro_dom_3rd : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachDomEx->pro_dom_4th) ? $eachDomEx->pro_dom_4th : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachDomEx->pro_dom_5th) ? $eachDomEx->pro_dom_5th : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;">{{ (isset($eachDomEx->pro_dom_total) && is_numeric($eachDomEx->pro_dom_total)  ? number_format($eachDomEx->pro_dom_total,2) : 0) }}</span>
                                                                </td>
                                                            </tr>
                                                            <?php $inc++; ?>
                                                            @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>13. Addition of Machinery  </strong></div>
                                    <div class="panel-body">
                                        <div style="clear:both" class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="machinaryTbl">
                                                            <thead class="alert alert-success">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:10px;" class="text-center">Details of Machinery </th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center">Country of Origin </th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center">Name of the Vendor</th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center">Value</th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center">State </th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center">If old, how old? (Year) </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(count($clr_machinary) > 0)
                                                                <?php $inc = 0; ?>
                                                                @foreach($clr_machinary as $machine)
                                                                <tr id="rowMachineCount {{ $inc }}">
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;">{{ (isset($machine->m_details) ? $machine->m_details : 'N/A') }}</span>
                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;padding:6px;display:block;">{{ (isset($machine->m_country) ? $countries[$machine->m_country] : 'N/A') }}</span>
                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span style="width:100%;display:block;margin:2px;padding:6px;">{{ (isset($machine->m_vendor)? $machine->m_vendor : 'N/A' ) }}</span>
                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span class="onlyNumber" style="width:100%;display:inline-block;margin:2px;padding:6px;">{{ (isset($machine->m_value) && is_numeric($machine->m_value) ? number_format($machine->m_value, 2, '.', '') : 0) }}</span>
                                                                        <span class="" style="width:100%;display:inline-block;margin:2px;padding:6px;">{{ (isset($currencie_arr[$machine->m_currency]->code) ? $currencie_arr[$machine->m_currency]->code : 'N/A') }}</span>
                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px"><span style="width:100%;padding:6px; display:block;">{{ (isset($machine->m_purchase_state) ? $machine->m_purchase_state : 'N/A') }}</span>
                                                                    </td>
                                                                    <td style="padding:5px; font-size:10px">
                                                                        <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;">{{ (isset($machine->m_age) ? $machine->m_age : 'N/A') }}</span>
                                                                    </td>
                                                                </tr>
                                                                <?php $inc++; ?>
                                                                @endforeach
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>14. Manpower requirements</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="mpReqTbl">
                                                        <thead class="alert alert-success">
                                                            <tr>
                                                                <th width="8%" style="padding:5px; font-size:18px;" valign="top" class="text-center valigh-middle">Year </th>
                                                                <th style="padding:5px; font-size:18px;" valign="top" class="text-center valigh-top" colspan="4">
                                                        <table class="table table-striped table-bordered dt-responsive">
                                                            <thead class="alert alert-success">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:18px;" valign="top" class="text-center valigh-top" colspan="4">Foreign</th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:18px; width: 130px;" class="text-center valigh-top">Managerial </th>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Skilled </th>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Unskilled </th>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Total </th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                        </th>
                                                        <th style="padding:5px; font-size:18px;" valign="top" class="text-center valigh-top" colspan="4">
                                                        <table class="table table-striped table-bordered dt-responsive">
                                                            <thead class="alert alert-success">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:18px;" valign="top" class="text-center valigh-top" colspan="4">Local</th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Managerial </th>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Skilled </th>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Unskilled </th>
                                                                    <th style="padding:5px; font-size:18px; width: 141px;" class="text-center valigh-top">Total </th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                        </th>
                                                        <th style="padding:5px; font-size:18px;" valign="top" class="text-center valigh-top" width="15%">Grand Total </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span style="width:100%;padding:6px; display:block;">{{ (isset($alreadyExistApplicant->mp_year_1) ? $alreadyExistApplicant->mp_year_1 : '') }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_man_1) && is_numeric($alreadyExistApplicant->for_man_1) ? number_format($alreadyExistApplicant->for_man_1) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_skill_1) && is_numeric($alreadyExistApplicant->for_skill_1) ? number_format($alreadyExistApplicant->for_skill_1) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_unskill_1) && is_numeric($alreadyExistApplicant->for_unskill_1) ? number_format($alreadyExistApplicant->for_unskill_1): 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_total_1) && is_numeric($alreadyExistApplicant->for_total_1) ? number_format($alreadyExistApplicant->for_total_1) : 0) }}</span>

                                                                </td>

                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_man_1) && is_numeric($alreadyExistApplicant->loc_man_1) ? number_format($alreadyExistApplicant->loc_man_1) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_skill_1) && is_numeric($alreadyExistApplicant->loc_skill_1) ? number_format($alreadyExistApplicant->loc_skill_1) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_unskill_1) && is_numeric($alreadyExistApplicant->loc_unskill_1) ? number_format($alreadyExistApplicant->loc_unskill_1) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_total_1) && is_numeric($alreadyExistApplicant->loc_total_1) ? number_format($alreadyExistApplicant->loc_total_1) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_total_1) && is_numeric($alreadyExistApplicant->loc_total_1) ? number_format($alreadyExistApplicant->gr_total_1) : 0) }}</span>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span style="width:100%;padding:6px; display:block;">{{ (isset($alreadyExistApplicant->mp_year_2) ? $alreadyExistApplicant->mp_year_2 : '') }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_man_2) && is_numeric($alreadyExistApplicant->for_man_2) ? number_format($alreadyExistApplicant->for_man_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_skill_2) && is_numeric($alreadyExistApplicant->for_skill_2) ? number_format($alreadyExistApplicant->for_skill_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_unskill_2) && is_numeric($alreadyExistApplicant->for_unskill_2) ? number_format($alreadyExistApplicant->for_unskill_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->for_total_2) && is_numeric($alreadyExistApplicant->for_total_2) ? number_format($alreadyExistApplicant->for_total_2) : 0) }}</span>

                                                                </td>

                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_man_2) && is_numeric($alreadyExistApplicant->loc_man_2) ? number_format($alreadyExistApplicant->loc_man_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_skill_2) && is_numeric($alreadyExistApplicant->loc_skill_2) ? number_format($alreadyExistApplicant->loc_skill_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_unskill_2) && is_numeric($alreadyExistApplicant->loc_unskill_2) ? number_format($alreadyExistApplicant->loc_unskill_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->loc_total_2) && is_numeric($alreadyExistApplicant->loc_total_2) ? number_format($alreadyExistApplicant->loc_total_2) : 0) }}</span>

                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->gr_total_2) && is_numeric($alreadyExistApplicant->gr_total_2) ? number_format($alreadyExistApplicant->gr_total_2) : 0) }}</span>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span style="width:100%;padding:6px;display:block;">
                                                                        {{ (isset($alreadyExistApplicant->mp_year_3) ? $alreadyExistApplicant->mp_year_3 : '') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_man_3) && is_numeric($alreadyExistApplicant->for_man_3) ? 
                                                                            number_format($alreadyExistApplicant->for_man_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_skill_3) && is_numeric($alreadyExistApplicant->for_skill_3) ? 
                                                                            number_format($alreadyExistApplicant->for_skill_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_unskill_3) && is_numeric($alreadyExistApplicant->for_unskill_3) ? 
                                                                            number_format($alreadyExistApplicant->for_unskill_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_total_3) && is_numeric($alreadyExistApplicant->for_total_3) ? 
                                                                            number_format($alreadyExistApplicant->for_total_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_man_3) && is_numeric($alreadyExistApplicant->loc_man_3) ? 
                                                                            number_format($alreadyExistApplicant->loc_man_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_skill_3) && is_numeric($alreadyExistApplicant->loc_skill_3) ?
                                                                            number_format($alreadyExistApplicant->loc_skill_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_unskill_3) && is_numeric($alreadyExistApplicant->loc_unskill_3) ?
                                                                            number_format($alreadyExistApplicant->loc_unskill_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_total_3) && is_numeric($alreadyExistApplicant->loc_total_3) ?
                                                                            number_format($alreadyExistApplicant->loc_total_3) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->gr_total_3) && is_numeric($alreadyExistApplicant->gr_total_3) ?
                                                                            number_format($alreadyExistApplicant->gr_total_3) : 0) }}</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span style="width:100%;padding:6px;display:block;">
                                                                        {{ (isset($alreadyExistApplicant->mp_year_4) ? $alreadyExistApplicant->mp_year_4 : '') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_man_4) && is_numeric($alreadyExistApplicant->for_man_4) ? number_format($alreadyExistApplicant->for_man_4) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_skill_4) && is_numeric($alreadyExistApplicant->for_skill_4) ? number_format($alreadyExistApplicant->for_skill_4) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_unskill_4) && is_numeric($alreadyExistApplicant->for_unskill_4) ? number_format($alreadyExistApplicant->for_unskill_4) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_total_4) && is_numeric($alreadyExistApplicant->for_total_4) ? number_format($alreadyExistApplicant->for_total_4) : 0) }}</span>
                                                                </td>

                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_man_4) && is_numeric($alreadyExistApplicant->loc_man_4) ? number_format($alreadyExistApplicant->loc_man_4) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_skill_4) && is_numeric($alreadyExistApplicant->loc_skill_4) ? number_format($alreadyExistApplicant->loc_skill_4) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_unskill_4) && is_numeric($alreadyExistApplicant->loc_unskill_4) ? number_format($alreadyExistApplicant->loc_unskill_4) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_total_4) && is_numeric($alreadyExistApplicant->loc_total_4) ? number_format($alreadyExistApplicant->loc_total_4 ): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->gr_total_4) && is_numeric($alreadyExistApplicant->gr_total_4) ? number_format($alreadyExistApplicant->gr_total_4): 0) }}</span>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span style="width:100%;padding:6px; display:block;">
                                                                        {{ (isset($alreadyExistApplicant->mp_year_5) ? $alreadyExistApplicant->mp_year_5 : '') }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_man_5) && is_numeric($alreadyExistApplicant->for_man_5) ? 
                                                                            number_format($alreadyExistApplicant->for_man_5 ): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_skill_5) && is_numeric($alreadyExistApplicant->for_skill_5) ? 
                                                                            number_format($alreadyExistApplicant->for_skill_5 ): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_unskill_5) && is_numeric($alreadyExistApplicant->for_unskill_5) ? 
                                                                            number_format($alreadyExistApplicant->for_unskill_5 ): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->for_total_5) && is_numeric($alreadyExistApplicant->for_total_5) ?
                                                                            number_format($alreadyExistApplicant->for_total_5 ): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_man_5) && is_numeric($alreadyExistApplicant->loc_man_5) ? 
                                                                            number_format($alreadyExistApplicant->loc_man_5) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_skill_5) && is_numeric($alreadyExistApplicant->loc_skill_5) ? 
                                                                            number_format($alreadyExistApplicant->loc_skill_5 ): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_unskill_5) && is_numeric($alreadyExistApplicant->loc_unskill_5) ? 
                                                                            number_format($alreadyExistApplicant->loc_unskill_5): 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->loc_total_5) && is_numeric($alreadyExistApplicant->loc_total_5) ? 
                                                                            number_format($alreadyExistApplicant->loc_total_5) : 0) }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:16px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">
                                                                        {{ (isset($alreadyExistApplicant->gr_total_5) && is_numeric($alreadyExistApplicant->gr_total_5) ? 
                                                                            number_format($alreadyExistApplicant->gr_total_5) : 0) }}</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>15. Cost of Production </strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="productionCostTbl">
                                                        <thead class="alert alert-info">
                                                            <tr>
                                                                <th style="padding:5px; font-size:10px;" valign="top" class="text-center valigh-middle">Total production cost per unit (in US$)
                                                                    <br>
                                                                </th>
                                                                <th valign="top" class="text-center valigh-top" colspan="2">
                                                        <table class="table table-striped table-bordered dt-responsive">
                                                            <thead class="alert alert-info">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:10px;" valign="top" class="text-center valigh-top" colspan="2">Raw Materials per annum (in US$)</th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center valigh-top">From Bangladesh </th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center valigh-top">From other countries </th>
                                                                </tr></thead>
                                                        </table>
                                                        </th>
                                                        <th style="padding:5px; font-size:10px;" valign="top" class="text-center valigh-top" colspan="2">
                                                        <table class="table table-striped table-bordered dt-responsive">
                                                            <thead class="alert alert-info">
                                                                <tr class="text-center">
                                                                    <th style="padding:5px; font-size:10px;" valign="top" class="text-center valigh-top" colspan="2">Packaging Materials per annum (in US$)</th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center valigh-top">From Bangladesh </th>
                                                                    <th style="padding:5px; font-size:10px;" class="text-center valigh-top">From other countries </th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                        </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count($clr_product_cost) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_product_cost as $eachProCost)
                                                            <tr id="rowProCostCount{{ $inc }}">
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachProCost->production_cost) && is_numeric($eachProCost->production_cost))? number_format($eachProCost->production_cost,2): 0 }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachProCost->raw_cost_bd) && is_numeric($eachProCost->raw_cost_bd))? number_format($eachProCost->raw_cost_bd,2): 0}}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachProCost->raw_cost_other) && is_numeric($eachProCost->raw_cost_other))? number_format($eachProCost->raw_cost_other,2): 0 }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachProCost->pac_cost_bd) && is_numeric($eachProCost->pac_cost_bd))? number_format($eachProCost->pac_cost_bd,2): 0 }}</span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($eachProCost->pac_cost_other) && is_numeric($eachProCost->pac_cost_other))? number_format($eachProCost->pac_cost_other,2 ): 0 }}</span>
                                                                </td>
                                                            </tr>
                                                            <?php $inc++; ?>
                                                            @endforeach
                                                            @else
                                                            <tr id="rowProCostCount{{ $inc }}">
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px">
                                                                    <span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px"><span style="width:100%;display:block;margin:2px;padding:6px;display:block"></span>
                                                                </td>
                                                                <td style="padding:5px; font-size:10px"></td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>16. Sales Revenue (at maximum capacity)</strong></div>
                                    <div class="panel-body">
                                        <table width="100%">
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Domestic : </strong></td>
                                                <td style="padding:5px;"><span class="onlyNumber" style="font-size:10px;">US$ {{ (isset($alreadyExistApplicant->sales_domestic) ? $alreadyExistApplicant->sales_domestic : 0) }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Export : </strong></td>
                                                <td style="padding:5px;"><span class="onlyNumber" style="font-size:10px;">US$ {{ (isset($alreadyExistApplicant->sales_export) ? $alreadyExistApplicant->sales_export : 0) }}</span></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;"><strong style="font-size:10px;">Sales to Export Oriented :</strong></td>
                                                <td style="padding:5px;"><span class="onlyNumber" style="font-size:10px;">US$ {{ (isset($alreadyExistApplicant->sales_exp_oriented) ? $alreadyExistApplicant->sales_exp_oriented: 0) }}</span></td>
                                                <td style="padding:5px;"><strong style="font-size:10px; vertical-align: text-top !important;">Total : </strong></td>
                                                <td style="padding:5px;"><span class="onlyNumber" style="font-size:10px;">US$ {{ (isset($alreadyExistApplicant->sales_total) ?
                                                    $alreadyExistApplicant->sales_total : 0) }}</span><br/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div><!--/panel-->

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>17. Required Infrastructure</strong></div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table width="100%" cellspacing="0" class="table table-striped table-bordered dt-responsive" id="infraReqTbl">
                                                <thead class="alert alert-info">
                                                    <tr>
                                                        <th style="padding:5px; font-size:10px;">Infrastructure </th>
                                                        <th style="padding:5px; font-size:10px;">Initial Period </th>
                                                        <th style="padding:5px; font-size:10px;">Regular Operation Period at maximum capacity  </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="padding:5px; font-size:10px">
                                                            <label class="text-left" for="land">Land (in square-meter) :</label>
                                                        </td>
                                                        <td style="padding:5px; font-size:10px"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->land_ini) && is_numeric($alreadyExistApplicant->land_ini) ? number_format($alreadyExistApplicant->land_ini,2): 0) }}</span>

                                                        </td>
                                                        <td style="padding:5px; font-size:10px">
                                                            <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->land_reg) && is_numeric($alreadyExistApplicant->land_reg) ? number_format($alreadyExistApplicant->land_reg,2): 0) }}</span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px; font-size:10px">
                                                            <label class="text-left" for="power">Power (in KW/H) :</label>
                                                        </td>
                                                        <td style="padding:5px; font-size:10px"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->power_ini) && is_numeric($alreadyExistApplicant->power_ini) ? number_format($alreadyExistApplicant->power_ini,2) : 0) }}</span>

                                                        </td>
                                                        <td style="padding:5px; font-size:10px">
                                                            <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->power_reg) && is_numeric($alreadyExistApplicant->power_reg) ? number_format($alreadyExistApplicant->power_reg,2): 0) }}</span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px; font-size:10px">
                                                            <label class="text-left" for="gas">GAS  (in CM) :</label>
                                                        </td>
                                                        <td style="padding:5px; font-size:10px"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->gas_ini) && is_numeric($alreadyExistApplicant->gas_ini) ? number_format($alreadyExistApplicant->gas_ini,2): 0) }}</span>

                                                        </td>
                                                        <td style="padding:5px; font-size:10px">
                                                            <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->gas_reg) && is_numeric($alreadyExistApplicant->gas_reg) ? number_format($alreadyExistApplicant->gas_reg,2): 0) }}</span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px; font-size:10px">
                                                            <label class="text-left" for="water ">Water   (in CM) :</label>
                                                        </td>
                                                        <td style="padding:5px; font-size:10px"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->water_ini) && is_numeric($alreadyExistApplicant->water_ini) ? number_format($alreadyExistApplicant->water_ini,2): 0) }}</span>

                                                        </td>
                                                        <td style="padding:5px; font-size:10px">
                                                            <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->water_reg) && is_numeric($alreadyExistApplicant->water_reg) ? number_format($alreadyExistApplicant->water_reg,2): 0) }}</span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px; font-size:10px">
                                                            <label class="text-left" for="etp ">Central ETP (in CM) :</label>
                                                        </td>
                                                        <td style="padding:5px; font-size:10px"><span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->etp_ini) && is_numeric($alreadyExistApplicant->etp_ini) ? number_format($alreadyExistApplicant->etp_ini,2): 0) }}</span>

                                                        </td>
                                                        <td style="padding:5px; font-size:10px">
                                                            <span class="onlyNumber" style="width:100%;display:block;margin:2px;padding:6px;display:block">{{ (isset($alreadyExistApplicant->etp_reg) && is_numeric($alreadyExistApplicant->etp_reg) ? number_format($alreadyExistApplicant->etp_reg,2): 0) }}</span>


                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><b>18. Required  Documents for attachment</b></div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover ">
                                                <thead>
                                                    <tr>
                                                        <th style="padding:5px; font-size:10px;">No.</th>
                                                        <th colspan="6" style="padding:5px;"><strong style="font-size:10px;">Required Attachments</strong></th>
                                                        <th style="padding:5px; font-size:10px;" colspan="2">Attached PDF file
                                                            <span title="Attached PDF file (Each File Maximum size 3MB)!" data-toggle="tooltip" onmouseover="toolTipFunction()">
                                                                <i aria-hidden="true" class="fa fa-question-circle"></i></span>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($document as $row)

                                                    <tr>
                                                        <td style="padding:5px; font-size:10px"><div align="center">{!! $i !!}<?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></div></td>
                                                        <td style="padding:5px; font-size:10px;" colspan="6">{!!  $row->doc_name !!}</td>
                                                        <td style="padding:5px; font-size:10px;" colspan="2">
                                                            <div id="preview_132">
                                                                @if(isset($clrDocuments[$row->doc_id]['file']))
                                                                <a href="{{URL::to('/uploads/'.(isset($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}"><img width="10" height="10" src="assets/images/pdf.png" alt="pdf" /> <?php
                                                                    if (isset($clrDocuments[$row->doc_id]['file'])) {
                                                                        $file_name = explode('/', $clrDocuments[$row->doc_id]['file']);
                                                                        echo end($file_name);
                                                                    }
                                                                    ?></a>
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div><!-- /.table-responsive -->



                                    </div><!-- /.panel-body -->
                                </div>

                            </fieldset>


                            <fieldset style="display: block;">

                            </fieldset>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection

    @section('footer-script')
    <script type="text/javascript">

        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            }
            return val;
        }

        $(document).ready(function () {
            $('.onlyNumber').each(function ()
            {
                var allClass = $(this).attr('class');
                if (allClass.match("onlyNumber")) {
                    var thisVal = commaSeparateNumber($('.' + allClass).html());
                    console.log(thisVal);
                } else {
                    var thisVal = $('.' + allClass).html();
                }
                //$('.'+allClass).html(thisVal);
                $(this).replaceWith('<span style="width:100%;display:block;margin:2px;padding:6px;display:block">' + thisVal + '</span>');
            });
        });
    </script>
    @endsection

</div>