<style>
    .text-box {
        width: 50px !important;
        padding: 3px !important;
        font-weight: bold !important;
    }    
</style>
<?php
$appsInDesk = \App\Libraries\CommonFunction::appInDesks();
$userType = Auth::user()->user_type;
?>

@if($appsInDesk && ($userType == '1x101' || $userType == '4x404'))
@foreach($appsInDesk as $row)
<div class="col-lg-2 col-md-2 col-xs-2" style="width: 5% !important">
    <div class="panel panel-{{ !empty($row->panel) ? $row->panel :'default' }} text-box">

        <div class="panel-heading" style="height: 45px !important; padding: 5px !important; alignment-adjust: central;"
             onmouseover="toolTipFunction()" data-toggle="tooltip"
             title="{{ !empty($row->name) ? $row->name :'N/A'}}">

            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="h3" style="margin-top:0;margin-bottom:0;font-size:20px;" id="{{ !empty($row->short_name) ? $row->short_name :'N/A'}}">
                        {{ !empty($row->totalApplication) ? $row->totalApplication :'0' }}
                    </div>  
                </div>
            </div>

<!--            <a id="URL{{ !empty($row->short_name) ? $row->short_name :'N/A'}}" target="_blank"
               href="{{ !empty($row->url) && $row->url =='/#' ? 'javascript:void(0)' : url($row->url) }}">-->
                <div class="row" style=" text-decoration: none !important">
                    <div class="col-xs-12 text-center">
                        <div class="h3" style="margin-top:0;margin-bottom:0;font-size:15px; font-weight: bold"> 
                            {{ !empty($row->short_name) ? $row->short_name :'N/A'}}
                        </div>
                    </div>
                </div>
            <!--</a>-->

        </div>
    </div>
</div>
@endforeach
@endif {{--checking not empty $appsInDesk --}}
