@extends('layouts.admin')

@section('content')
    @include('partials.messages')

    <?php
    $accessMode = ACL::getAccsessRight('projectClearance');
    if (!ACL::isAllowed($accessMode, $mode)) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    ?>

    <style>
        .text-sm{ font-size: 9px !important}
        #appClearenceForm label.error {display: none !important; }
        .calender-icon{
            border: medium none; padding-top: 30px ! important;
        }
    </style>
    <section class="content" id="projectClearanceForm">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-body">

                    @if($viewMode == 'on')
                        <!--If Applicant user, it throws an error, so to solve it-->
                        @if(Auth::user()->user_type == '5x505')
                            <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                            <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                        @endif
                        @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(1,2,3,4,5,6)))
                            {!! Form::open(['url' => '/project-clearance/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                            @include('projectClearance::batch-process')
                            <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                            <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                            {!! Form::close() !!}
                        @endif {{-- checking desk users --}}

                        @if(($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)

                            {!! Form::open(array('url' => 'project-clearance/challan-store/'.Encryption::encodeId($alreadyExistApplicant->id),'method' => 'post',
                            'files' => true, 'role'=>'form')) !!}
                            <div class="panel panel-primary">
                                <div class="panel-heading">Pay order related information</div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">

                                            <div class="form-group col-md-12 {{$errors->has('payorder_no') ? 'has-error' : ''}}">
                                                {!! Form::label('Pay Order No :','Pay Order No : ',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('payorder_no', null,['class'=>'form-control bnEng required input-sm',
                                                    'placeholder'=>'e.g. 1103', 'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('payorder_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                                {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('bank_name', $banks, '', ['class' => 'form-control input-sm required',
                                                    'id'=>'bank_name_form']) !!}
                                                    {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                                {!! Form::label('amount','Amount :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('amount',null, ['class'=>'form-control bnEng required input-sm','placeholder'=>'e.g. 5000',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                {!! Form::label('date','Date :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="datepicker col-md-7  input-group date" data-date-format="yyyy-mm-dd">
                                                    {!! Form::text('date', null, ['class'=>'form-control required user_DOB', 'id' => 'user_DOB', 'placeholder'=>'Pick from datepicker']) !!}
                                                    <label for="user_DOB" class="input-group-addon user_DOB">
                                                        <span class="glyphicon glyphicon-calendar user_DOB" id="user_DOB"></span></label>
                                                </div>
                                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('branch',null, ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12 {{$errors->has('payorder_file') ? 'has-error' : ''}}">
                                                {!! Form::label('payorder_file','Pay order copy :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::file('payorder_file',null, ['class'=>'form-control bnEng required input-sm',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('payorder_file','<span class="help-block">:message</span>') !!}
                                                    <span class="text-danger" style="font-size: 9px; font-weight: bold">
                                                    [File Format: *.pdf | Maximum File size 3MB]
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            @if(ACL::getAccsessRight('projectClearance','E'))
                                                <button type="submit" class="btn btn-primary pull-left next">
                                                    <i class="fa fa-chevron-circle-right"></i> Save</button>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                {!! Form::close() !!}<!-- /.form end -->
                            </div> <!--End of Panel Group-->
                        @endif {{-- status_id == 21 or 24  and created by logged user --}}
                    @endif {{-- view mode on --}}


                    @if($viewMode == 'on')
                        @if(isset($form_data->status_id) && $form_data->status_id != 8)
                            <div class="row">
                                <div class="col-md-12 text-right" style="margin-bottom:6px;">
                                    <a href="/project-clearance/re-download/{{ Encryption::encodeId($alreadyExistApplicant->id)}}" target="_blank"
                                       class="btn btn-danger btn-sm pull-right">
                                        <i class="fa fa-download"></i> <strong>Application Download as PDF</strong>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endif
                    <div class="panel panel-red"  id="inputForm">
                        <div class="panel-heading">Application for Project Clearance</div>
                        <div class="panel-body">
                            <?php if ($viewMode != 'on') { ?>


                            {!! Form::open(array('url' => 'project-clearance/store-app','method' => 'post','id' => 'appClearenceForm','role'=>'form','enctype'=>'multipart/form-data')) !!}
                            <input type ="hidden" name="app_id" value="{{(isset($alreadyExistApplicant->id) ? Encryption::encodeId($alreadyExistApplicant->id) : '')}}">
                            <input type="hidden" name="selected_file" id="selected_file" />
                            <input type="hidden" name="validateFieldName" id="validateFieldName" />
                            <input type="hidden" name="isRequired" id="isRequired" />
                            <?php } ?>


                            <h3 class="text-center stepHeader">Applicant Information (Part A)</h3>
                            <?php if ($viewMode == 'on') { ?>
                            <section class="content-header">
                                <ol class="breadcrumb">
                                    <li><strong>Tracking no. : </strong>
                                        {{ $process_data->track_no  }}
                                    </li>

                                    <li><strong> Date of Submission: </strong> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }} </li>
                                    <li><strong>Current Status : </strong>
                                        @if(isset($form_data) && $form_data->status_id == -1) Draft
                                        @else {!! $statusArray[$form_data->status_id] !!}
                                        @endif
                                    </li>
                                    <li>
                                        @if($process_data->desk_id != 0) <strong>Current Desk :</strong> {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                        @else
                                            <strong>Current Desk :</strong> Applicant
                                        @endif
                                    </li>
                                    @if(isset($form_data->status_id) && $form_data->status_id == 8)
                                    <li>
                                        <strong>Discard Reason :</strong> {{ !empty($form_data->remarks)? $form_data->remarks : 'N/A' }}
                                    </li>
                                    @endif
                                    <li>
                                        <?php if (isset($form_data) && $form_data->status_id == 23 && isset($form_data->certificate)) { ?>
                                        <a href="{{ url($form_data->certificate) }}" class="btn show-in-view btn-xs btn-info"
                                           title="Download Approval Letter" target="_blank"> <i class="fa  fa-file-pdf-o"></i> <b>Download Certificate</b></a>
                                        @if(Auth::user()->user_type == '1x101')
                                            <a onclick="return confirm('Are you sure ?')" href="/project-clearance/discard-certificate/{{ Encryption::encodeId($alreadyExistApplicant->id)}}" class="btn show-in-view btn-xs btn-danger"
                                               title="Download Approval Letter"> <i class="fa  fa-trash"></i> <b>Discard Certificate</b></a>
                                        @endif
                                        {{-- @if(Auth::user()->user_type != '5x505') --}}
                                        <a href="/project-clearance/project-cer-re-gen/{{ Encryption::encodeId($alreadyExistApplicant->id)}}" class="btn show-in-view btn-xs btn-warning"
                                           title="Download Approval Letter" target="_self"> <i class="fa  fa-file-pdf-o"></i> <b>Re-generate certificate</b></a>
                                        {{-- @endif  --}}
                                        <?php } ?>
                                    </li>
                                </ol>
                            </section>
                            @if(isset($alreadyExistApplicant->challan_no))

                                <div class="panel panel-primary"  id="ep_form">
                                    <div class="panel-heading">Pay order related information</div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                                    {!! Form::label('Pay Order No','Pay Order No : ',['class'=>'col-md-5 font-ok ']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('challan_no', (isset($alreadyExistApplicant->challan_no) ? $alreadyExistApplicant->challan_no : ''),['class'=>'form-control bnEng required input-sm',
                                                        'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                                    {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('bank_name', $banks, (isset($alreadyExistApplicant->bank_name) ?
                                                        $alreadyExistApplicant->bank_name : ''), ['class' => 'form-control input-sm required',
                                                        'placeholder'=>'Select One']) !!}
                                                        {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                                    {!! Form::label('amount','Amount :',['class'=>'col-md-5 font-ok']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('amount',(isset($alreadyExistApplicant->challan_amount) ? $alreadyExistApplicant->challan_amount : ''), ['class'=>'form-control bnEng required input-sm','placeholder'=>'5000',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                    {!! Form::label('date','Date :',['class'=>'col-md-5 font-ok']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('date',(isset($alreadyExistApplicant->challan_date) ? $alreadyExistApplicant->challan_date : ''), ['class'=>'form-control required input-sm','placeholder'=>'Date',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                    {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok ']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('branch',(isset($alreadyExistApplicant->challan_branch) ? $alreadyExistApplicant->challan_branch : ''), ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                @if(isset($form_data->status_id) && $form_data->status_id != 8)
                                                <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                    {!! Form::label('branch','Pay order copy :',['class'=>'col-md-5 font-ok ']) !!}
                                                    <div class="col-md-7">
                                                        <a href="{{url($alreadyExistApplicant->challan_file)}}" target="_blank"
                                                           class="btn show-in-view btn-xs btn-danger" title="Download Pay Order">
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download</a>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                @if(ACL::getAccsessRight('projectClearance','A'))
                                                    <button type="submit" class="btn btn-primary pull-left next">
                                                        <i class="fa fa-chevron-circle-right"></i> Save</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif {{-- application has challan --}}

                            <?php } ?>


                            <fieldset>
                                <div class="panel panel-primary">
                                    <div class="panel-heading margin-for-preview"><strong>1. Applicant Information</strong></div>
                                    <div class="panel-body">
                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-7 {{$errors->has('applicant_name') ? 'has-error': ''}}">
                                                    {!! Form::label('applicant_name','Applying Firm or Company :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('applicant_name',(isset($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : ''),
                                                        ['maxlength'=>'64', 'class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('applicant_name','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    {!! Form::label('infrastructureReq','Full Address of Registered Head Office of Applicant / Applying Firm or Company :',
                                                    ['class'=>'text-left col-md-12']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="clear: both">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('country') ? 'has-error': ''}}">
                                                    {!! Form::label('country','Country :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('country', $countries, (isset($alreadyExistApplicant->country) ? $alreadyExistApplicant->country : ''), ['class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('country','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('division') ? 'has-error': ''}}
                                                <?php  if(isset($alreadyExistApplicant->country) && $alreadyExistApplicant->country!= 'BD'){
                                                    echo "hidden"; }?>" id="division_div">
                                                    {!! Form::label('division','Division :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('division', $divition_eng, (isset($alreadyExistApplicant->division) ?
                                                        $alreadyExistApplicant->division : ''),['class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('division','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="col-md-6 has-feedback {{ $errors->has('state') ? 'has-error' : ''}}
                                                <?php  if(isset($alreadyExistApplicant->country) && $alreadyExistApplicant->country== 'BD'){
                                                    echo "hidden"; }?>" id="state_div">
                                                    {!! Form::label('state','State :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('state', (isset($alreadyExistApplicant->state) ? $alreadyExistApplicant->state : ''),
                                                        $attributes = array('class'=>'form-control', 'placeholder' => 'Name of your state / division',
                                                        'maxlength'=>'40', 'id'=>"state")) !!}
                                                        {!! $errors->first('state','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group" style="clear: both">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('district') ? 'has-error': ''}}
                                                <?php  if(isset($alreadyExistApplicant->country) && $alreadyExistApplicant->country!= 'BD'){
                                                    echo "hidden"; }?>"  id="district_div">
                                                    {!! Form::label('district','District :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('district', $district_eng, (isset($alreadyExistApplicant->district) ?
                                                        $alreadyExistApplicant->district : ''),
                                                        ['class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('district','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 has-feedback {{ $errors->has('province') ? 'has-error' : ''}}
                                                <?php  if(isset($alreadyExistApplicant->country) && $alreadyExistApplicant->country== 'BD'){
                                                    echo "hidden"; }?>" id="province_div">
                                                    {!! Form::label('province','Province :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('province', (isset($alreadyExistApplicant->province) ? $alreadyExistApplicant->province : null), $attributes = array('class'=>'form-control', 'maxlength'=>'40',
                                                        'placeholder' => 'Enter your Province', 'id'=>"province")) !!}
                                                        {!! $errors->first('province','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('road_no','Address Line 1 :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('road_no',(isset($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : null), ['maxlength'=>'80',
                                                        'class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="clear: both">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('house_no') ? 'has-error': ''}}">
                                                    {!! Form::label('house_no','Address Line 2 :', ['class'=>'col-md-5 text-left']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('house_no',(isset($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : null),
                                                        ['maxlength'=>'80','class' => 'form-control input-sm']) !!}
                                                        {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('post_code','Post Code :',['class'=>'col-md-5 text-left']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('post_code',(isset($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : null), ['maxlength'=>'20',
                                                        'class' => 'form-control input-sm nocomma number']) !!}
                                                        {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group" style="clear: both">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('phone') ? 'has-error': ''}}">
                                                    {!! Form::label('phone','Phone No :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('phone', (isset($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : null),
                                                        ['maxlength'=>'20', 'class' => 'phone form-control onlyNumber nocomma input-sm required','placeholder'=>'e.g. 012345678']) !!}
                                                        {!! $errors->first('phone','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('fax','Fax No :',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('fax', (isset($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : null), ['maxlength'=>'20',
                                                        'class' => 'form-control input-sm onlyNumber nocomma','placeholder'=>'e.g. 02 8059253 ']) !!}
                                                        {!! $errors->first('fax','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="clear: both">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('email') ? 'has-error': ''}}">
                                                    {!! Form::label('email','Email :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('email',(isset($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : null), ['maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                                        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('website','Website :',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('website',(isset($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : null),
                                                        ['maxlength'=>'100','class' => 'form-control input-sm', 'placeholder'=> 'www.example.com']) !!}
                                                        {!! $errors->first('website','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <strong style="display: inline-block;width:60%;">2. Authorized Information</strong>
                                    <span class="text-right" id="full_same_as_authorized" style=" width: 38%;display: inline-block;">
                                        {!! Form::checkbox('same_as_authorized','Yes',(empty($alreadyExistApplicant->same_as_authorized) ? false : true), ['class' => 'text-left','onclick'=>'editAuthorizeInfo(this)','id'=>'same_as_authorized']) !!}
                                        {!! Form::label('same_as_authorized','Same as Authorized Person',['class'=>'text-left','style'=>'font-size:13px;']) !!}
                                    </span>
                                    </div>

                                    <div class="panel-body">

                                        <div id="first_step_authorize_info">

                                            <div class="form-group"  style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_name') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_name','Name of the Correspondent Applicant Name :',['class'=>'required-star col-md-12']) !!}
                                                    </div>
                                                    <div class="col-md-6 {{$errors->has('correspondent_name') ? 'has-error': ''}}">
                                                        <div class="col-md-5">&nbsp;</div>
                                                        <div class="col-md-7">
                                                            <?php $authCorrespondent_name = (isset($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : ''); ?>
                                                            {!! Form::text('correspondent_name',(isset($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name),
                                                            ['maxlength'=>'64', 'class' => 'form-control input-sm required textOnly','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_name','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_nationality') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_nationality','Nationality :',['class'=>'required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authNationality = (isset($logged_user_info->nationality) ? $logged_user_info->nationality : ''); ?>
                                                            {!! Form::select('correspondent_nationality', $nationality,
                                                            (isset($alreadyExistApplicant->correspondent_nationality) ? $alreadyExistApplicant->correspondent_nationality : $authNationality),
                                                            ['class' => 'form-control input-sm required','readonly'=>true, 'placeholder' => 'Select One']) !!}
                                                            {!! $errors->first('correspondent_nationality','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                     
                                                    @if ($viewMode != 'on')
                                                    <div class="col-md-6">
                                                        {!! Form::label('Identification Type :','Identification Type:',['class'=>'col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <select class="form-control input-sm required" id="change_nid" readonly="">
                                                                <option value="1" <?php
                                                                if(empty($logged_user_info->passport_no) && empty($alreadyExistApplicant->correspondent_passport)
                                                                        && $logged_user_info->user_nid!=""){ echo "selected"; } ?> >NID</option>

                                                                <option value="2"  <?php  if(isset($alreadyExistApplicant->correspondent_passport) 
                                                                        and  $alreadyExistApplicant->correspondent_passport!=""){ echo "selected";} ?> >
                                                                    Passport</option>
                                                            </select>
                                                            {!! $errors->first('correspondent_passport','<span class="help-block">:message</span>') !!}
                                                        </div>                                                                                                               
                                                    </div>
                                                    @endif {{-- view mode off --}}
                                                    
                                          @if ($viewMode == 'on')
                                                    @if(isset($alreadyExistApplicant->correspondent_passport) && $alreadyExistApplicant->correspondent_passport != '')
                                                    <div class="col-md-6">
                                                            {!! Form::label('correspondent_passport','Passport :',['class'=>'text-left col-md-5']) !!}
                                                            <div class="col-md-7">
                                                            {!! Form::text('correspondent_nid',$alreadyExistApplicant->correspondent_passport,['class' => 'form-control input-sm']) !!}
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="col-md-6">
                                                            {!! Form::label('correspondent_nid','NID :',['class'=>'text-left col-md-5']) !!}
                                                            <div class="col-md-7">
                                                            {!! Form::text('correspondent_nid',$alreadyExistApplicant->correspondent_nid,['class' => 'form-control input-sm']) !!}
                                                            </div>
                                                        </div>
                                                    @endif {{-- checking passport / NID --}}
                                          @endif {{-- view mode on --}}
                                          
                                                </div><!--/row -->
                                            </div><!--/form-group -->
                                            
                                          @if ($viewMode != 'on')                                                       
                                            <div class="form-group" style="clear: both;">
                                                <div class="row">
                                                        <div class="col-md-12 col-md-offset-6  col-md-6{{$errors->has('correspondent_passport') ? 'has-error': ''}}"
                                                             id="correspondent_passport_show" style="<?php 
                                                             if (isset($alreadyExistApplicant->correspondent_passport)!="")
                                                        { echo 'display:block;'; }?>" >
                                                            {!! Form::label('correspondent_passport','Passport :',['class'=>'text-left col-md-5']) !!}
                                                            <div class="col-md-7">
                                                                <?php $authPassport_no = (isset($logged_user_info->passport_no) ? $logged_user_info->passport_no : ''); ?>
                                                                    {!! Form::text('correspondent_passport',(isset($alreadyExistApplicant->correspondent_passport) ?
                                                                    $alreadyExistApplicant->correspondent_passport: $authPassport_no),
                                                                ['maxlength'=>'64','class' => 'form-control input-sm','readonly'=>true]) !!}
                                                                {!! $errors->first('correspondent_passport','<span class="help-block">:message</span>') !!}
                                                            </div>
                                                        </div>
                                    
                                                        <div class="col-md-12 col-md-offset-6  col-md-6{{$errors->has('correspondent_nid') ? 'has-error': ''}}" 
                                                             id="nid_show"  style="<?php
                                                             if (isset($alreadyExistApplicant->correspondent_nid)!="")
                                                                 { echo 'display:block;'; }?>" >
                                                            {!! Form::label('correspondent_nid','Nid :',['class'=>'text-left col-md-5']) !!}
                                                            <div class="col-md-7">
                                                                <?php $authNid_no = (isset($logged_user_info->user_nid) ? $logged_user_info->user_nid : ''); ?>
                                                                {!! Form::text('correspondent_nid',(isset($alreadyExistApplicant->correspondent_nid) ? 
                                                                $alreadyExistApplicant->correspondent_nid : $authNid_no),['maxlength'=>'64',
                                                                'class' => 'form-control input-sm','readonly'=>true]) !!}
                                                                {!! $errors->first('correspondent_nid','<span class="help-block">:message</span>') !!}
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                          @endif {{-- view mode off --}}       

                                            <div class="form-group clearfix">
                                                <div class="row">
                                                    <div class=" col-md-12 ">
                                                        {!! Form::label('infrastructureReq','Correspondent Address & Contact Details :', ['class'=>'text-left col-md-12']) !!}
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group" style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_country') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_country','Country :',['class'=>'text-left required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authCountry = (isset($logged_user_info->country) ? $logged_user_info->country : ''); ?>
                                                            {!! Form::select('correspondent_country', $countries,
                                                            (isset($alreadyExistApplicant->correspondent_country) ? $alreadyExistApplicant->correspondent_country : $authCountry),
                                                            ['class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_country','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 {{$errors->has('correspondent_division') ? 'has-error': ''}}
                                                    <?php  if(isset($alreadyExistApplicant->correspondent_country) &&
                                                            $alreadyExistApplicant->correspondent_country!= 'BD'){
                                                        echo "hidden"; }?>" id="correspondent_division_div">
                                                        {!! Form::label('s','Division :',['class'=>'text-left required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authDivision = (isset($logged_user_info->division) ? $logged_user_info->division : ''); ?>
                                                            {!! Form::select('correspondent_division', $divition_eng,
                                                            (isset($alreadyExistApplicant->correspondent_division) ?
                                                            $alreadyExistApplicant->correspondent_division : $authDivision),['id' => 'correspondent_division',
                                                            'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_division','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 has-feedback {{ $errors->has('correspondent_state') ? 'has-error' : ''}}
                                                    <?php  if(isset($alreadyExistApplicant->correspondent_country) &&
                                                            $alreadyExistApplicant->correspondent_country== 'BD'){
                                                        echo "hidden"; }?>" id="correspondent_state_div">
                                                        {!! Form::label('correspondent_state','State :',['class'=>'text-left required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authState = (isset($logged_user_info->state) ? $logged_user_info->state : ''); ?>
                                                            {!! Form::text('correspondent_state', (isset($alreadyExistApplicant->correspondent_state) ?
                                                            $alreadyExistApplicant->correspondent_state : $authState), ['class'=>'form-control','readonly'=>true,
                                                            'placeholder' => 'Name of your state / division', 'maxlength'=>'40', 'id'=>"correspondent_state"]) !!}
                                                            {!! $errors->first('correspondent_state','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_district') ? 'has-error': ''}}
                                                    <?php  if(isset($alreadyExistApplicant->correspondent_country) &&
                                                            $alreadyExistApplicant->correspondent_country != 'BD'){
                                                        echo "hidden"; }?>" id="correspondent_district_div">
                                                        {!! Form::label('correspondent_district','District :',['class'=>'text-left col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authDistrict = (isset($logged_user_info->district) ? $logged_user_info->district : ''); ?>
                                                            {!! Form::select('correspondent_district', $district_eng, (isset($alreadyExistApplicant->correspondent_district) ?
                                                            $alreadyExistApplicant->correspondent_district : $authDistrict),['class' => 'form-control input-sm','readonly'=>false]) !!}
                                                            {!! $errors->first('correspondent_district','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 has-feedback {{ $errors->has('correspondent_province') ? 'has-error' : ''}}
                                                    <?php  if(isset($alreadyExistApplicant->correspondent_country) &&
                                                            $alreadyExistApplicant->correspondent_country== 'BD'){
                                                        echo "hidden"; }?>" id="correspondent_province_div">
                                                        {!! Form::label('correspondent_province','Province :',['class'=>'text-left col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authProvince = (isset($logged_user_info->province) ? $logged_user_info->province : ''); ?>
                                                            {!! Form::text('correspondent_province', (isset($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince),
                                                            $attributes = array('class'=>'form-control', 'maxlength'=>'40', 'placeholder' => 'Enter the name of your Province',
                                                            'readonly'=>true,'id'=>"correspondent_province")) !!}
                                                            {!! $errors->first('correspondent_province','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6  {{$errors->has('correspondent_road_no') ? 'has-error': ''}}">
                                                        <?php $authRoad_no = (isset($logged_user_info->road_no) ? $logged_user_info->road_no : ''); ?>
                                                        {!! Form::label('correspondent_road_no','Address Line 1 :',['class'=>'text-left required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            {!! Form::text('correspondent_road_no',(isset($alreadyExistApplicant->correspondent_road_no) ? $alreadyExistApplicant->correspondent_road_no : $authRoad_no), ['maxlength'=>'80',
                                                            'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_road_no','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_house_no') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_house_no','Address Line 2 :', ['class'=>'text-left col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authHouse_no = (isset($logged_user_info->house_no) ? $logged_user_info->house_no : ''); ?>
                                                            {!! Form::text('correspondent_house_no',(isset($alreadyExistApplicant->correspondent_house_no) ? $alreadyExistApplicant->correspondent_house_no :
                                                            $authHouse_no), ['maxlength'=>'80', 'class' => 'form-control input-sm','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_house_no','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6  {{$errors->has('correspondent_post_code') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_post_code','Post Code :',['class'=>'text-left col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authPost_code = (isset($logged_user_info->post_code) ? $logged_user_info->post_code : ''); ?>
                                                            {!! Form::text('correspondent_post_code',(isset($alreadyExistApplicant->correspondent_post_code) ?
                                                            $alreadyExistApplicant->correspondent_post_code : $authPost_code), ['maxlength'=>'20',
                                                            'class' => 'form-control input-sm nocomma number','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_post_code','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_phone') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_phone','Phone No :',['class'=>'text-left required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authUser_phone = (isset($logged_user_info->user_phone) ? $logged_user_info->user_phone : ''); ?>
                                                            {!! Form::text('correspondent_phone',(isset($alreadyExistApplicant->correspondent_phone) ?
                                                            $alreadyExistApplicant->correspondent_phone : $authUser_phone),
                                                            ['maxlength'=>'20', 'class' => 'phone form-control input-sm required onlyNumber nocomma','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_phone','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 {{$errors->has('correspondent_fax') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_fax','Fax No :',['class'=>'text-left col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authUser_fax = (isset($logged_user_info->user_fax) ? $logged_user_info->user_fax : ''); ?>
                                                            {!! Form::text('correspondent_fax',(isset($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax),
                                                            ['maxlength'=>'20', 'class' => 'form-control input-sm onlyNumber nocomma','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_fax','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group " style="clear: both">
                                                <div class="row">
                                                    <div class="col-md-6 {{$errors->has('correspondent_email') ? 'has-error': ''}}">
                                                        {!! Form::label('correspondent_email','Email :',['class'=>'text-left required-star col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <?php $authUser_email = (isset($logged_user_info->user_email) ? $logged_user_info->user_email : ''); ?>
                                                            {!! Form::text('correspondent_email',(isset($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email),
                                                            ['maxlength'=>'64','class' => 'form-control input-sm required email','readonly'=>true]) !!}
                                                            {!! $errors->first('correspondent_email','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>



                            <h3 class="text-center stepHeader">Proposed Project  (Part B)</h3>
                            <fieldset>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>3. Proposed Project</strong></div>
                                    <div class="panel-body">
                                        <div class="form-group clearfix">
                                            <div class="col-md-6 {{$errors->has('zone_type') ? 'has-error': ''}}">
                                                {!! Form::label('zone_type','Type of Economic Zone where business to be set :' ,
                                                ['class'=>'text-left required-star']) !!}
                                                {!! Form::select('zone_type', $zoneType,
                                                (isset($alreadyExistApplicant->zone_type) ? $alreadyExistApplicant->zone_type : ''), ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('zone_type','<span class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="col-md-6 {{$errors->has('eco_zone_id') ? 'has-error': ''}}">
                                                {!! Form::label('eco_zone_id','Economic Zone Name :',['class'=>'text-left required-star']) !!}
                                                {!! Form::select('eco_zone_id', $economicZone, (isset($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : ''),
                                                ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                {!! $errors->first('eco_zone_id','<span class="help-block">:message</span>') !!}
                                            </div>

                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-md-6 {{$errors->has('proposed_name') ? 'has-error': ''}}">
                                                {!! Form::label('proposed_name','Proposed Project / Company Name which will carry out the Business :',
                                                ['class'=>'text-left required-star','style'=>'font-size:12px;']) !!}
                                                {!! Form::text('proposed_name',(isset($alreadyExistApplicant->proposed_name) ?
                                                $alreadyExistApplicant->proposed_name : ''),['maxlength'=>'64','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('proposed_name','<span class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="col-md-6 {{$errors->has('company_logo') ? 'has-error': ''}}">
                                                {!! Form::label('company_logo','Company logo :',
                                                ['class'=>'text-left required-star','style'=>'font-size:12px;']) !!}

                                                <span id="company_logo_err" class="text-danger" style="font-size: 10px;"></span>
                                                {!! Form::file('company_logo', ['class'=>!empty($alreadyExistApplicant->company_logo)?'':'required',
                                                'data-rule-maxlength'=>'40','onchange'=>'companyLogo(this)'])!!}
                                                <span class="text-danger" style="font-size: 9px; font-weight: bold">[File Format: *.jpg/ .jpeg | File size within 3 MB]</span><br/>
                                                <div style="position:relative;">
                                                    <img id="companyLogoViewer" style="width:auto;height:70px;position:absolute;top:-56px;right:0px;border:1px solid #ddd;padding:2px;background:#a1a1a1;"
                                                         src="{{ (!empty($alreadyExistApplicant->company_logo)? url($alreadyExistApplicant->company_logo) :
                                                                 url('assets/images/company_logo.png')) }}" alt="">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="col-md-6 {{$errors->has('business_type') ? 'has-error': ''}}">
                                                {!! Form::label('business_type','Type of Business / Industry or Services :',['class'=>'text-left required-star']) !!}
                                                {!! Form::select('business_type', $businessIndustryServices, (isset($alreadyExistApplicant->business_type) ? $alreadyExistApplicant->business_type : ''),
                                                ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('business_type','<span class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="col-md-6 {{$errors->has('organization_type') ? 'has-error': ''}}">
                                                {!! Form::label('organization_type','Type of Organization :',['class'=>'text-left required-star']) !!}
                                                {!! Form::select('organization_type', $typeofOrganizations, (isset($alreadyExistApplicant->organization_type) ?
                                                $alreadyExistApplicant->organization_type : ''), ['class' => 'form-control input-sm required',
                                                'onchange'=>'in_joint_com(this.value)']) !!}
                                                {!! $errors->first('organization_type','<span class="help-block">:message</span>') !!}
                                            </div>


                                        </div>

                                        <div class="form-group" style="clear:both">
                                            <div class="col-md-6 {{$errors->has('industry_type') ? 'has-error': ''}}">
                                                {!! Form::label('industry_type','Type of Industry :',['class'=>'text-left required-star']) !!}
                                                {!! Form::select('industry_type', $typeofIndustry, (!empty($alreadyExistApplicant->industry_type) ?
                                                $alreadyExistApplicant->industry_type : ''), ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('industry_type','<span class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('industry_cat_id','Industry Category :',['class'=>'text-left']) !!}
                                                {!! Form::select('industry_cat_id', $industry_cat, (!empty($alreadyExistApplicant->industry_cat_id) ?
                                                $alreadyExistApplicant->industry_cat_id : ''),
                                                ['class' => 'form-control input-sm required colors_name','placeholder'=>'Select One']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group" style="clear:both; padding-top: 5px;">
                                            <div class="col-md-6"></div>
                                            <div class="col-md-6 ">
                                                <div class="col-md-6" id="color_name" style="font-weight: bold;">{{ (!empty($industryCatInfo->color_name)?$industryCatInfo->color_name:'') }}</div>
                                                <?php (!empty($industryCatInfo->color_code)? $color_code = $industryCatInfo->color_code : $color_code = '#FFFFFF') ?>
                                                <div id="change_colours" class="col-md-6" style="height: 20px;background-color: {{ $color_code }};"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-12"><br/></div>
                                        <div class="form-group type_of_organizations_main" style="clear:both;
                                            <?php if(isset($alreadyExistApplicant->organization_type) && $alreadyExistApplicant->organization_type != 'Joint Venture')
                                        { echo 'display:none;'; } ?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="type_of_organizations" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                            <thead class="alert alert-success">
                                                            <tr>
                                                                <th>Company Name <span class="required-star"></span></th>
                                                                <th>Company Address <span class="required-star"></span></th>
                                                                <th>Country <span class="required-star"></span></th>
                                                                <th><span class="hashs">#</span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(count($jointOrganizations) > 0)
                                                                <?php $inc = 0; ?>
                                                                @foreach($jointOrganizations as $eachJointOrg)
                                                                    <tr class="otherJointRows" id="joinRows{{$inc}}">
                                                                        <td>
                                                                            {!! Form::hidden("joint_id[$inc]",$eachJointOrg->id) !!}
                                                                            {!! Form::text("joint_company[$inc]",$eachJointOrg->joint_company, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first('joint_company','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            {!! Form::text("joint_company_address[$inc]",$eachJointOrg->joint_company_address, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first("joint_company_address[$inc]",'<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>{!! Form::select("joint_com_country[$inc]", $countries, $eachJointOrg->joint_com_country, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first("joint_com_country[$inc]",'<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                    <span class="add_new_span">
                                                                        <?php if ($inc !== 0) { ?>
                                                                        <a class="btn btn-xs addTableRows btn-danger" onclick="removeTableRow('type_of_organizations','joinRows{{$inc}}')">
                                                                            <i class="fa fa-times"></i></a>
                                                                        <?php
                                                                        } else { ?>
                                                                        <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('type_of_organizations', 'joinRows0');"><i class="fa fa-plus"></i></a>
                                                                        <?php } ?>
                                                                    </span>
                                                                        </td>
                                                                    </tr>
                                                                    <?php $inc++; ?>
                                                                @endforeach
                                                            @else

                                                                <tr id="joinRows0">
                                                                    <td>
                                                                        {!! Form::text('joint_company[0]','', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('joint_company','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text('joint_company_address[0]','', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('joint_company_address[0]','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::select('joint_com_country[0]', $countries,'', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('joint_com_country[0]','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                    <span class="add_new_span">
                                                                        <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('type_of_organizations', 'joinRows0');"><i class="fa fa-plus"></i></a>
                                                                    </span>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>4. Construction Schedule</strong></div>
                                    <div class="panel-body">

                                        <div class="form-group" style="clear:both">
                                            <div class="row col-md-12">
                                                <div class="col-md-4 {{$errors->has('construction_start') ? 'has-error': ''}}">
                                                    <div class="construction_start input-group date col-md-12">
                                                        {!! Form::label('construction_start',' Start Time :',['class'=>'text-left required-star']) !!}
                                                        {!! Form::text('construction_start',  (isset($alreadyExistApplicant->construction_start) ? App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->construction_start, 0, 10)) : ''), ['class' => 'form-control  input-sm required']) !!}
                                                        <span class="input-group-addon calender-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        {!! $errors->first('construction_start','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-4 {{$errors->has('construction_end') ? 'has-error': ''}}">
                                                    <div class="construction_end input-group date col-md-12">
                                                        {!! Form::label('construction_end',' End Time :',['class'=>'text-left required-star']) !!}
                                                        {!! Form::text('construction_end',  (isset($alreadyExistApplicant->construction_end) ? App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->construction_end, 0, 10)) : ''), ['class' => 'form-control  input-sm required']) !!}
                                                        <span class="input-group-addon calender-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        {!! $errors->first('construction_end','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-4 {{$errors->has('construction_duration') ? 'has-error': ''}}">
                                                    {!! Form::label('construction_duration',' Duration (in days) :',['class'=>'text-left required-star']) !!}
                                                    {!! Form::text('construction_duration',(isset($alreadyExistApplicant->construction_duration) ? $alreadyExistApplicant->construction_duration : ''), ['maxlength'=>'100',
                                                    'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                    {!! $errors->first('construction_duration','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="clear:both">
                                            <div class="row col-md-12">
                                                <div class="col-md-4 {{$errors->has('cod_date') ? 'has-error': ''}}">
                                                    <div class="datepicker input-group date col-md-12">
                                                        {!! Form::label('cod_date',' Commercial Operation Date (COD) :',['class'=>'text-left required-star']) !!}
                                                        {!! Form::text('cod_date',  (isset($alreadyExistApplicant->cod_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->cod_date, 0, 10)) : ''),
                                                        ['class' => 'form-control  input-sm required']) !!}
                                                        <span class="input-group-addon calender-icon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        {!! $errors->first('cod_date','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>5. Carry of the Business (BDT) </strong></div>
                                    <div class="panel-body">

                                        <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                            <thead class="alert alert-info">
                                                            <tr>
                                                                <th>Capital Structure<span class="required-star"></span></th>
                                                                <th>Total <span class="required-star"></span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <th>Authorized Capital :</th>
                                                                <td>{!! Form::text('auth_capital_to',(isset($alreadyExistApplicant->auth_capital_to) ?
                                                                    $alreadyExistApplicant->auth_capital_to : ''),['maxlength'=>'40','id'=>'auth_capital_to',
                                                                    'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('auth_capital_to','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Paid-up Capital : </th>
                                                                <td>{!! Form::text('paid_capital_to',(isset($alreadyExistApplicant->paid_capital_to) ?
                                                                    $alreadyExistApplicant->paid_capital_to : ''),['maxlength'=>'40',
                                                                    'id'=>'paid_capital_to','class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('paid_capital_to','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Borrowing / Loan : </th>
                                                                <td>{!! Form::text('ext_borrow_to',(isset($alreadyExistApplicant->ext_borrow_to) ?
                                                                    $alreadyExistApplicant->ext_borrow_to : ''),['maxlength'=>'40','id'=>'ext_borrow_to',
                                                                    'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('ext_borrow_to','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>6. Contribution in Paid-up Capital Among Shareholders</strong></div>
                                    <div class="panel-body">

                                        <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                            <thead class="alert alert-warning">
                                                            <tr>
                                                                <th>&nbsp;</th>
                                                                <th>Local Share<span class="required-star"></span></th>
                                                                <th>Foreign Share <span class="required-star"></span></th>
                                                                <th>Total Share  <span class="required-star"></span> <span id="paid_cap_percentage_error" style="color:red"></span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <th>Paid-up Capital (%)</th>
                                                                <td>
                                                                    {!! Form::text('paid_cap_amount',(isset($alreadyExistApplicant->paid_cap_amount) ? $alreadyExistApplicant->paid_cap_amount : ''),
                                                                    ['class' => 'form-control input-sm required onlyNumber','maxlength'=>20, 'id' => 'paid_cap_amount',
                                                                    'onKeyUp' => 'Calculate2Numbers("paid_cap_amount", "paid_cap_nature", "paid_cap_percentage","paid_cap_percentage_error")']) !!}
                                                                    {!! $errors->first('paid_cap_amount','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('paid_cap_nature',(isset($alreadyExistApplicant->paid_cap_nature) ? $alreadyExistApplicant->paid_cap_nature : ''),
                                                                    ['class' => 'form-control input-sm required onlyNumber','maxlength'=>20, 'id' => 'paid_cap_nature',
                                                                    'onKeyUp' => 'Calculate2Numbers("paid_cap_amount", "paid_cap_nature", "paid_cap_percentage","paid_cap_percentage_error")']) !!}
                                                                    {!! $errors->first('paid_cap_nature','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('paid_cap_percentage',(isset($alreadyExistApplicant->paid_cap_percentage) ? $alreadyExistApplicant->paid_cap_percentage : ''),
                                                                    ['class' => 'form-control input-sm required onlyNumber','maxlength'=>20, 'id' => 'paid_cap_percentage', 'readonly']) !!}
                                                                    {!! $errors->first('paid_cap_percentage','<span class="help-block">:message</span>') !!}
                                                                    <input type="text" value="" class="required"  style="display:none" />
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>7.Sponsor of Shareholder</strong></div>
                                    <div class="panel-body">
                                        <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="directors_list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                            <thead class="alert alert-success">
                                                            <tr class="text-center">
                                                                <th>Name <span class="required-star"></span></th>
                                                                <th>Address <span class="required-star"></span></th>
                                                                <th>Nationality</th>
                                                                <th>Status in the proposed company <span class="required-star"></span></th>
                                                                <th>Extent of share Holding (%) <span class="required-star"></span>
                                                                    <span id="share_holder_percentage_error" style="color:red"> </span>
                                                                </th>
                                                                <th class=""><span class="hashs">#</span>  </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(count($sponsorDirections) > 0)
                                                                <?php $inc = 0; ?>
                                                                @foreach($sponsorDirections as $eachSponsor)
                                                                    <tr class="trCountValue" id="templateRow{{$inc}}">
                                                                        <td>
                                                                            {!! Form::hidden("sponsor_id[$inc]",$eachSponsor->id) !!}
                                                                            {!! Form::text("sponsor_name[$inc]",$eachSponsor->sponsor_name, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first('sponsor_name','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            {!! Form::text("sponsor_address[$inc]",$eachSponsor->sponsor_address, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first('sponsor_address','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            {!! Form::select("sponsor_nationality[$inc]",$nationality,$eachSponsor->sponsor_nationality, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm', 'placeholder' => 'Select One']) !!}
                                                                            {!! $errors->first('sponsor_nationality','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>{!! Form::text("sponsor_status[$inc]",$eachSponsor->sponsor_status, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first('sponsor_status','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>{!! Form::text("sponsor_share_ext[$inc]",$eachSponsor->sponsor_share_ext, ['maxlength'=>'20',
                                                                            'class' => 'form-control input-sm required countValue onlyNumber','onblur'=>"countValue(this)"]) !!}
                                                                            {!! $errors->first('sponsor_share_ext','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            <?php if ($inc == 0) { ?>
                                                                            <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('directors_list', 'templateRow0');"><i class="fa fa-plus"></i></a>
                                                                            <?php } else { ?>
                                                                            <a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeTableRow('directors_list','templateRow{{$inc}}');">
                                                                                <i class="fa fa-times" aria-hidden="true"></i></a>
                                                                            <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php $inc++; ?>
                                                                @endforeach
                                                            @else
                                                                <tr id="templateRow">
                                                                    <td>
                                                                        {!! Form::text('sponsor_name[0]','', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('sponsor_name','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text('sponsor_address[0]','', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('sponsor_address','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::select('sponsor_nationality[0]', $nationality, '', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm', 'placeholder' => 'Select One']) !!}
                                                                        {!! $errors->first('sponsor_nationality','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text('sponsor_status[0]','', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('sponsor_status','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("sponsor_share_ext[0]", '', ['maxlength'=>'20', 'onblur'=>"countValue(this)",
                                                                        'class' => 'form-control input-sm required countValue onlyNumber']) !!}
                                                                        {!! $errors->first('sponsor_share_ext','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('directors_list', 'templateRow');"><i class="fa fa-plus"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>8. Area of Land / SFB to be allotted</strong></div>
                                    <div class="panel-body">
                                        <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="land_sfb_tbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                            <thead class="alert alert-info">
                                                            <tr class="text-center">
                                                                <th>Agreed land by developers (square meter) <span class="required-star"></span></th>
                                                                <th>Plot Address <span class="required-star"></span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr id="land_sfb_tbl_row">
                                                                <td>
                                                                    {!! Form::text("agreed_land",(isset($alreadyExistApplicant->agreed_land) ? $alreadyExistApplicant->agreed_land : ''),
                                                                    ['maxlength'=>'20','class' => 'form-control input-sm required']) !!}
                                                                    {!! $errors->first('agreed_land','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text("sfb_plot_address", (isset($alreadyExistApplicant->sfb_plot_address) ? $alreadyExistApplicant->sfb_plot_address : ''),
                                                                    ['maxlength'=>'200', 'class' => 'form-control input-sm required']) !!}
                                                                    {!! $errors->first('sfb_plot_address','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--/panel-body-->
                                </div><!--/panel-->

                            </fieldset>

                            <h3 class="text-center stepHeader">Proposed Project (Part C)</h3>
                            <fieldset>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>9. Service/Products</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class=" col-md-12 {{$errors->has('product_name') ? 'has-error': ''}}">
                                                {!! Form::label('product_name','a) Name / description of the product(s) :' ,
                                                ['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::textarea('product_name', (isset($alreadyExistApplicant->product_name) ? $alreadyExistApplicant->product_name : ''),
                                                    ['maxlength'=>'1000','class' => 'form-control input-sm required', 'size'=>'5x2']) !!}
                                                    {!! $errors->first('product_name','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class=" col-md-12 {{$errors->has('product_usage') ? 'has-error': ''}}">
                                                {!! Form::label('product_usage', 'b) Usage of the product(s) :' ,['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::textarea('product_usage', (isset($alreadyExistApplicant->product_usage) ? $alreadyExistApplicant->product_usage : ''),
                                                    ['class' => 'form-control input-sm required required-star', 'size' => '5x2', 'maxlength'=>'1000']) !!}
                                                    {!! $errors->first('product_usage','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class=" col-md-12 {{$errors->has('manufacture_process') ? 'has-error': ''}}">
                                                {!! Form::label('manufacture_process','c) Manufacturing process :', ['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::textarea('manufacture_process', (isset($alreadyExistApplicant->manufacture_process) ? $alreadyExistApplicant->manufacture_process : ''),
                                                    ['maxlength'=>'1000','class' => 'form-control input-sm required', 'size' => '5x2']) !!}
                                                    {!! $errors->first('manufacture_process','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class=" col-md-12 {{$errors->has('project_cost') ? 'has-error': ''}}">
                                                {!! Form::label('project_cost', 'd) Cost of the project (in US$) :' ,['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('project_cost', (isset($alreadyExistApplicant->project_cost) ? $alreadyExistApplicant->project_cost : ''),
                                                    ['class' => 'form-control input-sm required onlyNumber','maxlength'=>'20']) !!}
                                                    {!! $errors->first('project_cost','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                        </div><!--/row-->
                                    </div><!--/panel-body-->
                                </div><!--/panel-->

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>10. Production Programme</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="productionPrgTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                        <thead class="alert alert-success">
                                                        <tr class="text-center">
                                                            <th class="valigh-middle text-center"> <span class="required-star"></span>Description</th>
                                                            <th class="valigh-middle text-center" width="10%"> Unit </th>
                                                            <th class="text-center">1st Year  <span class="required-star"></span><br/>Qty</th>
                                                            <th class="text-center">2nd Year  <span class="required-star"></span><br/>Qty</th>
                                                            <th class="text-center">3rd Year  <span class="required-star"></span><br/>Qty</th>
                                                            <th class="text-center">4th Year  <span class="required-star"></span><br/>Qty</th>
                                                            <th class="text-center">5th Year  <span class="required-star"></span><br/>Qty</th>
                                                            <th class="valigh-middle text-center"> Total </th>
                                                            <th class="valigh-middle text-center"> <span class="hashs">#</span> </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(count($clr_production) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_production as $eachProduction)
                                                                <tr id="rowProductionCount{{$inc}}">
                                                                    <td>
                                                                        {!! Form::hidden("production_id[$inc]", $eachProduction->id) !!}
                                                                        {!! Form::text("production_desc[$inc]",$eachProduction->production_desc, ['maxlength'=>'100','class' => 'form-control input-sm required production_desc_1st']) !!}
                                                                        {!! $errors->first('production_desc','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::select("production_unit[$inc]",$units,$eachProduction->production_unit, ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm', 'placeholder'=>'Select']) !!}
                                                                        {!! $errors->first('production_unit','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("production_1st[$inc]",$eachProduction->production_1st, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('production_1st','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("production_2nd[$inc]",$eachProduction->production_2nd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('production_2nd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("production_3rd[$inc]",$eachProduction->production_3rd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('production_3rd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("production_4th[$inc]",$eachProduction->production_4th, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('production_4th','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("production_5th[$inc]",$eachProduction->production_5th, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('production_5th','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("production_total[$inc]",(isset($eachProduction->production_total) && !empty($eachProduction->production_total) ?
                                                                        $eachProduction->production_total : '0'), ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm onlyNumber', 'readonly'=>true]) !!}
                                                                        {!! $errors->first('production_total','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        <?php if ($inc == 0) { ?>
                                                                        <a class="btn btn-xs btn-primary addTableRows productionPrgAddRow" onclick="addTableRow('productionPrgTbl', 'rowProductionCount0');"><i class="fa fa-plus"></i></a>
                                                                        <?php } else { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeTableRow('productionPrgTbl','rowProductionCount{{$inc}}');">
                                                                            <i class="fa fa-times" aria-hidden="true"></i></a>
                                                                        <?php } ?>
                                                                    </td>
                                                                </tr>
                                                                <?php $inc++; ?>
                                                            @endforeach
                                                        @else
                                                            <tr id="rowProductionCount">
                                                                <td>
                                                                    {!! Form::text('production_desc[0]',(isset($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : ''),
                                                                    ['maxlength'=>'100','class' => 'form-control input-sm required production_desc_1st']) !!}
                                                                    {!! $errors->first('production_desc','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::select("production_unit[0]",$units,'',['maxlength'=>'100','class' =>'form-control input-sm','placeholder'=>'Select']) !!}
                                                                    {!! $errors->first('production_unit','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('production_1st[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('production_1st','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('production_2nd[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('production_2nd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('production_3rd[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('production_3rd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('production_4th[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('production_4th','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('production_5th[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('production_5th','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text("production_total[0]", '', ['maxlength'=>'100','class' => 'form-control input-sm onlyNumber', 'readonly'=>true]) !!}
                                                                    {!! $errors->first('production_total','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-xs btn-primary addTableRows productionPrgAddRow" onclick="addTableRow('productionPrgTbl', 'rowProductionCount');">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>11. Projection of Export</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="proExportTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                        <thead class="alert alert-info">
                                                        <tr>
                                                            <th class="valigh-middle text-center">Description</th>
                                                            <th class="valigh-middle text-center" width="10%">Unit</th>
                                                            <th class="text-center">1st Year<br/>Qty</th>
                                                            <th class="text-center">2nd Year<br/>Qty</th>
                                                            <th class="text-center">3rd Year<br/>Qty</th>
                                                            <th class="text-center">4th Year <br/>Qty</th>
                                                            <th class="text-center">5th Year <br/>Qty</th>
                                                            <th class="valigh-middle text-center">Total</th>
                                                            <th class="valigh-middle text-center">  <span class="hashs">#</span>  </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(count($clr_export) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_export as $eachExport)
                                                                <tr id="rowProExportCount{{$inc}}">
                                                                    <td>
                                                                        {!! Form::hidden("pro_ext_id[$inc]", $eachExport->id) !!}
                                                                        {!! Form::text("pro_ext_desc[$inc]",$eachExport->pro_ext_desc, ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm pro_ext_desc_1st']) !!}
                                                                        {!! $errors->first('pro_ext_desc','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::select("pro_ext_unit[$inc]",$units,$eachExport->pro_ext_unit, ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm', 'placeholder'=>'Select']) !!}
                                                                        {!! $errors->first('pro_ext_unit','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pro_ext_1st[$inc]",$eachExport->pro_ext_1st, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_ext_1st','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pro_ext_2nd[$inc]",$eachExport->pro_ext_2nd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_ext_2nd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pro_ext_3rd[$inc]",$eachExport->pro_ext_3rd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_ext_3rd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pro_ext_4th[$inc]",$eachExport->pro_ext_4th, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_ext_4th','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pro_ext_5th[$inc]",$eachExport->pro_ext_5th, ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm onlyNumber', 'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_ext_5th','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pro_ext_total[$inc]",(isset($eachExport->pro_ext_total) && !empty($eachExport->pro_ext_total) ? $eachExport->pro_ext_total : 0), ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm onlyNumber', 'readonly'=>true]) !!}
                                                                        {!! $errors->first('pro_ext_total','<span class="help-block">:message</span>') !!}
                                                                    </td>

                                                                    <td>
                                                                        <?php if ($inc == 0) { ?>
                                                                        <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('proExportTbl', 'rowProExportCount0');"><i class="fa fa-plus"></i></a>
                                                                        <?php } else { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeTableRow('proExportTbl','rowProExportCount{{$inc}}');">
                                                                            <i class="fa fa-times" aria-hidden="true"></i></a>
                                                                        <?php } ?>
                                                                    </td>
                                                                </tr>
                                                                <?php $inc++; ?>
                                                            @endforeach
                                                        @else
                                                            <tr id="rowProExportCount0">
                                                                <td>
                                                                    {!! Form::text('pro_ext_desc[0]','', ['maxlength'=>'100','class' => 'form-control input-sm pro_ext_desc_1st']) !!}
                                                                    {!! $errors->first('pro_ext_desc','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::select("pro_ext_unit[0]", $units,'', ['maxlength'=>'20','class' => 'form-control input-sm', 'placeholder'=>'Select']) !!}
                                                                    {!! $errors->first('pro_ext_unit','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pro_ext_1st[0]','', ['maxlength'=>'20','class' => 'form-control input-sm',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_ext_1st','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pro_ext_2nd[0]','', ['maxlength'=>'20','class' => 'form-control input-sm',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_ext_2nd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('pro_ext_3rd[0]','', ['maxlength'=>'20','class' => 'form-control input-sm',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_ext_3rd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pro_ext_4th[0]','', ['maxlength'=>'20','class' => 'form-control input-sm',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_ext_4th','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('pro_ext_5th[0]','', ['maxlength'=>'20','class' => 'form-control input-sm',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_ext_5th','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text("pro_ext_total[0]",'', ['maxlength'=>'20','class' => 'form-control input-sm onlyNumber', 'readonly'=>true]) !!}
                                                                    {!! $errors->first('pro_ext_total','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('proExportTbl', 'rowProExportCount');">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>12. Projection of Domestic</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="proDomesticTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                        <thead class="alert alert-warning">
                                                        <tr>
                                                            <th class="valigh-middle text-center">Description</th>
                                                            <th class="valigh-middle text-center" width="10%"> Unit </th>
                                                            <th class="text-center">1st Year<br/>Qty</th>
                                                            <th class="text-center">2nd Year<br/>Qty</th>
                                                            <th class="text-center">3rd Year<br/>Qty</th>
                                                            <th class="text-center">4th Year<br/>Qty</th>
                                                            <th class="text-center">5th Year<br/>Qty</th>
                                                            <th class="valigh-middle text-center"> Total </th>
                                                            <th class="valigh-middle text-center"> <span class="hashs">#</span>  </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(count($clr_domestic_export) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_domestic_export as $eachDomEx)
                                                                <tr id="rowProDomesticCount{{$inc}}">
                                                                    <td>
                                                                        {!! Form::hidden("pro_dom_id[$inc]", $eachDomEx->id) !!}
                                                                        {!! Form::text("pro_dom_desc[$inc]",$eachDomEx->pro_dom_desc, ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm pro_dom_desc_1st']) !!}
                                                                        {!! $errors->first('pro_dom_desc','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::select("pro_dom_unit[$inc]",$units,$eachDomEx->pro_dom_unit, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm', 'placeholder'=>'Select']) !!}
                                                                        {!! $errors->first('pro_dom_unit','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pro_dom_1st[$inc]",$eachDomEx->pro_dom_1st, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber',
                                                                        'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_dom_1st','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pro_dom_2nd[$inc]",$eachDomEx->pro_dom_2nd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber',
                                                                        'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_dom_2nd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pro_dom_3rd[$inc]",$eachDomEx->pro_dom_3rd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber',
                                                                        'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_dom_3rd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pro_dom_4th[$inc]",$eachDomEx->pro_dom_4th, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber',
                                                                        'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_dom_4th','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pro_dom_5th[$inc]",$eachDomEx->pro_dom_5th, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber',
                                                                        'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                        {!! $errors->first('pro_dom_5th','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pro_dom_total[$inc]", (isset($eachDomEx->pro_dom_total) && !empty($eachDomEx->pro_dom_total) ? $eachDomEx->pro_dom_total : 0), ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm onlyNumber', 'readonly']) !!}
                                                                        {!! $errors->first('pro_dom_total','<span class="help-block">:message</span>') !!}
                                                                    </td>

                                                                    <td>
                                                                        <?php if ($inc == 0) { ?>
                                                                        <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('proDomesticTbl', 'rowProDomesticCount0');"><i class="fa fa-plus"></i></a>
                                                                        <?php } else { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeTableRow('proDomesticTbl','rowProDomesticCount{{$inc}}');">
                                                                            <i class="fa fa-times" aria-hidden="true"></i></a>
                                                                        <?php } ?>

                                                                    </td>
                                                                </tr>
                                                                <?php $inc++; ?>
                                                            @endforeach
                                                        @else
                                                            <tr id="rowProDomesticCount0">
                                                                <td>
                                                                    {!! Form::text('pro_dom_desc[0]','', ['maxlength'=>'100','class' => 'form-control input-sm pro_dom_desc_1st']) !!}
                                                                    {!! $errors->first('pro_dom_desc','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::select('pro_dom_unit[0]',$units,'', ['maxlength'=>'100','class' => 'form-control input-sm',
                                                                    'placeholder'=>'Select']) !!}
                                                                    {!! $errors->first('pro_dom_unit','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pro_dom_1st[0]','', ['maxlength'=>'100', 'class' => 'form-control input-sm onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_dom_1st','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pro_dom_2nd[0]','', ['maxlength'=>'100','class' => 'form-control input-sm onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_dom_2nd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('pro_dom_3rd[0]','', ['maxlength'=>'100','class' => 'form-control input-sm onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_dom_3rd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pro_dom_4th[0]','', ['maxlength'=>'100','class' => 'form-control input-sm onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_dom_4th','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('pro_dom_5th[0]','', ['maxlength'=>'100','class' => 'form-control input-sm onlyNumber',
                                                                    'onKeyUp' => 'calculateTotal(this.parentNode.parentNode.id)']) !!}
                                                                    {!! $errors->first('pro_dom_5th','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('pro_dom_total[0]','', ['maxlength'=>'100','class' => 'form-control input-sm onlyNumber', 'readonly']) !!}
                                                                    {!! $errors->first('pro_dom_total','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('proDomesticTbl', 'rowProDomesticCount');">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>13. Addition of Machinery  </strong></div>
                                    <div class="panel-body">
                                        <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="machinaryTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                            <thead class="alert alert-success">
                                                            <tr class="text-center">
                                                                <th  class="text-center">Details of Machinery <span class="required-star"></span></th>
                                                                <th  class="text-center">Country of Origin <span class="required-star"></span></th>
                                                                <th  class="text-center">Name of the Vendor<span class="required-star"></span></th>
                                                                <th  class="text-center">Value<span class="required-star"></span></th>
                                                                <th  class="text-center" width="10%">State <span class="required-star"></span></th>
                                                                <th  class="text-center">If old, how old? (Year)</th>
                                                                <th  class="text-center"><span class="hashs">#</span>  </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if(count($clr_machinary) > 0)
                                                                <?php $inc = 0; ?>
                                                                @foreach($clr_machinary as $machine)
                                                                    <tr id="rowMachineCount{{$inc}}">
                                                                        <td>
                                                                            {!! Form::hidden("machine_id[$inc]",$machine->id) !!}
                                                                            {!! Form::textarea("m_details[$inc]",$machine->m_details, ['maxlength'=>'1000', 'size'=>'5x2',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first('m_details','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            {!! Form::select("m_country[$inc]",$countries, $machine->m_country, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm', 'placeholder' => 'Select One']) !!}
                                                                            {!! $errors->first('m_country','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>
                                                                            {!! Form::text("m_vendor[$inc]",$machine->m_vendor, ['maxlength'=>'100',
                                                                            'class' => 'form-control input-sm required']) !!}
                                                                            {!! $errors->first('m_vendor','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>

                                                                            {!! Form::select("m_currency[$inc]",$currencies,(!empty($machine->m_currency) ? $machine->m_currency : 107),['class'=>'input-sm required col-md-5 col-md-offset-1 m_currency'])  !!}
                                                                            {!! $errors->first('m_value','<span class="help-block">:message</span>') !!}
                                                                          {!! Form::text("m_value[$inc]",$machine->m_value, ['maxlength'=>'100',
                                                                            'class' => 'col-md-6 input-sm required onlyNumber']) !!}
                                                                        </td>
                                                                        <td>{!! Form::select("m_purchase_state[$inc]",['New'=>'New','Old'=>'Old'],$machine->m_purchase_state,
                                                                            ['maxlength'=>'100', 'class' => 'form-control input-sm required m_purchase_state','onchange'=>"newold(this)"]) !!}
                                                                            {!! $errors->first('m_purchase_state','<span class="help-block">:message</span>') !!}
                                                                        </td>
                                                                        <td>{!! Form::text("m_age[$inc]",$machine->m_age, ['maxlength'=>'3',

                                                                            'class' => 'form-control input-sm onlyNumber readonlyClass max_hundred']) !!}

                                                                            {!! $errors->first('m_age','<span class="help-block">:message</span>') !!}
                                                                        </td>

                                                                        <td>
                                                                            <?php if ($inc == 0) { ?>
                                                                            <a class="btn btn-xs btn-primary addTableRows"
                                                                               onclick="addTableRow('machinaryTbl', 'rowMachineCount0');"><i class="fa fa-plus"></i></a>
                                                                            <?php } else { ?>
                                                                            <a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow"
                                                                               onclick="removeTableRow('machinaryTbl','rowMachineCount{{$inc}}');">
                                                                                <i class="fa fa-times" aria-hidden="true"></i></a>
                                                                            <?php } ?>

                                                                        </td>
                                                                    </tr>
                                                                    <?php $inc++; ?>
                                                                @endforeach
                                                            @else
                                                                <tr id="rowMachineCount">
                                                                    <td>
                                                                        {!! Form::textarea('m_details[0]','', ['maxlength'=>'1000','class' => 'form-control input-sm required', 'size'=>'5x2']) !!}
                                                                        {!! $errors->first('m_details','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::select('m_country[0]', $countries, '', ['maxlength'=>'100','class' => 'form-control input-sm required',
                                                                        'placeholder' => 'Select One']) !!}
                                                                        {!! $errors->first('m_country','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text('m_vendor[0]','',['maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                                        {!! $errors->first('m_vendor','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text('m_value[0]','',['maxlength'=>'100','class' => 'col-md-6 input-sm required onlyNumber']) !!}
                                                                        {!! Form::select('m_currency[0]',$currencies,107,['class'=>'input-sm col-md-offset-1 col-md-5 m_currency required']) !!}
                                                                        {!! $errors->first('m_value','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::select('m_purchase_state[0]',['New'=>'New','Old'=>'Old'],'', ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required m_purchase_state','onchange'=>"newold(this)"]) !!}

                                                                        {!! $errors->first('m_purchase_state','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text('m_age[0]','', ['maxlength'=>'3','class' => 'form-control max_hundred input-sm onlyNumber readonlyClass',
                                                                        'readonly']) !!}

                                                                        {!! $errors->first('m_age','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>

                                                                        <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('machinaryTbl', 'rowMachineCount');">
                                                                            <i class="fa fa-plus"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>14. Manpower requirements</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="mpReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                        <thead class="alert alert-success">
                                                        <tr>
                                                            <th valign="top" class="text-center valigh-middle" width="11%">Year <span class="required-star"></span></th>
                                                            <th colspan="4" valign="top" class="text-center valigh-top">
                                                                <table class="table table-striped table-bordered dt-responsive">
                                                                    <thead class="alert alert-success">
                                                                    <tr class="text-center">
                                                                        <th colspan="4" valign="top" class="text-center valigh-top">Foreign</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center valigh-top" style="width:104px;">Managerial <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top" style="width:107px;">Skilled <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top" style="width:107px;">Unskilled <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top" style="width:105px;">Total <span class="required-star"></span></th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                            <th colspan="4" valign="top" class="text-center valigh-top">
                                                                <table class="table table-striped table-bordered dt-responsive">
                                                                    <thead class="alert alert-success">
                                                                    <tr class="text-center">
                                                                        <th colspan="4" valign="top" class="text-center valigh-top">Local</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center valigh-top" style="width:104px;">Managerial <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top" style="width:107px;">Skilled <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top" style="width:107px;">Unskilled <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top" style="width:105px;">Total <span class="required-star"></span></th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                            <th valign="top" class="text-center valigh-top">Grand Total <span class="required-star"></span></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                {!! Form::selectYear('mp_year_1',  Date('Y'), Date('Y')+ 5, (isset($alreadyExistApplicant->mp_year_1) ? $alreadyExistApplicant->mp_year_1 : ''),
                                                                ['maxlength'=>'20','class' => 'form-control input-sm required onlyNumber', 'placeholder' => 'Select One']) !!}
                                                                {!! $errors->first('mp_year_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_man_1',(isset($alreadyExistApplicant->for_man_1) ? $alreadyExistApplicant->for_man_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_man_1',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_1", "for_skill_1", "for_unskill_1", "for_total_1",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_1", "for_skill_1", "for_unskill_1", "for_total_1", 1)']) !!}
                                                                {!! $errors->first('for_man_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_skill_1',(isset($alreadyExistApplicant->for_skill_1) ? $alreadyExistApplicant->for_skill_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_skill_1',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_1", "for_skill_1", "for_unskill_1", "for_total_1", 1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_1", "for_skill_1", "for_unskill_1", "for_total_1", 1)']) !!}
                                                                {!! $errors->first('for_skill_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_unskill_1',(isset($alreadyExistApplicant->for_unskill_1) ? $alreadyExistApplicant->for_unskill_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_unskill_1',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_1", "for_skill_1", "for_unskill_1", "for_total_1",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_1", "for_skill_1", "for_unskill_1", "for_total_1",1)']) !!}
                                                                {!! $errors->first('for_unskill_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_total_1',(isset($alreadyExistApplicant->for_total_1) ? $alreadyExistApplicant->for_total_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_total_1', 'readonly']) !!}
                                                                {!! $errors->first('for_total_1','<span class="help-block">:message</span>') !!}
                                                            </td>

                                                            <td>
                                                                {!! Form::text('loc_man_1',(isset($alreadyExistApplicant->loc_man_1) ? $alreadyExistApplicant->loc_man_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_man_1',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_1", "loc_skill_1", "loc_unskill_1", "loc_total_1",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_1", "loc_skill_1", "loc_unskill_1", "loc_total_1",1)']) !!}
                                                                {!! $errors->first('loc_man_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_skill_1',(isset($alreadyExistApplicant->loc_skill_1) ? $alreadyExistApplicant->loc_skill_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_skill_1',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_1", "loc_skill_1", "loc_unskill_1", "loc_total_1",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_1", "loc_skill_1", "loc_unskill_1", "loc_total_1",1)']) !!}
                                                                {!! $errors->first('loc_skill_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_unskill_1',(isset($alreadyExistApplicant->loc_unskill_1) ? $alreadyExistApplicant->loc_unskill_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_unskill_1',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_1", "loc_skill_1", "loc_unskill_1", "loc_total_1",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_1", "loc_skill_1", "loc_unskill_1", "loc_total_1",1)']) !!}
                                                                {!! $errors->first('loc_unskill_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_total_1',(isset($alreadyExistApplicant->loc_total_1) ? $alreadyExistApplicant->loc_total_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_total_1', 'readonly']) !!}
                                                                {!! $errors->first('loc_total_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('gr_total_1',(isset($alreadyExistApplicant->gr_total_1) ? $alreadyExistApplicant->gr_total_1 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id'=>'gr_total_1', 'readonly']) !!}
                                                                {!! $errors->first('gr_total_1','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                {!! Form::selectYear('mp_year_2',  Date('Y'), Date('Y')+ 5, (isset($alreadyExistApplicant->mp_year_2) ? $alreadyExistApplicant->mp_year_2 : ''),
                                                                ['maxlength'=>'20','class' => 'form-control input-sm required onlyNumber', 'placeholder' => 'Select One']) !!}
                                                                {!! $errors->first('mp_year_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_man_2',(isset($alreadyExistApplicant->for_man_2) ? $alreadyExistApplicant->for_man_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_man_2',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_2", "for_skill_2", "for_unskill_2", "for_total_2",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_2", "for_skill_2", "for_unskill_2", "for_total_2", 1)']) !!}
                                                                {!! $errors->first('for_man_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_skill_2',(isset($alreadyExistApplicant->for_skill_2) ? $alreadyExistApplicant->for_skill_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_skill_2',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_2", "for_skill_2", "for_unskill_2", "for_total_2", 1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_2", "for_skill_2", "for_unskill_2", "for_total_2", 1)']) !!}
                                                                {!! $errors->first('for_skill_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_unskill_2',(isset($alreadyExistApplicant->for_unskill_2) ? $alreadyExistApplicant->for_unskill_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_unskill_2',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_2", "for_skill_2", "for_unskill_2", "for_total_2",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_2", "for_skill_2", "for_unskill_2", "for_total_2",1)']) !!}
                                                                {!! $errors->first('for_unskill_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_total_2',(isset($alreadyExistApplicant->for_total_2) ? $alreadyExistApplicant->for_total_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_total_2', 'readonly']) !!}
                                                                {!! $errors->first('for_total_2','<span class="help-block">:message</span>') !!}
                                                            </td>

                                                            <td>
                                                                {!! Form::text('loc_man_2',(isset($alreadyExistApplicant->loc_man_2) ? $alreadyExistApplicant->loc_man_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_man_2',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_2", "loc_skill_2", "loc_unskill_2", "loc_total_2",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_2", "loc_skill_2", "loc_unskill_2", "loc_total_2",1)']) !!}
                                                                {!! $errors->first('loc_man_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_skill_2',(isset($alreadyExistApplicant->loc_skill_2) ? $alreadyExistApplicant->loc_skill_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_skill_2',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_2", "loc_skill_2", "loc_unskill_2", "loc_total_2",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_2", "loc_skill_2", "loc_unskill_2", "loc_total_2",1)']) !!}
                                                                {!! $errors->first('loc_skill_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_unskill_2',(isset($alreadyExistApplicant->loc_unskill_2) ? $alreadyExistApplicant->loc_unskill_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_unskill_2',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_2", "loc_skill_2", "loc_unskill_2", "loc_total_2",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_2", "loc_skill_2", "loc_unskill_2", "loc_total_2",1)']) !!}
                                                                {!! $errors->first('loc_unskill_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_total_2',(isset($alreadyExistApplicant->loc_total_2) ? $alreadyExistApplicant->loc_total_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_total_2', 'readonly']) !!}
                                                                {!! $errors->first('loc_total_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('gr_total_2',(isset($alreadyExistApplicant->gr_total_2) ? $alreadyExistApplicant->gr_total_2 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id'=>'gr_total_2', 'readonly']) !!}
                                                                {!! $errors->first('gr_total_2','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                {!! Form::selectYear('mp_year_3',  Date('Y'), Date('Y')+ 5, (isset($alreadyExistApplicant->mp_year_3) ? $alreadyExistApplicant->mp_year_3 : ''),
                                                                ['maxlength'=>'20','class' => 'form-control input-sm required onlyNumber', 'placeholder' => 'Select One']) !!}
                                                                {!! $errors->first('mp_year_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_man_3',(isset($alreadyExistApplicant->for_man_3) ? $alreadyExistApplicant->for_man_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_man_3',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_3", "for_skill_3", "for_unskill_3", "for_total_3",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_3", "for_skill_3", "for_unskill_3", "for_total_3", 1)']) !!}
                                                                {!! $errors->first('for_man_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_skill_3',(isset($alreadyExistApplicant->for_skill_3) ? $alreadyExistApplicant->for_skill_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_skill_3',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_3", "for_skill_3", "for_unskill_3", "for_total_3", 1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_3", "for_skill_3", "for_unskill_3", "for_total_3", 1)']) !!}
                                                                {!! $errors->first('for_skill_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_unskill_3',(isset($alreadyExistApplicant->for_unskill_3) ? $alreadyExistApplicant->for_unskill_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_unskill_3',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_3", "for_skill_3", "for_unskill_3", "for_total_3",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_3", "for_skill_3", "for_unskill_3", "for_total_3",1)']) !!}
                                                                {!! $errors->first('for_unskill_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_total_3',(isset($alreadyExistApplicant->for_total_3) ? $alreadyExistApplicant->for_total_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_total_3', 'readonly']) !!}
                                                                {!! $errors->first('for_total_3','<span class="help-block">:message</span>') !!}
                                                            </td>

                                                            <td>
                                                                {!! Form::text('loc_man_3',(isset($alreadyExistApplicant->loc_man_3) ? $alreadyExistApplicant->loc_man_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_man_3',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_3", "loc_skill_3", "loc_unskill_3", "loc_total_3",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_3", "loc_skill_3", "loc_unskill_3", "loc_total_3",1)']) !!}
                                                                {!! $errors->first('loc_man_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_skill_3',(isset($alreadyExistApplicant->loc_skill_3) ? $alreadyExistApplicant->loc_skill_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_skill_3',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_3", "loc_skill_3", "loc_unskill_3", "loc_total_3",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_3", "loc_skill_3", "loc_unskill_3", "loc_total_3",1)']) !!}
                                                                {!! $errors->first('loc_skill_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_unskill_3',(isset($alreadyExistApplicant->loc_unskill_3) ? $alreadyExistApplicant->loc_unskill_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_unskill_3',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_3", "loc_skill_3", "loc_unskill_3", "loc_total_3",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_3", "loc_skill_3", "loc_unskill_3", "loc_total_3",1)']) !!}
                                                                {!! $errors->first('loc_unskill_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_total_3',(isset($alreadyExistApplicant->loc_total_3) ? $alreadyExistApplicant->loc_total_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_total_3', 'readonly']) !!}
                                                                {!! $errors->first('loc_total_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('gr_total_3',(isset($alreadyExistApplicant->gr_total_3) ? $alreadyExistApplicant->gr_total_3 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id'=>'gr_total_3', 'readonly']) !!}
                                                                {!! $errors->first('gr_total_3','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                {!! Form::selectYear('mp_year_4',  Date('Y'), Date('Y')+ 5, (isset($alreadyExistApplicant->mp_year_4) ? $alreadyExistApplicant->mp_year_4 : ''),
                                                                ['maxlength'=>'20','class' => 'form-control input-sm required onlyNumber', 'placeholder' => 'Select One']) !!}
                                                                {!! $errors->first('mp_year_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_man_4',(isset($alreadyExistApplicant->for_man_4) ? $alreadyExistApplicant->for_man_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_man_4',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_4", "for_skill_4", "for_unskill_4", "for_total_4",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_4", "for_skill_4", "for_unskill_4", "for_total_4", 1)']) !!}
                                                                {!! $errors->first('for_man_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_skill_4',(isset($alreadyExistApplicant->for_skill_4) ? $alreadyExistApplicant->for_skill_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_skill_4',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_4", "for_skill_4", "for_unskill_4", "for_total_4", 1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_4", "for_skill_4", "for_unskill_4", "for_total_4", 1)']) !!}
                                                                {!! $errors->first('for_skill_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_unskill_4',(isset($alreadyExistApplicant->for_unskill_4) ? $alreadyExistApplicant->for_unskill_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_unskill_4',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_4", "for_skill_4", "for_unskill_4", "for_total_4",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_4", "for_skill_4", "for_unskill_4", "for_total_4",1)']) !!}
                                                                {!! $errors->first('for_unskill_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_total_4',(isset($alreadyExistApplicant->for_total_4) ? $alreadyExistApplicant->for_total_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_total_4', 'readonly']) !!}
                                                                {!! $errors->first('for_total_4','<span class="help-block">:message</span>') !!}
                                                            </td>

                                                            <td>
                                                                {!! Form::text('loc_man_4',(isset($alreadyExistApplicant->loc_man_4) ? $alreadyExistApplicant->loc_man_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_man_4',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_4", "loc_skill_4", "loc_unskill_4", "loc_total_4",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_4", "loc_skill_4", "loc_unskill_4", "loc_total_4",1)']) !!}
                                                                {!! $errors->first('loc_man_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_skill_4',(isset($alreadyExistApplicant->loc_skill_4) ? $alreadyExistApplicant->loc_skill_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_skill_4',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_4", "loc_skill_4", "loc_unskill_4", "loc_total_4",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_4", "loc_skill_4", "loc_unskill_4", "loc_total_4",1)']) !!}
                                                                {!! $errors->first('loc_skill_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_unskill_4',(isset($alreadyExistApplicant->loc_unskill_4) ? $alreadyExistApplicant->loc_unskill_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_unskill_4',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_4", "loc_skill_4", "loc_unskill_4", "loc_total_4",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_4", "loc_skill_4", "loc_unskill_4", "loc_total_4",1)']) !!}
                                                                {!! $errors->first('loc_unskill_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_total_4',(isset($alreadyExistApplicant->loc_total_4) ? $alreadyExistApplicant->loc_total_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_total_4', 'readonly']) !!}
                                                                {!! $errors->first('loc_total_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('gr_total_4',(isset($alreadyExistApplicant->gr_total_4) ? $alreadyExistApplicant->gr_total_4 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id'=>'gr_total_4', 'readonly']) !!}
                                                                {!! $errors->first('gr_total_4','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                {!! Form::selectYear('mp_year_5',  Date('Y'), Date('Y')+ 5, (isset($alreadyExistApplicant->mp_year_5) ? $alreadyExistApplicant->mp_year_5 : ''),
                                                                ['maxlength'=>'20','class' => 'form-control input-sm required onlyNumber', 'placeholder' => 'Select One']) !!}
                                                                {!! $errors->first('mp_year_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_man_5',(isset($alreadyExistApplicant->for_man_5) ? $alreadyExistApplicant->for_man_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_man_5',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_5", "for_skill_5", "for_unskill_5", "for_total_5",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_5", "for_skill_5", "for_unskill_5", "for_total_5", 1)']) !!}
                                                                {!! $errors->first('for_man_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_skill_5',(isset($alreadyExistApplicant->for_skill_5) ? $alreadyExistApplicant->for_skill_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_skill_5',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_5", "for_skill_5", "for_unskill_5", "for_total_5", 1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_5", "for_skill_5", "for_unskill_5", "for_total_5", 1)']) !!}
                                                                {!! $errors->first('for_skill_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_unskill_5',(isset($alreadyExistApplicant->for_unskill_5) ? $alreadyExistApplicant->for_unskill_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_unskill_5',
                                                                'onKeyUp' => 'Calculate3Numbers("for_man_5", "for_skill_5", "for_unskill_5", "for_total_5",1)',
                                                                'onblur' => 'Calculate3Numbers("for_man_5", "for_skill_5", "for_unskill_5", "for_total_5",1)']) !!}
                                                                {!! $errors->first('for_unskill_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('for_total_5',(isset($alreadyExistApplicant->for_total_5) ? $alreadyExistApplicant->for_total_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'for_total_5', 'readonly']) !!}
                                                                {!! $errors->first('for_total_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_man_5',(isset($alreadyExistApplicant->loc_man_5) ? $alreadyExistApplicant->loc_man_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_man_5',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_5", "loc_skill_5", "loc_unskill_5", "loc_total_5",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_5", "loc_skill_5", "loc_unskill_5", "loc_total_5",1)']) !!}
                                                                {!! $errors->first('loc_man_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_skill_5',(isset($alreadyExistApplicant->loc_skill_5) ? $alreadyExistApplicant->loc_skill_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_skill_5',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_5", "loc_skill_5", "loc_unskill_5", "loc_total_5",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_5", "loc_skill_5", "loc_unskill_5", "loc_total_5",1)']) !!}
                                                                {!! $errors->first('loc_skill_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_unskill_5',(isset($alreadyExistApplicant->loc_unskill_5) ? $alreadyExistApplicant->loc_unskill_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_unskill_5',
                                                                'onKeyUp' => 'Calculate3Numbers("loc_man_5", "loc_skill_5", "loc_unskill_5", "loc_total_5",1)',
                                                                'onblur' => 'Calculate3Numbers("loc_man_5", "loc_skill_5", "loc_unskill_5", "loc_total_5",1)']) !!}
                                                                {!! $errors->first('loc_unskill_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('loc_total_5',(isset($alreadyExistApplicant->loc_total_5) ? $alreadyExistApplicant->loc_total_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id' => 'loc_total_5', 'readonly']) !!}
                                                                {!! $errors->first('loc_total_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>
                                                                {!! Form::text('gr_total_5',(isset($alreadyExistApplicant->gr_total_5) ? $alreadyExistApplicant->gr_total_5 : ''),
                                                                ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber', 'id'=>'gr_total_5', 'readonly']) !!}
                                                                {!! $errors->first('gr_total_5','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>15. Cost of Production </strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="productionCostTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                        <thead class="alert alert-info">
                                                        <tr>
                                                            <th valign="top" class="text-center valigh-middle">Total production cost per unit (in US$)
                                                                <span class="required-star"></span><br/>
                                                            </th>
                                                            <th colspan="2" valign="top" class="text-center valigh-top">
                                                                <table class="table table-striped table-bordered dt-responsive">
                                                                    <thead class="alert alert-info">
                                                                    <tr class="text-center">
                                                                        <th colspan="2" valign="top" class="text-center valigh-top">Raw Materials per annum (in US$)</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center valigh-top">From Bangladesh <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top">From other countries <span class="required-star"></span></th>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                            <th colspan="2" valign="top" class="text-center valigh-top">
                                                                <table class="table table-striped table-bordered dt-responsive">
                                                                    <thead class="alert alert-info">
                                                                    <tr class="text-center">
                                                                        <th colspan="2" valign="top" class="text-center valigh-top">Packaging Materials per annum (in US$)</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center valigh-top">From Bangladesh <span class="required-star"></span></th>
                                                                        <th class="text-center valigh-top">From other countries <span class="required-star"></span></th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                            <th class="valigh-middle text-center"><span class="hashs">#</span></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(count($clr_product_cost) > 0)
                                                            <?php $inc = 0; ?>
                                                            @foreach($clr_product_cost as $eachProCost)
                                                                <tr id="rowProCostCount{{$inc}}">
                                                                    <td>
                                                                        {!! Form::hidden("pro_cost_id[$inc]", $eachProCost->id) !!}
                                                                        {!! Form::text("production_cost[$inc]",$eachProCost->production_cost, ['maxlength'=>'100',
                                                                        'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                        {!! $errors->first('production_cost','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("raw_cost_bd[$inc]",$eachProCost->raw_cost_bd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                        {!! $errors->first('raw_cost_bd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("raw_cost_other[$inc]",$eachProCost->raw_cost_other, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                        {!! $errors->first('raw_cost_other','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! Form::text("pac_cost_bd[$inc]",$eachProCost->pac_cost_bd, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                        {!! $errors->first('pac_cost_bd','<span class="help-block">:message</span>') !!}
                                                                    </td>
                                                                    <td>{!! Form::text("pac_cost_other[$inc]",$eachProCost->pac_cost_other, ['maxlength'=>'20',
                                                                        'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                        {!! $errors->first('pac_cost_other','<span class="help-block">:message</span>') !!}
                                                                    </td>

                                                                    <td>
                                                                        <?php if ($inc == 0) { ?>
                                                                        <a class="btn btn-xs btn-primary addTableRows"
                                                                           onclick="addTableRow('productionCostTbl', 'rowProCostCount0');"><i class="fa fa-plus"></i></a>
                                                                        <?php } else { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow"
                                                                           onclick="removeTableRow('productionCostTbl','rowProCostCount{{$inc}}');">
                                                                            <i class="fa fa-times" aria-hidden="true"></i></a>
                                                                        <?php } ?>

                                                                    </td>
                                                                </tr>
                                                                <?php $inc++; ?>
                                                            @endforeach
                                                        @else
                                                            <tr id="rowProCostCount">
                                                                <td>
                                                                    {!! Form::text('production_cost[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('production_cost','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('raw_cost_bd[0]','', ['maxlength'=>'100', 'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('raw_cost_bd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('raw_cost_other[0]','', ['maxlength'=>'100', 'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('raw_cost_other','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('pac_cost_bd[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('pac_cost_bd','<span class="help-block">:message</span>') !!}
                                                                </td>
                                                                <td>{!! Form::text('pac_cost_other[0]','', ['maxlength'=>'100','class' => 'form-control input-sm required onlyNumber']) !!}
                                                                    {!! $errors->first('pac_cost_other','<span class="help-block">:message</span>') !!}
                                                                </td>

                                                                <td>
                                                                    <a class="btn btn-xs btn-primary addTableRows" onclick="addTableRow('productionCostTbl', 'rowProCostCount');">
                                                                        <i class="fa fa-plus"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>16. Sales Revenue (at maximum capacity)</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    {!! Form::label('sales_domestic','Domestic :', ['class'=>'col-md-6 text-left required-star']) !!}
                                                    <div class="col-md-1 text-right">US$</div>
                                                    <div class="col-md-5">
                                                        {!! Form::text('sales_domestic', (isset($alreadyExistApplicant->sales_domestic) ? $alreadyExistApplicant->sales_domestic : ''),
                                                        ['class' => 'form-control input-sm required onlyNumber', 'id' => 'sales_domestic',
                                                        'onKeyUp' => 'Calculate3Numbers("sales_export", "sales_exp_oriented", "sales_domestic", "sales_total")']) !!}
                                                        {!! $errors->first('sales_domestic','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('sales_export','Export :', ['class'=>'col-md-6 text-left required-star']) !!}
                                                    <div class="col-md-1 text-right">US$</div>
                                                    <div class="col-md-5">
                                                        {!! Form::text('sales_export', (isset($alreadyExistApplicant->sales_export) ? $alreadyExistApplicant->sales_export : ''),
                                                        ['class' => 'form-control input-sm required onlyNumber', 'id' => 'sales_export',
                                                        'onKeyUp' => 'Calculate3Numbers("sales_export", "sales_exp_oriented", "sales_domestic", "sales_total")']) !!}
                                                        {!! $errors->first('sales_export','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div><!--/col-md-12-->

                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    {!! Form::label('sales_exp_oriented','Sales to Export Oriented :', ['class'=>'col-md-6 text-left required-star']) !!}
                                                    <div class="col-md-1 text-right">US$</div>
                                                    <div class="col-md-5">
                                                        {!! Form::text('sales_exp_oriented', (isset($alreadyExistApplicant->sales_exp_oriented) ? $alreadyExistApplicant->sales_exp_oriented : ''),
                                                        ['class' => 'form-control input-sm required onlyNumber', 'id' => 'sales_exp_oriented',
                                                        'onKeyUp' => 'Calculate3Numbers("sales_export", "sales_exp_oriented", "sales_domestic", "sales_total")']) !!}
                                                        {!! $errors->first('sales_exp_oriented','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('sales_total','Total :', ['class'=>'col-md-6 text-left required-star']) !!}
                                                    <div class="col-md-1 text-right">US$</div>
                                                    <div class="col-md-5">
                                                        {!! Form::text('sales_total', (isset($alreadyExistApplicant->sales_total) ? $alreadyExistApplicant->sales_total : ''),
                                                        ['class' => 'form-control input-sm required onlyNumber', 'readonly', 'id' => 'sales_total']) !!}
                                                        {!! $errors->first('sales_total','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div><!--/col-md-12-->

                                        </div>
                                    </div>
                                </div><!--/panel-->

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><strong>17. Required Infrastructure</strong></div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="infraReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                <thead class="alert alert-info">
                                                <tr>
                                                    <th>Infrastructure </th>
                                                    <th>Initial Period <span class="required-star"></span></th>
                                                    <th>Regular Operation Period at maximum capacity <span class="required-star"></span></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <label class="text-left" for="land">Land (in M<sup>2</sup>) :</label>
                                                    </td>
                                                    <td>{!! Form::text('land_ini',(isset($alreadyExistApplicant->land_ini) ? $alreadyExistApplicant->land_ini : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('land_ini','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('land_reg',(isset($alreadyExistApplicant->land_reg) ? $alreadyExistApplicant->land_reg : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('land_reg','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {!! Form::label('power','Power (in KW/H) :', ['class' => 'text-left']) !!}
                                                    </td>
                                                    <td>{!! Form::text('power_ini',(isset($alreadyExistApplicant->power_ini) ? $alreadyExistApplicant->power_ini : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('power_ini','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('power_reg',(isset($alreadyExistApplicant->power_reg) ? $alreadyExistApplicant->power_reg : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('power_reg','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="text-left" for="gas">GAS (in M<sup>3</sup>) :</label>
                                                    </td>
                                                    <td>{!! Form::text('gas_ini',(isset($alreadyExistApplicant->gas_ini) ? $alreadyExistApplicant->gas_ini : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('gas_ini','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('gas_reg',(isset($alreadyExistApplicant->gas_reg) ? $alreadyExistApplicant->gas_reg : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('gas_reg','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="text-left" for="water">Water (in M<sup>3</sup>) :</label>
                                                    </td>
                                                    <td>{!! Form::text('water_ini',(isset($alreadyExistApplicant->water_ini) ? $alreadyExistApplicant->water_ini : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('water_ini','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('water_reg',(isset($alreadyExistApplicant->water_reg) ? $alreadyExistApplicant->water_reg : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('water_reg','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="text-left" for="etp">Central ETP (in M<sup>3</sup>) :</label>
                                                    </td>
                                                    <td>{!! Form::text('etp_ini',(isset($alreadyExistApplicant->etp_ini) ? $alreadyExistApplicant->etp_ini : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('etp_ini','<span class="help-block">:message</span>') !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('etp_reg',(isset($alreadyExistApplicant->etp_reg) ? $alreadyExistApplicant->etp_reg : ''),
                                                        ['maxlength'=>'40','class' => 'form-control input-sm required onlyNumber']) !!}
                                                        {!! $errors->first('etp_reg','<span class="help-block">:message</span>') !!}

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading"><b>18. Required Documents for attachment</b></div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover ">
                                                <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th colspan="6">Required Attachments</th>
                                                    <th colspan="2">Attached PDF file
                                                        <span onmouseover="toolTipFunction()" data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 3MB)!">
                                                            <i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1; ?>
                                                @foreach($document as $row)
                                                    <tr>
                                                        <td><div align="center">{!! $i !!}</div></td>
                                                        <td colspan="6">{!!  $row->doc_name !!}<?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></td>
                                                        <td colspan="2">
                                                            <input type="hidden"  name="document_id_<?php echo $row->doc_id; ?>"
                                                                   value="{{(!empty($clrDocuments[$row->doc_id]['doucument_id']) ? $clrDocuments[$row->doc_id]['doucument_id'] : '')}}">
                                                            <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>"
                                                                   name="doc_name_<?php echo $row->doc_id; ?>" />

                                                            <input name="file<?php echo $row->doc_id; ?>"   id="file<?php echo $row->doc_id; ?>" type="file" size="20"
                                                            <?php if (empty($clrDocuments[$row->doc_id]['file']) && $row->doc_priority == "1"){ echo "class='required'"; } ?>
                                                                   onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php
                                                                   echo $row->doc_id; ?>', '<?php echo $row->doc_priority; ?>')"/>

                                                            @if($row->additional_field == 1)
                                                                <table>
                                                                    <tr>
                                                                        <td>Other file Name : </td>
                                                                        <td> <input maxlength="64" class="form-control input-sm <?php if ($row->doc_priority == "1"){ echo 'required'; } ?>"
                                                                                    name="other_doc_name_<?php echo $row->doc_id; ?>" type="text" value="{{(!empty($clrDocuments[$row->doc_id]['doc_name']) ? $clrDocuments[$row->doc_id]['doc_name'] : '')}}"></td>
                                                                    </tr>
                                                                </table>
                                                            @endif

                                                            @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                                <div class="save_file saved_file_{{$row->doc_id}}">
                                                                    <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ?
                                                                    $clrDocuments[$row->doc_id]['file'] : ''))}}" title="{{$row->doc_name}}">
                                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name); ?></a>

                                                                    <?php if(!empty($alreadyExistApplicant) && Auth::user()->id == $alreadyExistApplicant->created_by && $viewMode != 'on') {?>


                                                                    <a href="javascript:void(0)" onclick="ConfirmDeleteFile({{ $row->doc_id }})">
                                                                        <span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span>
                                                                    </a>
                                                                    <?php } ?>
                                                                </div>
                                                            @endif

                                                            <div id="preview_<?php echo $row->doc_id; ?>">
                                                                <input type="hidden" value="<?php echo !empty($clrDocuments[$row->doc_id]['file']) ?
                                                                        $clrDocuments[$row->doc_id]['file'] : ''?>" id="validate_field_<?php echo $row->doc_id; ?>"
                                                                       name="validate_field_<?php echo $row->doc_id; ?>" class="<?php echo $row->doc_priority == "1" ? "required":'';  ?>"  />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div><!-- /.table-responsive -->
                                        <?php if ($viewMode == "on") { ?>

                                        @if(in_array(Auth::user()->desk_id,array(3,4,5,6)) || Auth::user()->user_type == '1x101')

                                            <div class="row">
                                                <div class="col-md-12"><br/></div>
                                                <div class="col-md-12" style="margin-bottom:6px;">
                                                    <a href="{{ url('project-clearance/view-cer/'. Encryption::encodeId($alreadyExistApplicant->created_by))}}"
                                                       target="_blank" class="btn btn-warning btn-xs pull-left show-in-view ">
                                                        <i class="fa fa-download"></i> <strong>Associated Certificates of Users</strong>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif {{-- checking if RD desks or system admin --}}

                                        @include('projectClearance::doc-tab')
                                        <?php } ?>

                                    </div><!-- /.panel-body -->
                                </div>

                            </fieldset>

                            <h3 class="stepHeader">Submit</h3>
                            <fieldset>
                                <div class="panel panel-primary hiddenDiv">
                                    <div class="panel-heading"><strong>19. Terms and Conditions</strong></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12" style="margin: 12px 0;">
                                                <input id="acceptTerms-2" @if(isset($alreadyExistApplicant->acceptance_of_terms) && $alreadyExistApplicant->acceptance_of_terms == 1) checked @endif name="acceptTerms" type="checkbox" class="required col-md-1 text-left" style="width:3%;">
                                                <label for="acceptTerms-2" class="col-md-11 text-left required-star">I agree with the Terms and Conditions.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <?php if ($viewMode != 'on') { ?>

                            <?php $statusId = (isset($alreadyExistApplicant->status_id) ? $alreadyExistApplicant->status_id : '');
                            if ($statusId == -1 || $statusId == '') { ?>
                            @if(ACL::getAccsessRight('projectClearance','A'))
                                <input type="submit" class="btn btn-primary btn-md cancel" value="Save As Draft" name="sv_draft">
                            @endif
                            <?php } else { ?>
                            &nbsp;
                            <?php } // end of checking status id ?>
                            {!! Form::close() !!}<!-- /.form end -->
                            <?php } // end of checking if view mode is on ?>
                        </div>

                        <?php if ($viewMode == "on" && in_array(Auth::user()->user_type, array('1x101', '2x202', '4x404'))) { ?>
                        @include('projectClearance::apps_history')
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer-script')
    @if($viewMode !== 'on')
        <link rel="stylesheet" href="{{ url('assets/css/jquery.steps.css') }}">
        <script src="{{ asset("assets/scripts/jquery.steps.js") }}"></script>
    @endif

    <script type="text/javascript">
        @if ($viewMode !== 'on')

        

        $(document).ready(function(){
           $(document.body).on('blur','.max_hundred',function(){
               var range = $(this).val();
                if(range>100){
                    $(this).val(100);
                }
           });
        });

        function SumArguments() {
            var _arguments = arguments;
            this.sum = function() {
                var i = _arguments.length;
                var result = 0;
                while (i--) {
                    result += _arguments[i];
                }
                return result;
            };
        }

        function Calculate2Numbers(arg1, arg2, place,messArea) {
            var no1 = $('#'+arg1).val() ? parseFloat($('#'+arg1).val()) : 0;
            if(no1 > 100) $('#'+arg1).addClass("error");
            var no2 = $('#'+arg2).val() ? parseFloat($('#'+arg2).val()) : 0;
            if(no2 > 100) $('#'+arg2).addClass("error");
            var total = new SumArguments(no1, no2);
            $('#'+ place).val(total.sum());
            if(total.sum() != 100){
                $("#"+place).addClass("error");
                $("#"+messArea).show().text("Should be 100%");
            } else {
                $("#"+place).removeClass("error");
                $("#"+messArea).hide().html('');
            }

        }

        function Calculate3Numbers(arg1, arg2, arg3, place, extra) {
            var no1 = $('#'+arg1).val() ? parseFloat($('#'+arg1).val()) : 0;
            var no2 = $('#'+arg2).val() ? parseFloat($('#'+arg2).val()) : 0;
            var no3 = $('#'+arg3).val() ? parseFloat($('#'+arg3).val()) : 0;
            var total = new SumArguments(no1, no2,no3);

            $('#'+ place).val(total.sum());

            /********This parameter 1 will be  used only when calculating grand total of Manpower div ******/
            if(extra == 1){
                var res = place.split("_");
                var totalField_Id = res[2];
                var fnT = $('#for_total_'+totalField_Id).val() ? parseFloat($('#for_total_'+totalField_Id).val()) : 0;
                var lcT = $('#loc_total_'+totalField_Id).val() ? parseFloat($('#loc_total_'+totalField_Id).val()) : 0;
                $("#gr_total_"+totalField_Id).val(fnT+lcT);
            }
        }

        function editAuthorizeInfo() {
            $('#first_step_authorize_info').find('input.form-control,select.form-control').attr('readonly', function (i, v) {
                //$('#first_step_authorize_info').find('select.form-control').attr('disabled',!v);//for select box disabled
                return !v;

            });
        };
        <?php if (empty($alreadyExistApplicant->same_as_authorized)) { ?>
        $('input[name="same_as_authorized"]').trigger('click');
        <?php } ?>

        function in_joint_com(sel) {
            var thisVal = sel;
            if (thisVal !== '' && thisVal === 'Joint Venture') {
                $('.type_of_organizations_main').show();
                //$('.add_new_span').html('<a class="btn btn-xs btn-primary addTableRows" onclick=addTableRow("type_of_organizations","joinRows0");><i class="fa fa-plus"></i></a>');
            } else {
                //$('.add_new_span').html('-');
                $('.type_of_organizations_main').hide();
                $('.otherJointRows[id!="joinRows0"]').remove();
                $('#joinRows0').find('input[type="text"]').val('');
                $('#joinRows0').find('select').prop('selectedIndex', 0);
            }
        }

        function uploadDocument(targets, id, vField, isRequired) {
            var inputFile = $("#" + id).val();
            if (inputFile == ''){
                $("#" + id).html('');
                document.getElementById("isRequired").value = '';
                document.getElementById("selected_file").value = '';
                document.getElementById("validateFieldName").value = '';
                document.getElementById(targets).innerHTML = '<input type="hidden" class="required" value="" id="' + vField + '" name="' + vField + '">';
                if ($('#label_' + id).length) $('#label_' + id).remove();
                return false;
            }

            try{
                document.getElementById("isRequired").value = isRequired;
                document.getElementById("selected_file").value = id;
                document.getElementById("validateFieldName").value = vField;
                document.getElementById(targets).style.color = "red";
                var action = "{{url('/project-clearance/upload-document')}}";
                $("#" + targets).html('Uploading....');
                var file_data = $("#" + id).prop('files')[0];
                var form_data = new FormData();
                form_data.append('selected_file', id);
                form_data.append('isRequired', isRequired);
                form_data.append('validateFieldName', vField);
                form_data.append('_token', "{{ csrf_token() }}");
                form_data.append(id, file_data);
                $.ajax({
                    target: '#' + targets,
                    url:action,
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        $('#' + targets).html(response);
                        var fileNameArr = inputFile.split("\\");
                        var l = fileNameArr.length;
                        if ($('#label_' + id).length)
                            $('#label_' + id).remove();
                        var doc_id = parseInt(id.substring(4));
                        var newInput = $('<label class="saved_file_'+doc_id+'" id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + ' <a href="javascript:void(0)" onclick="EmptyFile('+ doc_id +')"><span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span> </a></b></label>');
                        //var newInput = $('<label id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                        $("#" + id).after(newInput);
                        //check valid data
                        var validate_field = $('#' + vField).val();
                        if (validate_field == ''){
                            document.getElementById(id).value = '';
                        }
                    }
                });
            } catch (err) {
                document.getElementById(targets).innerHTML = "Sorry! Something went wrong... Please try again.";
            }
        } // end of uploadDocument function

        $(document).ready(function () {
            $('#organization_type').trigger('change');
            $('.m_purchase_state').trigger('change');
            var form = $("#appClearenceForm").show();
            form.steps({
                headerTag: "h3",
                bodyTag: "fieldset",
                transitionEffect: "slideLeft",
                onStepChanging: function (event, currentIndex, newIndex) {

                    // Allways allow previous action even if the current form is not valid!
                    if(newIndex == 2){
                        var company_logo_control = document.getElementById("company_logo");
                        var file_companyLogo = company_logo_control.files;
                        if (file_companyLogo && file_companyLogo[0]) {
                            $("#company_logo_err").html('');
                            var mime_type = file_companyLogo[0].type;
                            if(!(mime_type=='image/jpeg' || mime_type=='image/jpg' || mime_type=='image/png')){
                                $("#company_logo_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                                return false;
                            }
                        }

                        var no1 = $('#paid_cap_amount').val() ? parseFloat($('#paid_cap_amount').val()) : 0;
                        var no2 = $('#paid_cap_nature').val() ? parseFloat($('#paid_cap_nature').val()) : 0;
                        var total = no1 + no2;
                        if(total  != 100){
                            $("#paid_cap_percentage").addClass("error");
                            $("#paid_cap_percentage_error").show().text("Should be 100%");
                            return false;
                        } else {
                            $("#paid_cap_percentage").removeClass("error");
                            $("#paid_cap_percentage_error").hide().text('');
                        }
                        // To validation that shareholder percentage is 100%
                        var shareholder_percentage = 0;
                        $('.countValue').each(function () {
                            var value = $(this).val();
                            if (!isNaN(value) && value.length !== 0) {
                                shareholder_percentage += parseFloat(value);
                            }
                        });
                        if(shareholder_percentage != 100){
                            $("#share_holder_percentage_error").show().text("Should be 100% in total");
                            return false;
                        } else {
                            $("#share_holder_percentage_error").hide().text('');
                        }
                    }

                    if (currentIndex > newIndex)
                    {
                        return true;
                    }
                    // Forbid next action on "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age-2").val()) < 18)
                    {
                        return false;
                    }
                    // Needed in some cases if the user went back (clean up)
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        form.find(".body:eq(" + newIndex + ") label.error").remove();
                        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                    }
                    form.validate().settings.ignore = ":disabled,:hidden";
                    //return true;
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex) {

                    // Used to skip the "Warning" step if the user is old enough.
                    /*if (currentIndex === 2 && Number($("#age-2").val()) >= 18){
                     form.steps("next");
                     }
                     // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
                     if (currentIndex === 2 && priorIndex === 3){
                     //form.steps("previous");
                     }*/
                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    //alert("Submitted!");
                }
            });

            var popupWindow = null;
            $('.finish').on('click', function (e) {

                if ($('#acceptTerms-2').is(":checked")){
                    $('#acceptTerms-2').removeClass('error');
                    $('#acceptTerms-2').next('label').css('color', 'black');
                    $('#home').css({"display": "none"});
                    popupWindow = window.open('<?php echo URL::to('/project-clearance/preview'); ?>', 'Sample', '');
                } else {
                    $('#acceptTerms-2').addClass('error');
                    return false;
                }
            });

            var today = new Date();
            var yyyy = today.getFullYear();
            var mm = today.getMonth();
            var dd = today.getDate();
            $('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: '01/01/'+(yyyy+5),
                minDate: '01/01/'+(yyyy-50)
            });

            $('.construction_start').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                minDate: '01/01/' + (yyyy - 6)
            });
            $('.construction_end').datetimepicker({
                format: 'DD-MMM-YYYY',
                useCurrent: false // Important! See issue #1075
            });


            startDateSelected = '';
            $(".construction_start").on("dp.change", function (e) {
//                $('#construction_end').data("DateTimePicker").minDate(e.date);
                var nextDate = e.date.add(1, 'day');
                $('.construction_end').data("DateTimePicker").minDate(nextDate);
                startDateSelected = nextDate;
            });
            $(".construction_end").on("dp.change", function (e) {
                var startDateVal = $("#construction_start").val();
                var day = moment(startDateVal, ['DD-MMM-YYYY','YYYY-MM-DD']);
                var startDate = moment(day).add(1, 'day');
                if(startDateVal !=''){
                    $('.construction_end').data("DateTimePicker").minDate(startDate);
                }
                var endDate = moment($("#construction_end").val()).add(1, 'day');
                var endDateMoment = moment(endDate, ['DD-MMM-YYYY','YYYY-MM-DD']);
                var endDateVal = $("#construction_end").val();
                var dayEnd = moment(endDateVal, ['DD-MMM-YYYY','YYYY-MM-DD']);
                var endDate = moment(dayEnd).add(1, 'day');

                //var startDate = startDateSelected;
                //var endDate = e.date.add(1, 'day');
                if (startDate != '' && endDate != '') {
                    var days = (endDate - startDate) / 1000 / 60 / 60 / 24;
                    $('#construction_duration').val(Math.floor(days));
                }
            });

            $('.construction_end').trigger('dp.change');


            $("input[type=radio]").click(function () {
                var id = $(this).attr("id");
                var thename = $(this).attr("name");
                $("input[name=" + thename + "]").removeAttr('checked');
                $("#" + id).attr('checked', "checked");
            });
            $("select").change(function () {
                var id = $(this).attr("id");
                var val = $(this).val();
                $(this).find('option').removeAttr("selected");
                if (val != '') {
                    $(this).find('option[value="' + val + '"]').attr('selected', 'selected');
                    $(this).val(val);
                }
            });
            $("#change_nid").change(function () {
                var id = $(this).val();
                if(id==1){
                    $('#nid_show').show();
                    $('#correspondent_passport_show').hide();
                }else if(id==2){
                    $('#correspondent_passport_show').show();
                    $('#nid_show').hide();
                }
                else{
                    $('#correspondent_passport_show').hide();
                    $('#nid_show').hide();
                }
            });
            $('#change_nid').trigger('change');

//            var passport= $('#correspondent_passport').val();
//            if(passport){
//                $('#nid_show').hide();
//            }
//            var nid= $('#correspondent_nid').val();
//            if(nid){
//                $('#correspondent_passport_show').hide();
//            }
//            if(nid && passport){
//                $('#correspondent_passport_show').show();
//            }


        }); // end of document.ready

        function countValue(thisItem){
            var sum=0;
            $('.countValue').each(function () {
                var value = $(this).val();
                if (!isNaN(value) && value.length !== 0) {
                    sum += parseFloat(value);
                }
            });
            if(sum == 100){
                $('.countValue').each(function () {
                    var value = $(this).val();
                    if (value.length == '') {
                        $(this).val(0).prop('readonly',true);
                    }
                });
                $('#share_holder_percentage_error').text('');
            } else{
                $('#share_holder_percentage_error').text('Should be 100% in total');
            }
        }
        function addTableRow(tableID, templateRow){
            //rowCount++;
            //Direct Copy a row to many times
            var x = document.getElementById(templateRow).cloneNode(true);
            x.id = "";
            x.style.display = "";
            var table = document.getElementById(tableID);
            var rowCount = $('#' + tableID).find('tr').length - 1;
            var lastTr = $('#' + tableID).find('tr').last().attr('data-number');
            var production_desc_val = $('#' + tableID).find('tr').last().find('.production_desc_1st').val();
            if (lastTr != '' && typeof lastTr !== "undefined"){
                rowCount = parseInt(lastTr) + 1;
            }
            //var rowCount = table.rows.length;
            //Increment id
            var rowCo = rowCount;
            var idText = 'rowCount'+tableID + rowCount;
            x.id = idText;
            $("#" + tableID).append(x);
            //get select box elements
            var attrSel = $("#" + tableID).find('#' + idText).find('select');
            //edited by ishrat to solve select box id auto increment related bug
            for (var i = 0; i < attrSel.length; i++){
                var nameAtt = attrSel[i].name;
                var repText = nameAtt.replace('[0]', '[' + rowCo + ']'); //increment all array element name
                attrSel[i].name = repText;
            }
            attrSel.val(''); //value reset
            // end of  solving issue related select box id auto increment related bug by ishrat

            //get input elements
            var attrInput = $("#" + tableID).find('#' + idText).find('input');
            for (var i = 0; i < attrInput.length; i++){
                var nameAtt = attrInput[i].name;
                //increment all array element name
                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                attrInput[i].name = repText;
            }
            attrInput.val(''); //value reset
            //edited by ishrat to solve textarea id auto increment related bug
            //get textarea elements
            var attrTextarea = $("#" + tableID).find('#' + idText).find('textarea');
            for (var i = 0; i < attrTextarea.length; i++){
                var nameAtt = attrTextarea[i].name;
                //increment all array element name
                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                attrTextarea[i].name = repText;
                $('#' + idText).find('.readonlyClass').prop('readonly',true);
            }
            attrTextarea.val(''); //value reset
            // end of  solving issue related textarea id auto increment related bug by ishrat
            attrSel.prop('selectedIndex', 0);
            if((tableID==='machinaryTbl' && templateRow==='rowMachineCount0') || (tableID==='machinaryTbl' && templateRow==='rowMachineCount') ){
                $("#" + tableID).find('#' + idText).find('select.m_currency').val("107");  //selected index reset
            }else{
                attrSel.prop('selectedIndex', 0);  //selected index reset
            }
            //$('.m_currency ').prop('selectedIndex', 102);
            //Class change by btn-danger to btn-primary
            $("#" + tableID).find('#' + idText).find('.addTableRows').removeClass('btn-primary').addClass('btn-danger')
                    .attr('onclick', 'removeTableRow("' + tableID + '","' + idText + '")');
            $("#" + tableID).find('#' + idText).find('.addTableRows > .fa').removeClass('fa-plus').addClass('fa-times');
            $('#' + tableID).find('tr').last().attr('data-number', rowCount);

            $("#" + tableID).find('#' + idText).find('.onlyNumber').on('keydown', function (e) {
                //period decimal
                if ((e.which >= 48 && e.which <= 57)
                            //numpad decimal
                        || (e.which >= 96 && e.which <= 105)
                            // Allow: backspace, delete, tab, escape, enter and .
                        || $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1
                            // Allow: Ctrl+A
                        || (e.keyCode == 65 && e.ctrlKey === true)
                            // Allow: Ctrl+C
                        || (e.keyCode == 67 && e.ctrlKey === true)
                            // Allow: Ctrl+V
                        || (e.keyCode == 86 && e.ctrlKey === true)
                            // Allow: Ctrl+X
                        || (e.keyCode == 88 && e.ctrlKey === true)
                            // Allow: home, end, left, right
                        || (e.keyCode >= 35 && e.keyCode <= 39))
                {
                    var thisVal = $(this).val();
                    if (thisVal.indexOf(".") != -1 && e.key == '.') {
                        return false;
                    }
                    $(this).removeClass('error');
                    return true;
                }
                else
                {
                    $(this).addClass('error');
                    return false;
                }
            });

            var productionPrgAddRow = $("#" + tableID).find('.productionPrgAddRow').hasClass('productionPrgAddRow');
            if(productionPrgAddRow === true){
                var clonedProjectionExport = document.getElementById('rowProExportCount0').cloneNode(true);
                clonedProjectionExport.id = "";
                clonedProjectionExport.style.display = "";

                var rowCount = $('#proExportTbl').find('tr').length - 1;
                var lastTr = $('#proExportTbl').find('tr').last().attr('data-number');
                if (lastTr != '' && typeof lastTr !== "undefined"){
                    rowCount = parseInt(lastTr) + 1;
                }
                //Increment id
                var rowCo = rowCount;
                var idText = 'rowCountproExportTbl' + rowCount;
                clonedProjectionExport.id = idText;
                $('#proExportTbl').append(clonedProjectionExport);


                var attrSel = $("#proExportTbl").find('#' + idText).find('select');
                //edited by ishrat to solve select box id auto increment related bug
                for (var i = 0; i < attrSel.length; i++){
                    var nameAtt = attrSel[i].name;
                    var repText = nameAtt.replace('[0]', '[' + rowCo + ']'); //increment all array element name
                    attrSel[i].name = repText;
                }
                attrSel.val(''); //value reset
                // end of  solving issue related select box id auto increment related bug by ishrat

                //get input elements
                var attrInput = $("#proExportTbl").find('#' + idText).find('input');
                for (var i = 0; i < attrInput.length; i++){
                    var nameAtt = attrInput[i].name;
                    //increment all array element name
                    var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                    attrInput[i].name = repText;
                }
                attrInput.val(''); //value reset
                //edited by ishrat to solve textarea id auto increment related bug
                //get textarea elements
                var attrTextarea = $("#proExportTbl").find('#' + idText).find('textarea');
                for (var i = 0; i < attrTextarea.length; i++){
                    var nameAtt = attrTextarea[i].name;
                    //increment all array element name
                    var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                    attrTextarea[i].name = repText;
                    $('#' + idText).find('.readonlyClass').prop('readonly',true);
                }
                attrTextarea.val(''); //value reset
                // end of  solving issue related textarea id auto increment related bug by ishrat

                attrSel.prop('selectedIndex', 0);  //selected index reset
                //Class change by btn-danger to btn-primary
                $("#proExportTbl").find('#' + idText).find('.addTableRows').removeClass('btn-primary').addClass('btn-danger')
                        .attr('onclick', 'removeTableRow("proExportTbl","' + idText + '")');
                $("#proExportTbl").find('#' + idText).find('.addTableRows > .fa').removeClass('fa-plus').addClass('fa-times');
                $("#proExportTbl").find('tr').last().attr('data-number', rowCount);

                $("#proExportTbl").find('#' + idText).find('.onlyNumber').on('keydown', function (e) {
                    //period decimal
                    if ((e.which >= 48 && e.which <= 57)
                                //numpad decimal
                            || (e.which >= 96 && e.which <= 105)
                                // Allow: backspace, delete, tab, escape, enter and .
                            || $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1
                                // Allow: Ctrl+A
                            || (e.keyCode == 65 && e.ctrlKey === true)
                                // Allow: Ctrl+C
                            || (e.keyCode == 67 && e.ctrlKey === true)
                                // Allow: Ctrl+V
                            || (e.keyCode == 86 && e.ctrlKey === true)
                                // Allow: Ctrl+X
                            || (e.keyCode == 88 && e.ctrlKey === true)
                                // Allow: home, end, left, right
                            || (e.keyCode >= 35 && e.keyCode <= 39))
                    {
                        var thisVal = $(this).val();
                        if (thisVal.indexOf(".") != -1 && e.key == '.') {
                            return false;
                        }
                        $(this).removeClass('error');
                        return true;
                    }
                    else
                    {
                        $(this).addClass('error');
                        return false;
                    }
                });

                $("#proExportTbl").find('#' + idText).find('.pro_ext_desc_1st').val(production_desc_val);


                var clonedProjectionDomestic = document.getElementById('rowProDomesticCount0').cloneNode(true);
                clonedProjectionDomestic.id = "";
                clonedProjectionDomestic.style.display = "";
                var rowCountt = $('#proDomesticTbl').find('tr').length - 1;
                var lastTrr = $('#proDomesticTbl').find('tr').last().attr('data-number');
                if (lastTrr != '' && typeof lastTrr !== "undefined"){
                    rowCount = parseInt(lastTrr) + 1;
                }

                //Increment id
                var rowCoo = rowCountt;
                var idTextt = 'rowCountproDomesticTbl' + rowCountt;
                clonedProjectionDomestic.id = idTextt;
                $('#proDomesticTbl').append(clonedProjectionDomestic);

                var attrSel = $("#proDomesticTbl").find('#' + idTextt).find('select');
                //edited by ishrat to solve select box id auto increment related bug
                for (var i = 0; i < attrSel.length; i++){
                    var nameAtt = attrSel[i].name;
                    var repText = nameAtt.replace('[0]', '[' + rowCoo + ']'); //increment all array element name
                    attrSel[i].name = repText;
                }
                attrSel.val(''); //value reset
                // end of  solving issue related select box id auto increment related bug by ishrat

                //get input elements
                var attrInput = $("#proDomesticTbl").find('#' + idTextt).find('input');
                for (var i = 0; i < attrInput.length; i++){
                    var nameAtt = attrInput[i].name;
                    //increment all array element name
                    var repText = nameAtt.replace('[0]', '[' + rowCoo + ']');
                    attrInput[i].name = repText;
                }
                attrInput.val(''); //value reset
                //edited by ishrat to solve textarea id auto increment related bug
                //get textarea elements
                var attrTextarea = $("#proDomesticTbl").find('#' + idTextt).find('textarea');
                for (var i = 0; i < attrTextarea.length; i++){
                    var nameAtt = attrTextarea[i].name;
                    //increment all array element name
                    var repText = nameAtt.replace('[0]', '[' + rowCoo + ']');
                    attrTextarea[i].name = repText;
                    $('#' + idText).find('.readonlyClass').prop('readonly',true);
                }
                attrTextarea.val(''); //value reset
                // end of  solving issue related textarea id auto increment related bug by ishrat

                attrSel.prop('selectedIndex', 0);  //selected index reset
                //Class change by btn-danger to btn-primary
                $("#proDomesticTbl").find('#' + idTextt).find('.addTableRows').removeClass('btn-primary').addClass('btn-danger')
                        .attr('onclick', 'removeTableRow("proDomesticTbl","' + idTextt + '")');
                $("#proDomesticTbl").find('#' + idTextt).find('.addTableRows > .fa').removeClass('fa-plus').addClass('fa-times');
                $("#proDomesticTbl").find('tr').last().attr('data-number', rowCount);

                $("#proDomesticTbl").find('#' + idTextt).find('.onlyNumber').on('keydown', function (e) {
                    //period decimal
                    if ((e.which >= 48 && e.which <= 57)
                                //numpad decimal
                            || (e.which >= 96 && e.which <= 105)
                                // Allow: backspace, delete, tab, escape, enter and .
                            || $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1
                                // Allow: Ctrl+A
                            || (e.keyCode == 65 && e.ctrlKey === true)
                                // Allow: Ctrl+C
                            || (e.keyCode == 67 && e.ctrlKey === true)
                                // Allow: Ctrl+V
                            || (e.keyCode == 86 && e.ctrlKey === true)
                                // Allow: Ctrl+X
                            || (e.keyCode == 88 && e.ctrlKey === true)
                                // Allow: home, end, left, right
                            || (e.keyCode >= 35 && e.keyCode <= 39))
                    {
                        var thisVal = $(this).val();
                        if (thisVal.indexOf(".") != -1 && e.key == '.') {
                            return false;
                        }
                        $(this).removeClass('error');
                        return true;
                    }
                    else
                    {
                        $(this).addClass('error');
                        return false;
                    }
                });
                $("#proDomesticTbl").find('#' + idTextt).find('.pro_dom_desc_1st').val(production_desc_val);

            }


        } // end of addTableRow() function

        function removeTableRow(tableID, removeNum) {
            $('#' + tableID).find('#' + removeNum).remove();
        }

        $(document).ready(function () {
            $('#country').change(function (e) {
                if (this.value == 'BD') { // 001 is country_code of Bangladesh
                    $('#division_div').removeClass('hidden');
                    $('#division').addClass('required');
                    $('#district_div').removeClass('hidden');
                    $('#district').addClass('required');
                    $('#state_div').addClass('hidden');
                    $('#state').removeClass('required');
                    $('#province_div').addClass('hidden');
                    $('#province').removeClass('required');
                } else {
                    $('#state_div').removeClass('hidden');
                    $('#state').addClass('required');
                    $('#province_div').removeClass('hidden');
                    $('#province').addClass('required');
                    $('#division_div').addClass('hidden');
                    $('#division').removeClass('required');
                    $('#district_div').addClass('hidden');
                    $('#district').removeClass('required');
                }
            });
            $('#country').trigger('change');

            $('#correspondent_country').change(function (e) {
                if (this.value == 'BD') { // 001 is country_code of Bangladesh
                    $('#correspondent_division_div').removeClass('hidden');
                    $('#correspondent_division').addClass('required');
                    $('#correspondent_district_div').removeClass('hidden');
                    $('#correspondent_district').addClass('required');
                    $('#correspondent_state_div').addClass('hidden');
                    $('#correspondent_state').removeClass('required');
                    $('#correspondent_province_div').addClass('hidden');
                    $('#correspondent_province').removeClass('required');
                } else {
                    $('#correspondent_state_div').removeClass('hidden');
                    $('#correspondent_state').addClass('required');
                    $('#correspondent_province_div').removeClass('hidden');
                    $('#correspondent_province').addClass('required');
                    $('#correspondent_division_div').addClass('hidden');
                    $('#correspondent_division').removeClass('required');
                    $('#correspondent_district_div').addClass('hidden');
                    $('#correspondent_district').removeClass('required');
                }
            });
            $('#correspondent_country').trigger('change');

            $("#division").change(function () {
                var divisionId = $('#division').val();
                $(this).after('<span class="loading_data">Loading...</span>');
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "<?php echo url(); ?>/users/get-district-by-division",
                    data: {
                        divisionId: divisionId
                    },
                    success: function (response) {
                        var option = '<option value="">Select One</option>';
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                        }
                        $("#district").html(option);
                        $(self).next().hide();
                    }
                });
            });
            $("#correspondent_division").change(function () {
                var divisionId = $('#correspondent_division').val();
                $(this).after('<span class="loading_data">Loading...</span>');
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "<?php echo url(); ?>/users/get-district-by-division",
                    data: {
                        divisionId: divisionId
                    },
                    success: function (response) {
                        var option = '<option value="">Select One</option>';
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                        }
                        $("#correspondent_district").html(option);
                        $(self).next().hide();
                    }
                });
            });

            $("#industry_cat_id").change(function () {
                var industry_cat_id = $(this).val();
                if (!industry_cat_id) {
                    $('#change_colours').hide();
                    $('#color_name').hide();
                }else{
                    $(this).after('<span class="loading_data">Loading...</span>');
                    var self = $(this);
                    $.ajax({
                        dataType: 'json',
                        type: "GET",
                        url: "<?php echo url(); ?>/project-clearance/colour-change/",
                        data: {
                            industry_cat_id: industry_cat_id
                        },
                        success: function (response) {
                            var code = response.code;
                            var color_name = response.name;
                            $('#change_colours').css('background-color', code);
                            $('#color_name').html(color_name);
                            $('#change_colours').show();
                            $('#color_name').show();
                            $(self).next().hide();
                        }
                    });
                }
                $(this).next().css('display', 'none');
            });
            $('#industry_cat_id').trigger('change');

        }); // end of document.ready

        /********Calculating the numbers of two fields inside multiple rowed tables******/
        function calculateTotal(id)
        {
            var totalBoxStart = 1;
            var totalBoxEnd = 5;
            var setTotalBoxNum = 6;
            var totalVal = 0;
            $("#"+id).find("input[type=text]").each(function(key,value){

                if(key>=totalBoxStart && key <= totalBoxEnd){
                    if(this.value != '' && !isNaN(this.value))
                        totalVal += parseFloat(this.value);
                }
                if(key == setTotalBoxNum){
                    this.value = totalVal;
                }
            });
        }

        @endif {{-- end of view mode off --}}


        @if ($viewMode == 'on')

        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
            }
            return val;
        }

        @if (($process_data -> status_id == 21 || $process_data -> status_id == 24) && Auth::user() -> id == $alreadyExistApplicant -> created_by)
        $(document).ready(function () {
            var today = new Date();
            var yyyy = today.getFullYear();
            var mm = today.getMonth();
            var dd = today.getDate();
            $('.user_DOB').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                minDate: '01/01/' + (yyyy - 6)
            });
        }); //  end of document.ready
        @endif {{-- checking status --}}

        $('#inputForm select').each(function(index){
            var text = $(this).find('option:selected').text();
            var id = $(this).attr("id");
            var val = $(this).val();
            $('#' + id + ' option:selected').replaceWith("<option value='" + val + "' selected>" + text + "</option>");
        });
        $("#inputForm :input[type=text]").each(function(index) {


            $(this).attr("value", $(this).val());

        });
        $("#inputForm textarea").each(function(index) {
            $(this).text($(this).val());
        });
        $("#inputForm select").css({
            "border" : "none",
            "background" : "#fff",
            "pointer-events" : "none",
            "box-shadow": "none",
            "-webkit-appearance" : "none",
            "-moz-appearance" : "none",
            "appearance": "none"
        });

        $('#companyLogoViewer').css({'top':'-15px'});

        $("#inputForm fieldset").css({"display": "block"});
        $("#inputForm #full_same_as_authorized").css({"display": "none"});
        $("#inputForm .actions").css({"display": "none"});
        $("#inputForm .steps").css({"display": "none"});
        $("#inputForm .draft").css({"display": "none"});
        $("#inputForm .title ").css({"display": "none"});
        //document.getElementById("previewDiv").innerHTML = document.getElementById("projectClearanceForm").innerHTML;
        $('#inputForm #showPreview').remove();
        $('#inputForm #save_btn').remove();
        $('#inputForm #save_draft_btn').remove();
        $('#inputForm .stepHeader, #inputForm .calender-icon,#inputForm .hiddenDiv').remove();
        $('#inputForm .required-star').removeClass('required-star');
        $('#inputForm input[type=hidden], #inputForm input[type=file]').remove();
        $('#inputForm .panel-orange > .panel-heading').css('margin-bottom', '10px');
        $('#invalidInst').html('');

        $('#inputForm').find('input:not(:checked),textarea').each(function() {
            if (this.value != ''){
                var displayOp = ''; //display:block
            } else {
                var displayOp = 'display:none';
            }

            if($(this).hasClass("onlyNumber") && !$(this).hasClass("nocomma"))
            {
                var thisVal = commaSeparateNumber(this.value);
                $(this).replaceWith("<span class='onlyNumber " +this.className+ "' style='background-color:#ddd !important; height:auto; margin-bottom:2px; padding:6px;"
                + displayOp + "'>" + thisVal + "</span>");
            }else {
                $(this).replaceWith("<span class='" +this.className+ "' style='background-color:#ddd; height:auto; margin-bottom:2px; padding:6px;"
                + displayOp + "'>" + this.value + "</span>");
            }
        });

        $('#inputForm .hashs').each(function(){
            $(this).replaceWith("");
        });

        $('#inputForm .btn').not('.show-in-view').each(function(){
            $(this).replaceWith("");
        });

        $('#acceptTerms-2').attr("onclick", 'return false').prop("checked", true).css('margin-left', '5px');
        $('#inputForm').find('input[type=radio]').each(function(){
            jQuery(this).attr('disabled', 'disabled');
        });

        $("#inputForm select").replaceWith(function (){

            var selectedText = $(this).find('option:selected').text().trim();
            var displayOp = '';
            if (selectedText != '' && selectedText != 'Select One') {
                displayOp = ''; //display:block
            } else {
                displayOp = 'display:none';
            }

            return "<span class='" +this.className+ "' style='background-color:#ddd;  height:auto; margin-bottom:2px; " +
                    displayOp + "'>" + selectedText + "</span>";
        });
        @endif {{-- viewMode is on --}}

        function toolTipFunction() {
            $('[data-toggle="tooltip"]').tooltip();
        }

        $(document).on('click', '.download', function (e) {
            var value = $(this).attr('data');
            $('.show_' + value).show();
        });

        function newold(thisItem){
            var thisVal = $(thisItem).val();
            if(thisVal == 'New'){
                var parentItem = $(thisItem).parent().parent().find('.readonlyClass');
                parentItem.prop('readonly',true);
                parentItem.val('');
            }else{
                var parentItem = $(thisItem).parent().parent().find('.readonlyClass');
                parentItem.prop('readonly',false);
            }

        }

        function companyLogo(input) {
            if (input.files && input.files[0]) {
                $("#company_logo_err").html('');
                var mime_type = input.files[0].type;
                if(!(mime_type=='image/jpeg' || mime_type=='image/jpg' || mime_type=='image/png')){
                    $("#company_logo_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#companyLogoViewer').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(1,2,3,4,5,6)))

        {{--batch process start--}}
        <script language="javascript">
            var numberOfCheckedBox = 0;
            var curr_app_id = '';
            function setCheckBox(){
                numberOfCheckedBox = 0;
                var flag = 1;
                var selectedWO = $("input[type=checkbox]").not(".selectall");
                selectedWO.each(function() {
                    if (this.checked){
                        numberOfCheckedBox++;
                    }else{
                        flag = 0;
                    }
                });
                if (flag == 1){
                    $("#chk_id").checked = true;
                }else {
                    $("#chk_id").checked = false;
                }
                if(numberOfCheckedBox >= 1){
                    $('.applicable_status').trigger('click');
                }

            }

            function changeStatus(check){
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                setCheckBox();
            }

            $(document).ready(function() {

                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
//            $('.applicable_status').trigger('click');
//            $("#assign_form").validate({
//                errorPlacement: function() {
//                    return false;
//                }
//            });
                $("#apps_from").validate({
                    errorPlacement: function() {
                        return false;
                    }
                });
                $("#batch_from").validate({
                    errorPlacement: function() {
                        return false;
                    }
                });

                var base_checkbox = '.selectall';
                $(base_checkbox).click(function() {
                    if (this.checked) {
                        $('.appCheckBox:checkbox').each(function() {
                            this.checked = true;
                            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                        });
                    } else {
                        $('.appCheckBox:checkbox').each(function() {
                            this.checked = false;
                            //   $('#status_id').attr('disabled',false);
                            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                        });
                    }
                    $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    setCheckBox();

                });
                $('.appCheckBox:checkbox').not(base_checkbox).click(function() {
                    $(".selectall").prop("checked", false);
                });

                $(document).on('click', '.download', function (e) {
                    var value = $(this).attr('data');
                    $('.show_' + value).show();
                });

            });  //  end of document.ready

            var break_for_pending_verification = 0;
            $(document).ready(function() {

                $("#status_id").trigger("click");
                var curr_app_id = $("#curr_app_id").val();
                var curr_status_id = $("#curr_status_id").val();
                $.ajaxSetup({async: false});
                var _token = "{{ csrf_token() }}";
                var delegate = '{{ @$delegated_desk }}';
                var state = false;
                $.post('/project-clearance/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate, _token: _token}, function(response) {

                    if (response.responseCode == 1) {
                        var option = '';
                        option += '<option selected="selected" value="">Select Below</option>';
                        $.each(response.data, function(id, value) {
                            option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                        });
                        $("#status_id").html(option);
                        $("#status_id").trigger("change");
                        $("#status_id").focus();
                    } else if (response.responseCode == 5){
                        alert('Without verification, application can not be processed');
                        break_for_pending_verification = 1;
                        option = '<option selected="selected" value="">Select Below</option>';
                        $("#status_id").html(option);
                        $("#status_id").trigger("change");
                        return false;
                    } else {
                        $('#status_id').html('Please wait');
                    }
                });

                $.ajaxSetup({async: true});
            }); // end of  $(document).ready

            $(document).on('change', '.status_id', function() {
                var curr_app_id_for_process = $("#curr_app_id").val();
                var curr_status_id_for_process = $("#curr_status_id").val();

                var object = $(".status_id");
                var obj = $(object).parent().parent().parent();
                var id = $(object).val();
                var _token = "{{ csrf_token() }}";
                var status_from = $('#status_from').val();
                $('#sendToDeskOfficer').css('display', 'block');
                if (id == 0) {
                    obj.find('.param_id').html('<option value="">Select Below</option>');
                } else {
                    $.post('/project-clearance/ajax/process', {id: id, curr_app_id: curr_app_id_for_process, status_from: curr_status_id_for_process,
                        _token: _token}, function(response) {
                        if (response.responseCode == 1) {
                            var option = '';
                            option += '<option selected="selected" value="">Select Below</option>';
                            var countDesk = 0;
                            $.each(response.data, function(id, value) {
                                countDesk++;
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                            obj.find('#desk_id').html(option);
                            $('#desk_id').attr("disabled", false);
                            $('#remarks').attr("disabled", false);

                            if (countDesk == 0){
                                $('.dd_id').removeClass('required');
                                $('#sendToDeskOfficer').css('display', 'none');
                            }else{
                                $('.dd_id').addClass('required');
                            }

                            if (response.status_to == 5 || response.status_to == 8 || response.status_to == 16 || response.status_to == 17 || response.status_to == 19
                                    || response.status_to == 24|| response.status_to == 10 || response.status_to == 22){

                                $('#remarks').addClass('required');
                                $('#remarks').attr("disabled", false);
                            }else{
                                $('#remarks').removeClass('required');
                            }

                            if (response.file_attach == 1){
                                $('#sendToFile').css('display', 'block');
                            }else{
                                $('#sendToFile').css('display', 'none');
                            }
                        } // when response.responseCode == 1
                    });
                    //  }
                }
            });

            function resetElements(){
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            }

        </script>

        @endif
        @endsection <!--- footer-script--->
