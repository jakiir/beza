<?php $row_sl = 0; ?>
@foreach($getList as $row)
<?php $row_sl++ ?>

    <tr>
        <td>{!! $row_sl !!}</td>
        <td>{!! $row->track_no !!}</td>
        <td>{!! $row->applicant_name !!} </td>
        <td>
            @if($row->desk_name == '')
            Applicant
            @else
            {!! $row->desk_name !!}
            @endif
        </td>
        <td>
            @if(!empty($row->status_name))
            <span style="background-color:<?php echo $row->color; ?>;color: #fff; font-weight: bold;" class="label btn-sm">
                {!! $row->status_name !!}
            </span>
            @else
            <span style="background-color:#dd4b39;color: #fff; font-weight: bold;" class="label btn-sm">
                Draft
            </span>
            @endif {{-- checking status_name --}}

    <td>{!! CommonFunction::updatedOn($row->updated_at) !!}</td>
    <td>
        <a href="{{url('project-clearance/view/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" >
            <i class="fa fa-folder-open-o"></i> View</a>
        @if(!empty($row->certificate) && $row->status_id == 23)
        <a href="{{ url($row->certificate) }}" class="btn show-in-view btn-xs btn-info"
           title="Download Certificate" target="_blank"> <i class="fa  fa-file-pdf-o"></i> Certificate</a>
        @if(Auth::user()->user_type == '1x101')
        <a onclick="return confirm('Are you sure ?')" href="/project-clearance/discard-certificate/{{Encryption::encodeId($row->record_id)}}"
           class="btn btn-xs btn-danger show-in-view open" title="Discard Certificate"> <i class="fa fa-trash" aria-hidden="true"></i> Discard Certificate</a>
        @endif
        @endif
    </td>
</tr>
@endforeach
