<?php

Route::group(array('module' => 'ProcessPath', 'middleware' => ['auth', 'checkAdmin'], 'namespace' => 'App\Modules\ProcessPath\Controllers'), function() {
////     for project-clearance
//    Route::get('process-path/project-clearance/update-form', 'ProcessPathController@edit');
//    Route::get('process-path/project-clearance/create-form', 'ProcessPathController@create');
//    Route::post('process-path/update', 'ProcessPathController@update');
//    Route::post('process-path/store', 'ProcessPathController@store');
//
//    //    for visa recommendation
//    Route::get('process-path/visa-recommendation/update-form', 'ProcessPathController@vrEdit');
//    Route::get('process-path/visa-recommendation/create-form', 'ProcessPathController@vrCreate');
//    Route::post('process-path/visa-recommendation/update', 'ProcessPathController@vrUpdate');
//    Route::post('process-path/visa-recommendation/store', 'ProcessPathController@vrStore');
//
////    for visa
//    Route::get('process-path/visa-assistance/update-form', 'ProcessPathController@visaEdit');
//    Route::get('process-path/visa-assistance/create-form', 'ProcessPathController@visaCreate');
//    Route::post('process-path/visa-assistance/update', 'ProcessPathController@visaUpdate');
//    Route::post('process-path/visa-assistance/store', 'ProcessPathController@visaStore');
//
//    //    for Import Permit
//    Route::get('process-path/import-permit/update-form', 'ProcessPathController@importEdit');
//    Route::get('process-path/import-permit/create-form', 'ProcessPathController@importCreate');
//    Route::post('process-path/import-permit/update', 'ProcessPathController@importUpdate');
//    Route::post('process-path/import-permit/store', 'ProcessPathController@importStore');
//
//    //    for export permit
//    Route::get('process-path/export-permit/update-form', 'ProcessPathController@exportEdit');
//    Route::get('process-path/export-permit/create-form', 'ProcessPathController@exportCreate');
//    Route::post('process-path/export-permit/update', 'ProcessPathController@exportUpdate');
//    Route::post('process-path/export-permit/store', 'ProcessPathController@exportStore');
//
//    /* ************************Starting ofWork Permit***************************/
//    Route::get('process-path/work-permit/update-form', 'ProcessPathController@WPEdit');
//    Route::get('process-path/work-permit/create-form', 'ProcessPathController@WPCreate');
//    Route::post('process-path/work-permit/update', 'ProcessPathController@WPUpdate');
//    Route::post('process-path/work-permit/store', 'ProcessPathController@WPStore');
//
//    Route::resource('process-path', 'ProcessPathController');
    /*     * **************************End of Route Group******************************************* */
});
