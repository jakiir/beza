@extends('layouts.admin')

@section('content')

@include('partials.messages')

<div class="col-lg-12">
    <?php
    $accessMode = ACL::getAccsessRight('processPath');
    if (!ACL::isAllowed($accessMode, 'V'))
        die('You have no access right! Please contact with system admin if you have any query.');
    ?>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-list"></i> <b> Process path create for import permit</b>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="alert" style="display:none;"></div>

            {!! Form::open(array('url' => 'process-path/import-permit/store','method' => 'post', 'class' => 'form-horizontal')) !!}
            <div class="box-body">
                <table class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr class="alert-info">
                            <th>Status from</th>
                            <th>Status to</th>
                            <th>Desk from</th>
                            <th>Desk to</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                {!! Form::select("status_from", $app_status, null, ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('status_from','<span class="help-block">:message</span>') !!}
                            </td>
                            <td>
                                {!! Form::select("status_to[]", $app_status, null, ['class' => 'form-control input-sm required','multiple' => 'multiple']) !!}
                                {!! $errors->first('status_to','<span class="help-block">:message</span>') !!}
                            </td>
                            <td>
                                {!! Form::select('desk_from[]', $user_desk, null, ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('desk_from','<span class="help-block">:message</span>') !!}
                            </td>
                            <td>
                                {!! Form::select('desk_to[]', $user_desk, null, ['class' => 'form-control input-sm required','multiple' => 'multiple']) !!}
                                {!! $errors->first('desk_to','<span class="help-block">:message</span>') !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a href="{{url('process-path/import-permit/update-form')}}" class="btn btn-default pull-left" >Close</a>
                @if(ACL::getAccsessRight('processPath','A'))
                <button type="submit" class="btn btn-primary pull-right" >Save</button>
                @endif
            </div><!-- /.box -->
            {!! Form::close() !!}<!-- /.form end -->
        </div><!-- /box-body -->

    </div><!-- /col-md-12 -->



</div>
{!! Form::close() !!}<!-- /.form end -->


@endsection


@section('footer-script')



@endsection <!--- footer script--->