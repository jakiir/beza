@extends('layouts.admin')

@section('content')

@include('partials.messages')

<div class="col-lg-12">
    <?php
    $accessMode = ACL::getAccsessRight('processPath');
    if (!ACL::isAllowed($accessMode, 'V'))
        die('You have no access right! Please contact with system admin if you have any query.');
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">

            <div class="row">
                <div class="col-lg-6">
                    <div class="btn" style="color:#ffffff">
                        <i class="fa fa-list"></i> Process path for import permit
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="{{url('process-path/import-permit/create-form')}}" >
                        <button type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add New Path</button>
                    </a>
                </div>
            </div>
        </div>

        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="alert" style="display:none;"></div>


            {!! Form::open(array('url' => 'process-path/import-permit/update','method' => 'post', 'class' => 'form-horizontal')) !!}
            <div class="box-body">
                <table class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr class="alert-info">
                            <th>#</th>
                            <th>Status from</th>
                            <th>Status to</th>
                            <th>Desk from</th>
                            <th>Desk to</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        $sl = 0;
                        ?>
                        @foreach($process_path as $path_value)
                        <tr>
                            <td>
                                {{++$sl}}
                                <input type="hidden" value="{{$path_value->id}}" name="ids[]">
                            </td>

                            <td>

                                {!! Form::select("status_from[$i]", $app_status, @$path_value->status_from, ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('status_from','<span class="help-block">:message</span>') !!}
                            </td>

                            <td>

                                <?php
                                $status_to = explode(",", $path_value->status_to);
                                $status_to_field = 'status_to_' . $i . '[]';
                                ?>
                                {!! Form::select($status_to_field, $app_status, $status_to, ['class' => 'form-control input-sm required','multiple' => 'multiple']) !!}
                                {!! $errors->first('status_to','<span class="help-block">:message</span>') !!}
                            </td>

                            <td>

                                <?php
                                $desk_from = explode(",", $path_value->desk_from);
                                $desk_from_field = 'desk_from_' . $i . '[]';
                                ?>
                                {!! Form::select($desk_from_field, $user_desk, $desk_from, ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('desk_from','<span class="help-block">:message</span>') !!}
                            </td>
                            <td>

                                <?php
                                $desk_to = explode(",", $path_value->desk_to);
                                $desk_to_field = 'desk_to_' . $i . '[]';
                                ?>
                                {!! Form::select($desk_to_field, $user_desk, $desk_to, ['class' => 'form-control input-sm required','multiple' => 'multiple']) !!}
                                {!! $errors->first('desk_to','<span class="help-block">:message</span>') !!}
                            </td>




                        </tr>
                        <?php $i++; ?>
                        @endforeach


                    </tbody>
                </table>
                @if(ACL::getAccsessRight('processPath','E'))
                <button type="submit" class="btn btn-primary pull-right" >Save</button>
                @endif
            </div>
            {!! Form::close() !!}<!-- /.form end -->
        </div><!-- /box-body -->

    </div><!-- /col-md-12 -->



</div>
{!! Form::close() !!}<!-- /.form end -->


@endsection


@section('footer-script')



@endsection <!--- footer script--->