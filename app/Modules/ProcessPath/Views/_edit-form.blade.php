@extends('layouts.admin')

{{--@section('page_heading','Create process path')--}}
@section('content')
    <div class="col-lg-12">

        @include('partials.messages')
        <?php
        $accessMode = ACL::getAccsessRight('processPath');
        if (!ACL::isAllowed($accessMode, 'V'))
            die('You have no access right! Please contact with system admin if you have any query.');
        ?>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <a href="{{url('process-path/create-form')}}" class="btn btn-default" type="button"><i class="fa fa-plus"></i> Add New Path</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="alert" style="display:none;"></div>


                {!! Form::open(array('url' => 'process-path/update','method' => 'post', 'class' => 'form-horizontal')) !!}
                <div class="box-body">
                    <table class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                        <thead>
                        <tr class="alert-info">
                            <th>#</th>
                            <th>Service type</th>
                            <th>Status from</th>
                            <th>Status to</th>
                            <th>Desk from</th>
                            <th>Desk to</th>
                            <th>File</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        $sl = 0; ?>
                        @foreach($process_path as $path_value)
                            <tr>
                                <td>
                                    {{++$sl}}
                                    <input type="hidden" value="{{$path_value->id}}" name="ids[]">
                                </td>
                                <td>
                                    {!! Form::select("service_id[$i]", $service, @$path_value->service_id, ['class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('service_id','<span class="help-block">:message</span>') !!}
                                </td>
                                <td>
                                    {!! Form::select("status_from[$i]", $app_status, @$path_value->status_from, ['class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('status_from','<span class="help-block">:message</span>') !!}
                                </td>

                                <td>
                                    {!! Form::select("status_to[$i]", $app_status, @$path_value->status_to, ['class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('status_to','<span class="help-block">:message</span>') !!}
                                </td>

                                <td>

                                    <?php

                                    $desk_from = explode(",",$path_value->desk_from);
                                    $desk_from_field = 'desk_from_'.$i.'[]';
                                    ?>
                                    {!! Form::select($desk_from_field, $user_desk, $desk_from, ['class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('desk_from','<span class="help-block">:message</span>') !!}
                                </td>
                                <td>

                                    <?php
                                    $desk_to = explode(",",$path_value->desk_to);
                                    $desk_to_field = 'desk_to_'.$i.'[]';
                                    ?>
                                    {!! Form::select($desk_to_field, $user_desk, $desk_to, ['class' => 'form-control input-sm required','multiple' => 'multiple']) !!}
                                    {!! $errors->first('desk_to','<span class="help-block">:message</span>') !!}
                                </td>

                                <td>

                                    <?php
                                    $file_value = array(
                                            ''=>'Select One',
                                            '1' => 'Yes',
                                            '0' => 'No',
                                    );
                                    ?>
                                    {!! Form::select("FILE_ATTACHMENT[$i]", $file_value, $path_value->FILE_ATTACHMENT, ['class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('desk_to','<span class="help-block">:message</span>') !!}
                                </td>

                                <td>

                                    <?php
                                    $email_users = array(
                                            '0'=>'Select One',
                                            'User' => 'User',
                                            'StakeHolder' => 'StakeHolder',
                                    );
                                    ?>
                                    {!! Form::select("email_user[$i]", $email_users, $path_value->email, ['class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('email_user','<span class="help-block">:message</span>') !!}
                                </td>


                            </tr>
                            <?php $i++;?>
                        @endforeach


                        </tbody>
                    </table>

                    @if(ACL::getAccsessRight('processPath','E'))
                        <button type="submit" class="btn btn-primary pull-right" >Save</button>
                    @endif
                </div>
                {!! Form::close() !!}<!-- /.form end -->
            </div><!-- /box-body -->

        </div><!-- /col-md-12 -->



    </div>
    {!! Form::close() !!}<!-- /.form end -->


@endsection


@section('footer-script')



    @endsection <!--- footer script--->