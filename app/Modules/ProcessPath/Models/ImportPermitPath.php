<?php namespace App\Modules\ProcessPath\Models;

use Illuminate\Database\Eloquent\Model;

class ImportPermitPath extends Model {


    protected $table = 'import_permit_process_path';
    protected $fillable = [
        'status_to',
        'service_id',
        'status_from',
        'desk_to',
        'desk_from',
        'color'
    ];

}
