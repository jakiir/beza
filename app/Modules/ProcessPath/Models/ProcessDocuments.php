<?php namespace App\Modules\ProcessPath\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class ProcessDocuments extends Model {

    protected $table = 'process_documents';
    protected $fillable = array(
        'id',
        'service_id',
        'app_id',
        'to_desk_id',
        'from_desk_id',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    
    /***************************************End of Model Class****************************************/
}
