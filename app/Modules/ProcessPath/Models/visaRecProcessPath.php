<?php namespace App\Modules\ProcessPath\Models;

use Illuminate\Database\Eloquent\Model;

class visaRecProcessPath extends Model {


    protected $table = 'visa_recommend_process_path';
    protected $fillable = [
        'service_id',
        'status_to',
        'status_from',
        'desk_to',
        'desk_from',
        'color'
    ];

}
