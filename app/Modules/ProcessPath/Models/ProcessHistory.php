<?php namespace App\Modules\ProcessPath\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessHistory extends Model {


    protected $table = 'process_list_hist';
    protected $fillable = array(
        'process_id',
        'track_no',
        'reference_no',
        'company_id',
        'record_id',
        'process_type',
        'initiated_by',
        'closed_by',
        'desk_id',
        'status_id',
        'process_desc',
        'created_at',
        'updated_at',
        'updated_by',
        'on_behalf_of_desk'
    );

}
