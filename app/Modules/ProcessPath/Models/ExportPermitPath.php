<?php namespace App\Modules\ProcessPath\Models;

use Illuminate\Database\Eloquent\Model;

class ExportPermitPath extends Model {


    protected $table = 'export_permit_process_path';
    protected $fillable = [
        'service_id',
        'status_to',
        'status_from',
        'desk_to',
        'desk_from',
        'color'
    ];

}
