<?php

namespace App\Modules\ProcessPath\Models;

use Illuminate\Database\Eloquent\Model;

class WorkPerProcessPath extends Model {

    protected $table = 'work_permit_process_path';
    protected $fillable = [
        'id',
        'service_id',
        'status_to',
        'status_from',
        'desk_to',
        'desk_from',
        'service_id',
        'FILE_ATTACHMENT',
        'email',
        'created_at',
        'updated_at',
    ];

    /*     * ********************************End of Model Class********************************** */
}
