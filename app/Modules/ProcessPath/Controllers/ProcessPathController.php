<?php

namespace App\Modules\ProcessPath\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Modules\apps\Models\ClearanceProcessPath;
use App\Modules\ProcessPath\Models\ExportPermitPath;
use App\Modules\ProcessPath\Models\ImportPermitPath;
use App\Modules\ProcessPath\Models\VisaAssistantPath;
use App\Modules\ProcessPath\Models\visaRecProcessPath;
use App\Modules\ProcessPath\Models\WorkPerProcessPath;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ProcessPathController extends Controller {

    public function index() {
        
    }

    public function create() {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', 1)->lists('status_name', 'status_id');


        $user_desk = ['0' => 'Select One'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');
        return view("ProcessPath::project-create-form", compact('service', 'app_status', 'user_desk'));
    }

    public function store(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = $request->get('service_id');
        $status_from = $request->get('status_from');

        //calculation for desk_from
        $desk_from = $request->get('desk_from');
        $desk_from_count = array();
        $desk_from_final = array();

        if (count($desk_from) <= 1) {
            $desk_from_final = $desk_from;
        } else {
            foreach ($desk_from as $deskfromvalue) {
                $desk_from_count[] = $deskfromvalue;
            }
            $desk_from_final[0] = implode(',', $desk_from_count);
        }
        //calculation for desk_to
        $desk_to = $request->get('desk_to');
        $desk_to_count = array();
        $desk_to_final = array();
        if (count($desk_to) <= 1) {
            $desk_to_final = $desk_to;
        } else {
            foreach ($desk_to as $desktovalue) {
                $desk_to_count[] = $desktovalue;
            }
            $desk_to_final[0] = implode(',', $desk_to_count);
        }

        //calculation for status_to
        $status_to = $request->get('status_to');
        $status_to_count = array();
        $status_to_final = array();
        if (count($status_to) <= 1) {
            $status_to_final = $status_to;
        } else {
            foreach ($status_to as $statustovalue) {
                $status_to_count[] = $statustovalue;
            }
            $status_to_final[0] = implode(',', $status_to_count);
        }

        //finally save data in table
        $data = ClearanceProcessPath::create([
                    'service_id' => 1,
                    'process_type' => 1,
                    'status_to' => $status_to_final[0],
                    'status_from' => $status_from,
                    'desk_from' => $desk_from_final[0],
                    'desk_to' => $desk_to_final[0]
                        ]
        );

        Session::flash('success', 'Process path has been saved for project clearance!');
        return Redirect::to('process-path/project-clearance/update-form');
    }

    public function show($id) {
        //
    }

    public function edit() {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $process_path = ClearanceProcessPath::orderBy('desk_from')->get();
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', 1)->lists('status_name', 'status_id');


        $user_desk = ['0' => 'Select One', '-1' => 'Current Desk'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::project-edit-form", compact('process_path', 'service', 'app_status', 'user_desk'));
    }

    public function update(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $ids = $request->get('ids');
        $status_from = $request->get('status_from');
        foreach ($ids as $key => $row_value) {
            //calculation for desk_from
            $desk_from = $request->get('desk_from_' . $key);
            $desk_from_count = array();
            $desk_from_final = array();

            if (count($desk_from) <= 1) {
                $desk_from_final = $desk_from;
            } else {
                foreach ($desk_from as $deskfromvalue) {
                    $desk_from_count[] = $deskfromvalue;
                }
                $desk_from_final[0] = implode(',', $desk_from_count);
            }
            //calculation for desk_to
            $desk_to = $request->get('desk_to_' . $key);
            $status_to = $request->get('status_to_' . $key);
            $desk_to_count = array();
            $desk_to_final = array();
            $status_to_count = array();
            $status_to_final = array();
            if (count($desk_to) <= 1) {
                $desk_to_final = $desk_to;
            } else {
                foreach ($desk_to as $desktovalue) {
                    $desk_to_count[] = $desktovalue;
                }
                $desk_to_final[0] = implode(',', $desk_to_count);
            }

            if (count($status_to) <= 1) {
                $status_to_final = $status_to;
            } else {
                foreach ($status_to as $statustovalue) {
                    $status_to_count[] = $statustovalue;
                }
                $status_to_final[0] = implode(',', $status_to_count);
            }

            //finally save data in table
            $process_path = ClearanceProcessPath::where('id', $ids[$key])->first();
            $process_path->service_id = 1;
            $process_path->process_type = 1;
            if ($status_to_final[0] != '' || $status_to_final[0] != null) {
                $process_path->status_to = $status_to_final[0];
            } else {
                $process_path->status_to = 0;
            }
            $process_path->status_from = $status_from[$key];

            if ($desk_from_final[0] != '' || $desk_from_final[0] != null) {
                $process_path->desk_from = $desk_from_final[0];
            } else {
                $process_path->desk_from = 0;
            }

            if ($desk_to_final[0] != '' || $desk_to_final[0] != null) {
                $process_path->desk_to = $desk_to_final[0];
            } else {
                $process_path->desk_to = 0;
            }

            $process_path->save();
        }
        Session::flash('success', 'Process path has been saved for project clearance!');
        return Redirect::to('process-path/project-clearance/update-form');
    }

//  For visa assistance

    public function visaCreate() {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_id', 'asc')
                        ->where('service_id', 2)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::visa-create-form", compact('service', 'app_status', 'user_desk'));
    }

    public function visaStore(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = $request->get('service_id');
        $status_from = $request->get('status_from');

        //calculation for desk_from
        $desk_from = $request->get('desk_from');
        $desk_from_count = array();
        $desk_from_final = array();

        if (count($desk_from) <= 1) {
            $desk_from_final = $desk_from;
        } else {
            foreach ($desk_from as $deskfromvalue) {
                $desk_from_count[] = $deskfromvalue;
            }
            $desk_from_final[0] = implode(',', $desk_from_count);
        }
        //calculation for desk_to
        $desk_to = $request->get('desk_to');
        $desk_to_count = array();
        $desk_to_final = array();
        if (count($desk_to) <= 1) {
            $desk_to_final = $desk_to;
        } else {
            foreach ($desk_to as $desktovalue) {
                $desk_to_count[] = $desktovalue;
            }
            $desk_to_final[0] = implode(',', $desk_to_count);
        }

        //calculation for status_to
        $status_to = $request->get('status_to');
        $status_to_count = array();
        $status_to_final = array();
        if (count($status_to) <= 1) {
            $status_to_final = $status_to;
        } else {
            foreach ($status_to as $statustovalue) {
                $status_to_count[] = $statustovalue;
            }
            $status_to_final[0] = implode(',', $status_to_count);
        }

        //finally save data in table
        $data = VisaAssistantPath::create([
                    'service_id' => 2,
                    'process_type' => 2,
                    'status_to' => $status_to_final[0],
                    'status_from' => $status_from,
                    'desk_from' => $desk_from_final[0],
                    'desk_to' => $desk_to_final[0]
                        ]
        );

        Session::flash('success', 'Process path has been saved for visa assistance!');
        return Redirect::to('process-path/visa-assistance/update-form');
    }

    public function visaEdit() {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $process_path = VisaAssistantPath::all();
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', 2)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One', '-1' => 'Current Desk'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::visa-edit-form", compact('process_path', 'service', 'app_status', 'user_desk'));
    }

    public function visaUpdate(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $ids = $request->get('ids');
        $status_from = $request->get('status_from');
        foreach ($ids as $key => $row_value) {
            //calculation for desk_from
            $desk_from = $request->get('desk_from_' . $key);
            $desk_from_count = array();
            $desk_from_final = array();

            if (count($desk_from) <= 1) {
                $desk_from_final = $desk_from;
            } else {
                foreach ($desk_from as $deskfromvalue) {
                    $desk_from_count[] = $deskfromvalue;
                }
                $desk_from_final[0] = implode(',', $desk_from_count);
            }
            //calculation for desk_to
            $desk_to = $request->get('desk_to_' . $key);
            $status_to = $request->get('status_to_' . $key);
            $desk_to_count = array();
            $desk_to_final = array();
            $status_to_count = array();
            $status_to_final = array();
            if (count($desk_to) <= 1) {
                $desk_to_final = $desk_to;
            } else {
                foreach ($desk_to as $desktovalue) {
                    $desk_to_count[] = $desktovalue;
                }
                $desk_to_final[0] = implode(',', $desk_to_count);
            }

            if (count($status_to) <= 1) {
                $status_to_final = $status_to;
            } else {
                foreach ($status_to as $statustovalue) {
                    $status_to_count[] = $statustovalue;
                }
                $status_to_final[0] = implode(',', $status_to_count);
            }

            //finally save data in table
            $process_path = VisaAssistantPath::where('id', $ids[$key])->first();
            $process_path->service_id = 2;
            $process_path->process_type = 2;
            if ($status_to_final[0] != '' || $status_to_final[0] != null) {
                $process_path->status_to = $status_to_final[0];
            } else {
                $process_path->status_to = 0;
            }
            $process_path->status_from = $status_from[$key];

            if ($desk_from_final[0] != '' || $desk_from_final[0] != null) {
                $process_path->desk_from = $desk_from_final[0];
            } else {
                $process_path->desk_from = 0;
            }

            if ($desk_to_final[0] != '' || $desk_to_final[0] != null) {
                $process_path->desk_to = $desk_to_final[0];
            } else {
                $process_path->desk_to = 0;
            }

            $process_path->save();
        }
        Session::flash('success', 'Process path has been saved for visa assistance!');
        return Redirect::to('process-path/visa-assistance/update-form');
    }

    // For visa Recommendation

    public function vrCreate() {
        $service_id = 3;
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_id', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::vr-create-form", compact('service', 'app_status', 'user_desk'));
    }

    public function vrStore(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 3;
        $status_from = $request->get('status_from');

        //calculation for desk_from
        $desk_from = $request->get('desk_from');
        $desk_from_count = array();
        $desk_from_final = array();

        if (count($desk_from) <= 1) {
            $desk_from_final = $desk_from;
        } else {
            foreach ($desk_from as $deskfromvalue) {
                $desk_from_count[] = $deskfromvalue;
            }
            $desk_from_final[0] = implode(',', $desk_from_count);
        }
        //calculation for desk_to
        $desk_to = $request->get('desk_to');
        $desk_to_count = array();
        $desk_to_final = array();
        if (count($desk_to) <= 1) {
            $desk_to_final = $desk_to;
        } else {
            foreach ($desk_to as $desktovalue) {
                $desk_to_count[] = $desktovalue;
            }
            $desk_to_final[0] = implode(',', $desk_to_count);
        }

        //calculation for status_to
        $status_to = $request->get('status_to');
        $status_to_count = array();
        $status_to_final = array();
        if (count($status_to) <= 1) {
            $status_to_final = $status_to;
        } else {
            foreach ($status_to as $statustovalue) {
                $status_to_count[] = $statustovalue;
            }
            $status_to_final[0] = implode(',', $status_to_count);
        }

        //finally save data in table
        $data = visaRecProcessPath::create([
                    'service_id' => $service_id,
                    'status_to' => $status_to_final[0],
                    'status_from' => $status_from,
                    'desk_from' => $desk_from_final[0],
                    'desk_to' => $desk_to_final[0]
                        ]
        );

        Session::flash('success', 'Process path has been saved for visa recommendation!');
        return Redirect::back();
    }

    public function vrEdit() {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 3;
        $process_path = visaRecProcessPath::all();
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One', '-1' => 'Current Desk'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::vr-edit-form", compact('process_path', 'service', 'app_status', 'user_desk'));
    }

    public function vrUpdate(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 3;
        $ids = $request->get('ids');
        $status_from = $request->get('status_from');
        foreach ($ids as $key => $row_value) {
            //calculation for desk_from
            $desk_from = $request->get('desk_from_' . $key);
            $desk_from_count = array();
            $desk_from_final = array();

            if (count($desk_from) <= 1) {
                $desk_from_final = $desk_from;
            } else {
                foreach ($desk_from as $deskfromvalue) {
                    $desk_from_count[] = $deskfromvalue;
                }
                $desk_from_final[0] = implode(',', $desk_from_count);
            }
            //calculation for desk_to
            $desk_to = $request->get('desk_to_' . $key);
            $status_to = $request->get('status_to_' . $key);
            $desk_to_count = array();
            $desk_to_final = array();
            $status_to_count = array();
            $status_to_final = array();
            if (count($desk_to) <= 1) {
                $desk_to_final = $desk_to;
            } else {
                foreach ($desk_to as $desktovalue) {
                    $desk_to_count[] = $desktovalue;
                }
                $desk_to_final[0] = implode(',', $desk_to_count);
            }

            if (count($status_to) <= 1) {
                $status_to_final = $status_to;
            } else {
                foreach ($status_to as $statustovalue) {
                    $status_to_count[] = $statustovalue;
                }
                $status_to_final[0] = implode(',', $status_to_count);
            }

            //finally save data in table
            $process_path = visaRecProcessPath::where('id', $ids[$key])->first();
            $process_path->service_id = $service_id;
            if ($status_to_final[0] != '' || $status_to_final[0] != null) {
                $process_path->status_to = $status_to_final[0];
            } else {
                $process_path->status_to = 0;
            }
            $process_path->status_from = $status_from[$key];

            if ($desk_from_final[0] != '' || $desk_from_final[0] != null) {
                $process_path->desk_from = $desk_from_final[0];
            } else {
                $process_path->desk_from = 0;
            }

            if ($desk_to_final[0] != '' || $desk_to_final[0] != null) {
                $process_path->desk_to = $desk_to_final[0];
            } else {
                $process_path->desk_to = 0;
            }

            $process_path->save();
        }
        Session::flash('success', 'Process path has been saved for visa recommendation!');
        return Redirect::back();
    }

//  For Export permit
    public function exportCreate() {
        $service_id = 5;
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_id', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::export-create-form", compact('service', 'app_status', 'user_desk'));
    }

    public function exportStore(Request $request) {

//      $service_id = $request->get('service_id');
        $service_id = 5;
        $status_from = $request->get('status_from');

        //calculation for desk_from
        $desk_from = $request->get('desk_from');
        $desk_from_count = array();
        $desk_from_final = array();

        if (count($desk_from) <= 1) {
            $desk_from_final = $desk_from;
        } else {
            foreach ($desk_from as $deskfromvalue) {
                $desk_from_count[] = $deskfromvalue;
            }
            $desk_from_final[0] = implode(',', $desk_from_count);
        }

        //calculation for desk_to
        $desk_to = $request->get('desk_to');
        $desk_to_count = array();
        $desk_to_final = array();
        if (count($desk_to) <= 1) {
            $desk_to_final = $desk_to;
        } else {
            foreach ($desk_to as $desktovalue) {
                $desk_to_count[] = $desktovalue;
            }
            $desk_to_final[0] = implode(',', $desk_to_count);
        }

        //calculation for status_to
        $status_to = $request->get('status_to');
        $status_to_count = array();
        $status_to_final = array();
        if (count($status_to) <= 1) {
            $status_to_final = $status_to;
        } else {
            foreach ($status_to as $statustovalue) {
                $status_to_count[] = $statustovalue;
            }
            $status_to_final[0] = implode(',', $status_to_count);
        }

        //finally save data in table
        $data = ExportPermitPath::create([
                    'service_id' => $service_id,
                    'status_to' => $status_to_final[0],
                    'status_from' => $status_from,
                    'desk_from' => $desk_from_final[0],
                    'desk_to' => $desk_to_final[0]
                        ]
        );

        Session::flash('success', 'Process path has been saved for export permit!');
        return Redirect::back();
    }

    public function exportEdit() {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 5;
        $process_path = ExportPermitPath::all();
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One', '-1' => 'Current Desk'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::export-edit-form", compact('process_path', 'service', 'app_status', 'user_desk'));
    }

    public function exportUpdate(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 5;
        $ids = $request->get('ids');
        $status_from = $request->get('status_from');
        foreach ($ids as $key => $row_value) {
            //calculation for desk_from
            $desk_from = $request->get('desk_from_' . $key);
            $desk_from_count = array();
            $desk_from_final = array();

            if (count($desk_from) <= 1) {
                $desk_from_final = $desk_from;
            } else {
                foreach ($desk_from as $deskfromvalue) {
                    $desk_from_count[] = $deskfromvalue;
                }
                $desk_from_final[0] = implode(',', $desk_from_count);
            }
            //calculation for desk_to
            $desk_to = $request->get('desk_to_' . $key);
            $status_to = $request->get('status_to_' . $key);
            $desk_to_count = array();
            $desk_to_final = array();
            $status_to_count = array();
            $status_to_final = array();
            if (count($desk_to) <= 1) {
                $desk_to_final = $desk_to;
            } else {
                foreach ($desk_to as $desktovalue) {
                    $desk_to_count[] = $desktovalue;
                }
                $desk_to_final[0] = implode(',', $desk_to_count);
            }

            if (count($status_to) <= 1) {
                $status_to_final = $status_to;
            } else {
                foreach ($status_to as $statustovalue) {
                    $status_to_count[] = $statustovalue;
                }
                $status_to_final[0] = implode(',', $status_to_count);
            }

            //finally save data in table
            $process_path = ExportPermitPath::where('id', $ids[$key])->first();
            $process_path->service_id = $service_id;
            if ($status_to_final[0] != '' || $status_to_final[0] != null) {
                $process_path->status_to = $status_to_final[0];
            } else {
                $process_path->status_to = 0;
            }
            $process_path->status_from = $status_from[$key];

            if ($desk_from_final[0] != '' || $desk_from_final[0] != null) {
                $process_path->desk_from = $desk_from_final[0];
            } else {
                $process_path->desk_from = 0;
            }

            if ($desk_to_final[0] != '' || $desk_to_final[0] != null) {
                $process_path->desk_to = $desk_to_final[0];
            } else {
                $process_path->desk_to = 0;
            }

            $process_path->save();
        }
        Session::flash('success', 'Process path has been saved for export permit!');
        return Redirect::back();
    }

//  For Import permit

    public function importCreate() {
        $service_id = 6;
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::import-create-form", compact('service', 'app_status', 'user_desk'));
    }

    public function importStore(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
//      $service_id = $request->get('service_id');
        $service_id = 6;
        $status_from = $request->get('status_from');

        //calculation for desk_from
        $desk_from = $request->get('desk_from');
        $desk_from_count = array();
        $desk_from_final = array();

        if (count($desk_from) <= 1) {
            $desk_from_final = $desk_from;
        } else {
            foreach ($desk_from as $deskfromvalue) {
                $desk_from_count[] = $deskfromvalue;
            }
            $desk_from_final[0] = implode(',', $desk_from_count);
        }
        //calculation for desk_to
        $desk_to = $request->get('desk_to');
        $desk_to_count = array();
        $desk_to_final = array();
        if (count($desk_to) <= 1) {
            $desk_to_final = $desk_to;
        } else {
            foreach ($desk_to as $desktovalue) {
                $desk_to_count[] = $desktovalue;
            }
            $desk_to_final[0] = implode(',', $desk_to_count);
        }

        //calculation for status_to
        $status_to = $request->get('status_to');
        $status_to_count = array();
        $status_to_final = array();
        if (count($status_to) <= 1) {
            $status_to_final = $status_to;
        } else {
            foreach ($status_to as $statustovalue) {
                $status_to_count[] = $statustovalue;
            }
            $status_to_final[0] = implode(',', $status_to_count);
        }
        //finally save data in table
        $data = ImportPermitPath::create([
                    'service_id' => $service_id,
                    'status_to' => $status_to_final[0],
                    'status_from' => $status_from,
                    'desk_from' => $desk_from_final[0],
                    'desk_to' => $desk_to_final[0]
                        ]
        );

        Session::flash('success', 'Process path has been saved for import permit!');
        return Redirect::back();
    }

    public function importEdit() {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 6;
        $process_path = ImportPermitPath::all();
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One', '-1' => 'Current Desk'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::import-edit-form", compact('process_path', 'service', 'app_status', 'user_desk'));
    }

    public function importUpdate(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 6;
        $ids = $request->get('ids');
        $status_from = $request->get('status_from');
        foreach ($ids as $key => $row_value) {
            //calculation for desk_from
            $desk_from = $request->get('desk_from_' . $key);
            $desk_from_count = array();
            $desk_from_final = array();

            if (count($desk_from) <= 1) {
                $desk_from_final = $desk_from;
            } else {
                foreach ($desk_from as $deskfromvalue) {
                    $desk_from_count[] = $deskfromvalue;
                }
                $desk_from_final[0] = implode(',', $desk_from_count);
            }
            //calculation for desk_to
            $desk_to = $request->get('desk_to_' . $key);
            $status_to = $request->get('status_to_' . $key);
            $desk_to_count = array();
            $desk_to_final = array();
            $status_to_count = array();
            $status_to_final = array();
            if (count($desk_to) <= 1) {
                $desk_to_final = $desk_to;
            } else {
                foreach ($desk_to as $desktovalue) {
                    $desk_to_count[] = $desktovalue;
                }
                $desk_to_final[0] = implode(',', $desk_to_count);
            }

            if (count($status_to) <= 1) {
                $status_to_final = $status_to;
            } else {
                foreach ($status_to as $statustovalue) {
                    $status_to_count[] = $statustovalue;
                }
                $status_to_final[0] = implode(',', $status_to_count);
            }

            //finally save data in table
            $process_path = ImportPermitPath::where('id', $ids[$key])->first();
            $process_path->service_id = $service_id;
            if ($status_to_final[0] != '' || $status_to_final[0] != null) {
                $process_path->status_to = $status_to_final[0];
            } else {
                $process_path->status_to = 0;
            }
            $process_path->status_from = $status_from[$key];

            if ($desk_from_final[0] != '' || $desk_from_final[0] != null) {
                $process_path->desk_from = $desk_from_final[0];
            } else {
                $process_path->desk_from = 0;
            }

            if ($desk_to_final[0] != '' || $desk_to_final[0] != null) {
                $process_path->desk_to = $desk_to_final[0];
            } else {
                $process_path->desk_to = 0;
            }

            $process_path->save();
        }
        Session::flash('success', 'Process path has been saved for import permit!');
        return Redirect::back();
    }

    /*     * ******************** For Work Permit *************************** */

    public function WPCreate() {
        $service_id = 4; // 4 is Work Permit
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_id', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::workPermit.wp-create-form", compact('service', 'app_status', 'user_desk'));
    }

    public function WPStore(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 4; // 4 is Work Permit
        $status_from = $request->get('status_from');

        //calculation for desk_from
        $desk_from = $request->get('desk_from');
        $desk_from_count = array();
        $desk_from_final = array();

        if (count($desk_from) <= 1) {
            $desk_from_final = $desk_from;
        } else {
            foreach ($desk_from as $deskfromvalue) {
                $desk_from_count[] = $deskfromvalue;
            }
            $desk_from_final[0] = implode(',', $desk_from_count);
        }
        //calculation for desk_to
        $desk_to = $request->get('desk_to');
        $desk_to_count = array();
        $desk_to_final = array();
        if (count($desk_to) <= 1) {
            $desk_to_final = $desk_to;
        } else {
            foreach ($desk_to as $desktovalue) {
                $desk_to_count[] = $desktovalue;
            }
            $desk_to_final[0] = implode(',', $desk_to_count);
        }

        //calculation for status_to
        $status_to = $request->get('status_to');
        $status_to_count = array();
        $status_to_final = array();
        if (count($status_to) <= 1) {
            $status_to_final = $status_to;
        } else {
            foreach ($status_to as $statustovalue) {
                $status_to_count[] = $statustovalue;
            }
            $status_to_final[0] = implode(',', $status_to_count);
        }

        //finally save data in table
        WorkPerProcessPath::create([
            'service_id' => $service_id,
            'status_to' => $status_to_final[0],
            'status_from' => $status_from,
            'desk_from' => $desk_from_final[0],
            'desk_to' => $desk_to_final[0],
            'service_id' => $service_id
                ]
        );

        Session::flash('success', 'Process path has been saved for work permit!');
        return Redirect::back();
    }

    public function WPEdit() {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 4; // 4 is Work Permit
        $process_path = WorkPerProcessPath::all();
        $service = ['' => 'Select One'] + DB::table('service_info')->orderBy('service_info.name', 'asc')
                        ->lists('name', 'id');

        $app_status = ['' => 'Select One'] + DB::table('app_status')->orderBy('app_status.status_name', 'asc')
                        ->where('service_id', $service_id)->lists('status_name', 'status_id');

        $user_desk = ['0' => 'Select One', '-1' => 'Current Desk'] + DB::table('user_desk')->orderBy('user_desk.desk_id', 'asc')
                        ->lists('desk_name', 'desk_id');

        return view("ProcessPath::workPermit.wp-edit-form", compact('process_path', 'service', 'app_status', 'user_desk'));
    }

    public function WPUpdate(Request $request) {
        if (!ACL::getAccsessRight('processPath', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $service_id = 4; // 4 is Work Permit
        $ids = $request->get('ids');
        $status_from = $request->get('status_from');
        foreach ($ids as $key => $row_value) {
            //calculation for desk_from
            $desk_from = $request->get('desk_from_' . $key);
            $desk_from_count = array();
            $desk_from_final = array();

            if (count($desk_from) <= 1) {
                $desk_from_final = $desk_from;
            } else {
                foreach ($desk_from as $deskfromvalue) {
                    $desk_from_count[] = $deskfromvalue;
                }
                $desk_from_final[0] = implode(',', $desk_from_count);
            }
            //calculation for desk_to
            $desk_to = $request->get('desk_to_' . $key);
            $status_to = $request->get('status_to_' . $key);
            $desk_to_count = array();
            $desk_to_final = array();
            $status_to_count = array();
            $status_to_final = array();
            if (count($desk_to) <= 1) {
                $desk_to_final = $desk_to;
            } else {
                foreach ($desk_to as $desktovalue) {
                    $desk_to_count[] = $desktovalue;
                }
                $desk_to_final[0] = implode(',', $desk_to_count);
            }

            if (count($status_to) <= 1) {
                $status_to_final = $status_to;
            } else {
                foreach ($status_to as $statustovalue) {
                    $status_to_count[] = $statustovalue;
                }
                $status_to_final[0] = implode(',', $status_to_count);
            }

            //finally save data in table
            $process_path = WorkPerProcessPath::where('id', $ids[$key])->first();
            $process_path->service_id = $service_id;
            if ($status_to_final[0] != '' || $status_to_final[0] != null) {
                $process_path->status_to = $status_to_final[0];
            } else {
                $process_path->status_to = 0;
            }
            $process_path->status_from = $status_from[$key];

            if ($desk_from_final[0] != '' || $desk_from_final[0] != null) {
                $process_path->desk_from = $desk_from_final[0];
            } else {
                $process_path->desk_from = 0;
            }

            if ($desk_to_final[0] != '' || $desk_to_final[0] != null) {
                $process_path->desk_to = $desk_to_final[0];
            } else {
                $process_path->desk_to = 0;
            }

            $process_path->save();
        }
        Session::flash('success', 'Process path has been saved for work permit!');
        return Redirect::back();
    }

    /*     * ******************** End of Work Permit *************************** */

    /*     * ********************************************End of Controller Class************************************************* */
}
