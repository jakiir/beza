<?php

Route::group(array('module' => 'Certificate', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'],
    'namespace' => 'App\Modules\Certificate\Controllers'), function() {

    // Form for uploading Certificates
    Route::get('cer-doc/create', 'CertificateController@create');
    Route::get('cer-doc/edit-form/{id}', 'CertificateController@appFormEdit');
    Route::get('cer-doc/view/{id}', 'CertificateController@appFormView');

    Route::post('cer-doc/ajax/{param}', 'CertificateController@ajaxRequest');

// Certificate Uploading
    Route::any('cer-doc/upload-document', 'CertificateController@uploadDocument');
    Route::post('cer-doc/store', "CertificateController@storeDoc");

    // soft delete   
    Route::get('cer-doc/delete/{id}', "CertificateController@softDelete");

    // certificate List   
    Route::get('cer-doc', 'CertificateController@index');

    /*     * ****************** starting of Certificate Type add / update ********************** */

    Route::get('cer-doc/list-types', 'CertificateController@listCerType');
    Route::post('cer-doc/get-cer-type-data', "CertificateController@getCerTypeData");
    Route::get('cer-doc/create-cer-type', "CertificateController@createCerType");
    Route::post('cer-doc/store-cer-type', "CertificateController@storeCerType");
    Route::get('cer-doc/edit-cer-type/{id}', "CertificateController@editCerType");
    Route::patch('cer-doc/update-cer-type/{id}', "CertificateController@updateCerType");
    Route::get('cer-doc/delete-cer-type/{id}', "CertificateController@deleteCerType");

    /*     * ****************** Ending of Certificate Type add / update ********************** */

    // Check if unique certificate type name or order
    Route::get('cer-doc/check-existing-name', "CertificateController@checkExistingName");
    Route::get('cer-doc/check-existing-order', "CertificateController@checkExistingOrder");

    /*     * ********************************End of Route group****************************** */
});
