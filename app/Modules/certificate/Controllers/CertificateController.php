<?php

namespace App\Modules\Certificate\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Certificate\Models\DocCertificate;
use App\Modules\Certificate\Models\UploadedCertificates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use yajra\Datatables\Datatables;

class CertificateController extends Controller {

    public function index() {
        if (!ACL::getAccsessRight('certificate', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $loggedUser = CommonFunction::getUserId();
        $getList = UploadedCertificates::where('is_archieved', 0)->where('doc_file', '!=', "")->where('created_by', $loggedUser)->get();
        return view("certificate::list", compact('getList'));
    }

    public function create() {
        if (!ACL::getAccsessRight('certificate', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $loggedUser = CommonFunction::getUserId();
        $userType = CommonFunction::getUserType();

        $doc = DocCertificate::orderBy('order', 'ASC')->where('is_archieved', 0);

        if ($userType != '1x101') {
            $doc = $doc->where(function($query) {
                $query->where('admin_or_user', CommonFunction::getUserId())
                        ->orWhere('admin_or_user', 0);
            });
        }
        $document = $doc->get(['doc_id', 'doc_name']);

        $cerByUser = UploadedCertificates::where('is_archieved', 0)
                ->where('created_by', $loggedUser)
                ->get();
        if (count($cerByUser) > 0) {
            foreach ($cerByUser as $documents) {
                $uploadedDocs[$documents->doc_id]['doucument_id'] = $documents->id;
                $uploadedDocs[$documents->doc_id]['file'] = $documents->doc_file;
                $uploadedDocs[$documents->doc_id]['doc_name'] = $documents->doc_name;
                $uploadedDocs[$documents->doc_id]['created_by'] = $documents->created_by;
            }
            $mode = 'E';
        } else {
            $uploadedDocs = [];
            $mode = 'A';
        }

        return view("certificate::form", compact('document', 'documents', 'uploadedDocs', 'mode', 'userType', 'cerByUser'));
    }

    function storeDoc(Request $request) {

        $loggedUser = CommonFunction::getUserId();
        $userType = CommonFunction::getUserType();

       // $doc_row = DocCertificate::where('is_archieved', 0)->orderBy('order', 'ASC')->get(['doc_id', 'doc_name']);

        $doc = DocCertificate::orderBy('order', 'ASC')->where('is_archieved', 0);

        if ($userType != '1x101') {
            $doc = $doc->where(function($query) {
                $query->where('admin_or_user', CommonFunction::getUserId())
                    ->orWhere('admin_or_user', 0);
            });
        }
        $doc_row = $doc->get(['doc_id', 'doc_name']);



        ///Start file uploading
        if (isset($doc_row)) {
            $i = 0;
            foreach ($doc_row as $docs) {
                $i++;
                //  if ($request->get('validate_field_' . $docs->doc_id) != '') {
                $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ?
                                $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                $documnent_id = $docs->doc_id;
                if ($request->get('document_id_' . $docs->doc_id) == "") {
                    UploadedCertificates::create([
                        'doc_id' => $documnent_id,
                        'doc_name' => $documentName,
                        'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                    ]);
                } else {
                    $documentId = $request->get('document_id_' . $docs->doc_id);
                    $docData = UploadedCertificates::find($documentId);
                    $docData->doc_id = $documnent_id;
                    $docData->doc_name = $documentName;
                    $docData->doc_file = $request->get('validate_field_' . $docs->doc_id);
                    $docData->save();
                }
            }
            // }
        } /* End file uploading */

        Session::flash('success', 'Your documents have been uploaded successfully');
        return redirect('/cer-doc');
    }

    public function uploadDocument() {
        return View::make('certificate::ajaxUploadFile');
    }

    function softDelete($_id) {
        $id = Encryption::decodeId($_id);

        $docData = UploadedCertificates::find($id);
        $docData->is_archieved = 1;
        $docData->updated_by = CommonFunction::getUserId();
        $docData->save();

        Session::flash('success', 'Data has been deleted successfully.');
        return redirect('/cer-doc/');
    }

    /*     * ****************** Starting of Certificate Type add / update ********************** */

    public function listCerType() {
        if (!ACL::getAccsessRight('certificate', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $userType = CommonFunction::getUserType();
        return view("certificate::list-for-admin", compact('userType'));
    }

    public function getCerTypeData() {
        $mode = ACL::getAccsessRight('certificate', 'V');

        $userType = Auth::user()->user_type;

        $datas = DocCertificate::orderBy('order', 'asc')->where('is_archieved', 0);
        if ($userType != '1x101') {
            $datas = $datas->where('admin_or_user', CommonFunction::getUserId());
        }
        $datas = $datas->get(['doc_id', 'doc_name', 'doc_priority', 'updated_at', 'is_active']);

        return Datatables::of($datas)
                        ->addColumn('action', function ($datas) use ($mode) {
                            if ($mode) {
                                $url = "ConfirmDelete('" . Encryption::encodeId($datas->doc_id) . "')";
                                return '<a href="/cer-doc/edit-cer-type/' . Encryption::encodeId($datas->doc_id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>'
                                        . ' '
                                        . '<a href="javascript:void(0)" ' .
                                        " class='btn btn-xs btn-danger' onclick=$url><i class='fa fa-times'></i></a>";
                                ;
                            }
                        })
                        ->removeColumn('doc_id')
                        ->make(true);
    }

    public function createCerType() {
        if (!ACL::getAccsessRight('certificate', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $userType = CommonFunction::getUserType();
        return view("certificate::create-cer-type", compact('userType'));
    }

    public function storeCerType(Request $request) {
        if (!ACL::getAccsessRight('certificate', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'doc_name' => 'required',
        ]);

        if (Auth::user()->user_type == '1x101') { // admin        
            $user_or_admin = 0;
        } else {
            $user_or_admin = CommonFunction::getUserId();
        }
        $insert = DocCertificate::create([
                    'doc_name' => $request->get('doc_name'),
                    'doc_priority' => 0,
                    'order' => $request->get('order'),
                    'admin_or_user' => $user_or_admin,
                    'created_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/cer-doc/edit-cer-type/' . Encryption::encodeId($insert->id));
    }

    public function editCerType($id) {
        if (!ACL::getAccsessRight('certificate', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $_id = Encryption::decodeId($id);
        $data = DocCertificate::where('doc_id', $_id)->first();
        $userType = CommonFunction::getUserType();
        return view("certificate::edit-cer-type", compact('data', 'id', 'userType'));
    }

    public function updateCerType($id, Request $request) {
        if (!ACL::getAccsessRight('certificate', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $_id = Encryption::decodeId($id);

        $this->validate($request, [
            'doc_name' => 'required',
        ]);

        DocCertificate::where('doc_id', $_id)->update([
            'doc_name' => $request->get('doc_name'),
            'doc_priority' => 0, // not mandatory
            'order' => $request->get('order'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('cer-doc/edit-cer-type/' . $id);
    }

    function deleteCerType($_id) {
        $id = Encryption::decodeId($_id);

        DocCertificate::where('doc_id', $id)->update([
            'is_archieved' => 1,
            'updated_by' => CommonFunction::getUserId()
        ]);

        $userType = CommonFunction::getUserType();

        if ($userType == '1x101') {
            $url = '/cer-doc/list-types';
        } else {
            $url = '/cer-doc/create';
        }

        Session::flash('success', 'Data has been deleted successfully.');
        return redirect($url);
    }

    /*     * ****************** Ending of Certificate Type add / update ********************** */

    public function checkExistingName(Request $request) {
        $name = $request->get('name');
        $id = $request->get('_id');

        if ($id != '') {
            $ct_id = Encryption::decodeId($id);
            $if_existed = DocCertificate::where('is_archieved', 0)->where('doc_name', $name)->where('doc_id', '!=', $ct_id)->count();
        } else {
            $if_existed = DocCertificate::where('is_archieved', 0)->where('doc_name', $name)->count();
        }
        return $if_existed;
    }

    public function checkExistingOrder(Request $request) {
        $order = $request->get('order');
        $id = $request->get('_id');

        if ($id != '') {
            $ct_id = Encryption::decodeId($id);
            $if_existed = DocCertificate::where('is_archieved', 0)->where('order', $order)->where('doc_id', '!=', $ct_id)->count();
        } else {
            $if_existed = DocCertificate::where('is_archieved', 0)->where('order', $order)->count();
        }
        return $if_existed;
    }

    /*     * ********************************************End of Controller Class************************************************* */
}
