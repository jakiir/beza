<?php

namespace App\Modules\Certificate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Libraries\CommonFunction;

class UploadedCertificates extends Model {

    protected $table = 'uploaded_certificates';
    protected $fillable = [
        'doc_id',
        'doc_name',
        'doc_file',
        'is_active',
        'is_archieved',
        'created_by',
    ];

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->created_by = Auth::user()->id;
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *****************************End of Model Function********************************* */
}
