@extends('layouts.admin')

@section('content')

@include('partials.messages')

<?php
$accessMode = ACL::getAccsessRight('certificate');
if (!ACL::isAllowed($accessMode, $mode)) {
    die('You have no access right! Please contact with system admin if you have any query.');
}
?>


<div class="col-md-12 col-lg-12">
    <div class="panel panel panel-red"  id="inputForm">
        <div class="panel-heading"><b>Associated documents</b></div>
        <div class="panel-body">
            {!! Form::open(array('url' => 'cer-doc/store','method' => 'post','id' => 'formId','role'=>'form')) !!}
            <input type="hidden" name="selected_file" id="selected_file" />
            <input type="hidden" name="validateFieldName" id="validateFieldName" />
            <input type="hidden" name="isRequired" id="isRequired" />

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover ">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th colspan="6">Required Attachments</th>
                            <th colspan="2">Attached PDF file
                                <span onmouseover="toolTipFunction()" data-toggle="tooltip" title="Maximum size for each file is 15MB">
                                    <i class="fa fa-question-circle" aria-hidden="true"></i></span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i = 1; ?>
                        @foreach($document as $row)
                        <tr>
                            <td><div align="center">{!! $i !!}</div></td>
                            <td colspan="6">{!!  $row->doc_name !!}
                                <?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></td>
                            <td colspan="2">
                                <input type="hidden"  name="document_id_<?php echo $row->doc_id; ?>"
                                       value="{{(!empty($uploadedDocs[$row->doc_id]['doucument_id']) ? $uploadedDocs[$row->doc_id]['doucument_id'] : '')}}">
                                <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>"
                                       name="doc_name_<?php echo $row->doc_id; ?>" />

                                <input name="file<?php echo $row->doc_id; ?>"   id="file<?php echo $row->doc_id; ?>" type="file" size="20"
                                <?php
                                if (empty($uploadedDocs[$row->doc_id]['file']) && $row->doc_priority == "1") {
                                    echo "class='required'";
                                }
                                ?>
                                       onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php echo $row->doc_id; ?>', '<?php echo $row->doc_priority; ?>')"/>

                                @if($row->additional_field == 1)
                                <table>
                                    <tr>
                                        <td>Other file Name : </td>
                                        <td> <input maxlength="64" class="form-control input-sm <?php if ($row->doc_priority == "1") echo 'required'; ?>"
                                                    name="other_doc_name_<?php echo $row->doc_id; ?>" type="text"
                                                    value="{{(!empty($uploadedDocs[$row->doc_id]['doc_name']) ? $uploadedDocs[$row->doc_id]['doc_name'] : '')}}">
                                        </td>
                                    </tr>
                                </table>
                                @endif

                                @if(!empty($uploadedDocs[$row->doc_id]['file']))
                                    <div class="save_file saved_file_{{$row->doc_id}}">
                                        <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(!empty($uploadedDocs[$row->doc_id]['file']) ?
                                                                    $uploadedDocs[$row->doc_id]['file'] : ''))}}" title="{{$row->doc_name}}">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php $file_name = explode('/',$uploadedDocs[$row->doc_id]['file']); echo end($file_name); ?></a>

                                        <?php if(!empty($uploadedDocs[$row->doc_id]) && Auth::user()->id == $uploadedDocs[$row->doc_id]['created_by'])  {?>
                                        <a href="javascript:void(0)" onclick="ConfirmDeleteFile({{ $row->doc_id }})">
                                            <span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span>
                                        </a>
                                        <?php } ?>
                                    </div>
                                @endif
                                <div id="preview_<?php echo $row->doc_id; ?>">
                                    <input type="hidden" value="<?php echo !empty($uploadedDocs[$row->doc_id]['file']) ?
                                            $uploadedDocs[$row->doc_id]['file'] : ''?>" id="validate_field_<?php echo $row->doc_id; ?>"
                                           name="validate_field_<?php echo $row->doc_id; ?>" class="<?php echo $row->doc_priority == "1" ? "required":'';  ?>"  />
                                </div>

                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->

            <div class="col-md-12">
                <div class="col-md-3">
                    <a href="{{ url('/cer-doc/') }}">
                        {!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                    </a>
                </div>
                <div class="col-md-6 text-center">
                    <span class="text-muted"> Fields marked with red stars (</span><span style="color: red">*</span><span class="text-muted">)
                        are mandatory </span>
                </div>
                <div class="col-md-3">
                    <span class="pull-right">
                        @if(ACL::getAccsessRight('certificate','A'))
                        <input type="submit" class="btn btn-primary btn-md" value="Save" name="submit">
                        @endif
                    </span>
                </div>
            </div>


            <div class="col-md-12"><br/></div>

            <div class="col-md-12">
                <div class="alert alert-warning">
                    If you have larger files, you need excellent internet speed to upload these files.<br/>
                    For slower speed of internet, you have to compress the files under 5MB using any editor before uploading it to ensure
                    uninterrupted uploading.
                </div>
            </div>
            {!! Form::close() !!}<!-- /.form end -->
        </div>
    </div>


    @include('certificate::list-cer-type')

</div>

@endsection

@section('footer-script')
@include('partials.datatable-scripts')

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

<script type="text/javascript">
    function uploadDocument(targets, id, vField, isRequired) {
        var inputFile = $("#" + id).val();
        if (inputFile == '') {
            $("#" + id).html('');
            document.getElementById("isRequired").value = '';
            document.getElementById("selected_file").value = '';
            document.getElementById("validateFieldName").value = '';
            document.getElementById(targets).innerHTML = '<input type="hidden" class="required" value="" id="' + vField + '" name="' + vField + '">';
            if ($('#label_' + id).length)
                $('#label_' + id).remove();
            return false;
        }

        try {
            document.getElementById("isRequired").value = isRequired;
            document.getElementById("selected_file").value = id;
            document.getElementById("validateFieldName").value = vField;
            document.getElementById(targets).style.color = "red";
            var action = "{{url('/cer-doc/upload-document')}}";
            $("#" + targets).html('Uploading....');
            var file_data = $("#" + id).prop('files')[0];
            var form_data = new FormData();
            form_data.append('selected_file', id);
            form_data.append('isRequired', isRequired);
            form_data.append('validateFieldName', vField);
            form_data.append('_token', "{{ csrf_token() }}");
            form_data.append(id, file_data);
            $.ajax({
                target: '#' + targets,
                url: action,
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    $('#' + targets).html(response);
                    var fileNameArr = inputFile.split("\\");
                    var l = fileNameArr.length;
                    if ($('#label_' + id).length)
                        $('#label_' + id).remove();
                    var doc_id = parseInt(id.substring(4));
                    var newInput = $('<label class="saved_file_'+doc_id+'" id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + ' <a href="javascript:void(0)" onclick="EmptyFile('+ doc_id +')"><span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span> </a></b></label>');

                    $("#" + id).after(newInput);
                    //check valid data
                    var validate_field = $('#' + vField).val();
                    if (validate_field == '') {
                        document.getElementById(id).value = '';
                    }
                }
            });
        } catch (err) {
            document.getElementById(targets).innerHTML = "Sorry! Something went wrong... Please try again.";
        }
    } // end of uploadDocument function

    function toolTipFunction() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    $(function () {
        $('#CerTypeList').DataTable({
            processing: true,
            serverSide: true,
            iDisplayLength: 50,
            ajax: {
                url: '{{url("cer-doc/get-cer-type-data")}}',
                method: 'POST',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'doc_name', name: 'doc_name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });
    });

    function ConfirmDelete() {
        var sure_del = confirm("Are you sure you want to delete this item?");
        if (sure_del) {
            return true;
        }  else {
            return false;
        }
    }
</script>

@endsection <!--- footer-script--->

