<?php
$accessMode = ACL::getAccsessRight('certificate');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! Please contact system admin for more information');
}
?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">
                <strong>Types of Associated Documents </strong>
            </div>
            <div class="col-md-6">
                @if(ACL::getAccsessRight('certificate','A'))
                <a class="" href="{{ url('/cer-doc/create-cer-type') }}">
                    {!! Form::button('<i class="fa fa-plus"></i> <b>New Type of Document</b>', array('type' => 'button',
                    'class' => 'btn btn-success btn-xs pull-right')) !!}
                </a>
                @endif
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table id="CerTypeList" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Document Name</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>