@extends('layouts.admin')

@section('content')

@include('partials.messages')

<div class="col-md-12 col-lg-12">
    @include('certificate::list-cer-type')
</div>

@endsection

@section('footer-script')
@include('partials.datatable-scripts')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

<script type="text/javascript">

    $(function () {
        $('#CerTypeList').DataTable({
            processing: true,
            serverSide: true,
            iDisplayLength: 50,
            ajax: {
                url: '{{url("cer-doc/get-cer-type-data")}}',
                method: 'POST',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'doc_name', name: 'doc_name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });
    });

    function ConfirmDelete(id) {
        var sure_del = confirm("Are you sure you want to delete this item?");
        if (sure_del) {
            var url = '<?php echo url();?>';
            window.location=(url+"/cer-doc/delete-cer-type/"+id);
        }else {
            return false;
        }
    }
</script>

@endsection <!--- footer-script--->
