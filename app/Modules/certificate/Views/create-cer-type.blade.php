@extends('layouts.admin')

@section('content')
<?php
$accessMode = ACL::getAccsessRight('certificate');
if (!ACL::isAllowed($accessMode, 'A')) {
    die('You have no access right! Please contact system admin for more information');
}
?>

@include('partials.messages')

<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>New Type of Associated Document</b>
        </div>

        <div class="panel-body">
            {!! Form::open(array('url' => '/cer-doc/store-cer-type','method' => 'post', 'class' => 'form-horizontal', 'id' => 'formId',
            'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}

            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('doc_name','Name: ',['class'=>'col-md-3  required-star']) !!}
                    <div class="col-md-5 {{$errors->has('doc_name') ? 'has-error' : ''}}">
                        {!! Form::text('doc_name', null, ['class'=>'form-control required input-sm textOnly']) !!}
                        {!! $errors->first('doc_name','<span class="help-block">:message</span>') !!}
                        <p class="text-danger name-error"></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('order','Order',['class'=>'col-md-3']) !!}
                    <div class="col-md-5 {{$errors->has('order') ? 'has-error' : ''}}">
                        {!! Form::text('order', null, ['class' => 'form-control input-sm number',
                        'placeholder' => 'In the order you want them to appear on the application']) !!}
                    {!! $errors->first('order','<span class="help-block">:message</span>') !!}
                        <p class="text-danger order-error"></p>
                    </div>
                </div>
            </div>

            <div class="col-md-12">

                <?php
                if($userType == '1x101'){
                    $url = '/cer-doc/list-types';
                } else{
                    $url = '/cer-doc/create';
                }
                    ?>
                <a href="{{ url($url) }}">
                    {!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                </a>
                <button type="submit" class="btn btn-success btn-save pull-right">
                    <i class="fa fa-chevron-circle-right"></i> <b>Save</b></button>
            </div>

            {!! Form::close() !!}<!-- /.form end -->

            <div class="overlay" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>

</div>
@endsection

@section('footer-script')

<script>
    var _token = $('input[name="_token"]').val();

    $(document).ready(function () {
        $("#formId").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });
            $("#doc_name").blur(function () {
            var name = $(this).val().trim();
            var _id = '';
            if (name != '') {
                $(this).after('<span class="loading_data">Loading...</span>');
                var self = $(this);
                if (name.length > 5) {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo url(); ?>/cer-doc/check-existing-name",
                        data: {name: name,
                                        _id: _id
                                    },
                        success: function (res) {
                            if (res > 0) {
                                $('.btn-save').attr("disabled", true);
                                $('.name-error').html('This document type is already existed!<br/> Please use that one to upload your document.');
                            } else {
                                $('.btn-save').attr("disabled", false);
                                $('.name-error').html('');
                            }
                            self.next().hide();
                        }
                    });
                } else {
                    self.next().hide();
                    $('.name-error').html('At least six characters are required!');
                    $('.btn-save').attr("disabled", true);
                }
            }
        });

//            $("#order").blur(function () {
//            var order = $(this).val().trim();
//            var _id = '';
//            if (order != '') {
//                $(this).after('<span class="loading_data">Loading...</span>');
//                var self = $(this);
//                if (order.length > 0) {
//                    $.ajax({
//                        type: "GET",
//                        url: "<?php // echo url(); ?>/cer-doc/check-existing-order",
//                        data: {order: order,
//                                        _id: _id
//                                    },
//                        success: function (res) {
//                            if (res > 0) {
//                                $('.btn-save').attr("disabled", true);
//                                $('.order-error').html('This order is already existed for another document type!<br/> Please check the existing list before inserting.');
//                            } else {
//                                $('.btn-save').attr("disabled", false);
//                                $('.order-error').html('');
//                            }
//                            self.next().hide();
//                        }
//                    });
//                } else {
//                            self.next().hide();
//                            $('.order-error').html('At least a numeric number is required!');
//                            $('.btn-save').attr("disabled", true);
//                        }
//                    }
//                });
</script>
@endsection <!--- footer script--->