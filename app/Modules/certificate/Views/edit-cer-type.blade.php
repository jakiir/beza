@extends('layouts.admin')

@section('content')
<?php
$accessMode = ACL::getAccsessRight('certificate');
if (!ACL::isAllowed($accessMode, 'E')) {
    die('You have no access right! Please contact system admin for more information');
}
?>

@include('partials.messages')

<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>Changes of {!! $data->doc_name !!}</b>
        </div>

        <div class="panel-body">

            {!! Form::open(array('url' => '/cer-doc/update-cer-type/'.$id,'method' => 'patch', 'class' => 'form-horizontal', 'id' => 'info',
            'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}

            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('doc_name','Name: ',['class'=>'col-md-3  required-star']) !!}
                    <div class="col-md-5 {{$errors->has('doc_name') ? 'has-error' : ''}}">
                        {!! Form::text('doc_name',$data->doc_name, ['class'=>'form-control required input-sm']) !!}
                        {!! $errors->first('doc_name','<span class="help-block">:message</span>') !!}
                        <p class="text-danger name-error"></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('order','Order',['class'=>'col-md-3']) !!}
                    <div class="col-md-5 {{$errors->has('order') ? 'has-error' : ''}}">
                        {!! Form::text('order', $data->order, ['class' => 'form-control input-sm',
                        'placeholder' => 'In the order you want them to appear on the application']) !!}
                        {!! $errors->first('order','<span class="help-block">:message</span>') !!}
                        <p class="text-danger order-error"></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <?php
                        if ($userType == '1x101') {
                            $url = '/cer-doc/list-types';
                        } else {
                            $url = '/cer-doc/create';
                        }
                        ?>
                        <a href="{{ url($url) }}">
                            {!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                        </a>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success  pull-right">
                            <i class="fa fa-chevron-circle-right"></i> <b>Save</b></button>
                    </div>
                </div><!-- /.col-md-12 -->
            </div>

            {!! Form::close() !!}<!-- /.form end -->

            <div class="overlay" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>

</div>
@endsection

@section('footer-script')
<script>
    var _token = $('input[name="_token"]').val();

    $(document).ready(function () {
        $("#info").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });
    $("#doc_name").blur(function () {
        var name = $(this).val().trim();
        var _id = '<?php echo $id ?>';
        if (name != '') {
            $(this).after('<span class="loading_data">Loading...</span>');
            var self = $(this);
            if (name.length > 5) {
                $.ajax({
                    type: "GET",
                    url: "<?php echo url(); ?>/cer-doc/check-existing-name",
                    data: {name: name,
                        _id: _id
                    },
                    success: function (res) {
                        if (res > 0) {
                            $('.btn-save').attr("disabled", true);
                            $('.name-error').html('This document type is already existed!<br/> Please use that one to upload your document.');
                        } else {
                            $('.btn-save').attr("disabled", false);
                            $('.name-error').html('');
                        }
                        self.next().hide();
                    }
                });
            } else {
                self.next().hide();
                $('.name-error').html('At least six characters are required!');
                $('.btn-save').attr("disabled", true);
            }
        }
    });

//    $("#order").blur(function () {
//        var order = $(this).val().trim();
//        var _id = '<?php // echo $id ?>';
//        if (order != '') {
//            $(this).after('<span class="loading_data">Loading...</span>');
//            var self = $(this);
//            if (order.length > 0) {
//                $.ajax({
//                    type: "GET",
//                    url: "<?php // echo url();  ?>/cer-doc/check-existing-order",
//                    data: {order: order,
//                        _id: _id
//                    },
//                    success: function (res) {
//                        if (res > 0) {
//                            $('.btn-save').attr("disabled", true);
//                            $('.order-error').html('This order is already existed for another document type!<br/> Please check the existing list before inserting.');
//                        } else {
//                            $('.btn-save').attr("disabled", false);
//                            $('.order-error').html('');
//                        }
//                        self.next().hide();
//                    }
//                });
//            } else {
//                self.next().hide();
//                $('.order-error').html('At least a numeric number is required!');
//                $('.btn-save').attr("disabled", true);
//            }
//        }
//    });
</script>
@endsection <!--- footer script--->