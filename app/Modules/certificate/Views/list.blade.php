@extends('layouts.admin')

@section('content')

    @include('partials.messages')

    <?php
    $accessMode = ACL::getAccsessRight('certificate');
    if (!ACL::isAllowed($accessMode, 'V')) {
        die('You have no access right! Please contact system admin for more information');
    }
    ?>
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <i class="fa fa-list"></i> <strong>List of Associated Documents</strong>
                    </div>
                    <div class="col-md-6">
                        @if(ACL::getAccsessRight('certificate','A'))
                            <a class="" href="{{ url('/cer-doc/create') }}">
                                {!! Form::button('<i class="fa fa-plus"></i> <b>Upload Documents </b>', array('type' => 'button',
                                'class' => 'btn btn-default btn-sm pull-right')) !!}
                            </a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="table-responsive">

                    <table id="list" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th> # </th>
                            <th>Name</th>
                            <th width="15%">Mandatory</th>
                            <th>Action</th>
                        </tr>
                        </thead><tbody>
                        <?php $i = 1; ?>
                        @foreach($getList as $row)
                            <tr>
                                <td>{!! $i++ !!}</td>
                                <td>{!! $row->doc_name !!}</td>
                                <td>
                                    <?php
                                    if ($row->doc_priority == 1) {
                                        $class = 'text-success';
                                        $status = 'Mandatory';
                                    } else {
                                        $class = 'text-danger';
                                        $status = 'Not Mandatory';
                                    }
                                    ?>
                                    <span class="{{ $class }}">{{  $status }}</span>
                                </td>
                                <td>
                                    @if($row->doc_file)
                                        <a href="{!! url('uploads/'. $row->doc_file) !!}" target="_blank" class="btn btn-xs btn-primary">
                                            <i class="fa fa-folder-open-o"></i> View
                                        </a>
                                    @else
                                        <span class="text-danger">File not found!</span>
                                    @endif
                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger"
                                       onclick="ConfirmDelete('{{Encryption::encodeId($row->id)}}')"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    @include('partials.datatable-scripts')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <script>
        $(function () {
            $('#list').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "iDisplayLength": 50
            });
        });

        function redirect_to(id){
            var url = '<?php echo url();?>';
            window.location=(url+'/cer-doc/delete/'+id);
        }
        function ConfirmDelete(id) {
            var sure_del = confirm("Are you sure you want to delete this item?");
            if (sure_del) {
                redirect_to(id);
            }else {
                return false;
            }
        }
    </script>
@endsection
