<?php

namespace App\Modules\BankPayment\Models;

use App\Libraries\CommonFunction;
use App\Modules\exportPermit\Models\ExportPermit;
use App\Modules\importPermit\Models\ImportPermit;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\visaAssistance\Models\VisaAssistance;
use App\Modules\visaRecommendation\Models\VisaRecommend;
use App\Modules\workPermit\Models\WorkPermit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BillPaymentDetails extends Model {

    protected $table = 'bill_payment_details';
    protected $fillable = [
        'id',
        'bill_id',
        'arrears_bill_id',
        'app_ids',
        'service_id',
        'service_quantity',
        'per_amount',
        'total_amount',
        'bat_amount',
        'payment_status_id',
        'is_archived',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->created_by = Auth::user()->id;
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }


    public function updateEachSevicePaymentStatus($appIds, $service_id, $paymentStatus) {
        if($service_id==1){
            ProjectClearance::whereIn('id',$appIds)->update(['payment_status_id' => $paymentStatus]);
        }elseif($service_id==2){
            VisaAssistance::whereIn('id',$appIds)->update(['payment_status_id' => $paymentStatus]);
        }elseif($service_id==3){
            VisaRecommend::whereIn('id',$appIds)->update(['payment_status_id' => $paymentStatus]);
        }elseif($service_id==4){
            WorkPermit::whereIn('id',$appIds)->update(['payment_status_id' => $paymentStatus]);
        }elseif($service_id==5){
            ExportPermit::whereIn('id',$appIds)->update(['payment_status_id' => $paymentStatus]);
        }elseif($service_id==6){
            ImportPermit::whereIn('id',$appIds)->update(['payment_status_id' => $paymentStatus]);
        }
        return true;
    }

    /*     * *******************************End of Models****************************** */
}
