<?php

namespace App\Modules\BankPayment\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\apps\Models\pdfQueue;
use App\Modules\BankPayment\Models\BillPaymentDetails;
use App\Modules\billGeneration\Models\BillInfo;
use App\Modules\billGeneration\Models\BillInfoDetails;
use App\Modules\Settings\Models\Area;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\BankBranch;
use App\Modules\Settings\Models\Configuration;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\Notification;
use App\Modules\Users\Models\Users;
use App\Modules\workPermit\Models\Processlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Milon\Barcode\DNS1D;
use mPDF;
use yajra\Datatables\Datatables;

class BankPaymentController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));
        ACL::db_reconnect();
    }

    public function index() {
        if (!ACL::getAccsessRight('bankPayment', 'V')) {
            die('You have no access right! This incidence will be reported. Contact with system admin for more information..');
        }
        return view("BankPayment::bill-list-bank");
    }

    // For showing bills to specific Bank Branch Users
    public function getPendingBillsForBank(BillInfoDetails $billDetails) {
        $mode = ACL::getAccsessRight('bankPayment', 'V');
        $pendingBills = $billDetails->getBillsByPaymentStatus(11); // 11 is for pending payment

        return Datatables::of($pendingBills)
                        ->addColumn('action', function ($pendingBills) use ($mode) {
                            if ($mode) {
                                return '<a href="' . url('bank-payment/view-bill/' . Encryption::encodeId($pendingBills->id) . '/11') .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('bill_type', function ($pendingBills) {
                            if ($pendingBills->bill_type == 1) {
                                $billType = 'Project Clearance';
                            } elseif ($pendingBills->bill_type == 2) {
                                $billType = 'Visa Assistance';
                            } elseif ($pendingBills->bill_type == 3) {
                                $billType = 'Others';
                            }
                            return $billType;
                        })
                        ->editColumn('bill_month', function ($getList) {
                            return isset($getList->bill_month) ? date('M-Y', strtotime($getList->bill_month)) : '';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function getApprovedBillsForBank(BillInfoDetails $billDetails) {
        $mode = ACL::getAccsessRight('bankPayment', 'V');
        $approvedBills = $billDetails->getBillsByPaymentStatus(12); // 12 is paid payment status

        return Datatables::of($approvedBills)
                        ->addColumn('action', function ($approvedBills) use ($mode) {
                            if ($mode) {
                                if (isset($approvedBills->attachment) && $approvedBills->payment_status_id == '12') { // 12 = paid
                                    $moneyReceipt = '<a target="_blank" href="' . url($approvedBills->attachment) .
                                            '" class="btn btn-xs btn-danger"><i class="fa fa-file-pdf-o"></i> Money Receipt</a>';
                                } else {
                                    $moneyReceipt = '';
                                }
                                return '<a href="' . url('bank-payment/view-bill/' . Encryption::encodeId($approvedBills->id) . '/12') .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>' .
                                        ' ' . $moneyReceipt;
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('bill_type', function ($approvedBills) {
                            if ($approvedBills->bill_type == 1) {
                                $billType = 'Project Clearance';
                            } elseif ($approvedBills->bill_type == 2) {
                                $billType = 'Visa Assistance';
                            } elseif ($approvedBills->bill_type == 3) {
                                $billType = 'Others';
                            }
                            return $billType;
                        })
                        ->editColumn('bill_month', function ($getList) {
                            return isset($getList->bill_month) ? date('M-Y', strtotime($getList->bill_month)) : '';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function getRejectedBillsForBank(BillInfoDetails $billDetails) {
        $mode = ACL::getAccsessRight('bankPayment', 'V');
        $approvedBills = $billDetails->getBillsByPaymentStatus(2); // 2 is payment rejected status

        return Datatables::of($approvedBills)
                        ->addColumn('action', function ($approvedBills) use ($mode) {
                            if ($mode) {
                                return '<a href="' . url('bank-payment/view-bill/' . Encryption::encodeId($approvedBills->id) . '/2') .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('bill_type', function ($approvedBills) {
                            if ($approvedBills->bill_type == 1) {
                                $billType = 'Project Clearance';
                            } elseif ($approvedBills->bill_type == 2) {
                                $billType = 'Visa Assistance';
                            } elseif ($approvedBills->bill_type == 3) {
                                $billType = 'Others';
                            }
                            return $billType;
                        })
                        ->editColumn('bill_month', function ($getList) {
                            return isset($getList->bill_month) ? date('M-Y', strtotime($getList->bill_month)) : '';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function create() {
        if (!ACL::getAccsessRight('bankPayment', 'V')) {
            die('You have no access right! This incidence will be reported. Contact with system admin for more information..');
        }
        return view("BankPayment::bank-payment");
    }

    public function searchPayment(Request $request) {
        if (!ACL::getAccsessRight('bankPayment', 'PS')) {
            die('You have no access right! This incidence will be reported. Contact with system admin for more information..');
        }
        try {
            $invoice_no = trim($request->get('invoice_no'));
            $prefix = substr($invoice_no, 0, 1);

            if ($prefix != 'B') { // B is the usual prefix of Bill invoice
                Session::flash('error', "Sorry, it seems like you are searching with wrong invoice number! [BPC6000]..");
                return redirect('bank-payment/create')->withInput();
            }
            $this->validate($request, [
                'invoice_no' => 'required'
            ]);

            $billType = BillInfo::where('invoice_no', $invoice_no)->pluck('bill_type');

            // getting bill and company info
            if ($billType == 2) { // BillType 2 = VA
                $bill_info = BillInfo::leftJoin('visa_assistance as va', 'va.id', '=', 'bill_info.project_clearance_id')
                        ->leftjoin('pdf_queue as pq', function ($join) {
                            $pdfType = 'beza.bill-generation.' . env('server_type');
                            $join->on('pq.app_id', '=', 'bill_info.id');
                            $join->on('pq.service_id', '=', DB::raw('0'));
                            $join->where('pq.pdf_type', '=', $pdfType);
                        })
                        ->where('bill_info.invoice_no', $invoice_no)
                        ->whereIn('bill_info.payment_status_id', [0, 2]) // 0 is unpaid, 2 = rejected
                        ->first([
                    'bill_info.*',
                    'va.tracking_number',
                    'va.correspondent_name as proposed_name',
                    'va.country',
                    'va.division',
                    'va.district',
                    'va.state',
                    'va.province',
                    'va.road_no',
                    'va.house_no',
                    'va.post_code',
                    'va.mobile as phone',
                    'va.fax',
                    'va.email',
                    'va.website',
                    'pq.secret_key'
                ]);
            } elseif ($billType == 1 || $billType == 3) { // BillType 1 = PC, BillType 3= Others [Ep,IP,WP,VR]
                $bill_info = BillInfo::leftJoin('project_clearance', 'project_clearance.id', '=', 'bill_info.project_clearance_id')
                        ->leftjoin('pdf_queue as pq', function($join) {
                            $pdfType = 'beza.bill-generation.' . env('server_type');
                            $join->on('pq.app_id', '=', 'bill_info.id');
                            $join->on('pq.service_id', '=', DB::raw('0'));
                            $join->where('pq.pdf_type', '=', $pdfType);
                        })
                        ->where('bill_info.invoice_no', $invoice_no)
                        ->whereIn('bill_info.payment_status_id', [0, 2]) // 0 is unpaid, 2 = rejected
                        ->first([
                    'bill_info.*',
                    'project_clearance.tracking_number',
                    'project_clearance.proposed_name',
                    'project_clearance.country',
                    'project_clearance.division',
                    'project_clearance.district',
                    'project_clearance.state',
                    'project_clearance.province',
                    'project_clearance.road_no',
                    'project_clearance.house_no',
                    'project_clearance.post_code',
                    'project_clearance.phone',
                    'project_clearance.fax',
                    'project_clearance.email',
                    'project_clearance.website',
                    'pq.secret_key'
                ]);
            }

            if (!$bill_info) {
                Session::flash('error', 'Bill information not found! [BPC6005].');
                return redirect('bank-payment/create')->withInput();
            }
            if (!$bill_info->secret_key > 0) {
                Session::flash('error', 'Bill information has not been found! Please create the bill first [BPC6093].');
                return redirect('bank-payment/create')->withInput();
            }

            $amountWord = isset($bill_info->net_payable_amount) ? CommonFunction::convert_number_to_words($bill_info->net_payable_amount) : '';

            if (!CommonFunction::bankTransactionStatus(Auth::user()->bank_branch_id)) {
                Session::flash('error', "Unfortunately, transaction is currently postponed for this bank. [BPC6015].");
                redirect('bank-payment/create')->withInput();
            }

//            if (!CommonFunction::isArrears($bill_info->id)) {
//                Session::flash('error', "This bill invoice has been included as arrear with the next bill. [BPC7025].");
//                redirect('bank-payment/create')->withInput();
//            }

            // payment mode related information is fetched from configuration table
            $payment_config = Configuration::where('caption', 'BANK_PAYMENT_PERIOD')->first();
            $now = Carbon::now();

            if (!($payment_config->value == 1 && ($payment_config->value2 <= $now && $payment_config->value3 >= $now))) {
                Session::flash('error', 'Period of payment is not valid for now [BPC6020].');
//               Session::flash('error', 'Payment mode is not valid [BPC6020].  '.$payment_config->value.' / '.$payment_config->value2.'  /  '.$now.' / '.$payment_config->value3);
                redirect('bank-payment/create')->withInput();
            }

            $dn1d = new DNS1D();
            $trackingNo = $bill_info->tracking_number; // tracking no push on barcode.
            if (!empty($trackingNo)) {
                $barcode = $dn1d->getBarcodePNG($trackingNo, 'C39');
                $barcode_url = 'data:image/png;base64,' . $barcode;
            } else {
                $barcode_url = '';
            }

            // getting bill details info for payment search
            $billDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                    ->leftJoin('service_info as si', 'si.id', '=', 'bill_info_details.service_id')
                    ->where('bill_info_details.bill_id', $bill_info->id)
//                    ->where('bill_info_details.bill_id', $bill_info->id)
                    ->get(['si.name as service_name', 'bill_info_details.*']);

            $totalAvailedServices = 0;
            $serviceWiseTrackingNumbers = array();
            foreach ($billDetails as $data) {
                $service_id = 0;
                $service_id = $data->service_id;
                $explodedids = explode(',', $data->app_ids);

                $trackingNumbers = array();
                foreach ($explodedids as $app_id) {
                    $trackingNo = Processlist::where('service_id', $service_id)
                            ->where('record_id', $app_id)
                            ->pluck('track_no');
                    $trackingNumbers[] = $trackingNo;
                }

                $serviceWiseTrackingNumbers[$data->service_id] = $trackingNumbers;
                $totalAvailedServices += $data->service_quantity;
            }

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $divisions = ['' => 'Select One'] + Area::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $districts = ['' => 'Select One'] + Area::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            return view('BankPayment::bank-payment', compact(
                            'billDetails', 'voucher_info', 'bill_info', 'pin_code', 'countries', 'divisions', 'districts', 'barcode_url', 'totalAvailedServices', 'amountWord', 'serviceWiseTrackingNumbers'));
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BPC6025].');
            return Redirect::back()->withInput();
        }
    }

    public function receiveBill(Request $request, BillPaymentDetails $BillPayment) {
        if (!ACL::getAccsessRight('bankPayment', 'PS')) {
            die('You have no access right! This incidence will be reported. Contact with system admin for more information..');
        }
        $this->validate($request, [
            'ref_no' => 'required',
            'secret_key' => 'required'
        ]);

        $bill_Id = Encryption::decodeId($request->get('bill_id'));
        $refNo = $request->get('ref_no');
        try {
            // getting detailed bill info
            $bill_info = BillInfo::leftJoin('project_clearance', 'project_clearance.id', '=', 'bill_info.project_clearance_id')
                    ->leftjoin('pdf_queue as pq', function($join) {
                        $pdfType = 'beza.bill-generation.' . env('server_type');
                        $join->on('pq.app_id', '=', 'bill_info.id');
                        $join->on('pq.service_id', '=', DB::raw('0'));
                        $join->where('pq.pdf_type', '=', $pdfType);
                    })
                    ->where('bill_info.id', $bill_Id)
                    ->whereIn('bill_info.payment_status_id', [0, 2]) // 0 is unpaid, 2 = rejected
                    ->first([
                'bill_info.*',
                'project_clearance.proposed_name',
                'project_clearance.country',
                'project_clearance.division',
                'project_clearance.district',
                'project_clearance.state',
                'project_clearance.province',
                'project_clearance.road_no',
                'project_clearance.house_no',
                'project_clearance.post_code',
                'project_clearance.phone',
                'project_clearance.fax',
                'project_clearance.email',
                'project_clearance.website',
                'pq.secret_key'
            ]);
            if (!$bill_info) {
                Session::flash('error', 'Bill not found [BPC6030].');
                redirect('bank-payment/create')->withInput();
            }
            if (!CommonFunction::bankTransactionStatus(Auth::user()->bank_branch_id)) {
                Session::flash('error', "Sorry! Currently, Transaction is not possible with this bank [BPC6035].");
                redirect('bank-payment/create')->withInput();
            }

            // payment mode related information is fetched from configuration table
            $payment_config = Configuration::where('caption', 'BANK_PAYMENT_PERIOD')->first();
            $now = Carbon::now();
            if (!($payment_config->value == 1 && ($payment_config->value2 <= $now && $payment_config->value3 >= $now))) {
                Session::flash('error', 'Payment mode is not valid [BPC6040].');
                redirect('bank-payment/create')->withInput();
            }
            if ((!$request->has('secret_key') || $bill_info->secret_key != $request->get('secret_key') || $bill_info->secret_key == 0)) {
                Session::flash('error', 'Invalid Pin Number for ' . $bill_info->invoice_no . ' or bill data has been changed [BPC6045].');
                return redirect('bank-payment')->withInput();
            }

            $last_updated_at = Encryption::decodeId($request->get('last_updated_at'));
            $last_updated_by = Encryption::decodeId($request->get('last_updated_by'));

            if ($last_updated_by != $bill_info->updated_by || $last_updated_at != $bill_info->updated_at) {
                Session::flash('error', 'Mean while bill information has been updated! [BPC6045].');
                return redirect('bank-payment/create')->withInput();
            }

            $amount_decoded = Encryption::decodeId($request->get('amount'));
            $last_month_arrears = Encryption::decodeId($request->get('last_month_arrears'));
            $net_payable_amount_decoded = Encryption::decodeId($request->get('net_payable_amount'));

            if ($bill_info->amount != $amount_decoded || $bill_info->last_month_arrears != $last_month_arrears ||
                    $bill_info->net_payable_amount != $net_payable_amount_decoded) {
                Session::flash('error', 'Bill amount is changed! [BPC5020], Please try again or contact with administration [BPC6050].');
                return redirect('bank-payment/create')->withInput();
            }

            // getting bill details info for payment search
            $billDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                    ->where('bill_info_details.bill_id', $bill_info->id)
                    ->get(['bill_info_details.*']);

            CommonFunction::createAuditLog('receive.bank-payment', $request, Auth::user()->id);
            $userBankBranchInfo = BankBranch::where('id', Auth::user()->bank_branch_id)->first();

            DB::beginTransaction();
            //updating payment information or status
            foreach ($billDetails as $billsData) {
                $appIds = explode(',', $billsData->app_ids);
                $updateServiceInfo = $BillPayment->updateEachSevicePaymentStatus($appIds, $billsData->service_id, 11);
                if ($updateServiceInfo) {
                    $billPaymentData = BillPaymentDetails::firstOrNew(['bill_id' => $bill_info->id,
                                'arrears_bill_id' => $bill_info->arrears_bill_id,
                                'service_id' => $billsData->service_id]);
                    $billPaymentData->bill_id = $bill_info->id;
                    $billPaymentData->app_ids = $billsData->app_ids;
                    $billPaymentData->service_id = $billsData->service_id;
                    $billPaymentData->service_quantity = $billsData->service_quantity;
                    $billPaymentData->per_amount = $billsData->per_amount;
                    $billPaymentData->total_amount = $billsData->total_amount;
                    $billPaymentData->bat_amount = $billsData->bat_amount;
                    $billPaymentData->payment_status_id = 11; //11=Pending
                    $billPaymentData->save();
                }
            }

            // update bill info data           
            $update = BillInfo::where('id', $bill_info->id)->update([
                'payment_status_id' => 11, //Payment Status ID [11-Pending]
                'ref_no' => $refNo,
                'receiving_branch_id' => $userBankBranchInfo->id,
                'received_by' => CommonFunction::getUserId(),
                'received_at' => Carbon::now()
            ]);

            // getting bill details info for payment search
            if ($bill_info->arrears_bill_id != 0) {
                $arrearBillDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                        ->where('bill_info_details.bill_id', $bill_info->arrears_bill_id)
                        ->get(['bill_info_details.*']);

                foreach ($arrearBillDetails as $arrearBillsData) {

                    $arrearAppIds = explode(',', $arrearBillsData->app_ids);
                    $updateServiceInfo = $BillPayment->updateEachSevicePaymentStatus($arrearAppIds, $arrearBillsData->service_id, 11);

                    if ($updateServiceInfo) {
                        $arrearsBillPaymentData = BillPaymentDetails::firstOrNew([
                                    'bill_id' => $bill_info->id,
                                    'arrears_bill_id' => $bill_info->arrears_bill_id,
                                    'service_id' => $arrearBillsData->service_id
                        ]);
                        $arrearsBillPaymentData->bill_id = $bill_info->id;
                        $arrearsBillPaymentData->app_ids = $arrearBillsData->app_ids;
                        $arrearsBillPaymentData->service_id = $arrearBillsData->service_id;
                        $arrearsBillPaymentData->service_quantity = $arrearBillsData->service_quantity;
                        $arrearsBillPaymentData->per_amount = $arrearBillsData->per_amount;
                        $arrearsBillPaymentData->total_amount = $arrearBillsData->total_amount;
                        $arrearsBillPaymentData->bat_amount = $arrearBillsData->bat_amount;
                        $arrearsBillPaymentData->payment_status_id = 11; //11=Pending
                        $arrearsBillPaymentData->save();
                    }
                }
            }

            if ($billPaymentData && $update > 0) {
                DB::commit();

                $applicantEmail = Users::leftJoin('project_clearance', 'users.id', '=', 'project_clearance.created_by')
                        ->where('project_clearance.created_by', $bill_info->project_clerance_id)
                        ->pluck('user_email');
                $cur_status = "Received, pending approval";
                $this->mailingProcess($bill_info->invoice_no, $applicantEmail, $cur_status);

                Session::flash('success', 'Bill Payment is received Successfully. But need to be approved [BPC6007].');
                return redirect('bank-payment');
            } else {
                DB::rollback();
                Session::flash('error', 'Unfortunately some error has been occured, Please try again [BPC6055].');
                return redirect('bank-payment');
            }
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', 'Payment Not Done, Please Try again [BPC5060]');
            return redirect('bank-payment');
        }
    }

    public function viewBill($_bill_id, $status = 0) {
        if (!ACL::getAccsessRight('bankPayment', 'V')) {
            die('You have no access right! This incidence will be reported. Contact with system admin for more information..');
        }
        try {
            $bill_id = Encryption::decodeId($_bill_id);
            $billType = BillInfo::where('id', $bill_id)->pluck('bill_type');


            // getting bill and company info
            if ($billType == 2) {
                $bill_info = BillInfo::leftJoin('visa_assistance as va', 'va.id', '=', 'bill_info.project_clearance_id')
                        ->leftjoin('pdf_queue as pq', function($join) {
                            $pdfType = 'beza.bill-generation.' . env('server_type');
                            $join->on('pq.app_id', '=', 'bill_info.id');
                            $join->on('pq.service_id', '=', DB::raw('0'));
                            $join->where('pq.pdf_type', '=', $pdfType);
                        })
                        ->where('bill_info.id', $bill_id)
                        ->whereIn('bill_info.payment_status_id', [11, 12, 2]) // 0 is unpaid, 2 = rejected
                        ->first([
                    'bill_info.*',
                    'va.tracking_number',
                    'va.correspondent_name as proposed_name',
                    'va.country',
                    'va.division',
                    'va.district',
                    'va.state',
                    'va.province',
                    'va.road_no',
                    'va.house_no',
                    'va.post_code',
                    'va.phone_home as phone',
                    'va.fax',
                    'va.email',
                    'va.website',
                    'pq.secret_key'
                ]);
            } elseif ($billType == 1 || $billType == 3) { // BillType 1 = PC, BillType 3= Others [Ep,IP,WP,VR]
                $bill_info = BillInfo::leftJoin('project_clearance', 'project_clearance.id', '=', 'bill_info.project_clearance_id')
                        ->leftjoin('pdf_queue as pq', function ($join) {
                            $pdfType = 'beza.bill-generation.' . env('server_type');
                            $join->on('pq.app_id', '=', 'bill_info.id');
                            $join->on('pq.service_id', '=', DB::raw('0'));
                            $join->where('pq.pdf_type', '=', $pdfType);
                        })
                        ->where('bill_info.id', $bill_id)
                        ->whereIn('bill_info.payment_status_id', [11, 12, 2])// 0 is unpaid, 2 = rejected
                        ->first([
                    'bill_info.*',
                    'project_clearance.tracking_number',
                    'project_clearance.proposed_name',
                    'project_clearance.country',
                    'project_clearance.division',
                    'project_clearance.district',
                    'project_clearance.state',
                    'project_clearance.province',
                    'project_clearance.road_no',
                    'project_clearance.house_no',
                    'project_clearance.post_code',
                    'project_clearance.phone',
                    'project_clearance.fax',
                    'project_clearance.email',
                    'project_clearance.website',
                    'pq.secret_key'
                ]);
            }

            if (!$bill_info) {
                Session::flash('error', 'Bill information went missing! [BPC6065].');
                return redirect('bank-payment')->withInput();
            }
            if (!CommonFunction::bankTransactionStatus(Auth::user()->bank_branch_id)) {
                Session::flash('error', "Sorry! Currently, Transaction is not possible with this bank [BPC6035].");
                redirect('bank-payment/create')->withInput();
            }

            // payment mode related information is fetched from configuration table
            $payment_config = Configuration::where('caption', 'BANK_PAYMENT_PERIOD')->first();
            $now = Carbon::now();
            if (!($payment_config->value == 1 && ($payment_config->value2 <= $now && $payment_config->value3 >= $now))) {
                Session::flash('error', 'Payment mode is not valid [BPC6040].');
                redirect('bank-payment/create')->withInput();
            }

            $dn1d = new DNS1D();
            $trackingNo = $bill_info->tracking_number; // tracking no push on barcode.
            if (!empty($trackingNo)) {
                $barcode = $dn1d->getBarcodePNG($trackingNo, 'C39');
                $barcode_url = 'data:image/png;base64,' . $barcode;
            } else {
                $barcode_url = '';
            }

            $billDetails = BillInfoDetails::leftJoin('service_info as si', 'si.id', '=', 'bill_info_details.service_id')
                    ->where('bill_info_details.bill_id', $bill_id)
                    ->orderBy('bill_info_details.service_id')
                    ->get(['si.name as service_name', 'bill_info_details.*']);
            $totalAvailedServices = 0;
            $serviceWiseTrackingNumbers = array();
            foreach ($billDetails as $data) {
                $service_id = 0;
                $service_id = $data->service_id;
                $explodedids = explode(',', $data->app_ids);

                $trackingNumbers = array();
                foreach ($explodedids as $app_id) {
                    $trackingNo = Processlist::where('service_id', $service_id)
                            ->where('record_id', $app_id)
                            ->pluck('track_no');
                    $trackingNumbers[] = $trackingNo;
                }

                $serviceWiseTrackingNumbers[$data->service_id] = $trackingNumbers;
                $totalAvailedServices += $data->service_quantity;
            }

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $divisions = ['' => 'Select One'] + Area::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $districts = ['' => 'Select One'] + Area::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            return view('BankPayment::bill-view', compact('_bill_id', 'bill_info', 'pdf_info', 'billDetails', 'countries', 'divisions', 'districts', 'barcode_url', 'totalAvailedServices', 'serviceWiseTrackingNumbers'));
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function approvePayment($id, Request $request, BillPaymentDetails $BillPayment) {
        if (!ACL::getAccsessRight('bankPayment', 'PA')) {
            die('You have no access right! Please contact with system admin for more information!');
        }

// checking payment approve mode
        $bill_id = Encryption::decodeId($id);
        try {
            $bill_info = BillInfo::leftJoin('project_clearance', 'project_clearance.id', '=', 'bill_info.project_clearance_id')
                    ->where('bill_info.id', $bill_id)
                    ->where('bill_info.payment_status_id', 11) // 11 = pending
                    ->first([
                'bill_info.*',
                'project_clearance.proposed_name',
                'project_clearance.country',
                'project_clearance.division',
                'project_clearance.district',
                'project_clearance.state',
                'project_clearance.province',
                'project_clearance.road_no',
                'project_clearance.house_no',
                'project_clearance.post_code',
                'project_clearance.phone',
                'project_clearance.fax',
                'project_clearance.email',
                'project_clearance.website'
            ]);

            if (!$bill_info) {
                Session::flash('error', 'Bill not found [BPC6010].');
                redirect('bank-payment/create')->withInput();
            }
            if (!CommonFunction::bankTransactionStatus(Auth::user()->bank_branch_id)) {
                Session::flash('error', "Sorry! Currently, Transaction is not possible with this bank [BPC6068].");
                redirect('bank-payment/create')->withInput();
            }

            // payment mode related information is fetched from configuration table
            $payment_config = Configuration::where('caption', 'BANK_PAYMENT_PERIOD')->first();
            $now = Carbon::now();
            if (!($payment_config->value2 <= $now && $payment_config->value3 >= $now)) {
                Session::flash('error', 'Payment mode is not valid [BPC6070].');
                redirect('bank-payment/create')->withInput();
            }

            // getting bill details info for payment search
            $billDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                    ->where('bill_info_details.bill_id', $bill_info->id)
                    ->get(['bill_info_details.*']);

            CommonFunction::createAuditLog('approve.bank-payment', $request, Auth::user()->id);
            DB::beginTransaction();

            //updating payment information or status
            $update = BillInfo::where('id', $bill_info->id)
                    ->update([
                'payment_status_id' => 12, //Approved
                'confirmed_by' => CommonFunction::getUserId(),
                'confirmed_at' => Carbon::now()
            ]);

            foreach ($billDetails as $billsData) {
                $appIds = explode(',', $billsData->app_ids);

                $BillPayment->updateEachSevicePaymentStatus($appIds, $billsData->service_id, 12);
                BillPaymentDetails::where([
                            'bill_id' => $bill_info->id,
                            'arrears_bill_id' => $bill_info->arrears_bill_id,
                            'service_id' => $billsData->service_id])
                        ->update(['payment_status_id' => 12]);
            }

            // getting bill details info for arrears
            if ($bill_info->arrears_bill_id != 0) {
                $arrearBillDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                        ->where('bill_info_details.bill_id', $bill_info->arrears_bill_id)
                        ->get(['bill_info_details.*']);

                foreach ($arrearBillDetails as $arrearBillsData) {
                    $arrearAppIds = explode(',', $arrearBillsData->app_ids);
                    $BillPayment->updateEachSevicePaymentStatus($arrearAppIds, $arrearBillsData->service_id, 12);

                    BillPaymentDetails::where([
                                'bill_id' => $bill_info->id,
                                'arrears_bill_id' => $bill_info->arrears_bill_id,
                                'service_id' => $arrearBillsData->service_id
                            ])
                            ->update(['payment_status_id' => 12]);
                }
            }

            if ($update > 0) {

                if (count($bill_info->id) > 0) {
                    $this->moneyReceiptPdf($bill_info->id);
                }

                DB::commit();

                $applicantEmail = Users::leftJoin('project_clearance', 'users.id', '=', 'project_clearance.created_by')
                        ->where('project_clearance.created_by', $bill_info->project_clerance_id)
                        ->pluck('user_email');
                $cur_status = "Approved";
                $this->mailingProcess($bill_info->invoice_no, $applicantEmail, $cur_status);

                Session::flash('success', 'Bill Payment is approved Successfully! [BPC6017].');
                return redirect('bank-payment');
            } else {
                DB::rollback();
                Session::flash('error', 'Unfortunately some error has been occured [BPC6075].');
                return redirect('bank-payment');
            }
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', 'Payment Not Done, Please Try again [BPC6075].');
            return redirect('bank-payment');
        }
    }

    public function rejectPayment($id, BillPaymentDetails $BillPayment, Request $request) {
        if (!ACL::getAccsessRight('bankPayment', 'PR')) {
            die('You have no access right! Please contact with system admin for more information!');
        }
//      checking payment approve mode
        $bill_id = Encryption::decodeId($id);
        try {
            $bill_info = BillInfo::leftJoin('project_clearance', 'project_clearance.id', '=', 'bill_info.project_clearance_id')
                    ->where('bill_info.id', $bill_id)
                    ->where('bill_info.payment_status_id', 11) // 11 = pending
                    ->first([
                'bill_info.*',
                'project_clearance.proposed_name',
                'project_clearance.country',
                'project_clearance.division',
                'project_clearance.district',
                'project_clearance.state',
                'project_clearance.province',
                'project_clearance.road_no',
                'project_clearance.house_no',
                'project_clearance.post_code',
                'project_clearance.phone',
                'project_clearance.fax',
                'project_clearance.email',
                'project_clearance.website'
            ]);

            if (!$bill_info) {
                Session::flash('error', 'Bill not found [BPC6080].');
                redirect('bank-payment/create')->withInput();
            }
            if (!CommonFunction::bankTransactionStatus(Auth::user()->bank_branch_id)) {
                Session::flash('error', "Sorry! Currently, Transaction is not possible with this bank [BPC6085].");
                redirect('bank-payment/create')->withInput();
            }

            // payment mode related information is fetched from configuration table
            $payment_config = Configuration::where('caption', 'BANK_PAYMENT_PERIOD')->first();
            $now = Carbon::now();
            if (!($payment_config->value2 <= $now && $payment_config->value3 >= $now)) {
                Session::flash('error', 'Payment mode is not valid [BPC6090].');
                redirect('bank-payment/create')->withInput();
            }

            // getting bill details info for payment search
            $billDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                    ->where('bill_info_details.bill_id', $bill_info->id)
                    ->get(['bill_info_details.*']);

            CommonFunction::createAuditLog('reject.bank-payment', $request, Auth::user()->id);

            DB::beginTransaction();

            //updating payment information or status
            $update = BillInfo::where('id', $bill_info->id)
                    ->update([
                'payment_status_id' => 2, // Reject
                'confirmed_by' => CommonFunction::getUserId(),
                'confirmed_at' => Carbon::now()
            ]);

            foreach ($billDetails as $billsData) {
                $appIds = explode(',', $billsData->app_ids);

                $BillPayment->updateEachSevicePaymentStatus($appIds, $billsData->service_id, 2);
                BillPaymentDetails::where('bill_id', $bill_info->id)->update(['payment_status_id' => 2]);
            }

            // getting bill details info for arrears
            if ($bill_info->arrears_bill_id != 0) {
                $arrearBillDetails = BillInfoDetails::leftJoin('bill_info', 'bill_info.id', '=', 'bill_info_details.bill_id')
                        ->where('bill_info_details.bill_id', $bill_info->arrears_bill_id)
                        ->get(['bill_info_details.*']);

                foreach ($arrearBillDetails as $arrearBillsData) {
                    $arrearAppIds = explode(',', $arrearBillsData->app_ids);
                    $BillPayment->updateEachSevicePaymentStatus($arrearAppIds, $arrearBillsData->service_id, 2);

                    BillPaymentDetails::where([
                                'bill_id' => $bill_info->id,
                                'arrears_bill_id' => $bill_info->arrears_bill_id,
                                'service_id' => $arrearBillsData->service_id
                            ])
                            ->update(['payment_status_id' => 2]);
                }
            }

            if ($update > 0) {
                DB::commit();

                $applicantEmail = Users::leftJoin('project_clearance', 'users.id', '=', 'project_clearance.created_by')
                        ->where('project_clearance.created_by', $bill_info->project_clerance_id)
                        ->pluck('user_email');
                $cur_status = "Payment Rejected";
                $this->mailingProcess($bill_info->invoice_no, $applicantEmail, $cur_status);

                Session::flash('success', 'Bill Payment has been rejected successfully! [BPC6027].');
                return redirect('bank-payment');
            } else {
                DB::rollback();
                Session::flash('error', 'Unfortunately some error has been occured [BPC6100].');
                return redirect('bank-payment');
            }
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', 'Payment Not Done, Please Try again [BPC6105].');
            return redirect('bank-payment');
        }
    }

    public function mailingProcess($bill_invoice, $applicantEmail, $cur_status) {

//        if ($cur_status == 'Approved') { // 12 = payment approved
//            $smsbody = 'Your bill for BEZA with Invoice Number: ' . $bill_invoice . ' is now in status: <b>' . $cur_status . '</b>';
//            $applicantMobile = Users::where('user_email', $applicantEmail)->pluck('user_phone');
//            $params = array([
//                    'mobileNo' => '01745949926',
//                    'smsYes' => '1',
//                    'smsBody' => $smsbody
//            ]);
//            CommonFunction::sendMessageFromSystem($params);
//        }

        if ($cur_status == 'Approved') { // 12 = payment approved
            $notification = new Notification();
            $applicantMobile = Users::where('user_email', $applicantEmail)->pluck('user_phone');
            // to send SMS from system
            $notification->sendPaymentSms($bill_invoice, $cur_status, $applicantMobile);
        }

        $body_msg = '<span style="color:#000;text-align:justify;"><b>';
        $body_msg .= 'Your bill for BEZA with Invoice Number: ' . $bill_invoice . ' is now in status: <b>' . $cur_status . '</b>';
        $body_msg .= '</span>';
        $body_msg .= '<br/><br/><br/>Thanks<br/>';
        $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

        $data = array(
            'header' => 'Bill information has been updated',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function ($message) use ($applicantEmail) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                    ->to($applicantEmail)
                    ->subject('Monthly bill payment related information from BEZA');
        });
    }

    public function moneyReceiptPdf($billId) {
        try {
            $billType = BillInfo::where('bill_info.id', $billId)->pluck('bill_type');

            if ($billType == 1 || $billType == 3) { // 1 =pc, 3 =others
                $billInfo = BillInfo::leftJoin('project_clearance as pc', 'bill_info.project_clearance_id', '=', 'pc.id');
            } else if ($billType == 2) { // 2 = va
                $billInfo = BillInfo::leftJoin('visa_assistance as va', 'bill_info.project_clearance_id', '=', 'va.id');
            }
            $billInfo = $billInfo->leftjoin('pdf_queue as pq', function($join) {
                        $pdfType = 'beza.bill-generation.' . env('server_type');
                        $join->on('pq.app_id', '=', 'bill_info.id');
                        $join->on('pq.service_id', '=', DB::raw('0'));
                        $join->where('pq.pdf_type', '=', $pdfType);
                    })
                    ->where('bill_info.id', $billId);

            if ($billType == 1 || $billType == 3) {
                $billInfo = $billInfo->first(['bill_info.*',
                    'pc.tracking_number', 'pc.proposed_name', 'pc.country', 'pc.division', 'pc.district', 'pc.state', 'pc.province', 'pc.road_no', 'pc.house_no',
                    'pc.post_code', 'pc.phone', 'pc.fax', 'pc.email', 'pc.website', 'pq.secret_key']);
            } else if ($billType == 2) {
                $billInfo = $billInfo->first(['bill_info.*',
                    'va.tracking_number', 'va.correspondent_name as proposed_name', 'va.country', 'va.division', 'va.district', 'va.state', 'va.province',
                    'va.road_no', 'va.house_no', 'va.post_code', 'va.phone_home as phone', 'va.fax', 'va.email', 'va.website', 'pq.secret_key']);
            }
            if ($billInfo->net_payable_amount) {
                $amountWord = CommonFunction::convert_number_to_words($billInfo->net_payable_amount);
            } else {
                $amountWord = '';
            }
            $countries = Countries::lists('name', 'iso');
            $divisions = Area::where('area_type', 1)->lists('area_nm', 'area_id')->all();
            $districts = Area::where('area_type', 2)->lists('area_nm', 'area_id')->all();

            $billDetails = BillInfoDetails::leftJoin('service_info as si', 'si.id', '=', 'bill_info_details.service_id')
                    ->where('bill_info_details.bill_id', $billInfo->id)
                    ->orderBy('bill_info_details.service_id')
                    ->get(['si.name as service_name', 'bill_info_details.*']);

            $totalAvailedServices = 0;
            $serviceWiseTrackingNumbers = array();
            foreach ($billDetails as $data) {
                $service_id = 0;
                $service_id = $data->service_id;
                $explodedids = explode(',', $data->app_ids);

                $trackingNumbers = array();
                foreach ($explodedids as $app_id) {
                    $trackingNo = Processlist::where('service_id', $service_id)
                            ->where('record_id', $app_id)
                            ->pluck('track_no');
                    $trackingNumbers[] = $trackingNo;
                }

                $serviceWiseTrackingNumbers[$data->service_id] = $trackingNumbers;
                $totalAvailedServices += $data->service_quantity;
            }

            $dn1d = new DNS1D();
            $trackingNo = $billInfo->tracking_number; // tracking no push on barcode.
            if (!empty($trackingNo)) {
                $barcode = $dn1d->getBarcodePNG($trackingNo, 'C39');
                $barcode_url = 'data:image/png;base64,' . $barcode;
            } else {
                $barcode_url = '';
            }

            $bankInfo = Bank::leftJoin('bank_branches as bb', 'bank.id', '=', 'bb.bank_id')
                    ->leftJoin('users', function($join) {
                        $join->on('users.user_sub_type', '=', 'bank.id');
                        $join->on('users.bank_branch_id', '=', 'bb.id');
                    })
                    ->where('bank.id', Auth::user()->user_sub_type)
                    ->where('bb.id', Auth::user()->bank_branch_id)
                    ->first(['bank.name as bank_name', 'bb.branch_name', 'users.user_full_name']);

            $qrCodeGenText = $billInfo->tracking_number . '-' . $billInfo->proposed_name . '-' . $billInfo->invoice_no;
            $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
            $qrUrl = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";

            $content = view("BankPayment::receipt-pdf", compact(
                            'barcode_url', 'billDetails', 'billInfo', 'amountWord', 'totalAvailedServices', 'serviceWiseTrackingNumbers', 'countries', 'divisions', 'districts', 'bankInfo', 'qrUrl'))
                    ->render();

            $mpdf = new mPDF(
                    'utf-8', // mode - default ''
                    'A4', // format - A4, for example, default ''
                    11, // font size - default 0
                    'Times New Roman', // default font family
                    10, // margin_left
                    10, // margin right
                    10, // margin top
                    15, // margin bottom
                    10, // margin header
                    9, // margin footer
                    'P'
            );

            $mpdf->Bookmark('Start of the document');
            $mpdf->useSubstitutions;
            $mpdf->SetProtection(array('print'));
            $mpdf->SetDefaultBodyCSS('color', '#000');
            $mpdf->SetTitle("BEZA");
            $mpdf->SetSubject("Subject");
            $mpdf->SetAuthor("Business Automation Limited");
            $mpdf->autoScriptToLang = true;
            $mpdf->baseScript = 1;
            $mpdf->autoVietnamese = true;
            $mpdf->autoArabic = true;

            $mpdf->autoLangToFont = true;
            $mpdf->SetDisplayMode('fullwidth');
            $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
            $mpdf->setAutoTopMargin = 'stretch';
            $mpdf->setAutoBottomMargin = 'stretch';

            $mpdf->setWatermarkImage('assets/images/beza_watermark.png');
            $mpdf->showWatermarkImage = true;

            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->WriteHTML($content, 2);

            $mpdf->defaultfooterfontsize = 10;
            $mpdf->defaultfooterfontstyle = 'B';
            $mpdf->defaultfooterline = 0;
            $mpdf->setFooter('{PAGENO} / {nb}');

            $mpdf->SetCompression(true);
            $billYear = date('Y', strtotime($billInfo->bill_month));
            $baseURL = "bill_documents/";
            $basePath = date("Y/m", strtotime($billInfo->bill_month));

            $directoryName = $baseURL . $basePath . '/' . $billInfo->bill_type;
            $directoryNameYear = $baseURL . $billYear;

            if (!file_exists($directoryName)) {
                $oldmask = umask(0);
                mkdir($directoryName, 0777, true);
                umask($oldmask);
                $f = fopen($directoryName . "/index.html", "w");
                fclose($f);
                if (!file_exists($directoryNameYear . "/index.html")) {
                    $f = fopen($directoryNameYear . "/index.html", "w");
                    fclose($f);
                }
            }
            $doc_name = 'money_receipt' . $billInfo->tracking_number;
            $pdfFilePath = $directoryName . "/" . $doc_name . '.pdf';
            $mpdf->Output($pdfFilePath, 'F');   // Saving pdf "F" for Save only, "I" for view only.

            $pdfType = 'beza.money-receipt.' . env('server_type');
            $pdfQueue = pdfQueue::firstOrNew(['app_id' => $billInfo->id, 'service_id' => 0, 'pdf_type' => $pdfType]);
            $pdfQueue->attachment = $pdfFilePath;
            $pdfQueue->app_id = $billInfo->id;
            $pdfQueue->service_id = 0; // For Bill generation, Service ID is 0
            $pdfQueue->status = 1;
            $pdfQueue->attachment = $pdfFilePath;
            $pdfQueue->save();
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BP5008].');
            return Redirect::back()->withInput();
        }
    }

    /*     * ********************** Controller Class ends here *************************** */
}
