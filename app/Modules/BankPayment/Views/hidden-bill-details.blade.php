
{!! Form::hidden('last_updated_at', Encryption::encodeId($bill_info->updated_at)) !!}
{!! Form::hidden('last_updated_by', Encryption::encodeId($bill_info->updated_by)) !!}


<input type="hidden" name="amount" value="{{Encryption::encodeId($bill_info->amount)}}">
<input type="hidden" name="last_month_arrears" value="{{Encryption::encodeId($bill_info->last_month_arrears)}}">
<input type="hidden" name="net_payable_amount" value="{{Encryption::encodeId($bill_info->net_payable_amount)}}">


{!! Form::hidden('invoice_no', Encryption::encodeId($bill_info->invoice_no)) !!}