@extends('layouts.admin')

@section('content')

@include('partials.messages')

<?php
$accessMode = ACL::getAccsessRight('bankPayment');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no right to access this panel! This instance will be reported. Please contact with system admin for further investigation!');
}
?>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel panel-heading"><strong>Details of Bill </strong></div>
        <div class="panel-body">

            @if(isset($billDetails))
            <div class="panel panel-primary">
                <div class="panel-body">
                    <!--to show the previously generated bill information-->
                    @include('BankPayment::searched-invoice')
                </div>  <!-- /.panel-body -->
            </div>  <!-- /.panel -->

            @if(Auth::user()->user_type == '11x111'){{-- Only bank user can received payment --}}

            @if(count($billDetails) > 0 && $bill_info->payment_status_id == 11)

                    <div class="col-md-6">
                        {!! Form::open(['url' => 'bank-payment/reject-payment/'.$_bill_id, 'method' => 'post','id' => 'payment_reject'])!!}

                        <!--for the necessary bill relatedinformation-->
                        @include('BankPayment::hidden-bill-details')

                        <div class="text-center">
                            @if(ACL::getAccsessRight('bankPayment','PR'))
                            <button type="submit" class="btn btn-danger btn-md getPayment" title="Reject the payment of this bill">
                                <i class="fa fa-times-circle"></i> <strong>Reject</strong>
                            </button>
                            @endif {{--checking ACL --}}
                        </div>
                        {!! Form::close()!!}
                    </div>

                    <div class="col-md-6 text-right">
                        {!! Form::open(['url' => 'bank-payment/approve-payment/'.$_bill_id,'method' => 'post','id' => 'payment_approve'])!!}

                        <!--for the necessary bill relatedinformation-->
                        @include('BankPayment::hidden-bill-details')

                        <div class="text-center">
                            @if(ACL::getAccsessRight('bankPayment','PA'))
                            <button type="submit" class="btn btn-success btn-md getPayment" title="Approve  the payment of this bill">
                                <i class="fa fa-check-circle"></i> <strong>Approve</strong>
                            </button>
                            @endif {{--checking ACL --}}
                        </div>
                        {!! Form::close()!!}
                    </div>


            @else {{-- $billDetails < 0 no bill application found --}}
            <div class="alert alert-warning">
                <div class="text-warning text-center">
                    {!!Html::image('assets/images/warning.png') !!}
                    <b>The bill has no application attached to it!</b><br/><br/>
                </div>
            </div>
            @endif {{-- checking $billDetails --}}

            @else {{-- Auth::user()->user_type != '11x111'  --}}
            <div class="alert alert-warning">
                <div class="text-warning text-center">
                    {!!Html::image('assets/images/warning.png') !!}
                    <br/>
                    <b>Payment can be collected by  bank users only</b>
                </div>
            </div>
            @endif {{-- checking Auth::user()->user_type == '11x111'  --}}

            @endif {{-- isset($payments_info) --}}

        </div>  <!-- /.panel-body -->
    </div>  <!-- /.panel -->

</div>  <!-- /.col-lg-12 -->

@endsection {{-- content --}}

@section('footer-script')

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

@include('partials.datatable-scripts')
<script>
    $('#approveBtn').on('click', function () {
        if ($(".i_agree_checkbox").is(':checked') == false) {
            $(".i_agree_checkbox").parent().addClass('error');
            return false;
        } else {
            $(".i_agree_checkbox").parent().removeClass('error');
            return true;
        }
    });
    });
</script>
@endsection <!--- footer-script--->