<div class="col-md-8 col-md-offset-2">
    @if(Auth::user()->user_type == '11x111'){{-- Only bank user can received payment --}}

    @if(count($billDetails) > 0)

    <div class="panel panel-primary">
        <div class="panel-heading text-center"> <strong>Bill Confirmation</strong></div> 

        {!! Form::open(['url' => 'bank-payment/receive-bill/','method' => 'post','id' => 'payment_done'])!!}

        {!! Form::hidden('last_updated_at', Encryption::encodeId($bill_info->updated_at)) !!}
        {!! Form::hidden('last_updated_by', Encryption::encodeId($bill_info->updated_by)) !!}

        <div class="panel-body">

            <input type="hidden" name="amount" value="{{Encryption::encodeId($bill_info->amount)}}">
            <input type="hidden" name="last_month_arrears" value="{{Encryption::encodeId($bill_info->last_month_arrears)}}">
            <input type="hidden" name="net_payable_amount" value="{{Encryption::encodeId($bill_info->net_payable_amount)}}">

            <div class="clearfix"></div>
            <div class="col-md-12" style="border-top: 1px solid silver;padding-top: 5px;">
                <label class="pull-left text-left">Transaction Ref. No/Scroll No.: </label>
                <span class="pull-right text-right">
                    {!! Form::text('ref_no', '',['class'=>'form-control required input-sm']) !!}
                </span>
            </div>

            @if(isset($bill_info->secret_key) && $bill_info->secret_key !=0 )
            <div class="clearfix"></div><br/>
            <div class="col-md-12" style="border-top: 1px solid silver;padding-top: 5px;">
                <label class="pull-left text-left">Pin:</label>
                <span class="pull-right text-right">
                    {!! Form::text('secret_key', '',['class'=>'form-control required input-sm']) !!}
                </span>
            </div>
            @else {{-- $pin_code == 0 --}}
            <span style="color: red;">  This bill  has been changed, please ask for the revised bill! </span>
            @endif {{-- pin code --}}

            <div class="clearfix"><br/></div>
            <div class="clearfix"><br/></div>
            <div class="col-md-12 text-center" style="border-top: 1px solid silver;padding-top: 5px;">
                Some instructional text will be given later by BEZA.
            </div>

            <div class="clearfix"><br/></div>

            <div class="col-md-12" style="border-top: 1px solid silver;padding-top: 5px;">
                <div class="text-center">                    
                    All information provided in the Bill Invoice No - {!! $bill_info->invoice_no !!} is correct.
                    <input type="checkbox" class=" i_agree_checkbox" id="i_agree_all">
                    <label for="i_agree_all">I Agree</label>
                </div>
            </div>
        </div>

        <div class="panel-footer text-center">
            @if(ACL::getAccsessRight('bankPayment','PS'))
            {!! Form::hidden('bill_id', Encryption::encodeId($bill_info->id)) !!}
            <button type="submit" class="btn btn-success btn-md getPayment" title="Payment Submit">
                <i class="fa fa-check"></i> <strong>Payment</strong>
            </button>
            @endif {{--checking ACL --}}
        </div>
        {!! Form::close()!!}
    </div>

    @else {{-- $billDetails < 0 no bill application found --}}
    <div class="alert alert-warning">
        <div class="text-warning text-center">
            {!!Html::image('assets/images/warning.png') !!}
            <b>The bill has no application attached to it!</b><br/><br/>
        </div>
    </div>
    @endif {{-- checking $billDetails --}}

    @else {{-- Auth::user()->user_type != '11x111'  --}}
    <div class="alert alert-warning">
        <div class="text-warning text-center">
            {!!Html::image('assets/images/warning.png') !!}
            <br/>
            <b>Payment can be collected by  bank users only</b>
        </div>
    </div>
    @endif {{-- checking Auth::user()->user_type == '11x111'  --}}
</div>