<section class="content">
    <div class="panel panel-red">
        <div class="panel-heading">
            <strong> Bill Invoice</strong>
        </div><!-- /.panel-heading -->
        <div class="panel-body">

            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="/assets/images/beza_watermark.png" alt="BEZA" width="80px"/>
                    <h5><strong>BEZA::Bangladesh Economic Zone Authority</strong></h5>
                    <h5><b>INVOICE</b></h5>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                    <div class="panel panel-red">
                        <div class="panel-body">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <strong>Tracking no. : </strong>
                                        <span>{{ (!empty($bill_info->tracking_number)?$bill_info->tracking_number:'N/A') }}</span>
                                    </td>
                                    <td>
                                        <strong>Bill Date :</strong>
                                        <span>{{ (!empty($bill_info->bill_generation_date)?$bill_info->bill_generation_date:'N/A') }}</span>
                                    </td>
                                    <td><strong>Invoice No. : </strong>
                                        <span>{{ (isset($bill_info->invoice_no)?$bill_info->invoice_no:'N/A') }}</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center"><strong> Applicant Information</strong></div>
                        <div class="panel-body">
                            <table width="100%">
                                <tr>
                                    <td style="padding: 5px;" width="25%"><strong>Applying firm or Company :</strong> </td>
                                    <td style="padding: 5px;" width="25%"><span>
                                            {{ (isset($bill_info->proposed_name)?$bill_info->proposed_name:'N/A') }}</span></td>
                                    <td style="padding: 5px;" width="25%"></td>
                                    <td style="padding: 5px;" width="25%"></td>
                                </tr>
                            </table>

                            <table width="100%">
                                <tr>
                                    <td style="padding: 5px;">
                                        <strong>Full Address of Registered Head Office of Applicant / Applying Firm or Company :</strong>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%">
                                <tr>
                                    <td style="padding: 5px;" width="25%"><strong>Country :</strong> </td>
                                    <td style="padding: 5px;" width="25%"><span>
                                            {{ (isset($countries[$bill_info->country]) && isset($bill_info->country)) ? $countries[$bill_info->country]: '' }}</span></td>

                                    @if($bill_info->country == 'BD')
                                    <td style="padding: 5px;" width="25%"><strong>Division :</strong> </td>
                                    <td style="padding: 5px;" width="25%"><span>
                                            {{ (isset($divisions[$bill_info->division])  && $bill_info->division !=0 )?$divisions[$bill_info->division]:'' }}</span></td>
                                    @else
                                    <td style="padding: 5px;" width="25%"><strong>State :</strong> </td>
                                    <td style="padding: 5px;" width="25%"><span>
                                            {{ (isset($bill_info->state)? $bill_info->state :'') }}</span></td>
                                    @endif
                                </tr>

                                <tr>
                                    @if($bill_info->country == 'BD')
                                    <td style="padding: 5px;"><strong>District :</strong> </td>
                                    <td style="padding: 5px;"><span>
                                            {{ (isset($districts[$bill_info->district]) && $bill_info->district != 0) ?$districts[$bill_info->district]:'' }}</span></td>
                                    @else
                                    <td style="padding: 5px;" width="25%"><strong>Province :</strong> </td>
                                    <td style="padding: 5px;" width="25%"><span>
                                            {{ (isset($bill_info->province)?$bill_info->province:'') }}</span></td>
                                    @endif
                                    <td style="padding: 5px;"><strong>Address Line 1 :</strong> </td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->road_no)?$bill_info->road_no:'N/A') }}</span></td>
                                </tr>

                                <tr>
                                    <td style="padding: 5px;"><strong>Address Line 2 :</strong> </td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->house_no)?$bill_info->house_no:'N/A') }}</span></td>
                                    <td style="padding: 5px;"><strong>Post Code :</strong> </td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->post_code)?$bill_info->post_code:'N/A') }}</span></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px;"><strong>Phone No : </strong></td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->phone)?$bill_info->phone:'N/A') }}</span></td>
                                    <td style="padding: 5px;"><strong>Fax No : </strong></td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->fax)?$bill_info->fax:'N/A') }}</span></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px;"><strong>Email : </strong></td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->email)?$bill_info->email:'N/A') }}</span></td>
                                    <td style="padding: 5px;"><strong>Website : </strong></td>
                                    <td style="padding: 5px;"><span>{{ (!empty($bill_info->website)?$bill_info->website:'N/A') }}</span></td>
                                </tr>
                            </table>
                        </div>

                        <div class="panel panel-green">
                            <div class="panel-body">
                                <table width="100%">
                                    <tr>
                                        <td class="text-center"><strong>Last Payment Date : </strong><span> {{ (!empty($bill_info->bill_expired_date)?$bill_info->bill_expired_date:'N/A') }}</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <table width="100%">
                            <tr>
                                <td style="padding: 5px;" class="text-center"><strong>Billing Month : </strong> {{ (isset($bill_info->bill_month)?
                                      date("F-Y", strtotime($bill_info->bill_month))  :'') }}</td>
                                <td style="padding: 5px;" class="text-center"><strong>Total Availed Services : </strong>
                                    {{ (isset($totalAvailedServices)?$totalAvailedServices:'') }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th width="20%" style="padding: 5px; text-align: center;" width="25%">Service Description</th>
                            <th width="15%" style="padding: 5px; text-align: center;">Amt.Per Serve</th>
                            <th style="padding: 5px; text-align: center;">Service Details</th>
                            <th width="10%" style="padding: 5px; text-align: center;">QTY</th>
                            <th style="padding: 5px; text-align: center;">Total</th>
                        </tr>
                        @foreach($billDetails as $data)
                        <tr>
                            <td style="padding: 5px; text-align: left;">{{ $data->service_name }}</td>
                            <td style="padding: 5px; text-align: right;">{{ $data->per_amount }}</td>
                            <td style="padding: 5px;"></td>
                            <td style="padding: 5px; text-align: right;">{{ $data->service_quantity }}</td>
                            @if($data->service_quantity>0)

                            <td rowspan="2" style="padding: 5px; text-align: right;">{{ $data->total_amount }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" colspan="4">
                                <span>{{ implode(', ',$serviceWiseTrackingNumbers[$data->service_id]) }}</span>
                            </td>
                        </tr>
                        @else
                        <td style="padding: 5px; text-align: right;">{{ $data->total_amount }}</td>
                        </tr>
                        @endif
                        @endforeach
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">Sub Total : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;"><strong>{{ (!empty($bill_info->sub_total) ? number_format($bill_info->sub_total,2):'') }}</strong></td>
                        </tr>
                        <tr class="alert alert-success">
                            <td style="padding: 5px; background-color: yellowgreen;" colspan="5">
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">Arrears : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;"><span>{{ (!empty($bill_info->last_month_arrears)? number_format($bill_info->last_month_arrears,2) : '') }}</span></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">Others Charge : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;"><span>{{ (!empty($bill_info->other_charges) ? number_format($bill_info->other_charges,2) : '') }}</span></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">TAX & VAT : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;"><span>{{ (!empty($bill_info->tax_vat) ? number_format($bill_info->tax_vat,2) : '') }}</span></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px; background-color: yellowgreen" colspan="5"></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">Total : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;"><strong>{{ (!empty($bill_info->amount) ? number_format($bill_info->amount,2):'') }}</strong></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px; background: yellowgreen" colspan="5"></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">Late Payment Fee : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;">
                                <span>
                                    {{ (!empty($bill_info->late_payment_fee)? number_format($bill_info->late_payment_fee,2) : '') }}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;" colspan="4">
                                <strong style="float: left;">Net Payable Amount : </strong>
                            </td>
                            <td style="padding: 5px; text-align: right;"><strong>{{ (!empty($bill_info->net_payable_amount) ? number_format($bill_info->net_payable_amount,2):'') }}</strong></td>
                        </tr>
                    </table>
                    <br/>

                    <span style="font-size:12px;">
                        <strong>Total Amount in Words : </strong> {{ (isset($amountWord)? ucfirst($amountWord) : 
                            '.......................................................................................................................................................................') }}
                    </span>

                    <table width="100%">
                        <tr><td><br/></td></tr>
                        <tr>
                            <td class="text-center">
                                <br/>
                                <img src="{{ $barcode_url }}" alt="Barcode">
                            </td>
                        </tr>
                        <tr><td><br/></td></tr>
                        <tr>
                            <td  class="text-center">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        THIS IS A COMPUTER GENERATED INVOICE/MONEY RECEIPT. SO NO SIGNATURE IS REQUIRED.
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
