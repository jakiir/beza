@extends('layouts.admin')

@section('content')

@include('partials.messages')

<?php
$accessMode = ACL::getAccsessRight('bankPayment');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no right to access! This incidence will be reported! Please contact for system administration for more information');
}
?>

<div class="col-lg-12">
    <div class="panel panel panel-red">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-list"></i> <strong>List of Bills</strong>
                </div>
                <div class="col-md-6 text-right">
                    @if(Auth::user()->user_type == '11x111')
                    <a class="" href="{{ url('/bank-payment/create') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> <strong>Receive Payment </strong>', array('type' => 'button',
                        'class' => 'btn btn-md btn-default')) !!}
                    </a>
                    @endif
                </div>
            </div>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">

            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab_1" aria-expanded="true">
                            {!!Html::image('assets/images/warning.png') !!}
                            Pending Bills
                        </a>
                    </li>
                    <li class="results_tab">
                        <a data-toggle="tab" href="#tab_2" aria-expanded="false">
                            {!!Html::image('assets/images/ok.png') !!}
                            Approved Bills
                        </a>
                    </li>
                    <li class="results_tab">
                        <a data-toggle="tab" href="#tab_3" aria-expanded="false">
                            <img src="/assets/images/alarm_clock_time_bell_wait-512.png" height="35" widht="35"/>
                            Rejected Bills
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="tab_1" class="tab-pane active">
                        <div class="table-responsive">
                            <table id="pending_list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                <thead class="alert alert-info">
                                    <tr>
                                        <th># </th>
                                        <th>Invoice No. </th>
                                        <th>Payment Status</th>
                                        <th>Bill Type</th>
                                        <th>Bill Month</th>
                                        <th>No. of Services</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div><!-- /.tab-pane -->

                    <div id="tab_2" class="tab-pane">
                        <div class="results">
                            <div class="table-responsive">
                                <table id="approved_list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead class="alert alert-success">
                                        <tr>
                                            <th># </th>
                                            <th>Invoice No. </th>
                                            <th>Payment Status</th>
                                            <th>Bill Type</th>
                                            <th>Bill Month</th>
                                            <th>No. of Services</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->

                    <div id="tab_3" class="tab-pane">
                        <div class="results">
                            <div class="table-responsive">
                                <table id="rejected_list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead class="alert alert-warning">
                                        <tr>
                                            <th># </th>
                                            <th>Invoice No. </th>
                                            <th>Payment Status</th>
                                            <th>Bill Type</th>
                                            <th>Bill Month</th>
                                            <th>No. of Services</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
            </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
    </div><!-- /.panel -->
</div><!-- /.col-lg-12 -->
@endsection

@section('footer-script')
@include('partials.datatable-scripts')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<script>
    $(function () {
        $('#pending_list').DataTable({
            processing: true,
            serverSide: true,
            "paging": true,
            "lengthChange": true,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 50,
            ajax: {
                url: '{{url("bank-payment/get-bank-pendling-bills")}}',
                method: 'post',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'rownum', name: 'rownum'},
                {data: 'invoice_no', name: 'invoice_no'},
                {data: 'payment_status', name: 'payment_status'},
                {data: 'bill_type', name: 'bill_type'},
                {data: 'bill_month', name: 'bill_month'},
                {data: 'no_of_services', name: 'no_of_services'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });

        $('#approved_list').DataTable({
            processing: true,
            serverSide: true,
            "paging": true,
            "lengthChange": true,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 50,
            ajax: {
                url: '{{url("bank-payment/get-bank-approved-bills")}}',
                method: 'post',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'rownum', name: 'rownum'},
                {data: 'invoice_no', name: 'invoice_no'},
                {data: 'payment_status', name: 'payment_status'},
                {data: 'bill_type', name: 'bill_type'},
                {data: 'bill_month', name: 'bill_month'},
                {data: 'no_of_services', name: 'no_of_services'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });

        $('#rejected_list').DataTable({
            processing: true,
            serverSide: true,
            "paging": true,
            "lengthChange": true,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 50,
            ajax: {
                url: '{{url("bank-payment/get-bank-rejected-bills")}}',
                method: 'post',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'rownum', name: 'rownum'},
                {data: 'invoice_no', name: 'invoice_no'},
                {data: 'payment_status', name: 'payment_status'},
                {data: 'bill_type', name: 'bill_type'},
                {data: 'bill_month', name: 'bill_month'},
                {data: 'no_of_services', name: 'no_of_services'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });

    }); // end of function
</script>
@endsection
