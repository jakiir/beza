@extends('layouts.admin')

@section('content')

<?php
$accessMode = ACL::getAccsessRight('bankPayment');
if (!ACL::isAllowed($accessMode, 'PS')) {
    die('You have no right to access this panel! This instance will be reported. Please contact with system admin for further investigation!');
}
?>

<div class="col-md-12">
    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
    {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}
</div>

<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <strong> Search Bill to start payment receiving procedure </strong>
        </div><!-- /.panel-heading -->
        <div class="panel-body">

            <div class="box box-primary">
                {!! Form::open(['url' => '/bank-payment/search-payment', 'method' => 'post', 'class' => 'form', 'id'=>'bill_search','role' => 'form']) !!}
                <div class="box-body">

                    <div class="col-md-12">
                        <div class="clearfix"><br/><br/></div>
                        <div class="form-group">
                            <div class="col-md-12">
                                {!! Form::label('invoice_no','Search Bill by Invoice No. :',['class'=>'col-sm-3 text-right']) !!}
                                <div class="col-md-4">
                                    {!! Form::text('invoice_no',Request::get('invoice_no'),['class'=>'required input-sm form-control',
                                    'placeholder'=>'Enter Invoice No of the Bill']) !!}
                                    <span class="input-group-btn"></span>
                                    {!! $errors->first('invoice_no','<span class="text-danger">:message</span>') !!}
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-success btn-flat" id="invoice_no_search">
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <br/>
                                {!! Session::has('warning-message') ? '<div class="alert alert-danger alert-dismissible">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. 
                                    Session::get("warning-message") .'</div>' : '' !!}
                            </div>
                        </div>

                        <div class="col-md-12"></div>
                        <div class="col-md-12 text-center">
                            @if(isset($bill_info->invoice_no))
                            @if(empty($billDetails))
                            <span class="text-info" style="font-size: 16px"><b> Voucher No.: {!! $bill_info->invoice_no !!} </b></span>
                            @endif {{-- no pilgrim attached --}}
                            @endif {{--tracking no found--}}
                        </div>
                        <br/><br/><br/>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>   <!-- /.panel-body -->
    </div>  <!-- /.panel -->
    <!------------------------------------- Invoice searching part end here--------------------------------------------------------------->


    <!---------------------------------------------Starting Details Information about Application---------------------------------------------->

    @if(isset($billDetails))
    <div class="panel panel-primary">
        <div class="panel-body">
            <!--to show the previously generated bill information-->
            @include('BankPayment::searched-invoice')

            <!--the form to receive payment-->
            @include('BankPayment::payment-form')

        </div>  <!-- /.panel-body -->
    </div>  <!-- /.panel -->
    @endif {{-- isset($payments_info) --}}
</div>  <!-- /.col-lg-12 -->

@endsection
@section('footer-script')
<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            viewMode: 'years',
            format: 'DD-MMM-YYYY',
            maxDate: (new Date()),
            minDate: '01/01/1956'
        });

        $('#bill_search').on('submit', function () {
            var invoice_no = $('input[name="invoice_no"]').val();
            if (invoice_no.trim() == '') {
                $('input[name="invoice_no"]').addClass('error');
                return false;
            } else {
                $('input[name="invoice_no"]').removeClass('error');
            }
        });


        $('#payment_done').on('submit', function () {
            var unchecked = false;
            $(".i_agree_checkbox").each(function (i, input) {
                if ($(input).is(':checked') == false) {
                    $(input).parent().addClass('error');
                    unchecked = true;
                } else {
                    $('.getPayment').prop('disabled', true);
                    $(input).parent().removeClass('error');
                }
            });
            if (unchecked) {
                return false;
            } else {
                return true;
            }
        });

        $("#payment_done").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });
</script>
@endsection