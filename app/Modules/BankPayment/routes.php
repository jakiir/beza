<?php

Route::group(array('module' => 'BankPayment', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'],
    'namespace' => 'App\Modules\BankPayment\Controllers'), function() {

    Route::post('bank-payment/search-payment', 'BankPaymentController@searchPayment');
    Route::get('bank-payment/search-payment', function() {
        return redirect('bank-payment/create');
    });

//  List of Status wise Bills for Bank Branch users
    Route::post('bank-payment/get-bank-pendling-bills/', 'BankPaymentController@getPendingBillsForBank');
    Route::post('bank-payment/get-bank-approved-bills/', 'BankPaymentController@getApprovedBillsForBank');
    Route::post('bank-payment/get-bank-rejected-bills/', 'BankPaymentController@getRejectedBillsForBank');

//  To store the fees of registration
    Route::post('bank-payment/receive-bill', 'BankPaymentController@receiveBill');

    Route::post('bank-payment/approve-payment/{bill_id}', 'BankPaymentController@approvePayment');
    Route::post('bank-payment/reject-payment/{bill_id}', 'BankPaymentController@rejectPayment');

    Route::get('bank-payment/view-bill/{bill_id}/{status}', 'BankPaymentController@viewBill');
    Route::get('bank-payment/print-group-paid-voucher/{reg_voucher_id}', 'BankPaymentController@printGroupPaidVoucher');

//    PDF
    Route::get('bank-payment/money-receipt/{id}', 'BankPaymentController@moneyReceiptPdf');

    Route::resource('bank-payment', 'BankPaymentController');

    /*     * ***********************************************************End of Route Group****************************************************************** */
});
