<?php

namespace App\Modules\Settings\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Templates extends Model {

    protected $table = 'templates';
    protected $fillable = array(
        'caption',
        'details',
        'status'
    );

    public static function boot() {
        parent::boot();
        
        static::creating(function($post) {
            $post->created_by = Auth::user()->id;
            $post->created_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * ************************************************** End of Class*********************************************************** */
}
