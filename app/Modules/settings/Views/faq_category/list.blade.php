@extends('layouts.admin')

@section('page_heading',trans('messages.faq_cat_list'))

@section('content')
<?php $accessMode=ACL::getAccsessRight('settings');
if(!ACL::isAllowed($accessMode,'V')) die('no access right!');
?>
<div class="col-lg-12">
    @include('partials.messages')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="">
                @if(ACL::getAccsessRight('settings','A'))
                <a class="" href="{{ url('/settings/create-faq-cat') }}">
                    {!! Form::button('<i class="fa fa-plus"></i> '.trans('messages.new_faq_cat'), array('type' => 'button', 'class' => 'btn btn-info')) !!}
                </a>
                @endif
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Articles</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
    </div><!-- /.panel -->
</div><!-- /.col-lg-12 -->

@endsection

@section('footer-script')

@include('partials.datatable-scripts')

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

<script>
    $(function () {
        $('#list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{url("settings/get-faq-cat-details-data")}}',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'noOfItems', name: 'noOfItems'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
