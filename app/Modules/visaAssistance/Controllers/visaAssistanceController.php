<?php

namespace App\Modules\visaAssistance\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\apps\Models\docInfo;
use App\Modules\apps\Models\Document;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\Apps\Models\Status;
use App\Modules\Certificate\Models\DocCertificate;
use App\Modules\Certificate\Models\UploadedCertificates;
use App\Modules\Company\Models\Company;
use App\Modules\Dashboard\Models\Services;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Settings\Models\Bank;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\Users;
use App\Modules\visaAssistance\Models\Apps;
use App\Modules\visaAssistance\Models\VisaAssistance;
use App\Modules\workPermit\Models\Processlist;
use Carbon\Carbon;
use DB;
use Encryption;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use mPDF;
use Session;

class visaAssistanceController extends Controller {

    protected $service_id;

    public function __construct() {
        // Globally declaring service ID
        $this->service_id = 2; // 2 is Visa Assistance
    }

    public function index() {
        if (!ACL::getAccsessRight('visa_assistance', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $userId = CommonFunction::getUserId();
        $getList['result'] = Processlist::where('initiated_by', $userId)->get();
        return view("visaAssistance::list", ['getList' => $getList]);
    }

    public function lists($ind=0, $zone=0) {
        if (!ACL::getAccsessRight('visa_assistance', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
      //  $search_status_id = base64_decode($search_status_id);
        Session::put('sess_user_id', Auth::user()->id);

        $getList = Apps::getClearanceList();
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
            $statusList[$v->status_id . 'color'] = $v->color;
        }
        $statusList[-1] = "Draft";
        $statusList['-1' . 'color'] = '#AA0000';
        
        $desks = UserDesk::all();
        $deskList = array();
        foreach ($desks as $k => $v) {
            $deskList[$v->desk_id] = $v->desk_name;
        }

//        for advance search        
        $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');
        $mode = 'V';

        return view("visaAssistance::list", compact('economicZone', 'status', 'deskList','statusList', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id', 'mode','ind','zone'));
    }

    public function appForm() {
        if (!ACL::getAccsessRight('visa_assistance', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $logged_user_type = CommonFunction::getUserType();
        $authUserId = CommonFunction::getUserId();
        $statusArr = array(8, 22, '-1'); //8 is Discard, 42 is Rejected Application and -1 is draft
        $alreadyExistApplicant = VisaAssistance::leftJoin('process_list', 'process_list.record_id', '=', 'visa_assistance.id')
                ->where('process_list.service_id', $this->service_id)
                ->whereNotIn('process_list.status_id', $statusArr)
                ->where('visa_assistance.created_by', $authUserId)
                ->first();
        if ($alreadyExistApplicant) {
            Session::flash('error', "You have already applied! Your tracking no is : " . $alreadyExistApplicant->tracking_number);
            return redirect()->back();
        }

        $bd_as_country = Countries::where('iso', 'BD')->lists('name', 'iso');
        $countries = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
        $division_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
        $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

        $passport_types = ['B' => 'B'];
        $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
        $currency = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');

        $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

        if ($alreadyExistApplicant) {
            $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
            $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
        } else {
            $clrDocuments = [];
            $processlistExist = [];
        }
        $logged_user_info = Users::where('id', $authUserId)->first();
        $viewMode = 'off';
        $mode = 'A';
        return view("visaAssistance::application-form", compact('countries', 'division_eng', 'district_eng', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'passport_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'bd_as_country', 'processlistExist', 'mode'));
    }

    public function challanStore($id, Request $request) {
        $app_id = Encryption::decodeId($id);

        $this->validate($request, [
            'challan_no' => 'required',
            'bank_name' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'branch' => 'required',
            'challan_file' => 'required',
        ]);

        $file = $request->file('challan_file');
        $original_file = $file->getClientOriginalName();
        $file->move('uploads/', time() . $original_file);
        $filename = 'uploads/' . time() . $original_file;

        Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->update([
            'status_id' => 26, ///This is for challan re-submitted
            'desk_id' => 6,
        ]);
        VisaAssistance::find($app_id)->update([
            'challan_no' => $request->get('challan_no'),
            'status_id' => 26, ///This is for challan re-submitted
            'bank_name' => $request->get('bank_name'),
            'challan_amount' => $request->get('amount'),
            'challan_branch' => $request->get('branch'),
            'challan_date' => Carbon::createFromFormat('d-M-Y', $request->get('date'))->format('Y-m-d'),
            'challan_file' => $filename,
            'updated_by' => CommonFunction::getUserId(),
        ]);
        \Session::flash('success', "Pay order information has been successfully updated!");
        return redirect()->back();
    }

    public function appDownloadPDF($id) {
        $app_id = Encryption::decodeId($id);

        $form_data = VisaAssistance::where('id', $app_id)->first();
        $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();

        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');

        $authUserId = CommonFunction::getUserId();
        $alreadyExistApplicant = VisaAssistance::where('id', $app_id)->first();

        if ($alreadyExistApplicant) {
            $alreadyExistProcesslist = Processlist::where('track_no', $alreadyExistApplicant->tracking_number)->first();
            if (!$alreadyExistProcesslist) {
                Session::flash('error', "You have no access right!");
                $listOfVisaAssistant = DB::table('service_info')->where('id', 2)->pluck('url');
                return redirect($listOfVisaAssistant);
            }
        } else {
            Session::flash('error', "You have not applied visa assistance yet");
            $listOfVisaAssistant = DB::table('service_info')->where('id', 2)->pluck('url');
            return redirect($listOfVisaAssistant);
        }

        $logged_user_type = CommonFunction::getUserType();

        $passport_types = ['B' => 'B'];

        $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');

        $countries = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');

        $bd_as_country = Countries::where('iso', 'BD')->lists('name', 'iso');

        $division_eng = AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
        $district_eng = AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

        if ($alreadyExistApplicant) {
            $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
            $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
        } else {
            $clrDocuments = [];
            $processlistExist = [];
        }
        $logged_user_info = Users::where('id', $authUserId)->first();

        $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
        $currency = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');
        $viewMode = 'on';
        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`,
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`,
                                `process_list_hist`.`updated_by`,
                                `process_list_hist`.`status_id`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`record_id`,
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id'
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));


        $content = view("visaAssistance::application-form-html", compact('process_history', 'process_data', 'form_data', 'statusArray', 'countries', 'division_eng', 'district_eng', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'passport_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'bd_as_country', 'processlistExist'))->render();

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                12, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                15, // margin bottom
                10, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("BEZA");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->setFooter('{PAGENO} / {nb}');
        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->WriteHTML($content, 2);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        $mpdf->Output($process_data->track_no . '.pdf', 'I');  // Saving pdf "F" for Save only, "I" for view only.
    }

    public function appFormEdit($id) {
        if (!ACL::getAccsessRight('visa_assistance', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $app_id = Encryption::decodeId($id);
        $logged_user_type = CommonFunction::getUserType();

        $authUserId = CommonFunction::getUserId();
        $alreadyExistApplicant = VisaAssistance::where('id', $app_id)->first();
        if ($alreadyExistApplicant) {
            $alreadyExistProcesslist = Processlist::where('track_no', $alreadyExistApplicant->tracking_number)->where('service_id', $this->service_id)->first();
            if ($alreadyExistProcesslist) {
                if ($alreadyExistProcesslist->status_id != '-1' && $alreadyExistProcesslist->status_id != 5) {
                    Session::flash('error', "You have no access right! Please contact system administration for more information.");
                    $listOfVisaAssistant = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
                    return redirect($listOfVisaAssistant);
                }
            }
        }
        $passport_types = ['B' => 'B'];

        $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');

        $countries = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');

        $bd_as_country = Countries::where('iso', 'BD')->lists('name', 'iso');

        $division_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
        $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

        if ($alreadyExistApplicant) {
            $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
            $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
        } else {
            $clrDocuments = [];
            $processlistExist = [];
        }
        $logged_user_info = Users::where('id', $authUserId)->first();

        $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
        $currency = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');
        $viewMode = 'off';
        $mode = 'E';

        return view("visaAssistance::application-form", compact('countries', 'division_eng', 'district_eng', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'passport_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'bd_as_country', 'processlistExist', 'mode'));
    }

    public function appFormView($id) {
        if (!ACL::getAccsessRight('visa_assistance', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $app_id = Encryption::decodeId($id);
        //  breadcrumb start
        $form_data = VisaAssistance::where('id', $app_id)->first();
        $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
        // breadcrumb end

        $authUserId = CommonFunction::getUserId();
        $alreadyExistApplicant = VisaAssistance::where('id', $app_id)->first();
        if ($alreadyExistApplicant) {
            $alreadyExistProcesslist = Processlist::where('track_no', $alreadyExistApplicant->tracking_number)->first();
            if (!$alreadyExistProcesslist) {
                Session::flash('error', "You have no access right!");
                $listOfVisaAssistant = DB::table('service_info')->where('id', 2)->pluck('url');
                return redirect($listOfVisaAssistant);
            }
        } else {
            Session::flash('error', "You have not applied visa assistance yet");
            $listOfVisaAssistant = DB::table('service_info')->where('id', 2)->pluck('url');
            return redirect($listOfVisaAssistant);
        }

        $logged_user_type = CommonFunction::getUserType();

        $passport_types = ['B' => 'B'];

        $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');
        $bd_as_country = Countries::where('iso', 'BD')->lists('name', 'iso');
        
        $countries = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');        
        $division_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
        $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
        $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $document = docInfo::where('service_id', $this->service_id) // 2 is for visa assistance service
                        ->orderBy('order')->get();

        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

        if ($alreadyExistApplicant) {
            $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
            $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
        } else {
            $clrDocuments = [];
            $processlistExist = [];
        }
        $logged_user_info = Users::where('id', $authUserId)->first();

        $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
        $currency = Currencies::orderBy('code')->where('is_archieved', 0)->where('is_active', 1)->lists('code', 'id');
        $viewMode = 'on';
        $mode = 'V';

        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

        return view("visaAssistance::application-form", compact('process_history', 'process_data', 'form_data', 'statusArray', 'countries', 'division_eng', 'district_eng','banks', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'passport_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'bd_as_country', 'processlistExist', 'mode'));
    }

    public function appStore(Request $request, VisaAssistance $visaAssistance, Processlist $processlist) {
        $authUserId = CommonFunction::getUserId();
        $statusArr = array(5, 8, 22, '-1'); // 5 is shortfall, 8 is Discard, 22 is Rejected Application and -1 is draft
        $alreadyExistApplicant = VisaAssistance::leftJoin('process_list', function($join) {
                    $join->on('process_list.record_id', '=', 'visa_assistance.id');
                    $join->on('process_list.service_id', '=', DB::raw($this->service_id));
                })
                ->where('process_list.service_id', $this->service_id)
                ->whereNotIn('process_list.status_id', $statusArr)
                ->where('visa_assistance.created_by', $authUserId)
                ->first();
        if ($alreadyExistApplicant) {
            Session::flash('error', "You have already submitted visa assistance! Your tracking no is : " . $alreadyExistApplicant->tracking_number);
            return redirect()->back();
        }

        $app_id = (!empty($request->get('app_id')) ? Encryption::decodeId($request->get('app_id')) : '');
        $alreadyExistApplicant = VisaAssistance::where('created_by', $authUserId)->where('id', $app_id)->orderBy('id', 'ASC')->first();
        if ($alreadyExistApplicant) {
            if (!ACL::getAccsessRight('visa_assistance', 'E', $app_id))
                die('You have no access right! Please contact with system admin if you have any query.');
            $visaAssistance = $alreadyExistApplicant;
        }else {
            if (!ACL::getAccsessRight('visa_assistance', 'A'))
                die('You have no access right! Please contact with system admin if you have any query.');
        }

        $visaAssistance->applicant_name = $request->get('applicant_name');
        $visaAssistance->country = $request->get('country');
        $visaAssistance->state = $request->get('state');
        $visaAssistance->province = $request->get('province');
        $visaAssistance->road_no = $request->get('road_no');
        $visaAssistance->house_no = $request->get('house_no');
        $visaAssistance->post_code = $request->get('post_code');
        $visaAssistance->phone_home = $request->get('phone_home');
        $visaAssistance->phone_office = $request->get('phone_office');
        $visaAssistance->mobile = $request->get('mobile');
        $visaAssistance->fax = $request->get('fax');
        $visaAssistance->email = $request->get('email');
        $visaAssistance->website = $request->get('website');
        if ($request->get('same_as_authorized') != '') {
            $visaAssistance->same_as_authorized = $request->get('same_as_authorized');
        }
        $visaAssistance->correspondent_name = $request->get('correspondent_name');
        $visaAssistance->correspondent_nationality = $request->get('correspondent_nationality');
        $visaAssistance->correspondent_passport = $request->get('correspondent_passport');
        $visaAssistance->correspondent_country = $request->get('correspondent_country');
        $visaAssistance->correspondent_state = $request->get('correspondent_state');
        $visaAssistance->correspondent_province = $request->get('correspondent_province');
        $visaAssistance->correspondent_road_no = $request->get('correspondent_road_no');
        $visaAssistance->correspondent_house_no = $request->get('correspondent_house_no');
        $visaAssistance->correspondent_post_code = $request->get('correspondent_post_code');
        $visaAssistance->correspondent_phone = $request->get('correspondent_phone');
        $visaAssistance->correspondent_fax = $request->get('correspondent_fax');
        $visaAssistance->correspondent_email = $request->get('correspondent_email');
        $visaAssistance->correspondent_website = $request->get('correspondent_website');

        $visaAssistance->passport_types = $request->get('passport_types');        
        $visaAssistance->eco_zone_id = $request->get('eco_zone_id');
        $visaAssistance->bd_mission_country = $request->get('bd_mission_country');
        $visaAssistance->embassies = $request->get('embassies');
        $visaAssistance->visit_purpose = $request->get('visit_purpose');

        if ($request->get('applicant_pic')) {
            $image = $request->file('applicant_pic');
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            //$valid_formats = array("jpg","jpeg");
            $mime_type = $image->getMimeType();
            if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg') {
                if ($size < (1024 * 1024 * 3)) {
                    $imagename = uniqid("va_", true) . '.' . $extension;
                    $ym1 = "uploads/" . date("Y") . "/" . date("m");
                    if (!file_exists($ym1)) {
                        mkdir($ym1, 0777, true);
                        $myfile = fopen($ym1 . "/index.html", "w");
                        fclose($myfile);
                    }
                    $img = Image::make($image->getRealPath());

                    $img->resize(120, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $savedPath = "{$ym1}/{$imagename}";
                    $path = public_path($savedPath);
                    $img->save($path);
                    $image_path = $ym1 . "/" . $imagename;
                    $visaAssistance->applicant_pic = $image_path;
                } else {
                    Session::flash('error_message', "Image size must be less than 3 megabyte");
                    return redirect()->back();
                }
            } else {
                Session::flash('error_message', "Image format is not valid. Please upload an image of jpg or jpeg format");
                return redirect()->back();
            }
        }

        $visaAssistance->incumbent_national_name = $request->get('incumbent_national_name');
        $visaAssistance->incumbent_nationality = $request->get('incumbent_nationality');
        $visaAssistance->incumbent_gender = $request->get('incumbent_gender');
        $visaAssistance->incumbent_passport = $request->get('incumbent_passport');

        if ($request->get('incumbent_pass_expire') != '') {
            $visaAssistance->incumbent_pass_expire = CommonFunction::changeDateFormat($request->get('incumbent_pass_expire'), true);
        }
        if ($request->get('incumbent_pass_issue_date') != '') {
            $visaAssistance->incumbent_pass_issue_date = CommonFunction::changeDateFormat($request->get('incumbent_pass_issue_date'), true);
        }

        $visaAssistance->incumbent_pass_issue_place = $request->get('incumbent_pass_issue_place');
        $visaAssistance->incumbent_country = $request->get('incumbent_country');
        $visaAssistance->incumbent_division = $request->get('incumbent_division');
        $visaAssistance->incumbent_state = $request->get('incumbent_state');
        $visaAssistance->incumbent_district = $request->get('incumbent_district');
        $visaAssistance->incumbent_province = $request->get('incumbent_province');
        $visaAssistance->incumbent_road_no = $request->get('incumbent_road_no');
        $visaAssistance->incumbent_house_no = $request->get('incumbent_house_no');
        $visaAssistance->incumbent_post_code = $request->get('incumbent_post_code');
        $visaAssistance->incumbent_email = $request->get('incumbent_email');
        $visaAssistance->incumbent_phone = $request->get('incumbent_phone');
        $visaAssistance->incumbent_fax = $request->get('incumbent_fax');

        if ($request->get('incumbent_dob') != '') {
            $visaAssistance->incumbent_dob = CommonFunction::changeDateFormat($request->get('incumbent_dob'), true);
        }
        $visaAssistance->incumbent_martial_status = $request->get('incumbent_martial_status');
        if ($request->get('arrival_date') != '') {
            $visaAssistance->arrival_date = CommonFunction::changeDateFormat($request->get('arrival_date'), true);
        }
        if ($request->get('departure_date') != '') {
            $visaAssistance->departure_date = CommonFunction::changeDateFormat($request->get('departure_date'), true);
        }
        $visaAssistance->visit_duration = $request->get('visit_duration');

        $acceptTerms = (!empty($request->get('acceptTerms')) ? 1 : 0);
        $visaAssistance->acceptance_of_terms = $acceptTerms;

        $visaAssistance->created_by = $authUserId;
        $visaAssistance->save();

        //For Tracking ID generating and update        
        if (empty($request->get('sv_draft'))) {
            /* if applicant submit the application without saving as draft or from draft submit an application track no will be generated
              // if status id = 5 or shortfall, tracking id will not be regenerated */
            if ((!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number == '' && $alreadyExistApplicant->status_id != 5) ||
                    empty($alreadyExistApplicant)) {
                $tracking_number = 'VA-' . date("dMY") . $this->service_id . str_pad($visaAssistance->id, 6, '0', STR_PAD_LEFT);
            } else {
                $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
            }
        } else {
            if (!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number != '' && $alreadyExistApplicant->status_id == 5) {
                $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
            } else {
                $tracking_number = '';
            }
        }

        if (empty($request->get('sv_draft'))) {
            $visaAssistance->status_id = 1;
            $visaAssistance->is_draft = 0;
        } else {
            $visaAssistance->status_id = -1;
            $visaAssistance->is_draft = 1;
        }

        $visaAssistance->tracking_number = $tracking_number;
        $visaAssistance->save();

        $doc_row = docInfo::where('service_id', $this->service_id) // 2 for Visa Assistance
                ->get(['doc_id', 'doc_name']);

        if (isset($doc_row)) {
            foreach ($doc_row as $docs) {
                if ($request->get('validate_field_' . $docs->doc_id) != '') {
                    $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ? $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                    $documnent_id = $docs->doc_id;
                    if ($request->get('document_id_' . $docs->doc_id) == '') {
                        Document::create([
                            'service_id' => $this->service_id, // 2 for Visa Assistance
                            'app_id' => $visaAssistance->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    } else {
                        $documentId = $request->get('document_id_' . $docs->doc_id);
                        Document::where('id', $documentId)->update([
                            'service_id' => $this->service_id, // 2 for Visa Assistance
                            'app_id' => $visaAssistance->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    }
                }
            }
        }
        ///End file uploading
        ///Save data to process_list table

        $processlistExist = Processlist::where('record_id', $visaAssistance->id)->where('service_id', $this->service_id)->first();
        $deskId = 0;
        if (!empty($request->get('sv_draft'))) {
            $statusId = -1;
            $deskId = 0;
        } else {
            $statusId = 1;
            $deskId = 3; // 3 is RD1
        }

        if (count($processlistExist) < 1) {
            $process_list_insert = Processlist::create([
                        'track_no' => $tracking_number,
                        'reference_no' => '',
                        'company_id' => '',
                        'service_id' => $this->service_id,
                        'initiated_by' => CommonFunction::getUserId(),
                        'closed_by' => 0,
                        'status_id' => $statusId,
                        'desk_id' => $deskId,
                        'record_id' => $visaAssistance->id,
                        'eco_zone_id' => $request->get('eco_zone_id'),
                        'process_desc' => '',
                        'updated_by' => CommonFunction::getUserId()
            ]);
        } else {

            if ($processlistExist->status_id > -1) {
                if (empty($request->get('sv_draft'))) {
                    $statusId = 10; // resubmitted
                } else {
                    $statusId = $processlistExist->status_id; // if drafted, older status will remain
                }
            }
            $processlisUpdate = array(
                'track_no' => $tracking_number,
                'service_id' => $this->service_id,
                'status_id' => $statusId,
                'desk_id' => $deskId,
                'eco_zone_id' => $request->get('eco_zone_id'),
            );
            $processlist->update_draft_app_for_va($visaAssistance->id, $processlisUpdate);
        }
        if (empty($request->get('sv_draft'))) {
            $flassMsg = "Your application has been submitted with tracking no: <strong>" . $tracking_number . "</strong>";
        } else {
            $flassMsg = "Your application has been drafted successfully";
        }

        Session::flash('success', $flassMsg);
        $listOfVisaAssistant = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
        return redirect($listOfVisaAssistant);
    }

    public function uploadDocument() {
        return View::make('visaAssistance::ajaxUploadFile');
    }

    public function preview() {
        return view("visaAssistance::preview");
    }

    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');

        if ($param == 'process') {

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;

            $verifiedInfo = VisaAssistance::where('id', $curr_app_id)->first();
            $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM visa_assistant_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            // Get all applications' id
            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition
            $statusId_wz = sprintf("%02d", $statusId);

            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM visa_assistant_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";

            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {

            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-status-list') {

            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');

            $verifiedInfo = VisaAssistance::where('id', $curr_app_id)->first();
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM visa_assistant_process_path APP WHERE APP.status_from = '$statusId' $cond))
                        and APS.service_id = $this->service_id
                        order by APS.status_name
                        ";

            $statusList = \DB::select(DB::raw($sql));

            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    public function updateBatch(Request $request, processlist $process_model, VisaAssistance $visa_assistance_model) {

        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $status_id = $request->get('status_id');
        $attach_file = $request->file('attach_file');
        $onbehalf = $request->get('on_behalf_of');
        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }



        foreach ($apps_id as $app_id) {

            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->service_id = $this->service_id;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }


            $appInfo = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;

            if (empty($desk_id)) {
                $whereCond = "select * from visa_assistant_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'
                                               AND status_to REGEXP '^([0-9]*[,]+)*$status_id([,]+[,0-9]*)*$'";

                $processPath = DB::select(DB::raw($whereCond));
                if ($processPath[0]->desk_to == '0')  // Sent to Applicant
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')   // Keep in same desk
                    $desk_id = $deskFrom;
            }


            $app_data = array(
                'status_id' => $status_id,
                'remarks' => $remarks,
                'updated_at' => date('y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            );

            $info_data = array(
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8) { // 8 is discarded
                $info_data['closed_by'] = Auth::user()->id;
            }
            $process_model->update_app2($app_id, $info_data);

            $visa_assistance_model->update_method($app_id, $app_data);
            VisaAssistance::where('id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);

            if (in_array($status_id, array(5, 8, 23, 21, 22, 25, 27))) {
                $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();

//            Notification
                $body_msg = '<span style="color:#000;text-align:justify;"><b>';
                if ($status_id == 23) {
                    $smsbody = "Will be given by BEZA later";
                    $applicantMobile = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_phone', 'users');

                    $params = array([
                            'emailYes' => '0',
                            'emailTemplate' => 'users::message',
                            'emailSubject' => 'Application has been accepted',
                            'emailBody' => $smsbody,
                            'emailHeader' => 'Successfully Saved the Application header',
                            'emailAdd' => 'shahin.fci@gmail.com',
//                            'mobileNo' => $applicantMobile,
                            'mobileNo' => '01745949926',
                            'smsYes' => '0',
                            'smsBody' => 'Your application has been submitted with tracking id: ' . $process_data->track_no . ' received. Please fill up your Pay order information!',
                    ]);
                    CommonFunction::sendMessageFromSystem($params);
                    $body_msg .= 'Your application for ' . $process_data->track_no . ' has been ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status') . ', Please give your Pay order information!';
                } else {
                    $body_msg .= 'Your application for visa assistance with Tracking Number: ' . $process_data->track_no .
                            ' is now in status: ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status');
                }
                $body_msg .= '</span>';
                $body_msg .= '<br/><br/><br/>Thanks<br/>';
                $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                $data = array(
                    'header' => 'Application Update',
                    'param' => $body_msg
                );
                $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');
                $certificate = '';


                if ($status_id == 23) {
                    $certificate = $this->certificate_gen($app_id);   //Certificate Generation after payment accepted
                }


                $billMonth = date('Y-m');

                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $app_id, $certificate, $status_id,$billMonth) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                            ->to($fetched_email_address)
                            ->cc('ishrat@batworld.com')
                            ->subject('Application Update Information for visa assistance');
                    if ($status_id == 23) {
                        $message->attach($certificate);
                        VisaAssistance::where('id', $app_id)->update(['certificate' => $certificate,'bill_month'=>$billMonth]);
                    }
                });
            }
        }

        //         for previous and present status
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        Session::flash('success', "Application status updated Previous status: $statusList[$status_from] || Present Status: $statusList[$status_id]");
        return redirect()->back();
    }

    public function certificate_re_gen($id) {
        $app_id = Encryption::decodeId($id);
        $processlistExist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        $certificate = '';
        if ($status_id == 23) {
            $certificate = $this->certificate_gen($app_id);   //Certificate Generation after payment accepted
            VisaAssistance::where('id', $app_id)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function certificate_gen($appID) {

        $appId = $appID;
        $desk_data = Users::where('desk_id', Auth::user()->desk_id)->first();
        $desk_name = CommonFunction::getFieldName(8, 'desk_id', 'desk_name', 'user_desk');
        $content = view("visaAssistance::certificate",compact('desk_data','desk_name'))->render();




        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');
        $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;
        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");



        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }

        $certificateName = uniqid("beza_" . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');



        $mpdf->SetCompression(true);

        $mpdf->WriteHTML($content);


        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf
        return $pdfFilePath;
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', 1)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('visaAssistance::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

        $economic_zone = $request->get('eco_zone_id');

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $economic_zone);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('visaAssistance::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();

        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, 
                                                pl.eco_zone_id, pl.tracking_number, pl.created_by, pl.initiated_by
                                                from service_info si
                                                left join 
                                (
                                select pl.process_id, pl.eco_zone_id, pc.tracking_number, pc.created_by, pl.initiated_by, pl.service_id, pl.status_id  
                                from process_list pl 
                                inner join (
                                                select tracking_number, created_by 
                                                    from project_clearance 
                                                    where status_id = 23
                                                   ) pc
                                on pl.initiated_by = pc.created_by
                                ) pl 
                                on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id != -1
                                group by si.id;");

        $str = "";
        foreach ($sql as $sql_data) {
            $short_name = $sql_data->short_name;
            $id = $sql_data->id;
            $industry_cat_id = $sql_data->tracking_number;
            $application_number = $sql_data->application_number;
            $eco_zone_id = $sql_data->eco_zone_id;
            $str.="$short_name:$application_number:$industry_cat_id:$eco_zone_id==";
        }
        $data = ['responseCode' => 1, 'data' => $contents, 'totalApps' => $str]; // pattern= Module:AppsCount:Ind:zone
        // End of code to count total number of applications

        return response()->json($data);
    }

    public function discardCertificate($id) {
        $app_id = Encryption::decodeId($id);
        if (Auth::user()->user_type == '1x101') {
            $appInfo = VisaAssistance::find($app_id);
            if ($appInfo->status_id == 23) {
                $appInfo->status_id = 40;
                $appInfo->certificate = '';
                $appInfo->save();

                Processlist::where(['record_id' => $app_id, 'service_id' => $this->service_id])
                        ->update(['desk_id' => 0, 'status_id' => 40]);
                Session::flash('success', 'Certificate Discard');
                return redirect()->back();
            } else {
                Session::flash('error', 'Certificate discard is not possible [PC9002]');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'You are not authorized to discard certificate [PC9001]');
            return redirect()->back();
        }
    }

    /*     * *To view associated documents or certificate of the users, takes user_id as input ** */

    public function viewAssociateCertificates($_id) {
        $user_id = Encryption::decodeId($_id);
        $docCertificate = DocCertificate::where('is_archieved', 0)->orderBy('order')->get();

        $assoCertificates = UploadedCertificates::where('is_archieved', 0)->where('created_by', $user_id)->get();
        foreach ($assoCertificates as $documents) {
            $assoCert[$documents->doc_id]['doucument_id'] = $documents->id;
            $assoCert[$documents->doc_id]['file'] = $documents->doc_file;
            $assoCert[$documents->doc_id]['doc_name'] = $documents->doc_name;
        }

        return view("projectClearance::associateDoc.doc-cer", compact('assoCert', 'docCertificate', 'user_id'));
    }

    public function getEmbassyByCountry(Request $request) {
        $iso = $request->get('iso');

        $embassies = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->where('iso', $iso)
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');
        if (count($embassies) > 0) {
            $data = ['responseCode' => 1, 'data' => $embassies];
        } else {
            $data = ['responseCode' => 0, 'data' => ''];
        }
        return response()->json($data);
    }

    public function getDistrictByDivision(Request $request) {
        $division_id = $request->get('divisionId');

        $districts = AreaInfo::where('PARE_ID', $division_id)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
        $data = ['responseCode' => 1, 'data' => $districts];
        return response()->json($data);
    }

    public function checkPassportNo(Request $request) {
        $passport_no = $request->get('passport_no');
        $app_id = $request->get('app_id');

        $va_id = Encryption::decodeId($app_id);

        if ($va_id != '') {
            $if_existed_pass = VisaAssistance::where('incumbent_passport', $passport_no)->where('id', '!=', $va_id)->count();
        } else {
            $if_existed_pass = VisaAssistance::where('incumbent_passport', $passport_no)->count();
        }
        return $if_existed_pass;
    }

    /*     * ********************************************End of Controller Class************************************************* */
}
