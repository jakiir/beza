<?php

Route::group(array('module' => 'visaAssistance', 'middleware' => ['auth', 'checkAdmin'], 'namespace' => 'App\Modules\visaAssistance\Controllers'), function() {

// search
    Route::get('visa-assistance/search', 'visaAssistanceController@search');
    Route::post('visa-assistance/search-result', 'visaAssistanceController@searchResult');
    Route::post('visa-assistance/service-wise-result', 'visaAssistanceController@serviceWiseStatus');
    Route::post('visa-assistance/search-view/{param}', 'visaAssistanceController@searchView');

    Route::patch('visa-assistance/update-batch', "visaAssistanceController@updateBatch");
    Route::post('visa-assistance/ajax/{param}', 'visaAssistanceController@ajaxRequest');

    Route::get('visa-assistance/list', 'visaAssistanceController@lists');
    Route::get('visa-assistance/list/{ind}/{zone}', 'visaAssistanceController@lists');

// app form create
    Route::get('visa-assistance/form', 'visaAssistanceController@appForm');
    Route::post('visa-assistance/store-app', "visaAssistanceController@appStore");
    Route::post('visa-assistance/challan-store/{id}', "visaAssistanceController@challanStore");
    Route::get('visa-assistance/edit/{id}', 'visaAssistanceController@appFormEdit');
    Route::get('visa-assistance/view/{id}', 'visaAssistanceController@appFormView');
    Route::get('visa-assistance/view-pdf/{id}', 'visaAssistanceController@appDownloadPDF');
    Route::get('visa-assistance/re-download/{id}', 'visaAssistanceController@appsDownloadPDF');

    Route::any('visa-assistance/upload-document', 'visaAssistanceController@uploadDocument');

    Route::post('visa-assistance/save-as-draft', "visaAssistanceController@saveAsDraft");

    Route::get('/visa-assistance/get-embassy-by-country', 'visaAssistanceController@getEmbassyByCountry');
    Route::get('/visa-assistance/get-district-by-division', 'visaAssistanceController@getDistrictByDivision');

    Route::get('visa-assistance/preview', 'visaAssistanceController@preview');

    // For showing associated certificates
    Route::get('visa-assistance/view-cer/{id}', 'visaAssistanceController@viewAssociateCertificates');

    //VA Certificate Generation
    Route::get('visa-assistance/certificate-generate/{id}','visaAssistanceController@certificate_gen');
    //VA Certificate Regenerate
    Route::get('visa-assistance/certificate-re-generate/{id}','visaAssistanceController@certificate_re_gen');


    // Discarding certificate
    Route::get('visa-assistance/discard-certificate/{id}', 'visaAssistanceController@discardCertificate');

    // Check if valid unique passport no
    Route::get('visa-assistance/check-passport_no', "visaAssistanceController@checkPassportNo");

    Route::resource('visa-assistance', 'visaAssistanceController');

    /*     * ********************************End of Route group****************************** */
});
