<?php

namespace App\Modules\visaAssistance\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VisaAssistance extends Model {

    protected $table = 'visa_assistance';
    protected $fillable = array(
        'id',
        'tracking_number',
        'status_id',
        'bill_id',
        'bill_month',
        'payment_status_id',
        'passport_types',
        'bd_mission_country',
        'embassies',
        'visit_purpose',
        'applicant_name',
        'eco_zone_id',
        'country',
        'district',
        'division',
        'state',
        'province',
        'road_no',
        'house_no',
        'post_code',
        'phone_home',
        'phone_office',
        'mobile',
        'fax',
        'email',
        'website',
        'nationality', 
        'correspondent_name',
        'correspondent_nationality',
        'correspondent_passport',
        'correspondent_country',
        'correspondent_division',
        'correspondent_district',
        'correspondent_state',
        'correspondent_province',
        'correspondent_road_no',
        'correspondent_house_no',
        'correspondent_post_code',
        'correspondent_phone',
        'correspondent_fax',
        'correspondent_email',
        'correspondent_website',
        'incumbent_national_name',
        'incumbent_nationality',
        'incumbent_gender',
        'incumbent_passport',
        'incumbent_pass_expire',
        'incumbent_pass_issue_place',
        'incumbent_pass_issue_date',
        'incumbent_country',
        'incumbent_division',
        'incumbent_state',
        'incumbent_district',
        'incumbent_province',
        'incumbent_road_no',
        'incumbent_house_no',
        'incumbent_post_code',
        'incumbent_email',
        'incumbent_phone',
        'incumbent_fax',
        'incumbent_dob',
        'incumbent_martial_status',
        'arrival_date',
        'departure_date',
        'visit_duration',
        'acceptance_of_terms',
        'certificate',
        'is_draft',
        'is_locked',
        'remarks',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'challan_no',
        'challan_amount',
        'challan_branch',
        'challan_date',
        'challan_file',
        'bank_name',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }
    function update_method($app_id, $data) {
        DB::table($this->table)
            ->where('id', $app_id)
            ->update($data);
    }

    /*     * *****************************End of Model Class********************************** */
}
