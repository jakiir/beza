<?php $row_sl = 0; ?>
@foreach($getList as $row)
<?php $row_sl++ ?>
<tr>
    <td>{!! $row_sl !!}</td>
    <td>{!! $row->track_no !!}</td>
    <td>{!! $row->correspondent_name !!}</td>
    <td>
        @if($row->desk_name == '') 
        Applicant
        @else 
        {!! $row->desk_name !!}
        @endif
    </td>
    <td>
        @if(!empty($row->status_name))
        <span style="background-color:<?php echo $row->color; ?>;color: #fff; font-weight: bold;" class="label btn-sm">
            {!! $row->status_name !!}
        </span>
        @else
        <span style="background-color:#dd4b39;color: #fff; font-weight: bold;" class="label btn-sm">
            Draft
        </span>
        @endif {{-- checking status_name --}}   
    </td>

    <td>{!! CommonFunction::updatedOn($row->updated_at) !!}</td>
    <td>
        <a href="{{url('visa-assistance/view/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" >
            <i class="fa fa-folder-open-o"></i> View</a>
        @if(!empty($row->certificate && $row->status_id == 23))
        <a title="Download Certificate" href="{{url($row->certificate)}}" target="_blank" class="btn btn-xs btn-info open" >
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Certificate</a>
        @if(Auth::user()->user_type == '1x101')
        <a onclick="return confirm('Are you sure ?')" href="/visa-assistance/discard-certificate/{{ Encryption::encodeId($row->record_id)}}" class="btn show-in-view btn-xs btn-danger"
           title="Discard Certificate"> <i class="fa  fa-trash"></i> Discard Certificate</a>
        @endif {{-- checking system admin --}}
        @endif {{-- checking certificate availbility status --}}
    </td>
</tr>
@endforeach
