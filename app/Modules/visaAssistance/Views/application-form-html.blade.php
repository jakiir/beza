@extends('layouts.pdfGen')
@section('content')
@include('partials.messages')
<section class="content">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;margin-bottom:10px;">
                        <img src="assets/images/logo_beza_single.png"/><br/>
                        BEZA::Bangladesh Economic Zones Authority<br/>
                        Application for Visa Assistance
                    </div>
                </div>


                <div class="panel panel-red" id="inputForm">
                    <div class="panel-heading"><b> Application for Visa Assistance</b></div>
                    <div class="panel-body">



                        <section class="content-header">
                            <table width="100%">
                                <tr>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Tracking no. : </strong><span style="font-size:10px;">{{ $process_data->track_no  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;"> Date of Submission: </strong><span style="font-size:10px;"> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Current Status : </strong><span style="font-size:10px;">
                                            @if($form_data->status_id == -1) Draft
                                            @else {!! $statusArray[$form_data->status_id] !!}
                                            @endif</span>
                                    </td>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Current Desk :</strong><span style="font-size:10px;">
                                            @if($process_data->desk_id != 0)
                                                {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                            @else
                                                Applicant
                                            @endif
                                        </span></td>
                                </tr>

                                <tr>
                                    <td style="padding:5px; font-size:10px">
                                        <?php if ($form_data->status_id == 23 && !empty($form_data->certificate)) { ?>
                                        <a href="{{ url($form_data->certificate) }}"
                                           title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                        <?php } ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </section>


                        <fieldset style="display: block;">

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Basic Information</strong></div>
                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>Type of Visa Required for the Incumbent Foreign Nationals</strong></td>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->passport_types) ? $alreadyExistApplicant->passport_types : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="padding: 5px; font-size: 10px;"><strong>Bangladesh mission in abroad where recommendation letter to be sent</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($countries[$alreadyExistApplicant->bd_mission_country]) ? $countries[$alreadyExistApplicant->bd_mission_country] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Embassy / High Commission</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($high_comissions[$alreadyExistApplicant->embassies]) ? $high_comissions[$alreadyExistApplicant->embassies] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Purposed Of Visit</strong></td>
                                            <td colspan="3" width="755%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->visit_purpose) ? $alreadyExistApplicant->visit_purpose : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                            <div class="panel panel-primary">
                                <div class="panel-heading"> <strong>Particulars of Organizations / Company</strong></div>
                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>1. Preffered Investment Zone / Area</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($economicZone[$alreadyExistApplicant->eco_zone_id]) ? $economicZone[$alreadyExistApplicant->eco_zone_id] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Profile Picture</strong></td>
                                            <td rowspan="5" width="25%" style="padding: 5px; font-size: 10px;">
                                                <?php
                                                $picUrl = (!empty($alreadyExistApplicant->applicant_pic) ? $alreadyExistApplicant->applicant_pic : 'assets/images/avatar5.png');
                                                ?>
                                                <img width="100" height="100" alt="Profile Pic" src="{{ $picUrl }}" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>2. Full Address of Representative Company or Organization :</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 1.</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 2. </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Post Code </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($countries[$alreadyExistApplicant->country]) ? $countries[$alreadyExistApplicant->country] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>City </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->state) ? $alreadyExistApplicant->state : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>State / Province </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->province) ? $alreadyExistApplicant->province : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Phone (Home) </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->phone_home) ? $alreadyExistApplicant->phone_home : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Phone (Office) </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->phone_office) ? $alreadyExistApplicant->phone_office : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Mobile No. </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->mobile) ? $alreadyExistApplicant->mobile : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Fax No </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Email </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Website </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <strong style="display: inline-block;width:60%;">Correspondent Information</strong>
                                    <span class="text-right" id="full_same_as_authorized" style="width: 38%; display: none;">
                                        <span style="background-color:#ddd;width:100%;display:block;margin:2px;padding:6px;display:block">Yes</span>
                                        <label for="same_as_authorized" class="text-left" style="font-size:13px;">Same as mailing address</label>
                                    </span>
                                </div>

                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>3. Name of the Correspondent Company Name (Abroad) </strong></td>
                                            <?php $authCorrespondent_name = (!empty($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : 'N/A'); ?>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name) }}</span></td>
                                        </tr>
                                        <tr>
                                            <?php $authNationality = (!empty($nationality[$logged_user_info->nationality]) ? $nationality[$logged_user_info->nationality] : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Nationality </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($nationality[$alreadyExistApplicant->correspondent_nationality]) ? $nationality[$alreadyExistApplicant->correspondent_nationality] : $authNationality) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="padding: 5px; font-size: 10px;"><strong>4. Correspondent Address & Contact Details </strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country </strong></td>
                                            <?php $authCountry = (!empty($countries[$logged_user_info->country]) ? $countries[$logged_user_info->country] : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($countries[$alreadyExistApplicant->correspondent_country]) ? $countries[$alreadyExistApplicant->correspondent_country] : $authCountry) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>State </strong></td>
                                            <?php $authState = (!empty($logged_user_info->state) ? $logged_user_info->state : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_state) ? $alreadyExistApplicant->correspondent_state : $authState) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Province </strong></td>
                                            <?php $authProvince = (!empty($logged_user_info->province) ? $logged_user_info->province : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 1 </strong></td>
                                            <?php $authRoad_no = (!empty($logged_user_info->road_no) ? $logged_user_info->road_no : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_road_no) ?
                                                        $alreadyExistApplicant->correspondent_road_no : $authRoad_no) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 2 </strong></td>
                                            <?php $authHouse_no = (!empty($logged_user_info->house_no) ? $logged_user_info->house_no : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_house_no) ? $alreadyExistApplicant->correspondent_house_no : $authHouse_no) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Post Code </strong></td>
                                            <?php $authPost_code = (!empty($logged_user_info->post_code) ? $logged_user_info->post_code : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_post_code) ?
                                                        $alreadyExistApplicant->correspondent_post_code : $authPost_code) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Phone No. </strong></td>
                                            <?php $authUser_phone = (!empty($logged_user_info->user_phone) ? $logged_user_info->user_phone : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Fax No. </strong></td>
                                            <?php $authUser_fax = (!empty($logged_user_info->user_fax) ? $logged_user_info->user_fax : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Email </strong></td>
                                            <?php $authUser_email = (!empty($logged_user_info->user_email) ? $logged_user_info->user_email : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Website </strong></td>
                                            <?php $authWebsite = (!empty($logged_user_info->website) ? $logged_user_info->website : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_website) ?
                                                        $alreadyExistApplicant->correspondent_website : $authWebsite) }}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </fieldset>


                        <fieldset style="display: block;">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Particulars of Foreign Incumbent </strong></div>
                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>Name of the foreign national </strong></td>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_national_name) ? $alreadyExistApplicant->incumbent_national_name : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Nationality </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($nationality[$alreadyExistApplicant->incumbent_nationality]) ? $nationality[$alreadyExistApplicant->incumbent_nationality] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Gender </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_gender) ? $alreadyExistApplicant->incumbent_gender : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Passport No. </strong></td>
                                            <?php $authPassport = !empty($logged_user_info->passport_no) ? $logged_user_info->passport_no : 'N/A'; ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_passport) ? $alreadyExistApplicant->incumbent_passport : $authPassport) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Expiry Date </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_expire) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_expire, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Place of Issue </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_issue_place) ? $alreadyExistApplicant->incumbent_pass_issue_place : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Date of Issue </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_issue_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_issue_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Date of Birth  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_dob) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_dob, 0, 10)) : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Marital Status  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_martial_status) ? $alreadyExistApplicant->incumbent_martial_status : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>Contact Information of where you will be staying in Bangladesh  </strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($bd_as_country[$alreadyExistApplicant->incumbent_country]) ? $bd_as_country[$alreadyExistApplicant->incumbent_country] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Division  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($division_eng[$alreadyExistApplicant->incumbent_division]) ? $division_eng[$alreadyExistApplicant->incumbent_division] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>District  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($district_eng[$alreadyExistApplicant->incumbent_district]) ? $district_eng[$alreadyExistApplicant->incumbent_district] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 1  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_road_no) ? $alreadyExistApplicant->incumbent_road_no : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 2  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_house_no) ? $alreadyExistApplicant->incumbent_house_no : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Post Code  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_post_code) ? $alreadyExistApplicant->incumbent_post_code : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Contact No.  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_phone) ? $alreadyExistApplicant->incumbent_phone : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Fax No. </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_fax) ? $alreadyExistApplicant->incumbent_fax : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Email  </strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_email) ? $alreadyExistApplicant->incumbent_email : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                    </table>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Proposed Visit Schedule</strong></div>
                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td width="33%" style="padding: 5px; font-size: 10px;"><strong>Arrival Date  </strong></td>
                                            <td width="33%" style="padding: 5px; font-size: 10px;"><strong>Departure Date  </strong></td>
                                            <td width="33%" style="padding: 5px; font-size: 10px;"><strong>Duration of Staying (In days)  </strong></td>
                                        </tr>
                                        <tr>
                                            <td width="33%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->arrival_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->arrival_date, 0, 10)) : 'N/A') }}</span></td>
                                            <td width="33%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->departure_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->departure_date, 0, 10)) : 'N/A') }}</span></td>
                                            <td width="33%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->visit_duration) ? $alreadyExistApplicant->visit_duration : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                        </fieldset>


                        <fieldset style="display: block;">
                            <div class="col-md-12" style="padding: 0px;">
                                <div class="form-group document_upload">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Required Documents for attachment</div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <table width="100%" class="table-bordered">
                                                <tr>
                                                    <td width="10%" style="padding: 5px; font-size: 10px;"><strong>No.</strong></td>
                                                    <td width="50%" style="padding: 5px; font-size: 10px;"><strong>Required Attachment</strong></td>
                                                    <td width="40%" style="padding: 5px; font-size: 10px;"><strong>Attached PDF file</strong></td>
                                                </tr>
                                                <?php $i=1; ?>
                                                @foreach($document as $row)
                                                    <tr>
                                                        <td style="padding:5px;" width="10%"><span style="font-size:10px;">{{ $i }} <?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></span></td>
                                                        <td style="padding:5px;" width="45%"><span style="font-size:10px;">{{$row->doc_name }}</span></td>
                                                        <td style="padding:5px;" width="45%">
                                                        <span style="font-size:10px;">
                                                            @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                                <a href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="{{ $clrDocuments[$row->doc_id]['file'] }}" /> <?php if(!empty($clrDocuments[$row->doc_id]['file'])){ $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name);} ?></a>
                                                            @endif
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </table>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                            </div>
                            <div class="form-group" style="clear: both">

                            </div>
                        </fieldset>


                        <fieldset style="display: block;">

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection