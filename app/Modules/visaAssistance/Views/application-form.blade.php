@extends('layouts.admin')

@section('content')
@include('partials.messages')
<?php
$accessMode = ACL::getAccsessRight('visa_assistance');
if (!ACL::isAllowed($accessMode, $mode)) {
    die('You have no access right! Please contact with system admin if you have any query.');
}
?>
<style>
    .text-title{ font-size: 16px !important}
    .text-sm{ font-size: 9px !important}
    #VisaAssisForm label.error {display: none !important; }
    .calender-icon{
        border: medium none; padding-top: 8px ! important;
    }
</style>

<section class="content">
    @if($viewMode == 'on')
        <div class="col-md-12" style="padding-bottom: 5px;">
            <a href="/visa-assistance/view-pdf/{{ Encryption::encodeId($alreadyExistApplicant->id)}}" target="_blank"
               class="btn btn-danger btn-sm pull-right">
                <i class="fa fa-download"></i> <strong>Application Download as PDF</strong>
            </a>
        </div>
    @endif
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
            @if($viewMode == 'on')

                      <!--If Applicant user, it throws an error, so to solve it-->
                        @if(Auth::user()->user_type == '6x606')
                            <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                            <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                        @endif
                    @if(in_array(Auth::user()->desk_id,array(3,4,5,6)))
                        {!! Form::open(['url' => '/visa-assistance/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                        @include('visaAssistance::batch-process')
                        <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                        <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                        {!! Form::close() !!}
                    @endif

                    @if(($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)

                    {!! Form::open(array('url' => 'visa-assistance/challan-store/'.Encryption::encodeId($alreadyExistApplicant->id),'method' => 'post',
                    'files' => true, 'role'=>'form')) !!}
                    <div class="panel panel-primary">
                        <div class="panel-heading">Pay order related information</div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                        {!! Form::label('Pay Order No','Pay Order No : ',['class'=>'col-md-5 font-ok required-star']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('challan_no', null,['class'=>'form-control bnEng required',
                                            'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                            {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                        {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                        <div class="col-md-7">
                                                    {!! Form::select('bank_name', $banks, '', ['class' => 'form-control required',
                                                    'id'=>'bank_name_form']) !!}
                                            {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                        {!! Form::label('amount','Amount :',['class'=>'col-md-5 font-ok required-star']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('amount',null, ['class'=>'form-control bnEng required','placeholder'=>'5000',
                                            'data-rule-maxlength'=>'40']) !!}
                                            {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                        <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                {!! Form::label('date','Date :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                <div class="datepicker col-md-7  input-group date" data-date-format="yyyy-mm-dd">
                                                    {!! Form::text('date', null, ['class'=>'form-control required pay_order_date', 'id' => 'pay_order_date', 'placeholder'=>'Pick from datepicker']) !!}
                                                    <label for="pay_order_date" class="input-group-addon pay_order_date">
                                                        <span class="glyphicon glyphicon-calendar pay_order_date" id="pay_order_date"></span></label>
                                                </div>
                                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                            </div>
                                    <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                        {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                        <div class="col-md-7">
                                            {!! Form::text('branch',null, ['class'=>'form-control required','placeholder'=>'Branch Name',
                                            'data-rule-maxlength'=>'40']) !!}
                                            {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12 {{$errors->has('challan_file') ? 'has-error' : ''}}">
                                        {!! Form::label('challan_file','Chalan copy :',['class'=>'col-md-5 font-ok required-star']) !!}
                                        <div class="col-md-7">
                                            {!! Form::file('challan_file',null, ['class'=>'form-control bnEng required input-sm',
                                            'data-rule-maxlength'=>'40']) !!}
                                            {!! $errors->first('challan_file','<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-left next">
                                        <i class="fa fa-chevron-circle-right"></i> Save</button>
                                </div>
                            </div>

                        </div>

                    {!! Form::close() !!}<!-- /.form end -->
                    </div> <!--End of Panel Group-->
                @endif {{-- status_id == 21  and created by logged user --}}
            @endif  {{-- view mode on --}}

                <div class="panel panel-red"  id="inputForm">
                    <div class="panel-heading"><b> Application for Visa Assistance</b></div>
                    <div class="panel-body">
                        <?php if ($viewMode != 'on') { ?>
                        {!! Form::open(array('url' => 'visa-assistance/store-app','method' => 'post','id' => 'VisaAssisForm','enctype'=>'multipart/form-data','method' => 'post', 'files' => true, 'role'=>'form')) !!}
                            <input type ="hidden" name="app_id" id="app_id" value="{{(!empty($alreadyExistApplicant->id) ? Encryption::encodeId($alreadyExistApplicant->id) : '')}}">
                            <input type="hidden" name="selected_file" id="selected_file" />
                            <input type="hidden" name="validateFieldName" id="validateFieldName" />
                            <input type="hidden" name="isRequired" id="isRequired" />
                        <?php } ?>

                        <h3 class="text-center stepHeader">Applicant Information (Part A)</h3>
                        <?php if ($viewMode == 'on') { ?>
                            <section class="content-header">
                                <ol class="breadcrumb">
                                    <li><strong>Tracking no. : </strong>
                                        {{ $process_data->track_no  }}
                                    </li>
                                    <li><strong> Date of Submission: </strong> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }} </li>
                                    <li><strong>Current Status : </strong>
                                        @if(isset($form_data) && $form_data->status_id == -1) Draft
                                        @else {!! $statusArray[$form_data->status_id] !!}
                                        @endif
                                    </li>
                                    <li>
                                        @if($process_data->desk_id != 0) <strong>Current Desk :</strong> {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                        @else
                                        <strong>Current Desk :</strong> Applicant
                                        @endif
                                    </li>
                                    <li>
                                        <?php if ($form_data->status_id == 23) { ?>
                                        <a href="{{ url($form_data->certificate) }}" class="btn show-in-view btn-xs btn-info"
                                           title="Download Certificate" target="_blank"> <i class="fa  fa-file-pdf-o"></i> <b>Download Certificate</b></a>

                                        @if(Auth::user()->user_type == '1x101' || Auth::user()->user_type == '6x606')
                                             <a href="/visa-assistance/certificate-re-generate/{{ Encryption::encodeId($form_data->id)}}" class="btn show-in-view btn-xs btn-warning"
                                              title="Re Generate Certificate"> <i class="fa  fa-file-pdf-o"></i> <b>Re-generate certificate</b></a>
                                        @endif

                                        @if(Auth::user()->user_type == '1x101')
                                        <a onclick="return confirm('Are you sure ?')" href="/visa-assistance/discard-certificate/{{ Encryption::encodeId($form_data->id)}}" class="btn show-in-view btn-xs btn-danger"
                                           title="Download Approval Letter"> <i class="fa  fa-trash"></i> <b>Discard Certificate</b></a>
                                        @endif
                                        <?php } ?>
                                    </li>
                                </ol>
                            </section>

                            @if(isset($alreadyExistApplicant->challan_no))
                            <div class="panel panel-primary"  id="ep_form">
                                <div class="panel-heading">Pay order related information</div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                                {!! Form::label('Pay Order No','Pay Order No : ',['class'=>'col-md-5 font-ok ']) !!}
                                                <div class="col-md-7">
                                                        {!! Form::text('challan_no', (isset($alreadyExistApplicant->challan_no) ? $alreadyExistApplicant->challan_no : ''),['class'=>'form-control bnEng required input-sm',
                                                    'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                                {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok']) !!}
                                                <div class="col-md-7">
                                                        {!! Form::select('bank_name', $banks, (isset($alreadyExistApplicant->bank_name) ?
                                                         $alreadyExistApplicant->bank_name : ''), ['class' => 'form-control input-sm required',
                                                         'placeholder'=>'Select One']) !!}
                                                    {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                                {!! Form::label('amount','Amount :',['class'=>'col-md-5 font-ok']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('amount',(isset($alreadyExistApplicant->challan_amount) ? $alreadyExistApplicant->challan_amount : ''), ['class'=>'form-control bnEng required input-sm','placeholder'=>'5000',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                {!! Form::label('date','Date :',['class'=>'col-md-5 font-ok']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('date',(isset($alreadyExistApplicant->challan_date) ? $alreadyExistApplicant->challan_date : ''), ['class'=>'form-control required input-sm','placeholder'=>'Date',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok ']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('branch',(isset($alreadyExistApplicant->challan_branch) ? $alreadyExistApplicant->challan_branch : ''), ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                {!! Form::label('branch','Pay Order Copy :',['class'=>'col-md-5 font-ok ']) !!}
                                                <div class="col-md-7">
                                                        <a href="{{url($alreadyExistApplicant->challan_file)}}" target="_blank"
                                                           class="btn show-in-view btn-xs btn-danger" title="Download Pay Order">
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-left next">
                                                <i class="fa fa-chevron-circle-right"></i> Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif {{-- application has challan --}}

                        <?php } ?>
                        <fieldset>

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Basic Information</strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-10 {{$errors->has('passport_types') ? 'has-error': ''}}">
                                                {!! Form::label('passport_types','Type of Visa Required for the Incumbent Foreign Nationals',
                                                ['class'=>'col-md-7 text-left required-star']) !!}
                                                <div class="col-md-5">
                                                    {!! Form::select('passport_types', $passport_types,
                                                    (!empty($alreadyExistApplicant->passport_types) ? $alreadyExistApplicant->passport_types : null),
                                                    ['data-rule-maxlength'=>'64', 'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('passport_types','<span class="help-block">:message</span>') !!}
                                                </div>
                                                </div>
                                            <div class="col-md-2">
                                                    <!--<a href="{{url('/#')}}" class="btn btn-outline btn-sm btn-default">
                                                        <i class="fa fa-question-circle"></i> Help Link </a>-->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix ">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                {!! Form::label('bdMissionAbroad','Bangladesh mission in abroad where recommendation letter to be sent',
                                                ['class'=>'text-left col-md-12']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-5 {{$errors->has('bd_mission_country') ? 'has-error': ''}}">
                                                {!! Form::label('bd_mission_country','Country',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('bd_mission_country', $countries,
                                                    (!empty($alreadyExistApplicant->bd_mission_country) ? $alreadyExistApplicant->bd_mission_country : null),
                                                    ['data-rule-maxlength'=>'64', 'placeholder' => 'Select One',
                                                    'class' => 'form-control input-sm required', 'id' => 'bd_mission_country']) !!}
                                                    {!! $errors->first('bd_mission_country','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-7 {{$errors->has('embassies') ? 'has-error': ''}}">
                                                {!! Form::label('embassies','Embassy / High Commission',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('embassies', $high_comissions,
                                                    (!empty($alreadyExistApplicant->embassies) ? $alreadyExistApplicant->embassies : null),
                                                    ['data-rule-maxlength'=>'64', 'placeholder' => 'Select Country First',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('embassies','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix" id="visit_purpose_div">
                                         <div class="row">
                                        <div class="col-md-12 {{$errors->has('visit_purpose') ? 'has-error': ''}}">
                                            {!! Form::label('visit_purpose','Purposed of Visit',['class'=>'col-md-2 text-left required-star']) !!}
                                            <div class="col-md-10">
                                                {!! Form::textarea('visit_purpose', (!empty($alreadyExistApplicant->visit_purpose) ? $alreadyExistApplicant->visit_purpose : null),
                                                ['data-rule-maxlength'=>'999', 'class' => 'form-control input-sm required', 'size'=>'5x2']) !!}
                                                {!! $errors->first('visit_purpose','<span class="help-block">:message</span>') !!}
                                            </div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-primary">
                                <div class="panel-heading"> <strong>Particulars of Organizations / Company</strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix image-outside-div">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('eco_zone_id') ? 'has-error': ''}}">
                                                {!! Form::label('eco_zone_id','1. Preffered Investment Zone / Area',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('eco_zone_id', $economicZone,
                                                    (!empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : null),
                                                    ['data-rule-maxlength'=>'64', 'placeholder' => 'Select One',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('eco_zone_id','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 image-responsive">
                                                <div class="col-md-5 text-left"><strong class="required-star">Profile Picture:</strong><br/>
                                                    <span class="text-danger" style="font-size: 9px; font-weight: bold">
                                                        [File Format: *.jpg/ .jpeg | File size within 3 MB</span></div>
                                                <div class="col-md-7 profilePicArea">
                                                    <?php
                                                        $fileUrl = (!empty($alreadyExistApplicant->applicant_pic) ? $alreadyExistApplicant->applicant_pic : '');
                                                        $requiredClass = '';
                                                        if(!file_exists($fileUrl)){
                                                            $requiredClass = 'required';
                                                        }
                                                    ?>
                                                    {!! Form::file('applicant_pic', ['class' => $requiredClass, 'style'=>'border:0px !important;','id'=>'applicant_pic','onchange' => 'imageDisplay(this)']) !!}
                                                    {!! Html::image((empty($requiredClass) ? $alreadyExistApplicant->applicant_pic : 'assets/images/avatar5.png'), 'A Picture', [ 'id'=>'profile_image','class'=> 'img-responsive image-style']) !!}
                                                    @if (Session::has('error_message'))
                                                        <div class="text-danger">{{ Session::get('error_message') }}</div>
                                                    @endif
                                                    <div id="upload_error" class="text-danger"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix ">
                                        <div class="row">
                                            <div class="col-md-8">
                                                {!! Form::label('infrastructureReq',' 2. Full Address of Representative Company or Organization',
                                                ['class'=>'text-left col-md-12']) !!}
                                            </div>
                                            <div class="col-md-4"><br/></div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('house_no') ? 'has-error': ''}}">
                                                {!! Form::label('house_no','Address Line 1.', ['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('house_no',(!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : null),
                                                    ['data-rule-maxlength'=>'80','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('road_no','Address Line 2. ',['class'=>'col-md-5 text-left']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('road_no',(!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : null),
                                                    ['data-rule-maxlength'=>'80','class' => 'form-control input-sm']) !!}
                                                    {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('post_code','Post Code',['class'=>'col-md-5 text-left']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('post_code', (!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : null),
                                                    ['data-rule-maxlength'=>'20','class' => 'form-control input-sm']) !!}
                                                    {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('country') ? 'has-error': ''}}">
                                                {!! Form::label('country','Country',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('country', $countries, (!empty($alreadyExistApplicant->country) ? $alreadyExistApplicant->country : ''),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                    {!! $errors->first('country','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 has-feedback {{ $errors->has('state') ? 'has-error' : ''}}" id="state_div">
                                                {!! Form::label('state','City',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('state', (!empty($alreadyExistApplicant->state) ? $alreadyExistApplicant->state : ''),
                                                    ['class'=>'textOnly form-control required', 'placeholder' => 'Name of your city',  'data-rule-maxlength'=>'40', 'id'=>"state"]) !!}
                                                    {!! $errors->first('state','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 has-feedback {{ $errors->has('province') ? 'has-error' : ''}}" id="province_div">
                                                {!! Form::label('province','State / Province',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('province', (!empty($alreadyExistApplicant->province) ? $alreadyExistApplicant->province : null),
                                                    ['class'=>'textOnly form-control required', 'data-rule-maxlength'=>'40','placeholder' => 'Enter your Province', 'id'=>"province"]) !!}
                                                    {!! $errors->first('province','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                      </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('phone_home') ? 'has-error': ''}}">
                                                {!! Form::label('phone_home','Phone (Home)',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('phone_home',
                                                    (!empty($alreadyExistApplicant->phone_home) ? $alreadyExistApplicant->phone_home : null),
                                                    ['data-rule-maxlength'=>'20', 'class' => 'phone form-control input-sm required']) !!}
                                                    {!! $errors->first('phone_home','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('phone_office','Phone (Office)',['class'=>'text-left col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('phone_office', (!empty($alreadyExistApplicant->phone_office) ? $alreadyExistApplicant->phone_office : null),
                                                    ['data-rule-maxlength'=>'20', 'class' => 'phone form-control input-sm number']) !!}
                                                    {!! $errors->first('phone_office','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('mobile') ? 'has-error': ''}}">
                                                {!! Form::label('mobile','Mobile No.',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('mobile', (!empty($alreadyExistApplicant->mobile) ? $alreadyExistApplicant->mobile : null),
                                                    ['data-rule-maxlength'=>'20', 'class' => 'phone form-control input-sm required']) !!}
                                                    {!! $errors->first('mobile','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('fax','Fax No',['class'=>'text-left col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('fax', (!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : null), ['data-rule-maxlength'=>'20',
                                                    'class' => 'form-control input-sm number']) !!}
                                                    {!! $errors->first('fax','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('email') ? 'has-error': ''}}">
                                                {!! Form::label('email','Email',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('email',(!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : null),
                                                    ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                                    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('website','Website',['class'=>'text-left col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('website',(!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : null),
                                                    ['data-rule-maxlength'=>'60','class' => 'form-control input-sm', 'placeholder'=>'www.example.com']) !!}
                                                    {!! $errors->first('website','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <strong style="display: inline-block;width:60%;">Correspondent Information</strong>
                                    <span class="text-right" id="full_same_as_authorized" style=" width: 38%;display: inline-block;">
                                        {!! Form::checkbox('same_as_authorized','Yes',(empty($alreadyExistApplicant->same_as_authorized) ? false : true),
                                        ['class' => 'text-left','onclick'=>'editAuthorizeInfo(this)','id'=>'same_as_authorized']) !!}
                                        {!! Form::label('same_as_authorized','Same as mailing address',['class'=>'text-left','style'=>'font-size:13px;']) !!}
                                    </span>
                                </div>

                                <div class="panel-body">
                                    <div id="first_step_authorize_info">
                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-12 {{$errors->has('correspondent_name') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_name','3. Name of the Correspondent Company Name (Abroad)',
                                                    ['class'=>'text-left required-star col-md-6']) !!}
                                                    <div class="col-md-6">
                                                        <?php $authCorrespondent_name = (!empty($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : ''); ?>
                                                        {!! Form::text('correspondent_name',(!empty($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name),
                                                        ['data-rule-maxlength'=>'64','class' => 'textOnly form-control input-sm required','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_name','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_nationality') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_nationality','Nationality',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authNationality = (!empty($logged_user_info->nationality) ? $logged_user_info->nationality : ''); ?>
                                                        {!! Form::select('correspondent_nationality', $nationality,
                                                        (!empty($alreadyExistApplicant->correspondent_nationality) ? $alreadyExistApplicant->correspondent_nationality : $authNationality),
                                                        ['class' => 'form-control input-sm required','readonly'=>true, 'placeholder' => 'Select One']) !!}
                                                        {!! $errors->first('correspondent_nationality','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="row"></div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class=" col-md-12 ">
                                                    {!! Form::label('infrastructureReq','4. Correspondent Address & Contact Details :', ['class'=>'text-left col-md-12']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_country') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_country','Country',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authCountry = (!empty($logged_user_info->country) ? $logged_user_info->country : ''); ?>
                                                        {!! Form::select('correspondent_country', $countries,
                                                        (!empty($alreadyExistApplicant->correspondent_country) ? $alreadyExistApplicant->correspondent_country : $authCountry),
                                                        ['class' => 'form-control input-sm required','placeholder' => 'Select One','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_country','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            <div class="col-md-6 has-feedback {{ $errors->has('correspondent_state') ? 'has-error' : ''}}" id="correspondent_state_div">
                                                {!! Form::label('correspondent_state','State',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    <?php $authState = (!empty($logged_user_info->state) ? $logged_user_info->state : ''); ?>
                                                    {!! Form::text('correspondent_state', (!empty($alreadyExistApplicant->correspondent_state) ? $alreadyExistApplicant->correspondent_state : $authState), $attributes = array('class'=>'form-control','readonly'=>true,
                                                    'placeholder' => 'Name of your state / division', 'data-rule-maxlength'=>'40', 'id'=>"correspondent_state")) !!}
                                                    {!! $errors->first('correspondent_state','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                      </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                            <div class="col-md-6 has-feedback {{ $errors->has('correspondent_province') ? 'has-error' : ''}}" id="correspondent_province_div">
                                                {!! Form::label('correspondent_province','Province',['class'=>'text-left col-md-5 required-star']) !!}
                                                <div class="col-md-7">
                                                    <?php $authProvince = (!empty($logged_user_info->province) ? $logged_user_info->province : ''); ?>
                                                    {!! Form::text('correspondent_province', (!empty($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince),
                                                    $attributes = array('class'=>'form-control', 'data-rule-maxlength'=>'40', 'placeholder' => 'Enter the name of your Province',
                                                    'readonly'=>true,'id'=>"correspondent_province")) !!}
                                                    {!! $errors->first('correspondent_province','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                                <div class="col-md-6  {{$errors->has('correspondent_road_no') ? 'has-error': ''}}">
                                                    <?php $authRoad_no = (!empty($logged_user_info->road_no) ? $logged_user_info->road_no : ''); ?>
                                                    {!! Form::label('correspondent_road_no','Address Line 1',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('correspondent_road_no',(!empty($alreadyExistApplicant->correspondent_road_no) ?
                                                        $alreadyExistApplicant->correspondent_road_no : $authRoad_no), ['data-rule-maxlength'=>'80',
                                                        'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_road_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                        </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_house_no') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_house_no','Address Line 2', ['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authHouse_no = (!empty($logged_user_info->house_no) ? $logged_user_info->house_no : ''); ?>
                                                        {!! Form::text('correspondent_house_no',(!empty($alreadyExistApplicant->correspondent_house_no) ? $alreadyExistApplicant->correspondent_house_no : $authHouse_no), ['data-rule-maxlength'=>'80',
                                                        'class' => 'form-control input-sm','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_house_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6  {{$errors->has('correspondent_post_code') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_post_code','Post Code',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authPost_code = (!empty($logged_user_info->post_code) ? $logged_user_info->post_code : ''); ?>
                                                        {!! Form::text('correspondent_post_code',
                                                        (!empty($alreadyExistApplicant->correspondent_post_code) ?
                                                        $alreadyExistApplicant->correspondent_post_code : $authPost_code),
                                                        ['data-rule-maxlength'=>'20',  'class' => 'form-control input-sm','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_post_code','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('user_phone') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_phone','Phone No.',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authUser_phone = (!empty($logged_user_info->user_phone) ? $logged_user_info->user_phone : ''); ?>
                                                        {!! Form::text('correspondent_phone',
                                                        (!empty($alreadyExistApplicant->correspondent_phone) ? $alreadyExistApplicant->correspondent_phone : $authUser_phone),
                                                        ['data-rule-maxlength'=>'20', 'class' => 'phone form-control input-sm required','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_phone','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_fax') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_fax','Fax No.',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authUser_fax = (!empty($logged_user_info->user_fax) ? $logged_user_info->user_fax : ''); ?>
                                                        {!! Form::text('correspondent_fax',(!empty($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax), ['data-rule-maxlength'=>'20',
                                                        'class' => 'form-control input-sm number','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_fax','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_email') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_email','Email',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authUser_email = (!empty($logged_user_info->user_email) ? $logged_user_info->user_email : ''); ?>
                                                        {!! Form::text('correspondent_email',(!empty($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email),
                                                        ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_email','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_website') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_website','Website',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authWebsite = (!empty($logged_user_info->website) ? $logged_user_info->website : ''); ?>
                                                        {!! Form::text('correspondent_website',(!empty($alreadyExistApplicant->correspondent_website) ?
                                                        $alreadyExistApplicant->correspondent_website : $authWebsite),['data-rule-maxlength'=>'100',
                                                        'class' => 'form-control input-sm','readonly'=>true, 'placeholder'=>'www.example.com']) !!}
                                                        {!! $errors->first('correspondent_website','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <h3 class="text-center stepHeader">Applicant Details (Part B)</h3>
                        <fieldset>
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Particulars of Foreign Incumbent </strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix">
                                        <div class="col-md-8 {{$errors->has('incumbent_national_name') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_national_name','Name of the foreign national',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                {!! Form::text('incumbent_national_name',
                                                (!empty($alreadyExistApplicant->incumbent_national_name) ? $alreadyExistApplicant->incumbent_national_name : null),
                                                ['class' => 'textOnly form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_national_name','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{ $errors->has('incumbent_nationality') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_nationality','Nationality',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                {!! Form::select('incumbent_nationality', $nationality,
                                                (!empty($alreadyExistApplicant->incumbent_nationality) ? $alreadyExistApplicant->incumbent_nationality : null),
                                                ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                {!! $errors->first('incumbent_nationality','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6 {{$errors->has('incumbent_gender') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_gender','Gender',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                <label class="radio-inline">{!! Form::radio('incumbent_gender', 'Male',
                                                    (!empty($alreadyExistApplicant->incumbent_gender) ? $alreadyExistApplicant->incumbent_gender : 'true'),
                                                    ['class'=>' ']) !!} Male</label>
                                                &nbsp;&nbsp;
                                                <label class="radio-inline">{!! Form::radio('incumbent_gender', 'Female',
                                                    (!empty($alreadyExistApplicant->incumbent_gender) ? $alreadyExistApplicant->incumbent_gender : null),
                                                    ['class'=>'']) !!} Female</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_passport') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_passport','Passport No.',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                 <?php $authPassport = !empty($logged_user_info->passport_no) ? $logged_user_info->passport_no : ''; ?>
                                                {!! Form::text('incumbent_passport',
                                                (!empty($alreadyExistApplicant->incumbent_passport) ? $alreadyExistApplicant->incumbent_passport : $authPassport),
                                                ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_passport','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 {{$errors->has('incumbent_pass_expire') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_pass_expire','Expiry Date',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                <div class="col-md-12 datepicker input-group date">
                                                    {!! Form::text('incumbent_pass_expire', (!empty($alreadyExistApplicant->incumbent_pass_expire) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_expire, 0, 10)) : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('incumbent_pass_expire','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_pass_issue_place') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_pass_issue_place','Place of Issue',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6 input-group">
                                                {!! Form::text('incumbent_pass_issue_place',
                                                (!empty($alreadyExistApplicant->incumbent_pass_issue_place) ? $alreadyExistApplicant->incumbent_pass_issue_place : null),
                                                ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_pass_issue_place','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6 {{$errors->has('incumbent_pass_issue_date') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_pass_issue_date','Date of Issue',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                <div class="col-md-12 datepicker input-group date ">
                                                    {!! Form::text('incumbent_pass_issue_date',  (!empty($alreadyExistApplicant->incumbent_pass_issue_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_issue_date, 0, 10)) : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('incumbent_pass_issue_date','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{ $errors->has('incumbent_dob') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_dob','Date of Birth',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                <div class="col-md-12 datepicker input-group date">
                                                    {!! Form::text('incumbent_dob', (!empty($alreadyExistApplicant->incumbent_dob) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_dob, 0, 10)) : ''),
                                                    ['class' => 'form-control  input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('incumbent_dob','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 {{$errors->has('incumbent_martial_status') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_martial_status','Martial Status',['class'=>'text-left required-star col-md-6']) !!}
                                            <div class="col-md-6">
                                                <label class="radio-inline">{!! Form::radio('incumbent_martial_status', 'Married',
                                                    (!empty($alreadyExistApplicant->incumbent_martial_status) ? $alreadyExistApplicant->incumbent_martial_status : null),
                                                    ['class'=>' ']) !!} Married</label>
                                                &nbsp;&nbsp;
                                                <label class="radio-inline">{!! Form::radio('incumbent_martial_status', 'Unmarried',
                                                    (!empty($alreadyExistApplicant->incumbent_martial_status) ? $alreadyExistApplicant->incumbent_martial_status : 'true'),
                                                    ['class'=>'']) !!} Unmarried</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class=" col-md-12 ">
                                            {!! Form::label('incumbentPermanentAddress','Contact Information of where you will be staying in Bangladesh ',
                                            ['class'=>'text-left col-md-12']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_country') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_country','Country',['class'=>'text-left required-star col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::select('incumbent_country', $bd_as_country,
                                                (!empty($alreadyExistApplicant->incumbent_country) ? $alreadyExistApplicant->incumbent_country : null),
                                                ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_country','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6 {{$errors->has('incumbent_division') ? 'has-error': ''}}" id="incumbent_division_div">
                                            {!! Form::label('incumbent_division','Division',['class'=>'text-left required-star col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::select('incumbent_division', $division_eng,
                                                (!empty($alreadyExistApplicant->incumbent_division) ? $alreadyExistApplicant->incumbent_division : null),
                                                ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_division','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_district') ? 'has-error': ''}}" id="incumbent_district_div">
                                            {!! Form::label('incumbent_district','District',['class'=>'text-left required-star col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::select('incumbent_district', $district_eng,
                                                (!empty($alreadyExistApplicant->incumbent_district) ? $alreadyExistApplicant->incumbent_district : null),
                                                ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_district','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6  {{$errors->has('incumbent_road_no') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_road_no','Address Line 1',['class'=>'text-left required-star col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('incumbent_road_no',
                                                (!empty($alreadyExistApplicant->incumbent_road_no) ? $alreadyExistApplicant->incumbent_road_no : null),
                                                ['data-rule-maxlength'=>'80','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_road_no','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_house_no') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_house_no','Address Line 2', ['class'=>'text-left col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('incumbent_house_no',
                                                (!empty($alreadyExistApplicant->incumbent_house_no) ? $alreadyExistApplicant->incumbent_house_no : null),
                                                ['data-rule-maxlength'=>'80', 'class' => 'form-control input-sm']) !!}
                                                {!! $errors->first('incumbent_house_no','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6  {{$errors->has('incumbent_post_code') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_post_code','Post Code',['class'=>'text-left col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('incumbent_post_code',
                                                (!empty($alreadyExistApplicant->incumbent_post_code) ? $alreadyExistApplicant->incumbent_post_code : null),
                                                ['data-rule-maxlength'=>'20','class' => 'form-control input-sm']) !!}
                                                {!! $errors->first('incumbent_post_code','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_phone') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_phone','Contact No.',['class'=>'text-left required-star col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('incumbent_phone',
                                                (!empty($alreadyExistApplicant->incumbent_phone) ? $alreadyExistApplicant->incumbent_phone : null),
                                                ['data-rule-maxlength'=>'20', 'class' => 'phone form-control input-sm required']) !!}
                                                {!! $errors->first('incumbent_phone','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6 {{$errors->has('incumbent_fax') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_fax','Fax No.',['class'=>'text-left col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('incumbent_fax',
                                                (!empty($alreadyExistApplicant->incumbent_fax) ? $alreadyExistApplicant->incumbent_fax : null),
                                                ['data-rule-maxlength'=>'20',
                                                'class' => 'form-control input-sm number']) !!}
                                                {!! $errors->first('incumbent_fax','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="col-md-6 {{$errors->has('incumbent_email') ? 'has-error': ''}}">
                                            {!! Form::label('incumbent_email','Email',['class'=>'text-left col-md-4']) !!}
                                            <div class="col-md-8">
                                                {!! Form::text('incumbent_email', (!empty($alreadyExistApplicant->incumbent_email) ? $alreadyExistApplicant->incumbent_email : null),
                                                ['data-rule-maxlength'=>'64','class' => 'form-control input-sm email']) !!}
                                                {!! $errors->first('incumbent_email','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Proposed Visit Schedule</strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix" style="clear:both">
                                            <div class="col-md-4 {{$errors->has('arrival_date') ? 'has-error': ''}}">
                                                <div class="arrival_date input-group date col-md-12">
                                                    {!! Form::label('arrival_date','Arrival Date :',['class'=>'text-left required-star']) !!}
                                                    {!! Form::text('arrival_date', (!empty($alreadyExistApplicant->arrival_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->arrival_date, 0, 10)) : ''),
                                                    ['class' => 'form-control  input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('arrival_date','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 {{$errors->has('departure_date') ? 'has-error': ''}}">
                                                <div class="departure_date input-group date col-md-12">
                                                    {!! Form::label('departure_date','Departure Date :',['class'=>'text-left required-star']) !!}
                                                    {!! Form::text('departure_date',  (!empty($alreadyExistApplicant->departure_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->departure_date, 0, 10)) : ''),
                                                    ['class' => 'form-control  input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('departure_date','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 {{$errors->has('visit_duration') ? 'has-error': ''}}">
                                                {!! Form::label('visit_duration','Duration of staying (In days) :',['class'=>'text-left required-star']) !!}
                                                {!! Form::text('visit_duration',(!empty($alreadyExistApplicant->visit_duration) ? $alreadyExistApplicant->visit_duration : ''),
                                                ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                {!! $errors->first('visit_duration','<span class="help-block">:message</span>') !!}
                                            </div>
                                    </div>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                        </fieldset>

                        <h3 class="text-center stepHeader">Attachments (Part C)</h3>
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group document_upload">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Required Documents for attachment</div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover ">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th colspan="6">Required Attachments</th>
                                                            <th colspan="2">Attached PDF file
                                                                <span onmouseover="toolTipFunction()" data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 1MB)!">
                                                                    <i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; ?>
                                                        @foreach($document as $row)
                                                        <tr>
                                                            <td><div align="center">{!! $i !!}<?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></div></td>
                                                            <td colspan="6">{!!  $row->doc_name !!}</td>
                                                            <td colspan="2">
                                                                <input name="document_id_<?php echo $row->doc_id; ?>" type="hidden" value="{{(!empty($clrDocuments[$row->doc_id]['doucument_id']) ? $clrDocuments[$row->doc_id]['doucument_id'] : '')}}">
                                                                <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>"
                                                                       name="doc_name_<?php echo $row->doc_id; ?>" />
                                                                <input name="file<?php echo $row->doc_id; ?>" <?php if (empty($clrDocuments[$row->doc_id]['file']) ) echo $row->doc_priority == "1" ? "class='required'" : ""; ?>
                                                                       id="file<?php echo $row->doc_id; ?>" type="file" size="20"
                                                                       onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php echo $row->doc_id; ?>', <?php echo $row->doc_priority; ?>)"/>
                                                                @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                                <div class="save_file">
                                                                    <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}" title="{{$row->doc_name}}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                                </div>
                                                                @endif
                                                                <div id="preview_<?php echo $row->doc_id; ?>">
                                                                    <input type="hidden" <?php echo $row->doc_priority == "1" ? "class='required'" : ""; ?> value=""
                                                                           id="validate_field_<?php echo $row->doc_id; ?>" name="validate_field_<?php echo $row->doc_id; ?>" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                            </div>
                            <div class="form-group" style="clear: both">

                        <?php if ($viewMode == "on") { ?>
                            @if(in_array(Auth::user()->desk_id,array(3,4,5,6)) || Auth::user()->user_type == '1x101')
                                  <div class="row">
                                      <div class="col-md-12"><br/></div>
                                      <div class="col-md-12" style="margin-bottom:6px;">
                                          <a href="{{ url('visa-assistance/view-cer/'. Encryption::encodeId($alreadyExistApplicant->created_by))}}"
                                             target="_blank" class="btn btn-warning btn-xs pull-left show-in-view ">
                                              <i class="fa fa-download"></i> <strong>Associated Certificates of Users</strong>
                                          </a>
                                      </div>
                                  </div>
                            @endif {{-- checking if RD desks or system admin --}}

                              @include('visaAssistance::doc-tab')
                              <?php } ?>


                            </div>
                        </fieldset>

                        <h3 class="stepHeader">Submit</h3>
                        <fieldset>
                            <div class="panel panel-primary hiddenDiv">
                                <div class="panel-heading"><strong>Terms and Conditions</strong></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12" style="margin: 12px 0;">
                                            <input id="acceptTerms-2" type="checkbox" name="acceptTerms"
                                                   <?php if(!empty($alreadyExistApplicant->acceptance_of_terms) && $alreadyExistApplicant->acceptance_of_terms == 1) { ?>
                                                   checked <?php } ?> class="required col-md-1 text-left" style="width:3%;">
                                            <label for="acceptTerms-2" class="col-md-11 text-left required-star">I agree with the Terms and Conditions.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <?php if ($viewMode != 'on') { ?>
                            <?php $statusId = (!empty($processlistExist->status_id) ? $processlistExist->status_id : '');
                            if ($statusId == -1 || $statusId == '') {
                                ?>
                                <input type="submit" class="btn btn-primary btn-md cancel" value="Save As Draft" name="sv_draft">
                            <?php } else { ?>
                                &nbsp;
                            <?php } ?>
                            {!! Form::close() !!}<!-- /.form end -->
                        <?php } ?>

                    </div>
                    <?php if ($viewMode == "on" && in_array(Auth::user()->user_type, array('1x101', '2x202', '4x404'))) { ?>
                        @include('visaAssistance::apps_history');
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer-script')
@if($viewMode !== 'on')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
<link rel="stylesheet" href="{{ url('assets/css/jquery.steps.css') }}">
<script src="{{ asset("assets/scripts/jquery.steps.js") }}"></script>
@endif

{{--jQuery code for image upload--}}
<script type="text/javascript">
    function imageDisplay(input) {
        if (input.files && input.files[0]) {
            $("#upload_error").html('');
            var mime_type = input.files[0].type;
            if(!(mime_type=='image/jpeg' || mime_type=='image/jpg')){
                $("#upload_error").html("Image format is not valid. Only JPEG or JPG type images are allowed.");
                return false;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile_image').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script type="text/javascript">
       @if ($viewMode !== 'on')
<?php if (empty($alreadyExistApplicant->correspondent_name)) { ?>
           $('input[name="same_as_authorized"]').trigger('click');
<?php } ?>

       function editAuthorizeInfo() {
           $('#first_step_authorize_info').find('input.form-control,select.form-control').attr('readonly', function (i, v) {
               return !v;
           });
       };

       function uploadDocument(targets, id, vField, isRequired)
       {
           var inputFile =  $("#" + id).val();
           if(inputFile == ''){
               $("#" + id).html('');
               document.getElementById("isRequired").value = '';
               document.getElementById("selected_file").value = '';
               document.getElementById("validateFieldName").value = '';
               document.getElementById(targets).innerHTML = '<input type="hidden" class="required" value="" id="'+vField+'" name="'+vField+'">';
               if ($('#label_' + id).length) $('#label_' + id).remove();
               return false;
           }

           try{
               document.getElementById("isRequired").value = isRequired;
               document.getElementById("selected_file").value = id;
               document.getElementById("validateFieldName").value = vField;
               document.getElementById(targets).style.color = "red";
               var action = "{{url('/visa-assistance/upload-document')}}";

               $("#" + targets).html('Uploading....');
               var file_data = $("#" + id).prop('files')[0];
               var form_data = new FormData();
               form_data.append('selected_file', id);
               form_data.append('isRequired', isRequired);
               form_data.append('validateFieldName', vField);
               form_data.append('_token', "{{ csrf_token() }}");
               form_data.append(id, file_data);
               $.ajax({
                   target: '#' + targets,
                   url:action,
                   dataType: 'text',  // what to expect back from the PHP script, if anything
                   cache: false,
                   contentType: false,
                   processData: false,
                   data: form_data,
                   type: 'post',
                   success: function(response){
                       $('#' + targets).html(response);
                       var fileNameArr = inputFile.split("\\");
                       var l = fileNameArr.length;
                       if ($('#label_' + id).length)
                           $('#label_' + id).remove();
                       var newInput = $('<label id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                       $("#" + id).after(newInput);
                       var validate_field = $('#'+vField).val();
                       if(validate_field ==''){
                           document.getElementById(id).value = '';
                       }
                   }
               });
           } catch (err) {
               document.getElementById(targets).innerHTML = "Sorry! Something Wrong.";
           }
       }

       $(document).ready(function () {

<?php if (empty($alreadyExistApplicant->same_as_authorized)) { ?>
               $('input[name="same_as_authorized"]').trigger('click');
<?php } ?>

           var form = $("#VisaAssisForm").show();
           form.steps({
               headerTag: "h3",
               bodyTag: "fieldset",
               transitionEffect: "slideLeft",
               onStepChanging: function (event, currentIndex, newIndex) {
                   // Always allow previous action even if the current form is not valid!
                   if (currentIndex > newIndex)
                   {
                       return true;
                   }
                   // Forbid next action on "Warning" step if the user is to young
                   if (newIndex === 3 && Number($("#age-2").val()) < 18)
                   {
                       return false;
                   }
                   // Needed in some cases if the user went back (clean up)
                   if (currentIndex < newIndex)
                   {
                       // To remove error styles
                       form.find(".body:eq(" + newIndex + ") label.error").remove();
                       form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                   }
                   form.validate().settings.ignore = ":disabled,:hidden";
                   return form.valid();
               },
               onStepChanged: function (event, currentIndex, priorIndex) {

               },
               onFinishing: function (event, currentIndex) {
                   form.validate().settings.ignore = ":disabled";
                   return form.valid();
               },
               onFinished: function (event, currentIndex) {
                   errorPlacement: function errorPlacement(error, element) {
                       element.before(error);
                   }
               }
           });

           var popupWindow = null;
           $('.finish').on('click', function (e) {
               if ($('#acceptTerms-2').is(":checked")) {
                   $('#acceptTerms-2').removeClass('error');
                   $('#home').css({"display": "none"});
                   popupWindow = window.open('<?php echo URL::to('/visa-assistance/preview'); ?>', 'Sample', '');
               } else {
                   $('#acceptTerms-2').addClass('error');
                   return false;
               }
           });

           $("select").change(function () {
               var id = $(this).attr("id");
               var val = $(this).val();
               $(this).find('option').removeAttr("selected");
               if(val !='') {
                   $(this).find('option[value="' + val + '"]').attr('selected', 'selected');
                   $(this).val(val);
               }
           });

           $(".document_upload input[type=file]").change(function () {
               var fileName = $(this).val();
               var id = $(this).attr("id");
               var fileNameArr = fileName.split("\\");
               var l = fileNameArr.length;
               if ($('#label_' + id).length)
                   $('#label_' + id).remove();
               var newInput = $('<br/><label id="label_' + id + '"><b>File: ' + fileNameArr[l - 1] + '</b></label>');
               $(this).after(newInput);
           });


           $("#incumbent_passport").blur(function () {
               var passport_no = $(this).val().trim();
               var app_id = $('#app_id').val();
               if (passport_no != '') {
                   $(this).after('<span class="loading_data">Loading...</span>');
                   var self = $(this);
                   if (passport_no.length == 9) {
                       $.ajax({
                           type: "GET",
                           url: "<?php echo url(); ?>/visa-assistance/check-passport_no",
                           data: {
                               passport_no: passport_no,
                               app_id: app_id,
                           },
                           success: function (res) {
                               if (res > 0) {
                                   $('.btn-primary').attr("disabled", true);
                                   $('.pss-error').html('The passport number has already been used by some other user!');
                               } else {
                                   $('.btn-primary').attr("disabled", false);
                                   $('.pss-error').html('');
                               }
                               self.next().hide();
                           }
                       });
                   } else {
                       self.next().hide();
                       $('.pss-error').html('Valid passport number required!');
                       $('.btn-primary').attr("disabled", true);
                   }
               }
           });

       });

       $(document).ready(function () {
           $('.datepicker').datetimepicker({
               viewMode: 'years',
               format: 'DD-MMM-YYYY',
               maxDate: (new Date()),
               minDate: '01/01/1970'
           });



           $("#bd_mission_country").change(function () {
               var iso = $('#bd_mission_country').val();
               $(this).after('<span class="loading_data">Loading...</span>');
               var self = $(this);
               $.ajax({
                   type: "GET",
                   url: "<?php echo url(); ?>/visa-assistance/get-embassy-by-country",
                   data: {
                       iso: iso
                   },
                   success: function (response) {
                       var option;
                       if (response.responseCode == 1) {
                           $.each(response.data, function (id, value) {
                                    if (value == null) {
                                        option += '<option value="">No embassy has been found for this country! </option>';
                                    } else {
                                        option += '<option value="' + id + '">' + value + '</option>';
                                    }
                           });
                       }else{
                           if(iso == ''){
                           option += '<option value="">Select country first! </option>';
                           }
                           else {
                           option += '<option value="">No embassy has been found for this country! </option>';
                       }
                       }
                       $("#embassies").html(option);
                       $(self).next().hide();
                   }
               });
           });
           $("#bd_mission_country").trigger('change');

           var today = new Date();
           var yyyy = today.getFullYear();
           var mm = today.getMonth();
           var dd = today.getDate();
           $('#arrival_date, .arrival_date').datetimepicker({
               viewMode: 'years',
               format: 'DD-MMM-YYYY',
               minDate: '01/01/' + (yyyy - 6)
           });
           $('#departure_date, .departure_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: false // Important! See issue #1075
           });
           startDateSelected = '';
           $(".arrival_date").on("dp.change", function (e) {
               $('#departure_date').data("DateTimePicker").minDate(e.date);
               $('.departure_date').data("DateTimePicker").minDate(e.date);
               startDateSelected = e.date;
           });
           $(".departure_date").on("dp.change", function (e) {
               var startDate = startDateSelected;
               var endDate = e.date;
               if (startDate != '' && endDate != '') {
                   var days = (endDate - startDate) / 1000 / 60 / 60 / 24;
                   $('#visit_duration').val(Math.floor(days));
               }
           });

           $("#incumbent_division").change(function () {
               var divisionId = $('#incumbent_division').val();
               $(this).after('<span class="loading_data">Loading...</span>');
               var self = $(this);
               $.ajax({
                   type: "GET",
                   url: "<?php echo url(); ?>/visa-assistance/get-district-by-division",
                   data: {
                       divisionId: divisionId
                   },
                   success: function (response) {
                       var option = '<option value="">Select One</option>';
                       if (response.responseCode == 1) {
                           $.each(response.data, function (id, value) {
                               option += '<option value="' + id + '">' + value + '</option>';
                           });
                       }
                       $("#incumbent_district").html(option);
                       $(self).next().hide();
                   }
               });
           });
       });
               @endif {{--view mode off --}}

              @if($viewMode == 'on')

            @if(($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)
                    $(document).ready(function () {
                   var today = new Date();
                   var yyyy = today.getFullYear();
                   var mm = today.getMonth();
                   var dd = today.getDate();
                   $('#pay_order_date').datetimepicker({
                       viewMode: 'years',
                       format: 'DD-MMM-YYYY',
                       minDate: '01/01/' + (yyyy - 1),
                       maxDate: (new Date())
                   });
        }); //  end of document.ready
        @endif {{-- checking status --}}

   $('#inputForm select').each(function(index){
           var text = $(this).find('option:selected').text();
           var id = $(this).attr("id");
           var val = $(this).val();
           $('#' + id + ' option:selected').replaceWith("<option value='" + val + "' selected>" + text + "</option>");
       });

       $("#inputForm :input[type=text]").each(function(index) {
           $(this).attr("value", $(this).val());
       });
        $("#inputForm textarea").each(function(index) {
            $(this).text($(this).val());
        });
       $("#inputForm select").css({
           "border" : "none",
           "background" : "#fff",
           "pointer-events" : "none",
           "box-shadow": "none",
           "-webkit-appearance" : "none",
           "-moz-appearance" : "none",
           "appearance": "none"
       });
       $("#inputForm fieldset").css({"display": "block"});
       $("#inputForm #full_same_as_authorized").css({"display": "none"});
       $("#inputForm .actions").css({"display": "none"});
       $("#inputForm .steps").css({"display": "none"});
       $("#inputForm .draft").css({"display": "none"});
       $("#inputForm .title ").css({"display": "none"});
       //document.getElementById("previewDiv").innerHTML = document.getElementById("projectClearanceForm").innerHTML;
       $('#inputForm #showPreview').remove();
       $('#inputForm #save_btn').remove();
       $('#inputForm #save_draft_btn').remove();
       $('#inputForm .stepHeader, #inputForm .calender-icon,#inputForm .pss-error,#inputForm .hiddenDiv').remove();
       $('#inputForm .required-star').removeClass('required-star');
       $('#inputForm input[type=hidden], #inputForm input[type=file]').remove();
       $('#inputForm .panel-orange > .panel-heading').css('margin-bottom','10px');
       $('#invalidInst').html('');

       $('#inputForm').find('input:not([type=radio],checked),textarea').each(function(){
            if (this.value != ''){
                var displayOp = ''; //display:block
            } else {
                var displayOp = 'display:none';
            }

            if($(this).hasClass("onlyNumber") && !$(this).hasClass("nocomma"))
            {
                var thisVal = commaSeparateNumber(this.value);
                $(this).replaceWith("<span class='onlyNumber " +this.className+ "' style='background-color:#ddd !important; height:auto; margin-bottom:2px; padding:6px;"
                        + displayOp + "'>" + thisVal + "</span>");
            }else {
                $(this).replaceWith("<span class='" +this.className+ "' style='background-color:#ddd; height:auto; margin-bottom:2px; padding:6px;"
                        + displayOp + "'>" + this.value + "</span>");
            }
       });

        $('#inputForm .btn').not('.show-in-view').each(function(){
            $(this).replaceWith("");
        });

       $('#acceptTerms-2').attr("onclick",'return false').prop("checked",true).css('margin-left','5px');
       $('#inputForm').find('input[type=radio]').each(function(){
           jQuery(this).attr('disabled', 'disabled');
       });

        $("#inputForm select").replaceWith(function (){

            var selectedText = $(this).find('option:selected').text().trim();
            var displayOp = '';
            if (selectedText != '' && selectedText != 'Select One') {
                displayOp = ''; //display:block
            } else {
                displayOp = 'display:none';
            }

            return "<span class='" +this.className+ "' style='background-color:#ddd; height:auto; margin-bottom:2px; " +
                    displayOp + "'>" + selectedText + "</span>";
        });
       @endif {{-- viewMode is on --}}

       function toolTipFunction() {
           $('[data-toggle="tooltip"]').tooltip();
       }

       $(document).on('click', '.download', function (e) {
           var value = $(this).attr('data');
           $('.show_' + value).show();
       });
</script>


@if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(1,2,3,4,5,6)))

    {{--batch process start--}}
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    <script language="javascript">
        var numberOfCheckedBox = 0;
        var curr_app_id = '';
        function setCheckBox() {
            numberOfCheckedBox = 0;
            var flag = 1;
            var selectedWO = $("input[type=checkbox]").not(".selectall");
            selectedWO.each(function() {
                if (this.checked){
                    numberOfCheckedBox++;
                }else {
                    flag = 0;
                }
            });
            if (flag == 1){
                $("#chk_id").checked = true;
            }else {
                $("#chk_id").checked = false;
            }
            if(numberOfCheckedBox >= 1){
                $('.applicable_status').trigger('click');
            }

        }

        function changeStatus(check){
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            setCheckBox();
        }

        $(document).ready(function() {

            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
//            $('.applicable_status').trigger('click');
//            $("#assign_form").validate({
//                errorPlacement: function() {
//                    return false;
//                }
//            });
            $("#apps_from").validate({
                errorPlacement: function() {
                    return false;
                }
            });
            $("#batch_from").validate({
                errorPlacement: function() {
                    return false;
                }
            });

            var base_checkbox = '.selectall';
            $(base_checkbox).click(function() {
                if (this.checked) {
                    $('.appCheckBox:checkbox').each(function() {
                        this.checked = true;
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    });
                } else {
                    $('.appCheckBox:checkbox').each(function() {
                        this.checked = false;
                        //   $('#status_id').attr('disabled',false);
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    });
                }
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                setCheckBox();

            });
            $('.appCheckBox:checkbox').not(base_checkbox).click(function() {
                $(".selectall").prop("checked", false);
            });
            var break_for_pending_verification = 0;
            $(document).ready(function() {

                $("#status_id").trigger("click");
                var curr_app_id = $("#curr_app_id").val();
                var curr_status_id = $("#curr_status_id").val();
                $.ajaxSetup({async: false});
                var _token = $('input[name="_token"]').val();
                var delegate = '{{ @$delegated_desk }}';
                var state = false;
                $.post('/visa-assistance/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate, _token: _token}, function(response) {

                    if (response.responseCode == 1) {
                        var option = '';
                        option += '<option selected="selected" value="">Select Below</option>';
                        $.each(response.data, function(id, value) {
                            option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                        });
                        {{--state = !state; --}}
                        {{--$("#status_id").prop("", state ? $("option").length : 1); --}}
                        $("#status_id").html(option);
                        $("#status_id").trigger("change");
                        $("#status_id").focus();
                    } else if (response.responseCode == 5){
                        alert('Without verification, application can not be processed');
                        break_for_pending_verification = 1;
                        option = '<option selected="selected" value="">Select Below</option>';
                        $("#status_id").html(option);
                        $("#status_id").trigger("change");
                        return false;
                    } else {
                        $('#status_id').html('Please wait');
                    }
                });

                $.ajaxSetup({async: true});

            });
            $(document).on('change', '.status_id', function() {
                var curr_app_id_for_process = $("#curr_app_id").val();
                var curr_status_id_for_process = $("#curr_status_id").val();

                var object = $(".status_id");
                var obj = $(object).parent().parent().parent();
                var id = $(object).val();
                var _token = $('input[name="_token"]').val();
                var status_from = $('#status_from').val();
                $('#sendToDeskOfficer').css('display', 'block');
                if (id == 0) {
                    obj.find('.param_id').html('<option value="">Select Below</option>');
                } else {
                    //  if(id == 5 || id == 8 || id == 9 || id == 14 || id == 2){
                    $.post('/visa-assistance/ajax/process', {id: id, curr_app_id: curr_app_id_for_process, status_from: curr_status_id_for_process, _token: _token}, function(response) {
                        if (response.responseCode == 1) {
                            var option = '';
                            option += '<option selected="selected" value="">Select Below</option>';
                            var countDesk = 0;
                            $.each(response.data, function(id, value) {
                                countDesk++;
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                            obj.find('#desk_id').html(option);
                            $('#desk_id').attr("disabled", false);
                            $('#remarks').attr("disabled", false);

                            if (countDesk == 0){
                                $('.dd_id').removeClass('required');
                                $('#sendToDeskOfficer').css('display', 'none');
                            } else{
                                $('.dd_id').addClass('required');
                            }

                            if (response.status_to == 5 || response.status_to == 8 || response.status_to == 16 || response.status_to == 17 || response.status_to == 19
                                    || response.status_to == 24 || response.status_to == 10 || response.status_to == 22){

                                $('#remarks').addClass('required');
                                $('#remarks').attr("disabled", false);
                            }else{
                                $('#remarks').removeClass('required');
                            }

                            if (response.file_attach == 1) {
                                $('#sendToFile').css('display', 'block');
                            }else{
                                $('#sendToFile').css('display', 'none');
                            }
                         } // when response.responseCode == 1
                    });
                    //  }
                }
            });
        });
        function resetElements(){
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
        }

        <?php if (isset($search_status_id) && $search_status_id > 0) { ?>
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: base_url + '/dashboard/search-result',
            type: 'post',
            data: {
                _token: _token,
                status_id:<?php echo $search_status_id; ?>,
            },
            dataType: 'json',
            success: function(response) {
                // success
                if (response.responseCode == 1) {
                    $('table.resultTable tbody').html(response.data);
                } else {
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            beforeSend: function(xhr) {
                console.log('before send');
            },
            complete: function() {
                //completed
            }
        });
        <?php } ?>

         $("#search_app").on('click', function() {

            $('.selectall').addClass("hidden");
            var _token = $('input[name="_token"]').val();
            var tracking_number = $('#tracking_number').val();
            var passport_number = $('#passport_number').val();
            var applicant_name = $('#applicant_name').val();
            var nationality = $('#nationality').val();
            var status_id = $('#modal_status_id').val();
            var selected = false;

            $.ajax({
                url: base_url + '/visa-assistance/search-result',
                type: 'post',
                data: {
                    _token: _token,
                    tracking_number: tracking_number,
                    passport_number: passport_number,
                    applicant_name: applicant_name,
                    nationality: nationality,
                    status_id:status_id,
                },
                dataType: 'json',
                success: function(response) {
                    // success
                    if (response.responseCode == 1) {
//                        alert(organization);
//                        window.location = base_url + "/dashboard/search-view/";

                        $('table.resultTable tbody').html(response.data);
                        $('#modal-close').trigger('click');
//                        $('#modal-dialog').hide();
//                        $('#frmAddProject').modal('hide');
//                        console.log($('#frmAddProject').html());
                    } else {
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                beforeSend: function(xhr) {
                    console.log('before send');
                },
                complete: function() {
                    //completed
                }
            });
        });

    </script>
@endif
@endsection <!--- footer-script--->
