@extends('layouts.admin')

@section('page_heading',trans('messages.sms_status'))

@section('content')
<div class="col-lg-12">
    @include('partials.messages')
    <div class="panel panel-primary">
            <div class="panel-heading">
                {!! Form::open(['url' => 'support/search-sms','method' => 'get','id' => 'sb-search'])!!}
                <div class="col-md-6 input-group custom-search-form">
                <span class="input-group-addon">
                    <i class="fa fa-list"></i>
                </span>
                    <input name="q" type="text" class="form-control input-sm" value="{{Request::get('q')}}" placeholder="Search by Mobile No">
                <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                {!! Form::close()!!}
            </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Message</th>
                            <th>Destination</th>
                            <th>Status</th>
                            <th>No. Of Try</th>
                            <th>Created At</th>
                            <th>Sent On</th>
                            <th>Message Type</th>
                            <th>Template Id</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->

@endsection

@section('footer-script')

@include('partials.datatable-scripts')
{{--<script src="{{ asset("assets/scripts/datatable/handlebars.js") }}" type="text/javascript"></script>--}}

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<script>
    $(function () {

        $('#list').DataTable({
            iDisplayLength: 50,
            processing: true,
            serverSide: true,
            searching: true,
             ajax: {
                url:  '{{url("support/get-row-details-sms_status")}}',
                method:'post',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
                },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'source', name: 'source'},
                {data: 'destination', name: 'destination'},
                {data: 'is_sent', name: 'is_sent'},
                {data: 'no_of_try', name: 'no_of_try'},
                {data: 'created_at', name: 'created_at'},
                {data: 'sent_on', name: 'sent_on'},
                {data: 'msg_type', name: 'msg_type'},
                {data: 'template_id', name: 'template_id'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });
    });
</script>
@endsection
