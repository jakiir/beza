@extends('layouts.admin')

@section('page_heading',trans('messages.sms_status'))

@section('content')
    <div class="col-lg-12">
        @include('partials.messages')
        <div class="panel panel-primary">
            <div class="panel-heading">
                {!! Form::open(['url' => 'support/searchSMS','method' => 'get','id' => 'sb-search'])!!}
                <div class="col-md-6 input-group custom-search-form">
                <span class="input-group-addon">
                    <i class="fa fa-list"></i>
                </span>
                    <input name="q" type="text" class="form-control input-sm" value="{{Request::get('q')}}" placeholder="Search by Mobile No">
                <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                {!! Form::close()!!}
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Message</th>
                            <th>Destination</th>
                            <th>Status</th>
                            <th>No. Of Try</th>
                            <th>Created At</th>
                            <th>Sent On</th>
                            <th>Message Type</th>
                            <th>Template Id</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($search_result as $row)
                            <tr>
                                <td>{!! $row->id !!}</td>
                                <td>{!! substr($row->source,0,30).'.......' !!}</td>
                                <td>{!! $row->destination !!}</td>
                                <td>{!! $row->is_sent !!}</td>
                                <td>{!! $row->no_of_try !!}</td>
                                <td>{!! CommonFunction::updatedOn($row->created_at) !!}</td>
                                <td>{!! CommonFunction::updatedOn($row->sent_on) !!}</td>
                                <td>{!! $row->msg_type !!}</td>
                                <td>{!! $row->template_id !!}</td>
                                <td>
                                    <?php
                                    if($row->is_sent == 1)
                                        echo '<a href="' . url('support/re_send_sms_status/' . Encryption::encodeId($row->id)) .'" class="btn btn-xs btn-primary">Re-send</a> &nbsp;';
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

@endsection

@section('footer-script')

    @include('partials.datatable-scripts')
    {{--<script src="{{ asset("assets/scripts/datatable/handlebars.js") }}" type="text/javascript"></script>--}}
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <script>

        $(function() {
            $('#list').DataTable();

        });

    </script>
@endsection
