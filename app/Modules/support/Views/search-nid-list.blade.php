@extends('layouts.admin')

@section('page_heading',trans('messages.nid_status'))

@section('content')
    <div class="col-lg-12">
        @include('partials.messages')
        <div class="panel panel-primary">
            <div class="panel-heading">
                {!! Form::open(['url' => 'support/search-nid','method' => 'get','id' => 'sb-search'])!!}
                <div class="col-md-6 input-group custom-search-form">
                <span class="input-group-addon">
                    <i class="fa fa-list"></i>
                </span>
                    <input name="q" type="text" class="form-control input-sm" value="{{Request::get('q')}}" placeholder="Search by National Id">
                <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                {!! Form::close()!!}
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>National Id</th>
                            <th>Name</th>
                            <th>Date of Birth</th>
                            <th>Verification Flag</th>
                            <th>Submitted Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($search_result as $row)
                            <tr>
                                <td>{!! $row->nid !!}</td>
                                <td>{!! $row->name !!}</td>
                                <td>{!! CommonFunction::age($row->dob) !!}</td>
                                <td><?php
                                    if($row->verification_flag == 0 || $row->verification_flag == -1){
                                        echo 'In Process';
                                    }elseif($row->verification_flag == 1){
                                        echo 'Process';
                                    }elseif($row->verification_flag == -9){
                                        echo 'Failed';
                                    }else{
                                        echo $row->verification_flag;
                                    }
                                    ?></td>
                                <td>{!! CommonFunction::updatedOn($row->submitted_at) !!}</td>
                                <td>
                                    <?php
                                    if($row->verification_flag == -9)
                                        echo '<a href="' . url('support/re_submit_nid_status/' . Encryption::encodeId($row->id)) . '" class="btn btn-xs btn-primary" onclick="return confirm(\'Are you sure you re-submit it ?\');">Re-submit</a> &nbsp;';
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

@endsection

@section('footer-script')

    @include('partials.datatable-scripts')
    {{--<script src="{{ asset("assets/scripts/datatable/handlebars.js") }}" type="text/javascript"></script>--}}
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <script>

        $(function() {
            $('#list').DataTable();

        });

    </script>
@endsection
