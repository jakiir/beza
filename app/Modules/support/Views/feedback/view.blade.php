@extends('layouts.admin')

@section('page_heading',trans('messages.feedback'))
<link rel="stylesheet" href="{{ asset("assets/stylesheets/AdminLTE.min.css") }}" />

@section('content')
<style>
    /*    .direct-chat-messages{
            overflow: visible !important;
        }*/
</style>
<div class="col-lg-12">

    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
    {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}

    <div class="panel panel-primary">
        <div class="panel-heading">
            <b> {!!trans('messages.feedback_view')!!} </b>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div id="feedback-info">
                <div class="col-md-8">
                    <div class="form-group col-md-12 {{$errors->has('topic_id') ? 'has-error' : ''}}">
                        {!! Form::label('topic_id','Topic: ',['class'=>'col-md-5']) !!}
                        <div class="col-md-7">
                            {!! Form::select('topic_id',$topics, null, array('class'=>'form-control required')) !!}
                            {!! $errors->first('topic_id','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group col-md-12 {{$errors->has('description') ? 'has-error' : ''}}">
                        {!! Form::label('description','আপনার সমস্যাটি বর্ণনা করুন ',['class'=>'col-md-5']) !!}
                        <div class="col-md-7">
                            {!! Form::textarea('description', $data->description, ['class'=>'form-control required', 'size' => "10x10"]) !!}
                            {!! $errors->first('description','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <?php
                    $priorities = array(
                        'low' => 'Low',
                        'medium' => 'Medium',
                        'high' => 'High',
                    );
                    ?>
                    <div class="form-group col-md-10 {{$errors->has('priority') ? 'has-error' : ''}}">
                        {!! Form::label('priority','Priority: ',['class'=>'col-md-5']) !!}
                        <div class="col-md-7">
                            {!! Form::select('priority',$priorities, $data->priority, array('class'=>'form-control required')) !!}
                            {!! $errors->first('priority','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <!--                    <div class="form-group col-md-12 {{$errors->has('screenshot') ? 'has-error' : ''}}">
                                            {!! Form::label('screenshot','সমস্যাটির স্ক্রিনশট সংযুক্তি (প্রযোজ্য ক্ষেত্রে)',['class'=>'col-md-5']) !!}
                                            <div class="col-md-7">
                                                {!! Form::file('screenshot', '', ['class'=>'form-control']) !!}
                                                {!! $errors->first('screenshot','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>-->
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-5">
                        {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>

            <div class="overlay" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>

        </div><!-- /.box -->
    </div>

    <!--2x203 for Call Centre, 2x202 for IT help Desk, -->
    @if($logged_in_user_type == '1x101' OR $logged_in_user_type == '2x202' OR $logged_in_user_type == '2x203') 
    <div class="panel panel-success">
        <div class="panel-heading">
            <b>Assign User</b>
        </div>
        <div class="panel-body">
            {!! Form::open(array('url' => '/support/assign-feedback/'.$encrypted_id,'method' => 'patch', 'class' => 'form-horizontal smart-form', 
            'id' => 'feedback-reply-status', 'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}
            <div class="form-group col-md-10 {{$errors->has('assigned_to') ? 'has-error' : ''}}">
                {!! Form::label('assigned_to','Assigned to: ',['class'=>'col-md-5']) !!}
                <div class="col-md-7">
                    {!! Form::select('assigned_to', $call_centre_users, null, array('class'=>'form-control required')) !!}
                    {!! $errors->first('assigned_to','<span class="help-block">:message</span>') !!}
                </div>
            </div>

            <button type="submit" id="assign" name="assign"  class="btn btn-large btn-success text-right">
                <i class="fa fa-chevron-circle-right"></i> <b>Assign</b></button>
            {!! Form::close() !!}<!-- /.form end -->
        </div>
    </div>
    @endif
    
    <div class="panel panel-info">
        <div class="panel-heading">
            <b>Replies </b>
            {{--@if($data->status  == 'submitted' OR $data->status  == 'replied')--}}
            <span class="text-right">
                {!! Form::open(array('url' => '/support/change-feedback-status','method' => 'patch', 'class' => 'form-horizontal smart-form', 
                'id' => 'feedback-reply-status', 'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}

                {!! Form::hidden('parent_id', $encrypted_id, ['class'=>'']) !!}
                &nbsp;<span>
                    <button type="submit" name="resolved" value="resolved" class="btn btn-success pull-right">
                        <i class="fa fa-chevron-circle-right"></i> <b>Resolved</b></button>
                </span>            
                &nbsp;<span>
                    <button type="submit" name="rejected" value="rejected" class="btn btn-danger pull-right">
                        <i class="fa fa-chevron-circle-right"></i> <b>Rejected</b></button>
                </span>
                &nbsp;<span>
                    <button type="submit" name="canceled" value="canceled" class="btn btn-warning pull-right">
                        <i class="fa fa-chevron-circle-right"></i> <b>Canceled</b></button>
                </span>&nbsp;
                <button type="button" id="viewReplyBoxBtn"  class="btn btn-large btn-primary text-right">
                    <i class="fa fa-chevron-circle-right"></i> <b>Reply</b></button>
                {!! Form::close() !!}<!-- /.form end -->
            </span>
            {{--  @endif --}}
        </div>
        @if(!empty($replies) && count($replies) > 0)
        <div class="panel-body">
            <div class="direct-chat-messages">
                @foreach($replies as $reply)
                <div class="direct-chat-msg <?php
                echo $data->created_by == $reply->replier_id ? 'right' : '';
                ?>">
                    <div class="direct-chat-info clearfix">                            
                        <span class="direct-chat-name pull-<?php
                        echo $data->created_by == $reply->replier_id ? 'right' : 'left';
                        ?>">
                            {{ $reply->replied_by}} 
                        </span>
                        <span class="direct-chat-timestamp pull-<?php
                        echo $data->created_by == $reply->replier_id ? 'left' : 'right';
                        ?>">
                            &nbsp;On
                            {!! CommonFunction::changeDateFormat(substr($reply->time, 0, 10)) !!} 
                            at {{ substr($reply->time, -8)}}
                        </span>
                    </div>
                    <?php $replier_pic = CommonFunction::getPicture('user', $reply->replier_id); ?>
                    <img class="direct-chat-img" alt="image" src="{{ $replier_pic }}">
                    <div class="direct-chat-text"  style="<?php
                    echo $data->created_by == $reply->replier_id ? 'background-color: #d9edf7' : '';
                    ?>"> {{ $reply->description}} </div>
                </div> 
                @endforeach
            </div>
        </div>
        @else
        <div>
            <span class="text-center text-primary text-bold">No replies available right now!</span>
        </div>
        @endif
    </div>


    <div class="clearfix"></div>


    {{-- @if($data->status  == 'submitted') --}}
    <div class="panel panel-success" id="replyBoxDiv" style="display: none">
        <div class="panel-heading">
            <b>Submit a Reply </b>
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                {!! Form::open(array('url' => '/support/store-feedback-reply','method' => 'patch', 'class' => 'form-horizontal smart-form', 
                'id' => 'feedback-reply', 'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}

                {!! Form::hidden('topic_id', $data->topic_id, ['id'=>'topic_id']) !!}
                {!! Form::hidden('parent_feedback', $encrypted_id, ['id'=>'parent_feedback']) !!}

                <div class="form-group col-md-12 {{$errors->has('description') ? 'has-error' : ''}}">
                    {!! Form::label('description','সমস্যাটির উত্তর দিন ',['class'=>'col-md-5']) !!}
                    <div class="col-md-7">
                        {!! Form::textarea('description', '', ['class'=>'form-control required', 'size' => "10x3"]) !!}
                        {!! $errors->first('description','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <!--                <div class="form-group col-md-9 {{$errors->has('screenshot') ? 'has-error' : ''}}">
                                    {!! Form::label('screenshot','সমাধানের স্ক্রিনশট সংযুক্তি (প্রযোজ্য ক্ষেত্রে)',['class'=>'col-md-7']) !!}
                                    <div class="col-md-5">
                                        {!! Form::file('screenshot', '', ['class'=>'form-control']) !!}
                                        {!! $errors->first('screenshot','<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>-->

                <div class="col-md-3">
                    <button type="submit" name="reply" value="reply" class="btn btn-primary pull-right">
                        <i class="fa fa-chevron-circle-right"></i> <b>Reply</b></button>
                </div>
            </div>

            {!! Form::close() !!}<!-- /.form end -->
        </div>
    </div><!-- /.box -->
    {{-- @endif --}}

</div>

@endsection


@section('footer-script')
<script>
    $('#feedback-info').find('input:not([type=checkbox]), hidden, button').each(function () {
        $(this).replaceWith("<span><b>" + this.value + "</b></span>");
    });
    $('#feedback-info').find('textarea').each(function () {
        $(this).replaceWith("<span class=\"span3\"><b>" + this.value + "</b></span>");
    });
    $("#feedback-info").find('select').replaceWith(function () {
        return $(this).find('option:selected').text();
    });

    var _token = $('input[name="_token"]').val();

    var age = -1;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $("#feedback-reply").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });

    $('#viewReplyBoxBtn').on('click', function () {

        $('#replyBoxDiv').toggle('show')
        $("html, body").animate({scrollTop: $(document).height()}, "slow");
    });
</script>
@endsection <!--- footer script--->