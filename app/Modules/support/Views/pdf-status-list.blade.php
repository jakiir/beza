@extends('layouts.admin')

@section('page_heading',trans('messages.pdf_status'))

@section('content')
<div class="col-lg-12">
    @include('partials.messages')
    <div class="panel panel-primary">
        <div class="panel-heading">
            {!! Form::open(['url' => 'support/search-pdf','method' => 'get','id' => 'sb-search'])!!}
            <div class="col-md-6 input-group custom-search-form">
                <span class="input-group-addon">
                    <i class="fa fa-list"></i>
                </span>
                <input name="q" type="text" class="form-control input-sm" value="{{Request::get('q')}}" placeholder="Search by Reference Id">
                <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close()!!}
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>PDF Type</th>
                            <th>Reference Id</th>
                            <th>No. Of Try</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->

@endsection

@section('footer-script')

@include('partials.datatable-scripts')
{{--<script src="{{ asset("assets/scripts/datatable/handlebars.js") }}" type="text/javascript"></script>--}}

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<script>
    $(function () {

        $('#list').DataTable({
            iDisplayLength: 50,
            processing: true,
            serverSide: true,
            searching: true,
             ajax: {
                url:  '{{url("support/get-row-details-pdf_status")}}',
                method:'post',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
                },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'pdf_type', name: 'pdf_type'},
                {data: 'tracking_no', name: 'tracking_no'},
                {data: 'no_of_try', name: 'no_of_try'},
                {data: 'status', name: 'status'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });
    });
</script>
@endsection
