@extends('layouts.admin')

@section('page_heading',trans('messages.feedback_form_title'))

@section('content')

{!! Session::has('success') ? '<div class="alert alert-success alert-dismissible">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
{!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}


<div class="row">
    @if($faqs)
    <div class="col-md-12">
        <div class="">
            <div class="panel-heading">
                <div><strong>{{trans('messages.faq')}}</strong></div>
            </div>
            @foreach($faqs as $faq)
            <div class="form-group">
                <a href='#faq_{{$faq->id}}' class="col-lg-12"><strong>{{ $faq->question}}</strong> </a>
            </div>
            @endforeach
        </div>
    </div>
    @endif        

    <div class="col-md-12"><br/></div>

    @if($faqs)
    <div class="col-md-12">
        <div class="">
            @foreach($faqs as $faq)
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label  class="col-lg-12"><code id='faq_{{$faq->id}}'>প্রশ্ন:</code> {{ $faq->question}} </label>
                        <div class="col-lg-12"> <code>উত্তর:</code>
                            {{ $faq->answer}} 
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif

    <div class="col-md-12"><br/></div>


</div>
@endsection 