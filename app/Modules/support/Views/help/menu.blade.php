@extends('layouts.admin')

@section('page_heading','My Menu')

@section('content')
    <style type="text/css">
        .customclass{
            padding-left: 59px;
        }

        .ul li{
            background-color: red;
        }
        .white{
            background-color: white;
        }

        .menu .sidebar ul li{
            border-bottom: 2px solid gray;
        }
    </style>

    <div class="col-md-12 white">
        <div class="col-md-6 menu col-md-offset-2">
            @include ('navigation.sidebar')
        </div>


    </div>



    <script src="{{ asset("assets/scripts/jquery.min.js") }}" src="" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu .sidefooter').hide();
            $('.menu .text-danger').hide();
            $(".menu .sidebar .nav-second-level li a").addClass('customclass');
            $(".menu .sidebar li").addClass('active');
            $(".menu .sidebar ul li.active > a ").css("background-color", "white");
            $(".menu .sidebar .nav-second-level li a ").css("padding-left", "59px");
            $(".menu .sidebar ul li").addClass("ss");
            $(".menu .panel-body").css("display", "none");
        })
    </script>


@endsection
