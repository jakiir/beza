<?php

Route::group(array('module' => 'Support', 'middleware' => ['auth'], 'namespace' => 'App\Modules\Support\Controllers'), function() {


    Route::get('support/sitemap', "SupportController@sitemap");

    Route::get('support/nid_status', "SupportController@nid_status");
    Route::post('support/get-row-details-nid_status', "SupportController@getNidStatusDetailsData");
    Route::get('support/re_submit_nid_status/{id}', "SupportController@reSubmitNidStatus");
    Route::get('support/search-nid', "SupportController@searchNid");

    Route::get('support/sms_status', "SupportController@sms_status");
    Route::post('support/get-row-details-sms_status', "SupportController@getNotificationDetailsData");
    Route::get('support/re_send_sms_status/{id}', "SupportController@reSendSMSStatus");
    Route::get('support/search-sms', "SupportController@searchSMS");

    Route::get('support/pdf_status', "SupportController@pdf_status");
    Route::post('support/get-row-details-pdf_status', "SupportController@getPdfStatusDetailsData");
    Route::get('support/re_pdf_status/{id}', "SupportController@reGeneratePdfStatus");
    Route::get('support/search-pdf', "SupportController@searchPdf");


    //****** Feedback ****//
    Route::get('support/feedback', "SupportController@feedback");
    Route::get('support/create-feedback', "SupportController@createFeedback");
    Route::get('support/edit-feedback/{id}', "SupportController@editFeedback");
    Route::get('support/view-feedback/{id}', "SupportController@viewFeedback");

    Route::patch('support/store-feedback', "SupportController@storeFeedback");
    Route::patch('support/update-feedback/{id}', "SupportController@updateFeedback");
    Route::patch('support/assign-feedback/{id}', "SupportController@assignFeedback");

    Route::patch('support/store-feedback-reply', "SupportController@storeFeedbackReply");
    Route::patch('support/change-feedback-status', "SupportController@changeFeedbackStatus");

    Route::get('support/get-feedback-details-data', "SupportController@getFeedbackDetailsData");
    Route::post('support/get-uncategorized-feedback-data/{flag}', "SupportController@getUncategorizedFeedbackData");
    //****** End of Feedback ****//
    /* Notice Start */
    Route::get('support/view-notice/{id}', "SupportController@viewNotice");
    /* Notice End */

    Route::get('support/help/{segment}', "SupportController@help");
});


Route::group(array('module' => 'Support', 'middleware' => ['auth', 'checkAdmin'], 'namespace' => 'App\Modules\Pilgrim\Controllers'), function() {

});
