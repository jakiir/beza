<?php

namespace App\Modules\Support\Models;

use App\Libraries\CommonFunction;
use App\Modules\Pilgrim\Models\Pilgrim;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PdfGenerator extends Model {

    protected $table = 'pdf_generator';
    protected $fillable = [
            'id',
            'pdf_type',
            'ref_id',
            'status',
            'no_of_try',
            'created_at',
            'updated_at'
    ];

    public function getPDFGenerateList() {
        $fields = [
            'pdf_generator.id',
            'pdf_generator.pdf_type',
            'group_payment.tracking_no',
            'pdf_generator.status',
            'pdf_generator.no_of_try',
            'pdf_generator.created_at',
            'pdf_generator.updated_at'
        ];

        $users_type = Auth::user()->user_type;
        $result = PdfGenerator::leftJoin('group_payment', 'group_payment.id', '=', 'pdf_generator.ref_id')
            ->orderBy('id','desc')
            ->take(100)
            ->where('pdf_type','=',2)
            ->orWhere('pdf_type','=',4)
            ->get($fields);
        return $result;
    }

    public function getSearchList($search_text) {
        $fields = [
            'pdf_generator.id',
            'pdf_generator.pdf_type',
            'group_payment.tracking_no',
            'pdf_generator.status',
            'pdf_generator.no_of_try',
            'pdf_generator.created_at',
            'pdf_generator.updated_at'
        ];
        $result = PdfGenerator::leftJoin('group_payment', 'group_payment.id', '=', 'pdf_generator.ref_id')
            ->orderBy('id','desc')
            ->where('tracking_no', '=', $search_text)
            ->get($fields);
        return $result;
    }

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
            $post->created_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }




    /*     * ********************** Model Class ends here *************************** */
}
