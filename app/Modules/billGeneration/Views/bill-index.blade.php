@extends('layouts.admin')

@section('content')

@include('partials.messages')

<?php
$accessMode = ACL::getAccsessRight('billGen');
if (!ACL::isAllowed($accessMode, 'A')) {
    abort('400', 'You have no right to access! This incidence will be reported! Please contact for system administration for more information');
}
?>

<div class="col-md-12 col-lg-12">
    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
    {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}
</div>
<div class="col-lg-12 col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-list"></i> <strong>Bill Generation</strong>
                </div>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="col-md-6">

                {!! Form::open(['url' => 'bill-generation','method'=>'post', 'id'=>'billForm']) !!}
                <div class="form-group col-md-12">
                    {!! Form::label('bill_month','Select Month :',['class'=>'col-md-5 required-star']) !!}
                    <div class="col-md-7 {{$errors->has('bill_month') ? 'has-error': ''}}">
                        <div class="datepicker input-group date">
                            {!! Form::text('bill_month','', ['class' => 'form-control  input-sm required', 'placeholder' => 'mm-yyyy']) !!}
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12 {{$errors->has('bill_for') ? 'has-error' : ''}}">
                    {!! Form::label('bill_for','Bill For:',['class'=>'col-md-5 required-star']) !!}
                    <div class="col-md-7">
                        {!! Form::select('bill_for', [1=>'Project Clearance', 2=>'Visa Assistance', 3=>'Others'], '', 
                        ['class' => 'form-control input-sm required','placeholder'=>'Select One']) !!}
                        {!! $errors->first('bill_for','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group col-md-12">
                    {!! Form::submit('Submit',['class'=>'btn btn-success col-md-4 col-md-offset-8 input-sm']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div><!-- /.panel-body -->
    </div><!-- /.panel -->
</div><!-- /.col-lg-12 -->

@endsection

@section('footer-script')

@include('partials.datatable-scripts')
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            viewMode: 'years',
            format: 'MMM-YYYY',
            maxDate: (new Date()),
            minDate: '01/01/1992'
        });

        $("#billForm").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });

    $(function () {
        $('#list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 50
        });
    });
</script>
@endsection
