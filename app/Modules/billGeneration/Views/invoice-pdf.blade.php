@extends('layouts.pdfGen')
@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="assets/images/beza_watermark.png" alt="BEZA" width="80px"/>
            <h5>BEZA::Bangladesh Economic Zone Authority</h5>
            <h5><b>INVOICE</b></h5>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
            <div class="panel panel-red">
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Tracking no. : </strong><span style="font-size: 10px;">
                                    {{ (!empty($billInfo->tracking_number)?$billInfo->tracking_number:'') }}</span></td>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Bill Date : </strong> <span style="font-size: 10px;">
                                    {{ (!empty($billInfo->bill_generation_date)?$billInfo->bill_generation_date:'') }}</span></td>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Invoice No. : </strong> <span style="font-size: 10px;">
                                    {{ (!empty($billInfo->invoice_no)?$billInfo->invoice_no:'') }}</span></td>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">PIN : </strong> <span style="font-size: 10px;">
                                    {{ (!empty($billInfo->secret_key)?$billInfo->secret_key:'') }}</span></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading margin-for-preview"><strong>1. Applicant Information</strong></div>
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Applying firm or Company :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">
                                    {{ (!empty($billInfo->proposed_name)?$billInfo->proposed_name:'') }}</span></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                        </tr>
                    </table>

                    <table width="100%">
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Full Address of Registered Head Office of Applicant / 
                                    Applying Firm or Company :</strong></td>
                        </tr>
                    </table>

                    <table width="100%">
                        <tr>
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Country :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">
                                    {{ (!empty($countries[$billInfo->country])?$countries[$billInfo->country]:'') }}</span></td>
                            @if($billInfo->country !== 'BD')
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">State :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->state)?$billInfo->state:'') }}</span></td>
                            @else
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Division :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">
                                    {{ (!empty($divisions[$billInfo->division])?$divisions[$billInfo->division]:'') }}</span></td>
                            @endif

                        </tr>
                        <tr>
                            @if($billInfo->country !== 'BD')
                            <td style="padding:5px;"><strong style="font-size:10px;">Province :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->province)?$billInfo->province:'') }}</span></td>
                            @else
                            <td style="padding:5px;"><strong style="font-size:10px;">District :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">
                                    {{ (!empty($districts[$billInfo->district])?$districts[$billInfo->district]:'') }}</span></td>
                            @endif
                            <td style="padding:5px;"><strong style="font-size:10px;">Address Line 1 :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->road_no)?$billInfo->road_no:'') }}</span></td>
                        </tr>

                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Address Line 2 :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->house_no)?$billInfo->house_no:'') }}</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Post Code :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->post_code)?$billInfo->post_code:'') }}</span></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Phone No : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->phone)?$billInfo->phone:'') }}</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Fax No : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->fax)?$billInfo->fax:'') }}</span></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Email : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->email)?$billInfo->email:'') }}</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Website : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($billInfo->website)?$billInfo->website:'') }}</span></td>
                        </tr>
                    </table>
                </div>

                <div class="panel panel-green">
                    <div class="panel-body">
                        <table width="100%">
                            <tr>
                                <td class="text-center"><strong>Last Payment Date : </strong><span>
                                        {{ (!empty($billInfo->bill_expired_date)?$billInfo->bill_expired_date:'') }}</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table width="100%">
                    <tr>
                        <td style="padding: 5px;" class="text-center"><strong>Biling Month : </strong> 
                            {{ (!empty($billInfo->bill_month)? date('F , Y',strtotime($billInfo->bill_month)):'') }}</td>
                        <td style="padding: 5px;" class="text-center"><strong>Total Availed Services : </strong>
                            {{ (!empty($totalAvailedServices)?$totalAvailedServices:'') }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-stripped">
                <tr>
                    <th class="text-center" width="25%" style="padding: 5px;">Service Description</th>
                    <th class="text-center" style="padding: 5px;">Amt.Per Serve</th>
                    <th class="text-center" style="padding: 5px;">Service Details</th>
                    <th class="text-center" style="padding: 5px;">QTY</th>
                    <th class="text-center" style="padding: 5px;">Total</th>
                </tr>
                <?php $grandTotal = 0; ?>
                @foreach($billDetails as $data)
                <tr>
                    <td style="padding: 5px;">{{ $data->service_name }}</td>
                    <td width="20%" class="text-right" style="padding: 5px;">{{ $data->per_amount }}</td>
                    <td style="padding: 5px;"></td>
                    <td width="10%" class="text-right" style="padding: 5px;">{{ $data->service_quantity }}</td>
                    @if($data->service_quantity>0)

                    <td rowspan="2" style="padding: 5px; text-align: right;">{{ $data->total_amount }}</td>
                </tr>
                <tr>
                    <td style="text-align: right" colspan="4">
                        <span>{{ implode(', ',$serviceWiseTrackingNumbers[$data->service_id]) }}</span>
                    </td>
                </tr>
                @else
                <td style="padding: 5px; text-align: right;">{{ $data->total_amount }}</td>
                </tr>
                @endif
                @endforeach
                <tr>
                    <td style="padding: 5px; " colspan="4" class="text-left">
                        <strong>Sub Total : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><strong>{{ number_format($billInfo->sub_total,2) }}</strong></td>
                </tr>
                <tr class="alert alert-success">
                    <td style="padding: 5px; background-color: yellowgreen;" colspan="5">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px;" colspan="4" class="text-left">
                        <strong>Arrears : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><span>{{ number_format($billInfo->last_month_arrears,2) }}</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px; " colspan="4" class="text-left">
                        <strong>Others Charge : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><span>{{ number_format($billInfo->other_charges,2) }}</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px; " colspan="4" class="text-left">
                        <strong>TAX & VAT : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><span>{{ number_format($billInfo->tax_vat,2) }}</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px; background-color: yellowgreen" colspan="5"></td>
                </tr>
                <tr>
                    <td style="padding: 5px; " colspan="4" class="text-left">
                        <strong>Total : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><strong>{{ number_format($billInfo->amount,2) }}</strong></td>
                </tr>
                <tr>
                    <td style="padding: 5px; background: yellowgreen" colspan="5"></td>
                </tr>
                <tr>
                    <td style="padding: 5px; " colspan="4" class="text-left">
                        <strong>Late Payment Fee : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><span>{{ number_format($billInfo->late_payment_fee,2) }}</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px; " colspan="4" class="text-left">
                        <strong>Net Payable Amount : </strong>
                    </td>
                    <td class="text-right" style="padding: 5px"><strong>{{ number_format($billInfo->net_payable_amount,2) }}</strong></td>
                </tr>
            </table>

            <span style="font-size:12px;">
                <strong>Total Amount in Words : </strong> {{ (isset($amountWord)? ucfirst($amountWord) : 
                            '.......................................................................................................................................................................') }}
            </span>

            <table width="100%">
                <tr><td><br/></td></tr>
                <tr>
                    <td class="text-center">
                        <br/>
                        <img src="{{ $barcode_url }}" alt="Barcode">
                    </td>
                </tr>
                <tr><td><br/></td></tr>
                <tr>
                    <td  class="text-center">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                THIS IS A COMPUTER GENERATED INVOICE/MONEY RECEIPT. SO NO SIGNATURE IS REQUIRED.
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</section>
@endsection <!--- footer-script--->

