@extends('layouts.pdfGen')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="assets/images/beza_watermark.png" alt="BEZA" width="80px"/>
            <h5>BEZA::Bangladesh Economic Zone Authority</h5>
            <h5><b>MONEY RECEIPT</b></h5>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-md-12">
            <div class="panel panel-red">
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Tracking no. : </strong><span style="font-size: 10px;">TEXT</span></td>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Payment Date : </strong> <span style="font-size: 10px;">TEXT</span></td>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">SL No. : </strong> <span style="font-size: 10px;">TEXT</span></td>
                            <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Invoice No. : </strong> <span style="font-size: 10px;">TEXT</span></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading margin-for-preview"><strong>1. Applicant Information</strong></div>
                <div class="panel-body">
                    <table width="100%">
                        <tr>
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Applying firm or Company :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                        </tr>
                    </table>

                    <table width="100%">
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Full Address of Registered Head Office of Applicant / Applying Firm or Company :</strong></td>
                        </tr>
                    </table>

                    <table width="100%">
                        <tr>
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Country :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                            <td width="25%" style="padding:5px;"><strong style="font-size:10px;">Division :</strong> </td>
                            <td width="25%" style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">District :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Address Line 1 :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                        </tr>

                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Address Line 2 :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Post Code :</strong> </td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Phone No : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Fax No : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><strong style="font-size:10px;">Email : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                            <td style="padding:5px;"><strong style="font-size:10px;">Website : </strong></td>
                            <td style="padding:5px;"><span style="font-size:10px;">TEXT</span></td>
                        </tr>
                    </table>
                </div>

                <div class="panel panel-green">
                    <div class="panel-body">
                        <table width="100%">
                            <tr>
                                <td><strong>Payment Type : </strong><span>SKY Pay/ Local Bank</span></td>
                                <td><strong>Bank : </strong><span>EBL</span></td>
                                <td><strong>Branch : </strong><span>Banani</span></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-center">
                                    <br/>
                                    <img src="{{ $barcode_url }}" alt="Barcode">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th width="25%" style="padding: 5px;">Service Description</th>
                    <th style="padding: 5px;">Amt.Per Serve</th>
                    <th style="padding: 5px;">Service Details</th>
                    <th style="padding: 5px;">QTY</th>
                    <th style="padding: 5px;">Total</th>
                </tr>
                <tr>
                    <td style="padding: 5px;">Project Clearance</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">Import Permit</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">Export Permit</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">Visa Recommendation</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;" colspan="4" class="text-right">
                        <strong>Total : </strong>
                    </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px;" colspan="4" class="text-right">
                        <strong>Arrears : </strong>
                    </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
                <tr class="alert alert-success">
                    <td style="padding: 5px; background-color: yellowgreen;" colspan="5">
                    </td>
                </tr>
                <tr>
                   <td colspan="1" rowspan="7" class="text-center">
                       <img src="{{ $url }}" alt="QR Code">
                   </td>
                   <td colspan="3" class="text-right">
                       <strong>Late Fee: </strong>
                   </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">
                        <strong>Late Payment Fee : </strong>
                    </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">
                        <strong>Others Charge : </strong>
                    </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px; background-color: yellowgreen" colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">
                        <strong>VAT : </strong>
                    </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
                <tr>
                    <td style="padding: 5px; background: yellowgreen" colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">
                        <strong>Net Payable Amount : </strong>
                    </td>
                    <td style="padding: 5px"><span>XXXXX</span></td>
                </tr>
            </table>
            Total Amount in Words : ......................................................................................................................................
            <table width="100%">
                <tr>
                    <td class="text-center">
                        <br/><br/>________________________<br/>
                        <span>Authorized Signature</span>
                    </td>
                    <td class="text-center">
                        <br/><br/>________________________<br/>
                        <span>Authorized Signature</span>
                    </td>
                    <td class="text-center">
                        <br/><br/>________________________<br/>
                        <span>Authorized Signature</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>
@endsection <!--- footer-script--->

