@extends('layouts.pdfGen')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>
        </div>
    </div>

    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            <table width="100%" style="font-size:10px;">
                <tr>
                    <td width="20%"><img src="assets/images/demo_qrcode.png" alt="BEZA" width="80px"/></td>
                    <td width="60%" style="text-align: center">
                        <h5>Payment Voucher</h5><br/>
                        Voucher No : 0101123456<br/>
                        Pin Number : 123456<br/>
                        Date : 18 / 01 / 2017
                    </td>
                    <td width="20%"><img src="assets/images/demo_barcode.png" alt="BEZA" width="150px"/></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped" style="margin-top: 50px;">
                <tr>
                    <th style="padding: 5px;" width="10%">#SL No</th>
                    <th style="padding: 5px;" width="30%">Service</th>
                    <th style="padding: 5px;" width="10%">Qty</th>
                    <th style="padding: 5px;" width="25%">Fee</th>
                    <th style="padding: 5px;" width="25%">Amount</th>
                </tr>
                <tr>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">Project Clearance</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">2</td>
                    <td style="padding: 5px;">Export Permit</td>
                    <td style="padding: 5px;">2</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">1000</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">3</td>
                    <td style="padding: 5px;">Import Permit</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">4</td>
                    <td style="padding: 5px;">Visa Recommendation</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td style="padding: 5px;">5</td>
                    <td style="padding: 5px;">Work Permit</td>
                    <td style="padding: 5px;">1</td>
                    <td style="padding: 5px;">500</td>
                    <td style="padding: 5px;">500</td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 5px; text-align: right;"><b>Total</b></td>
                    <td style="padding: 5px;"><b>3000</b></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 5px; text-align: right;"><b>Late Fee</b></td>
                    <td style="padding: 5px;"><b>500</b></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 5px; text-align: right;"><b>Grand Total</b></td>
                    <td style="padding: 5px;"><b>3500</b></td>
                </tr>
            </table>
        </div>
    </div>
</section>
@endsection <!--- footer-script--->

