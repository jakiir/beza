@extends('layouts.admin')

@section('content')

@include('partials.messages')

<?php
$accessMode = ACL::getAccsessRight('billGen');
if (!ACL::isAllowed($accessMode, 'V')) {
    abort('400', 'You have no right to access! This incidence will be reported! Please contact for system administration for more information');
}
?>

<div class="col-lg-12 col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-list"></i> <strong>List of all Bills</strong>
                </div>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Invoice No</th>
                            <th>Bill Type</th>
                            <th>Bill Month</th>
                            <th>Payment Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
    </div><!-- /.panel -->
</div><!-- /.col-lg-12 -->

@endsection

@section('footer-script')

@include('partials.datatable-scripts')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<script>
    $(function () {
    var rlt = $('#list').DataTable({
        processing: true,
        serverSide: true,
        "paging": true,
        "lengthChange": true,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "iDisplayLength": 50,
        ajax: {
            url: '{{url("bill-generation/get-bills-by-type")}}',
            method: 'post',
            data: function (d) {
                d._token = $('input[name="_token"]').val();
            }
        },
        columns: [
            {data: 'rownum', name: 'rownum'},
            {data: 'invoice_no', name: 'invoice_no'},
            {data: 'bill_type', name: 'bill_type'},
            {data: 'bill_month', name: 'bill_month'},
            {data: 'status_name', name: 'status_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "aaSorting": []
    });
        
    }); // end of function
</script>
@endsection
