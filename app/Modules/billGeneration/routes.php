<?php

Route::group(array('module' => 'billGeneration', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'],
    'namespace' => 'App\Modules\billGeneration\Controllers'), function() {

    // List
    Route::get('bill-generation/bill-list', 'billGenerationController@billLists');
    Route::post('bill-generation/get-bills-by-type/', 'billGenerationController@getBillsByType');

    // Insertion
    Route::get('bill-generation', 'billGenerationController@index');
    Route::post('bill-generation', 'billGenerationController@billInfoGeneration');
    Route::get('bill-generation/others', 'billGenerationController@billGenerationForOthers');

    // Search
    Route::post('bill-generation/search', 'billGenerationController@search');

    // PDF generation
    Route::get('bill-generation/invoice', 'billGenerationController@invoicePDF');
    Route::get('bill-generation/bill-pdf/{id}', 'billGenerationController@billPDF');

//  Route::resource('bill-gen', 'billGenerationController');

    /*     * ****************************End of Route Group********************************** */
});
