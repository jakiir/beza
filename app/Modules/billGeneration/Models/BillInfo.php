<?php

namespace App\Modules\billGeneration\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BillInfo extends Model {

    protected $table = 'bill_info';
    protected $fillable = [
        'id',
        'invoice_no',
        'bill_type',
        'bill_month',
        'project_clearance_id',
        'qrcode_barcode_text',
        'payment_status_id',
        'beza_account_id',
        'bat_account_id',
        'receiving_branch_id',
        'ref_no',
        'amount',
        'sub_total',
        'arrears_bill_id',
        'last_month_arrears',
        'late_payment_fee',
        'other_charges',
        'tax_vat',
        'net_payable_amount',
        'bill_generation_date',
        'bill_expired_date',
        'depositor_name',
        'depositor_address',
        'depositor_mobile',
        'received_by',
        'received_at',
        'rejected_by',
        'rejected_at',
        'confirmed_by',
        'confirmed_at',
        'is_locked',
        'is_archived',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->created_by = Auth::user()->id;
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *******************************End of Models****************************** */
}
