<?php

namespace App\Modules\billGeneration\Models;

use App\Modules\billGeneration\Models\BillInfo;
use App\Modules\Settings\Models\BankBranch;
use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BillInfoDetails extends Model {

    protected $table = 'bill_info_details';
    protected $fillable = [
        'id',
        'bill_id',
        'is_arrears_of',
        'service_id',
        'service_quantity',
        'per_amount',
        'total_amount',
        'bat_amount',
        'is_archived',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->created_by = Auth::user()->id;
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    public function getBillsByPaymentStatus($payment_status) {
        $userBranch = BankBranch::where('id', Auth::user()->bank_branch_id)->first();
        
        DB::statement(DB::raw('set @rownum=0'));
        $data = BillInfo::leftJoin('bill_payment_details', 'bill_info.id', '=', 'bill_payment_details.bill_id')
                ->leftJoin('payment_status', 'bill_info.payment_status_id', '=', 'payment_status.id')
                ->leftjoin('pdf_queue as pq', function($join) {
                    $pdfType = 'beza.money-receipt.' . env('server_type'); // to fetch the mooney receipts
                    $join->on('pq.app_id', '=', 'bill_info.id');
                    $join->on('pq.service_id', '=', DB::raw('0'));
                    $join->where('pq.pdf_type','=', $pdfType);
                })
                ->where('bill_info.receiving_branch_id', '=', $userBranch->id)
                ->where('bill_info.payment_status_id', $payment_status)
                ->groupBY('bill_info.id')
                ->orderBy('rownum', 'asc')
                ->get(['bill_info.id', 'bill_info.invoice_no', 'bill_info.bill_type', 'bill_info.payment_status_id','bill_info.bill_month',
                    'payment_status.status_name as payment_status', 'pq.attachment',
            DB::raw('count(bill_payment_details.service_id) as no_of_services'), DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
        return $data;
    }

    /*     * *******************************End of Models****************************** */
}
