<?php

namespace App\Modules\billGeneration\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\apps\Models\pdfQueue;
use App\Modules\billGeneration\Models\BillInfo;
use App\Modules\billGeneration\Models\BillInfoDetails;
use App\Modules\Dashboard\Models\Services;
use App\Modules\Settings\Models\Configuration;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\workPermit\Models\Processlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Milon\Barcode\DNS1D;
use mPDF;
use yajra\Datatables\Datatables;

class billGenerationController extends Controller {

    protected $pc_service_id;
    protected $va_service_id;
    protected $vr_service_id;
    protected $wp_service_id;
    protected $ep_service_id;
    protected $ip_service_id;

    public function __construct() {
        $this->pc_service_id = 1;
        $this->va_service_id = 2;
        $this->vr_service_id = 3;
        $this->wp_service_id = 4;
        $this->ep_service_id = 5;
        $this->ip_service_id = 6;
    }

    public function index() {
        if (!ACL::getAccsessRight('billGen', 'A')) {
            abort('400', 'You have no access right! This incidence will be reported. Contact with system admin for more information.');
        }
        return view('billGeneration::bill-index');
    }

    //  Bill lists
    public function billLists() {
        if (!ACL::getAccsessRight('billGen', 'V')) {
            abort('400', 'You have no access right! Please contact with system admin for more information.');
        }
        try {
            return view('billGeneration::bill-list');
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5020].');
            return Redirect::back()->withInput();
        }
    }

    // For showing bills to specific Bank Branch Users
    public function getBillsByType() {
        $mode = ACL::getAccsessRight('billGen', 'V');

        $userType = CommonFunction::getUserType();
        $root_app_id = CommonFunction::getRootAppId();

        DB::statement(DB::raw('set @rownum=0'));
        if ($userType == '5x505') {
            $list = BillInfo::whereIn('bill_info.bill_type', [1, 3]) // 1 = PC, 3 = Others [ip,ep,vr,wp]
                    ->where('project_clearance_id', $root_app_id);
        } else if ($userType == '6x606') {
            $list = BillInfo::whereIn('bill_info.bill_type', [2]) // 2 = VA
                    ->where('project_clearance_id', $root_app_id); // for type 2, pc_id is va_id
        } else { // for system admin
            $list = BillInfo::whereIn('bill_info.bill_type', [1, 2, 3]); // 1 = PC, 2 = VA, 3 = Others [ip,ep,vr,wp]
        }
        $getList = $list->leftJoin('payment_status', 'payment_status.id','=','payment_status_id')
                ->leftjoin('pdf_queue as pq', function($join) {
                    $pdfType = 'beza.money-receipt.' . env('server_type');
                    $join->on('pq.app_id', '=', 'bill_info.id');
                    $join->on('pq.service_id', '=', DB::raw('0'));
                    $join->where('pq.pdf_type', '=', $pdfType);
                })
                ->leftjoin('pdf_queue as pq2', function($join) {
                    $pdfType2 = 'beza.bill-generation.' . env('server_type');
                    $join->on('pq2.app_id', '=', 'bill_info.id');
                    $join->on('pq2.service_id', '=', DB::raw('0'));
                    $join->where('pq2.pdf_type', '=', $pdfType2);
                })
                ->orderBy('rownum', 'asc')
                ->get([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                    'bill_info.id', 'bill_info.bill_type', 'bill_info.bill_month',
                    'bill_info.payment_status_id', 'bill_info.invoice_no',
                    'pq.attachment as moneyReceipt', 'pq2.attachment as invoice',
                    'payment_status.status_name'
                    ]);

        return Datatables::of($getList)
                        ->addColumn('action', function ($getList) use ($mode) {
                            if ($mode) {
                                if (isset($getList->payment_status_id) && $getList->payment_status_id == '12') { // 12 = paid
                                    $moneyReceipt = '<a target="_blank" href="' . url($getList->moneyReceipt) .
                                            '" class="btn btn-xs btn-danger"><i class="fa fa-file-pdf-o"></i> Money Receipt</a>';
                                } else {
                                    $moneyReceipt = '';
                                }
                                return '<a target="_blank" href="' . url($getList->invoice) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Bill Invoice</a>' .
                                        ' ' . $moneyReceipt;
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('bill_type', function ($getList) {
                            if ($getList->bill_type == 1) {
                                $billType = 'Project Clearance';
                            } elseif ($getList->bill_type == 2) {
                                $billType = 'Visa Assistance';
                            } elseif ($getList->bill_type == 3) {
                                $billType = 'Others';
                            }
                            return $billType;
                        })
                        ->editColumn('bill_month', function ($getList) {
                            return isset($getList->bill_month) ? date('M-Y', strtotime($getList->bill_month)) : '';
                        })
                        ->editColumn('payment_status_id', function ($getList) {
                            $status = isset($getList->payment_status_id) ? $getList->payment_status_id : 0;
                            switch ($status) {
                                case 0 :
                                    $pay_stat = 'Unpaid';
                                    $class = 'text-warning';
                                    break;
                                case 2 :
                                    $pay_stat = 'Rejected';
                                    $class = 'text-danger';
                                    break;
                                case 11 :
                                    $pay_stat = 'Pending';
                                    $class = 'text-primary';
                                    break;
                                case 12 :
                                    $pay_stat = 'Approved';
                                    $class = 'text-success';
                                    break;
                                default :
                                    $pay_stat = 'Unpaid';
                                    $class = 'text-warning';
                            }
                            return '<span class="' . $class . '">' . $pay_stat . '</span>';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function billInfoGeneration(Request $request) {
        if (!ACL::getAccsessRight('billGen', 'V')) {
            abort('400', 'You have no access right! Please contact with system admin for more information.');
        }
        $this->validate($request, [
            'bill_month' => 'required',
            'bill_for' => 'required',
        ]);
        $bill_month = date('Y-m', strtotime($request->get('bill_month')));
        $bill_for = $request->get('bill_for');

        switch ($bill_for) {
            case 1 :
                $this->billGenerationForPC($bill_month);
                break;
            case 2 :
                $this->billGenerationForVA($bill_month);
                break;
            case 3 :
                $this->billGenerationForOthers($bill_month);
                break;
            default:
                return redirect('/bill-generation');
        }
    }

    public function billGenerationForPC($bill_month = '') {
        if (!ACL::getAccsessRight('billGen', 'A')) {
            abort('400', 'You have no access right! Please contact with system admin for more information.');
        }
        try {
            $completedPCsforMonth = Processlist::leftJoin('project_clearance as pc', 'process_list.record_id', '=', 'pc.id')
                    ->whereIn('process_list.status_id', [23, 40])
                    ->where('pc.bill_month', $bill_month)
                    ->where('process_list.service_id', $this->pc_service_id)
                    ->get(['process_list.record_id as pc_id', 'pc.created_by']);

            // to get the account related information from account table through configuration
            $config_account = Configuration::where('caption', 'PAYMENT_ACCOUNT')->first(['value', 'value2']);
            // To get late payment fee, other charges, tax or vat
            $billExtraFeeData = Configuration::where('caption', 'EXTRA_FEE')->first(['value', 'value2', 'value3']);

            $serviceInfo = Services::where('id', $this->pc_service_id)->where('is_active', 1)
                    ->first(['id', 'per_certificate_fee as pc_certificate_fee', 'bat_fee']);
            $noOfPC = count($completedPCsforMonth);

            $billIds = array();

            if ($noOfPC > 0) {
                DB::beginTransaction();
                foreach ($completedPCsforMonth as $pcInfo) {
                    $concateDate = $bill_month . '-01';
                    $billGenerationDate = date("Y-m-t", strtotime($concateDate));
                    $billExpiredDate = date("Y-m-10", strtotime("+1 month", strtotime($concateDate)));
                    $last_month = date("Y-m", strtotime("-1 month", strtotime($bill_month)));

                    $lastMonthBillInfos = BillInfo::where('bill_month', $last_month)
                            ->where('bill_type', 1) // Bill type 1 is Project Clearance
                            ->where('project_clearance_id', $pcInfo->pc_id)
                            ->whereIn('payment_status_id', [0, 2])
                            ->first(['id', 'net_payable_amount']);
                    $lastMonthBill = 0;
                    if ($lastMonthBillInfos) {
                        $lastMonthBill = ($lastMonthBillInfos->net_payable_amount > 0) ? $lastMonthBillInfos->net_payable_amount : 0;
                    }

                    $pc_amount = $serviceInfo->pc_certificate_fee;

//                  $total_amount = $pc_amount + $ip_amount + $ep_amount + $vr_amount + $wp_amount;
                    $sub_total = $pc_amount;


                    $late_fee_percentage = ($sub_total * $billExtraFeeData->value) / 100; //value = late fee percentage = 5%
                    $other_charge = $billExtraFeeData->value2; // value2 = other charge fixed amount = 400
                    $taxVat = ($sub_total * $billExtraFeeData->value3) / 100; // value3=vat or tax in percentage = 8%
                    $total_amount = $sub_total + $lastMonthBill + $other_charge + $taxVat;

                    $netPayableAmount = $total_amount + $late_fee_percentage ;

                    $billInfo = BillInfo::firstOrNew(['project_clearance_id' => $pcInfo->pc_id, 'bill_type' => 1, 'bill_month' => $bill_month]);
                    $billInfo->bill_type = 1;
                    $billInfo->bill_month = $bill_month;
                    $billInfo->project_clearance_id = $pcInfo->pc_id;
                    $billInfo->payment_status_id = 0; // 0 Unpaid
                    $billInfo->beza_account_id = $config_account->value; // BEZA Account ID is 1
                    $billInfo->bat_account_id = $config_account->value2; // BAT Account ID is 2
                    $billInfo->amount = $total_amount;
                    $billInfo->sub_total = $sub_total;
                    if ($lastMonthBill > 0) {
                        $billInfo->arrears_bill_id = $lastMonthBillInfos->id;
                    }
                    $billInfo->last_month_arrears = $lastMonthBill;
                    $billInfo->late_payment_fee = $late_fee_percentage;
                    $billInfo->other_charges = $other_charge;
                    $billInfo->tax_vat = $taxVat;
                    $billInfo->net_payable_amount = $netPayableAmount;
                    $billInfo->bill_generation_date = $billGenerationDate;
//                  $billInfo->bill_expired_date = date('Y-m-d', strtotime('+15 days'));
                    $billInfo->bill_expired_date = $billExpiredDate;  // Bill date next month 10 days
                    $billInfo->save();
                    $bill_id = $billInfo->id;
                    $billIds[] = $bill_id;
                    $billInfo->invoice_no = $billTrackingNo = CommonFunction::generateTrackingID('B', $bill_id);
                    $billInfo->save();

                    $pdfType = 'beza.bill-generation.' . env('server_type');
                    $pdfQueue = pdfQueue::firstOrNew(['app_id' => $bill_id, 'service_id' => 0, 'pdf_type' => $pdfType]);
                    $pdfQueue->pdf_type = $pdfType;
                    $pdfQueue->app_id = $bill_id;
                    $pdfQueue->service_id = 0; // For Bill generation, Service ID is 0
                    $pdfQueue->secret_key = rand(0, 999999);
                    $pdfQueue->status = 1;
                    $pdfQueue->save();

                    $this->billDetailsInsert($bill_id, $pcInfo->pc_id, $this->pc_service_id, 1, $serviceInfo->pc_certificate_fee, $serviceInfo->bat_fee);
                    if ($billInfo->arrears_bill_id > 0) {
                        $this->arrearsbillDetailsUpdate($billInfo->arrears_bill_id, $bill_id);
                    }
//                  $this->invoicePDF($bill_id);
                } // end of foreach ($completedPC as $pcInfo)

                DB::commit();
            }
            if (count($billIds) > 0)
                $this->invoicePDF($billIds);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5004].');
            return Redirect::back()->withInput();
        }
    }

    public function billGenerationForVA($bill_month = '') {
        if (!ACL::getAccsessRight('billGen', 'A')) {
            abort('400', 'You have no access right! Please contact with system admin for more information.');
        }
        try {
            $completedVAsforMonth = Processlist::leftJoin('visa_assistance as va', 'process_list.record_id', '=', 'va.id')
                    ->whereIn('process_list.status_id', [23, 40])
                    ->where('va.bill_month', $bill_month)
                    ->where('process_list.service_id', $this->va_service_id)
                    ->get(['process_list.record_id as va_id', 'va.created_by']);

            // to get the account related information from account table through configuration
            $config_account = Configuration::where('caption', 'PAYMENT_ACCOUNT')->first(['value', 'value2']);
            // To get late payment fee, other charges, tax or vat
            $billExtraFeeData = Configuration::where('caption', 'EXTRA_FEE')->first(['value', 'value2', 'value3']);

            $serviceInfo = Services::where('id', $this->va_service_id)->where('is_active', 1)->first(['id', 'per_certificate_fee as va_certificate_fee', 'bat_fee']);
            $noOfVA = count($completedVAsforMonth);

            $billIds = array();

            if ($noOfVA > 0) {
                DB::beginTransaction();
                foreach ($completedVAsforMonth as $vaInfo) {
                    $concateDate = $bill_month . '-01';
                    $billGenerationDate = date("Y-m-t", strtotime($concateDate));
                    $billExpiredDate = date("Y-m-10", strtotime("+1 month", strtotime($concateDate)));
                    $last_month = date("Y-m", strtotime("-1 month", strtotime($bill_month)));

                    $lastMonthBillInfos = BillInfo::where('bill_month', $last_month)
                            ->where('bill_type', 2) // Bill type 2 is Visa Assistance
                            ->where('project_clearance_id', $vaInfo->va_id)
                            ->whereIn('payment_status_id', [0, 2])
                            ->first(['id', 'net_payable_amount']);

                    $lastMonthBill = 0;
                    if ($lastMonthBillInfos) {
                        $lastMonthBill = ($lastMonthBillInfos->net_payable_amount > 0) ? $lastMonthBillInfos->net_payable_amount : 0;
                    }

                    $sub_total = $serviceInfo->va_certificate_fee;

                    $late_fee_percentage = ($sub_total * $billExtraFeeData->value) / 100;
                    $other_charge = $billExtraFeeData->value2;
                    $taxVat = ($sub_total * $billExtraFeeData->value3) / 100;
                    $total_amount = $sub_total + $lastMonthBill + $other_charge + $taxVat;
                    $netPayableAmount = $total_amount + $late_fee_percentage;

                    $billInfo = BillInfo::firstOrNew(['project_clearance_id' => $vaInfo->va_id, 'bill_type' => 2, 'bill_month' => $bill_month]);
                    $billInfo->bill_type = 2;
                    $billInfo->bill_month = $bill_month;
                    $billInfo->project_clearance_id = $vaInfo->va_id;
                    $billInfo->payment_status_id = 0; // 0 Unpaid
                    $billInfo->beza_account_id = $config_account->value; // BEZA Account ID is 1
                    $billInfo->bat_account_id = $config_account->value2; // BAT Account ID is 2
                    $billInfo->amount = $total_amount;
                    $billInfo->sub_total = $sub_total;
                    if ($lastMonthBill > 0)
                        $billInfo->arrears_bill_id = $lastMonthBillInfos->id;

                    $billInfo->last_month_arrears = $lastMonthBill;
                    $billInfo->late_payment_fee = $late_fee_percentage;
                    $billInfo->other_charges = $other_charge;
                    $billInfo->tax_vat = $taxVat;
                    $billInfo->net_payable_amount = $netPayableAmount;
                    $billInfo->bill_generation_date = $billGenerationDate;
//                  $billInfo->bill_expired_date = date('Y-m-d', strtotime('+15 days'));
                    $billInfo->bill_expired_date = $billExpiredDate;  // Bill date next month 10 days
                    $billInfo->save();
                    $bill_id = $billInfo->id;
                    $billIds[] = $bill_id;
                    $billInfo->invoice_no = $billTrackingNo = CommonFunction::generateTrackingID('B', $bill_id);
                    $billInfo->save();

                    $pdfType = 'beza.bill-generation.' . env('server_type');
                    $pdfQueue = pdfQueue::firstOrNew(['app_id' => $bill_id, 'service_id' => 0, 'pdf_type' => $pdfType]);
                    $pdfQueue->pdf_type = $pdfType;
                    $pdfQueue->app_id = $bill_id;
                    $pdfQueue->service_id = 0; // For Bill generation, Service ID is 0
                    $pdfQueue->secret_key = rand(0, 999999);
                    $pdfQueue->status = 1;
                    $pdfQueue->save();

                    $this->billDetailsInsert($bill_id, $vaInfo->va_id, $this->va_service_id, 1, $serviceInfo->va_certificate_fee, $serviceInfo->bat_fee);
                    if ($billInfo->arrears_bill_id > 0) {
                        $this->arrearsbillDetailsUpdate($billInfo->arrears_bill_id, $bill_id);
                    }
//                  $this->invoicePDF($bill_id);
                } // end of foreach ($completedPC as $pcInfo)            
                DB::commit();
            }

            if (count($billIds) > 0)
                $this->invoicePDFforVA($billIds);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5005].');
            return Redirect::back()->withInput();
        }
    }

    public function billGenerationForOthers($bill_month = '') {
        if (!ACL::getAccsessRight('billGen', 'A')) {
            abort('400', 'You have no access right! Please contact with system admin for more information.');
        }
        try {
            $completedPC = Processlist::leftJoin('project_clearance as pc', 'process_list.record_id', '=', 'pc.id')
                    ->whereIn('process_list.status_id', [23, 40])
                    ->where('service_id', $this->pc_service_id)
                    ->get(['process_list.record_id as pc_id', 'pc.created_by']);

            // to get the account related information from account table through configuration
            $config_account = Configuration::where('caption', 'PAYMENT_ACCOUNT')->first(['value', 'value2']);
            // To get late payment fee, other charges, tax or vat
            $billExtraFeeData = Configuration::where('caption', 'EXTRA_FEE')->first(['value', 'value2', 'value3']);

            $serviceInfo = Services::where('is_active', 1)->get(['id', 'per_certificate_fee', 'bat_fee']);
            foreach ($serviceInfo as $service) {
                $serviceData[$service->id] ['per_certificate_fee'] = $service->per_certificate_fee;
                $serviceData[$service->id] ['bat_fee'] = $service->bat_fee;
            }

            $billIds = array();
            foreach ($completedPC as $pcInfo) {

                $ipDatas = Processlist::leftJoin('import_permit as ip', 'process_list.record_id', '=', 'ip.id')
                        ->where('process_list.service_id', $this->ip_service_id)
                        ->where('ip.created_by', $pcInfo->created_by)
                        ->where('ip.bill_month', $bill_month)
                        ->whereIn('process_list.status_id', [21, 29, 30, 31, 40])
                        ->get(['process_list.record_id as app_id']);

                $ipIds = '';
                foreach ($ipDatas as $value) {
                    $ipIds = $ipIds . $value->app_id . ',';
                }
                $ipIds = rtrim($ipIds, ","); // to eliminate the last comman

                $epDatas = Processlist::leftJoin('export_permit as ep', 'process_list.record_id', '=', 'ep.id')
                        ->where('process_list.service_id', $this->ep_service_id)
                        ->where('ep.created_by', $pcInfo->created_by)
                        ->where('ep.bill_month', $bill_month)
                        ->whereIn('process_list.status_id', [21, 29, 30, 31, 40])
                        ->get(['process_list.record_id as app_id']);

                $epIds = '';
                foreach ($epDatas as $value) {
                    $epIds = $epIds . $value->app_id . ',';
                }
                $epIds = rtrim($epIds, ","); // to eliminate the last comman

                $vrDatas = Processlist::leftJoin('visa_recommendation as vr', 'process_list.record_id', '=', 'vr.id')
                        ->where('process_list.service_id', $this->vr_service_id)
                        ->where('vr.created_by', $pcInfo->created_by)
                        ->where('vr.bill_month', $bill_month)
                        ->whereIn('process_list.status_id', [23, 40])
                        ->get(['process_list.record_id as app_id']);

                $vrIds = '';
                foreach ($vrDatas as $value) {
                    $vrIds = $vrIds . $value->app_id . ',';
                }
                $vrIds = rtrim($vrIds, ","); // to eliminate the last comman

                $wpDatas = Processlist::leftJoin('work_permit as wp', 'process_list.record_id', '=', 'wp.id')
                        ->where('process_list.service_id', $this->wp_service_id)
                        ->where('wp.created_by', $pcInfo->created_by)
                        ->where('wp.bill_month', $bill_month)
                        ->whereIn('process_list.status_id', [23, 40])
                        ->get(['process_list.record_id as app_id']);

                $wpIds = '';
                foreach ($wpDatas as $value) {
                    $wpIds = $wpIds . $value->app_id . ',';
                }
                $wpIds = rtrim($wpIds, ","); // to eliminate the last comman

                $no_of_ips = count($ipDatas);
                $no_of_eps = count($epDatas);
                $no_of_vrs = count($vrDatas);
                $no_of_wps = count($wpDatas);
                if (!($no_of_ips > 0 || $no_of_eps > 0 || $no_of_vrs > 0 || $no_of_wps > 0 )) {
                    continue;
                }

                $concateDate = $bill_month . '-01';
                $billGenerationDate = date("Y-m-t", strtotime($concateDate));
                $billExpiredDate = date("Y-m-10", strtotime("+1 month", strtotime($concateDate)));
                $last_month = date("Y-m", strtotime("-1 month", strtotime($bill_month)));

                $lastMonthBillInfos = BillInfo::where('bill_month', $last_month)
                        ->where('bill_type', 3) // Bill type 3 is for others
                        ->where('project_clearance_id', $pcInfo->pc_id)
                        ->whereIn('payment_status_id', [0, 2])
                        ->first(['id', 'net_payable_amount']);

                $lastMonthBill = 0;
                if ($lastMonthBillInfos) {
                    $lastMonthBill = ($lastMonthBillInfos->net_payable_amount > 0) ? $lastMonthBillInfos->net_payable_amount : 0;
                }

                // $pc_amount = $serviceData[$this->pc_service_id] ['per_certificate_fee'];
                $ip_amount = $serviceData[$this->ip_service_id] ['per_certificate_fee'] * $no_of_ips;
                $ep_amount = $serviceData[$this->ep_service_id] ['per_certificate_fee'] * $no_of_eps;
                $vr_amount = $serviceData[$this->vr_service_id] ['per_certificate_fee'] * $no_of_vrs;
                $wp_amount = $serviceData[$this->wp_service_id] ['per_certificate_fee'] * $no_of_wps;

//              $total_amount = $pc_amount + $ip_amount + $ep_amount + $vr_amount + $wp_amount;
                $sub_total = $ip_amount + $ep_amount + $vr_amount + $wp_amount;

                $late_fee_percentage = ($sub_total * $billExtraFeeData->value) / 100;
                $other_charge = $billExtraFeeData->value2;
                $taxVat = ($sub_total * $billExtraFeeData->value3) / 100;
//              $arrears
                $total_amount = $sub_total + $lastMonthBill + $other_charge + $taxVat;
                $netPayableAmount = $total_amount + $late_fee_percentage;

                DB::beginTransaction();
                $billInfo = BillInfo::firstOrNew(['project_clearance_id' => $pcInfo->pc_id, 'bill_type' => 3, 'bill_month' => $bill_month]);
                $billInfo->bill_type = 3;
                $billInfo->bill_month = $bill_month;
                $billInfo->project_clearance_id = $pcInfo->pc_id;
                $billInfo->payment_status_id = 0; // 0 Unpaid
                $billInfo->beza_account_id = $config_account->value; // BEZA Account ID is 1
                $billInfo->bat_account_id = $config_account->value2; // BAT Account ID is 2
                $billInfo->sub_total = $sub_total;
                $billInfo->amount = $total_amount;
                $billInfo->last_month_arrears = $lastMonthBill;
                if ($lastMonthBill > 0)
                    $billInfo->arrears_bill_id = $lastMonthBillInfos->id;

                $billInfo->late_payment_fee = $late_fee_percentage;
                $billInfo->other_charges = $other_charge;
                $billInfo->tax_vat = $taxVat;
                $billInfo->net_payable_amount = $netPayableAmount;
                $billInfo->bill_generation_date = $billGenerationDate;
//            $billInfo->bill_expired_date = date('Y-m-d', strtotime('+15 days'));
                $billInfo->bill_expired_date = $billExpiredDate;  // Bill date next month 10 days
                $billInfo->save();
                $bill_id = $billInfo->id;
                $billIds[] = $bill_id;
                $billInfo->invoice_no = $billTrackingNo = CommonFunction::generateTrackingID('B', $bill_id);
                $billInfo->save();

                $pdfType = 'beza.bill-generation.' . env('server_type');
                $pdfQueue = pdfQueue::firstOrNew(['app_id' => $bill_id, 'service_id' => 0, 'pdf_type' => $pdfType]);
                $pdfQueue->pdf_type = $pdfType;
                $pdfQueue->app_id = $bill_id;
                $pdfQueue->service_id = 0; // For Bill generation, Service ID is 0
                $pdfQueue->secret_key = rand(0, 999999);
                $pdfQueue->status = 1;
                $pdfQueue->save();

//              $this->billDetailsInsert($bill_id, $pcInfo->pc_id, $this->pc_service_id, 1, $serviceData[$this->pc_service_id]['per_certificate_fee'], $serviceData[$this->pc_service_id]['bat_fee']);
                $this->billDetailsInsert($bill_id, $ipIds, $this->ip_service_id, $no_of_ips, $serviceData[$this->ip_service_id]['per_certificate_fee'], $serviceData[$this->ip_service_id]['bat_fee']);
                $this->billDetailsInsert($bill_id, $epIds, $this->ep_service_id, $no_of_eps, $serviceData[$this->ep_service_id]['per_certificate_fee'], $serviceData[$this->ep_service_id]['bat_fee']);
                $this->billDetailsInsert($bill_id, $vrIds, $this->vr_service_id, $no_of_vrs, $serviceData[$this->vr_service_id]['per_certificate_fee'], $serviceData[$this->vr_service_id]['bat_fee']);
                $this->billDetailsInsert($bill_id, $wpIds, $this->wp_service_id, $no_of_wps, $serviceData[$this->wp_service_id]['per_certificate_fee'], $serviceData[$this->wp_service_id]['bat_fee']);
                if ($billInfo->arrears_bill_id > 0) {
                    $this->arrearsbillDetailsUpdate($billInfo->arrears_bill_id, $bill_id);
                }
            } // end of foreach ($completedPC as $pcInfo)
            if (count($billIds) > 0)
                $this->invoicePDF($billIds);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5006].');
            return Redirect::back()->withInput();
        }
    }

    public function billDetailsInsert($bill_id, $appIds, $service_id, $service_quantity, $per_amount, $bat_amount) {
        try {
            $billInfoDetails = BillInfoDetails::firstOrNew(['bill_id' => $bill_id, 'service_id' => $service_id]);
            $billInfoDetails->bill_id = $bill_id;
            $billInfoDetails->app_ids = $appIds;
            $billInfoDetails->service_id = $service_id;
            $billInfoDetails->service_quantity = $service_quantity;
            $billInfoDetails->per_amount = $per_amount;
            $billInfoDetails->total_amount = $per_amount * $service_quantity;
            $billInfoDetails->bat_amount = $bat_amount;
            $billInfoDetails->save();
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5007].');
            return Redirect::back()->withInput();
        }
    }

    public function arrearsbillDetailsUpdate($bill_id, $is_arrears_of) {

        // previous month bill payment status will be "Becames arrears"
        $updateArrearBillPaymentStatus = BillInfo::where('id', $bill_id)
            ->update(['payment_status_id' => -15]);

        //previous month services (in bill details) will be picked in current month bill
        $updateArrearsDetails = BillInfoDetails::where('bill_id', $bill_id)
                ->update(['is_arrears_of' => $is_arrears_of]);
    }

    public function invoicePDF($billIds = array()) {
        try {
            $billInfos = BillInfo::leftJoin('project_clearance as pc', 'bill_info.project_clearance_id', '=', 'pc.id')
                    ->leftjoin('pdf_queue as pq', function($join) {
                        $pdfType = 'beza.bill-generation.' . env('server_type');
                        $join->on('pq.app_id', '=', 'bill_info.id');
                        $join->on('pq.service_id', '=', DB::raw('0'));
                        $join->where('pq.pdf_type', '=', $pdfType);
                    })
                    ->whereIn('bill_info.id', $billIds)
                    ->get(['bill_info.*',
                'pc.tracking_number', 'pc.proposed_name', 'pc.country', 'pc.division', 'pc.district', 'pc.state', 'pc.province', 'pc.road_no', 'pc.house_no',
                'pc.post_code', 'pc.phone', 'pc.fax', 'pc.email', 'pc.website', 'pq.secret_key']);

            $countries = Countries::lists('name', 'iso');
            $divisions = AreaInfo::where('area_type', 1)->lists('area_nm', 'area_id')->all();
            $districts = AreaInfo::where('area_type', 2)->lists('area_nm', 'area_id')->all();

            foreach ($billInfos as $billInfo) {
                $billDetails = BillInfoDetails::leftJoin('service_info as si', 'si.id', '=', 'bill_info_details.service_id')
                        ->where('bill_info_details.bill_id', $billInfo->id)
                        ->orderBy('bill_info_details.service_id')
                        ->get(['si.name as service_name', 'bill_info_details.*']);

                $amountWord = CommonFunction::convert_number_to_words($billInfo->net_payable_amount);

                $totalAvailedServices = 0;
                $serviceWiseTrackingNumbers = array();
                foreach ($billDetails as $data) {
                    $service_id = 0;
                    $service_id = $data->service_id;
                    $explodedids = explode(',', $data->app_ids);

                    $trackingNumbers = array();
                    foreach ($explodedids as $app_id) {
                        $trackingNo = Processlist::where('service_id', $service_id)
                                ->where('record_id', $app_id)
                                ->pluck('track_no');
                        $trackingNumbers[] = $trackingNo;
                    }

                    $serviceWiseTrackingNumbers[$data->service_id] = $trackingNumbers;
                    $totalAvailedServices += $data->service_quantity;
                }

                $dn1d = new DNS1D();
                $trackingNo = $billInfo->tracking_number; // tracking no push on barcode.
                if (!empty($trackingNo)) {
                    $barcode = $dn1d->getBarcodePNG($trackingNo, 'C39');
                    $barcode_url = 'data:image/png;base64,' . $barcode;
                } else {
                    $barcode_url = '';
                }

                $content = view("billGeneration::invoice-pdf", compact(
                                'barcode_url', 'billDetails', 'billInfo', 'amountWord', 'totalAvailedServices', 'serviceWiseTrackingNumbers', 'countries', 'divisions', 'districts'))
                        ->render();

                $mpdf = new mPDF(
                        'utf-8', // mode - default ''
                        'A4', // format - A4, for example, default ''
                        11, // font size - default 0
                        'Times New Roman', // default font family
                        10, // margin_left
                        10, // margin right
                        10, // margin top
                        15, // margin bottom
                        10, // margin header
                        9, // margin footer
                        'P'
                );

                $mpdf->Bookmark('Start of the document');
                $mpdf->useSubstitutions;
                $mpdf->SetProtection(array('print'));
                $mpdf->SetDefaultBodyCSS('color', '#000');
                $mpdf->SetTitle("BEZA");
                $mpdf->SetSubject("Subject");
                $mpdf->SetAuthor("Business Automation Limited");
                $mpdf->autoScriptToLang = true;
                $mpdf->baseScript = 1;
                $mpdf->autoVietnamese = true;
                $mpdf->autoArabic = true;

                $mpdf->autoLangToFont = true;
                $mpdf->SetDisplayMode('fullwidth');
                $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
                $mpdf->setAutoTopMargin = 'stretch';
                $mpdf->setAutoBottomMargin = 'stretch';

                $mpdf->setWatermarkImage('assets/images/beza_watermark.png');
                $mpdf->showWatermarkImage = true;

                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHTML($content, 2);

                $mpdf->defaultfooterfontsize = 10;
                $mpdf->defaultfooterfontstyle = 'B';
                $mpdf->defaultfooterline = 0;
                $mpdf->setFooter('{PAGENO} / {nb}');

                $mpdf->SetCompression(true);
                $billYear = date('Y', strtotime($billInfo->bill_month));
                $baseURL = "bill_documents/";
                $basePath = date("Y/m", strtotime($billInfo->bill_month));

                $directoryName = $baseURL . $basePath . '/' . $billInfo->bill_type;
                $directoryNameYear = $baseURL . $billYear;

                if (!file_exists($directoryName)) {
                    $oldmask = umask(0);
                    mkdir($directoryName, 0777, true);
                    umask($oldmask);
                    $f = fopen($directoryName . "/index.html", "w");
                    fclose($f);
                    if (!file_exists($directoryNameYear . "/index.html")) {
                        $f = fopen($directoryNameYear . "/index.html", "w");
                        fclose($f);
                    }
                }
                $doc_name = $billInfo->tracking_number;
                $pdfFilePath = $directoryName . "/" . $doc_name . '.pdf';
                $mpdf->Output($pdfFilePath, 'F');   // Saving pdf "F" for Save only, "I" for view only.

                $pdfType = 'beza.bill-generation.' . env('server_type');
                $pdfQueue = pdfQueue::firstOrNew(['app_id' => $billInfo->id, 'service_id' => 0, 'pdf_type' => $pdfType]);
                $pdfQueue->attachment = $pdfFilePath;
                $pdfQueue->save();
            } // End billinfos foreach
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5008].');
            return Redirect::back()->withInput();
        }
    }

    public function invoicePDFforVA($billIds = array()) {
        try {
            $billInfos = BillInfo::leftJoin('visa_assistance as va', 'bill_info.project_clearance_id', '=', 'va.id')
                    ->leftjoin('pdf_queue as pq', function($join) {
                        $pdfType = 'beza.bill-generation.' . env('server_type');
                        $join->on('pq.app_id', '=', 'bill_info.id');
                        $join->on('pq.service_id', '=', DB::raw('0'));
                        $join->where('pq.pdf_type', '=', $pdfType);
                    })
                    ->whereIn('bill_info.id', $billIds)
                    ->get(['bill_info.*', 'va.tracking_number', 'va.applicant_name', 'va.country',
                'va.division', 'va.district', 'va.state', 'va.province',
                'va.road_no', 'va.house_no', 'va.post_code',
                'va.correspondent_phone as phone', 'va.fax', 'va.email', 'va.website', 'pq.secret_key']);

            $countries = Countries::lists('name', 'iso');
            $divisions = AreaInfo::where('area_type', 1)->lists('area_nm', 'area_id')->all();
            $districts = AreaInfo::where('area_type', 2)->lists('area_nm', 'area_id')->all();

            foreach ($billInfos as $billInfo) {

                $billDetails = BillInfoDetails::leftJoin('service_info as si', 'si.id', '=', 'bill_info_details.service_id')
                        ->where('bill_info_details.bill_id', $billInfo->id)
                        ->orderBy('bill_info_details.service_id')
                        ->get(['si.name as service_name', 'bill_info_details.*']);

                $amountWord = CommonFunction::convert_number_to_words($billInfo->net_payable_amount);

                $totalAvailedServices = 0;
                $serviceWiseTrackingNumbers = array();
                foreach ($billDetails as $data) {
                    $service_id = 0;
                    $service_id = $data->service_id;
                    $explodedids = explode(',', $data->app_ids);

                    $trackingNumbers = array();
                    foreach ($explodedids as $app_id) {
                        $trackingNo = Processlist::where('service_id', $service_id)
                                ->where('record_id', $app_id)
                                ->pluck('track_no');
                        $trackingNumbers[] = $trackingNo;
                    }

                    $serviceWiseTrackingNumbers[$data->service_id] = $trackingNumbers;
                    $totalAvailedServices += $data->service_quantity;
                }

                $dn1d = new DNS1D();
                $trackingNo = $billInfo->tracking_number; // tracking no push on barcode.
                if (!empty($trackingNo)) {
                    $barcode = $dn1d->getBarcodePNG($trackingNo, 'C39');
                    $barcode_url = 'data:image/png;base64,' . $barcode;
                } else {
                    $barcode_url = '';
                }
                $content = view("billGeneration::invoice-pdf", compact(
                                'barcode_url', 'billDetails', 'billInfo', 'amountWord', 'totalAvailedServices', 'serviceWiseTrackingNumbers', 'countries', 'divisions', 'districts'))->render();

                $mpdf = new mPDF(
                        'utf-8', // mode - default ''
                        'A4', // format - A4, for example, default ''
                        12, // font size - default 0
                        'dejavusans', // default font family
                        10, // margin_left
                        10, // margin right
                        10, // margin top
                        15, // margin bottom
                        10, // margin header
                        9, // margin footer
                        'P'
                );

                $mpdf->Bookmark('Start of the document');
                $mpdf->useSubstitutions;
                $mpdf->SetProtection(array('print'));
                $mpdf->SetDefaultBodyCSS('color', '#000');
                $mpdf->SetTitle("BEZA");
                $mpdf->SetSubject("Subject");
                $mpdf->SetAuthor("Business Automation Limited");
                $mpdf->autoScriptToLang = true;
                $mpdf->baseScript = 1;
                $mpdf->autoVietnamese = true;
                $mpdf->autoArabic = true;

                $mpdf->autoLangToFont = true;
                $mpdf->SetDisplayMode('fullwidth');
                $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
                $mpdf->setAutoTopMargin = 'stretch';
                $mpdf->setAutoBottomMargin = 'stretch';
                $mpdf->setWatermarkImage('assets/images/beza_watermark.png');
                $mpdf->showWatermarkImage = true;
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHTML($content, 2);

                $mpdf->defaultfooterfontsize = 10;
                $mpdf->defaultfooterfontstyle = 'B';
                $mpdf->defaultfooterline = 0;
                $mpdf->setFooter('{PAGENO} / {nb}');

                $mpdf->SetCompression(true);
                $billYear = date('Y', strtotime($billInfo->bill_month));
                $baseURL = "bill_documents/";
                $basePath = date("Y/m", strtotime($billInfo->bill_month));

                $directoryName = $baseURL . $basePath . '/' . $billInfo->bill_type;
                $directoryNameYear = $baseURL . $billYear;

                if (!file_exists($directoryName)) {
                    $oldmask = umask(0);
                    mkdir($directoryName, 0777, true);
                    umask($oldmask);
                    $f = fopen($directoryName . "/index.html", "w");
                    fclose($f);
                    if (!file_exists($directoryNameYear . "/index.html")) {
                        $f = fopen($directoryNameYear . "/index.html", "w");
                        fclose($f);
                    }
                }
                $doc_name = $billInfo->tracking_number;
                $pdfFilePath = $directoryName . "/" . $doc_name . '.pdf';
                $mpdf->Output($pdfFilePath, 'F');   // Saving pdf "F" for Save only, "I" for view only.

                $pdfType = 'beza.bill-generation.' . env('server_type');
                $pdfQueue = pdfQueue::firstOrNew(['app_id' => $billInfo->id, 'service_id' => 0, 'pdf_type' => $pdfType]);
                $pdfQueue->attachment = $pdfFilePath;
                $pdfQueue->save();
            }
        } catch (\Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()) . '[BGC5009].');
            return Redirect::back()->withInput();
        }
    }

    /*     * ***************************End of Controller Class******************************* */
}
