<?php

namespace App\Modules\Files\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Files\Models\BirthCrtTmp;
use App\Modules\Files\Models\Files;
use App\Modules\Files\Models\FilesTmp;
use App\Modules\Files\Models\ImgAuthFile;
use App\Modules\Files\Models\ImgBCertificate;
use App\Modules\Files\Models\ImgPilgrim;
use App\Modules\Files\Models\ImgUserProfile;
use App\Modules\Files\Models\PilgrimNrbTmp;
use Illuminate\Http\Request;

class FilesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @param $type
     * @param Request $request
     * @param ImgUserProfile $ImgUserProfile
     * @return Response
     */
    public function store($type, Request $request, ImgUserProfile $ImgUserProfile, ImgAuthFile $imgAuthFile, ImgBCertificate $imgBCertificate, ImgPilgrim $imgPilgrim, FilesTmp $FilesTmp) {
        if ($type) {
            $err = array();
            $base64_img = $request->get('base64_img');
            $ing_data = getimagesize($base64_img);
            $img_mime_type = $ing_data['mime'];
            $img_width = $ing_data['0'];
            $img_height = $ing_data['1'];
            extract(CommonFunction::getImageDocConfig());

            // picture dimension and size validation
            if($img_mime_type!='image/jpeg' && $img_mime_type!='image/png'){
                $err[]="Please select JPEG, PNG file<br/>";
            }

            // checking image size and dimension
            if($type=='user' || $type=='profile_pic_tmp') {
                    if (!($img_width >= $IMAGE_MIN_WIDTH && $img_width <= $IMAGE_MAX_WIDTH ) || !($img_height >= $IMAGE_MIN_HEIGHT && $img_height<= $IMAGE_MAX_HEIGHT )) {
//                        $err[]= "Image width  between ($IMAGE_MIN_WIDTH-$IMAGE_MAX_WIDTH)px <br/>& height  between ($IMAGE_MIN_HEIGHT-$IMAGE_MAX_HEIGHT)px required<br/>";
                    }
            }

            if($type=='auth_file' || $type=='auth_file_tmp' || $type=='birth_crt_tmp' || $type == 'passport_nid_file') {
                    if (!($img_width >= $DOC_MIN_WIDTH && $img_width <= $DOC_MAX_WIDTH ) || !($img_height >= $DOC_MIN_HEIGHT && $img_height <= $DOC_MAX_HEIGHT )) {
//                        $err[]= "File width  between ($DOC_MIN_WIDTH-$DOC_MAX_WIDTH)px <br/>& height  between ($DOC_MIN_HEIGHT-$DOC_MAX_HEIGHT)px required<br/>";
                    }
            }

            if(isset($err) && count($err)>0){
                $data = ['responseCode' => 0, 'data' => $err];
            }else {
                if ($type != 'auth_file_tmp') {
                    $ref_id = Encryption::decodeId($request->get('ref_id'));
                } else {
                    $ref_id = $request->get('ref_id');
                }

                if ($type != 'passport_nid_file') {
                    $ref_id = Encryption::decodeId($request->get('ref_id'));
                } else {
                    $ref_id = $request->get('ref_id');
                }

                if ($type == 'user') {
                    $files_data = array(
                        'ref_id' => $ref_id,
                        'details' => $base64_img
                    );
                    $count = ImgUserProfile::where(['ref_id' => $ref_id])->count();
                    if ($count == 0) {
                        $ImgUserProfile->create($files_data);
                    } else {
                        $ImgUserProfile->where(array('ref_id' => $ref_id))->update($files_data);
                    }
                } elseif ($type == 'auth_file') {
                    $files_data = array(
                        'ref_id' => $ref_id,
                        'details' => $base64_img
                    );
                    $count = ImgAuthFile::where(['ref_id' => $ref_id])->count();
                    if ($count == 0) {
                        $imgAuthFile->create($files_data);
                    } else {
                        $imgAuthFile->where(array('ref_id' => $ref_id))->update($files_data);
                    }
                } elseif ($type == 'auth_file_tmp' || $type == 'birth_crt_tmp' || $type == 'profile_pic_tmp' || $type == 'pilgrim_pic_tmp' || $type=="pilgrim_nrb_tmp" || $type=="passport_nid_file") {
                    $files_data = array(
                        'nid' => $ref_id,
                        'type' => $type,
                        'details' => $base64_img
                    );
                    $count = FilesTmp::where(['nid' => $ref_id, 'type' => $type])->count();
                    if ($count == 0) {
                        $FilesTmp->create($files_data);
                    } else {
                        $FilesTmp->where(array('nid' => $ref_id, 'type' => $type))->update($files_data);
                    }
                }
                $data = ['responseCode' => 1, 'data' => $files_data];
            }
            return json_encode($data);
        }
    }

    public function getFile($param) {
        if ($param['type'] == 'user') {
            $info = ImgUserProfile::where(['ref_id' => $param['ref_id']])->first();
        } elseif ($param['type'] == 'pilgrim') {
            $info = ImgPilgrim::where(['ref_id' => $param['ref_id']])->first();
        } elseif ($param['type'] == 'auth_file') {
            $info = ImgAuthFile::where(['ref_id' => $param['ref_id']])->first();
        } elseif ($param['type'] == 'birth_crt') {
            $info = ImgBCertificate::where(['ref_id' => $param['ref_id']])->first();
        } elseif ($param['type'] == 'nationality2') {
            $info = PilgrimNrbTmp::where(['ref_id' => $param['ref_id']])->first();
        }

        if (!$info) {
            $data = ['responseCode' => 0, 'data' => null];
        } else {
            $data = ['responseCode' => 1, 'data' => $info->details];
        }
        return response()->json($data);
    }

    public function transferTmpFile($parm) {
        $file_data = FilesTmp::where(['type' => $parm['type']])->first();
        if ($file_data) {
             if ($parm['type'] == 'auth_file_tmp') {
                $files_data['ref_id'] = $parm['ref_id'];
                $files_data['details'] = $file_data->details;
                ImgAuthFile::create($files_data);
                FilesTmp::where(['type' => $parm['type'], 'nid' => $parm['nid']])->delete();
            }

        } else {
            return null;
        }
    }

}
