 <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td><div align="center">#</div></td>
                        <td class="labelcell" colspan="6"><div align="center">Required Attachments</div></td>
                        <td class="labelcell" colspan="2"><div align="center">Action</div></td>
                    </tr>
                    <?php  $i = 1; ?>
                    @foreach($document as $row)
                        <tr>
                            <td><div align="center">{!! $i !!}</div></td>
                            <td colspan="6">{!!  $row->doc_name !!}</td>
                            <td colspan="2">
                                    {!! Form::hidden('doc_name',$row->doc_name) !!}
                                    {!! Form::file('doc_file') !!}
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach

                    </tbody>
                </table>