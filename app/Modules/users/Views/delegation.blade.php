@extends('layouts.admin')

@section('page_heading','Delegated Information')

@section("content")
<?php
$accessMode = ACL::getAccsessRight('user');
if (!ACL::isAllowed($accessMode, 'E'))
    die('no access right!');
?>
<section class="content">

    <!-- Default box -->
    <div class="box box-success col-md-10">
        <div class="box-header with-border">
            @include('partials.messages')
        </div>

        <div class="box-body">
            <div class="col-md-12">
                {!! Form::open(array('url' => '/users/store-delegation','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'delegation_form',
                'enctype' =>'multipart/form-data', 'files' => 'true')) !!}

                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('desk_id') ? 'has-error' : ''}}">
                            <label  class="col-md-3 text-right">Designation From : </label>
                            <div class="col-md-4">
                                {{ $info->desk_name }}
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('desk_id') ? 'has-error' : ''}}">
                            <label  class="col-md-3 text-right">Delegate From : </label>
                            <div class="col-md-4">
                                {!! Form::hidden('user_id',$info->id) !!}
                                {{ $info->user_full_name }}
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('designation') ? 'has-error' : ''}}">
                            <label  class="col-md-3 text-right required-star">Designation To :</label>

                            <div class="col-md-4">
                                {!! Form::select('designation', $designation, '', $attributes = array('class'=>'form-control required', 'onchange'=>'getUserDeligate()', 'placeholder' => 'Select Desk', 'id'=>"designation")) !!}
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('delegated_user') ? 'has-error' : ''}}">
                            <label  class="col-md-3 text-right required-star">Delegate To :</label>

                            <div class="col-md-4">
                                {!! Form::select('delegated_user', [] , '', $attributes = array('class'=>'form-control required',
                                'placeholder' => 'Select User', 'id'=>"delegated_user")) !!}
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('remarks') ? 'has-error' : ''}}">
                            <label  class="col-md-3 text-right required-star">Remarks :</label>

                            <div class="col-md-4">
                                {!! Form::text('remarks','', $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter your Remarks','id'=>"remarks")) !!}
                            </div>
                        </div>
                        <div class='clearfix'></div>
                        <div class="col-md-3"></div>
                        <div class="form-group col-md-4">
                            @if(ACL::getAccsessRight('user','A'))
                            <button type="submit" class="btn btn-block btn-primary"><b>Deligate</b></button>
                            @endif
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @endsection

        @section('footer-script')
        <script>
            $(function() {
                $("#delegation_form").validate({
                    errorPlacement: function() {
                        return false;
                    }
                });
            });
            function getUserDeligate() {
                var _token = $('input[name="_token"]').val();
                var designation = $('#designation').val();

                $.ajax({
                    url: '{{url("users/get-delegate-userinfos")}}',
                    type: 'post',
                    data: {
                        _token: _token,
                        designation: designation
                    },
                    dataType: 'json',
                    success: function(response) {
                        html = '<option value="">Select One</option>';

                        $.each(response, function(index, value) {
                            html += '<option value="' + value.id + '" >' + value.user_full_name + '</option>';
                        });
                        $('#delegated_user').html(html);
                    },
                    beforeSend: function(xhr) {
                        console.log('before send');
                    },
                    complete: function() {
                        //completed
                    }
                });
            }
        </script>
        @endsection <!--- footer-script--->
