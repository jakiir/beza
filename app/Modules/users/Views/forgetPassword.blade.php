@extends('layouts.front')

@section ('body')

    <link rel="stylesheet" href="{{ asset("assets_2/css/bootstrap.css") }}" />

    <header style="width: 100%; height: auto;">
        <div class="col-md-12 text-center">
            <div class="col-md-3"></div>
            <div class="col-md-6"  style="margin-top:5px;">
                <img width="70" alt="Government Logo" src="/assets/images/logo_beza_single.png"/>
                <br /><br />
                <h3 class="less-padding" style="color: #119213;">Bangladesh Economic Zones Authority  (BEZA)</h3>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="clearfix"> <br></div>
    </header>

    <div class="col-md-12">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <hr class="top-border"/>
    </div>
    <div class="col-md-1"></div>
</div>
    
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-md-offset-4"><br /><br />

                @section ('login_panel_title','Forget Password')
                @section ('login_panel_body')
                    @if (count($errors))
                        <ul class="">
                            @foreach($errors->all() as $error)
                                <li>{!!$error!!}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(array('url' => 'users/reset-forgotten-password','method' => 'patch', 'id' => 'forget_form')) !!}
                    {!! session()->has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'.session('success') .'</div>' : '' !!}
                    {!! session()->has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. session('error') .'</div>' : '' !!}

                    <fieldset>
                        <div class="form-group">
                            {!! Form::text('user_email', $value = null, $attributes = array('class'=>'form-control required email',
                            'placeholder'=>'Enter your Email Address','id'=>"user_email")) !!}
                        </div>
                        <div class="form-group pull-right  {{$errors->has('g-recaptcha-response') ? 'has-error' : ''}}">
                            <div class="col-md-12">
                                {!! Recaptcha::render() !!}
                                {!! $errors->first('g-recaptcha-response','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LfYyCUTAAAAAI30AkHMZ-V7M-IV38JFdTMCpKVY"></div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                        <span class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-block btn-info"><b>Submit</b></button><br/>
                        </span>
                            </div>
                    <span class="col-md-12 text-center">
                        <p><b>Don't have an account? {!! link_to('users/create', 'Sign Up', array("class" => " ")) !!}</b></p>
                        <p><b>Back to Login! {!! link_to('users/login', 'Login', array("class" => " ")) !!}</b></p>
                    </span>
                        </div>
                    </fieldset>
                    {!! Form::close() !!}

                @endsection
                @include('widgets.panel', array('as'=>'login', 'header'=>true))
            </div>
        </div>
    </div>

@section('footer-script')
    <script language="javascript">
        $(document).ready(function() {
            $("#forget_form").validate({
                errorPlacement: function() {
                    return false;
                }
            });
        });
    </script>
@endsection

@stop