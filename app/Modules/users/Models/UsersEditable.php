<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UsersEditable extends Model {

    protected $table = 'users';
    protected $fillable = array(
        'id',
        'user_type',
        'user_sub_type',
        'desk_id',
        'bank_branch_id',
        'user_email',
        'password',
        'user_hash',
        'eco_zone_id',
        'user_status',
        'user_verification',
        'user_full_name',
        'designation',
        'user_middle_name',
        'user_last_name',
        'user_nid',
        'user_DOB',
        'user_gender',
        'user_aboutme',
        'user_street_address',
        'user_country',
        'user_city',
        'user_division',
        'user_zip',
        'user_phone',
        'authorization_file',
        'passport_nid_file',
        'signature',
        'user_pic',
        'user_first_login',
        'user_language',
        'security_profile_id',
        'division',
        'district',
        'thana',
        'details',
        'country',
        'nationality',
        'passport_no',
        'state',
        'province',
        'road_no',
        'house_no',
        'post_code',
        'user_fax',
        'remember_token',
        'login_token',
        'auth_token_allow',
        'user_hash_expire_time',
        'auth_token',
        'auth_token_allow',
        'user_agreement',
        'first_login',
        'is_approved',
        'is_locked',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            if (Auth::guest()) {
                $post->created_by = 0;
                $post->updated_by = 0;
            } else {
                $post->created_by = Auth::user()->id;
                $post->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($post) {
            if (Auth::guest()) {
                $post->updated_by = 0;
            } else {
                $post->updated_by = Auth::user()->id;
            }
        });
    }

    /*     * ***************************** Users Model Class ends here ************************* */
}
