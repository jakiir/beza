<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;

class Stakeholder extends Model {

    protected $table = 'stakeholder';
    protected $fillable = array(
        'process_id',
        'stholder_name',
        'stholder_designation',
        'stholder_address',
        'stholder_email',
        'updated_by',
    );

    /************************ Users Model Class ends here ****************************/
}
