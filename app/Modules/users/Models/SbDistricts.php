<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class SbDistricts extends Model {

    protected $table = 'sb_districts';
    protected $fillable = array(
        'id',
        'user_id',
        'district_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    /************************ Users Model Class ends here ****************************/
}
