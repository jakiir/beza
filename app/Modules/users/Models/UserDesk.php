<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;

class UserDesk extends Model {

    protected $table = 'user_desk';
    protected $fillable = array(
        'desk_id',
        'desk_name',
        'desk_status',
        'user_type',
        'is_registarable',
        'delegate_to_desk',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *****************************End of Model Class********************************** */
}
