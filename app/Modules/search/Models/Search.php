<?php namespace App\Modules\Search\Models;

use App\Modules\Pilgrim\Models\Pilgrim;
use App\Modules\Users\Models\UsersModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Search extends Model {

    public function getSearchList($search_text) {
        $search_text = trim($search_text);
        $returnData = Pilgrim::leftJoin('pilgrim_groups', 'pilgrim_groups.id', '=', 'pilgrims.group_id')
            ->orderBy('pilgrims.created_at', 'desc')
            ->Where(function($query) use ($search_text) {
                return $query->where('full_name_english', 'like',"%$search_text%")
                    ->orWhere('tracking_no', "$search_text")
                    ->orWhere('national_id', "$search_text");
            })
            ->where('pilgrims.is_archived',0)
            ->limit(10)
            ->get([
                'pilgrims.id',
                'tracking_no',
                'full_name_english',
                'father_name',
                'national_id',
                'mobile',
                'birth_date',
                'serial_no',
                'father_name',
                'district',
                'payment_status',
                'pilgrims.created_by',
                'pilgrim_groups.group_name',
                'pilgrims.is_registrable'
            ]);
        return $returnData;
    }

    public function getUserList($search_text) {
        $search_text = trim($search_text);
        $returnData = UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                ->leftJoin('area_info', 'users.district', '=', 'area_info.area_id')
                ->orderBy('users.created_at', 'desc')
                ->where('users.user_status', '!=', 'rejected')
                ->where('users.id', '!=', Auth::user()->id)
                ->Where(function($query) use ($search_text) {
                    return $query->where('user_full_name','like', "%$search_text%")
                        ->orWhere('user_email', "$search_text");
                })
                ->get([
                    'users.id',
                    'users.user_type',
                    'users.user_full_name',
                    'users.user_email',
                    'mty.type_name',
                    'users.user_status',
                    'area_info.area_nm as users_district'
                ]);
        return $returnData;
    }
}
