@extends('layouts.admin')

@section('page_heading',trans('messages.user_view'))

@section('content')
    <?php $accessMode=ACL::getAccsessRight('search');
    if(!ACL::isAllowed($accessMode,'V')) die('no access right!');
    ?>
    <div class="col-lg-12">
        <section class="col-md-12" id="printDiv">
            <div class="row"><!-- Horizontal Form -->

                {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
                {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}

                <div class="panel">
                    <div class="panel-body">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Profile of : {!!$user->user_full_name!!}</h3>
                            </div> <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="col-md-3">

                                    {!! Html::image("assets/images/envelope.jpg", "Auth letter",['class'=>'profile-user-img img-responsive img-circle','width'=>200]) !!}

                                    <br/>

                                    @if (!empty($auth_file))
                                        <a href="<?php echo $auth_file; ?>" target="_blank">
                                            Click to see authorization letter
                                        </a>
                                    @else

                                        <span class="text-danger"> Letter Not Found</span>
                                    @endif
                                </div>
                                <div class="col-md-9">
                                    <dl class="dls-horizontal">
                                        <dt>Full Name :</dt>
                                        <dd>{!!$user->user_full_name!!}&nbsp;</dd>
                                        <dt>Type :</dt>
                                        <dd>{!!$user->type_name!!}&nbsp;</dd>
                                        <dt>NID :</dt>
                                        <dd>{!!$user->user_nid!!}&nbsp;</dd>
                                        <dt>Phone :</dt>
                                        <dd>{!!$user->user_phone!!}&nbsp;</dd>
                                        <dt>Email :</dt>
                                        <dd>{!!$user->user_email!!}&nbsp;</dd>
                                        <dt>Gender :</dt>
                                        <dd>{!!$user->user_gender!!}&nbsp;</dd>
                                        @if($user->district_name)
                                            <dt>District :</dt>
                                            <dd>{!!$user->district_name!!}&nbsp;</dd>
                                        @endif
                                        @if($user->thana_name)
                                            <dt>Thana :</dt>
                                            <dd>{!!$user->thana_name!!}&nbsp;</dd>
                                        @endif
                                        @if($user->user_DOB)
                                            <dt>Date of Birth :</dt>
                                            <dd>
                                                {!!CommonFunction::changeDateFormat($user->user_DOB)!!}&nbsp;
                                            </dd>
                                        @endif
                                        <dd>
                                        @if ($user->is_approved != 1)
                                            <dt>Verification expire time :</dt>
                                            <dd>{!!$user->user_hash_expire_time!!}&nbsp;</dd>
                                        @endif
                                        @if(in_array(Auth::user()->user_type,array("1x101","2x202","2x203")))
                                            @foreach($userMoreInfo as $key=>$info)
                                                <dt>{!!$key!!} :</dt>
                                                <dd>{!!$info!!}&nbsp;</dd>
                                            @endforeach
                                        @endif
                                    </dl>
                                </div>
                                @if(Auth::user()->user_type=='2x203' || Auth::user()->user_type=='2x202' || Auth::user()->user_type=='1x101')
                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <?php $email=Encryption::encode($user->user_email);?>
                                            @if($user->user_verification!='yes')
                                                <a href="{{url('users/resendMailByAdmin/'.$email)}}" class="btn btn-warning btn-sm">Send Activation Mail</a>
                                            @endif
                                            <?php
                                            if ($user->is_approved != 1) {
                                                echo '<a href="'.url('users/extent-verification-time/'.Encryption::encodeId($user->id)).'"><button class="btn btn-primary">Extend expired verification time 1 hour</button></a>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-md-12">
                                    @if(count($logging['fails'])>0)
                                        <h4 class=" text-danger">Fails logged </h4>
                                        {!! createHTMLTable($logging['fails']) !!}
                                    @endif

                                    @if(count($logging['log'])>0)
                                        <h4 class="text-success">Success logged </h4>
                                        {!! createHTMLTable($logging['log']) !!}
                                    @endif
                                </div>
                            </div>
                        </div><!-- /.box -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection