<?php

namespace App\Modules\Search\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\Encryption;
use App\Libraries\CommonFunction;
use App\Modules\Search\Models\Search;
use App\Modules\Settings\Models\Faq;
use App\Modules\Settings\Models\FaqTypes;
use App\Modules\Users\Models\FailedLogin;
use App\Modules\Users\Models\UserLogs;
use App\Modules\Users\Models\UsersModel;
use App\Modules\Settings\Models\FaqMultiTypes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            \App::setLocale(Session::get('lang'));
    }

    public function index(Request $request) {
        if(!ACL::getAccsessRight('report','V')) die ('no access right!');
        $category = $request->get('faqs_type');
        $search = $request->get('q');
        $status = $request->get('status');
        $faqs_type = ['' => 'All'] + FaqTypes::orderBy('name')->lists('name', 'id')->all();


        $faqs = Faq::leftJoin('faq_multitypes', 'faq.id', '=', 'faq_multitypes.faq_id')
                ->leftJoin('faq_types', 'faq_multitypes.faq_type_id', '=', 'faq_types.id')
                ->where(function($query) use ($category, $search, $status) {
                    if ($category) {
                        $query->where('faq_types.id', $category);
                    }
                    if ($search) {
                        $query->where('question', 'like', "%$search%");
                    }
                    if ($status) {
                        $query->where('faq.status', $status);
                    } else {
                        $query->where('status', 'public');
                         if ($category) {
                            $query->orWhere(['faq_types.id'=> $category,'faq.status'=>'draft','faq.updated_by'=> Auth::user()->id]);
                         } else {
                            $query->orWhere(['faq.status'=>'draft','faq.updated_by'=> Auth::user()->id]);
                         }
                    }
                })
                ->groupBy('faq.id')
                ->get(['faq.id', 'question', 'answer', 'status', 'faq_type_id as types', DB::raw('group_concat(distinct name) as faq_type_name'),
            'faq.created_by', 'faq.updated_by', 'faq.updated_at']);
        return view("search::list", compact('faqs', 'faqs_type'));
    }

    public function pilgrim(Request $request) {
        if (!ACL::getAccsessRight('search', 'V'))
            die('no access right!');
        $faqs_type = ['' => 'All'] + FaqTypes::orderBy('name')->lists('name', 'id')->all();
        $faqs = [];
        $search = $request->get('qp');
        $searchModel = new Search();
        $search_result = [];
        if (trim($search)) {
            $search_result = $searchModel->getSearchList($search);
            if (count($search_result) == 1) {
                if($search_result[0]->is_registrable==1){
                    return redirect('registration/application/view/' . Encryption::encodeId($search_result[0]->id));
                }else{
                    return redirect('pilgrim/view/' . Encryption::encodeId($search_result[0]->id) . '/search/pilgrim');
                }
            }
        }
        return view("search::list", compact('faqs', 'faqs_type', 'search_result'));
    }

    public function user(Request $request) {
        $faqs_type = ['' => 'All'] + FaqTypes::orderBy('name')->lists('name', 'id')->all();
        $faqs = [];
        $search = trim($request->get('qu'));
        $searchModel = new Search();
        $user_result = [];
        if ($search) {
            $user_result = $searchModel->getUserList($search);
        }
        return view("search::list", compact('faqs', 'faqs_type', 'user_result'));
    }

    /* Start of FAQ related functions */

    public function faq() {
        if(!ACL::getAccsessRight('report','V')) die ('no access right!');
        return view("search::faq.list", compact('getList'));
    }

    public function createFaq() {
        if(!ACL::getAccsessRight('report','A')) die ('no access right!');
        $faq_types = FaqTypes::orderBy('name')->lists('name', 'id');
        return view("search::faq.create", compact('faq_types'));
    }

    public function getFaqDetailsData() {
        if(!ACL::getAccsessRight('report','V')) die ('no access right!');
        $faqs = Faq::leftJoin('faq_multitypes', 'faq.id', '=', 'faq_multitypes.faq_id')
                ->leftJoin('faq_types', 'faq_multitypes.faq_type_id', '=', 'faq_types.id')
                ->groupBy('faq.id')
                ->get(['faq.id as faq_id', 'question', 'answer', 'status', 'faq_types.name as faq_type']);
        return Datatables::of($faqs)
                        ->addColumn('action', function ($faqs) {
                            return '<a href="/search/edit-faq/' . Encryption::encodeId($faqs->faq_id) .
                                    '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                        })
                        ->removeColumn('id', 'faq_type')
                        ->make(true);
    }

    public function storeFaq(Request $request) {
        if (!ACL::getAccsessRight('search', 'A')) {
            die('no access right!');
        }
        $this->validate($request, [
            'question' => 'required',
            'answer' => 'required',
            'status' => 'required',
        ]);
        try{
            DB::beginTransaction();
            $insert = Faq::create(
                            array(
                                'question' => $request->get('question'),
                                'answer' => $request->get('answer'),
                                'status' => $request->get('status'),
                                'created_by' => CommonFunction::getUserId()
            ));

            $faq_id = $insert->id;

            $types = $request->get('type');

            foreach ($types as $type) {
                FaqMultiTypes::create(
                        array(
                            'faq_id' => $faq_id,
                            'faq_type_id' => $type,
                            'created_by' => CommonFunction::getUserId()
                ));
            }

            DB::commit();
            Session::flash('success', 'Data is stored successfully!');
            return redirect('/search/edit-faq/' . Encryption::encodeId($insert->id));
        }catch (\Exception $e) {
            DB::rollback();
            Session::flash('error', 'Sorry! Somthing Wrong.');
            return Redirect::back()
                ->withInput();
        }
    }

    public function editFaq($encrypted_id) {
        if (!ACL::getAccsessRight('search', 'E'))
            die('no access right!');
        $id = Encryption::decodeId($encrypted_id);
        $data = Faq::where('id', $id)->first();

        $faq_types = FaqTypes::orderBy('name')->lists('name', 'id');
        $multitypes = FaqMultiTypes::where('faq_id', $id)->get(['faq_type_id as id']);
        $selec = array();
        foreach ($multitypes as $row) {
            $selec[] = $row->id;
        }
        return view("search::faq.edit", compact('data', 'encrypted_id', 'faq_types', 'selec'));
    }

    public function updateFaq($id, Request $request) {
        if (!ACL::getAccsessRight('search', 'E'))
            die('no access right!');
        $faq_id = Encryption::decodeId($id);

        $this->validate($request, [
            'question' => 'required',
            'answer' => 'required',
            'status' => 'required',
        ]);

        Faq::where('id', $faq_id)->update([
            'question' => $request->get('question'),
            'answer' => $request->get('answer'),
            'status' => $request->get('status'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        FaqMultiTypes::where('faq_id', $faq_id)->delete();

        $types = $request->get('type');

        foreach ($types as $type) {
            FaqMultiTypes::create(
                    array(
                        'faq_id' => $faq_id,
                        'faq_type_id' => $type,
                        'created_by' => CommonFunction::getUserId()
            ));
        }

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/search/edit-faq/' . $id);
    }

    public function userView($id, UsersModel $usersModel) {
        $user_id = Encryption::decodeId($id);

        $profile_pic = CommonFunction::getPicture('user', $user_id);
        $auth_file = CommonFunction::getPicture('auth_file', $user_id);

        $user = $usersModel->getUserRow($user_id);
        $userMoreInfo = ACL::getUserDetails($user->id, $user->user_type, $user->user_sub_type);

        $logging['log'] = UserLogs::where('user_id',$user_id)->orderBy('id','desc')->take(20)->get(['ip_address as IP','login_dt as login_time','logout_dt as logout_time']);
        $logging['fails'] = FailedLogin::where('user_email',$user->user_email)->orderBy('id','desc')->take(20)->get(['remote_address as IP','created_at as time']);
        return view('search::view_user', compact("user", "profile_pic", "auth_file", "userMoreInfo",'logging'));
    }

    /* End of FAQ related functions */

    /*     * *****************************End of Controller Class****************************** */
}
