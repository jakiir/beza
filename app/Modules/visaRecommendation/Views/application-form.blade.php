@extends('layouts.admin')

@section('content')

@include('partials.messages')

    <?php
    $accessMode = ACL::getAccsessRight('visaRecommend');
    if (!ACL::isAllowed($accessMode, $mode)) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    ?>
<style>
    .text-title{ font-size: 16px !important}
    .text-sm{ font-size: 9px !important}
    #VisaRecForm label.error {display: none !important; }
    .calender-icon{
        border: none; padding-top: 8px ! important;
    }

     input[type=radio].error, input[type=checkbox].error{
         outline: 1px solid red !important;
     }
</style>

<section class="content">
    @if($viewMode == 'on')
        @if(isset($alreadyExistApplicant->status_id) && $alreadyExistApplicant->status_id != 8)
        <div class="col-md-12 pull-right" style="padding-bottom: 5px;">
            <a href="/visa-recommend/view-pdf/{{ Encryption::encodeId($alreadyExistApplicant->id)}}" target="_blank"
               class="btn btn-danger btn-sm pull-right">
                <i class="fa fa-download"></i> <strong>Application Download as PDF</strong>
            </a>
        </div>
        @endif
    @endif

    
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                @if($viewMode == 'on')
                    @if(in_array(Auth::user()->desk_id,array(3,4,5,6)))
                        {!! Form::open(['url' => '/visa-recommend/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from',
                        'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                        @include('visaRecommendation::batch-process')
                        <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                        <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                        {!! Form::close() !!}
                    @endif

                    @if(($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)
                        {!! Form::open(array('url' => 'visa-recommend/challan-store/'.Encryption::encodeId($alreadyExistApplicant->id),
                        'enctype'=>'multipart/form-data','method' => 'post', 'files' => true, 'role'=>'form')) !!}
                        <div class="panel panel-primary">
                            <div class="panel-heading">Pay order related information</div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                            {!! Form::label('challan_no','Pay Order No : ',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('challan_no', null,['class'=>'form-control bnEng required input-sm',
                                                'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                            {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::select('bank_name', $banks, (!empty($alreadyExistApplicant->bank_name) ? $alreadyExistApplicant->bank_name : ''),
                                                ['class' => 'form-control input-sm textOnly required']) !!}
                                                {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                            {!! Form::label('amount','Amount :',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('amount',null, ['class'=>'form-control input-sm bnEng required input-sm','placeholder'=>'5000',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                            {!! Form::label('date','Date :',['class'=>'col-md-6 font-ok required-star']) !!}
                                            <div class="datepicker col-md-6  input-group date" data-date-format="yyyy-mm-dd">
                                                {!! Form::text('date', null, ['class'=>'form-control input-sm required', 'id' => 'user_DOB']) !!}
                                                <span class="input-group-addon calender-icon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                            {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('branch',null, ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 {{$errors->has('challan_file') ? 'has-error' : ''}}">
                                            {!! Form::label('challan_file','Pay Order copy :',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::file('challan_file',null, ['class'=>'form-control bnEng required input-sm',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('challan_file','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary pull-left next">
                                            <i class="fa fa-chevron-circle-right"></i> Save</button>
                                    </div>
                                </div>

                            </div>

                        {!! Form::close() !!}<!-- /.form end -->
                        </div> <!--End of Panel Group-->
                    @endif {{-- status_id == 21  and created by logged user --}}
                @endif



                <div class="panel panel-red"  id="inputForm">
                    <div class="panel-heading"><b> Application for Visa Recommendation</b></div>
                    <div class="panel-body">
                        <?php if ($viewMode != 'on') { ?>
                        {!! Form::open(array('url' => 'visa-recommend/store-app','enctype'=>'multipart/form-data','method' => 'post','id' => 'VisaRecForm','role'=>'form')) !!}
                        
                        <input type ="hidden" name="app_id" id="app_id" value="{{(!empty($alreadyExistApplicant->id) ? Encryption::encodeId($alreadyExistApplicant->id) : '')}}">
                        <input type="hidden" name="selected_file" id="selected_file" />
                        <input type="hidden" name="validateFieldName" id="validateFieldName" />
                        <input type="hidden" name="isRequired" id="isRequired" />
                        <?php } ?>
                        <h3 class="text-center hiddenDiv">Applicant Information (Part A)</h3>
                            <?php if($viewMode == 'on') { ?>
                            <section class="content-header">
                                <ol class="breadcrumb">
                                    <li><strong>Tracking no. : </strong>
                                        {{ $process_data->track_no  }}
                                    </li>
                                    <li><strong> Date of Submission : </strong> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }} </li>
                                    <li><strong>Current Status : </strong>
                                        @if($form_data->status_id == -1) Draft
                                        @else {!! $statusArray[$form_data->status_id] !!}
                                        @endif
                                    </li>

                                    <li>
                                        @if($process_data->desk_id != 0) <strong>Current Desk :</strong> {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                        @else
                                            <strong>Current Desk :</strong> Applicant
                                        @endif
                                    </li>
                                    @if(isset($form_data->status_id) && $form_data->status_id == 8)
                                        <li>
                                            <strong>Discard Reason :</strong> {{ (!empty($form_data->remarks) ? $form_data->remarks : 'N/A') }}
                                        </li>
                                    @endif
                                    <li>
                                        <?php if ($form_data->status_id == 23 && !empty($form_data->certificate)) { ?>
                                        <a target="_blank" title="Download Certificate" class="btn show-in-view btn-xs btn-info" href="{{ url($form_data->certificate) }}"><i class="fa  fa-file-pdf-o"></i> <b>Download Certificate</b></a>
                                            @if(Auth::user()->user_type == '1x101')
                                                <a onclick="return confirm('Are you sure ?')" href="/visa-recommend/discard-certificate/{{ Encryption::encodeId($form_data->id)}}" class="btn show-in-view btn-xs btn-danger"
                                                   title="Discard Certificate"> <i class="fa  fa-trash"></i> <b>Discard Certificate</b></a>
                                            @endif
                                            <a href="/visa-recommend/certificate-re-gen/{{ Encryption::encodeId($form_data->id)}}" class="btn show-in-view btn-xs btn-warning"
                                               title="Re Generate Certificate" target="_self"> <i class="fa  fa-file-pdf-o"></i> <b>Re-generate certificate</b></a>
                                        <?php } ?>
                                    </li>
                                </ol>
                            </section>

                            @if(!empty($alreadyExistApplicant->challan_no))
                                <div class="panel panel-primary"  id="ep_form">
                                    <div class="panel-heading">Pay Order related information</div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                                    {!! Form::label('challan_no','Pay Order No : ',['class'=>'col-md-5 font-ok ']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('challan_no', (!empty($alreadyExistApplicant->challan_no) ? $alreadyExistApplicant->challan_no : ''),['class'=>'form-control bnEng required input-sm',
                                                        'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                                    {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('bank_name', $banks, (!empty($alreadyExistApplicant->bank_name) ? $alreadyExistApplicant->bank_name : ''), ['class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                                    {!! Form::label('amount','Amount : ',['class'=>'col-md-5 font-ok']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('amount',(!empty($alreadyExistApplicant->challan_amount) ? $alreadyExistApplicant->challan_amount : ''), ['class'=>'form-control bnEng required input-sm','placeholder'=>'5000',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                    {!! Form::label('date','Date :',['class'=>'col-md-5 font-ok']) !!}
                                                    <div class="col-md-7">

                                                        {!! Form::text('date',(!empty($alreadyExistApplicant->challan_date) ? App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->challan_date, 0, 10)) : ''), ['class'=>'form-control required input-sm','placeholder'=>'Date',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                    {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok ']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('branch',(!empty($alreadyExistApplicant->challan_branch) ? $alreadyExistApplicant->challan_branch : ''), ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                @if(isset($form_data->status_id) && $form_data->status_id != 8)
                                                <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                    {!! Form::label('branch','Pay Order Copy :',['class'=>'col-md-5 font-ok ']) !!}
                                                    <div class="col-md-7">
                                                        <a class="btn show-in-view btn-xs btn-danger" href="{{url($alreadyExistApplicant->challan_file)}}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download</a>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary pull-left next">
                                                    <i class="fa fa-chevron-circle-right"></i> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif {{-- application has challan --}}

                            <?php } ?>
                        <fieldset>

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>1. Basic Information</strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-10 {{$errors->has('visa_cat_id') ? 'has-error': ''}}">
                                                {!! Form::label('visa_cat_id','Type of Visa Required for the Incumbent Foreign Nationals :',
                                                ['class'=>'col-md-6 text-left required-star']) !!}
                                                <div class="col-md-4">
                                                    {!! Form::select('visa_cat_id', $visa_types, (!empty($alreadyExistApplicant->visa_cat_id) ? $alreadyExistApplicant->visa_cat_id : ''),
                                                    ['data-rule-maxlength'=>'64', 'class' => 'form-control input-sm required',
                                                    'placeholder'=>'Select One']) !!}
                                                    {!! $errors->first('passport_types','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <span onmouseover="toolTipFunction()" data-toggle="tooltip" class="btn btn-outline btn-sm btn-default"
                                                      title="A3 - Work on Government Projects Visa,
                                                    E - Employment Visa,
                                                    EI - Employment Type -1 Visa,
                                                    P - Player Type Visa,
                                                    PI - Private Investor Visa,
                                                    VoA - Visa on Arrival">
                                                    <i class="fa fa-question-circle"></i> Help Link
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix ">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                {!! Form::label('bdMissionAbroad','Bangladesh mission in abroad where recommendation letter to be sent :',
                                                ['class'=>'text-left col-md-12']) !!}
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group clearfix">
                                        <div class="row">
                                                <div class="col-md-6 {{$errors->has('bd_mission_country') ? 'has-error': ''}}">
                                                    {!! Form::label('bd_mission_country','Country :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('bd_mission_country', $countriesWithoutBD, (!empty($alreadyExistApplicant->bd_mission_country) ?
                                                        $alreadyExistApplicant->bd_mission_country : ''), ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required',
                                                        'placeholder' => 'Select One']) !!}
                                                        {!! $errors->first('bd_mission_country','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('embassies') ? 'has-error': ''}}">
                                                    {!! Form::label('embassies','Embassy / High Commission :',['class'=>'col-md-6 text-left required-star']) !!}
                                                    <div class="col-md-6">
                                                        {!! Form::select('embassies', $high_comissions, (!empty($alreadyExistApplicant->embassies) ? $alreadyExistApplicant->embassies : ''), ['data-rule-maxlength'=>'64', 'placeholder' => 'Select One',
                                                        'class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('embassies','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group clearfix" id="clr_approval_no_div">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('clr_approval_no') ? 'has-error': ''}}">
                                                {!! Form::label('clr_approval_no','Project Clearance Approval Number :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                   &nbsp; <span>{{ (!empty($pc_track_no) ? $pc_track_no : 'Not found!') }}</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading"> <strong>2. Particulars of Sponsors / Employers</strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix image-outside-div">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('applicant_name') ? 'has-error': ''}}">
                                                {!! Form::label('applicant_name','1. Name of the Applicant / Applying Firm or Company :',['class'=>'col-md-5 text-left']) !!}
                                                <div class="col-md-7">
                                                    <span>{{ (!empty($pc_applicant_name) ? $pc_applicant_name : 'N/A') }}</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 image-responsive">
                                                <div class="col-md-5 text-left"><strong class="required-star">Profile Picture :</strong><br/>
                                                    <span class="text-danger" style="font-size: 9px; font-weight: bold">
                                                        [File Format: *.jpg/ .jpeg / .png | File size within 3 MB]</span></div>
                                                <div class="col-md-7 profilePicArea">
                                                    <?php
                                                        $fileUrl = (!empty($alreadyExistApplicant->applicant_pic) ? $alreadyExistApplicant->applicant_pic : '');
                                                        $requiredClass = '';
                                                        if(!file_exists($fileUrl)){
                                                            $requiredClass = 'required';
                                                        }
                                                    ?>
                                                    {!! Form::file('applicant_pic', ['class' => $requiredClass, 'style'=>'border:0px !important;','id'=>'applicant_pic',
                                                    'onchange' => 'imageDisplay(this)']) !!}
                                                    {!! Html::image((empty($requiredClass) ? $alreadyExistApplicant->applicant_pic : 'assets/images/avatar5.png'),
                                                    'Picture', [ 'id'=>'profile_image', 'class'=>'img-responsive image-style']) !!}
                                                    @if (Session::has('error_message'))
                                                        <div class="text-danger">{{ Session::get('error_message') }}</div>
                                                    @endif
                                                    <div id="upload_error" class="text-danger"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('eco_zone_id') ? 'has-error': ''}}">
                                                {!! Form::label('eco_zone_id','Name of Economic Zone :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                        {{ ( !empty($economicZone)) ? $economicZone : '' }}
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix ">
                                        <div class="row">
                                            <div class="col-md-9">
                                                {!! Form::label('infrastructureReq',' 2. Full Address of Registered Head Office of Applicant / Applying Firm or Company :',
                                                ['class'=>'text-left col-md-10']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('country') ? 'has-error': ''}}">
                                                {!! Form::label('country','Country :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('country', $countries, (!empty($alreadyExistApplicant->country) ? $alreadyExistApplicant->country : '001'),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                    {!! $errors->first('country','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('division') ? 'has-error': ''}}" id="division_div">
                                                {!! Form::label('division','Division :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('division', $divition_eng, (!empty($alreadyExistApplicant->division) ? $alreadyExistApplicant->division : '1'),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                    {!! $errors->first('division','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                                <div class="col-md-6 has-feedback {{ $errors->has('state') ? 'has-error' : ''}} hidden" id="state_div">
                                                {!! Form::label('state','State :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('state', (!empty($alreadyExistApplicant->state) ? $alreadyExistApplicant->state : ''), $attributes = array('class'=>'form-control input-sm required', 'placeholder' => 'Name of your state / division',
                                                    'data-rule-maxlength'=>'40', 'id'=>"state")) !!}
                                                    {!! $errors->first('state','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('district') ? 'has-error': ''}}"  id="district_div">
                                                {!! Form::label('district','District :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('district', $district_eng, (!empty($alreadyExistApplicant->district) ? $alreadyExistApplicant->district : '2'),
                                                    ['class' => 'form-control input-sm required','placeholder' => 'Select division first!']) !!}
                                                    {!! $errors->first('district','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 has-feedback {{ $errors->has('province') ? 'has-error' : ''}} hidden" id="province_div">
                                                {!! Form::label('province','Province :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('province', (!empty($alreadyExistApplicant->province) ? $alreadyExistApplicant->province : null),
                                                    $attributes = array('class'=>'form-control input-sm required', 'data-rule-maxlength'=>'40','placeholder' => 'Enter your Province', 'id'=>"province")) !!}
                                                    {!! $errors->first('province','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('road_no','Address Line 1 :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('road_no',(!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : null),
                                                    ['data-rule-maxlength'=>'80','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('house_no') ? 'has-error': ''}}">
                                                {!! Form::label('house_no','Address Line 2 :', ['class'=>'col-md-5 text-left']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('house_no',(!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : null ),
                                                    ['data-rule-maxlength'=>'80','class' => 'form-control input-sm']) !!}
                                                    {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('post_code','Post Code :',['class'=>'col-md-5 text-left']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('post_code',(!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : null), ['data-rule-maxlength'=>'20',
                                                    'class' => 'form-control input-sm nocomma']) !!}
                                                    {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('phone') ? 'has-error': ''}}">
                                                {!! Form::label('phone','Phone No :',['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('phone', (!empty($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : null), 
                                                    ['data-rule-maxlength'=>'20', 'class' => 'form-control phone input-sm required onlyNumber nocomma']) !!}
                                                    {!! $errors->first('phone','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('fax','Fax No :',['class'=>'text-left col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('fax', (!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : null), ['data-rule-maxlength'=>'20',
                                                    'class' => 'form-control input-sm number onlyNumber nocomma']) !!}
                                                    {!! $errors->first('fax','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('email') ? 'has-error': ''}}">
                                                {!! Form::label('email','Email :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('email',(!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : null),
                                                    ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                                    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('website','Website :',['class'=>'text-left col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('website',(!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : null),
                                                    ['data-rule-maxlength'=>'100','class' => 'form-control input-sm', 'placeholder'=> 'https://www.example.com']) !!}
                                                    {!! $errors->first('website','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->





















                            <div class="panel panel-primary">
                                    <div class="panel-heading">
                                         <strong style="display: inline-block;width:60%;">3. Authorized Information</strong>
                                        <span class="text-right" id="full_same_as_authorized" style=" width: 38%;display: inline-block;">
                                            {!! Form::checkbox('same_as_authorized','Yes',(empty($alreadyExistApplicant->same_as_authorized) ? false : true),
                                            ['class' => 'text-left','onclick'=>'editAuthorizeInfo(this)','id'=>'same_as_authorized']) !!}
                                            {!! Form::label('same_as_authorized','Same as Authorized Person',['class'=>'text-left','style'=>'font-size:13px;']) !!}
                                        </span>
                                    </div>
                            <div class="panel-body">


                                    <div id="first_step_authorize_info">

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_name') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_name','Name of the Correspondent Applicant Name :',['class'=>'required-star col-md-12']) !!}
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_name') ? 'has-error': ''}}">
                                                    <div class="col-md-5">&nbsp;</div>
                                                    <div class="col-md-7">
                                                        <?php $authCorrespondent_name = (isset($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : ''); ?>
                                                        {!! Form::text('correspondent_name',(isset($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name),
                                                        ['maxlength'=>'64', 'class' => 'form-control input-sm required textOnly','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_name','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_nationality') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_nationality','Nationality :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authNationality = (!empty($logged_user_info->nationality) ? $logged_user_info->nationality : ''); ?>
                                                        {!! Form::select('correspondent_nationality', $nationality,
                                                        (!empty($alreadyExistApplicant->correspondent_nationality) ? $alreadyExistApplicant->correspondent_nationality : $authNationality),
                                                        ['class' => 'form-control input-sm required','readonly'=>true, 'placeholder' => 'Select One']) !!}
                                                        {!! $errors->first('correspondent_nationality','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_passport') ? 'has-error': ''}}" id="correspondent_passport_div">
                                                    {!! Form::label('correspondent_passport','Passport :',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authPassport_no = (!empty($logged_user_info->passport_no) ? $logged_user_info->passport_no : ''); ?>
                                                        {!! Form::text('correspondent_passport',
                                                        (!empty($alreadyExistApplicant->correspondent_passport) ? $alreadyExistApplicant->correspondent_passport : $authPassport_no),
                                                        ['data-rule-maxlength'=>'64', 'class' => 'form-control input-sm','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_passport','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class=" col-md-12 ">
                                                    {!! Form::label('infrastructureReq','4. Correspondent Address & Contact Details :', ['class'=>'text-left col-md-12']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_country') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_country','Country :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authCountry = (!empty($logged_user_info->country) ? $logged_user_info->country : ''); ?>
                                                        {!! Form::select('correspondent_country', $countries,
                                                        (!empty($alreadyExistApplicant->correspondent_country) ? $alreadyExistApplicant->correspondent_country : $authCountry),
                                                        ['class' => 'form-control input-sm required','placeholder' => 'Select One', 'readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_country','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_division') ? 'has-error': ''}} hidden" id="correspondent_division_div">
                                                    {!! Form::label('correspondent_division','Division :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authDivision = (!empty($logged_user_info->division) ? $logged_user_info->division : ''); ?>
                                                        {!! Form::select('correspondent_division', $divition_eng,
                                                        (!empty($alreadyExistApplicant->correspondent_division) ? $alreadyExistApplicant->correspondent_division : $authDivision),
                                                        ['id' => 'correspondent_division','class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_division','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 has-feedback {{ $errors->has('correspondent_state') ? 'has-error' : ''}}" id="correspondent_state_div">
                                                    {!! Form::label('correspondent_state','State :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authState = (!empty($logged_user_info->state) ? $logged_user_info->state : ''); ?>
                                                        {!! Form::text('correspondent_state', (!empty($alreadyExistApplicant->correspondent_state) ? $alreadyExistApplicant->correspondent_state : $authState), $attributes = array('class'=>'form-control input-sm','readonly'=>true,
                                                        'placeholder' => 'Name of your state / division', 'data-rule-maxlength'=>'40', 'id'=>"correspondent_state")) !!}
                                                        {!! $errors->first('correspondent_state','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_district') ? 'has-error': ''}} hidden" id="correspondent_district_div">
                                                    {!! Form::label('correspondent_district','District :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authDistrict = (!empty($logged_user_info->district) ? $logged_user_info->district : ''); ?>
                                                        {!! Form::select('correspondent_district', $district_eng,
                                                        (!empty($alreadyExistApplicant->correspondent_district) ? $alreadyExistApplicant->correspondent_district : $authDistrict),
                                                        ['class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_district','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 has-feedback {{ $errors->has('correspondent_province') ? 'has-error' : ''}}" id="correspondent_province_div">
                                                    {!! Form::label('correspondent_province','Province :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authProvince = (!empty($logged_user_info->province) ? $logged_user_info->province : ''); ?>
                                                        {!! Form::text('correspondent_province', (!empty($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince),
                                                        $attributes = array('class'=>'form-control input-sm', 'data-rule-maxlength'=>'40', 'placeholder' => 'Enter the name of your Province',
                                                        'readonly'=>true,'id'=>"correspondent_province")) !!}
                                                        {!! $errors->first('correspondent_province','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6  {{$errors->has('correspondent_road_no') ? 'has-error': ''}}">
                                                    <?php $authRoad_no = (!empty($logged_user_info->road_no) ? $logged_user_info->road_no : ''); ?>
                                                    {!! Form::label('correspondent_road_no','Address Line 1 :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('correspondent_road_no',(!empty($alreadyExistApplicant->correspondent_road_no) ? $alreadyExistApplicant->correspondent_road_no : $authRoad_no), ['data-rule-maxlength'=>'80',
                                                        'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_road_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_house_no') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_house_no','Address Line 2 :', ['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authHouse_no = (!empty($logged_user_info->house_no) ? $logged_user_info->house_no : ''); ?>
                                                        {!! Form::text('correspondent_house_no',(!empty($alreadyExistApplicant->correspondent_house_no) ? $alreadyExistApplicant->correspondent_house_no : $authHouse_no), ['data-rule-maxlength'=>'80',
                                                        'class' => 'form-control input-sm','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_house_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6  {{$errors->has('correspondent_post_code') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_post_code','Post Code :',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authPost_code = (!empty($logged_user_info->post_code) ? $logged_user_info->post_code : ''); ?>
                                                        {!! Form::text('correspondent_post_code',(!empty($alreadyExistApplicant->correspondent_post_code) ? $alreadyExistApplicant->correspondent_post_code : $authPost_code),
                                                        ['data-rule-maxlength'=>'20', 'class' => 'form-control input-sm nocomma','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_post_code','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('phone') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_phone','Phone No :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authUser_phone = (!empty($logged_user_info->user_phone) ? $logged_user_info->user_phone : ''); ?>
                                                        {!! Form::text('correspondent_phone',(!empty($alreadyExistApplicant->correspondent_phone) ? $alreadyExistApplicant->correspondent_phone : $authUser_phone),
                                                        ['data-rule-maxlength'=>'20', 'class' => 'form-control phone input-sm required onlyNumber nocomma','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_phone','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_fax') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_fax','Fax No :',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authUser_fax = (!empty($logged_user_info->user_fax) ? $logged_user_info->user_fax : ''); ?>
                                                        {!! Form::text('correspondent_fax',(!empty($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax), ['data-rule-maxlength'=>'20',
                                                        'class' => 'form-control input-sm number onlyNumber nocomma','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_fax','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('correspondent_email') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_email','Email :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authUser_email = (!empty($logged_user_info->user_email) ? $logged_user_info->user_email : ''); ?>
                                                        {!! Form::text('correspondent_email',(!empty($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email),
                                                        ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email','readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_email','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 {{$errors->has('correspondent_website') ? 'has-error': ''}}">
                                                    {!! Form::label('correspondent_website','Website :',['class'=>'text-left col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <?php $authWebsite = (!empty($logged_user_info->website) ? $logged_user_info->website : ''); ?>
                                                        {!! Form::text('correspondent_website',(!empty($alreadyExistApplicant->correspondent_website) ? 
                                                        $alreadyExistApplicant->correspondent_website : $authWebsite), ['data-rule-maxlength'=>'100',
                                                        'class' => 'form-control input-sm', 'placeholder'=> 'https://www.example.com', 'readonly'=>true]) !!}
                                                        {!! $errors->first('correspondent_website','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->

                        </fieldset>

                        <h3 class="text-center hiddenDiv">Applicant Details (Part B)</h3>
                        <fieldset>
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>4. Particulars of Foreign Incumbent </strong></div>
                                <div class="panel-body">

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_national_name') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_national_name','Name of the foreign national :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_national_name', (!empty($alreadyExistApplicant->incumbent_national_name) ? $alreadyExistApplicant->incumbent_national_name : ''),
                                                    ['class' => 'form-control textOnly input-sm required']) !!}
                                                    {!! $errors->first('incumbent_national_name','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="col-md-6 {{$errors->has('incumbent_designation') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_designation','Incumbent Designation :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_designation', (!empty($alreadyExistApplicant->incumbent_designation) ? $alreadyExistApplicant->incumbent_designation : ''),
                                                    ['class' => 'form-control textOnly input-sm required']) !!}
                                                    {!! $errors->first('incumbent_designation','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{ $errors->has('incumbent_nationality') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_nationality','Nationality :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('incumbent_nationality', $nationalityWithoutBD, (!empty($alreadyExistApplicant->incumbent_nationality) ? $alreadyExistApplicant->incumbent_nationality : ''),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                    {!! $errors->first('incumbent_nationality','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 {{$errors->has('incumbent_gender') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_gender','Gender :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    <?php
                                                    $male= true;
                                                    $female='';
                                                    if (!empty($alreadyExistApplicant)) {
                                                        if ($alreadyExistApplicant->incumbent_gender == 'Male') {
                                                            $male = true;
                                                        } else {
                                                            $male = false;
                                                        }
                                                        if ($alreadyExistApplicant->incumbent_gender == 'Female') {
                                                            $female = true;
                                                        } else {
                                                            $female = false;
                                                        }
                                                    }
                                                    ?>
                                                    <label class="radio-inline">{!! Form::radio('incumbent_gender', 'Male', $male, ['class'=>'required', 'required'=>'yes']) !!} Male</label>
                                                    <label class="radio-inline">{!! Form::radio('incumbent_gender', 'Female', $female, ['class'=>'required','required'=>'yes']) !!} Female</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_passport') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_passport','Passport No. :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_passport', (!empty($alreadyExistApplicant->incumbent_passport) ? $alreadyExistApplicant->incumbent_passport : ''), ['class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('incumbent_passport','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 {{$errors->has('incumbent_pass_expire') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_pass_expire','Expiry Date :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    <div class="col-md-12 input-group datepicker-future">
                                                    {!! Form::text('incumbent_pass_expire', (!empty($alreadyExistApplicant->incumbent_pass_expire) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_expire, 0, 10)) : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon"><span class="fa fa-calendar"></span></span>
                                                    {!! $errors->first('incumbent_pass_expire','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_pass_issue_place') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_pass_issue_place','Place of Issue :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_pass_issue_place', (!empty($alreadyExistApplicant->incumbent_pass_issue_place) ?
                                                    $alreadyExistApplicant->incumbent_pass_issue_place : ''), ['class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('incumbent_pass_issue_place','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 {{$errors->has('incumbent_pass_issue_date') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_pass_issue_date','Date of Issue :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    <div class="col-md-12 datepicker input-group date">
                                                   {!! Form::text('incumbent_pass_issue_date', (!empty($alreadyExistApplicant->incumbent_pass_issue_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_issue_date, 0, 10)) : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                    <span class="input-group-addon calender-icon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('incumbent_pass_issue_date','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class=" col-md-12 ">
                                                {!! Form::label('incumbentPermanentAddress','Permanent Address', ['class'=>'text-left col-md-12']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_country') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_country','Country :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('incumbent_country',$countriesWithoutBD, (!empty($alreadyExistApplicant->incumbent_country) ?
                                                    $alreadyExistApplicant->incumbent_country : ''),['class' => 'form-control input-sm required','placeholder' => 'Select One']) !!}
                                                    {!! $errors->first('incumbent_country','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 {{$errors->has('incumbent_division') ? 'has-error': ''}} hidden" id="incumbent_division">
                                                {!! Form::label('incumbent_division','Division :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::select('incumbent_division', $divition_eng, (!empty($alreadyExistApplicant->incumbent_division) ?
                                                    $alreadyExistApplicant->incumbent_division : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('incumbent_division','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 has-feedback {{ $errors->has('incumbent_state') ? 'has-error' : ''}}" id="incumbent_state_div">
                                                {!! Form::label('incumbent_state','State :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_state', (!empty($alreadyExistApplicant->incumbent_state) ? $alreadyExistApplicant->incumbent_state : ''),
                                                    $attributes = array('class'=>'form-control input-sm required', 'placeholder' => 'Name of your state / division', 'data-rule-maxlength'=>'40', 'id'=>"incumbent_state")) !!}
                                                    {!! $errors->first('incumbent_state','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                            <div class="row">
                                                <div class="col-md-6 {{$errors->has('incumbent_district') ? 'has-error': ''}} hidden" id="incumbent_district_div">
                                                    {!! Form::label('incumbent_district','District :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('incumbent_district', $district_eng, (!empty($alreadyExistApplicant->incumbent_district) ?
                                                        $alreadyExistApplicant->incumbent_district : ''), ['class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('incumbent_district','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 has-feedback {{ $errors->has('incumbent_province') ? 'has-error' : ''}}" id="incumbent_province_div">
                                                    {!! Form::label('incumbent_province','Province :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('incumbent_province', (!empty($alreadyExistApplicant->incumbent_province) ? $alreadyExistApplicant->incumbent_province : ''),
                                                        $attributes = array('class'=>'form-control input-sm required', 'data-rule-maxlength'=>'40', 'placeholder' => 'Enter the name of your Province',
                                                        'id'=>"incumbent_province")) !!}
                                                        {!! $errors->first('incumbent_province','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>                               
                                                <div class="col-md-6  {{$errors->has('incumbent_road_no') ? 'has-error': ''}}">
                                                    {!! Form::label('incumbent_road_no','Address Line 1 :',['class'=>'text-left required-star col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('incumbent_road_no', (!empty($alreadyExistApplicant->incumbent_road_no) ? 
                                                        $alreadyExistApplicant->incumbent_road_no : ''),['data-rule-maxlength'=>'80','class' => 'form-control input-sm required']) !!}
                                                        {!! $errors->first('incumbent_road_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                   </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                                <div class="col-md-6 {{$errors->has('incumbent_house_no') ? 'has-error': ''}}">
                                                        {!! Form::label('incumbent_house_no','Address Line 2 :', ['class'=>'text-left col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            {!! Form::text('incumbent_house_no',(!empty($alreadyExistApplicant->incumbent_house_no) ? 
                                                            $alreadyExistApplicant->incumbent_house_no : ''),['data-rule-maxlength'=>'80','class' => 'form-control input-sm']) !!}
                                                            {!! $errors->first('incumbent_house_no','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6  {{$errors->has('incumbent_post_code') ? 'has-error': ''}}">
                                                        {!! Form::label('incumbent_post_code','Post Code :',['class'=>'text-left col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            {!! Form::text('incumbent_post_code', (!empty($alreadyExistApplicant->incumbent_post_code) ? $alreadyExistApplicant->incumbent_post_code : ''),
                                                            ['data-rule-maxlength'=>'20','class' => 'form-control input-sm nocomma']) !!}
                                                            {!! $errors->first('incumbent_post_code','<span class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                         </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_phone') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_phone','Phone No :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_phone', (!empty($alreadyExistApplicant->incumbent_phone) ? $alreadyExistApplicant->incumbent_phone : ''),
                                                    ['data-rule-maxlength'=>'20', 'class' => 'form-control phone input-sm required onlyNumber nocomma']) !!}
                                                    {!! $errors->first('incumbent_phone','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 {{$errors->has('incumbent_fax') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_fax','Fax No :',['class'=>'text-left col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_fax', (!empty($alreadyExistApplicant->incumbent_fax) ? $alreadyExistApplicant->incumbent_fax : ''),
                                                    ['data-rule-maxlength'=>'20','class' => 'form-control input-sm number onlyNumber nocomma']) !!}
                                                    {!! $errors->first('incumbent_fax','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_email') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_email','Email :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('incumbent_email', (!empty($alreadyExistApplicant->incumbent_email) ? $alreadyExistApplicant->incumbent_email : ''),
                                                    ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                                    {!! $errors->first('incumbent_email','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 {{ $errors->has('incumbent_dob') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_dob','Date of Birth :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    <div class="col-md-12 datepicker input-group date">
                                                        {!! Form::text('incumbent_dob', (!empty($alreadyExistApplicant->incumbent_dob) ? $alreadyExistApplicant->incumbent_dob : ''),
                                                        ['class' => 'form-control input-sm required']) !!}
                                                        <span class="input-group-addon calender-icon">
                                                        <span class="fa fa-calendar"></span>
                                                        </span>
                                                        {!! $errors->first('incumbent_dob','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <div class="row">
                                            <div class="col-md-6 {{$errors->has('incumbent_martial_status') ? 'has-error': ''}}">
                                                {!! Form::label('incumbent_martial_status','Martial Status :',['class'=>'text-left required-star col-md-5']) !!}
                                                <div class="col-md-7">
                                                    <?php
                                                    $married = true;
                                                    $unmarried = '';
                                                    if (!empty($alreadyExistApplicant)) {
                                                        if ($alreadyExistApplicant->incumbent_martial_status == 'Married') {
                                                            $married = true;
                                                        } else {
                                                            $married = false;
                                                        }
                                                        if ($alreadyExistApplicant->incumbent_martial_status == 'Unmarried') {
                                                            $unmarried = true;
                                                        } else {
                                                            $unmarried = false;
                                                        }
                                                    }
                                                    ?>
                                                    <label class="radio-inline">{!! Form::radio('incumbent_martial_status', 'Married', $married, ['class'=>'required']) !!} 
                                                        Married</label>
                                                    &nbsp;&nbsp;
                                                    <label class="radio-inline">{!! Form::radio('incumbent_martial_status', 'Unmarried', $unmarried, ['class'=>'required']) !!} 
                                                        Unmarried</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="infraReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                <thead class="alert alert-success">
                                                    <tr>
                                                        <th colspan="4" style="font-size: 15px;">Academic Qualification (please attach certificates) :</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Highest Degree <span class="required-star"></span></th>
                                                        <th> College / University <span class="required-star"></span></th>
                                                       <th style="width: 120px !important;"> Result <span class="required-star"></span></th>
                                                        <th> Certificate <span class="required-star"></span><br/> 
                                                            <span class="text-danger text-sm">(PDF | Max file size 3 MB)</span>
                                                            <span onmouseover="toolTipFunction()" data-toggle="tooltip" 
                                                                      title="Attached PDF file (Maximum file size 3MB)!">
                                                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                                </span>                                      
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{!! Form::text('incumbent_degree', (!empty($alreadyExistApplicant->incumbent_degree) ? $alreadyExistApplicant->incumbent_degree : ''),
                                                            ['data-rule-maxlength'=>'40','class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('incumbent_degree','<span class="help-block">:message</span>') !!}
                                                        </td>
                                                        <td>{!! Form::text('incumbent_institute', (!empty($alreadyExistApplicant->incumbent_institute) ? $alreadyExistApplicant->incumbent_institute : ''),
                                                            ['data-rule-maxlength'=>'40','class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('incumbent_institute','<span class="help-block">:message</span>') !!}
                                                        </td>
                                                        <td>{!! Form::text('incumbent_result', (!empty($alreadyExistApplicant->incumbent_result) ? $alreadyExistApplicant->incumbent_result : ''),
                                                            ['data-rule-maxlength'=>'40','class' => 'form-control input-sm required onlyNumber nocomma']) !!}
                                                            {!! $errors->first('incumbent_result','<span class="help-block">:message</span>') !!}
                                                        </td>
                                                        <td>

                                                            <input type="file" size="20" name="incumbent_certificate"   id="incumbent_certificate"
                                                                   class="<?php if(empty($alreadyExistApplicant->incumbent_certificate)) { echo "required"; } ?>"/>
                                                            <span id="incumbent_certificate_error" class="text-danger"></span>
                                                            {!! $errors->first('incumbent_certificate','<span class="help-block">:message</span>') !!}

                                                            @if(!empty($alreadyExistApplicant->incumbent_certificate))
                                                                <div class="save_file">
                                                                    <a target="_blank" class="documentUrl" title="{{$alreadyExistApplicant->incumbent_certificate}}"
                                                                       href="{{URL::to('/'.$alreadyExistApplicant->incumbent_certificate)}}">
                                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php $certificate_name_exp = explode('/',$alreadyExistApplicant->incumbent_certificate); echo end($certificate_name_exp); ?> </a>
                                                                </div>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>5. Compensation and Benefit </strong></div>
                                <div class="panel-body">

                                    <div class="form-group" style="clear:both">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                    <thead class="alert alert-warning">
                                                        <tr>
                                                            <th class="text-center">Salary Structure</th>
                                                            <th class="text-center" colspan="3">Payable Locally</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th class="alert alert-warning text-center">&nbsp</th>
                                                            <th class="alert alert-warning text-center">Payment</th>
                                                            <th class="alert alert-warning text-center">Amount</th>
                                                            <th class="alert alert-warning text-center">Currency</th>
                                                        </tr>
                                                        <tr>
                                                            <th>a. Basic Salary / Honorarium :</th>
                                                            <td>{!! Form::select('salary_pay_method', $payment_method, (!empty($alreadyExistApplicant->salary_pay_method) ? 
                                                                $alreadyExistApplicant->salary_pay_method : ''), ['data-rule-maxlength'=>'40','class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('salary_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('salary_local_amount', (!empty($alreadyExistApplicant->salary_local_amount) ? $alreadyExistApplicant->salary_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('salary_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('salary_local_currency', $currency, (!empty($alreadyExistApplicant->salary_local_currency) ?
                                                                $alreadyExistApplicant->salary_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('salary_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>b. Overseas Allowance :</th>
                                                            <td>{!! Form::select('oversea_pay_method', $payment_method, (!empty($alreadyExistApplicant->oversea_pay_method) ? $alreadyExistApplicant->oversea_pay_method : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('oversea_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('oversea_local_amount', (!empty($alreadyExistApplicant->oversea_local_amount) ? $alreadyExistApplicant->oversea_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('oversea_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('oversea_local_currency', $currency, (!empty($alreadyExistApplicant->oversea_local_currency) ? 
                                                                $alreadyExistApplicant->oversea_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('oversea_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>c. House Rent :</th>
                                                            <td>{!! Form::select('house_pay_method', $payment_method, (!empty($alreadyExistApplicant->house_pay_method) ? $alreadyExistApplicant->house_pay_method : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('house_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('house_local_amount', (!empty($alreadyExistApplicant->house_local_amount) ? $alreadyExistApplicant->house_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('house_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('house_local_currency', $currency, (!empty($alreadyExistApplicant->house_local_currency) ?
                                                                $alreadyExistApplicant->house_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('house_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>d. Conveyance :</th>
                                                            <td>{!! Form::select('conveyance_pay_method', $payment_method, (!empty($alreadyExistApplicant->conveyance_pay_method) ? $alreadyExistApplicant->conveyance_pay_method : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('conveyance_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('conveyance_local_amount', (!empty($alreadyExistApplicant->conveyance_local_amount) ? $alreadyExistApplicant->conveyance_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('conveyance_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('conveyance_local_currency', $currency, (!empty($alreadyExistApplicant->conveyance_local_currency) ?
                                                                $alreadyExistApplicant->conveyance_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('conveyance_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>e. Medical Allowance :</th>
                                                            <td>{!! Form::select('medical_pay_method', $payment_method, (!empty($alreadyExistApplicant->medical_pay_method) ? $alreadyExistApplicant->medical_pay_method : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('medical_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('medical_local_amount', (!empty($alreadyExistApplicant->medical_local_amount) ? $alreadyExistApplicant->medical_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('medical_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('medical_local_currency', $currency, (!empty($alreadyExistApplicant->medical_local_currency) ?
                                                                $alreadyExistApplicant->medical_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('medical_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>f. Entertainment Allowance :</th>
                                                            <td>{!! Form::select('entertain_pay_method', $payment_method, (!empty($alreadyExistApplicant->entertain_pay_method) ? $alreadyExistApplicant->entertain_pay_method : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('entertain_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('entertain_local_amount', (!empty($alreadyExistApplicant->entertain_local_amount) ? $alreadyExistApplicant->entertain_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('entertain_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('entertain_local_currency', $currency, (!empty($alreadyExistApplicant->entertain_local_currency) ? 
                                                                $alreadyExistApplicant->entertain_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('entertain_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        <tr>
                                                            <th>g. Annual Bonus :</th>
                                                            <td>{!! Form::select('bonus_pay_method', $payment_method, (!empty($alreadyExistApplicant->bonus_pay_method) ? $alreadyExistApplicant->bonus_pay_method : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('bonus_pay_method','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('bonus_local_amount', (!empty($alreadyExistApplicant->bonus_local_amount) ? $alreadyExistApplicant->bonus_local_amount : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber']) !!}
                                                                {!! $errors->first('salary_local_amount','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::select('bonus_local_currency', $currency, (!empty($alreadyExistApplicant->bonus_local_currency) ? 
                                                                $alreadyExistApplicant->bonus_local_currency : '107'), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('bonus_local_currency','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>h. Other fringe benefits (if any) :</th>
                                                            <td colspan="2">
                                                                {!! Form::text('fringe_benefits', (!empty($alreadyExistApplicant->fringe_benefits) ? $alreadyExistApplicant->fringe_benefits : ''),
                                                                ['data-rule-maxlength'=>'120','class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('fringe_benefits','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td style="color: grey; font-size: 10px">
                                                                Maximum 120 characters
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>i. Any particular comments or remarks :</th>
                                                            <td colspan="2">
                                                                {!! Form::text('salary_remarks', (!empty($alreadyExistApplicant->salary_remarks) ? $alreadyExistApplicant->salary_remarks : ''), ['data-rule-maxlength'=>'120',
                                                                'class' => 'form-control input-sm required']) !!}
                                                                {!! $errors->first('salary_remarks','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td style="color: grey; font-size: 10px">
                                                                Maximum 120 characters
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group" style="clear:both">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                    <thead class="alert alert-info">
                                                        <tr>
                                                            <th class="text-left" colspan="9"> Manpower of the office</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="manpower">
                                                        <tr>
                                                            <th class="alert alert-success text-center" colspan="3">Local (a)</th>
                                                            <th class="alert alert-success text-center" colspan="3">Foreign (b)</th>
                                                            <th class="alert alert-success text-center" colspan="1">Grand Total</th>
                                                            <th class="alert alert-success text-center" colspan="2">Ratio</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="alert alert-success text-center">Executive</th>
                                                            <th class="alert alert-success text-center">Supporting Staff</th>
                                                            <th class="alert alert-success text-center">Total</th>
                                                            <th class="alert alert-success text-center">Executive</th>
                                                            <th class="alert alert-success text-center">Supporting Staff</th>
                                                            <th class="alert alert-success text-center">Total</th>
                                                            <th class="alert alert-success text-center"> (a+b)</th>
                                                            <th class="alert alert-success text-center">Local</th>
                                                            <th class="alert alert-success text-center">Foreign</th>
                                                        </tr>
                                                        <tr>
                                                            <td>{!! Form::text('local_executive', (!empty($alreadyExistApplicant->local_executive) ? $alreadyExistApplicant->local_executive : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'local_executive']) !!}
                                                                {!! $errors->first('local_executive','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('local_stuff', (!empty($alreadyExistApplicant->local_stuff) ? $alreadyExistApplicant->local_stuff : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'local_stuff']) !!}
                                                                {!! $errors->first('local_stuff','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('local_total_no', (!empty($alreadyExistApplicant->local_total_no) ? $alreadyExistApplicant->local_total_no : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'local_total','readonly']) !!}
                                                                {!! $errors->first('local_total_no','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('foreign_executive', (!empty($alreadyExistApplicant->foreign_executive) ? $alreadyExistApplicant->foreign_executive : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'foreign_executive']) !!}
                                                                {!! $errors->first('foreign_executive','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('foreign_stuff', (!empty($alreadyExistApplicant->foreign_stuff) ? $alreadyExistApplicant->foreign_stuff : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'foreign_stuff']) !!}
                                                                {!! $errors->first('foreign_stuff','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('foreign_total', (!empty($alreadyExistApplicant->foreign_total) ? $alreadyExistApplicant->foreign_total : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'foreign_total','readonly']) !!}
                                                                {!! $errors->first('foreign_total','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('mp_total', (!empty($alreadyExistApplicant->mp_total) ? $alreadyExistApplicant->mp_total : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'mp_total','readonly']) !!}
                                                                {!! $errors->first('mp_total','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('mp_ratio_local', (!empty($alreadyExistApplicant->mp_ratio_local) ? $alreadyExistApplicant->mp_ratio_local : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'mp_ratio_local','readonly']) !!}
                                                                {!! $errors->first('mp_ratio_local','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                            <td>{!! Form::text('mp_ratio_foreign', (!empty($alreadyExistApplicant->mp_ratio_foreign) ? $alreadyExistApplicant->mp_ratio_foreign : ''), ['data-rule-maxlength'=>'40',
                                                                'class' => 'form-control input-sm required onlyNumber','id'=>'mp_ratio_foreign','readonly']) !!}
                                                                {!! $errors->first('mp_ratio_foreign','<span class="help-block">:message</span>') !!}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- /panel-body-->
                            </div><!-- /panel-->
                        </fieldset>

                        <h3 class="text-center hiddenDiv">Attachments (Part C)</h3>
                        <fieldset class="uploadDocuments">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading"> <strong>6. Required Documents for attachment </strong></div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover ">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th colspan="6">Required Attachments</th>
                                                            <th colspan="2">Attached PDF file 
                                                                <span onmouseover="toolTipFunction()" data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 1MB)!">
                                                                    <i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; ?>
                                                        @foreach($document as $row)
                                                        <tr>
                                                            <td><div align="center">{!! $i !!}</div></td>
                                                            <td colspan="6">{!!  $row->doc_name !!} <?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></td>
                                                            <td colspan="2">
                                                                <input name="document_id_<?php echo $row->doc_id; ?>" type="hidden" value="{{(!empty($clrDocuments[$row->doc_id]['doucument_id']) ? $clrDocuments[$row->doc_id]['doucument_id'] : '')}}">
                                                                <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>"
                                                                       name="doc_name_<?php echo $row->doc_id; ?>" />
                                                                <input name="file<?php echo $row->doc_id; ?>" <?php if (empty($clrDocuments[$row->doc_id]['file'])) echo $row->doc_priority == "1" ? "class='required'" : ""; ?>
                                                                       id="file<?php echo $row->doc_id; ?>" type="file" size="20"
                                                                       onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php echo $row->doc_id; ?>', <?php echo $row->doc_priority; ?>)"/>



                                                                @if($row->additional_field == 1)
                                                                    <table>
                                                                        <tr>
                                                                            <td>Other file Name : </td>
                                                                            <td> <input maxlength="64" class="form-control input-sm <?php if ($row->doc_priority == "1"){ echo 'required'; } ?>"
                                                                                        name="other_doc_name_<?php echo $row->doc_id; ?>" id="other_doc_name_<?php echo $row->doc_id; ?>" type="text"
                                                                                        value="{{(!empty($clrDocuments[$row->doc_id]['doc_name']) ?
                                                                                    $clrDocuments[$row->doc_id]['doc_name'] : '')}}"></td>
                                                                        </tr>
                                                                    </table>
                                                                @endif

                                                                @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                                    <div class="save_file saved_file_{{$row->doc_id}}">
                                                                        <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ?
                                                                    $clrDocuments[$row->doc_id]['file'] : ''))}}" title="{{$row->doc_name}}">
                                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name); ?></a>

                                                                        <?php if(!empty($alreadyExistApplicant) && Auth::user()->id == $alreadyExistApplicant->created_by && $viewMode != 'on') {?>
                                                                        <a href="javascript:void(0)" onclick="ConfirmDeleteFile({{ $row->doc_id }})">
                                                                            <span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span>
                                                                        </a>
                                                                        <?php } ?>
                                                                    </div>
                                                                @endif

                                                                <div id="preview_<?php echo $row->doc_id; ?>">
                                                                    <input type="hidden" value="<?php echo !empty($clrDocuments[$row->doc_id]['file']) ?
                                                                            $clrDocuments[$row->doc_id]['file'] : ''?>" id="validate_field_<?php echo $row->doc_id; ?>"
                                                                           name="validate_field_<?php echo $row->doc_id; ?>" class="<?php echo $row->doc_priority == "1" ? "required":'';  ?>"  />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div> <!-- /.table-responsive -->
                                            
                                        <?php if ($viewMode == "on") { ?>                                        
                                        
                                    @if(in_array(Auth::user()->desk_id,array(3,4,5,6)) || Auth::user()->user_type == '1x101')
                                            <div class="row">
                                                <div class="col-md-12"><br/></div>
                                                <div class="col-md-12" style="margin-bottom:6px;">
                                                    <a href="{{ url('project-clearance/view-cer/'. Encryption::encodeId($alreadyExistApplicant->created_by))}}" 
                                                       target="_blank" class="btn btn-warning btn-xs pull-left show-in-view ">
                                                        <i class="fa fa-download"></i> <strong>Associated Certificates of Users</strong>
                                                    </a>
                                                </div>
                                            </div>
                                    @endif {{-- checking if RD desks or system admin --}}
                                        
                                        @include('visaRecommendation::doc-tab')
                                        <?php } ?>
                                            
                                            
                                        </div> <!-- /.panel-body -->
                                    </div><!-- /.panel -->
                            <div class="form-group" style="clear: both"></div>
                        </fieldset>

                        <h3 class="stepHeader">Submit</h3>
                        <fieldset>
                            <div class="panel panel-primary hiddenDiv">
                                <div class="panel-heading"><strong>7. Terms and Conditions</strong></div>
                                <div class="panel-body">
                                    <div class="col-md-12" style="margin: 12px 0;">
                                        <input id="acceptTerms-2" type="checkbox" name="acceptTerms"
                                        <?php if(!empty($alreadyExistApplicant->acceptance_of_terms) && $alreadyExistApplicant->acceptance_of_terms == 1) { ?>
                                        checked <?php } ?> class="required col-md-1 text-left" style="width:3%;">                                         
                                        <label for="acceptTerms-2" class="col-md-11 text-left required-star">I agree with the Terms and Conditions.</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <?php if ($viewMode != 'on') { ?>
                            <div class="col-md-12 draft">
                                <div class="col-md-6">
                                    <?php $statusId = (!empty($alreadyExistApplicant->status_id) ? $alreadyExistApplicant->status_id : '');
                                    if ($statusId == -1 || $statusId == '') {
                                    ?>
                                    <input type="submit" class="btn btn-primary btn-md cancel" value="Save As Draft" name="sv_draft"><?php } else { ?>&nbsp;<?php } ?>
                                </div>
                            </div>
                        {!! Form::close() !!}<!-- /.form end -->
                        <?php } ?>
                    </div>
                    
                        <?php if ($viewMode == "on" && in_array(Auth::user()->user_type, array('1x101', '2x202', '4x404'))) { ?>
                        @include('visaRecommendation::apps_history')
                        <?php } ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer-script')
 @if($viewMode !== 'on')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

<link rel="stylesheet" href="{{ url('assets/css/jquery.steps.css') }}">
<script src="{{ asset("assets/scripts/jquery.steps.js") }}"></script>
@endif
 {{--jQuery code for image upload--}}
 <script type="text/javascript">
         function imageDisplay(input) {
             if (input.files && input.files[0]) {
                 $("#upload_error").html('');
                 var mime_type = input.files[0].type;
                 if(!(mime_type=='image/jpeg' || mime_type=='image/jpg'|| mime_type=='image/png')){
                  $("#upload_error").html("Image format is not valid. Only JPEG or JPG type images are allowed.");
                     return false;
                 }
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $('#profile_image').attr('src', e.target.result);
                 }
                 reader.readAsDataURL(input.files[0]);
             }
         }
 </script>

<script type="text/javascript">
             @if ($viewMode != 'on')
            
             function editAuthorizeInfo() {
                $('#first_step_authorize_info').find('input.form-control,select.form-control').attr('readonly', function (i, v) {
                    //$('#first_step_authorize_info').find('select.form-control').attr('disabled',!v);//select box disabled when checked
                    return !v;
            });
        };

        <?php if(empty($alreadyExistApplicant->same_as_authorized)){ ?>
            $('input[name="same_as_authorized"]').trigger('click');
        <?php } ?>
           
            function uploadDocument(targets, id, vField, isRequired) {
            var inputFile =  $("#" + id).val();
            if(inputFile == ''){
                $("#" + id).html('');
                document.getElementById("isRequired").value = '';
                document.getElementById("selected_file").value = '';
                document.getElementById("validateFieldName").value = '';
                document.getElementById(targets).innerHTML = '<input type="hidden" class="required" value="" id="'+vField+'" name="'+vField+'">';
                if ($('#label_' + id).length) $('#label_' + id).remove();
                return false;
            }

            try{
                document.getElementById("isRequired").value = isRequired;
                document.getElementById("selected_file").value = id;
                document.getElementById("validateFieldName").value = vField;
                document.getElementById(targets).style.color = "red";
                var action = "{{url('/visa-recommend/upload-document')}}";

                $("#" + targets).html('Uploading....');
                var file_data = $("#" + id).prop('files')[0];
                var form_data = new FormData();
                form_data.append('selected_file', id);
                form_data.append('isRequired', isRequired);
                form_data.append('validateFieldName', vField);
                form_data.append('_token', "{{ csrf_token() }}");
                form_data.append(id, file_data);
                $.ajax({
                    target: '#' + targets,
                    url:action,
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        $('#' + targets).html(response);
                        var fileNameArr = inputFile.split("\\");
                        var l = fileNameArr.length;
                        if ($('#label_' + id).length)
                            $('#label_' + id).remove();
                        var doc_id = parseInt(id.substring(4));
                        var newInput = $('<label class="saved_file_'+doc_id+'" id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] +
                                ' <a href="javascript:void(0)" onclick="EmptyFile('+ doc_id 
                                +')"><span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span> </a></b></label>');
                        //var newInput = $('<label id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                        $("#" + id).after(newInput);
                        //check valid data
                        var validate_field = $('#'+vField).val();
                        if(validate_field ==''){
                            document.getElementById(id).value = '';
                        }
                    }
                });
            } catch (err) {
                document.getElementById(targets).innerHTML = "Sorry! Something Wrong.";
            }
        }

           $(document).ready(function () {
           var form = $("#VisaRecForm").show();
                   form.steps({
                   headerTag: "h3",
                           bodyTag: "fieldset",
                           transitionEffect: "slideLeft",
                           onStepChanging: function (event, currentIndex, newIndex) {
                          
                        // Always allow previous action even if the current form is not valid!
                           if (currentIndex > newIndex) {
                           return true;
                           }
                           // Forbid next action on "Warning" step if the user is to young
                           if (newIndex == 1){
                                        var app_image = document.getElementById("applicant_pic");
                                        var file_app_image = app_image.files;
                                        if (file_app_image && file_app_image[0]) {
                                                $("#upload_error").html('');
                                                var mime_type = file_app_image[0].type;
                                                if(!(mime_type=='image/jpeg' || mime_type=='image/jpg'|| mime_type=='image/png')){
                                                 $("#upload_error").html("Image format is not valid. Only JPEG or JPG type images are allowed.");
                                                    return false;
                                                }
                                   }
                           }
                           if (newIndex == 2){
                                        var app_cer = document.getElementById("incumbent_certificate");
                                        var file_app_cer = app_cer.files;
                                        if (file_app_cer && file_app_cer[0]) {
                                                $("#incumbent_certificate_error").html('');
                                                var mime_type = file_app_cer[0].type;
                                                if(!(mime_type=='application/pdf')){
                                                 $("#incumbent_certificate_error").html("File format is not valid. Only PDF file is allowed.");
                                                    return false;
                                                }
                                   }
                           }
                           // Forbid next action on "Warning" step if the user is to young
                           if (newIndex === 3 && Number($("#age-2").val()) < 18){
                           return false;
                           }
                           // Needed in some cases if the user went back (clean up)
                           if (currentIndex < newIndex){
                           // To remove error styles
                           form.find(".body:eq(" + newIndex + ") label.error").remove();
                                   form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                           }
                           form.validate().settings.ignore = ":disabled,:hidden";
//                                   return true;
                                   return form.valid();
                           },
                           onStepChanged: function (event, currentIndex, priorIndex) {
                           // Used to skip the "Warning" step if the user is old enough.
                           /*if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
                            {
                            form.steps("next");
                            }
                            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
                            if (currentIndex === 2 && priorIndex === 3)
                            {
                            form.steps("previous");
                            }*/
                           },
                           onFinishing: function (event, currentIndex) {
                           form.validate().settings.ignore = ":disabled";
                                   return  form.valid();
                           },
                           onFinished: function (event, currentIndex) {
//                           alert("Submitted!");
                           }
           });
           
            var popupWindow = null;
            $('.finish').on('click', function (e) {
                if($('#acceptTerms-2').is(":checked")){
                    $('#acceptTerms-2').removeClass('error');
                    $('#acceptTerms-2').next('label').css('color','black');
                    $('#home').css({"display": "none"});
                    popupWindow = window.open('<?php echo URL::to('/visa-recommend/preview'); ?>', 'Sample', '');
                } else {
                    $('#acceptTerms-2').addClass('error');
                    return false;
                }
            });

//          $("input[type=radio]").click(function () {
//                var id = $(this).attr("id");
//                var thename = $(this).attr("name");
//                $("input[name=" + thename + "]").removeAttr('checked');
//                $("#" + id).attr('checked', "checked");
//          });


           $('#incumbent_certificate').change(function() {
               var file = $('#incumbent_certificate')[0].files[0]
               if (file){
                   $('.incumbent_certificate').html('<b>File: '+file.name+'</b>');
               }
           });

           $("select").change(function () {
               var id = $(this).attr("id");
               var val = $(this).val();
               $(this).find('option').removeAttr("selected");
               if(val !='') {
                   $(this).find('option[value="' + val + '"]').attr('selected', 'selected');
                   $(this).val(val);
               }
           });

            $(".uploadDocuments input[type=file]").change(function () {
                    var fileName = $(this).val();
                   var id = $(this).attr("id");
                   var fileNameArr = fileName.split("\\");
                   var l = fileNameArr.length;
                   if ($('#label_' + id).length)
                   $('#label_' + id).remove();
                   var newInput = $('<br/><label id="label_' + id + '"><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                   $(this).after(newInput);
           });


           $('#manpower').find('input').keyup(function(){
               var local_executive = $('#local_executive').val()?parseFloat($('#local_executive').val()):0;
               var local_stuff = $('#local_stuff').val()?parseFloat($('#local_stuff').val()):0;
               var local_total = parseInt(local_executive+local_stuff);
               $('#local_total').val(local_total);


               var foreign_executive = $('#foreign_executive').val()?parseFloat($('#foreign_executive').val()):0;
               var foreign_stuff = $('#foreign_stuff').val()?parseFloat($('#foreign_stuff').val()):0;
               var foreign_total = parseInt(foreign_executive+foreign_stuff);
               $('#foreign_total').val(foreign_total);

               var mp_total = parseInt(local_total+foreign_total);
               $('#mp_total').val(mp_total);

               var mp_ratio_local = parseFloat(local_total/mp_total);
               var mp_ratio_foreign = parseFloat(foreign_total/mp_total);

               mp_ratio_local = Number((mp_ratio_local).toFixed(3));
               mp_ratio_foreign = Number((mp_ratio_foreign).toFixed(3));

               $('#mp_ratio_local').val(mp_ratio_local);
               $('#mp_ratio_foreign').val(mp_ratio_foreign);
           });

           }); /*end of $(document).ready(function */
           
          $(document).ready(function () {
                   $('.datepicker').datetimepicker({
                   viewMode: 'years',
                   format: 'DD-MMM-YYYY',
                   maxDate: (new Date()),
                   minDate: '01/01/1970'
               });
                    var today = new Date();
                    var yyyy = today.getFullYear();

                   $('.datepicker-future').datetimepicker({
                   viewMode: 'years',
                   format: 'DD-MMM-YYYY',
                   maxDate: '01/01/' + (yyyy + 5),
                   minDate: '01/01/'+yyyy
           });
                   $('#country').change(function (e) {
                    if (this.value == 'BD') { // 001 is country_code of Bangladesh
                    $('#division_div').removeClass('hidden');
                   $('#division').addClass('required');
                   $('#district_div').removeClass('hidden');
                   $('#district').addClass('required');
                   $('#state_div').addClass('hidden');
                   $('#state').removeClass('required');
                   $('#province_div').addClass('hidden');
                   $('#province').removeClass('required');
           }
           else {
           $('#state_div').removeClass('hidden');
                   $('#state').addClass('required');
                   $('#province_div').removeClass('hidden');
                   $('#province').addClass('required');
                   $('#division_div').addClass('hidden');
                   $('#division').removeClass('required');
                   $('#district_div').addClass('hidden');
                   $('#district').removeClass('required');
           }
           });
                   $('#country').trigger('change');
                   $('#correspondent_country').change(function (e) {
           if (this.value == 'BD') { // 001 is country_code of Bangladesh
           $('#correspondent_division_div').removeClass('hidden');
                   $('#correspondent_division').addClass('required');
                   $('#correspondent_district_div').removeClass('hidden');
                   $('#correspondent_district').addClass('required');
                   $('#correspondent_state_div').addClass('hidden');
                   $('#correspondent_state').removeClass('required');
                   $('#correspondent_province_div').addClass('hidden');
                   $('#correspondent_province').removeClass('required');
           }
           else {
           $('#correspondent_state_div').removeClass('hidden');
                   $('#correspondent_state').addClass('required');
                   $('#correspondent_province_div').removeClass('hidden');
                   $('#correspondent_province').addClass('required');
                   $('#correspondent_division_div').addClass('hidden');
                   $('#correspondent_division').removeClass('required');
                   $('#correspondent_district_div').addClass('hidden');
                   $('#correspondent_district').removeClass('required');
           }
           });
                   $('#correspondent_country').trigger('change');
                                      
            $("#division").change(function () {
                var divisionId = $('#division').val();
                $(this).after('<span class="loading_data">Loading...</span>');
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "<?php echo url(); ?>/users/get-district-by-division",
                    data: {
                        divisionId: divisionId
                    },
                    success: function (response) {
                        var option = '<option value="">Select One</option>';
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                        }
                        $("#district").html(option);
                        $(self).next().hide();
                    }
                });
            });
            $("#correspondent_division").change(function () {
                var divisionId = $('#correspondent_division').val();
                $(this).after('<span class="loading_data">Loading...</span>');
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "<?php echo url(); ?>/users/get-district-by-division",
                    data: {
                        divisionId: divisionId
                    },
                    success: function (response) {
                        var option = '<option value="">Select One</option>';
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                        }
                        $("#correspondent_district").html(option);
                        $(self).next().hide();
                    }
                });
            });
                   
                   $("#bd_mission_country").change(function () {
                    var iso = $('#bd_mission_country').val();
                   $(this).after('<span class="loading_data">Loading...</span>');
                   var self = $(this);
                   $.ajax({
                   type: "GET",
                           url: "<?php echo url(); ?>/visa-recommend/get-embassy-by-country",
                           data: {
                           iso: iso
                           },
                           success: function (response) {
                           var option;
                                   if (response.responseCode == 1) {
                           $.each(response.data, function (id, value) {
                           if (value == null) {
                           option += '<option value="">No embassy has been found for this country! </option>';
                           } else {
                           option += '<option value="' + id + '">' + value + '</option>';
                           }
                           });
                           } else {
                           if (iso == '' || iso == null) {
                           option += '<option value="">Select country first! </option>';
                           }
                           else {
                           option += '<option value="">No embassy has been found for this country! </option>';
                           }
                           }
                           $("#embassies").html(option);
                                   $(self).next().hide();
                           }
                   });
           });
                   $("#bd_mission_country").trigger('change');
           });
           @endif

           @if ($viewMode == 'on')
           
        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
            }
            return val;
        }
        
           @if(($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)
                    $(document).ready(function () {
                       var today = new Date();
                       var yyyy = today.getFullYear();
                       var mm = today.getMonth();
                       var dd = today.getDate();
                       $('#user_DOB').datetimepicker({
                           viewMode: 'years',
                           format: 'DD-MMM-YYYY',
                           minDate: '01/01/' + (yyyy - 6)
                       });
                   });
             @endif

            $('#inputForm select').each(function(index){
           var text = $(this).find('option:selected').text();
                   var id = $(this).attr("id");
                   var val = $(this).val();
                   $('#' + id + ' option:selected').replaceWith("<option value='" + val + "' selected>" + text + "</option>");
           });
           $("#inputForm :input[type=text]").each(function(index) {
                    $(this).attr("value", $(this).val());
           });
           $("#inputForm textarea").each(function(index) {
                    $(this).text($(this).val());
           });
                   $("#inputForm select").css({
                   "border" : "none",
                   "background" : "#fff",
                   "pointer-events" : "none",
                   "box-shadow": "none",
                   "-webkit-appearance" : "none",
                   "-moz-appearance" : "none",
                   "appearance": "none"
           });
                   $("#inputForm fieldset").css({"display": "block"});
                   $("#inputForm #full_same_as_authorized").css({"display": "none"});
                   $("#inputForm .actions").css({"display": "none"});
                   $("#inputForm .steps").css({"display": "none"});
                   $("#inputForm .draft").css({"display": "none"});
                   $("#inputForm .title ").css({"display": "none"});
                   //document.getElementById("previewDiv").innerHTML = document.getElementById("projectClearanceForm").innerHTML;

                   $('#inputForm #showPreview').remove();
                   $('#inputForm #save_btn').remove();
                   $('#inputForm #save_draft_btn').remove();
                   $('#inputForm .stepHeader, #inputForm .calender-icon,#inputForm .pss-error,#inputForm .hiddenDiv').remove();
                   $('#inputForm .required-star').removeClass('required-star');
                   $('#inputForm input[type=hidden], #inputForm input[type=file]').remove();
                   $('#inputForm .panel-orange > .panel-heading').css('margin-bottom', '10px');
                   $('#invalidInst').html('');
                   
        $('#inputForm').find('input:not(:checked, :radio),textarea').each(function() {
            if (this.value != ''){
                var displayOp = ''; //display:block
            } else {
                var displayOp = 'display:none';
            }

            if($(this).hasClass("onlyNumber") && !$(this).hasClass("nocomma")){
                var thisVal = commaSeparateNumber(this.value);
                $(this).replaceWith("<span class='onlyNumber " +this.className+ "' style='background-color:#ddd !important; height:auto; margin-bottom:2px; padding:6px;"
                + displayOp + "'>" + thisVal + "</span>");
            }else {
                $(this).replaceWith("<span class='" +this.className+ "' style='background-color:#ddd; height:auto; margin-bottom:2px; padding:6px;"
                + displayOp + "'>" + this.value + "</span>");
            }
                    });
                    $('#inputForm .btn').not('.show-in-view').each(function(){
                        $(this).replaceWith("");
                    });
           
           $('#acceptTerms-2').attr("onclick", 'return false').prop("checked", true).css('margin-left', '5px');

             $('#inputForm').find('input[type=radio]').each(function()
            {
                jQuery(this).attr('disabled', 'disabled');
            });

           $("#inputForm select").replaceWith(function ()
           {
               var selectedText = $(this).find('option:selected').text().trim();
               var displayOp = '';
               if (selectedText != '' && selectedText != 'Select One') {
                   displayOp = 'display:block';
               } else {
                   displayOp = 'display:none';
               }
               return "<span style='background-color:#ddd;width:100%;padding:6px;display:block;" + displayOp + "'>" + selectedText + "</span>";
           });

                   @endif {{-- viewMode is on --}}
        
        function toolTipFunction() {
            $('[data-toggle="tooltip"]').tooltip();
        }

        $(document).on('click', '.download', function (e) {
            var value = $(this).attr('data');
            $('.show_' + value).show();
        });
</script>

 @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(1,2,3,4,5,6)))
     {{--batch process start--}}
     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
     <script language="javascript">

         var numberOfCheckedBox = 0;
         var curr_app_id = '';
         function setCheckBox()
         {
             numberOfCheckedBox = 0;
             var flag = 1;
             var selectedWO = $("input[type=checkbox]").not(".selectall");
             selectedWO.each(function() {
                 if (this.checked)
                 {
                     numberOfCheckedBox++;
                 }
                 else
                 {
                     flag = 0;
                 }
             });
             if (flag == 1)
             {
                 $("#chk_id").checked = true;
             }
             else {
                 $("#chk_id").checked = false;
             }
             if(numberOfCheckedBox >= 1){
                 $('.applicable_status').trigger('click');
             }

         }

         function changeStatus(check)
         {
             $('#status_id').html('<option selected="selected" value="">Select Below</option>');
             setCheckBox();
         }
         $(document).ready(function() {

             $('#status_id').html('<option selected="selected" value="">Select Below</option>');

//            $('.applicable_status').trigger('click');
//            $("#assign_form").validate({
//                errorPlacement: function() {
//                    return false;
//                }
//            });
             $("#apps_from").validate({
                 errorPlacement: function() {
                     return false;
                 }
             });
             $("#batch_from").validate({
                 errorPlacement: function() {
                     return false;
                 }
             });

             var base_checkbox = '.selectall';
             $(base_checkbox).click(function() {
                 if (this.checked) {
                     $('.appCheckBox:checkbox').each(function() {
                         this.checked = true;
                         $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                     });
                 } else {
                     $('.appCheckBox:checkbox').each(function() {
                         this.checked = false;
                         //                            $('#status_id').attr('disabled',false);
                         $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                     });
                 }
                 $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                 setCheckBox();

             });
             $('.appCheckBox:checkbox').not(base_checkbox).click(function() {
                 $(".selectall").prop("checked", false);
             });
             var break_for_pending_verification = 0;
             $(document).ready(function() {

                 $("#status_id").trigger("click");
                 var curr_app_id = $("#curr_app_id").val();
                 var curr_status_id = $("#curr_status_id").val();
                 $.ajaxSetup({async: false});
                 var _token = $('input[name="_token"]').val();
                 var delegate = '{{ @$delegated_desk }}';
                 var state = false;
                 $.post('/visa-recommend/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate, _token: _token}, function(response) {

                     if (response.responseCode == 1) {

                         var option = '';
                         option += '<option selected="selected" value="">Select Below</option>';
                         $.each(response.data, function(id, value) {
                             option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                         });
                         {{--state = !state; --}}
                         {{--$("#status_id").prop("", state ? $("option").length : 1); --}}
                         $("#status_id").html(option);
                         $("#status_id").trigger("change");
                         $("#status_id").focus();
                     } else if (response.responseCode == 5){
                         alert('Without verification, application can not be processed');
                         break_for_pending_verification = 1;
                         option = '<option selected="selected" value="">Select Below</option>';
                         $("#status_id").html(option);
                         $("#status_id").trigger("change");
                         return false;
                     } else {
                         $('#status_id').html('Please wait');
                     }
                 });

                 $.ajaxSetup({async: true});

             });
             $(document).on('change', '.status_id', function() {


                 var curr_app_id_for_process = $("#curr_app_id").val();
                 var curr_status_id_for_process = $("#curr_status_id").val();

                 var object = $(".status_id");
                 var obj = $(object).parent().parent().parent();
                 var id = $(object).val();
                 var _token = $('input[name="_token"]').val();
                 var status_from = $('#status_from').val();
                 $('#sendToDeskOfficer').css('display', 'block');
                 if (id == 0) {
                     obj.find('.param_id').html('<option value="">Select Below</option>');
                 } else {

                     $.post('/visa-recommend/ajax/process', {id: id, curr_app_id: curr_app_id_for_process, status_from: curr_status_id_for_process, _token: _token}, function(response) {
                         console.log(response);
                         if (response.responseCode == 1) {

                             var option = '';
                             option += '<option selected="selected" value="">Select Below</option>';
                             var countDesk = 0;
                             $.each(response.data, function(id, value) {
                                 countDesk++;
                                 option += '<option value="' + id + '">' + value + '</option>';
                             });
                             obj.find('#desk_id').html(option);
                             $('#desk_id').attr("disabled", false);
                             $('#remarks').attr("disabled", false);
                             if (countDesk == 0){
                                 $('.dd_id').removeClass('required');
                                 $('#sendToDeskOfficer').css('display', 'none');
                             } else{
                                 $('.dd_id').addClass('required');
                             }
                             if (response.status_to == 5 || response.status_to == 8 || response.status_to == 24 || response.status_to == 10 || response.status_to == 22){
                                 $('#remarks').addClass('required');
                                 $('#remarks').attr("disabled", false);
                             } else{
                                 $('#remarks').removeClass('required');
                             }

                             if (response.file_attach == 1){
                                 $('#sendToFile').css('display', 'block');
                             }else{
                                 $('#sendToFile').css('display', 'none');
                             }
                         }
                     });
                     //       }
                 }


             });
         });
         function resetElements(){
             $('#status_id').html('<option selected="selected" value="">Select Below</option>');
         }

     </script>
 @endif


@endsection <!--- footer-script--->

