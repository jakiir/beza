@extends('layouts.pdfGen')
@section('content')
@include('partials.messages')



<section class="content">
      <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center">
                        <img src="assets/images/logo_beza_single.png"/><br/>
                        BEZA::Bangladesh Economic Zones Authority<br/>
                        Application for Visa Recommendation
                    </div>
                </div>
                <div class="panel panel-red" id="inputForm">
                    <div class="panel-heading"><b> Application for Visa Recommendation</b></div>
                    <div class="panel-body">

                        <table width="100%">
                            <tr>
                                <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Tracking no. : </strong><span style="font-size: 10px;">{{ $process_data->track_no  }}</span></td>
                                <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Date of Submission: </strong> <span style="font-size: 10px;">{{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }}</span></td>
                                <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Status : </strong> <span style="font-size: 10px;">@if(isset($form_data) && $form_data->status_id == -1) Draft
                                        @else {!! $statusArray[$form_data->status_id] !!}
                                        @endif</span></td>
                                @if($process_data->desk_id != 0)
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Desk :</strong> <span style="font-size: 10px;">{{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}</span></td>
                                @else
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Desk :</strong> <span style="font-size: 10px;">Applicant</span></td>
                                @endif
                            </tr>
                            <tr>
                                <td style="padding:5px; font-size:10px">
                                    <?php if (isset($form_data) && $form_data->status_id == 23 && isset($form_data->certificate)) { ?>
                                    <a href="{{ url($form_data->certificate) }}"
                                       title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="
                                               assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                    <?php } ?>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        @if(!empty($alreadyExistApplicant->challan_no) && !empty($alreadyExistApplicant->bank_name) && !empty($alreadyExistApplicant->challan_amount))
                            <div id="ep_form" class="panel panel-primary">
                                <div class="panel-heading">Pay order related information</div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Pay Order No : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->challan_date) ? App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->challan_date, 0, 10)) : '') }}</span></td>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Date : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->challan_date) ? $alreadyExistApplicant->challan_date : '') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Bank Name : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->bank_name) ? $banks[$alreadyExistApplicant->bank_name] : '') }}</span></td>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Branch Name : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->challan_branch) ? $alreadyExistApplicant->challan_branch : '') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Amount : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->challan_amount) ? $alreadyExistApplicant->challan_amount : '') }}</span></td>
                                            <td style="padding:5px;"><strong style="font-size:10px;">Pay order copy : </strong></td>
                                            <td style="padding:5px;"><span style="font-size:10px;"><a href="{{$alreadyExistApplicant->challan_file}}"><img width="10" height="10" src="assets/images/pdf.png" alt="Pay Order Copy" /> Download</a></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @else
                            {!! '' !!}
                        @endif



                        <fieldset style="display: block;">

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Basic Information</strong></div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td colspan="2" style="font-size: 10px; padding: 5px;"><strong>Type of Visa Required for the Incumbent Foreign Nationals :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($visa_types[$alreadyExistApplicant->visa_cat_id]) ? $visa_types[$alreadyExistApplicant->visa_cat_id] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;">
                                                <strong>Bangladesh mission in abroad where recommendation letter to be sent :</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Country : </strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->bd_mission_country) ? $countries[$alreadyExistApplicant->bd_mission_country] : 'N/A') }}</span></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Embassy / High Commission :</strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->embassies) ? $high_comissions[$alreadyExistApplicant->embassies] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Project Clearance Approval Number :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->clr_approval_no) ? $alreadyExistApplicant->clr_approval_no : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"></td>
                                            <td style="font-size: 10px; padding: 5px;"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>













                            <div class="panel panel-primary">
                                <div class="panel-heading"> <strong>Particulars of Sponsors / Employers</strong></div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>1. Name of the Applicant / Applying Firm or Company :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Profile Picture :</strong></td>
                                            <td rowspan="3" style="font-size: 10px; padding: 5px;">
                                                <?php
                                                $fileUrl = (!empty($alreadyExistApplicant->applicant_pic) ? $alreadyExistApplicant->applicant_pic : 'assets/images/avatar5.png');
                                                ?>
                                                <img width="100" height="100" src="<?php echo $fileUrl; ?>" alt="Profile Pic">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Name of Economic Zone :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($economicZone)?$economicZone:'N/A')}}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;"><strong>2. Full Address of Registered Head Office of Applicant / Applying Firm or Company :</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Country : </strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->country) ? $countries[$alreadyExistApplicant->country] : 'N/A') }}</span></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Division : </strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->division) ? $divition_eng[$alreadyExistApplicant->division] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>District : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->district) ? $district_eng[$alreadyExistApplicant->district] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Address Line 1 :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Address Line 2 :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Post Code : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Phone No : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Fax No : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Email : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Website : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <strong style="display: inline-block;width:60%;">Authorized Information</strong>
                                        <span class="text-right" id="full_same_as_authorized" style="width: 38%; display: none;">
                                            <span style="background-color:#ddd;width:100%;display:block;margin:2px;padding:6px;display:block">Yes</span>
                                            <label for="same_as_authorized" class="text-left" style="font-size:13px;" onclick="editAuthorizeInfo(this)">Same as Authorized Person</label>
                                        </span>
                                </div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td colspan="2" style="font-size: 10px; padding: 5px;"><strong>3. Name of the Correspondent Applicant Name :</strong></td>
                                            <?php $authCorrespondent_name = (!empty($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : 'Bangladeshi'); ?>
                                            <td colspan="2" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name) }}</span></td>
                                        </tr> 
                                        <tr>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Nationality : </strong></td>
                                            <?php $authNationality = (!empty($logged_user_info->nationality && $logged_user_info->nationality != 'BD') ? $nationality[$logged_user_info->nationality] : 'N/A'); ?>
                                            
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($nationality[$alreadyExistApplicant->correspondent_nationality]) ? $nationality[$alreadyExistApplicant->correspondent_nationality] : $authNationality) }}</span></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Passport : </strong></td> 
                                            <?php $authPassport_no = (!empty($logged_user_info->passport_no) ? $logged_user_info->passport_no : 'N/A'); ?>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_passport) ? $alreadyExistApplicant->correspondent_passport : $authPassport_no) }}</span></td>
                                        </tr> 
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;"><strong>4. Correspondent Address & Contact Details :</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Country : </strong></td>
                                            <?php $authCountry = (!empty($logged_user_info->country) ? $countries[$logged_user_info->country] : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_country) ? $countries[$alreadyExistApplicant->correspondent_country] : $authCountry) }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>State : </strong></td>
                                            <?php $authState = (!empty($logged_user_info->state) ? $logged_user_info->state : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_state) ? $alreadyExistApplicant->correspondent_state : $authState) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Province : </strong></td>
                                            <?php $authProvince = (!empty($logged_user_info->province) ? $logged_user_info->province : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince) }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Address Line 1 :</strong></td>
                                            <?php $authRoad_no = (!empty($logged_user_info->road_no) ? $logged_user_info->road_no : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_road_no) ? $alreadyExistApplicant->correspondent_road_no : $authRoad_no) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Address Line 2 : </strong></td>
                                            <?php $authHouse_no = (!empty($logged_user_info->house_no) ? $logged_user_info->house_no : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_house_no) ? $alreadyExistApplicant->correspondent_house_no : $authHouse_no) }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Post Code : </strong></td>
                                            <?php $authPost_code = (!empty($logged_user_info->post_code) ? $logged_user_info->post_code : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_post_code) ? $alreadyExistApplicant->correspondent_post_code : $authPost_code) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Phone No : </strong></td>
                                            <?php $authUser_phone = (!empty($logged_user_info->user_phone) ? $logged_user_info->user_phone : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_phone) ? $alreadyExistApplicant->correspondent_phone : $authUser_phone) }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Fax No : </strong></td>
                                            <?php $authUser_fax = (!empty($logged_user_info->user_fax) ? $logged_user_info->user_fax : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Eamil : </strong></td>
                                            <?php $authUser_email = (!empty($logged_user_info->user_email) ? $logged_user_info->user_email : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email) }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Website : </strong></td>
                                            <?php $authWebsite = (!empty($logged_user_info->website) ? $logged_user_info->website : 'N/A'); ?>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_website) ?
                                                        $alreadyExistApplicant->correspondent_website : $authWebsite) }}</span></td>
                                        </tr>
                                    </table>
                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->

                        </fieldset>


                        <fieldset style="display: block;"> 
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Particulars of Foreign Incumbent </strong></div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Name of the foreign national :</strong></td>
                                            <td colspan="2" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_national_name) ? $alreadyExistApplicant->incumbent_national_name : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Nationality : </strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($nationality[$alreadyExistApplicant->incumbent_nationality]) ? $nationality[$alreadyExistApplicant->incumbent_nationality] : 'N/A') }}</span></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><strong>Gender : </strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_gender) ? $alreadyExistApplicant->incumbent_gender : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Passport No : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_passport) ? $alreadyExistApplicant->incumbent_passport : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Expiry Date : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_expire) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_expire, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Place of Issue : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_issue_place) ? $alreadyExistApplicant->incumbent_pass_issue_place : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Date of Issue : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_issue_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_issue_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;"><strong>Permanent Address</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Country : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_country) ?
                                                    $countries[$alreadyExistApplicant->incumbent_country] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>State : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_state) ? $alreadyExistApplicant->incumbent_state : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Province : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_province) ? $alreadyExistApplicant->incumbent_province : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Address Line 1 : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_road_no) ?
                                                        $alreadyExistApplicant->incumbent_road_no : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Address Line 2 : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_house_no) ?
                                                            $alreadyExistApplicant->incumbent_house_no : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Post Code : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_post_code) ? $alreadyExistApplicant->incumbent_post_code : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Phone No : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_phone) ? $alreadyExistApplicant->incumbent_phone : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Fax No : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_fax) ? $alreadyExistApplicant->incumbent_fax : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Eamil : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_email) ? $alreadyExistApplicant->incumbent_email : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Date of Birth : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_dob) ? $alreadyExistApplicant->incumbent_dob : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Marital Status : </strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_martial_status)?$alreadyExistApplicant->incumbent_martial_status:'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"></td>
                                            <td style="font-size: 10px; padding: 5px;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;"><strong>Academic Qualification (please attach certificates) :</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;">
                                                <table width="100%" class="table-bordered">
                                                    <tr class="alert alert-success">
                                                        <td style="font-size: 10px; padding: 5px;"><strong>Highest Degree</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;"><strong>College / University</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;"><strong>Result</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;"><strong>Certificate
                                                                (upload max file size 3 MB)</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_degree) ? $alreadyExistApplicant->incumbent_degree : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_institute) ? $alreadyExistApplicant->incumbent_institute : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_result) ? $alreadyExistApplicant->incumbent_result : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;"><span><?php if(!empty($alreadyExistApplicant->incumbent_certificate)){$certificate_name_exp = explode('/',$alreadyExistApplicant->incumbent_certificate); echo end($certificate_name_exp);}else{echo "N/A";} ?></span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Compensation and Benefit </strong></div>
                                <div class="panel-body">
                                    <table width="100%" cellpadding="10">
                                        <tr class="alert alert-warning">
                                            <td style="font-size: 10px; padding: 5px;"><strong>Salary Structure</strong></td>
                                            <td style="font-size: 10px; padding: 5px;"><strong>Payable Locally</strong></td>
                                        </tr>
                                        <tr class="alert alert-warning">
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong></strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><strong>Payment</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><strong>Amount</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><strong>Currency</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>a. Basic Salary / Honorarium :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->salary_pay_method) ? $payment_method[$alreadyExistApplicant->salary_pay_method] : '') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->salary_local_amount) ? $alreadyExistApplicant->salary_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->salary_local_currency]) ? $currency[$alreadyExistApplicant->salary_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>b. Overseas Allowance :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->oversea_pay_method) ? $payment_method[$alreadyExistApplicant->oversea_pay_method] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->oversea_local_amount) ? $alreadyExistApplicant->oversea_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->oversea_local_currency]) ? $currency[$alreadyExistApplicant->oversea_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>c. House Rent :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->house_pay_method) ? $payment_method[$alreadyExistApplicant->house_pay_method] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->house_local_amount) ? $alreadyExistApplicant->house_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->house_local_currency]) ? $currency[$alreadyExistApplicant->house_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>d. Conveyance :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->conveyance_pay_method) ? $payment_method[$alreadyExistApplicant->conveyance_pay_method] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->conveyance_local_amount) ? $alreadyExistApplicant->conveyance_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->conveyance_local_currency]) ? $currency[$alreadyExistApplicant->conveyance_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>e. Medical Allowance :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->medical_pay_method) ? $payment_method[$alreadyExistApplicant->medical_pay_method] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->medical_local_amount) ? $alreadyExistApplicant->medical_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->medical_local_currency]) ? $currency[$alreadyExistApplicant->medical_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>f. Entertainment Allowance :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->entertain_pay_method) ? $payment_method[$alreadyExistApplicant->entertain_pay_method] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->entertain_local_amount) ? $alreadyExistApplicant->entertain_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->entertain_local_currency]) ? $currency[$alreadyExistApplicant->entertain_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;" width="50%"><strong>g. Annual Bonus :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->bonus_pay_method) ? $payment_method[$alreadyExistApplicant->bonus_pay_method] : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="15%"><span>{{ (!empty($alreadyExistApplicant->bonus_local_amount) ? $alreadyExistApplicant->bonus_local_amount : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;" width="30%"><span>{{ (!empty($currency[$alreadyExistApplicant->bonus_local_currency]) ? $currency[$alreadyExistApplicant->bonus_local_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>h. Other fringe benefits (if any) :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" colspan="2"><span>{{ (!empty($alreadyExistApplicant->fringe_benefits) ? $alreadyExistApplicant->fringe_benefits : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><span style="color: gray;">Maximum 120 characters</span></td>

                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px; padding: 5px;"><strong>i. Any particular comments or remarks :</strong></td>
                                            <td style="font-size: 10px; padding: 5px;" colspan="2"><span>{{ (!empty($alreadyExistApplicant->salary_remarks) ? $alreadyExistApplicant->salary_remarks : 'N/A') }}</span></td>
                                            <td style="font-size: 10px; padding: 5px;"><span style="color: gray;">Maximum 120 characters</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="font-size: 10px; padding: 5px;" width="50%">
                                                <table width="100%" class="table-bordered">
                                                    <tr class="alert alert-info">
                                                        <td colspan="9" style="font-size: 10px; padding: 5px;" width="50%"><strong>Manpower of the office</strong></td>
                                                    </tr>
                                                    <tr class="alert alert-success">
                                                        <td style="font-size: 10px; padding: 5px;" width="50%" colspan="3"><strong>Local (a)</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%" colspan="3"><strong>Foreign (b)</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Grand Total</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%" colspan="2"><strong>Ratio</strong></td>
                                                    </tr>
                                                    <tr class="alert alert-success">
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Executive</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Supporting Staff</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Total</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Executive</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Supporting Staff</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Total</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>(a+b)</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Local</strong></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Foreign</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->local_executive) ? $alreadyExistApplicant->local_executive : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->local_stuff) ? $alreadyExistApplicant->local_stuff : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->local_total_no) ? $alreadyExistApplicant->local_total_no : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->foreign_executive) ? $alreadyExistApplicant->foreign_executive : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->foreign_stuff) ? $alreadyExistApplicant->foreign_stuff : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->foreign_total) ? $alreadyExistApplicant->foreign_total : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->mp_total) ? $alreadyExistApplicant->mp_total : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->mp_ratio_local) ? $alreadyExistApplicant->mp_ratio_local : 'N/A') }}</span></td>
                                                        <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ (!empty($alreadyExistApplicant->mp_ratio_foreign) ? $alreadyExistApplicant->mp_ratio_foreign : 'N/A') }}</span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->
                        </fieldset>


                        <fieldset class="uploadDocuments" style="display: block;">
                            <div class="col-md-12" style="padding: 5px;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Required  Documents for attachment</div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <table width="100%" class="table-bordered">
                                                <tr>
                                                    <td style="font-size: 10px; padding: 5px;" width="10%"><strong>No.</strong></td>
                                                    <td style="font-size: 10px; padding: 5px;" width="50%"><strong>Required Attachments</strong></td>
                                                    <td style="font-size: 10px; padding: 5px;" width="40%"><strong>Attached PDF file</strong></td>
                                                </tr>
                                                <?php $i = 1; ?>
                                                @foreach($document as $row)
                                                <tr>
                                                    <td style="font-size: 10px; padding: 5px;" width="10%"><span>{{ $i }}</span></td>
                                                    <td style="font-size: 10px; padding: 5px;" width="50%"><span>{{ $row->doc_name }}</span></td>
                                                    <td style="font-size: 10px; padding: 5px;" width="40%"><span>{{ (!empty($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : 'N/A')}}</span></td>
                                                </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </table>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection