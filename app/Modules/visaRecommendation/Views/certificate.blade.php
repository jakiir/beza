<table border="0" style="text-align: justify;">
    <tr>
        <td colspan="2">
            <p style="font-size:13px !important"><b>File no : {{ $track_no }}</b></p>
        </td>
        <td colspan="1" style="text-align:right">
            <p>Date: {{ $dateNow }}</p>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="padding-top: 50px;">
            <p>Sub: <strong>Request for issuance of visa</strong></p>
        </td>
    </tr>
    <tr><td colspan="3" style="padding-top: 30px;"><strong>Dear Sir/Madam</strong></td></tr>
    <tr>
        <td colspan="3" style="padding-top:15px;">
            <p>&nbsp;&nbsp;&nbsp;&nbsp;With reference to the above mentioned subject, it is to inform you that <strong>{{ $applicantName }} </strong>is a commercial or industrial unit registered with the Bangladesh Economic Zone Authority.
                The Company has requested for visa recommendation for the following expatriate :
            </p>
        </td>
    </tr>

    <tr style="padding-top: 25px;"><td colspan="3"><p>&nbsp;</p></td></tr>
    <tr>
        <td style="width: 200px !important; vertical-align: text-top;"><p>Name : </p></td>
        <td  colspan="2">
            <table><tr><td style="vertical-align: text-top;">:</td><td><strong>&nbsp; {{ $name }}</strong></td></tr></table>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: text-top;"><p>Nationality : </p></td>
        <td  colspan="2">
            <table>
                <tr><td style="vertical-align: text-top;">:</td><td>&nbsp; {{ $nationality }}</td></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: text-top;"><p>Passport No : </p></td>
        <td  colspan="2">
            <table>
                <tr>
                    <td style="vertical-align: text-top;">: </td>
                    <td>&nbsp; {{ $passport }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: text-top;"><p>Designation : </p></td>
        <td  colspan="2">
            <table>
                <tr>
                    <td style="vertical-align: text-top;">: </td>
                    <td>&nbsp; {{ $designation }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: text-top;"><p>Type of Visa required : </p></td>
        <td  colspan="2">
            <table>
                <tr>
                    <td style="vertical-align: text-top;">: </td>
                    <td>&nbsp; {{ $visaType }}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan="3" style="padding-top: 40px;">
            <p>It would be highly appreciated if you would kindly issue the desired in favour of the above-mentioned national
                on proper verification of the papers upon submission of application for the same. </p>
        </td>
    </tr>


    <tr><td colspan="3"><p>&nbsp;</p></td></tr>
    <tr>
        <td colspan="2">
            <p>
                <img src="{{ $url }}" alt="QR code" />
            </p>
            <strong>The High Commissioner/Ambassador</strong>
            <p>High Commission/Embassy of the People's Republic of Bangladesh</p>
            <p>Tokyo, Japan</p>
        </td>
        <td colspan="1" style="text-align: center; vertical-align:top;">
            <p>Yours faithfully,&nbsp;&nbsp;&nbsp;</p><br/>

            <img src="{{ $signature }}" alt="(signature)" width="150px"/>
            <br/>
            <br/>
            Mohammad Ayub<br/>
            Joint Secretary<br/>
            Secretary, BEZA Executive Board<br/>
            Bangladesh Economic Zones Authority (BEZA)<br/>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="padding-top: 20px;">
            <strong>CC to : </strong>
            <p>For kind information and necessary action : </p><br/>
            <p>
                1. Senior Secretary Ministry of Home Affairs, Bangladesh Secretariat, Dhaka<br/>
                2. Director General, Department of Immigration and Passport, Dhaka <br/>
                3. ................................................ <br/>
                4. ................................................ <br/>
                5. ................................................ <br/>
            </p>
        </td>
    </tr>

    <tr>
        <td colspan="3" style="padding-top: 20px;">
            <p>The above mentioned foreign national may be advised to approach in Embassy/High Commission of Bangladesh
            in <strong>{{ $country }}</strong> with the copy of his appointment letter and other related documents for obtaining
            <strong>{{ $visaType }}</strong>. <strong>You are also advised to apply for his/her work permit to this authority
                within {{ $visaDays }} after arrival in Bangladesh.</strong> The company shall be responsible for all action
            of the expatriate during his/her stay in Bangladesh.</p>
        </td>
    </tr>
</table>