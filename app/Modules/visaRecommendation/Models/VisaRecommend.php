<?php

namespace App\Modules\visaRecommendation\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VisaRecommend extends Model {

    protected $table = 'visa_recommendation';
    protected $fillable = array(
        'id',
        'tracking_number',
        'status_id',
        'bill_id',
        'bill_month',
        'payment_status_id',
        'visa_cat_id',
        'bd_mission_country',
        'embassies',
        'clr_approval_no',
        'visit_purpose',
        'applicant_name',
        'eco_zone_id',
        'country',
        'district',
        'division',
        'state',
        'province',
        'road_no',
        'house_no',
        'post_code',
        'phone',
        'fax',
        'email',
        'website',
        'nationality',
        'correspondent_name',
        'correspondent_nationality',
        'correspondent_passport',
        'correspondent_country',
        'correspondent_division',
        'correspondent_district',
        'correspondent_state',
        'correspondent_province',
        'correspondent_road_no',
        'correspondent_house_no',
        'correspondent_post_code',
        'correspondent_phone',
        'correspondent_fax',
        'correspondent_email',
        'correspondent_website',
        'incumbent_national_name',
        'incumbent_designation',
        'incumbent_nationality',
        'incumbent_gender',
        'incumbent_passport',
        'incumbent_pass_expire',
        'incumbent_pass_issue_place',
        'incumbent_pass_issue_date',
        'incumbent_country',
        'incumbent_division',
        'incumbent_state',
        'incumbent_district',
        'incumbent_province',
        'incumbent_road_no',
        'incumbent_house_no',
        'incumbent_post_code',
        'incumbent_email',
        'incumbent_phone',
        'incumbent_fax',
        'incumbent_dob',
        'incumbent_martial_status',        
        'incumbent_degree',
        'incumbent_institute',
        'incumbent_result',
        'incumbent_certificate',
        'salary_pay_method',
        'salary_local_amount',
        'salary_local_currency',
        'oversea_pay_method',
        'oversea_local_amount',
        'oversea_local_currency',
        'house_pay_method',
        'house_local_amount',
        'house_local_currency',
        'conveyance_pay_method',
        'conveyance_local_amount',
        'conveyance_local_currency',
        'medical_pay_method',
        'medical_local_amount',
        'medical_local_currency',
        'entertain_pay_method',
        'entertain_local_amount',
        'entertain_local_currency',
        'bonus_pay_method',
        'bonus_local_amount',
        'bonus_local_currency',
        'fringe_benefits',
        'salary_remarks',
        'local_executive',
        'local_stuff',
        'local_total_no',
        'foreign_executive',
        'foreign_stuff',
        'foreign_total',
        'mp_total',
        'mp_ratio_local',
        'mp_ratio_foreign',
        'sb_gk_verification_status',
        'sb_gk_verification_remarks',
        'nsi_gk_verification_status',
        'nsi_gk_verification_remarks',
        'acceptance_of_terms',
        'challan_no',
        'challan_amount',
        'challan_branch',
        'challan_date',
        'challan_file',
        'bank_name',
        'certificate',
        'is_draft',
        'is_locked',
        'remarks',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }
    function update_method($app_id, $data) {
        DB::table($this->table)
            ->where('id', $app_id)
            ->update($data);
    }

    /*     * *****************************End of Model Class********************************** */
}
