<?php

namespace App\Modules\visaRecommendation\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Modules\Users\Models\Users;

class Apps extends Model {

    protected $table = 'process_list';
    protected $fillable = array(
        'process_id',
        'track_no',
        'company_id',
        'record_id',
        'process_type',
        'initiated_by',
        'closed_by',
        'desk_id',
        'status_id',
        'service_id',
        'process_desc',
        'on_behalf_of_desk',
        'created_at',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->updated_by = 1;
        });

        static::updating(function($post) {
            $post->updated_by = 1;
        });
    }

    /**
     * @return application list
     */
    public static function getClearanceList($search_status_id = 0, $service_id = 3) {
        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        if ($userType == '5x505' || $userType == '6x606') { //Applicant
            $query = Apps::leftJoin('visa_recommendation as vr', 'vr.id', '=', 'process_list.record_id')
                    ->where('process_list.initiated_by', '=', $userId)
                    ->where('process_list.service_id', '=', $service_id)
                    ->groupBy('process_list.process_id')
                    ->orderBy('process_list.updated_at', 'desc');

            return $query->get(['process_list.initiated_by',
                        'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'vr.applicant_name', 'process_list.desk_id',
                        'process_list.status_id', 'process_list.updated_at', 'process_list.company_id',
                        'vr.certificate',
            ]);
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            $query = Apps::leftJoin('visa_recommendation as vr', 'vr.id', '=', 'process_list.record_id')
                    ->whereNotIn('process_list.status_id', [-1, 8, 22])
                    ->where('process_list.service_id', '=', $service_id)
                    ->groupBy('process_list.process_id')
                    ->orderBy('process_list.updated_at', 'desc');

            return $query->get(['process_list.initiated_by',
                        'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'vr.applicant_name', 'process_list.desk_id',
                        'process_list.status_id', 'process_list.updated_at', 'process_list.company_id',
                        'vr.certificate',
            ]);
        } else { //For others desks
            $query = Apps::leftJoin('visa_recommendation as vr', 'vr.id', '=', 'process_list.record_id')
                    ->where('process_list.desk_id', '=', $desk_id)
                    ->where('process_list.service_id', '=', $service_id)
                    ->whereNotIn('process_list.status_id', [-1, 8, 23, 22])
                    ->groupBy('process_list.process_id')
                    ->orderBy('process_list.updated_at', 'desc');
            if ($desk_id == 1 || $desk_id == 2)
                $query->where('process_list.eco_zone_id', Auth::user()->eco_zone_id);

            return $query->get(['process_list.initiated_by',
                        'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'vr.applicant_name', 'process_list.desk_id',
                        'process_list.status_id', 'process_list.updated_at', 'process_list.company_id',
                        'vr.certificate',
            ]);
        }
// else {
//            return [];
//        }
    }

    public static function getDelegationAppList() {
        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $curr_desk_id = CommonFunction::getDeskId();
        $delegated_usersArr = Users::select(DB::raw('group_concat(desk_id) AS delegated_desk'))->where('delegate_to_user_id', $userId)->first();

        $delegated_desk = $delegated_usersArr->delegated_desk;

        $desk_ids = array();
        $desk_id = array('-1');
        if (!empty($delegated_desk)) {
            $desk_id = $delegated_desk;
            $desk_ids = explode(',', $desk_id);
            $desk_id_array = array_unique($desk_ids);
            if (count($desk_id_array) == 1)
                $desk_id = $desk_id_array[0];
        }

        if ($desk_id == 3 || $desk_id == 9) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 4 || $desk_id == 10) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } else {
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
//                            ->where('process_list.status_id', '!=', 11)
                            ->whereIn('process_list.desk_id', explode(',', $delegated_desk))
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        }
    }

    public static function getSearchAppList($tracking_number, $passport_number, $nationality, $organization, $applicant_name) {
        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();

        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.initiated_by', '=', $userId)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 3) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 6) { //Special Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->join('apps_verify_result as avr', function($join) use($userId) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.user_id', '=', DB::raw("$userId"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 4) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 7) { //NSI Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->join('apps_verify_result as avr', function($join) use($userId) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.user_id', '=', DB::raw("$userId"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get([ 'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 2) { //Additional secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 6)
                            ->where('process_list.desk_id', '=', 2)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 1) { //Administrative officer
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->where('process_list.status_id', '!=', 11)
                            ->where('process_list.desk_id', '=', 1)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 8) { //Joint secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->where('process_list.status_id', '!=', 11)
                            ->where('process_list.desk_id', '=', 8)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_id', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

    public static function getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $valid_track_no, $economic_zone) {
        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();

        if ($userType == '5x505' || $userType == '6x606') { //Applicant
            return $query = Apps::leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                    ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                    ->leftJoin('visa_recommendation as wp', 'wp.id', '=', 'process_list.record_id')
                    ->leftJoin('project_clearance as pc', 'pc.created_by', '=', 'wp.created_by')
                    ->where(function($query) {
                        $query->orWhere('wp.status_id', -1)
                        ->orWhere('pc.status_id', 23); // 23 is payment accepted
                    })
                    ->where('process_list.initiated_by', '=', $userId)
                    ->where('process_list.service_id', '=', 3)
                    ->where(function ($query) use ($tracking_number) {
                        if (!empty($tracking_number)) {
                            $query->where('process_list.track_no', '=', $tracking_number);
                        }
                    })
                    ->where(function ($query2) use ($passport_number) {
                        if (!empty($passport_number)) {
                            $query2->where('wp.correspondent_passport', '=', $passport_number);
                        }
                    })->where(function ($query4) use ($applicant_name) {
                        if (!empty($applicant_name)) {
                            $query4->where('wp.applicant_name', '=', $applicant_name);
                        }
                    })->where(function ($query5) use ($status_id) {
                        if (!empty($status_id)) {
                            $query5->where('process_list.status_id', '=', $status_id);
                        }
                    })->where(function ($query6) use ($valid_track_no) {
                        if (!empty($valid_track_no)) {
                            $query6->where('wp.tracking_number', '=', $valid_track_no);
                        }
                    })->where(function ($query7) use ($economic_zone) {
                        if (!empty($economic_zone)) {
                            $query7->where('wp.eco_zone_id', '=', $economic_zone);
                        }
                    })
                    ->where('process_list.service_id', '=', 3)
                    ->where('as1.service_id', '=', 3)
                    ->groupBy('process_list.process_id')
                    ->orderBy('process_list.created_at', 'desc')
                    ->get(['ud.desk_name', 'as1.color', 'wp.certificate', 'wp.applicant_name', 'as1.status_name', 'as1.status_id',
                'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'process_list.updated_at']);
        } else { // all admin
            return $query = Apps::leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                    ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                    ->leftJoin('visa_recommendation as wp', 'wp.id', '=', 'process_list.record_id')
                    ->leftJoin('project_clearance as pc', 'pc.created_by', '=', 'wp.created_by')
                    ->where('pc.status_id', 23) // 23 is payment accepted
                    ->where(function ($query) use ($tracking_number) {
                        if (!empty($tracking_number)) {
                            $query->where('process_list.track_no', '=', $tracking_number);
                        }
                    })
                    ->where(function ($query2) use ($passport_number) {
                        if (!empty($passport_number)) {
                            $query2->where('wp.correspondent_passport', '=', $passport_number);
                        }
                    })
                    ->where(function ($query4) use ($applicant_name) {
                        if (!empty($applicant_name)) {
                            $query4->where('wp.applicant_name', '=', $applicant_name);
                        }
                    })->where(function ($query5) use ($status_id) {
                        if (!empty($status_id)) {
                            $query5->where('process_list.status_id', '=', $status_id);
                        }
                    })->where(function ($query6) use ($valid_track_no) {
                        if (!empty($valid_track_no)) {
                            $query6->where('wp.tracking_number', '=', $valid_track_no);
                        }
                    })->where(function ($query7) use ($economic_zone) {
                        if (!empty($economic_zone)) {
                            $query7->where('pc.eco_zone_id', '=', $economic_zone);
                        }
                    })
                    ->where('process_list.service_id', '=', 3)
                    ->where('process_list.status_id', '!=', -1)
                    ->where('as1.service_id', '=', 3)
                    ->groupBy('process_list.process_id')
                    ->orderBy('process_list.created_at', 'desc')
                    ->get(['ud.desk_name', 'as1.color', 'process_list.process_id', 'wp.certificate', 'wp.applicant_name', 'as1.status_id',
                'process_list.record_id', 'process_list.track_no', 'as1.status_name', 'process_list.updated_at']);
        }
    }

    /*     * *****************************************End of Model Class**************************************************** */
}
