<?php

namespace App\Modules\visaRecommendation\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\apps\Models\docInfo;
use App\Modules\apps\Models\Document;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\apps\Models\Status;
use App\Modules\Company\Models\Company;
use App\Modules\ProcessPath\Models\ProcessHistory;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Settings\Models\VisaCategories;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\Users;
use App\Modules\visaRecommendation\Models\Apps;
use App\Modules\visaRecommendation\Models\VisaRecommend;
use App\Modules\workPermit\Models\Processlist;
use Carbon\Carbon;
use DB;
use Encryption;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use mPDF;
use Session;

class visaRecommendationController extends Controller {

    protected $service_id;

    public function __construct() {
        // Globally declaring service ID
        $this->service_id = 3; // 3 is Visa Recommendation
    }

    public function index() {
        if (!ACL::getAccsessRight('visaRecommend', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $userId = CommonFunction::getUserId();
        $getList['result'] = processlist::where('initiated_by', $userId)->get();
        return view("visaRecommendation::list", ['getList' => $getList]);
    }

    public function listOfApp($ind = 0, $zone = 0) {
        if (!ACL::getAccsessRight('visaRecommend', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            // $search_status_id = base64_decode($search_status_id);
            Session::put('sess_user_id', Auth::user()->id);

            $getList = Apps::getClearanceList();
            $appStatus = Status::where('service_id', $this->service_id)->get();
            $statusList = array();
            foreach ($appStatus as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
                $statusList[$v->status_id . 'color'] = $v->color;
            }
            $statusList[-1] = "Draft";
            $statusList['-1' . 'color'] = '#AA0000';

            $desks = UserDesk::all();
            $deskList = array();
            foreach ($desks as $k => $v) {
                $deskList[$v->desk_id] = $v->desk_name;
            }

//        for advance search
            $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
            $validCompanies = Processlist::leftJoin('project_clearance as pc', 'pc.id', '=', 'process_list.record_id')
                            ->where('process_list.status_id', 23)
                            ->where('pc.status_id', 23)
                            ->orderBy('pc.applicant_name', 'ASC')
                            ->lists('pc.applicant_name', 'pc.tracking_number')->all();
            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');
            $mode = 'V';

            return view("visaRecommendation::list", compact('status', 'deskList', 'companyList', 'statusList', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id', 'mode', 'validCompanies', 'economicZone', 'ind', 'zone'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appForm() {
        if (!ACL::getAccsessRight('visaRecommend', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $statusArr = array(8, 22, '-1'); //8 is Discard, 22 is Rejected Application and -1 is draft
            $alreadyExistApplicant = '';

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();


            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')
                        ->where('id', $projectClearanceData->eco_zone_id)
                        ->pluck('zone');
                $pc_track_no = $projectClearanceData->tracking_number;
                $pc_applicant_name = $projectClearanceData->applicant_name;
            } else {
                $economicZone = '';
                $pc_track_no = '';
                $pc_applicant_name = '';
            }

            $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $countriesWithoutBD = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            $nationalityWithoutBD = Countries::orderby('nationality')->where('nationality', '!=', '')->where('country_code', '!=', '001')->lists('nationality', 'iso');

            $currency = ['' => 'Select One'] + Currencies::orderby('code')->where('is_active', 1)->lists('code', 'id')->all();
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                    ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'iso');
            $visa_types = VisaCategories::where('is_active', 1)->where('is_archived', 0)->orderBy('type', 'asc')->lists('description', 'id');
            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
            } else {
                $clrDocuments = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $viewMode = 'off';
            $mode = 'A';
            return view("visaRecommendation::application-form", compact(
                            'countries', 'countriesWithoutBD', 'divition_eng', 'district_eng', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'nationalityWithoutBD', 'visa_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'projectClearanceData', 'mode', 'pc_track_no', 'pc_applicant_name'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function challanStore($id, Request $request) {
        $app_id = Encryption::decodeId($id);

        $this->validate($request, [
            'challan_no' => 'required',
            'bank_name' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'branch' => 'required',
            'challan_file' => 'required',
        ]);
        try {
            $file = $request->file('challan_file');
            $original_file = $file->getClientOriginalName();
            $file->move('uploads/', time() . $original_file);
            $filename = 'uploads/' . time() . $original_file;

            Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->update([
                'status_id' => 26, ///This is for challan re-submitted
                'desk_id' => 6,
            ]);
            VisaRecommend::find($app_id)->update([
                'challan_no' => $request->get('challan_no'),
                'status_id' => 26, ///This is for challan re-submitted
                'bank_name' => $request->get('bank_name'),
                'challan_amount' => $request->get('amount'),
                'challan_branch' => $request->get('branch'),
                'challan_date' => Carbon::createFromFormat('d-M-Y', $request->get('date'))->format('Y-m-d'),
                'challan_file' => $filename,
                'updated_by' => CommonFunction::getUserId(),
            ]);
            \Session::flash('success', "Pay order information has been successfully updated!");
            return redirect()->back();
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormEdit($id) {
        $app_id = Encryption::decodeId($id);
        if (!ACL::getAccsessRight('visaRecommend', 'E', $app_id)) {
            abort('400',"You have no access right! Please contact with system admin if you have any query.");
        }
        $logged_user_type = CommonFunction::getUserType();

        $authUserId = CommonFunction::getUserId();
        $alreadyExistApplicant = VisaRecommend::where('id', $app_id)->first();
        if ($alreadyExistApplicant) {
            $alreadyExistProcesslist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
            if ($alreadyExistProcesslist) {
                if ($alreadyExistProcesslist->status_id != '-1' && $alreadyExistProcesslist->status_id != 5) {
                    Session::flash('error', "You have no access right! Please contact system administration for more information.");
                    $listUrl = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
                    return redirect($listUrl);
                }
            }
        }

        try {

            $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $countriesWithoutBD = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            $nationalityWithoutBD = Countries::orderby('nationality')->where('nationality', '!=', '')->where('country_code', '!=', '001')->lists('nationality', 'iso');

            $currency = Currencies::orderby('code')->where('is_active', 1)->lists('code', 'id')->all();
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')
                        ->where('id', $projectClearanceData->eco_zone_id)
                        ->pluck('zone');
                $pc_track_no = $projectClearanceData->tracking_number;
                $pc_applicant_name = $projectClearanceData->applicant_name;
            } else {
                $economicZone = '';
                $pc_track_no = '';
                $pc_applicant_name = '';
            }

            $visa_types = VisaCategories::where('is_active', 1)->where('is_archived', 0)->orderBy('type', 'asc')->lists('description', 'id');
            $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                    ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'iso');

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
            } else {
                $clrDocuments = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();

            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
            $viewMode = 'off';
            $mode = 'E';
            return view("visaRecommendation::application-form", compact(
                            'countries', 'countriesWithoutBD', 'divition_eng', 'district_eng', 'economicZone', 'pc_track_no', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'nationalityWithoutBD', 'visa_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'projectClearanceData', 'mode', 'pc_applicant_name'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormView($id) {
        if (!ACL::getAccsessRight('visaRecommend', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            $app_id = Encryption::decodeId($id);
//        breadcrumb start
            $form_data = VisaRecommend::where('id', $app_id)->first();
            $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
            $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end

            $authUserId = CommonFunction::getUserId();
            $loggedUserType = CommonFunction::getUserType();

            $alreadyExistApplicant = VisaRecommend::where('id', $app_id)->first();


            $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $countriesWithoutBD = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            $nationalityWithoutBD = Countries::orderby('nationality')->where('nationality', '!=', '')->where('country_code', '!=', '001')->lists('nationality', 'iso');

            $currency = Currencies::orderby('code')->where('is_active', 1)->lists('code', 'id');
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $form_data->created_by)
                    ->first();

//            if ($loggedUserType == '5x505' && empty($projectClearanceData)) {
//                Session::flash('error', "You must have a approved project clearance certificate attached to your profile!");
//                return redirect()->back();
//            }
            if ($loggedUserType != '5x505' && empty($projectClearanceData)) {
                Session::flash('error', "This user doesn't have any approved project clearance certificate attached to
                 his profile. To investigate further, please contact with system admin!");
                return redirect()->back();
            }

            $visa_types = VisaCategories::where('is_active', 1)->where('is_archived', 0)->orderBy('type', 'asc')->lists('description', 'id');

            $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                    ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'iso');

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')
                        ->where('id', $projectClearanceData->eco_zone_id)
                        ->pluck('zone');
                $pc_track_no = $projectClearanceData->tracking_number;
                $pc_applicant_name = $projectClearanceData->applicant_name;
            } else {
                $economicZone = '';
                $pc_track_no = '';
                $pc_applicant_name = '';
            }

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();
            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];

            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $clrDocuments = [];
                $processlistExist = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $viewMode = 'on';
            $mode = 'V';

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

            return view("visaRecommendation::application-form", compact(
                            'countries', 'countriesWithoutBD', 'divition_eng', 'district_eng', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'nationalityWithoutBD', 'visa_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'process_data', 'statusArray', 'banks', 'mode', 'pc_track_no', 'pc_applicant_name'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appStore(Request $request, VisaRecommend $visaRecommendation, Processlist $processlist) {
        $app_id = (!empty($request->get('app_id')) ? Encryption::decodeId($request->get('app_id')) : '');
        try {
            $authUserId = CommonFunction::getUserId();

            $alreadyExistApplicant = VisaRecommend::where('created_by', $authUserId)->where('id', $app_id)->orderBy('id', 'ASC')->first();
            if ($alreadyExistApplicant) {
                if (!ACL::getAccsessRight('visaRecommend', 'E', $app_id))
                    die('You have no access right! Please contact with system admin if you have any query.');
                $visaRecommendation = $alreadyExistApplicant;
                if ($alreadyExistApplicant->created_by != $authUserId) {
                    session()->flash('error', 'You are not authorized to edit this application.');
                    return redirect()->back();
                }
            } else {
                if (!ACL::getAccsessRight('visaRecommend', 'A'))
                    die('You have no access right! Please contact with system admin if you have any query.');
            }

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();

            if (Auth::user()->user_type == '5x505' && empty($projectClearanceData) && empty($request->get('sv_draft'))) {
                Session::flash('error', "You must have an approved project clearance certificate attached to your profile!");
                return redirect()->back();
            }

            if ($projectClearanceData) {
                $pc_applicant_name = $projectClearanceData->applicant_name;
                $eco_zone = $projectClearanceData->eco_zone_id;
                $pc_track_no = $projectClearanceData->tracking_number;
            } else {
                $pc_applicant_name = '';
                $eco_zone = '';
                $pc_track_no = '';
            }

            $visaRecommendation->applicant_name = $pc_applicant_name;
            $visaRecommendation->country = $request->get('country');
            $visaRecommendation->division = $request->get('division');
            $visaRecommendation->district = $request->get('district');
            $visaRecommendation->state = $request->get('state');
            $visaRecommendation->province = $request->get('province');
            $visaRecommendation->road_no = $request->get('road_no');
            $visaRecommendation->house_no = $request->get('house_no');
            $visaRecommendation->post_code = $request->get('post_code');
            $visaRecommendation->phone = $request->get('phone');
            $visaRecommendation->fax = $request->get('fax');
            $visaRecommendation->email = $request->get('email');
            $visaRecommendation->website = $request->get('website');
            if ($request->get('same_as_authorized') != '') {
                $visaRecommendation->same_as_authorized = $request->get('same_as_authorized');
            }
            $visaRecommendation->correspondent_name = $request->get('correspondent_name');
            $visaRecommendation->correspondent_nationality = $request->get('correspondent_nationality');
            $visaRecommendation->correspondent_passport = $request->get('correspondent_passport');
            $visaRecommendation->correspondent_country = $request->get('correspondent_country');
            $visaRecommendation->correspondent_division = $request->get('correspondent_division');
            $visaRecommendation->correspondent_district = $request->get('correspondent_district');
            $visaRecommendation->correspondent_state = $request->get('correspondent_state');
            $visaRecommendation->correspondent_province = $request->get('correspondent_province');
            $visaRecommendation->correspondent_road_no = $request->get('correspondent_road_no');
            $visaRecommendation->correspondent_house_no = $request->get('correspondent_house_no');
            $visaRecommendation->correspondent_post_code = $request->get('correspondent_post_code');
            $visaRecommendation->correspondent_phone = $request->get('correspondent_phone');
            $visaRecommendation->correspondent_fax = $request->get('correspondent_fax');
            $visaRecommendation->correspondent_email = $request->get('correspondent_email');
            $visaRecommendation->correspondent_website = $request->get('correspondent_website');

            $visaRecommendation->visa_cat_id = $request->get('visa_cat_id');
            $visaRecommendation->bd_mission_country = $request->get('bd_mission_country');
            $visaRecommendation->embassies = $request->get('embassies');
            $visaRecommendation->clr_approval_no = $pc_track_no;

            if ($request->get('applicant_pic')) {
                $image = $request->file('applicant_pic');
                $size = $image->getSize();
                $extension = $image->getClientOriginalExtension();
                $mime_type = $image->getClientMimeType();
                //$valid_formats = array("jpg","jpeg");
                if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
                    if ($size < (1024 * 1024 * 3)) {
                        $imagename = uniqid("vr_", true) . '.' . $extension;
                        $ym1 = "uploads/" . date("Y") . "/" . date("m");
                        if (!file_exists($ym1)) {
                            mkdir($ym1, 0777, true);
                            $myfile = fopen($ym1 . "/index.html", "w");
                            fclose($myfile);
                        }
                        $img = Image::make($image->getRealPath());

                        $img->resize(120, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                        $savedPath = "{$ym1}/{$imagename}";
                        $path = public_path($savedPath);
                        $img->save($path);
                        $image_path = $ym1 . "/" . $imagename;
                        $visaRecommendation->applicant_pic = $image_path;
                    } else {
                        Session::flash('error_message', "Image size must be less than 3 megabyte");
                        return redirect()->back();
                    }
                } else {
                    Session::flash('error_message', "Image format is not valid. Please upload an image of jpg or jpeg format");
                    return redirect()->back();
                }
            }
            if (!empty($projectClearanceData)) {
                $visaRecommendation->eco_zone_id = $projectClearanceData->eco_zone_id;
            }

            $visaRecommendation->incumbent_national_name = $request->get('incumbent_national_name');
            $visaRecommendation->incumbent_designation = $request->get('incumbent_designation');
            $visaRecommendation->incumbent_nationality = $request->get('incumbent_nationality');
            $visaRecommendation->incumbent_gender = $request->get('incumbent_gender');
            $visaRecommendation->incumbent_passport = $request->get('incumbent_passport');
            if ($request->get('incumbent_pass_expire') != '') {
                $visaRecommendation->incumbent_pass_expire = CommonFunction::changeDateFormat($request->get('incumbent_pass_expire'), true);
            }
            if ($request->get('incumbent_pass_issue_date') != '') {
                $visaRecommendation->incumbent_pass_issue_date = CommonFunction::changeDateFormat($request->get('incumbent_pass_issue_date'), true);
            }
            $visaRecommendation->incumbent_pass_issue_place = $request->get('incumbent_pass_issue_place');
            $visaRecommendation->incumbent_country = $request->get('incumbent_country');
            $visaRecommendation->incumbent_division = $request->get('incumbent_division');
            $visaRecommendation->incumbent_state = $request->get('incumbent_state');
            $visaRecommendation->incumbent_district = $request->get('incumbent_district');
            $visaRecommendation->incumbent_province = $request->get('incumbent_province');
            $visaRecommendation->incumbent_road_no = $request->get('incumbent_road_no');
            $visaRecommendation->incumbent_house_no = $request->get('incumbent_house_no');
            $visaRecommendation->incumbent_post_code = $request->get('incumbent_post_code');
            $visaRecommendation->incumbent_email = $request->get('incumbent_email');
            $visaRecommendation->incumbent_phone = $request->get('incumbent_phone');
            $visaRecommendation->incumbent_fax = $request->get('incumbent_fax');
            $visaRecommendation->incumbent_dob = $request->get('incumbent_dob');
            $visaRecommendation->incumbent_martial_status = $request->get('incumbent_martial_status');
            $visaRecommendation->incumbent_degree = $request->get('incumbent_degree');
            $visaRecommendation->incumbent_institute = $request->get('incumbent_institute');
            $visaRecommendation->incumbent_result = $request->get('incumbent_result');

            if ($request->get('incumbent_certificate')) {
                $file = $request->file('incumbent_certificate');
                $size = $file->getSize();
                $extension = $file->getClientOriginalExtension();
                $valid_formats = array("pdf");
                if (in_array($extension, $valid_formats)) {
                    if ($size < (1024 * 1024 * 3)) {

                        $original_file = $file->getClientOriginalName();
                        $file->move('uploads/', time() . $original_file);
                        $fileName = 'uploads/' . time() . $original_file;

                        $visaRecommendation->incumbent_certificate = $fileName;
                    } else {
                        Session::flash('error_message', "File size must be less than 3 megabyte");
                        return redirect()->back();
                    }
                } else {
                    Session::flash('error_message', "Image format is not valid! Please upload an pdf format");
                    return redirect()->back();
                }
            }

            $visaRecommendation->salary_pay_method = $request->get('salary_pay_method');
            $visaRecommendation->salary_local_amount = $request->get('salary_local_amount');
            $visaRecommendation->salary_local_currency = $request->get('salary_local_currency');
            $visaRecommendation->oversea_pay_method = $request->get('oversea_pay_method');
            $visaRecommendation->oversea_local_amount = $request->get('oversea_local_amount');
            $visaRecommendation->oversea_local_currency = $request->get('oversea_local_currency');
            $visaRecommendation->house_pay_method = $request->get('house_pay_method');
            $visaRecommendation->house_local_amount = $request->get('house_local_amount');
            $visaRecommendation->house_local_currency = $request->get('house_local_currency');
            $visaRecommendation->conveyance_pay_method = $request->get('conveyance_pay_method');
            $visaRecommendation->conveyance_local_amount = $request->get('conveyance_local_amount');
            $visaRecommendation->conveyance_local_currency = $request->get('conveyance_local_currency');
            $visaRecommendation->medical_pay_method = $request->get('medical_pay_method');
            $visaRecommendation->medical_local_amount = $request->get('medical_local_amount');
            $visaRecommendation->medical_local_currency = $request->get('medical_local_currency');
            $visaRecommendation->entertain_pay_method = $request->get('entertain_pay_method');
            $visaRecommendation->entertain_local_amount = $request->get('entertain_local_amount');
            $visaRecommendation->entertain_local_currency = $request->get('entertain_local_currency');
            $visaRecommendation->bonus_pay_method = $request->get('bonus_pay_method');
            $visaRecommendation->bonus_local_amount = $request->get('bonus_local_amount');
            $visaRecommendation->bonus_local_currency = $request->get('bonus_local_currency');
            $visaRecommendation->fringe_benefits = $request->get('fringe_benefits');
            $visaRecommendation->salary_remarks = $request->get('salary_remarks');

            $visaRecommendation->local_executive = $request->get('local_executive');
            $visaRecommendation->local_stuff = $request->get('local_stuff');
            $visaRecommendation->local_total_no = $request->get('local_total_no');
            $visaRecommendation->foreign_executive = $request->get('foreign_executive');
            $visaRecommendation->foreign_stuff = $request->get('foreign_stuff');
            $visaRecommendation->foreign_total = $request->get('foreign_total');
            $visaRecommendation->mp_total = $request->get('mp_total');
            $visaRecommendation->mp_ratio_local = $request->get('mp_ratio_local');
            $visaRecommendation->mp_ratio_foreign = $request->get('mp_ratio_foreign');

            $acceptTerms = (!empty($request->get('acceptTerms')) ? 1 : 0);
            $visaRecommendation->acceptance_of_terms = $acceptTerms;
            $visaRecommendation->created_by = $authUserId;
            $visaRecommendation->status_id = 1;
            $visaRecommendation->service_id = $this->service_id;
            $visaRecommendation->save();


            if (empty($request->get('sv_draft'))) {
                /* if applicant submit the application without saving as draft or from draft submit an application track no will be generated
                  // if status id = 5 or shortfall, tracking id will not be regenerated */
                if ((!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number == '' && $alreadyExistApplicant->status_id != 5) ||
                        empty($alreadyExistApplicant)) {
                    $tracking_number = 'VR-' . date("dMY") . $this->service_id . str_pad($visaRecommendation->id, 6, '0', STR_PAD_LEFT);
                } else {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                }
            } else {
                if (!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number != '' && $alreadyExistApplicant->status_id == 5) {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                } else {
                    $tracking_number = '';
                }
            }

            if (empty($request->get('sv_draft'))) {
                $visaRecommendation->status_id = 1;
                $visaRecommendation->is_draft = 0;
            } else {
                $visaRecommendation->status_id = -1;
                $visaRecommendation->is_draft = 1;
            }

            $visaRecommendation->tracking_number = $tracking_number;
            $visaRecommendation->save();

            $doc_row = docInfo::where('service_id', $this->service_id)->get(['doc_id', 'doc_name']);

            ///Start file uploading

            if (isset($doc_row)) {
                foreach ($doc_row as $docs) {
                    //   if ($request->get('validate_field_' . $docs->doc_id) != '') {
                    $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ? $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                    $documnent_id = $docs->doc_id;
                    if ($request->get('document_id_' . $docs->doc_id) == '') {
                        Document::create([
                            'service_id' => $this->service_id, //  3 is for Visa Recommendation 
                            'app_id' => $visaRecommendation->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    } else {
                        $documentId = $request->get('document_id_' . $docs->doc_id);
                        Document::where('id', $documentId)->update([
                            'service_id' => $this->service_id, // 3 is for Visa Recommendation 
                            'app_id' => $visaRecommendation->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    }
                    // }
                }
            }
            /* End file uploading */

            /* Save data to process_list table */

            $processlistExist = Processlist::where('record_id', $visaRecommendation->id)->where('service_id', $this->service_id)->first();
            $deskId = 0;
            if (!empty($request->get('sv_draft'))) {
                $statusId = -1;
                $deskId = 0;
            } else {
                $statusId = 1;
                $deskId = 3; // 3 is RD1
            }

            if (count($processlistExist) < 1) {
                $process_list_insert = Processlist::create([
                            'track_no' => $tracking_number,
                            'reference_no' => '',
                            'company_id' => '',
                            'service_id' => $this->service_id,
                            'initiated_by' => CommonFunction::getUserId(),
                            'closed_by' => 0,
                            'status_id' => $statusId,
                            'desk_id' => $deskId,
                            'record_id' => $visaRecommendation->id,
                            'eco_zone_id' => CommonFunction::getEcoId(),
                            'process_desc' => '',
                            'updated_by' => CommonFunction::getUserId()
                ]);
            } else {

                if ($processlistExist->status_id > -1) {
                    if (empty($request->get('sv_draft'))) {
                        $statusId = 10; // resubmitted
                    } else {
                        $statusId = $processlistExist->status_id; // if drafted, older status will remain
                    }
                }
                $processlisUpdate = array(
                    'track_no' => $tracking_number,
                    'service_id' => $this->service_id,
                    'status_id' => $statusId,
                    'desk_id' => $deskId,
                    'eco_zone_id' => CommonFunction::getEcoId(),
                );
                $processlist->update_draft_app_for_vr($visaRecommendation->id, $processlisUpdate);
            }

            if (empty($request->get('sv_draft'))) {
                $flassMsg = "Your application has been submitted with tracking no: <strong>" . $tracking_number . "</strong>";
            } else {
                $flassMsg = "Your application has been drafted successfully";
            }

            Session::flash('success', $flassMsg);
            $listOfVisaRecommendation = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
            return redirect($listOfVisaRecommendation);
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appDownloadPDF($id) {
        $app_id = Encryption::decodeId($id);

        $form_data = VisaRecommend::where('id', $app_id)->first();
        $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();

        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');

        $authUserId = CommonFunction::getUserId();
        $alreadyExistApplicant = VisaRecommend::where('id', $app_id)->first();

        $logged_user_type = CommonFunction::getUserType();
        $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', 1) // 1 is for project Clearance
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('project_clearance.created_by', $authUserId)
                ->first();

        $visa_types = VisaCategories::where('is_active', 1)->where('is_archived', 0)->orderBy('type', 'asc')->lists('description', 'id');

        $countries = Countries::where('country_status', 'Yes')
                // ->where('country_code', '!=', '001')
                ->orderBy('nicename', 'asc')
                ->lists('nicename', 'iso');
        $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
        $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

        $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();

        $high_comissions = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');

        if (!empty($projectClearanceData)) {
            $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                    ->orderBy('zone')
                    ->where('id', $projectClearanceData->eco_zone_id)
                    ->pluck('zone');
            $pc_track_no = $projectClearanceData->tracking_number;
        } else {
            $economicZone = '';
            $pc_track_no = '';
        }

        $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
        $currency = Currencies::orderby('code')->where('is_active', 1)->lists('code', 'id');

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        $nationality = Countries::orderBy('nationality', 'asc')->where('iso', '!=', 'BD')->lists('nationality', 'iso');

        if ($alreadyExistApplicant) {
            $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
            $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
        } else {
            $clrDocuments = [];
            $processlistExist = [];
        }
        $logged_user_info = Users::where('id', $authUserId)->first();
        $viewMode = 'on';
        $mode = 'V';
        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`,
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`,
                                `process_list_hist`.`updated_by`,
                                `process_list_hist`.`status_id`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`record_id`,
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id'
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

        $content = view("visaRecommendation::application-form-html", compact('countries', 'divition_eng', 'district_eng', 'economicZone', 'document', 'logged_user_type', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'visa_types', 'high_comissions', 'payment_method', 'currency', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'process_data', 'statusArray', 'banks', 'mode', 'pc_track_no'))->render();
        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                12, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("BEZA");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->setFooter('{PAGENO} / {nb}');
        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->WriteHTML($content, 2);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        $mpdf->Output($process_data->track_no . '.pdf', 'I');  // Saving pdf "F" for Save only, "I" for view only.
    }

    public function uploadDocument() {
        return View::make('visaRecommendation::ajaxUploadFile');
    }

    public function preview() {
        return view("visaRecommendation::preview");
    }

    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');

        if ($param == 'process') {

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;

            $verifiedInfo = VisaRecommend::where('id', $curr_app_id)->first();
            $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM visa_recommend_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            // Get all applications' id
            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition
            $statusId_wz = sprintf("%02d", $statusId);

            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM visa_recommend_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";

            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {

            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-status-list') {

            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');

            $verifiedInfo = VisaRecommend::where('id', $curr_app_id)->first();
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS                        
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM visa_recommend_process_path APP  WHERE APP.status_from = '$statusId' $cond))
                        AND APS.service_id = $this->service_id
                        order by APS.status_name
                        ";

            $statusList = \DB::select(DB::raw($sql));

            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    public function updateBatch(Request $request, processlist $process_model, VisaRecommend $VisaRecommend) {
        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $status_id = $request->get('status_id');
        $attach_file = $request->file('attach_file');
        $onbehalf = $request->get('on_behalf_of');
        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }

        foreach ($apps_id as $app_id) {

            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->service_id = $this->service_id;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }

            $appInfo = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;

            if (empty($desk_id)) {
                $whereCond = "select * from visa_recommend_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'
                        AND status_to REGEXP '^([0-9]*[,]+)*$status_id([,]+[,0-9]*)*$'";

                $processPath = DB::select(DB::raw($whereCond));
                if ($processPath[0]->desk_to == '0')  // Sent to Applicant
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')   // Keep in same desk
                    $desk_id = $deskFrom;
            }

            $app_data = array(
                'status_id' => $status_id,
                'remarks' => $remarks,
                'updated_at' => date('y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            );

            $info_data = array(
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8 || $status_id == 14) {
                $info_data['closed_by'] = Auth::user()->id;
            }
            $process_model->update_app_for_vr($app_id, $info_data);

            $VisaRecommend->update_method($app_id, $app_data);
            VisaRecommend::where('id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);

            if (in_array($status_id, array(5, 8, 21, 22, 23))) {
                $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
                $cur_status = Status::where('status_id', $status_id)->where('service_id', $this->service_id)->pluck('status_name');

//            Maling
                $body_msg = '<span style="color:#000;text-align:justify;"><b>';

                $body_msg .= 'Your application for visa recommendation with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' . $cur_status . '</b>';
                $body_msg .= '</span>';
                $body_msg .= '<br/><br/><br/>Thanks<br/>';
                $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                $data = array(
                    'header' => 'Application Update',
                    'param' => $body_msg
                );
                $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');
                $certificate = '';

                if ($status_id == 23) {
                    $certificate = $this->vrCertificate($app_id);   //Certificate Generation
                    VisaRecommend::where('id', $app_id)->update(['certificate' => $certificate]);
                }
                $billMonth = date('Y-m');
                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $app_id, $certificate, $status_id,$billMonth) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                            ->to($fetched_email_address)
                            ->subject('Application Update Information for visa recommendation');
                    if ($status_id == 23) { //23 = Completed
                        $message->attach($certificate);
                        VisaRecommend::where('id', $app_id)->update(['certificate' => $certificate,'bill_month'=>$billMonth]);
                    }
                });
            }
        }

        // for previous and present status
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        Session::flash('success', "Application status updated Previous status: $statusList[$status_from] || Present Status: $statusList[$status_id]");
        return redirect()->back();
    }

    public function certificate_re_gen($id) {
        $app_id = Encryption::decodeId($id);
        $processlistExist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        $certificate = '';
        if ($status_id == 23) {
            $certificate = $this->importPermitCer($app_id);   //Certificate Generation after payment accepted
            VisaRecommend::where('id', $app_id)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function vrCertificate($id) {
        $appID = $id;

        $alreadyExistApplicant = Processlist::leftJoin('visa_recommendation', 'visa_recommendation.id', '=', 'process_list.record_id')
                ->leftJoin('users', 'users.id', '=', 'visa_recommendation.created_by')
                ->where('process_list.service_id', $this->service_id)
                ->where('process_list.record_id', $appID)
                ->first(['visa_recommendation.*', 'process_list.*', 'users.designation']);

        $countries = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
        $visa_types = VisaCategories::where('is_active', 1)->where('is_archived', 0)->orderBy('type', 'asc')->lists('description', 'id');

        $directory = 'users/signature/';
        $approver = 'Mohammed Ayub';
        $signature = ( file_exists($directory . 'rd3-sign.jpg')) ?
                'users/signature/rd3-sign.jpg' : '';
        if (!$alreadyExistApplicant) {
            return '';
        } else {
            $track_no = (!empty($alreadyExistApplicant->track_no) ? $alreadyExistApplicant->track_no : '');
            $ApproveData = ProcessHistory::where('process_id', $alreadyExistApplicant->process_id)->where('record_id', $appID)
                            ->where('status_id', 23) // 23 = approved
                            ->orderBy('p_hist_id', 'asc')->first();
            $formatted_date = '';
            if (!empty($ApproveData->updated_at)) {
                $formatted_date = date_format($ApproveData->updated_at, "d-M-Y");
            }


            $visaCategories = VisaRecommend::leftJoin('visa_categories', 'visa_recommendation.visa_cat_id', '=', 'visa_categories.id')
                    ->where('visa_recommendation.visa_cat_id', $alreadyExistApplicant->visa_cat_id)
                    ->first(['expire_after', 'description']);

            $visaType = (!empty($visaCategories->description) ? $visaCategories->description : '');
            $visaDays = (!empty($visaCategories->expire_after) ? $visaCategories->expire_after : '');


            $country = (!empty($countries[$alreadyExistApplicant->bd_mission_country])) ? $countries[$alreadyExistApplicant->bd_mission_country] : '';

            $dateNow = !empty($formatted_date) ? $formatted_date : '';

            $designation = !empty($alreadyExistApplicant->incumbent_designation) ? $alreadyExistApplicant->incumbent_designation : '';

            $applicantName = (!empty($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : '');
            $name = (!empty($alreadyExistApplicant->incumbent_national_name) ? $alreadyExistApplicant->incumbent_national_name : '');
            $passport = (!empty($alreadyExistApplicant->incumbent_passport) ? $alreadyExistApplicant->incumbent_passport : '');

            $eco_zone_id = (!empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : '');
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);
            $qrCodeGenText = $alreadyExistApplicant->tracking_number . '-' . $alreadyExistApplicant->proposed_name . '-' .
                    $economicZones->name . '-' . $dateNow;
            $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
            $url = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";

            $nationalities = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');

            $nationality = (!empty($nationalities[$alreadyExistApplicant->incumbent_nationality]) ? $nationalities[$alreadyExistApplicant->incumbent_nationality] : '');
        }

        $content = view("visaRecommendation::certificate", compact('track_no', 'country', 'visaType', 'visaDays', 'designation', 'url', 'nationality', 'passport', 'dateNow', 'signature', 'applicantName', 'name'))->render();

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                10, // margin_left
                10, // margin right
                30, // margin top
                50, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );
        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Visa Recommendation");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');

        $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;

        $mpdf->setFooter('{PAGENO} / {nb}');

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');

        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');

        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
//        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        //$mpdf->Output($process_data->track_no . '.pdf', 'I');  // Saving pdf "F" for Save only, "I" for view only.

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("beza_", true);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
        return $pdfFilePath;
    }

    public function reGenerateVrCertificate($id) {
        $appId = Encryption::decodeId($id);

        $processlistExist = Processlist::where('record_id', $appId)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        if ($status_id == 23) {
            $certificate = $this->vrCertificate($appId);
            VisaRecommend::where('id', $appId)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function certificate_gen($appID) {

        ini_set('memory_limit', '99M');

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');
        $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;
        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("beza_" . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');

        $form_data = VisaRecommend::where('id', $appID)->first();
        $desk_data = Users::where('desk_id', Auth::user()->desk_id)->first();


        $desk_name = CommonFunction::getFieldName(8, 'desk_id', 'desk_name', 'user_desk');

        if (!empty($desk_data->signature)) {
            $signature = '<img src="users/signature/' . $desk_data->signature . '" width="100" height="50">';
        } else {
            $signature = '';
        }

        $pdf_body = <<< HERE
<table border="0">
  <tr>
    <td width="222">&nbsp;</td>
    <td width="289" align="center"><pre>Government of the Peoples Republic Bangladesh</pre>
    <br/>
    <h1> The full format of certificate need to be provided from BEZA</h1>
    </td>
    
    
    <td width="213">&nbsp;</td>
  </tr>
  
<br/>
$signature
  <br/>($desk_data->user_full_name ) <br/><br/> $desk_name <br/>  
  <br/><br/><br/><br/>
   
   <br/><br/><br/>
   
   <br/>
   ($desk_data->user_full_name ) <br/><br/> $desk_name
  </td>
  </tr>
</table>
HERE;
        $mpdf->SetCompression(true);
        $mpdf->WriteHTML($pdf_body);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf
        return $pdfFilePath;
    }

    public function discardCertificate($id) {
        $app_id = Encryption::decodeId($id);
        if (Auth::user()->user_type == '1x101') {
            $appInfo = VisaRecommend::find($app_id);
            if ($appInfo->status_id == 23) {
                $appInfo->status_id = 40;
                $appInfo->certificate = '';
                $appInfo->save();

                Processlist::where(['record_id' => $app_id, 'service_id' => 3])
                        ->update(['desk_id' => 0, 'status_id' => 40]);
                Session::flash('success', 'Certificate Discard');
                return redirect()->back();
            } else {
                Session::flash('error', 'Certificate discard is not possible [VR9002]');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'You are not authorized to discard certificate [VR9001]');
            return redirect()->back();
        }
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', 1)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('visaRecommendation::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

        $industrial_category = $request->get('industry_cat_id');
        $economic_zone = $request->get('eco_zone_id');

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $industrial_category, $economic_zone);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('visaRecommendation::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();

// Code to count the total number of application for different services as a whole
        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, pl.eco_zone_id, industry_cat_id
                                            from service_info si
                                            left join process_list pl on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id !='-1'
                                            left join (select industry_cat_id, created_by
                                                    from project_clearance
                                                    where industry_cat_id = '$industrial_category'
                                                     limit 1) pc
                                             on pl.initiated_by = pc.created_by
                                            group by si.id;");

        $str = "";
        foreach ($sql as $sql_data) {
            $short_name = $sql_data->short_name;
            $id = $sql_data->id;
            $industry_cat_id = $sql_data->industry_cat_id;
            $application_number = $sql_data->application_number;
            $eco_zone_id = $sql_data->eco_zone_id;
            $str.="$short_name:$application_number:$industry_cat_id:$eco_zone_id==";
        }
        $data = ['responseCode' => 1, 'data' => $contents, 'totalApps' => $str]; // pattern= Module:AppsCount:Ind:zone
        // End of code to count total number of applications

        return response()->json($data);
    }

    public function getEmbassyByCountry(Request $request) {
        $iso = $request->get('iso');

        $embassies = Countries::leftJoin('high_comissions', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->where('iso', $iso)
                ->select('iso', DB::raw('CONCAT(high_comissions.name, ", ", high_comissions.address) AS commission'))
                ->orderBy('commission')
                ->lists('commission', 'iso');
        if (count($embassies) > 0) {
            $data = ['responseCode' => 1, 'data' => $embassies];
        } else {
            $data = ['responseCode' => 0, 'data' => ''];
        }
        return response()->json($data);
    }

    /*     * ********************************************End of Controller Class************************************************* */
}
