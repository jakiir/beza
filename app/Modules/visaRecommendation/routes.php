<?php

Route::group(array('module' => 'visaRecommendation',  'middleware' => ['auth', 'checkAdmin'], 'namespace' => 'App\Modules\visaRecommendation\Controllers'), function() {

//Search
    Route::get('visa-recommend/search', 'visaRecommendationController@search');
    Route::post('visa-recommend/search-result', 'visaRecommendationController@searchResult');
    Route::post('visa-recommend/service-wise-result', 'visaRecommendationController@serviceWiseStatus');
    Route::post('visa-recommend/search-view/{param}', 'visaRecommendationController@searchView');

    //app form create
    Route::get('visa-recommend/form', 'visaRecommendationController@appForm');
    Route::post('visa-recommend/store-app', "visaRecommendationController@appStore");
    Route::get('visa-recommend/edit-form/{id}', 'visaRecommendationController@appFormEdit');
    Route::get('visa-recommend/view/{id}', 'visaRecommendationController@appFormView');
    Route::any('visa-recommend/upload-document', 'visaRecommendationController@uploadDocument');


    Route::patch('visa-recommend/update-batch', "visaRecommendationController@updateBatch");
    Route::post('visa-recommend/challan-store/{id}', "visaRecommendationController@challanStore");

    Route::get('/visa-recommend/get-embassy-by-country', 'visaRecommendationController@getEmbassyByCountry');
    Route::post('visa-recommend/ajax/{param}', 'visaRecommendationController@ajaxRequest');
    Route::get('visa-recommend/list/', 'visaRecommendationController@listOfApp');
    Route::get('visa-recommend/list/{ind}/{zone}', 'visaRecommendationController@listOfApp');

    Route::get('visa-recommend/preview', 'visaRecommendationController@preview');
    
    Route::resource('visa-recommend', 'visaRecommendationController');

    Route::get('visa-recommend/discard-certificate/{id}','visaRecommendationController@discardCertificate');

    Route::get('visa-recommend/certificate-gen/{id}', 'visaRecommendationController@vrCertificate');

    Route::get('visa-recommend/certificate-re-gen/{id}','visaRecommendationController@reGenerateVrCertificate');
    //Download PDF
    Route::get('visa-recommend/view-pdf/{id}','visaRecommendationController@appDownloadPDF');
    /*     * ********************************End of Route group****************************** */
});
