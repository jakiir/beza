<?php

Route::group(array('module' => 'importPermit', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'],
    'namespace' => 'App\Modules\importPermit\Controllers'), function() {

    // search related
    Route::get('import-permit/search', 'importPermitController@search');
    Route::post('import-permit/search-result', 'importPermitController@searchResult');
    Route::post('import-permit/service-wise-result', 'importPermitController@serviceWiseStatus');
    Route::post('import-permit/search-view/{param}', 'importPermitController@searchView');

    // app form 
    Route::get('import-permit/form', 'importPermitController@appForm');
    Route::get('import-permit/edit-form/{id}', 'importPermitController@appFormEdit');
    Route::get('import-permit/view/{id}', 'importPermitController@appFormView');

    Route::post('import-permit/save-as-draft', "importPermitController@saveAsDraft");
    Route::post('import-permit/store-app', "importPermitController@appStore");
    Route::patch('import-permit/update-batch', "importPermitController@updateBatch");

    Route::post('import-permit/ajax/{param}', 'importPermitController@ajaxRequest');

    Route::any('import-permit/upload-document', 'importPermitController@uploadDocument');
    Route::get('import-permit/list/', 'importPermitController@listIP');
    Route::get('import-permit/list/{ind}/{zone}', 'importPermitController@listIP');

    Route::get('import-permit/preview', 'importPermitController@preview');

    Route::get('/import-permit/get-hscodes/', 'importPermitController@getHsList');

    // regarding extension or cancellation
    Route::get('import-permit/extension-cancellation', 'importPermitController@extensionCancellation');

    // PDF generetaion
    Route::get('import-permit/undertaking-gen/{id}/{type}', "importPermitController@undertakingGen");
    Route::get('import-permit/import-cer-gen/{id}', "importPermitController@importPermitCer");
    Route::get('import-permit/import-cer-re-gen/{id}', "importPermitController@certificateReGeneration");
    Route::get('import-permit/view-pdf/{id}', 'importPermitController@appDownloadPDF');

    // Gate pass generation
    Route::get('import-permit/gp-pdf/{id}/{gp_id}', 'importPermitController@gatePassGen');
    Route::post('import-permit/pass-history/{id}', "importPermitController@storePassHistory");

    Route::resource('import-permit', 'importPermitController');


    //Discard Certificate
    Route::get('/import-permit/discard-certificate/{id}','importPermitController@discardCertificate');

    /*     * ********************************End of Route group****************************** */
});
