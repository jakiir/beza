<?php

namespace App\Modules\importPermit\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\apps\Models\docInfo;
use App\Modules\apps\Models\Document;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\Apps\Models\Status;
use App\Modules\Company\Models\Company;
use App\Modules\Dashboard\Models\Services;
use App\Modules\importPermit\Models\Apps;
use App\Modules\importPermit\Models\CarrierType;
use App\Modules\importPermit\Models\GatePassHistory;
use App\Modules\importPermit\Models\ImportPermit;
use App\Modules\importPermit\Models\IpGatePass;
use App\Modules\importPermit\Models\IpIcInfo;
use App\Modules\importPermit\Models\IpMaterials;
use App\Modules\importPermit\Models\MaterialType;
use App\Modules\importPermit\Models\PermitType;
use App\Modules\ProcessPath\Models\ProcessHistory;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Settings\Models\HsCodes;
use App\Modules\Settings\Models\Ports;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Settings\Models\Units;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\Users;
use App\Modules\workPermit\Models\Processlist;
use Carbon\Carbon;
use DB;
use Encryption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Milon\Barcode\DNS1D;
use mPDF;
use Session;
use Symfony\Component\HttpKernel\Tests\DataCollector\DumpDataCollectorTest;

class importPermitController extends Controller {

    protected $service_id;

    public function __construct() {
        // Globally declaring service ID
        $this->service_id = 6; // 6 is Import Permit
    }

    public function listIP($ind = 0, $zone = 0) {
        if (!ACL::getAccsessRight('importPermit', 'V')) {
            abort('400', 'You have no access right! Contact with system admin for more information.');
        }
        try {
            Session::put('sess_user_id', Auth::user()->id);

            $getList = Apps::getList();
            $appStatus = Status::where('service_id', $this->service_id)->get();
            $statusList = array();
            foreach ($appStatus as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
                $statusList[$v->status_id . 'color'] = $v->color;
            }
            $statusList[-1] = "Draft";
            $statusList['-1' . 'color'] = '#AA0000';

            $desks = UserDesk::all();
            $deskList = array();
            foreach ($desks as $k => $v) {
                $deskList[$v->desk_id] = $v->desk_name;
            }

            //for advance search            
            $validCompanies = Processlist::leftJoin('project_clearance as pc', 'pc.id', '=', 'process_list.record_id')
                            ->where('process_list.status_id', 23)
                            ->where('pc.status_id', 23)
                            ->orderBy('pc.applicant_name', 'ASC')
                            ->lists('pc.applicant_name', 'pc.tracking_number')->all();
            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            if (Auth::user()->user_type == '9x909') { // 9x909 = customs
                $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')
                                ->whereIn('status_id', [21, 29, 30]) // regarding customs
                                ->lists('status_name', 'status_id')->all();
            } else if (Auth::user()->user_type == '3x303') { // 3x303 = security
                $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')
                                ->whereIn('status_id', [30, 31]) // regarding security
                                ->lists('status_name', 'status_id')->all();
            } else {
                $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
            }
            return view("importPermit::list", compact('status', 'deskList', 'statusList', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id', 'validCompanies', 'economicZone', 'zone', 'ind'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appForm() {
        if (!ACL::getAccsessRight('importPermit', 'A')) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }

        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = '';

            /* Start of preventing user to apply with approved project clearance */
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();
            /* end of preventing user to apply with approved project clearance */

            if (!empty($projectClearanceData)) {
                $zone_type = $projectClearanceData->zone_type;
            } else {
                $zone_type = '';
            }

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
            } else {
                $economicZone = '';
            }

            $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            $ip_permit_type = PermitType::orderBy('name')->where('type', 1)->where('status', 1)->lists('name', 'id');
            $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
            $material_type = ['' => 'Select One'] + MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id')->all();

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
            $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

            $banks = Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id');
            $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            $clrDocuments = [];
            $meterials = [];
            $ipIcInfo = [];

            $viewMode = 'off';
            $mode = 'A';
            $destination_ports = Ports::orderBy('name')->lists('name', 'id');
            return view("importPermit::application-form", compact('ip_permit_type', 'carrier_type', 'countries', 'economicZone', 'allEcoZones', 'zone_type', 'currencies', 'material_type', 'quantity_unit', 'banks', 'bankTypes', 'document', 'alreadyExistApplicant', 'clrDocuments', 'viewMode', 'meterials', 'ipIcInfo', 'mode', 'destination_ports'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormEdit($id) {
        $app_id = Encryption::decodeId($id);
        if (!ACL::getAccsessRight('importPermit', 'E', $app_id)) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = ImportPermit::where('id', $app_id)->first();
            if ($alreadyExistApplicant) {
                $alreadyExistProcesslist = Processlist::where('track_no', $alreadyExistApplicant->tracking_number)
                                ->where('service_id', $this->service_id)->first();
                if ($alreadyExistProcesslist) {
                    if ($alreadyExistProcesslist->status_id != '-1' && $alreadyExistProcesslist->status_id != 5) {
                        Session::flash('error', "You have no access right! Please contact system administration for more information.");
                        $listOfVisaAssistant = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
                        return redirect($listOfVisaAssistant);
                    }
                }
            }

            /* Start of preventing user to apply with approved project clearance */
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();
            /* end of preventing user to apply with approved project clearance */

            if (!empty($projectClearanceData)) {
                $zone_type = $projectClearanceData->zone_type;
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
            } else {
                $zone_type = '';
                $economicZone = '';
            }


            $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            $form_data = ImportPermit::where('id', $app_id)->first(['status_id', 'certificate']);
            if (!count($form_data) > 0) {
                session()->flash('error', 'Application data not found.');
                return redirect()->back();
            }

            $ip_permit_type = PermitType::orderBy('name')->where('type', 1)->where('status', 1)->lists('name', 'id');
            $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
            $material_type = ['' => 'Select One'] + MaterialType::where('status', 1)->orderBy('name')->lists('name', 'id')->all();

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
            $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

            $banks = Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id');
            $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                $meterials = IpMaterials::where('ip_id', $alreadyExistApplicant->id)->get();
                $ipIcInfo = IpIcInfo::where('ip_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
            } else {
                $clrDocuments = [];
                $meterials = [];
                $ipIcInfo = [];
            }

            $viewMode = 'off';
            $mode = 'E';
            $destination_ports = Ports::orderBy('name')->lists('name', 'id');
            return view("importPermit::application-form", compact('countries', 'economicZone', 'allEcoZones', 'zone_type', 'businessIndustryServices', 'typeofOrganizations', 'document', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'ip_permit_type', 'carrier_type', 'material_type', 'currencies', 'quantity_unit', 'banks', 'bankTypes', 'meterials', 'ipIcInfo', 'mode', 'destination_ports', 'form_data'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormView($id) {
        if (!ACL::getAccsessRight('importPermit', 'V')) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $app_id = Encryption::decodeId($id);

            // breadcrumb start
            $form_data = ImportPermit::where('id', $app_id)->first(['import_permit.*']);

            if (!count($form_data) > 0) {
                session()->flash('error', 'Application data not found.');
                return redirect()->back();
            }
            $process_data = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();

            $submitHistory = ProcessHistory::where('record_id', $app_id)->where('process_id', $process_data->process_id)
                    ->where('status_id', 1) // 1 = submitted
                    ->first();
            $submissionDate = !empty($submitHistory) ? $submitHistory->updated_at : '';

            $statusId = $process_data->status_id;
            $deskId = $process_data->desk_id;
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE APS.service_id = $this->service_id
                        AND
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM import_permit_process_path APP 
                        WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";

            $statusList2 = \DB::select(DB::raw($sql));
            $statusList = array();
            foreach ($statusList2 as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
            }

            $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end

            $authUserId = CommonFunction::getUserId();
            $loggedUserType = CommonFunction::getUserType();
//            $alreadyExistApplicant = ImportPermit::where('id', $app_id)->first();
            $alreadyExistApplicant = ImportPermit::leftJoin('project_clearance', 'project_clearance.created_by', '=', 'import_permit.created_by')
                    ->where('project_clearance.status_id', 23)
                    ->where('import_permit.id', $app_id)
                    ->first(['import_permit.*', 'project_clearance.applicant_name']);

            /* Start of preventing user to apply with approved project clearance */
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $form_data->created_by)
                    ->first();

            if ($loggedUserType != '5x505' && empty($projectClearanceData)) {
                Session::flash('error', "This user doesn't have a approved project clearance certificate attached to
                 his profile. To investigate further, please contact with system admin!");
                return redirect()->back();
            }
            /* end of preventing user to apply with approved project clearance */

            if (!empty($projectClearanceData)) {
                $zone_type = $projectClearanceData->zone_type;
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))->where('id', $projectClearanceData->eco_zone_id)->first();
            } else {
                $zone_type = '';
                $economicZone = '';
            }


            $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            $ip_permit_type = PermitType::orderBy('name')->where('type', 1)->where('status', 1)->lists('name', 'id');
            $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
            $material_type = ['' => 'Select One'] + MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id')->all();

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
            $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

            $banks = ['' => 'Select One'] + Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id')->all();
            $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            if ($alreadyExistApplicant) {
                $applicantInfo = Users::where('id', $alreadyExistApplicant->created_by)->first(['authorization_file']);
                $meterials = IpMaterials::where('ip_id', $alreadyExistApplicant->id)->get();
                $ipIcInfo = IpIcInfo::where('ip_id', $alreadyExistApplicant->id)->get();
                $gpMaterials = IpMaterials::leftJoin('units',  'ip_materials.mat_quantity_unit', '=', 'units.id')
                        ->where('ip_id', $alreadyExistApplicant->id)
                        ->get([ 'ip_materials.id as ip_mat_id', 'material_description', 'mat_quantity', 'mat_remaining_quantity', 'units.name as unit_name']);
                $remaining_quntities = IpMaterials::where('ip_id', $alreadyExistApplicant->id)->get(['mat_remaining_quantity']);
                $gpHistory = GatePassHistory::orderBy('ip_gatepass.id', 'desc')
                        ->leftJoin('ip_gatepass', 'gatepass_history.gatepass_id', '=', 'ip_gatepass.id')
                        ->leftJoin('ip_materials', 'ip_gatepass.ip_material_id', '=', 'ip_materials.id')
                        ->leftJoin('import_permit', 'gatepass_history.app_id', '=', 'import_permit.id')
                        ->leftJoin('units', 'ip_materials.mat_quantity_unit', '=', 'units.id')
                        ->where('gatepass_history.app_id', $alreadyExistApplicant->id)
                        ->where('gatepass_history.service_id', $this->service_id)
                        ->get(['import_permit.tracking_number as tracking_no',
                    'material_description',
                    'units.name as unit_name',
                    'mat_quantity', 'mat_remaining_quantity', 'passed_quantity',
                    'ip_gatepass.gatepass_no as gatepass_no',
                    'generated_gatepass',
                ]);
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
            } else {
                $applicantInfo = [];
                $clrDocuments = [];
                $meterials = [];
                $remaining_quntities = [];
                $gpMaterials = [];
                $gpHistory = [];
                $ipIcInfo = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

            $viewMode = 'on';
            $mode = 'V';
            $destination_ports = Ports::orderBy('name')->lists('name', 'id');
            return view("importPermit::application-form", compact('countries', 'economicZone', 'allEcoZones', 'zone_type', 'remaining_quntities', 'businessIndustryServices', 'typeofOrganizations', 'document', 'alreadyExistApplicant', 'logged_user_info', 'applicantInfo', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'submissionDate', 'process_data', 'statusArray', 'ip_permit_type', 'carrier_type', 'material_type', 'currencies', 'quantity_unit', 'banks', 'bankTypes', 'meterials', 'gpMaterials', 'gpHistory', 'ipIcInfo', 'mode', 'destination_ports'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appStore(Request $request, ImportPermit $importPermit, Processlist $processlist) {
        $authUserId = CommonFunction::getUserId();

        /* Start of preventing user to apply with approved project clearance */
        $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', 1) // 1 is for project Clearance
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('project_clearance.created_by', $authUserId)
                ->first();
        /* end of preventing user to apply with approved project clearance */
        if (!empty($projectClearanceData)) {
            $zone_type = $projectClearanceData->zone_type;
            $ecoZoneId = $projectClearanceData->eco_zone_id;
        } else {
            $zone_type = '';
            $ecoZoneId = '';
        }

        if ($request->get('submitInsert') == 'save') {
            $this->validate($request, [
                'permit_type' => 'required',
                'carrier_type' => 'required',
                'undertaking_no' => 'required',
                'undertaking_date' => 'required',
                'invoice_ref_no' => 'required',
                'invoice_ref_date' => 'required',
                'aw_bill_no' => 'required',
                'hs_product' => 'required',
                'fob_value' => 'required',
                'fob_cif_value' => 'required',
                'shipper_name' => 'required',
                'import_port' => 'required',
                'destination_port' => 'required',
                'material_type' => 'requiredarray',
                'hs_code' => 'requiredarray',
                'material_description' => 'requiredarray',
                'fob_usd' => 'requiredarray',
                'fob_usd_value' => 'requiredarray',
                'mat_quantity' => 'requiredarray',
                'mat_quantity_unit' => 'requiredarray',
                'tt_no' => 'requiredarray',
                'tt_value' => 'required',
                'bank_id' => 'requiredarray',
                'bank_type' => 'requiredarray',
                'bank_invoice_date' => 'requiredarray',
            ]);

            $statusArr = array(8, 13, 22, '-1');
            $alreadyExistApplicant = ImportPermit::leftJoin('process_list', function($join) {
                        $join->on('process_list.record_id', '=', 'import_permit.id');
                        $join->on('process_list.service_id', '=', DB::raw($this->service_id));
                    })
                    ->where('process_list.service_id', $this->service_id)
                    ->whereNotIn('process_list.status_id', $statusArr)
                    ->where('import_permit.created_by', $authUserId)
                    ->first();
        }
        try {
            $app_id = (!empty($request->get('app_id')) ? Encryption::decodeId($request->get('app_id')) : '');
            $alreadyExistApplicant = ImportPermit::where('created_by', $authUserId)->where('id', $app_id)->orderBy('id', 'ASC')->first();
            if ($alreadyExistApplicant) {
                if (!ACL::getAccsessRight('importPermit', 'E', $app_id))
                    abort('400', "You have no access right! Please contact with system admin if you have any query.");
                $importPermit = $alreadyExistApplicant;
                if ($alreadyExistApplicant->created_by != $authUserId) {
                    session()->flash('error', 'You are not authorized person to edit the application.');
                    return redirect()->back();
                }
            } else {
                if (!ACL::getAccsessRight('importPermit', 'A'))
                    abort('400', "You have no access right! Please contact with system admin if you have any query.");
            }


            DB::beginTransaction();
            $importPermit->applicant_name = Auth::user()->user_full_name;
            $importPermit->nationality = Auth::user()->nationality;
            $importPermit->service_id = $this->service_id;
            $importPermit->permit_type = $request->get('permit_type');
            $importPermit->carrier_type = $request->get('carrier_type');
            $importPermit->zone_type = $zone_type;
            $importPermit->eco_zone_id = $ecoZoneId;
            $importPermit->undertaking_no = $request->get('undertaking_no');
            if ($request->get('undertaking_date') != '')
                $importPermit->undertaking_date = CommonFunction::changeDateFormat($request->get('undertaking_date'), true);
            $importPermit->invoice_ref_no = $request->get('invoice_ref_no');
            if ($request->get('invoice_ref_date') != '')
                $importPermit->invoice_ref_date = CommonFunction::changeDateFormat($request->get('invoice_ref_date'), true);
            $importPermit->aw_bill_no = $request->get('aw_bill_no');
            $importPermit->fob_value = $request->get('fob_value');
            $importPermit->fob_cif_value = $request->get('fob_cif_value');
            $importPermit->shipper_name = $request->get('shipper_name');
            $importPermit->import_port = $request->get('import_port');
            $importPermit->source_country = $request->get('source_country');
            $importPermit->destination_zone = $request->get('destination_zone');
            $importPermit->destination_port = $request->get('destination_port');
            $importPermit->carrier_name = $request->get('carrier_name');
            $importPermit->carrier_passport_no = $request->get('carrier_passport_no');
            if ($request->get('carrier_pass_validity') != '')
                $importPermit->carrier_pass_validity = CommonFunction::changeDateFormat($request->get('carrier_pass_validity'), true);
            $acceptTerms = (!empty($request->get('acceptTerms')) ? 1 : 0);
            $importPermit->acceptTerms = $acceptTerms;
            $importPermit->is_locked = 0;
            $importPermit->is_archieved = 0;
            $importPermit->created_by = $authUserId;
            $importPermit->save();

            if ($request->get('submitInsert') == 'save') {
                if (empty($projectClearanceData)) {
                    Session::flash('error', "You must have a approved project clearance certificate attached to your profile!");
                    return redirect()->back();
                }
            }

            ///For Tracking ID generating and update
            if ($request->get('submitInsert') == 'save') {
                /* if applicant submit the application without saving as draft or from draft submit an application track no will be generated
                  // if status id = 5 or shortfall, tracking id will not be regenerated */
                if ((!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number == '' && $alreadyExistApplicant->status_id != 5) ||
                        empty($alreadyExistApplicant)) {
                    $tracking_number = 'IP-' . date("dMY") . $this->service_id . str_pad($importPermit->id, 6, '0', STR_PAD_LEFT);
                } else {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                }
            } else {
                if (!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number != '' && $alreadyExistApplicant->status_id == 5) {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                } else {
                    $tracking_number = '';
                }
            }

            if ($request->get('submitInsert') == 'save') {
                $importPermit->status_id = 1;
                $importPermit->is_draft = 0;
            } else {
                $importPermit->status_id = -1;
                $importPermit->is_draft = 1;
            }

            $importPermit->tracking_number = $tracking_number;
            $importPermit->save();

            $data = $request->all();
            $metIds = [];
            foreach ($data['material_type'] as $key => $meterial) {
                if (empty($data['meterial_ids'][$key])) {
                    $materialData = new IpMaterials();
                } else {
                    $materialId = $data['meterial_ids'][$key];
                    $materialData = IpMaterials::where('id', $materialId)->first();
                }
                $materialData->ip_id = $importPermit->id;
                $materialData->material_type = $data['material_type'][$key];
                $materialData->material_description = $data['material_description'][$key];
                $materialData->hs_code = $data['hs_code'][$key];
                $materialData->hs_product = $data['hs_product'][$key];
                $materialData->mat_quantity = $data['mat_quantity'][$key];
                $materialData->mat_remaining_quantity = $data['mat_quantity'][$key]; // for calculating in gatepass
                $materialData->mat_quantity_unit = $data['mat_quantity_unit'][$key];
                $materialData->flight_no = $data['flight_no'][$key];
                if (!empty($data['flight_date'][$key]))
                    $materialData->flight_date = CommonFunction::changeDateFormat($data['flight_date'][$key], true);
                $materialData->fob_usd = $data['fob_usd'][$key];
                $materialData->fob_usd_value = $data['fob_usd_value'][$key];
                $materialData->is_active = 1;
                $materialData->is_archieved = 1;
                $materialData->save();
                $metIds[] = $materialData->id;
            }
            if (!empty($metIds))
                IpMaterials::where('ip_id', $importPermit->id)->whereNotIn('id', $metIds)->delete();

            $iciIds = [];
            foreach ($data['tt_no'] as $key => $TtNo) {
                if (empty($data['icipinfo_ids'][$key])) {
                    $icIpData = new IpIcInfo();
                } else {
                    $icIplId = $data['icipinfo_ids'][$key];
                    $icIpData = IpIcInfo::where('id', $icIplId)->first();
                }
                $icIpData->ip_id = $importPermit->id;
                $icIpData->tt_no = $data['tt_no'][$key];
                $icIpData->tt_value = $data['tt_value'][$key];
                $icIpData->bank_id = $data['bank_id'][$key];
                $icIpData->bank_type = $data['bank_type'][$key];
                if (!empty($data['bank_invoice_date'][$key]))
                    $icIpData->bank_invoice_date = CommonFunction::changeDateFormat($data['bank_invoice_date'][$key], true);
                $icIpData->is_active = 1;
                $icIpData->is_archieved = 1;
                $icIpData->save();
                $iciIds[] = $icIpData->id;
            }
            if (!empty($iciIds))
                IpIcInfo::where('ip_id', $importPermit->id)->whereNotIn('id', $iciIds)->delete();

            $doc_row = docInfo::where('service_id', $this->service_id) // 6 for import permit
                    ->get(['doc_id', 'doc_name']);

            ///Start file uploading

            if (isset($doc_row)) {
                foreach ($doc_row as $docs) {
//                    if ($request->get('validate_field_' . $docs->doc_id) != '') {
                    $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ? $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                    $documnent_id = $docs->doc_id;
                    if ($request->get('document_id_' . $docs->doc_id) == '') {
                        $insertDoc = Document::create([
                                    'service_id' => $this->service_id, // 6 for import permit
                                    'app_id' => $importPermit->id,
                                    'doc_id' => $documnent_id,
                                    'doc_name' => $documentName,
                                    'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    } else {
                        $documentId = $request->get('document_id_' . $docs->doc_id);
                        $updateDoc = Document::where('id', $documentId)->update([
                            'service_id' => $this->service_id, // 6 for import permit
                            'app_id' => $importPermit->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    }
//                    }
                }
            } /* End file uploading */

            //Saving data to process_list table
            $processlistExist = Processlist::where('record_id', $importPermit->id)->where('service_id', $this->service_id)->first();

            $statusId = -1;
            $deskId = 0;
            if ($request->get('submitInsert') == 'save') {
                $statusId = 1;
                $deskId = 3; // 3 is RD1
            }

            if (count($processlistExist) < 1) {
                $process_list_insert = Processlist::create([
                            'track_no' => $tracking_number,
                            'reference_no' => '',
                            'company_id' => '',
                            'service_id' => $this->service_id,
                            'initiated_by' => CommonFunction::getUserId(),
                            'closed_by' => 0,
                            'status_id' => $statusId,
                            'desk_id' => $deskId,
                            'record_id' => $importPermit->id,
                            'eco_zone_id' => $ecoZoneId,
                            'process_desc' => '',
                            'updated_by' => CommonFunction::getUserId()
                ]);
            } else {
                if ($processlistExist->status_id > -1) {
                    if ($request->get('submitInsert') == 'save') {
                        $statusId = 10; // resubmitted
                    } else {
                        $statusId = $processlistExist->status_id; // if drafted, older status will remain
                    }
                }
                $processlisUpdate = array(
                    'track_no' => $tracking_number,
                    'service_id' => $this->service_id,
                    'status_id' => $statusId,
                    'desk_id' => $deskId,
                );
                $processlist->update_draft_app_for_ip($importPermit->id, $processlisUpdate);
            }

            $saved_app_id = $importPermit->id;
            $undertaking = '';
            if ($statusId == 1) {
                $undertaking = $this->undertakingGen($saved_app_id, 'undertaking');   // Undertaking Generation after application submission
                ImportPermit::where('id', $saved_app_id)->update(['undertaking' => $undertaking]);
            }

            DB::commit();
            if ($request->get('submitInsert') == 'save') {
                Session::flash('success', "Your application has been submitted with tracking no: <strong>" . $tracking_number . "</strong>");
            } else {
                Session::flash('success', "Your application has been drafted successfully!</strong>");
            }

            $listOfVisaAssistant = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
            return redirect($listOfVisaAssistant);
        } catch (Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function uploadDocument() {
        return View::make('importPermit::ajaxUploadFile');
    }

    public function preview() {
        return view("importPermit::preview");
    }

    /**
     * @param $param request type for ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');

        if ($param == 'process') {
            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;

            $verifiedInfo = ImportPermit::where('id', $curr_app_id)->first();
            $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM import_permit_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            // Get all applications' id
            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition

            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM import_permit_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";

            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {

            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-status-list') {
            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
//                $deskId = Users::where('id', $user_id)->pluck('desk_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');

            $verifiedInfo = ImportPermit::where('id', $curr_app_id)->first();
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS                        
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM import_permit_process_path APP  WHERE APP.status_from = '$statusId' $cond))
                        AND APS.service_id = $this->service_id
                        order by APS.status_name
                        ";

            $statusList = \DB::select(DB::raw($sql));

            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    /**
     * @param Request $request
     * @param Processlist $process_model
     * @param ImportPermit $ImportPermit_model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateBatch(Request $request, processlist $process_model, ImportPermit $ImportPermit_model) {

        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $status_id = $request->get('status_id');
        $attach_file = $request->file('attach_file');
        $onbehalf = $request->get('on_behalf_of');

        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }
        foreach ($apps_id as $app_id) {

            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->service_id = $this->service_id;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }

            $appInfo = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;

            if (empty($desk_id)) {
                $whereCond = "select * from import_permit_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'
                        AND status_to REGEXP '^([0-9]*[,]+)*$status_id([,]+[,0-9]*)*$'";

                $processPath = DB::select(DB::raw($whereCond));
                if ($processPath[0]->desk_to == '0')  // Sent to Applicant
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')   // Keep in same desk
                    $desk_id = $deskFrom;
            }

            if ($status_id == 21) { // 21 is Approved and sent to custom
                $currentDate = date("dmY");

                $ipNumber = 'IP' . $currentDate;
                $importPermitData = ImportPermit::where('service_id', '=', $this->service_id)
                                ->where('ip_number', 'LIKE', $ipNumber . '%')->count();
                $ip_number = $ipNumber . str_pad($importPermitData + 1, 3, '0', STR_PAD_LEFT);

                $app_data = array(
                    'ip_number' => $ip_number,
                    'status_id' => $status_id,
                    'remarks' => $remarks,
                    'updated_at' => date('y-m-d H:i:s'),
                    'updated_by' => Auth::user()->id
                );
            } else {
                $app_data = array(
                    'status_id' => $status_id,
                    'remarks' => $remarks,
                    'updated_at' => date('y-m-d H:i:s'),
                    'updated_by' => Auth::user()->id
                );
            }

            $info_data = array(
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8) { // 8 is discard
                $info_data['closed_by'] = Auth::user()->id;
            }
            $process_model->update_app_for_import($app_id, $info_data);

            $ImportPermit_model->update_method($app_id, $app_data);
            ImportPermit::where('id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);

            // 21 = Approved & sent to customs, 22 = Rejected,
            if (in_array($status_id, array(5, 8, 21, 22))) {
                $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
                $cur_status = Status::where('status_id', $status_id)->where('service_id', $this->service_id)->pluck('status_name');

//            Mailing
                $body_msg = '<span style="color:#000;text-align:justify;"><b>';
                $body_msg .= 'Your application for import permit with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' .
                        $cur_status . '</b>';

                $body_msg .= '</span>';
                $body_msg .= '<br/><br/><br/>Thanks<br/>';
                $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                $data = array(
                    'header' => 'Application Update',
                    'param' => $body_msg
                );

                $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');
                $certificate = '';
                if ($status_id == 21) {  // the new status 21 = Approved and sent to custom
                    $certificate = $this->importPermitCer($app_id);
//                    $certificate = $this->undertakingGen($app_id, 'approval');
                }
                $billMonth = date('Y-m');
                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $app_id, $certificate, $status_id, $billMonth) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                            ->to($fetched_email_address)
                            ->cc('jakir.ocpl@batworld.com')
                            ->subject('Application Update Information for import permit');
                    if ($status_id == 21) { // 21 = Approved and sent to custom
                        $message->attach($certificate);
                        ImportPermit::where('id', $app_id)->update(['certificate' => $certificate, 'bill_month' => $billMonth]);
                    }
                });
            }
        }

        //  for previous and present status
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        Session::flash('success', "Application status updated: Previous status: $statusList[$status_from] || Present Status: $statusList[$status_id]");
        return redirect()->back();
    }

    public function certificateReGeneration($id) {
        $app_id = Encryption::decodeId($id);
        $processlistExist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        $certificate = '';
        if ($status_id == 21 || $status_id >= 28) {
            $certificate = $this->importPermitCer($app_id);   //Certificate Generation after payment accepted
            ImportPermit::where('id', $app_id)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function storePassHistory($ip_id, Request $request) {
        $app_id = Encryption::decodeId($ip_id);

        $ipInfo = ImportPermit::where('id', $app_id)->first();

        if (empty($ipInfo)) {
            Session::flash('error', "Sufficient information couldn't be fetched for this import permit! Please contact to system admin for further investigation.");
            return redirect()->back();
        }

        $status_id = $ipInfo->status_id;

        if ($status_id == 30 || $status_id == 32) { // 30 = custom verified, 32 = Partial Gatepass
            $currentTime = time();

            $ip_mat_ids = $request->get('ip_mat_id');
            $passed_quantities = $request->get('passed_quantity');

            $i = 0;
            foreach ($ip_mat_ids as $mat_id) {
                $ipMatInfo = IpMaterials::where('id', $mat_id)->first();

                $remain_quantity = $ipMatInfo->mat_remaining_quantity;
                $total_quntity = $ipMatInfo->mat_quantity;

                $passed = $passed_quantities[$i];

                if ($passed > 0) {
                    $gp_no = 'GP-' . $this->service_id . str_pad($app_id, 6, '0', STR_PAD_LEFT) . $currentTime;

                    if ($total_quntity >= $remain_quantity) {
                        $new_remain = $remain_quantity - $passed; // Substracting passed quantity from remaining
                        IpMaterials::where('id', $mat_id)->update([
                            'mat_remaining_quantity' => $new_remain,
                        ]);
                    } else {
                        Session::flash('error', "The passed quantity can not exceed the original quantity");
                        return redirect()->back();
                    }

                    $insertedGp = IpGatePass::create([
                                'app_id' => $app_id,
                                'ip_material_id' => $mat_id,
                                'gatepass_no' => $gp_no,
                                'remaining_quantity' => $remain_quantity,
                                'passed_quantity' => $passed,
                                'created_at' => date('y-m-d H:i:s'),
                                'created_by' => Auth::user()->id
                    ]);

                    if ($insertedGp) {
                        $gen_gatepass = $this->gatePassGen($app_id, $insertedGp->id);

                        IpGatePass::where('id', $insertedGp->id)->update([
                            'generated_gatepass' => $gen_gatepass,
                        ]);

                        GatePassHistory::create([
                            'app_id' => $app_id,
                            'service_id' => $this->service_id,
                            'gatepass_id' => $insertedGp->id,
                            'gatepass_no' => $gp_no,
                            'created_at' => date('y-m-d H:i:s'),
                            'created_by' => Auth::user()->id
                        ]);


                        //  Sending gatepass as attachment
                        $body_msg = '<span style="color:#000;text-align:justify;">';
                        $body_msg .= 'You can find the gatepass generated for your import permit application with Tracking Number: <b>'
                                . $ipInfo->tracking_number . '</b> in the attachment.';
                        $body_msg .= '</span>';
                        $body_msg .= '<br/><br/><span>Please print and preserve this copy for further usage.</span>';
                        $body_msg .= '<br/><br/><br/>Thanks<br/>';
                        $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                        $data = array(
                            'header' => 'Application Update',
                            'param' => $body_msg
                        );

                        $fetched_email_address = CommonFunction::getFieldName($ipInfo->created_by, 'id', 'user_email', 'users');

                        \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $gen_gatepass ) {
                            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                                    ->to($fetched_email_address)
                                    ->cc('jakir.ocpl@batworld.com')
                                    ->subject('Gate Pass for import permit');
                            $message->attach($gen_gatepass);
                        });
                    } else {
                        Session::flash('error', "Some errors have been occurred while processing. Please try again later!");
                        return redirect()->back();
                    }
                } // if passed quantity is greater than 0
                $i++;
            }
        } else {
            Session::flash('error', "This import permit doesn't have the correct status for processing! Please contact to system admin for further investigation.");
            return redirect()->back();
        }

        // 30 = custom verfied & sent to customs, 22 = Rejected,
        if (in_array($status_id, array(5, 8, 22))) {
            $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
            $cur_status = Status::where('status_id', $status_id)->where('service_id', $this->service_id)->pluck('status_name');

//            Notification
            $body_msg = '<span style="color:#000;text-align:justify;"><b>';
            $body_msg .= 'Your application for import permit with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' .
                    $cur_status . '</b></span>';
            $body_msg .= '<br/><br/><br/>Thanks<br/>';
            $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

            $data = array(
                'header' => 'Update of Application',
                'param' => $body_msg
            );

            $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');

            \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $gen_gatepass, $status_id) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->subject('Application Update Information for import permit');
                if ($status_id == 21) { // the new status 21 = Approved and sent to custom
                    $message->attach($gen_gatepass);
                }
            });
        }

        Session::flash('success', "Gatepass is generated successfully");
        return redirect()->back();
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', $this->service_id)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('importPermit::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

        $industrial_category = $request->get('industry_cat_id');
        $economic_zone = $request->get('eco_zone_id');

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $industrial_category, $economic_zone);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('importPermit::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();

// Code to count the total number of application for different services as a whole
        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, pl.eco_zone_id, industry_cat_id
                                            from service_info si
                                            left join process_list pl on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id !='-1'
                                            left join (select industry_cat_id, created_by
                                                    from project_clearance
                                                    where industry_cat_id = '$industrial_category'
                                                     limit 1) pc
                                             on pl.initiated_by = pc.created_by
                                            group by si.id;");

        $str = "";
        foreach ($sql as $sql_data) {
            $short_name = $sql_data->short_name;
            $id = $sql_data->id;
            $industry_cat_id = $sql_data->industry_cat_id;
            $application_number = $sql_data->application_number;
            $eco_zone_id = $sql_data->eco_zone_id;
            $str.="$short_name:$application_number:$industry_cat_id:$eco_zone_id==";
        }
        $data = ['responseCode' => 1, 'data' => $contents, 'totalApps' => $str]; // pattern= Module:AppsCount:Ind:zone
        // End of code to count total number of applications

        return response()->json($data);
    }

    public function undertakingGen($appID, $pdfType) {
        ini_set('memory_limit', '99M');

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        if ($pdfType == 'undertaking') {
            $mpdf->SetHTMLHeader('');
        } else {
            $mpdf->SetHTMLHeader('<img src="assets/images/logo_beza.png" alt="BEZA" width="150px"/>');
            $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
            $mpdf->showWatermarkImage = true;
        }

        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }

        $certificateName = uniqid("beza_" . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');


        $alreadyExistApplicant = Processlist::leftJoin('import_permit', 'import_permit.id', '=', 'process_list.record_id')
                ->where('process_list.service_id', $this->service_id)
                ->where('process_list.record_id', $appID)
                ->first();

        if (!$alreadyExistApplicant) {
            return '';
        } else {
            $projectClearanceInfo = ProjectClearance::where('status_id', 23) // 23 is payment acceptance
                    ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                    ->first();
            $company_name = $projectClearanceInfo->proposed_name;

            $track_no = !empty($alreadyExistApplicant->track_no) ? $alreadyExistApplicant->track_no : '';
            $dateNow = date("dS F, Y");

            $eco_zone_id = !empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : '';
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);

            $invoice_no = !empty($alreadyExistApplicant->invoice_ref_no) ? $alreadyExistApplicant->invoice_ref_no : '';
            $invoice_date = !empty($alreadyExistApplicant->invoice_ref_date) ?
                    CommonFunction::changeDateFormat(substr($alreadyExistApplicant->invoice_ref_date, 0, 10)) : '';

            $directory = 'users/signature/';

            if ($pdfType == 'undertaking') {
                $approver = '';
                $url = '';
                $applicant = Users::where('users.id', $alreadyExistApplicant->created_by)
                        ->first(['user_full_name', 'signature']);

                $signature = (!empty($applicant->signature) && file_exists($directory . $applicant->signature)) ?
                        'users/signature/' . $applicant->signature : '';

                $permit_type_name = PermitType::where('type', 1)->where('id', $alreadyExistApplicant->permit_type)->pluck('name');

                $heading = $permit_type_name . " Permit Undertaking";
            } else { // for certificate generation
                $applicant = '';

                /*                 * **Start of commenting code for Static RD3 as instructed by BEZA ***************** */
//                $approver = Users::leftJoin('user_desk', 'users.desk_id', '=', 'user_desk.desk_id')
//                        ->where('users.desk_id', 5) // 5 is RD3 approver
//                        ->first(['user_desk.desk_name as desk', 'user_full_name', 'signature']);
//             $signature = (!empty($approver->signature) && file_exists($directory . $approver->signature)) ?
//                        'users/signature/' . $approver->signature : '';
                /*                 * **End of commenting code for Static RD3 as instructed by BEZA ***************** */

                $approval_date = !empty($projectClearanceInfo->updated_at) ?
                        CommonFunction::changeDateFormat(substr($alreadyExistApplicant->updated_at, 0, 10)) : '';

                $signature = (file_exists($directory . 'rd3-sign.jpg')) ? 'users/signature/rd3-sign.jpg' : '';
                $approver = 'Mohammed Ayub';

                $heading = "Import Permit Forwarding Letter";

                if ($alreadyExistApplicant->status_id == 21 && !empty($alreadyExistApplicant->ip_number)) { //  21 = Approved and sent to custom
                    $ipNo = $alreadyExistApplicant->ip_number;
                } else {
                    $ipNo = '';
                }

                $qrCodeGenText = $alreadyExistApplicant->tracking_number . '-' . $company_name . '-' .
                        $economicZones->name . '-' . $approval_date;
                $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
                $url = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";
            }

            $all_materials = IpMaterials::leftJoin('ip_material_type', 'ip_materials.material_type', '=', 'ip_material_type.id')
                            ->where('ip_id', $appID)->get(['ip_material_type.name as material']);

            $materials_arr = array();
            foreach ($all_materials as $row) {
                $materials_arr[] = $row->material;
            }

            if (in_array('Building Materials', $materials_arr) && !in_array('Raw Materials', $materials_arr) &&
                    !in_array('Capital Machineries', $materials_arr)) {
                $materials = "Building Materials";
            } else if (!in_array('Building Materials', $materials_arr) && in_array('Raw Materials', $materials_arr) &&
                    !in_array('Capital Machineries', $materials_arr)) {
                $materials = 'Raw Materials';
            } else if (!in_array('Building Materials', $materials_arr) && !in_array('Raw Materials', $materials_arr) &&
                    in_array('Capital Machineries', $materials_arr)) {
                $materials = 'Capital Machineries';
            } else {
                $materials = implode(', ', $materials_arr);
            }

            $ip_documents = Document::where('app_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)
                    ->get(['doc_name', 'created_at']);

            $pdf_body = View::make("importPermit::undertaking-html", compact('dateNow', 'track_no', 'economicZones', 'materials', 'ip_documents', 'alreadyExistApplicant', 'permit_type_name', 'invoice_no', 'invoice_date', 'signature', 'applicant', 'company_name', 'pdfType', 'heading', 'ipNo', 'approver', 'applicant', 'url'))
                    ->render();

            $mpdf->SetCompression(true);
            $mpdf->WriteHTML($pdf_body);
            $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
            $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
            return $pdfFilePath;
        }
    }

    public function gatePassGen($appID, $gpId) {
        ini_set('memory_limit', '99M');

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $mpdf->SetHTMLHeader('');

        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }

        $certificateName = uniqid("beza_" . $this->service_id . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');


        $alreadyExistApplicant = Processlist::leftJoin('import_permit', 'import_permit.id', '=', 'process_list.record_id')
                ->where('process_list.service_id', $this->service_id)
                ->where('process_list.record_id', $appID)
                ->first();

        if (!$alreadyExistApplicant) {
            return '';
        } else {
            $projectClearanceInfo = ProjectClearance::where('status_id', 23) // 23 is payment acceptance
                    ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                    ->first();

            $company_name = $projectClearanceInfo->proposed_name;
            $permit_type = PermitType::where('id', $alreadyExistApplicant->permit_type)->pluck('name');

            $ip_no = !empty($alreadyExistApplicant->ip_number) ? $alreadyExistApplicant->ip_number : '';
            $track_no = !empty($alreadyExistApplicant->track_no) ? $alreadyExistApplicant->track_no : '';
            $dateNow = date("dS F, Y");
            $timeNow = date("h:i:sa");

            $eco_zone_id = !empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : '';
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);

            $passedMaterials = IpMaterials::leftJoin('units', 'ip_materials.mat_quantity_unit', '=', 'units.id')
                    ->leftJoin('ip_gatepass', 'ip_gatepass.ip_material_id', '=', 'ip_materials.id')
                    ->where('ip_id', $appID)
                    ->where('ip_gatepass.passed_quantity', '>', 0)
                    ->where('ip_gatepass.id', $gpId)
                    ->get(['material_description', 'hs_code', 'units.name as unit_name', 'passed_quantity']);

            $logged_user_id = Auth::user()->id;
            $security_user = Users::leftJoin('user_desk', 'users.desk_id', '=', 'user_desk.desk_id')
                    ->where('users.desk_id', 9) // 9 is RD3 approver
                    ->where('id', $logged_user_id)
                    ->first(['user_desk.desk_name as desk', 'user_full_name', 'designation', 'signature']);

            if (!empty($security_user)) {
                $directory = 'users/signature/';
                $signature = (!empty($security_user->signature) && file_exists($directory . $security_user->signature)) ?
                        'users/signature/' . $security_user->signature : '';
                $security_name = $security_user->user_full_name;
                $designation = $security_user->desk; // as designation field is not being in yet
            } else {
                $signature = '';
                $security_name = '';
                $designation = '';
            }

            $pdf_body = View::make("importPermit::gp-html", compact('company_name', 'track_no', 'dateNow', 'timeNow', 'economicZones', 'permit_type', 'ip_no', 'passedMaterials', 'signature', 'security_name', 'designation'))
                    ->render();

            $mpdf->SetCompression(true);
            $mpdf->WriteHTML($pdf_body);
            $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
            $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
            return $pdfFilePath;
        }
    }

    public function importPermitCer($id) {
        $app_id = $id;
        $dn1d = new DNS1D();


        $alreadyExistApplicant = ImportPermit::where('id', $app_id)->first();

        $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', 1) // 1 is for project Clearance
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                ->first(['id', 'proposed_name', 'process_id']);

        $process_data = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
        $statusId = $process_data->status_id;
        $deskId = $process_data->desk_id;
        $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE APS.service_id = 6
                        AND
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM import_permit_process_path APP WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";

        $statusList2 = \DB::select(DB::raw($sql));
        $statusList = array();
        foreach ($statusList2 as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');

        $ip_permit_type = PermitType::orderBy('name')->where('type', 1)->where('status', 1)->lists('name', 'id');
        $permit_type_name = PermitType::where('type', 1)->where('id', $alreadyExistApplicant->permit_type)->pluck('name');

        $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
        $material_type = ['' => 'Select One'] + MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id')->all();

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso')->all();
        $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
        $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

        $banks = ['' => 'Select One'] + Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id')->all();
        $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        if ($alreadyExistApplicant) {
            $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
            $meterials = IpMaterials::where('ip_id', $alreadyExistApplicant->id)->get();
            $ipIcInfo = IpIcInfo::where('ip_id', $alreadyExistApplicant->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
        } else {
            $clrDocuments = [];
            $meterials = [];
            $ipIcInfo = [];
        }

        $ApproveData = ProcessHistory::where('record_id', $app_id)->where('status_id', 21) // 21 = approved
                        ->orderBy('p_hist_id', 'desc')->first();

        $formatted_date = '';
        if (!empty($ApproveData->created_at)) {
            $formatted_date = date_format($ApproveData->created_at, "d-M-Y");
            ;
        }

        $certificate_issue_date = !empty($formatted_date) ? $formatted_date : '';


        $authUserId = CommonFunction::getUserId();
        $logged_user_info = Users::where('id', $authUserId)->first();

        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

        $viewMode = 'on';
        $mode = 'V';
        $destination_ports = Ports::orderBy('name')->lists('name', 'id');


        $content = view("importPermit::certificate", compact('countries', 'projectClearanceData', 'economicZone', 'zone_type', 'businessIndustryServices', 'typeofOrganizations', 'document', 'alreadyExistApplicant', 'permit_type_name', 'logged_user_info', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'process_history', 'certificate_issue_date', 'process_data', 'statusArray', 'ip_permit_type', 'carrier_type', 'material_type', 'currencies', 'quantity_unit', 'banks', 'bankTypes', 'meterials', 'ipIcInfo', 'mode', 'destination_ports'))->render();


        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                10, // margin_left
                10, // margin right
                30, // margin top
                50, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );
        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Import Permit");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');

        $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;

        if (!empty($alreadyExistApplicant->ip_number)) {
            $barcode = $dn1d->getBarcodePNG($alreadyExistApplicant->ip_number, 'C39');
            $img = '<img src="data:image/png;base64,' . $barcode . '" alt="barcode" width="250"/>';
        } else {
            $img = '';
        }

        $mpdf->setFooter('<div style="margin-top:6px;">
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style="padding:2px;font-size:9px;" class="alert alert-info">
                                            The goods specified in the invoice(s) are eligible for exemption for the whole of the customs duty, sales tax etc.
                                            provided S.R.O. No. 209-LAW/2015/46/Customs dated July 1,2015 issued by the Internal Resources Division,
                                            Ministry of Finance, Dhaka
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding:2px;font-size:10px;" class="alert alert-danger">
                                            <b> THIS PERMIT IS VALID FOR 30 (THIRTY) DAYS FROM THE DATE OF ISSUE</b>
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding:2px;font-size:10px;" class="alert alert-warning">
                                            <b>
                                                THIS IS COMPUTER GENERATED PERMIT. SO SIGNATURE IS NOT REQUIRED
                                            </b>
                                            <br/>' . $img . '<br/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br/> {PAGENO} / {nb}');

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');

        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        //$mpdf->Output($process_data->track_no . '.pdf', 'I');  // Saving pdf "F" for Save only, "I" for view only.

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("beza_" . $process_data->track_no . "_", true);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
        return $pdfFilePath;
    }

    public function discardCertificate($id) {
        $app_id = Encryption::decodeId($id);

        if (Auth::user()->user_type == '1x101') {
            $appInfo = ImportPermit::find($app_id);
            if (in_array($appInfo->status_id, [21, 28, 30])) {
                $appInfo->status_id = 40;
                $appInfo->certificate = '';
                $appInfo->save();

                ProcessList::where(['record_id' => $app_id, 'service_id' => 6])
                        ->update(['desk_id' => 0, 'status_id' => 40]);

                Session::flash('success', 'Certificate cancelled');
                return redirect()->back();
            } else {
                Session::flash('error', 'Certificate discard is not possible [IP9001]');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'You are not authorized user to discard certificate [IP9002]');
            return redirect()->back();
        }
    }

    public function appDownloadPDF($id) {
        $app_id = Encryption::decodeId($id);

        $form_data = ImportPermit::where('id', $app_id)->first(['import_permit.*']);

        if (!count($form_data) > 0) {
            session()->flash('error', 'Application data not found.');
            return redirect()->back();
        }

        $process_data = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
        $statusId = $process_data->status_id;
        $deskId = $process_data->desk_id;
        $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE APS.service_id = 6
                        AND
                        find_in_set(APS.status_id,
                            (SELECT GROUP_CONCAT(status_to) FROM import_permit_process_path APP WHERE APP.status_from = '$statusId'
                            AND desk_from LIKE '%$deskId%'))";

        $statusList2 = \DB::select(DB::raw($sql));
        $statusList = array();
        foreach ($statusList2 as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end

        $authUserId = CommonFunction::getUserId();

        $alreadyExistApplicant = ImportPermit::leftJoin('project_clearance', 'project_clearance.created_by', '=', 'import_permit.created_by')
                        ->Where('project_clearance.status_id', 23)
                        ->where('import_permit.id', $app_id)->first(['import_permit.*', 'project_clearance.applicant_name']);


        $ip_permit_type = PermitType::orderBy('name')->where('type', 1)->where('status', 1)->lists('name', 'id');
        $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
        $material_type = ['' => 'Select One'] + MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id')->all();

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');
        $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso')->all();
        $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
        $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

        $banks = ['' => 'Select One'] + Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id')->all();
        $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        if ($form_data) {
            $clr_document = Document::where('app_id', $form_data->id)->get();
            $meterials = IpMaterials::where('ip_id', $form_data->id)->get();
            $ipIcInfo = IpIcInfo::where('ip_id', $form_data->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
        } else {
            $clrDocuments = [];
            $meterials = [];
            $ipIcInfo = [];
        }

        $logged_user_info = Users::where('id', $authUserId)->first();

        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

        $viewMode = 'on';
        $mode = 'V';
        $destination_ports = Ports::orderBy('name')->lists('name', 'id');


        $content = view("importPermit::application-form-html", compact('countries', 'allEcoZones', 'economicZone', 'zone_type', 'businessIndustryServices', 'typeofOrganizations', 'document', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'process_data', 'statusArray', 'ip_permit_type', 'carrier_type', 'material_type', 'currencies', 'quantity_unit', 'banks', 'bankTypes', 'meterials', 'ipIcInfo', 'mode', 'destination_ports'))->render();

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                12, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );
        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Import Permit");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->setFooter('{PAGENO} / {nb}');
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');

        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->WriteHTML($content, 2);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        $mpdf->Output($process_data->track_no . '.pdf', 'I');  // Saving pdf "F" for Save only, "I" for view only.
    }

    public function getHsList(Request $request) {
        $results = HsCodes::where('hs_code', 'LIKE', '%' . $request->get('q') . '%')->get(['hs_code', 'product_name', 'id']);

        $data = array();
        foreach ($results as $key => $value) {
            $data[] = array(
                'value' => $value->hs_code,
                'product' => $value->product_name,
                'id' => $value->id);
        }

        return json_encode($data);
    }

    // regarding extension or cancellation
    public function extensionCancellation(Request $request) {
        $response = array();
        $process_list_data = array();
        $data = '';
        $decoded_app_id = Encryption::decodeId($request->get('app_id'));

        $extension_cancellation = $request->get('extension_cancellation');
        $extension_cancellation_remark = $request->get('extension_cancellation_remark');

        if ($extension_cancellation == '') {
            $data = 'Please select apply for type';
        } elseif ($extension_cancellation_remark == '') {
            $data = 'Please enter remarks';
        }

        if ($extension_cancellation == 'extension') {
            $extension_upto = $request->get('extension_upto');
            if ($extension_upto == '') {
                $data = 'Please enter extension upto date';
            }
        }

        if (strlen($data) <= 0) {
            if ($extension_cancellation == 'extension') {
                $update_data['extension_cancellation'] = 1;
                $update_data['extension_date'] = $extension_upto;
                $update_data['extension_remark'] = $extension_cancellation_remark;
            } else {
                $update_data['extension_cancellation'] = 2;
                $update_data['cancellation_remark'] = $extension_cancellation_remark;
            }
            $update_data['status_id'] = $process_list_data['status_id'] = 1;
            ImportPermit::where('id', $decoded_app_id)->update($update_data);
            $response = array(
                'responseCode' => 1,
                'message' => $update_data
            );
            $process_list_data['desk_id'] = 3;
            Processlist::where(['record_id' => $decoded_app_id, 'service_id' => 6])->update($process_list_data);
        } else {
            $response = array(
                'responseCode' => 0,
                'message' => $data
            );
        }

        return json_encode($response);
    }

    /*     * ********************************************End of Controller Class************************************************* */
}
