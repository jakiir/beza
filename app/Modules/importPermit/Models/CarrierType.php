<?php

namespace App\Modules\importPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class CarrierType extends Model {

    protected $table = 'ip_carrier_type';
    protected $fillable = array(
        'id',
        'name',
        'status',
    );

/*******************************End of Model Class***********************************/
}
