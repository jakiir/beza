<?php

namespace App\Modules\importPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IpMaterials extends Model {

    protected $table = 'ip_materials';
    protected $primaryKey = 'id';
    protected $fillable = array(
        'id',
        'ip_id',
        'material_type',
        'material_description',
        'hs_code',
        'hs_product',
        'mat_quantity',
        'mat_quantity_unit',
        'mat_remaining_quantity',
        'fob_usd',
        'fob_usd_value',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }


    /*     * *****************************End of Model Class********************************** */
}
