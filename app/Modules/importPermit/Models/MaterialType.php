<?php

namespace App\Modules\importPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class MaterialType extends Model {

    protected $table = 'ip_material_type';
    protected $fillable = array(
        'id',
        'name',
        'status',
    );

/*******************************End of Model Class***********************************/
}
