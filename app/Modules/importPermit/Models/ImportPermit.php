<?php

namespace App\Modules\importPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ImportPermit extends Model {

    protected $table = 'import_permit';
    protected $primaryKey = 'id';
    protected $fillable = array(
        'id',
        'tracking_number',
        'ip_number',
        'status_id',
        'bill_id',
        'bill_month',
        'payment_status_id',
        'permit_type',
        'carrier_type',
        'eco_zone_id',
        'zone_type',
        'undertaking_no',
        'undertaking_date',
        'invoice_ref_no',
        'invoice_ref_date',
        'aw_bill_no',
        'shipper_name',
        'import_port',
        'fob_value',
        'fob_cif_value',
        'source_country',
        'destination_zone',
        'destination_port',
        'hs_code',
        'carrier_name',
        'carrier_passport_no',
        'carrier_pass_validity',
        'challan_no',
        'bank_name',
        'challan_amount',
        'remarks',
        'challan_branch',
        'challan_date',
        'challan_file',
        'is_draft',
        'is_locked',
        'acceptTerms',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

        public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    function update_method($app_id, $data) {
        DB::table($this->table)
            ->where('id', $app_id)
            ->update($data);
    }
    
    /*     * *****************************End of Model Class********************************** */
}
