<?php

namespace App\Modules\importPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class PermitType extends Model {

    protected $table = 'permit_type';
    protected $fillable = array(
        'id',
        'name',
        'type',
        'status',
    );

/*******************************End of Model Class***********************************/
}
