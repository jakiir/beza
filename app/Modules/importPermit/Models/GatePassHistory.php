<?php

namespace App\Modules\importPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class GatePassHistory extends Model {

    protected $table = 'gatepass_history';
    protected $primaryKey = 'id';
    protected $fillable = array(
        'id',
        'app_id',
        'service_id',
        'gatepass_id',
        'gatepass_no',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }


    /*     * *****************************End of Model Class********************************** */
}
