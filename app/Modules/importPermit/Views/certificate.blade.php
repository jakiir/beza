@extends('layouts.pdfGen')
@section('content')
<section class="content" id="projectClearanceForm">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="" id="inputForm" style="text-align: center">
                    <div class="" style="background-color: red; color: whitesmoke;">
                        <strong>IMPORT PERMIT</strong>
                    </div>
                    <div class="panel-body" style="margin:6px;">

                        <section class="content-header">
                            <table width="100%" style="font-size:8px;">
                                <tr>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Import Permit (IP) No : </strong><span style="font-size:10px;">{{ $process_data->track_no  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;"> Date of Issue: </strong><span style="font-size:10px;"> 
                                            {{ $certificate_issue_date  }}</span></td>
                                </tr>

                                <tr>
                                    <td style="padding:5px; font-size:10px">
                                        <?php if ($alreadyExistApplicant->status_id == 23 && !empty($alreadyExistApplicant->certificate)) { ?>
                                            <a href="{{ url($alreadyExistApplicant->certificate) }}"
                                               title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                           <?php } ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </section>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong style="font-size:10px;">General Information</strong></div>
                            <div class="panel-body" style="font-size:8px;">
                                <table width="100%" style="font-size:8px;">
                                    @if(isset($alreadyExistApplicant->ip_number))
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Name of Company : </strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($projectClearanceData->proposed_name)?$projectClearanceData->proposed_name:'N/A')}}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;"></strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"></span></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Import Permit Type : </strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->permit_type) ? $ip_permit_type[$alreadyExistApplicant->permit_type] : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Carrier Type :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->carrier_type) ? $carrier_type[$alreadyExistApplicant->carrier_type] : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Type of Zone :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->zone_type) ? $alreadyExistApplicant->zone_type : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Name of Economic Zone :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->eco_zone_id) ? $economicZone[$alreadyExistApplicant->eco_zone_id] : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Undertaking No. :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->undertaking_no) ? $alreadyExistApplicant->undertaking_no : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Undertaking Date :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->undertaking_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->undertaking_date, 0, 10)) : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Invoice/Vendor Reference No. :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->invoice_ref_no) ? $alreadyExistApplicant->invoice_ref_no : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Invoice/Vendor Ref. Date :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->invoice_ref_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->invoice_ref_date, 0, 10)) : 'N/A') }}</span></td>
                                    </tr>

                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">B/L, A/W Bill, T.C No. :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->aw_bill_no) ? $alreadyExistApplicant->aw_bill_no : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">FoB/CIF/CFR/C&F Value :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->fob_value) ? $currencies[$alreadyExistApplicant->fob_value] : 'N/A') }}</span> <span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->fob_cif_value) ? $alreadyExistApplicant->fob_cif_value : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Name of the Shipper :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->shipper_name) ? $alreadyExistApplicant->shipper_name : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Import from / Port of loading :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->import_port) ? $alreadyExistApplicant->import_port : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Source Country :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->source_country) ? $countries[$alreadyExistApplicant->source_country] : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Port of destination :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($alreadyExistApplicant->destination_port) ? $destination_ports[$alreadyExistApplicant->destination_port] : 'N/A') }}</span></td>
                                    </tr>
                                </table>
                            </div> <!--/panel-body-->
                        </div> <!--/panel-->

                        @if(count($meterials) > 0)
                        <?php $inc = 0; ?>

                        <div id="templateImdFullPar">
                            @foreach($meterials as $eachmeterials)
                            <div class="panel panel-black templateImdFull">
                                <div class="panel-heading"><strong style="font-size:10px;"><i class="fa fa-list"></i> Import Materials &amp; L/C Information Group</strong></div>
                                <div class="panel-body" style="margin: 4px;">
                                    <div id="edidMoreInfoMd {{ $inc }}">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <span class=""><strong style="font-size:10px;">Import Material Details</strong></span>
                                            </div>
                                            <div class="panel-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Type of material :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"> {{ (!empty($material_type[$eachmeterials->material_type]))?$material_type[$eachmeterials->material_type] : 'N/A' }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">HS Code :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($eachmeterials->hs_code)? $eachmeterials->hs_code : 'N/A') }}</span><br/><span style="font-size: 10px;">{{ (!empty($eachmeterials->hs_product)?$eachmeterials->hs_product:'') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Material description :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($eachmeterials->material_description)? $eachmeterials->material_description : 'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">FoB / CNF / CIF / CRF / X-Factory / DDU / Replacement / Free of Cost value :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"> {{ (!empty($currencies[$eachmeterials->fob_usd])? $currencies[$eachmeterials->fob_usd] : 0) }}</span><span style="font-size:10px;"> {{ (!empty($eachmeterials->fob_usd_value) ? $eachmeterials->fob_usd_value: 'N/A') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Unit of Quantity :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($eachmeterials->mat_quantity_unit) ? $quantity_unit[$eachmeterials->mat_quantity_unit] : 'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Quantity :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($eachmeterials->mat_quantity) ? $eachmeterials->mat_quantity : 'N/A') }}</span></td>
                                                    </tr>
                                                </table>
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                    </div>


                                    <div id="edidMoreInfoPo">
                                        <div class="panel panel-primary templateTtPo" id="templateTtPo">
                                            <div class="panel-heading"><strong style="font-size:10px;">TT / P.O/ SC/ CM / L/C Information</strong>

                                            </div>
                                            <div class="panel-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">TT / P.O/ SC/ CM / L/C No. :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($ipIcInfo[$inc]->tt_no)? $ipIcInfo[$inc]->tt_no : 'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">TT / P.O/ SC/ CM / L/C value :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($ipIcInfo[$inc]->tt_value)? $ipIcInfo[$inc]->tt_value : 'N/A') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Issuing Bank :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($banks[$ipIcInfo[$inc]->bank_id]))?$banks[$ipIcInfo[$inc]->bank_id]:'N/A' }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Type :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($bankTypes[$ipIcInfo[$inc]->bank_type])? $bankTypes[$ipIcInfo[$inc]->bank_type] : 'N/A') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Issue Date :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($ipIcInfo[$inc]->bank_invoice_date)?  date("d-M-Y", strtotime($ipIcInfo[$inc]->bank_invoice_date)) :'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;"></strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"></span></td>
                                                    </tr>
                                                </table>
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                    </div>
                                </div> <!--/panel-body-->
                            </div> <!--/panel-->
                            @if($inc%2 == 0)
                                <pagebreak />
                            @endif
                            <?php $inc++; ?>
                            @endforeach
                            @endif
                        </div>
                        <div id="moreInfoImd" class="clear"></div>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong style="font-size:10px;">Uploaded Supporting Documents</strong></div>
                            <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered">

                                    <tr>
                                        <td style="padding:5px;" width="5%"><strong style="font-size:10px;">No</strong></td>
                                        <td style="padding:5px;" width="40%"><strong style="font-size:10px;">Document Type</strong></td>
                                        <td style="padding:5px;" width="5%"><strong style="font-size:10px;"><img width="10" height="10" src="assets/images/attachment.png"/></strong></td>
                                        <td style="padding:5px;" width="5%"><strong style="font-size:10px;">No</strong></td>
                                        <td style="padding:5px;" width="40%"><strong style="font-size:10px;">Document Type</strong></td>
                                        <td style="padding:5px;" width="5%"><strong style="font-size:10px;"><img width="10" height="10" src="assets/images/attachment.png"/></strong></td>
                                    </tr>
                                    <tr>
                                        <?php $i = 1; ?>
                                        @foreach($document as $row)
                                            @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                <td style="padding:5px;" width="5%"><span style="font-size:10px;">{{ $i }} <?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></span></td>
                                                <td style="padding:5px;" width="40%"><span style="font-size:10px;">{{$row->doc_name }}</span></td>
                                                <td style="padding:5px;" width="5%"><span style="font-size:10px;">
                                                        @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                        <a href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}" target="_blank"><img width="10" height="10" src="assets/images/pdf.png" alt="{{ $clrDocuments[$row->doc_id]['file'] }}" /></a>
                                                        @endif
                                                    </span>
                                                </td>
                                                <?php if ($i % 2 == 0) echo '</tr><tr>'; ?>
                                                <?php $i++; ?>
                                            @endif
                                        @endforeach
                                    </tr>
                                </table>

                            </div><!-- /.panel-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection <!--- footer-script--->

