@extends('layouts.admin')

@section('content')

@include('partials.messages')

<?php
$accessMode = ACL::getAccsessRight('importPermit');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! Please call to system admin for more information.');
}
$user_type = CommonFunction::getUserType();
$desk_id = Auth::user()->desk_id;
?>

<section class="content" xmlns="http://www.w3.org/1999/html">

    <div class="box">
        <div class="box-body">

            @if(in_array($desk_id,array(3,4,5,6,7)))
            {!! Form::open(['url' => '/import-permit/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from', 
            'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
            @endif
            <div class="col-lg-12">
                <div class="with-border">
                    @if(in_array($desk_id,array(3,4,5,6,7)))
                    @include('importPermit::batch-process')
                    @endif {{-- checking desk --}}
                </div>
            </div>

            @if(empty($delegated_desk))
            <div class="modal fade" id="ProjectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" id="frmAddProject"></div>
                </div>
            </div>
            @endif {{-- checking not delegeted --}}

            <div class="col-lg-12">
                <div class="panel panel-red">
                    <div class="panel-heading">

                        @if($user_type == 5  || $user_type == 6)
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="btn" style="color:#ffffff">
                                    @if(!empty($delegated_desk))
                                    <i class="fa fa-list"></i> Delegate Application List
                                    @else
                                    <i class="fa fa-list"></i> <strong>Application List for import permit</strong>
                                    @endif {{-- checking if delegated --}}
                                </div>
                            </div>
                            <div class="col-lg-6 text-right">
                                <a href="{{URL::to('import-permit/form/')}}" class="">
                                    {!! Form::button('<i class="fa fa-plus"></i> <strong>New Application</strong>', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                                </a>
                                <a class="btn btn-default addAdvanceSearch"> <strong>Advanced Search </strong> <i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                            </div>
                        </div>

                        @else
                        @if(empty($delegated_desk))
                        <div class="row">
                            <div class="col-lg-6">
                                <i class="fa fa-list"></i>  Application list for import permit
                            </div>
                            <div class="col-lg-6 text-right">
                                <a class="btn btn-default addAdvanceSearch"> <strong>Advanced Search</strong>
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        @endif {{-- not empty delegated desk --}}

                        @endif {{-- checking user type 5, 6 --}}

                    </div><!-- /.panel-heading -->

                    <div class="panel-body" style="padding:0;">
                        <div class="panel panel-primary" id="innerAdvanceSearch" style="display:none;border-radius: 0 0 4px 4px;">
                            <div class="panel-body">


                                @if($user_type == 1  || $user_type == 4)
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">                                    
                                            <div class="alert alert-success">
                                                <h4>Economic Zone Search</h4>
                                                <div class="form-group" style="clear: both">
                                                    <div class="row">
                                                        <div class="col-md-10 {{$errors->has('eco_zone_id') ? 'has-error': ''}}">
                                                            {!! Form::label('eco_zone_id','Economic Zone :', ['class'=>'col-md-5 text-left']) !!}
                                                            <div class="col-md-7">
                                                                {!! Form::select('eco_zone_id', $economicZone, $zone, array('class'=>'form-control input-sm',
                                                                'placeholder' => 'Select an economic zone','id'=>"eco_zone_id",'onchange'=>"getIndustrialList(this)")) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-success btn-md adv_search-dynamic adv_search " value="Done" type="button"><b>Search</b></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--end of alert-success-->
                                        </div>

                                        <div class="col-md-6">                                    
                                            <div class="alert alert-info">
                                                <h4>Company Search</h4>
                                                <div class="form-group" style="clear: both">
                                                    <div class="row">
                                                        <div class="col-md-10 {{$errors->has('valid_applicant_name') ? 'has-error': ''}}">
                                                            {!! Form::label('valid_applicant_name','Approved company :', ['class'=>'col-md-5 text-left']) !!}
                                                            <div class="col-md-7">
                                                                {!! Form::select('valid_applicant_name', $validCompanies, '', array('class'=>'form-control input-sm',
                                                                'placeholder' => 'Select a company','id'=>"valid_applicant_name")) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-info btn-md adv_search-dynamic adv_search " value="Done" type="button"><b>Search</b></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--end of alert-info-->
                                        </div>
                                    </div>
                                </div>
                                @endif {{-- user type 5,6 --}}

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning">          
                                                <h4>General Search</h4>                            
                                                <div class="form-group" style="clear: both">
                                                    <div class="row">
                                                        <div class="col-md-6 {{$errors->has('tracking_number') ? 'has-error': ''}}">
                                                            {!! Form::label('tracking_number','Tracking Number', ['class'=>'col-md-5 text-left']) !!}
                                                            <div class="col-md-7">
                                                                <input class="form-control input-sm" placeholder="Tracking Number" name="tracking_number" type="text"
                                                                       id="tracking_number" value="{!! Session::has('tracking_no') ? Session::get('tracking_no'): ''!!}" 
                                                                       maxlength="100"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            {!! Form::label('applicant_name','Name ',['class'=>'col-md-5 text-left']) !!}
                                                            <div class="col-md-7">
                                                                <input class="form-control input-sm" placeholder="Name" name="applicant_name" type="text"
                                                                       id="applicant_name" maxlength="100"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="clear: both">
                                                    <div class="row">
                                                        <div class="col-md-6 {{$errors->has('modal_status_id') ? 'has-error': ''}}">
                                                            {!! Form::label('modal_status_id','Status:',['class'=>'text-left required-star col-md-5']) !!}
                                                            <div class="col-md-7">
                                                                {!! Form::select('modal_status_id', $status, '', array('class'=>'form-control input-sm',
                                                                'placeholder' => 'Select Status', 'id'=>"modal_status_id")) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            {!! Form::label('fax','Passport Number: ',['class'=>'text-left col-md-5']) !!}
                                                            <div class="col-md-7">
                                                                <input class="form-control input-sm" placeholder="Passport Number" name="passport_number" type="text"
                                                                       id="passport_number" maxlength="100"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="clear: both">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <button class="btn btn-danger btn-md closeSearch" value="Done" type="button"><b>Hide</b></button>
                                                            </div>
                                                            <div class="col-md-6  text-right">
                                                                <button class="btn btn-warning btn-md adv_search-dynamic adv_search" value="Done" type="button"><b>Search</b></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--end of alert-warning-->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!---/innerAdvanceSearch-->

                        <div class="row" id="numberOfAppsDiv">
                            @include('projectClearance::appsInDesk')
                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <table id="_list" class="table table-striped resultTable display" role="grid">
                                    <thead>
                                        <tr>
                                            <th>
                                                @if (in_array($desk_id, array(1,2,3,4,5,6,7)))
                                                {!! Form::checkbox('chk_id','chk_id','',['class'=>'selectall', 'id'=>'chk_id']) !!}
                                                @endif {{-- checking desk--}}
                                            </th>
                                            <th>#</th>
                                            <th>Tracking Id</th>
                                            <th width="15%">Applicant</th>
                                            <th>Serving Desk</th>
                                            <th>Status</th>
                                            <th>Modified</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $row_sl = 0; ?>
                                        @foreach($getList as $row)

                                        <?php $row_sl++ ?>
                                        <tr>
                                            <td>
                                                @if ($row->desk_id == $desk_id && $row->desk_id > 0 )
                                                {!! Form::checkbox('application[]',$row->record_id, '',['class'=>'appCheckBox', 'onChange' => 'changeStatus(this.checked)']) !!}
                                                {!! Form::hidden('hdn_batch[]',$row->status_id, ['class'=>'hdnStatus','id'=>"".$row->record_id."_status"]) !!}
                                                @endif
                                            </td>
                                            <td>{!! $row_sl !!}</td>
                                            <td>{!! $row->track_no !!}<br/></td>
                                            <td>{!! $row->applicant_name !!}</td>
                                            <td> @if($row->desk_id == 0) Applicant @else {!! $deskList[$row->desk_id] !!} @endif </td>
                                            <td>
                                                <span style="background-color: <?php echo $statusList[$row->status_id . 'color']; ?>; font-weight: bold;" class="label btn-sm">
                                                    {!! $statusList[$row->status_id]!!}
                                                </span>
                                            </td>
                                            <td>{!! CommonFunction::updatedOn($row->updated_at) !!}</td>
                                            <td>
                                                @if(ACL::getAccsessRight('importPermit','V'))
                                                <a href="{{url('import-permit/view/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" >
                                                    <i class="fa fa-folder-open-o"></i> View</a>
                                                @endif {{--cheking View permission --}}

                                                <!--chcking undertaking generated and status is not draft, discarded or rejected-->
                                                @if(!empty($row->undertaking) && !(in_array($row->status_id, array(-1,8,22))))
                                                <a href="{{url($row->undertaking)}}" target="_blank" class="btn btn-xs btn-warning open" >
                                                    <i class="fa fa-download" aria-hidden="true"></i> Undertaking</a>
                                                @endif {{--checking undertaking condition --}}

                                                @if(($row->status_id == -1 || $row->status_id == 5) && $row->initiated_by == Auth::user()->id )
                                                @if(ACL::getAccsessRight('importPermit','E'))
                                                <a href="{{url('import-permit/edit-form/'.Encryption::encodeId($row->record_id))}}"
                                                   class="btn btn-xs btn-primary open" ><i class="fa fa-folder-open-o"></i> Edit</a>
                                                @endif {{--cheking edit permission --}}
                                                <!-- 21 = approved and sent to custom, 28 > customs and security related status--->
                                                @elseif(!empty($row->certificate) && in_array($row->status_id, [21,28,29,30]))
                                                <a href="{{url($row->certificate)}}" target="_blank" class="btn btn-xs btn-info open" >
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Certificate</a>
                                                @if(Auth::user()->user_type == '1x101')
                                                <a href="/import-permit/discard-certificate/{{Encryption::encodeId($row->record_id)}}" class="btn btn-xs btn-danger open show-in-view " onclick="return confirm('Are you sure??')"
                                                   title="Discard Certificate"><i class="fa fa-trash" aria-hidden="true"></i> Discard Certificate</a>
                                                @endif {{-- checking system admin --}}
                                                @endif {{-- checking edit condition and certificate availbility status --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>


                                <table id="search_result_list" class="table table-striped searchResultTbl display hidden" role="grid">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tracking Id</th>
                                            <th>Applicant</th>
                                            <th>Serving Desk</th>
                                            <th>Status</th>
                                            <th>Modified</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    @if(in_array($desk_id,array(3,4,5,6,7,8)))
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('footer-script')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
@include('partials.datatable-scripts')
<script language="javascript">
    var numberOfCheckedBox = 0;
    var curr_app_id = '';
    function setCheckBox() {
        numberOfCheckedBox = 0;
        var flag = 1;
        var selectedWO = $("input[type=checkbox]").not(".selectall");
        selectedWO.each(function () {
            if (this.checked) {
                numberOfCheckedBox++;
            } else {
                flag = 0;
            }
        });
        if (flag == 1) {
            $("#chk_id").checked = true;
        } else {
            $("#chk_id").checked = false;
        }
        if (numberOfCheckedBox >= 1) {
            $('.applicable_status').trigger('click');
        }
    } // end of setCheckBox()

    function changeStatus(check) {
        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
        setCheckBox();
    } // end of changeStatus();

    $(document).ready(function () {
        $('#status_id').html('<option selected="selected" value="">Select Below</option>');

        $('#_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "iDisplayLength": 50
        });
        $('#search_result_list_wrapper').addClass('hidden');
        $("#apps_from").validate({
            errorPlacement: function () {
                return false;
            }
        });
        $("#batch_from").validate({
            errorPlacement: function () {
                return false;
            }
        });
        $(".send").click(function () {
            var all_app_id = [];
            $('.appCheckBox').each(function () {
                if ($(this).is(':checked')) {
                    all_app_id.push($(this).val());
                }
            });
            if (all_app_id == '') {
                alert("Please select any application");
                return false;
            }
        });
        $(".addAdvanceSearch").click(function () {
            $('#innerAdvanceSearch').slideDown();
            $(this).find('i').removeClass("fa fa-arrow-down");
            $(this).find('i').addClass("fa fa-arrow-up");
            $(".addAdvanceSearch").css("background-color", "#1abc9c");
            $(".addAdvanceSearch").css("color", "white");
        });
        $(".closeSearch").click(function () {
            $('#innerAdvanceSearch').slideUp();
            $('.addAdvanceSearch').find('i').removeClass("fa-arrow-up fa");
            $('.addAdvanceSearch').find('i').addClass("fa fa-arrow-down");
            $(".addAdvanceSearch").css("background-color", "");
            $(".addAdvanceSearch").css("color", "");
        });
        $(".process").click(function () {
            if (numberOfCheckedBox == 0) {
                alert('Select Application');
                return false;
            }
        });
        var base_checkbox = '.selectall';
        $(base_checkbox).click(function () {
            if (this.checked) {
                $('.appCheckBox:checkbox').each(function () {
                    this.checked = true;
                    $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                });
            } else {
                $('.appCheckBox:checkbox').each(function () {
                    this.checked = false;
                    $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                });
            }
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            setCheckBox();
        });
        $('.appCheckBox:checkbox').not(base_checkbox).click(function () {
            $(".selectall").prop("checked", false);
        });
        var break_for_pending_verification = 0;
        $(".applicable_status").click(function () {
            // $("#status_id").trigger("click");
            var all_app_id = [];
            break_for_pending_verification = 0;
            $('.appCheckBox').each(function () {
                if ($(this).is(':checked')) {
                    all_app_id.push($(this).val());
                }
            });
            if (all_app_id == '') {
                alert("Please select any application");
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                return false;
            } else {
                $('#status_id').attr('disabled', false);
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                curr_app_id = all_app_id[0];
                var curr_status_id = $("#" + curr_app_id + "_status").val();
                $.ajaxSetup({async: false});

                $.each(all_app_id, function (j, i) {
                    if (break_for_pending_verification == 1) {
                        return false;
                    }
                    var tmp_curr_status = $("#" + i + "_status").val();
                    if (curr_status_id != tmp_curr_status) {
                        alert('Please select application of same status...');
                        $('#status_id').attr('disabled', false);
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                        return false;
                    } else {
                        var _token = $('input[name="_token"]').val();
                        var delegate = '{{ @$delegated_desk }}';
                        var state = false;
                        $.post('/import-permit/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate,
                            _token: _token}, function (response) {

                            if (response.responseCode == 1) {
                                var option = '';
                                option += '<option selected="selected" value="">Select Below</option>';
                                $.each(response.data, function (id, value) {
                                    option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                                });
                                $("#status_id").html(option);
                                $("#status_id").focus();
                            } else if (response.responseCode == 5) {
                                alert('Without verification, application can not be processed');
                                break_for_pending_verification = 1;
                                option = '<option selected="selected" value="">Select Below</option>';
                                $("#status_id").html(option);
                                return false;
                            } else {
                                $('#status_id').html('Please wait');
                            }
                        });
                    }
                });
                $.ajaxSetup({async: true});
            }
        }); // end of $(".applicable_status").click(function ()

        $(document).on('change', '.status_id', function () {
            var object = $(".status_id");
            var obj = $(object).parent().parent().parent();
            var id = $(object).val();
            var _token = $('input[name="_token"]').val();
            var status_from = $('#status_from').val();
            $('#sendToDeskOfficer').css('display', 'block');
            if (id == 0) {
                obj.find('.param_id').html('<option value="">Select Below</option>');
            } else {
                $.post('/import-permit/ajax/process', {id: id, curr_app_id: curr_app_id, status_from: status_from, _token: _token}, function (response) {
                    if (response.responseCode == 1) {
                        var option = '';
                        option += '<option selected="selected" value="">Select Below</option>';
                        var countDesk = 0;
                        $.each(response.data, function (id, value) {
                            countDesk++;
                            option += '<option value="' + id + '">' + value + '</option>';
                        });
                        obj.find('#desk_id').html(option);
                        $('#desk_id').attr("disabled", false);
                        $('#remarks').attr("disabled", false);
                        if (countDesk == 0) {
                            $('.dd_id').removeClass('required');
                            $('#sendToDeskOfficer').css('display', 'none');
                        } else {
                            $('.dd_id').addClass('required');
                        }
                        if (response.status_to == 5 || response.status_to == 8 || response.status_to == 16 || response.status_to == 17 || response.status_to == 19
                                || response.status_to == 24 || response.status_to == 10 || response.status_to == 22) {
                            $('#remarks').addClass('required');
                            $('#remarks').attr("disabled", false);
                        } else {
                            $('#remarks').removeClass('required');
                        }
                        if (response.file_attach == 1) {
                            $('#sendToFile').css('display', 'block');
                        } else {
                            $('#sendToFile').css('display', 'none');
                        }
                    } // end if (response.responseCode == 1)
                });     //end of $.post('/import-permit/ajax/process')
            }
        }); // $(document).on('change', '.status_id', function()
    }); // end of document.ready

    function resetElements() {
        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
    }

    $(".adv_search").on('click', function () {
        $("#search_result_list").dataTable().fnDestroy();
        $('.selectall').addClass("hidden");
        var _token = $('input[name="_token"]').val();
        var tracking_number = $('#tracking_number').val();
        var passport_number = $('#passport_number').val();
        var applicant_name = $('#applicant_name').val();
        var status_id = $('#modal_status_id').val();
        var valid_applicant_name = $('#valid_applicant_name').val();
        var eco_zone_id = $('#eco_zone_id').val();

        // to restrict users to select at lest one option
        if (tracking_number == '' && status_id == '' && valid_applicant_name == '' && eco_zone_id == '') {
            alert('You have to enter either tracking number or select any status, company name or economic zone to continue!');
            return;
        }

        $.ajax({
            url: base_url + '/import-permit/search-result',
            type: 'post',
            data: {
                _token: _token,
                tracking_number: tracking_number,
                passport_number: passport_number,
                applicant_name: applicant_name,
                status_id: status_id,
                valid_applicant_name: valid_applicant_name,
                eco_zone_id: eco_zone_id,
            },
            dataType: 'json',
            success: function (response) {
                if (response.responseCode == 1) {
                    var str = response.totalApps;
                    var appsArray = str.split("==");

                    for (var i = 0; i < appsArray.length; i++) {
                        var temp = appsArray[i];
                        var searchResultInfo = temp.split(":");
                        $('#' + searchResultInfo[0]).html(searchResultInfo[1]);
                        var url = $('#URL' + searchResultInfo[0]).attr("href");
                        $('#URL' + searchResultInfo[0]).attr("href", url + "/" + searchResultInfo[2] + "/" + searchResultInfo[3]);
                    }

                    $('#_list_wrapper').addClass('hidden');
                    $('#search_result_list_wrapper').removeClass('hidden');
                    $('#search_result_list').removeClass('hidden');
                    $('table.searchResultTbl tbody').html(response.data);
                    $('.closeSearch').trigger('click');
                } else {
                    $('#_list_wrapper').removeClass('hidden');
                }
                $("#search_result_list").dataTable().fnDestroy();
                $('#search_result_list').DataTable({
                    searching: true,
                    "paging": true,
                    "lengthChange": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true,
                    "iDisplayLength": 50
                });

                if ($('#valid_applicant_name').val() != '' || eco_zone_id != '') {
                    $('#numberOfAppsDiv').removeClass('hidden');
                } else {
                    $('#numberOfAppsDiv').addClass('hidden');
                }
                // To reset the previous value
                $('#tracking_number').val('');
                $('#passport_number').val('');
                $('#applicant_name').val('');
                $('#modal_status_id').prop('selectedIndex', 0);
                $('#eco_zone_id').prop('selectedIndex', 0);

                $('#valid_applicant_name').find('option').remove().end();
                $('#valid_applicant_name').append('<option value="">Select Economic zone first</option>');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            beforeSend: function (xhr) {
                console.log('before send');
            },
            complete: function () {
                //completed
            }
        });
    });
<?php
if ($ind > 0 || $zone > 0) {
    ?>
        $(".adv_search-dynamic").trigger('click');
<?php } ?>
</script>
@endsection
