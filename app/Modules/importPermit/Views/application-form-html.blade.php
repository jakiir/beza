@extends('layouts.pdfGen')

@section('content')
@include('partials.messages')

<section class="content" id="projectClearanceForm">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;margin-bottom:10px;">
                        <img src="assets/images/logo_beza_single.png"/><br/>
                        BEZA::Bangladesh Economic Zones Authority<br/>
                        Application for Import Permitnce
                    </div>
                </div>              

                <div class="panel panel-red" id="inputForm">
                    <div class="panel-heading">Application for Import Permit </div>
                    <div class="panel-body" style="margin:6px;">

                        <section class="content-header">
                            <table width="100%">
                                <tr>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Tracking no. : </strong><span style="font-size:10px;">{{ $process_data->track_no  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;"> Date of Submission: </strong><span style="font-size:10px;"> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Current Status : </strong><span style="font-size:10px;">
                                            @if($form_data->status_id == -1) Draft
                                            @else {!! isset($form_data->status_id) && $statusArray[$form_data->status_id] !!}
                                            @endif</span>
                                    </td>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Current Desk :</strong><span style="font-size:10px;">
                                            @if(isset($process_data->desk_id) && $process_data->desk_id != 0)
                                                {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                            @else
                                                Applicant
                                            @endif
                                        </span></td>
                                </tr>

                                <tr>
                                    <td style="padding:5px; font-size:10px">
                                            <?php if (isset($form_data->status_id) && $form_data->status_id == 23 && isset($form_data->certificate)) { ?>
                                            <a href="{{ url($form_data->certificate) }}"
                                               title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                            <?php } ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </section>


                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong style="font-size:10px;">General Information</strong></div>
                            <div class="panel-body">
                                <table width="100%">
                                    @if(isset($alreadyExistApplicant->ip_number))
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Import Permit (IP) No : </strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{$alreadyExistApplicant->ip_number}}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;"></strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"></span></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Applicant Name : </strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">
                                                {{ (isset($alreadyExistApplicant->applicant_name)? $alreadyExistApplicant->applicant_name : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;"></strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"></span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Import Permit Type : </strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">
                                                {{ (isset($alreadyExistApplicant->permit_type) && $alreadyExistApplicant->permit_type != 0 && $alreadyExistApplicant->permit_type != '') ? 
                                                    $ip_permit_type[$alreadyExistApplicant->permit_type] : 'N/A' }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Carrier Type :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->carrier_type) ? $carrier_type[$alreadyExistApplicant->carrier_type] : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Type of Zone :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->zone_type) ? $alreadyExistApplicant->zone_type : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Name of Economic Zone :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->eco_zone_id) ? $economicZone[$alreadyExistApplicant->eco_zone_id] : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Undertaking No. :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->undertaking_no) ? $alreadyExistApplicant->undertaking_no : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Undertaking Date :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->undertaking_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->undertaking_date, 0, 10)) : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Invoice/Vendor Reference No. :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->invoice_ref_no) ? $alreadyExistApplicant->invoice_ref_no : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Invoice/Vendor Ref. Date :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->invoice_ref_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->invoice_ref_date, 0, 10)) : 'N/A') }}</span></td>
                                    </tr>

                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">B/L, A/W Bill, T.C No. :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->aw_bill_no) ? $alreadyExistApplicant->aw_bill_no : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">FoB/CIF/CFR/C&F Value :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->fob_value) ? $currencies[$alreadyExistApplicant->fob_value] : 'N/A') }}</span> <br/> <span style="font-size:10px;">{{ (isset($alreadyExistApplicant->fob_cif_value) ? $alreadyExistApplicant->fob_cif_value : 'N/A') }}</span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Name of the Shipper :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->shipper_name) ? $alreadyExistApplicant->shipper_name : 'N/A') }}</span></td>
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Import from / Port of loading :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->import_port) ? $alreadyExistApplicant->import_port : 'N/A') }}</span></td>
                                    </tr>
                                    <tr> @if( isset($alreadyExistApplicant->permit_type) && ($alreadyExistApplicant->permit_type == 5 || $alreadyExistApplicant->permit_type == 6))
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Destination Zone :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->destination_zone) ? $allEcoZones[$alreadyExistApplicant->destination_zone] : 'N/A') }}</span></td>
                                        @else
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Source Country :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->source_country) ? $countries[$alreadyExistApplicant->source_country] : 'N/A') }}</span></td>
                                        @endif
                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Port of destination :</strong></td>
                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($alreadyExistApplicant->destination_port) ? $destination_ports[$alreadyExistApplicant->destination_port] : 'N/A') }}</span></td>
                                    </tr>
                                </table>
                            </div> <!--/panel-body-->
                        </div> <!--/panel-->
                        @if(count($meterials) > 0)
                            <?php $inc = 0;  ?>

                        <div id="templateImdFullPar">
                            @foreach($meterials as $eachmeterials)
                                <div class="panel panel-black templateImdFull">
                                <div class="panel-heading"><strong style="font-size:10px;"><i class="fa fa-list"></i> 
                                        Import Materials &amp; L/C Information Group</strong><span class="pull-right"></span>
                                </div>
                                <div class="panel-body" style="margin: 4px;">

                                    <div id="edidMoreInfoMd {{ $inc }}">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <span class=""><strong style="font-size:10px;">Import Material Details</strong></span>
                                            </div>
                                            <div class="panel-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Type of material :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">
                                                                {{ ( isset($eachmeterials->material_type) &&  isset($material_type[$eachmeterials->material_type]) ) ?
                                                                    $material_type[$eachmeterials->material_type] : 'N/A' }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">HS Code :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($eachmeterials->hs_code)? $eachmeterials->hs_code : 'N/A') }}</span><br/><span style="font-size: 10px;">{{ (isset($eachmeterials->hs_product)?$eachmeterials->hs_product:'') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Material description :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">
                                                                {{ (isset($eachmeterials->material_description)? $eachmeterials->material_description : 'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">FoB / CNF / CIF / CRF / X-Factory / DDU / Replacement / Free of Cost value :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"> 
                                                                {{ (isset($eachmeterials->fob_usd) && $eachmeterials->fob_usd != ''  && isset($eachmeterials->fob_usd) )?
                                                                    $currencies[$eachmeterials->fob_usd] : 0 }}</span><span style="font-size:10px;"><br/> 
                                                                        {{ (isset($eachmeterials->fob_usd_value) ? $eachmeterials->fob_usd_value: 'N/A') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Unit of Quantity :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">
                                                                {{ (isset($eachmeterials->mat_quantity_unit) && $eachmeterials->mat_quantity_unit != 0) ? 
                                                                    $quantity_unit[$eachmeterials->mat_quantity_unit] : 'N/A' }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Quantity :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">
                                                                {{ (isset($eachmeterials->mat_quantity) ? $eachmeterials->mat_quantity : 'N/A') }}</span></td>
                                                    </tr>
                                                </table>
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                    </div>


                                    <div id="edidMoreInfoPo">
                                        <div class="panel panel-primary templateTtPo" id="templateTtPo">
                                            <div class="panel-heading"><strong style="font-size:10px;">TT / P.O/ SC/ CM / L/C Information</strong>

                                            </div>
                                            <div class="panel-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">TT / P.O/ SC/ CM / L/C No. :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($ipIcInfo[$inc]->tt_no)? $ipIcInfo[$inc]->tt_no : 'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">TT / P.O/ SC/ CM / L/C value :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($ipIcInfo[$inc]->tt_value)? $ipIcInfo[$inc]->tt_value : 'N/A') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Issuing Bank :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($banks[$ipIcInfo[$inc]->bank_id]))?$banks[$ipIcInfo[$inc]->bank_id]:'N/A' }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Type :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($bankTypes[$ipIcInfo[$inc]->bank_type])? $bankTypes[$ipIcInfo[$inc]->bank_type] : 'N/A') }}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Issue Date :</strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (isset($ipIcInfo[$inc]->bank_invoice_date)?  date("d-M-Y", strtotime($ipIcInfo[$inc]->bank_invoice_date)) :'N/A') }}</span></td>
                                                        <td style="padding:5px;" width="25%"><strong style="font-size:10px;"></strong></td>
                                                        <td style="padding:5px;" width="25%"><span style="font-size:10px;"></span></td>
                                                    </tr>
                                                </table>
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                    </div>
                                </div> <!--/panel-body-->
                            </div> <!--/panel-->
                                    <?php $inc++; ?>
                            @endforeach
                            @endif
                        </div>
                        <div id="moreInfoImd" class="clear"></div>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong style="font-size:10px;">Required  Documents for attachment</strong></div>
                            <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered">

                                    <tr>
                                        <td style="padding:5px;" width="10%"><strong style="font-size:10px;">No</strong></td>
                                        <td style="padding:5px;" width="45%"><strong style="font-size:10px;">Required Attachments</strong></td>
                                        <td style="padding:5px;" width="45%"><strong style="font-size:10px;">Attached PDF file </strong></td>
                                    </tr>
                                    <?php $i = 1; ?>
                                    @foreach($document as $row)
                                    <tr>
                                        <td style="padding:5px;" width="10%"><span style="font-size:10px;">{{ $i }} <?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></span></td>
                                        <td style="padding:5px;" width="45%"><span style="font-size:10px;">{{$row->doc_name }}</span></td>
                                        <td style="padding:5px;" width="45%"><span style="font-size:10px;">
                                                @if(isset($clrDocuments[$row->doc_id]['file']))
                                                    <a href="{{URL::to('/uploads/'.(isset($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="{{ $clrDocuments[$row->doc_id]['file'] }}" /> <?php if(isset($clrDocuments[$row->doc_id]['file'])){ $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name);} ?></a>
                                                @endif
                                            </span>
                                        </td>
                                    </tr>
                                            <?php $i++; ?>
                                        @endforeach
                                </table>

                            </div><!-- /.panel-body -->
                        </div>

                        {{--@if($form_data->status_id > 20)
                            <div style="margin-top:6px;">
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding: 20px;" class="alert alert-info">
                                            The goods specified in the invoice(s) are eligible for exemption for the whole of the customs duty, sales tax etc. vide S.R.O. No. 209-LAW/2015/46/Customs dated July 1,2015 issued by the Internal Resources Division, Ministry of Finance, Dhaka
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding: 20px;" class="alert alert-danger">
                                            <b>
                                                THIS PERMIT IS VALID FOR 30 (THIRTY) DAYS FROM THE DATE OF ISSUE
                                            </b>
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding: 20px;" class="alert alert-warning">
                                            <b>
                                                THIS IS COMPUTER GENERATED PERMIT. SO SIGNATURE IS NOT REQUIRED
                                            </b>
                                            <br/>
                                            <br/>
                                            @if(isset($alreadyExistApplicant->ip_number))
                                                <img src="data:image/png;base64,{{DNS1D::getBarcodePNG( $alreadyExistApplicant->ip_number, 'C39')}}" alt="barcode" width="250"/>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    @endif--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection <!--- footer-script--->

