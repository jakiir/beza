@extends('layouts.admin')

@section('content')
    @include('partials.messages')
    <?php
    $accessMode = ACL::getAccsessRight('importPermit');
    if (!ACL::isAllowed($accessMode, $mode)) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    ?>

    <style>
        .text-title{ font-size: 16px !important}
        .text-sm{ font-size: 9px !important}
        #importPermitForm label.error {display: none !important; }
        .calender-icon{
            border: medium none; padding-top: 30px ! important;
        }
        .input-sm {
            padding: 3px 6px;
        }
        .form-group{
            margin-bottom: 2px;
        }
        @media screen and (max-width: 767px) {
            #dropdown-responsive {
                padding-left: 30px;
                width: 5%;
            }
        }
        .pull-right {
            float: right;
            margin-bottom: 3px;
            margin-left: 5px;
        }
        .borderless {
            border-top-style: none;
            border-left-style: none;
            border-right-style: none;
            border-bottom-style: none;
        }
    </style>
    <section class="content" id="projectClearanceForm">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    @if($errors->all())
                        <div class="alert alert-danger"><ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul></div>
                    @endif

                    @if($viewMode == 'on')
                        <div class="row">
                            <div class="col-md-12">
                                @if(isset($form_data->status_id) && $form_data->status_id != 8)
                                <div class="col-md-12 pull-right">
                                        <a href="/import-permit/view-pdf/{{ Encryption::encodeId($form_data->id)}}" target="_blank"
                                           class="btn btn-danger btn-sm pull-right">
                                            <i class="fa fa-download"></i> <strong>Application Download as PDF</strong>
                                        </a>
                                </div>  
                                @endif

                                @if($viewMode == "on" && $form_data->status_id == 21)
                                    <?php
                                    $date1 = $process_data->updated_at;
                                    $date2 = date('Y-m-d h:i:s');
                                    $diff = abs(strtotime($date2) - strtotime($date1));
                                    $years = floor($diff / (365*60*60*24));
                                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                    ?>

                                    @if($days >= 30 || $months > 0) {{-- after 30 days is over after payment accepting --}}

                                    {!! link_to('#'. Encryption::encodeId($alreadyExistApplicant->id),'Cancel',['class' => 'btn btn-primary btn-sm pull-right','data-toggle'=>'modal','data-target'=>'#myModal']) !!}
                                    {!! link_to('import-permit/view-pdf/'. Encryption::encodeId($alreadyExistApplicant->id),'Resend',['class' => 'btn btn-primary btn-sm pull-right','target'=>'_blank']) !!}

                                    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    {!! Form::open(array('url' => 'apps/upload-sb-file/','method' => 'post', 'class' => 'form-horizontal', 'id' => 'bulkUpload',
                                                    'enctype' =>'multipart/form-data', 'files' => 'true')) !!}
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    {!! Form::label('tr_no','Tracking No.:',['class'=>'col-sm-4 control-label']) !!}
                                                                    <div class="col-sm-8">
                                                                        <input type="text" value="{{ $process_data->track_no  }}" readonly class="form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    {!! Form::label('dos','Date of Submission:',['class'=>'col-sm-4 control-label']) !!}
                                                                    <div class="col-sm-8">
                                                                        <input type="text" value="{{ CommonFunction::formateDate($process_data->created_at)  }}" readonly class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    {!! Form::label('dos','Remark:',['class'=>'col-sm-4 control-label']) !!}
                                                                    <div class="col-sm-8">
                                                                        <textarea class="form-control"></textarea>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-info pull-right">Cancel Request</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    @endif  {{-- checking after 30 days is over after payment accepting --}}
                                @endif {{-- checking view mode on --}}
                            </div>
                        </div>

                        <!--3 = RD1, 4 = RD2, 5 = RD3, 6 = RD4, 7 = Customs Officer, 9 = security-->
                        @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(3,4,5,6,7)))
                            {!! Form::open(['url' => '/import-permit/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from',
                            'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                            @include('importPermit::batch-process')
                            <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                            <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                            {!! Form::close() !!}
                        @endif {{-- checking desk id 3,4,5,6,7,9 --}}

                        @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(9)))
                            @if($remaining_quntities)
                                <?php $remainingCounter=0; ?>
                                @foreach($remaining_quntities as $remaining_quntity)
                                    @if($remaining_quntity->mat_remaining_quantity > 0)
                                        <?php $remainingCounter++; ?>
                                    @endif
                                @endforeach
                            @endif

                            @if($remainingCounter == 0)
                                {!! Form::open(['url' => '/import-permit/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from',
                                'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                                @include('importPermit::batch-process')
                                <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                                <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                                {!! Form::close() !!}
                            @endif

                        @endif {{-- checking desk id 9 --}}

                        {{-- desk 9 is security --}} {{-- status 30 is custom verified, 31 is gate pass partial issue --}}
                        @if(in_array(Auth::user()->desk_id,array(9)) && ($alreadyExistApplicant->status_id == 30 || $alreadyExistApplicant->status_id == 31) )
                            @if($remainingCounter != 0)
                                @include('importPermit::gatepass')
                            @endif
                        @endif {{-- checking security desk --}}

                        @if(count($gpHistory) > 0)
                            @if( in_array(Auth::user()->desk_id,array(3,4,5,6,7,9,10)) || $alreadyExistApplicant->created_by == Auth::user()->id )
                                @include('importPermit::gatepass-history')
                            @endif {{-- checking desk id 3,4,5,6,7,9--}}
                        @endif {{-- checking gp history --}}

                    @endif {{-- checking view mode on --}}

                    @if($viewMode == "on")
                        <?php
                        $date1 = $process_data->updated_at;
                        $date2 = date('Y-m-d h:i:s');
                        $diff = abs(strtotime($date2) - strtotime($date1));
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                        ?>

                        @if($days >= 30 || $months > 0)
                            <div class="panel panel-primary" id="ep_form">
                                <div class="panel-heading">Process EP Extension / Cancellation</div>
                                <div class="panel-body">
                                    @if(isset($alreadyExistApplicant->extension_cancellation))
                                        @if($alreadyExistApplicant->extension_cancellation>0)
                                            <div class="row-fluid">
                                                @if($alreadyExistApplicant->extension_cancellation==1)
                                                    <b>Extended Date:</b> {{$alreadyExistApplicant->extension_date}}<br/>
                                                    <b>Remarks:</b> {{$alreadyExistApplicant->extension_remark}}
                                                @elseif($alreadyExistApplicant->extension_cancellation==2)
                                                    <b>Cancellation Remarks:</b> {{$alreadyExistApplicant->cancellation_remark}}
                                                @endif
                                            </div>

                                        @endif
                                    @else
                                        <div id="extension_cancellation_error" style="color:red;"></div>
                                        <div class="col-md-12">
                                            <table width="100%" class="table borderless">
                                                <tr>
                                                    <th width="10%">Apply For</th>
                                                    <td width="10%">
                                                        <input value="extension" class="ext_chng" id="extension" type="radio" name="extension"> Extension<br/>
                                                        <input value="cancel" class="ext_chng" id="cencel" type="radio" name="extension"> Cancel
                                                    </td>
                                                    <td>
                                                        <span id="ext_upto"><label class="label-inline">Extension Up to</label>
                                                    </td>
                                                    <td>
                                                        <input id="ext_upto_txt" style="height:30px;" type="text" name="extension_upto" class="form-control datepicker"></span>
                                                    </td>
                                                    <td>
                                                        <label>Reason for Extension / Cancellation <span style="color:red">*</span></label>
                                                    </td>
                                                    <td>
                                                        <textarea style="height:30px;" rows="1" class="form-control" name="extension_cancellation_remark"></textarea>
                                                    </td>
                                                    <td>
                                                        <input type="submit" name="apply" id="extension_cancellation" class="btn btn-sm btn-success"
                                                               value="Apply">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                </div>
                                @endif
                            </div>
                        @endif
                    @endif

                    <div class="panel panel-red"  id="inputForm">
                        <div class="panel-heading">Application for Import Permit </div>
                        <div class="panel-body" style="margin:6px;">
                            <?php if ($viewMode != 'on') { ?>
                            {!! Form::open(array('url' => 'import-permit/store-app','method' => 'post','id' => 'importPermitForm','role'=>'form')) !!}
                            <input type ="hidden" name="app_id" value="{{(isset($alreadyExistApplicant->id) ?
                                Encryption::encodeId($alreadyExistApplicant->id) : '')}}">
                            <input type="hidden" name="selected_file" id="selected_file" />
                            <input type="hidden" name="validateFieldName" id="validateFieldName" />
                            <input type="hidden" name="isRequired" id="isRequired" />
                            <?php } ?>

                            <?php if ($viewMode == 'on') { ?>
                            <section class="content-header">
                                <ol class="breadcrumb">
                                    <li><strong>Tracking no. : </strong>{{ $process_data->track_no  }}</li>
                                    <li>
                                        <strong> Date of Submission: </strong>
                                        <?php
                                        $submit_date =   isset($submissionDate) ? \App\Libraries\CommonFunction::formateDate($submissionDate) :
                                                \App\Libraries\CommonFunction::formateDate($process_data->created_at); ?>
                                        {{ $submit_date  }}
                                    </li>
                                    <li><strong>Current Status : </strong> @if($form_data->status_id == -1) Draft
                                        @else {!! $statusArray[$form_data->status_id] !!}
                                        @endif
                                    </li>
                                    <li>
                                        @if($process_data->desk_id != 0)
                                            <strong>Current Desk :</strong> {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                        @else
                                            <strong>Current Desk :</strong> Applicant
                                        @endif {{-- process desk id is 0 --}}
                                    </li>
                                    @if(isset($alreadyExistApplicant->status_id) && $alreadyExistApplicant->status_id == 8)
                                    <li>
                                        <strong>Discard Reason :</strong> {{ (isset($alreadyExistApplicant->remarks)?$alreadyExistApplicant->remarks:'N/A') }}
                                    </li>
                                    @endif
                                    <li>
                                        <!-- 21 = approved and sent to custom, 28 > customs and security related status--->
                                        @if(isset($form_data->status_id) && in_array($form_data->status_id, [21,28,29, 30,31]) && isset($form_data->certificate))
                                            <a class="btn btn-xs btn-info show-in-view" href="{{ url($form_data->certificate) }}" title="Download Certificate"
                                               target="_blank"> <i class="fa  fa-file-pdf-o"></i><b> Download Certificate</b></a>
                                            @if(Auth::user()->user_type == '1x101')
                                                <a class="btn btn-xs btn-danger show-in-view" href="/import-permit/discard-certificate/{{ Encryption::encodeId($alreadyExistApplicant->id) }}"
                                                   onclick="return confirm('Are you sure??')" title="Discard Certificate">
                                                    <i class="fa  fa-trash"></i><b> Discard Certificate</b></a>
                                            @endif
                                        @endif
                                        @if(isset($form_data) && (in_array($form_data->status_id,[21])) && isset($form_data->certificate))
                                        <a href="/import-permit/import-cer-re-gen/{{ Encryption::encodeId($alreadyExistApplicant->id)}}" class="btn show-in-view btn-xs btn-warning"
                                           title="Re-generate certificate" target="_self"> <i class="fa  fa-file-pdf-o"></i> <b>Re-generate certificate</b></a>
                                        @endif
                                    </li>
                                </ol>
                            </section>

                            <?php
                            }
                            $allRequestVal = old();
                            ?>

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>1. General Information</strong></div>
                                <div class="panel-body">

                                    <div class="col-md-12">

                                        <div class="form-group" style="">
                                            @if ($viewMode == 'on' && isset($alreadyExistApplicant->applicant_name))
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        {!! Form::label('applicant_name',' Applicant Name:' , ['class'=>'col-md-5 text-left required-star']) !!}
                                                        <div class="col-md-7">
                                                            <span style="background-color:#ddd;width:100%;padding:6px;display:block;display:block">{{(isset($alreadyExistApplicant->applicant_name)?$alreadyExistApplicant->applicant_name:'N/A')}}</span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <br/>
                                            @endif
                                            <div class="row">

                                                <div class="col-md-6">
                                                    {!! Form::label('permit_type','Import Permit Type :' , ['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('permit_type') ? 'has-error': ''}}">
                                                        {!! Form::select('permit_type', $ip_permit_type, (isset($alreadyExistApplicant->permit_type) ? $alreadyExistApplicant->permit_type : ''),
                                                        ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('carrier_type','Carrier Type :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('carrier_type') ? 'has-error': ''}}">
                                                        {!! Form::select('carrier_type', $carrier_type, (isset($alreadyExistApplicant->carrier_type) ? $alreadyExistApplicant->carrier_type : ''),
                                                        ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    {!! Form::label('zone_type','Type of Zone :', ['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {{ $zone_type }}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('eco_zone_id','Economic Zone :',['class'=>'col-md-5 text-left required-star']) !!}
                                                    <div class="col-md-7">
                                                        {{ isset($economicZone->zone) ? $economicZone->zone : $economicZone }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    {!! Form::label('undertaking_no','Undertaking No. :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('undertaking_no') ? 'has-error': ''}}">
                                                        {!! Form::text('undertaking_no',  (isset($alreadyExistApplicant->undertaking_no) ? $alreadyExistApplicant->undertaking_no : ''),
                                                        ['class' => 'form-control input-sm required']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('undertaking_date','Undertaking Date :',['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7">
                                                        <div class="datepicker input-group date">
                                                            {!! Form::text('undertaking_date', (isset($alreadyExistApplicant->undertaking_date) && $alreadyExistApplicant->undertaking_date != '0000-00-00' ?
                                                            App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->undertaking_date, 0, 10)) : ''),
                                                            ['class' => 'form-control  input-sm required']) !!}
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    {!! Form::label('invoice_ref_no','Invoice/Vendor Reference No. :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('invoice_ref_no') ? 'has-error': ''}}">
                                                        {!! Form::text('invoice_ref_no',  (isset($alreadyExistApplicant->invoice_ref_no) ? $alreadyExistApplicant->invoice_ref_no : ''),
                                                        ['class' => 'form-control input-sm required']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('invoice_ref_date','Invoice/Vendor Ref. Date :',['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7">
                                                        <div class="datepicker input-group date">
                                                            {!! Form::text('invoice_ref_date', (isset($alreadyExistApplicant->invoice_ref_date) && $alreadyExistApplicant->invoice_ref_date != '0000-00-00' ?
                                                            App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->invoice_ref_date, 0, 10)) : ''),
                                                            ['class' => 'form-control  input-sm required']) !!}
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    {!! Form::label('aw_bill_no','B/L, A/W Bill, T.C No. :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('aw_bill_no') ? 'has-error': ''}}">
                                                        {!! Form::text('aw_bill_no',  (isset($alreadyExistApplicant->aw_bill_no) ? $alreadyExistApplicant->aw_bill_no : ''),
                                                        ['class' => 'form-control input-sm required']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('fob_value','FoB/CIF/CFR/C&F Value :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('fob_value') ? 'has-error': ''}}">
                                                        {!! Form::select('fob_value', $currencies,  (isset($alreadyExistApplicant->fob_value) ? $alreadyExistApplicant->fob_value : '107'),
                                                        ['class' => 'input-sm required col-md-4 required', 'placeholder' => ' Select One']) !!}
                                                        <div class="col-md-1"></div>
                                                        {!! Form::text('fob_cif_value',  (isset($alreadyExistApplicant->fob_cif_value) ? $alreadyExistApplicant->fob_cif_value : ''),
                                                        ['class' => 'clone-form-control input-sm required col-md-7 onlyNumber']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    {!! Form::label('shipper_name','Name of the Shipper :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('shipper_name') ? 'has-error': ''}}">
                                                        {!! Form::text('shipper_name',  (isset($alreadyExistApplicant->shipper_name) ? $alreadyExistApplicant->shipper_name : ''),
                                                        ['class' => 'form-control textOnly input-sm required']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('import_port','Import from / Port of loading :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('import_port') ? 'has-error': ''}}">
                                                        {!! Form::text('import_port',  (isset($alreadyExistApplicant->import_port) ? $alreadyExistApplicant->import_port : ''),
                                                        ['class' => 'form-control input-sm required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div id="source_country_div" class="<?php
                                                    if(isset($alreadyExistApplicant->permit_type) &&
                                                            ($alreadyExistApplicant->permit_type == 5 || $alreadyExistApplicant->permit_type == 6) ){
                                                        echo 'hidden';
                                                    }
                                                    ?>">
                                                        {!! Form::label('source_country','Source Country :', ['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7 {{$errors->has('source_country') ? 'has-error': ''}}">
                                                            {!! Form::select('source_country', $countries,  (isset($alreadyExistApplicant->source_country) ?
                                                            $alreadyExistApplicant->source_country : ''),['class' => 'form-control input-sm']) !!}
                                                        </div>
                                                    </div>

                                                    <div id="destination_zone_div" class="<?php
                                                    if(isset($alreadyExistApplicant->permit_type) &&
                                                            $alreadyExistApplicant->permit_type != 5 && $alreadyExistApplicant->permit_type != 6 ){
                                                        echo 'hidden';
                                                    } ?>">
                                                        {!! Form::label('destination_zone','Destination Zone :', ['class'=>'col-md-5']) !!}
                                                        <div class="col-md-7 {{$errors->has('destination_zone') ? 'has-error': ''}}">
                                                            {!! Form::select('destination_zone', $allEcoZones,  (isset($alreadyExistApplicant->destination_zone) ?
                                                            $alreadyExistApplicant->destination_zone : ''),['class' => 'col-md-12 input-sm', 'placeholder' => 'Select One']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('destination_port','Port of destination :', ['class'=>'col-md-5 required-star']) !!}
                                                    <div class="col-md-7 {{$errors->has('destination_port') ? 'has-error': ''}}">
                                                        {!! Form::select('destination_port', $destination_ports,  (isset($alreadyExistApplicant->destination_port) ?
                                                        $alreadyExistApplicant->destination_port : ''),['class' => 'col-md-12 input-sm required', 'placeholder' => 'Select One']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" ">
                                            <div class="row">
                                                <div class="col md-6">
                                                    <div class="hidden col-md-6" id="carrier_name_div">
                                                        {!! Form::label('carrier_name','Name of carrier :', ['class'=>'col-md-5']) !!}
                                                        <div class="col-md-7 {{$errors->has('carrier_name') ? 'has-error': ''}}">
                                                            {!! Form::text('carrier_name', (isset($alreadyExistApplicant->carrier_name) ? $alreadyExistApplicant->carrier_name : ''),
                                                            ['class' => 'form-control textOnly input-sm']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="hidden col-md-12-" id="carrier_passport_div">
                                                    <div class="col-md-6">
                                                        {!! Form::label('carrier_passport_no','Passport No. :', ['class'=>'col-md-5']) !!}
                                                        <div class="col-md-7 {{$errors->has('carrier_passport_no') ? 'has-error': ''}}">
                                                            {!! Form::text('carrier_passport_no',  (isset($alreadyExistApplicant->carrier_passport_no) ?
                                                            $alreadyExistApplicant->carrier_passport_no : ''), ['class' => 'col-md-12 input-sm form-control']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::label('carrier_pass_validity','Passport Validity :', ['class'=>'col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <div class="datepicker input-group date">
                                                                {!! Form::text('carrier_pass_validity', (isset($alreadyExistApplicant->carrier_pass_validity) && $alreadyExistApplicant->carrier_pass_validity != '0000-00-00' ?
                                                                App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->carrier_pass_validity, 0, 10)) : ''),
                                                                ['class' => 'col-md-12  input-sm form-control']) !!}
                                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <p></p>

                                    </div><!--/col-md-12-->
                                </div> <!--/panel-body-->
                            </div> <!--/panel-->

                            <?php
                            if (isset($allRequestVal)) {
                                if (isset($allRequestVal['meterial_ids'])) {
                                    $meterials = array();
                                    $meterialsd = array();
                                    foreach ($allRequestVal['meterial_ids'] as $key => $meterial) {
                                        $meterialsd['id'] = $allRequestVal['meterial_ids'][$key];
                                        $meterialsd['material_description'] = $allRequestVal['material_description'][$key];
                                        $meterialsd['fob_usd'] = $allRequestVal['fob_usd'][$key];
                                        $meterialsd['fob_usd_value'] = $allRequestVal['fob_usd_value'][$key];
                                        $meterialsd['hs_code'] = $allRequestVal['hs_code'][$key];
                                        $meterialsd['hs_product'] = $allRequestVal['hs_product'][$key];
                                        $meterialsd['mat_quantity'] = $allRequestVal['mat_quantity'][$key];
                                        $meterialsd['mat_quantity_unit'] = $allRequestVal['mat_quantity_unit'][$key];
                                        $meterialsd['flight_no'] = $allRequestVal['flight_no'][$key];
                                        $meterialsd['flight_date'] = $allRequestVal['flight_date'][$key];
                                        $meterials[] = (object) $meterialsd;
                                    }
                                }
                            }
                            ?>
                            <?php
                            if (isset($allRequestVal)) {
                                if (isset($allRequestVal['icipinfo_ids'])) {
                                    $ipIcInfo = array();
                                    $ipIcInfosd = array();
                                    foreach ($allRequestVal['icipinfo_ids'] as $key => $epLcInfosddd) {
                                        $ipIcInfosd['id'] = $allRequestVal['icipinfo_ids'][$key];
                                        $ipIcInfosd['tt_no'] = $allRequestVal['tt_no'][$key];
                                        $ipIcInfosd['tt_value'] = $allRequestVal['tt_value'][$key];
                                        $ipIcInfosd['bank_id'] = $allRequestVal['bank_id'][$key];
                                        $ipIcInfosd['bank_invoice_date'] = $allRequestVal['bank_invoice_date'][$key];
                                        $ipIcInfosd['bank_type'] = $allRequestVal['bank_type'][$key];
                                        $ipIcInfo[] = (object) $ipIcInfosd;
                                    }
                                }
                            }
                            ?>

                            @if(count($meterials) > 0)
                                <?php $inc = 0; ?>
                                <div id="templateImdFullPar">
                                    @foreach($meterials as $eachmeterials)
                                        <?php $eachIpIcInfo = $ipIcInfo[$inc]; ?>
                                        <?php $templateImdFull = ($inc == 0 ? 'templateImdFull' : 'rowCount' . $inc); ?>
                                        <div class="panel panel-black templateImdFull" id="{{$templateImdFull}}">
                                            <div class="panel-heading"><strong>2. Import Materials & L/C Information Group</strong>
                                    <span class="pull-right">
                                        <?php if ($inc == 0) { ?>
                                        <a href="javascript:void(0)" onclick="addTableRow('moreInfoImd', 'templateImdFull');"
                                           class="btn btn-xs btn-info addTableRows"> <i class="fa fa-plus"></i></a>
                                        <?php } else { ?>
                                        <a href="javascript:void(0)" onclick="removeTableRow('templateImdFullPar', 'rowCount<?php echo $inc; ?>')"
                                           class="btn btn-xs btn-info addTableRows btn-danger"><i class="fa fa-times"></i></a>
                                        <?php } ?>

                                    </span>
                                            </div>
                                            <div class="panel-body" style="margin: 4px;">

                                                <div id="edidMoreInfoMd{{$inc}}">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <span class=""><strong> 2.i) Import Material Details</strong></span>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="col-md-12">

                                                                <div class="form-group" style=" ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <input type="hidden" name="meterial_ids[{{$inc}}]" value="<?php echo isset($eachmeterials->id) ? $eachmeterials->id : ''; ?>">
                                                                            {!! Form::label("material_type[$inc]",'Type of material :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7 {{$errors->has('material_type') ? 'has-error': ''}}">
                                                                                <select name="material_type[{{$inc}}]" id="material_type[{{$inc}}]" class="form-control input-sm required"  placeholder="Select One">

                                                                                    @foreach ($material_type as $key => $val)
                                                                                        <?php $selected1 = ( isset($eachmeterials->material_type) && $eachmeterials->material_type == $key ) ? 'selected' : ''; ?>
                                                                                        <?php $selected2 = ( isset($allRequestVal['material_type'][$inc]) && $allRequestVal['material_type'][$inc] == $key ) ? 'selected' : ''; ?>
                                                                                        <?php $selected = ( isset($selected1) || isset($selected2) ) ? 'selected="selected"' : null; ?>
                                                                                        <option {{$selected}} value="{{ $key }}">{{ $val }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            {!! Form::label("hs_code[$inc]",'HS Code :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7">
                                                                                <input type="text" name="hs_code[{{$inc}}]" id="hs_code[{{$inc}}]"
                                                                                       class="hscodes form-control input-sm required" value="<?php echo isset($eachmeterials->hs_code) ? $eachmeterials->hs_code : ''; ?>"/>
                                                                                <div style="clear: both;height: 2px;"></div>
                                                                                <input type="text" name="hs_product[{{$inc}}]" id="hs_product[{{$inc}}]"
                                                                                       value="<?php echo isset($eachmeterials->hs_product) ? $eachmeterials->hs_product : ''; ?>"
                                                                                       class="productNames form-control input-sm required" placeholder="HS Product" readonly="readonly"/>
                                                                                <p class="empty-message"></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" style=" ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("material_description[$inc]",'Material description :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7 {{$errors->has('material_description') ? 'has-error': ''}}">
                                                                    <textarea name="material_description[{{$inc}}]" id="material_description[{{$inc}}]"
                                                                              class="form-control input-sm required" />
                                                                                <?php echo isset($eachmeterials->material_description) ? $eachmeterials->material_description : '';
                                                                                ?> </textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {!! Form::label('fob_usd_value','FoB / CNF / CIF / CFR / X-Factory / DDU / Replacement / Free of Cost value :',
                                                                            ['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7 {{$errors->has('fob_usd_value') ? 'has-error': ''}}">
                                                                                {!! Form::select("fob_usd[$inc]", $currencies,  (isset($eachmeterials->fob_usd) ? $eachmeterials->fob_usd : '107'),
                                                                                ['class' => 'col-md-4 col-xs-12 col-sm-12 input-sm required', 'placeholder' => ' Select One', 'id'=>"fob_usd[$inc]"]) !!}
                                                                                <div class="col-md-1 col-xs-12 col-sm-12"></div>
                                                                                <input type="text" name="fob_usd_value[{{$inc}}]" id="fob_usd_value[{{$inc}}]"
                                                                                       class="col-md-7 col-xs-12 col-sm-12 input-sm onlyNumber required clone-form-control"
                                                                                       value="<?php echo isset($eachmeterials->fob_usd_value) ? $eachmeterials->fob_usd_value : ''; ?>"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("mat_quantity_unit[$inc]",'Unit of Quantity :',['class'=>'col-md-5']) !!}
                                                                            <div class="col-md-7 {{$errors->has('mat_quantity_unit') ? 'has-error': ''}}">
                                                                                <select name="mat_quantity_unit[{{$inc}}]" id="mat_quantity_unit[{{$inc}}]"
                                                                                        class="form-control input-sm required">
                                                                                    <option value="">Select One</option>
                                                                                    @foreach ($quantity_unit as $key => $val)
                                                                                        <?php $selected = (isset($eachmeterials->mat_quantity_unit) && $eachmeterials->mat_quantity_unit == $key ) ? 'selected="selected"' : ''; ?>
                                                                                        <option value="{{ $key }}" {{$selected}}> {{ $val }} </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("mat_quantity[$inc]",'Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7 {{$errors->has('mat_quantity') ? 'has-error': ''}}">
                                                                                <input type="text" name="mat_quantity[{{$inc}}]" id="mat_quantity[{{$inc}}]"
                                                                                       class="form-control input-sm required onlyNumber" maxlength="15"
                                                                                       value="<?php echo isset($eachmeterials->mat_quantity) ? $eachmeterials->mat_quantity : ''; ?>"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="row hidden" id="flight_div{{$inc}}">
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("flight_no[$inc]",'Flight No :',['class'=>'col-md-5']) !!}
                                                                            <div class="col-md-7 {{$errors->has('flight_no') ? 'has-error': ''}}">
                                                                                <input type="text" name="flight_no[{{$inc}}]" id="flight_no[{{$inc}}]" class="form-control input-sm"
                                                                                       value="<?php echo isset($eachmeterials->flight_no) ? $eachmeterials->flight_no : ''; ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("flight_date[$inc]",'Flight Date :',['class'=>'col-md-5']) !!}
                                                                            <div class="col-md-7">
                                                                                <div class="datepicker input-group date">
                                                                                    <?php
                                                                                    if (isset($allRequestVal)) {
                                                                                        $flight = $eachmeterials->flight_date;
                                                                                    } else {
                                                                                        $flight = (isset($eachmeterials->flight_date) ? App\Libraries\CommonFunction::changeDateFormat(substr($eachmeterials->flight_date, 0, 10)) : '');
                                                                                    }
                                                                                    ?>
                                                                                    <input type="text" name="flight_date[{{$inc}}]" id="flight_date[{{$inc}}]" class="form-control input-sm date"
                                                                                           value=""/>
                                                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                                <p></p>

                                                            </div><!--/col-md-12-->
                                                        </div> <!--/panel-body-->
                                                    </div> <!--/panel-->
                                                </div>


                                                <div id="edidMoreInfoPo{{$inc}}">
                                                    <?php $templateTtPo = ($inc == 0 ? 'templateTtPo' : 'rowCount' . $inc); ?>
                                                    <div class="panel panel-primary templateTtPo" id="{{$templateTtPo}}">
                                                        <div class="panel-heading"><strong>2.ii) TT / P.O/ SC/ CM / L/C Information</strong></div>
                                                        <div class="panel-body">

                                                            <div class="col-md-12">

                                                                <div class="form-group" style=" ">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <input type="hidden" name="icipinfo_ids[{{$inc}}]" value="<?php echo isset($eachIpIcInfo->id) ? $eachIpIcInfo->id : ''; ?>">
                                                                                {!! Form::label("tt_no[$inc]",'TT / P.O/ SC/ CM / L/C No. :',['class'=>'col-md-5 required-star']) !!}
                                                                                <div class="col-md-7 {{$errors->has('tt_no') ? 'has-error': ''}}">
                                                                                    <input type="text" name="tt_no[{{$inc}}]" id="tt_no[{{$inc}}]" class="form-control input-sm required" value="{{(isset($eachIpIcInfo->tt_no) ? $eachIpIcInfo->tt_no : '')}}"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                {!! Form::label("tt_value[$inc]",'TT / P.O/ SC/ CM / L/C value :',['class'=>'col-md-5 required-star']) !!}
                                                                                <div class="col-md-7 {{$errors->has('tt_value') ? 'has-error': ''}}">
                                                                                    <input type="text" name="tt_value[{{$inc}}]" id="tt_value[{{$inc}}]" class="form-control input-sm required onlyNumber" value="{{(isset($eachIpIcInfo->tt_value) ? $eachIpIcInfo->tt_value : '')}}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" style=" ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("bank_id[$inc]",'Issuing Bank :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7 {{$errors->has('bank_id') ? 'has-error': ''}}">
                                                                                <select name="bank_id[{{$inc}}]" id="bank_id[{{$inc}}]" class="input-sm form-control required"  placeholder="Select One">
                                                                                    <option value="">Select One</option>
                                                                                    @foreach ($banks as $key => $val) {
                                                                                    <option value="{{ $key }}"
                                                                                    <?php
                                                                                            if (isset($eachIpIcInfo->bank_id)) {
                                                                                                echo $eachIpIcInfo->bank_id == $key ? 'selected' : '';
                                                                                            }
                                                                                            ?>>
                                                                                        {{ $val }}
                                                                                    </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("bank_type[$inc]",'Type :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7 {{$errors->has('bank_type') ? 'has-error': ''}}">
                                                                                <select name="bank_type[{{$inc}}]" id="bank_type[{{$inc}}]" class="form-control input-sm required" placeholder="Select One">
                                                                                    @foreach ($bankTypes as $key => $val)
                                                                                        <option value="{{ $key }}" <?php if (isset($eachIpIcInfo->bank_type)) { echo $eachIpIcInfo->bank_type == $key ? 'selected' : '';}?>>{{ $val }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" style=" ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("bank_invoice_date[$inc]",'Issue Date :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7">
                                                                                <div class="datepicker input-group date">
                                                                                    <?php
                                                                                    if (isset($allRequestVal)) {
                                                                                        $issueDate = $eachIpIcInfo->bank_invoice_date;
                                                                                    } else {
                                                                                        $issueDate = (isset($eachIpIcInfo->bank_invoice_date) && $alreadyExistApplicant->bank_invoice_date != '0000-00-00' ? App\Libraries\CommonFunction::changeDateFormat(substr($eachIpIcInfo->bank_invoice_date, 0, 10)) : '');
                                                                                    }
                                                                                    ?>
                                                                                    <input type="text" name="bank_invoice_date[{{$inc}}]" id="bank_invoice_date[{{$inc}}]" class="input-sm form-control required" value="{{$issueDate}}" />
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6"></div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                                <p></p>

                                                            </div><!--/col-md-12-->
                                                        </div> <!--/panel-body-->
                                                    </div> <!--/panel-->
                                                </div>
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                        <?php $inc++; ?>
                                    @endforeach
                                </div>
                            @else

                                <div class="panel panel-black templateImdFull" id="templateImdFull">
                                    <div class="panel-heading"><strong><i class="fa fa-list"></i> Import Materials & L/C Information Group</strong>
                                <span class="pull-right">
                                    <a href="javascript:void(0)" onclick="addTableRow('moreInfoImd', 'templateImdFull');" class="btn btn-xs btn-info addTableRows">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </span>
                                    </div>
                                    <div class="panel-body" style="margin: 4px;">

                                        <div class="panel panel-primary templateImd">
                                            <div class="panel-heading">
                                                <span class=""><strong>Import Material Details</strong></span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12">

                                                    <div class="form-group" style=" ">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="meterial_ids[0]" value="">
                                                                {!! Form::label('material_type[0]','Type of material :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('material_type') ? 'has-error': ''}}">
                                                                    <select name="material_type[0]" id="material_type[0]" class="form-control input-sm required">
                                                                        @foreach ($material_type as $key => $val)
                                                                            <?php $selected = ( isset($allRequestVal['material_type'][0]) && $allRequestVal['material_type'][0] == $key ) ? 'selected="selected"' : null; ?>
                                                                            <option {{$selected}} value="{{ $key }}"> {{ $val }} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label('hs_code[0]','HS Code :',['class'=>'hscodes col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{ $errors->has('hs_code') ? 'has-error' : '' }}">
                                                                    <input type="text" name="hs_code[0]" id="hs_code[0]" class="hscodes form-control input-sm required"/>
                                                                    <div style="clear: both;height: 2px;"></div>
                                                                    <input type="text" name="hs_product[0]" id="hs_product[0]" value=""
                                                                           class="productNames form-control input-sm required" placeholder="HS product" readonly="readonly"/>
                                                                    <p class="empty-message"></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label('material_description[0]','Material description :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('material_description') ? 'has-error': ''}}">
                                                            <textarea name="material_description[0]" id="material_description[0]"
                                                                      class="form-control input-sm required"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label('fob_usd_value','FoB / CNF / CIF / CFR / X-Factory / DDU / Replacement / Free of Cost value :',
                                                                ['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('fob_usd_value') ? 'has-error': ''}}">
                                                                    {!! Form::select("fob_usd[0]", $currencies,  '107',
                                                                    ['class' => 'col-md-4 col-xs-12 col-sm-12 input-sm required', 'placeholder' => ' Select One', 'id'=>"fob_usd[0]"]) !!}
                                                                    <div class="col-md-1 col-xs-12 col-sm-12"></div>
                                                                    <input type="text" name="fob_usd_value[0]" class="col-md-7 col-xs-12 col-sm-12 input-sm  required onlyNumber clone-form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" style=" ">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label('mat_quantity[0]','Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('mat_quantity') ? 'has-error': ''}}">
                                                                    <input type="text" name="mat_quantity[0]" id="mat_quantity[0]" maxlength="15"
                                                                           class="form-control input-sm required onlyNumber" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label('mat_quantity_unit[0]','Unit of Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('mat_quantity_unit') ? 'has-error': ''}}">
                                                                    <select name="mat_quantity_unit[0]" id="mat_quantity_unit[0]"
                                                                            class="form-control input-sm required">
                                                                        <option value="">Select One</option>
                                                                        @foreach ($quantity_unit as $key => $val) {
                                                                        <option value="{{ $key }}"> {{ $val }} </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row hidden" id="flight_div">
                                                            <div class="col-md-6">
                                                                {!! Form::label('flight_no[0]','Flight No :',['class'=>'col-md-5']) !!}
                                                                <div class="col-md-7 {{$errors->has('flight_no') ? 'has-error': ''}}">
                                                                    <input type="text" name="flight_no[0]" id="flight_no[0]" class="form-control input-sm"
                                                                           value="<?php echo isset($alreadyExistApplicant->flight_no) ? $alreadyExistApplicant->flight_no : ''; ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label('flight_date','Flight Date :',['class'=>'col-md-5']) !!}
                                                                <div class="col-md-7">
                                                                    <div class="datepicker input-group date">
                                                                        <input type="text" name="flight_date[0]" id="flight_date[0]" class="form-control input-sm"
                                                                               value="<?php
                                                                               echo isset($alreadyExistApplicant->flight_date) && $alreadyExistApplicant->flight_date != '0000-00-00' ?
                                                                                       App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->flight_date, 0, 10)) : ''
                                                                               ?>"/>
                                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <p></p>

                                                </div><!--/col-md-12-->
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->

                                        <div class="panel panel-primary templateTtPo" id="templateTtPo">
                                            <div class="panel-heading"><strong>TT / P.O/ SC/ CM / L/C Information</strong></div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="icipinfo_ids[0]" value="">
                                                                {!! Form::label('tt_no','TT / P.O/ SC/ CM / L/C No. :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('tt_no') ? 'has-error': ''}}">
                                                                    <input type="text" name="tt_no[0]" class="form-control input-sm required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 {{ $errors->has('tt_value') ? 'has-error' : '' }}">
                                                                {!! Form::label('tt_value','TT / P.O / SC / CM / L/C value :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('tt_value') ? 'has-error': ''}}">
                                                                    {!! Form::text('tt_value[0]', (isset($alreadyExistApplicant->tt_value) ? $alreadyExistApplicant->tt_value : ''),
                                                                    ['class' => 'form-control input-sm required onlyNumber']) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label('bank_id','Issuing Bank :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{$errors->has('bank_id') ? 'has-error': ''}}">
                                                                    <select name="bank_id[0]" id="bank_id" class="form-control input-sm required">
                                                                        <option value=""> Select One </option>
                                                                        @foreach ($banks as $key => $val) {
                                                                        <option value="{{ $key }}">{{ $val }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="col-md-5 text-left required-star" for="bank_type">Type of L/C :</label>
                                                                <div class="col-md-7 ">
                                                                    <select name="bank_type[0]" id="bank_type" class="form-control input-sm required">
                                                                        @foreach ($bankTypes as $key => $val)
                                                                            <option value="{{$key}}">{{$val}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label('bank_invoice_date','Issue Date :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7">
                                                                    <div class="datepicker input-group date">
                                                                        <input type="text" name="bank_invoice_date[0]" class="form-control input-sm required" />
                                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"></div>
                                                        </div>
                                                        <div class="clearfix"></div><p></p>
                                                    </div>
                                                </div><!--/col-md-12 close-->
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                    </div> <!--/panel-black body close-->
                                </div> <!--/panel-black close-->
                            @endif
                            <div id="moreInfoImd" class="clear"></div>

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>3. Required Documents for attachment</strong></div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover ">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th colspan="6">Required Attachments</th>
                                                <th colspan="2">Attached PDF file
                                                    <span onmouseover="toolTipFunction()" data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 2MB)!">
                                                        <i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = 1; ?>
                                            @foreach($document as $row)
                                                <tr>
                                                    <td><div align="center">{!! $i !!}</div></td>
                                                    <td colspan="6">{!!  $row->doc_name !!}<?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></td>
                                                    <td colspan="2">
                                                        <input name="document_id_<?php echo $row->doc_id; ?>" type="hidden" value="{{(isset($clrDocuments[$row->doc_id]['doucument_id']) ? $clrDocuments[$row->doc_id]['doucument_id'] : '')}}">
                                                        <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>"
                                                               name="doc_name_<?php echo $row->doc_id; ?>" />
                                                        <input name="file<?php echo $row->doc_id; ?>" <?php if (empty($clrDocuments[$row->doc_id]['file']) && empty($allRequestVal["file$row->doc_id"]) && $row->doc_priority == "1") { echo "class='required'"; } ?>
                                                               id="file<?php echo $row->doc_id; ?>" type="file" size="20"
                                                               onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php echo $row->doc_id; ?>', '<?php echo $row->doc_priority; ?>')"/>

                                                        @if($row->additional_field == 1)
                                                            <table>
                                                                <tr>
                                                                    <td>Other file Name : </td>
                                                                    <td> <input maxlength="64" class="form-control input-sm <?php if ($row->doc_priority == "1"){ echo 'required'; } ?>"
                                                                                name="other_doc_name_<?php echo $row->doc_id; ?>" id="other_doc_name_<?php echo $row->doc_id; ?>" type="text"
                                                                                value="{{(isset($clrDocuments[$row->doc_id]['doc_name']) ? 
                                                                                    $clrDocuments[$row->doc_id]['doc_name'] : '')}}"></td>
                                                                </tr>
                                                            </table>
                                                        @endif
                                                        @if(isset($clrDocuments[$row->doc_id]['file']))
                                                            <div class="save_file saved_file_{{$row->doc_id}}">
                                                                <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(isset($clrDocuments[$row->doc_id]['file']) ?
                                                                    $clrDocuments[$row->doc_id]['file'] : ''))}}" title="{{$row->doc_name}}">
                                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name); ?></a>

                                                                <?php if(isset($alreadyExistApplicant) && Auth::user()->id == $alreadyExistApplicant->created_by && $viewMode != 'on') {?>
                                                                <a href="javascript:void(0)" onclick="ConfirmDeleteFile({{ $row->doc_id }})">
                                                                    <span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span>
                                                                </a>
                                                                <?php } ?>
                                                            </div>
                                                        @endif

                                                        @if(isset($allRequestVal["file$row->doc_id"]))
                                                            <label id="label_file{{$row->doc_id}}"><b>File: {{$allRequestVal["file$row->doc_id"]}}</b></label>
                                                            <input type="hidden" class="required" value="{{$allRequestVal["validate_field_".$row->doc_id]}}" id="validate_field_{{$row->doc_id}}" name="validate_field_{{$row->doc_id}}">
                                                        @endif

                                                        <div id="preview_<?php echo $row->doc_id; ?>">
                                                            <input type="hidden" value="<?php echo isset($clrDocuments[$row->doc_id]['file']) ?
                                                                    $clrDocuments[$row->doc_id]['file'] : ''?>" id="validate_field_<?php echo $row->doc_id; ?>"
                                                                   name="validate_field_<?php echo $row->doc_id; ?>" class="<?php echo $row->doc_priority == "1" ? "required":'';  ?>"  />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div><!-- /.panel-body -->
                            </div>

                            <?php if ($viewMode == "on") { ?>

                            @if(in_array(Auth::user()->desk_id,array(3,4,5,6)) || Auth::user()->user_type == '1x101')
                                <div class="row">
                                    <div class="col-md-12"><br/></div>
                                    <div class="col-md-12" style="margin-bottom:6px;">
                                        <a href="{{ url('project-clearance/view-cer/'. Encryption::encodeId($alreadyExistApplicant->created_by))}}"
                                           target="_blank" class="btn btn-warning btn-xs pull-left show-in-view ">
                                            <i class="fa fa-download"></i> <strong>Associated Certificates of Users</strong>
                                        </a>
                                    </div>
                                </div>
                            @endif {{-- checking if RD desks or system admin --}}

                            @include('importPermit::doc-tab')
                            <?php } ?>

                            <div class="panel panel-primary hiddenDiv">
                                <div class="panel-heading"><strong>Terms and Conditions</strong></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12" style="margin: 12px 0;">
                                            <input id="acceptTerms-2" @if(isset($alreadyExistApplicant->acceptTerms) && $alreadyExistApplicant->acceptTerms == 1) checked @endif name="acceptTerms" type="checkbox" class="required col-md-1 col-xs-1 col-sm-1 text-left" style="width: 3%; margin-left: 2px;">
                                            <label for="acceptTerms-2" class="col-md-11 col-xs-11 col-sm-11 text-left required-star">I agree with the Terms and Conditions.</label>
                                        </div>
                                    </div>
                                </div>
                            </div> <!--/ main red panel with margin-->

                            <div style="margin:6px;">
                                @if(ACL::getAccsessRight('importPermit','A'))
                                    <div class="row">
                                        <?php if ($viewMode != 'on') { ?>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <?php
                                            $statusId = (isset($alreadyExistApplicant->status_id) ? $alreadyExistApplicant->status_id : '');
                                            if ($statusId == -1 || $statusId == '') {
                                            ?>
                                            <button type="submit" class="btn btn-primary btn-md cancel" value="draft" name="submitInsert">Save as Draft</button>
                                            <?php } else { ?>
                                            &nbsp;
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                            <button type="submit" class="btn btn-primary btn-md" value="save" name="submitInsert">Submit</button>
                                        </div>
                                        {!! Form::close() !!}<!-- /.form end -->
                                        <?php } ?>
                                    </div>
                                @endif

                                <?php if ($viewMode == "on" && in_array(Auth::user()->user_type, array('1x101', '2x202', '4x404', '7x707', '8x808', '9x909'))) { ?>
                                @include('importPermit::apps_history')
                                <?php } ?>

                            </div>

                            @if($viewMode == "on" && isset($form_data->status_id) && $form_data->status_id > 20)
                                    <?php if ( isset($form_data->status_id) && in_array($form_data->status_id, [21,28,29,30 ,31]) && isset($form_data->certificate) ) { ?>
                                <?php
                                        }else{
                                            ?>
                                        <table class="" border="1" width="100%" style="text-align: center;">
                                            <tr>
                                                <td style=" padding: 20px;" class="alert alert-info">
                                                    The goods specified in the invoice(s) are eligible for exemption for the whole of the customs duty, sales tax etc. vide S.R.O. No. 209-LAW/2015/46/Customs dated July 1,2015 issued by the Internal Resources Division, Ministry of Finance, Dhaka
                                                </td>
                                            </tr>
                                        </table>
                                        </br>
                                        <table class="" border="1" width="100%" style="text-align: center;">
                                            <tr>
                                                <td style=" padding: 20px;" class="alert alert-danger">
                                                    <b>
                                                        THIS PERMIT IS VALID FOR 30 (THIRTY) DAYS FROM THE DATE OF ISSUE
                                                    </b>
                                                </td>
                                            </tr>
                                        </table>
                                        </br>
                                        <table class="" border="1" width="100%" style="text-align: center;">
                                            <tr>
                                                <td style=" padding: 20px;"  class="alert alert-warning">
                                                    <b>
                                                        THIS IS COMPUTER GENERATED PERMIT. SO SIGNATURE IS NOT REQUIRED
                                                    </b>
                                                    <br/>
                                                    @if(isset($alreadyExistApplicant->ip_number))
                                                        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG( $alreadyExistApplicant->ip_number, 'C39')}}" alt="barcode" width="250"/>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    <?php
                                        }
                                        ?>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer-script')
    @if(in_array(Auth::user()->desk_id,array(9)) && ($alreadyExistApplicant->status_id == 30 || $alreadyExistApplicant->status_id == 31) )
        @include('partials.datatable-scripts')
    @endif

    @if($viewMode !== 'on')
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    @endif

    <script type="text/javascript">

        // regarding extension or cancellation
        $(document).ready(function(){
            $('.ext_chng').eq(0).prop('checked',true);

            $('.ext_chng').on('change',function(){
                var ext_upto = $('#ext_upto');
                var ext_upto_txt = $('#ext_upto_txt');

                if(this.value=='extension'){
                    ext_upto.show();
                    ext_upto_txt.show();
                }else{
                    ext_upto.hide();
                    ext_upto_txt.hide();
                }
            });
            $("input[name=extension_upto], textarea[name=extension_cancellation_remark]").on('blur',function(){
                if($(this).val()!=''){
                    $(this).removeClass('error');
                }else{
                    $(this).addClass('error');
                }
            });

            $("#extension_cancellation").on('click',function(){
                $("#ep_form").find('.error').removeClass('error');
                var error = false;
                var extension_cancellation = $("input[name=extension]:checked").val();
                var extension_cancellation_remark = $("textarea[name=extension_cancellation_remark]").val();
                if(extension_cancellation_remark==''){
                    $("textarea[name=extension_cancellation_remark]").addClass('error');
                    error = true;
                }
                var data={
                    app_id:"{{Request::segment(3)}}",
                    extension_cancellation:extension_cancellation,
                    extension_cancellation_remark:extension_cancellation_remark,
                    _token:'{{ csrf_token() }}'
                };
                if(extension_cancellation=='extension'){
                    data.extension_upto = $("input[name=extension_upto]").val();
                    if( data.extension_upto==''){
                        $("input[name=extension_upto]").addClass('error');
                        error = true;
                    }
                }
                if(error){
                    return false;
                }

                $.ajax({
                    url:"{{url('import-permit/extension-cancellation')}}",
                    method:'GET',
                    data:data,
                    dataType:'json',
                    success:function(data){
                        var body
                        if(data.responseCode===1){
                            if(extension_cancellation=='extension'){
                                body = "<b>Extended Date:</b>"+data.message.extension_date+
                                "<br/><b>Remarks:</b>"+data.message.extension_remark;
                            }else if(extension_cancellation=='cancel'){
                                body = "<b>Cancellation Remarks:</b>"+data.message.cancellation_remark;
                            }
                            $("#ep_form").find(".panel-body").html(body);
                        }else{
                            $("#ep_form").find("#extension_cancellation_error").html(data.message);
                        }
                    },
                    error:function(){
                        console.log('error');
                    },
                    completed:function(){
                    }

                });

            });
        });

        <?php if ($viewMode != 'on') { ?>

        function uploadDocument(targets, id, vField, isRequired){
            var inputFile = $("#" + id).val();
            if (inputFile == '') {
                $("#" + id).html('');
                document.getElementById("isRequired").value = '';
                document.getElementById("selected_file").value = '';
                document.getElementById("validateFieldName").value = '';
                document.getElementById(targets).innerHTML = '<input type="hidden" class="required" value="" id="' + vField + '" name="' + vField + '">';
                if ($('#label_' + id).length)
                    $('#label_' + id).remove();
                return false;
            }

            try {
                document.getElementById("isRequired").value = isRequired;
                document.getElementById("selected_file").value = id;
                document.getElementById("validateFieldName").value = vField;
                document.getElementById(targets).style.color = "red";
                var action = "{{url('/import-permit/upload-document')}}";
                $("#" + targets).html('Uploading....');
                var file_data = $("#" + id).prop('files')[0];
                var form_data = new FormData();
                form_data.append('selected_file', id);
                form_data.append('isRequired', isRequired);
                form_data.append('validateFieldName', vField);
                form_data.append('_token', "{{ csrf_token() }}");
                form_data.append(id, file_data);
                $.ajax({
                    target: '#' + targets,
                    url: action,
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (response) {
                        $('#' + targets).html(response);
                        var fileNameArr = inputFile.split("\\");
                        var l = fileNameArr.length;
                        if ($('#label_' + id).length)
                            $('#label_' + id).remove();
                        var doc_id = parseInt(id.substring(4));
                        var newInput = $('<label class="saved_file_'+doc_id+'" id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + ' <a href="javascript:void(0)" onclick="EmptyFile('+ doc_id +')"><span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span> </a></b></label>');
//                        var newInput = $('<label id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                        $("#" + id).after(newInput);
                        //check valid data
                        var validate_field = $('#' + vField).val();
                        if (validate_field == '') {
                            document.getElementById(id).value = '';
                        }
                    }
                });
            } catch (err) {
                document.getElementById(targets).innerHTML = "Sorry! Something Wrong.";
            }
        }

        $(document).ready(function () {
            $("form#importPermitForm").validate();
            $(document).on('click', '.cancel', function (evt)
            {
                // find parent form, cancel validation and submit it
                // cancelSubmit just prevents jQuery validation from kicking in
                $("#importPermitForm").closest('form').data("validator").cancelSubmit = true;
                $("#importPermitForm").closest('form').submit();
                return false;
            });

        });
        //var rowCount = 0;
        function addTableRow(tableID, templateRow){
            //rowCount++;
            //Direct Copy a row to many times
            var x = document.getElementById(templateRow).cloneNode(true);
            x.id = "";
            x.style.display = "";
            var table = document.getElementById(tableID);
            var rowCount = $('.' + templateRow).length;
            var lastTr = $('#' + tableID).find('.' + templateRow).last().attr('data-number');
            if (lastTr != '' && typeof lastTr !== "undefined") {
                rowCount = parseInt(lastTr) + 1;
            }
            //var rowCount = table.rows.length;
            //Increment id
            var rowCo = rowCount;
            var idText = 'rowCount' + rowCount;
            x.id = idText;
            $("#" + tableID).append(x);
            //get select box elements
            var attrSel = $("#" + tableID).find('#' + idText).find('select');
            for (var i = 0; i < attrSel.length; i++) {
                var nameAtt = attrSel[i].name;
                var idAtt = attrSel[i].id;
                //increment all array element name
                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                var repTextId = idAtt.replace('[0]', '[' + rowCo + ']');
                attrSel[i].name = repText;
                attrSel[i].id = repTextId;
            }
            //get input elements
            var attrImput = $("#" + tableID).find('#' + idText).find('input');
            for (var i = 0; i < attrImput.length; i++) {
                var nameAtt = attrImput[i].name;
                var idAtt = attrImput[i].id;
                //increment all array element name
                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                var repTextId = idAtt.replace('[0]', '[' + rowCo + ']');
                attrImput[i].name = repText;
                attrImput[i].id = repTextId;
            }

            //get textarea elements
            var attrTextarea = $("#" + tableID).find('#' + idText).find('textarea');
            for (var i = 0; i < attrTextarea.length; i++){
                var nameAtt = attrTextarea[i].name;
                var idAtt = attrTextarea[i].id;
                //increment all array element name
                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                var repTextId = idAtt.replace('[0]', '[' + rowCo + ']');
                attrTextarea[i].name = repText;
                attrTextarea[i].id = repTextId;
                $('#' + idText).find('.readonlyClass').prop('readonly',true);
            }
            attrTextarea.val(''); //value reset

            var attrLable = $("#" + tableID).find('#' + idText).find('label');
            for (var i = 0; i < attrLable.length; i++) {
                var htmlFor = attrLable[i].htmlFor;
                //increment all array element name
                var repTextFor = htmlFor.replace('[0]', '[' + rowCo + ']');
                attrLable[i].htmlFor = repTextFor;
            }

            //value reset
            attrImput.val('');
            //selected index reset
            attrSel.prop('selectedIndex', 0);
            //Class change by btn-danger to btn-primary
            $("#" + tableID).find('#' + idText).find('.addTableRows').removeClass('btn-primary').addClass('btn-danger').attr('onclick', 'removeTableRow("' + tableID + '","' + idText + '")');
            $("#" + tableID).find('#' + idText).find('.addTableRows > .fa').removeClass('fa-plus').addClass('fa-times');
            $('#' + tableID).find('.' + templateRow).last().attr('data-number', rowCount);
            $("#" + tableID).find('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: (new Date()),
                minDate: '01/01/1905'
            });

            $("#" + tableID).find('#' + idText).find('.onlyNumber').on('keydown', function (e) {
                //period decimal
                if ((e.which >= 48 && e.which <= 57)
                            //numpad decimal
                        || (e.which >= 96 && e.which <= 105)
                            // Allow: backspace, delete, tab, escape, enter and .
                        || $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1
                            // Allow: Ctrl+A
                        || (e.keyCode == 65 && e.ctrlKey === true)
                            // Allow: Ctrl+C
                        || (e.keyCode == 67 && e.ctrlKey === true)
                            // Allow: Ctrl+V
                        || (e.keyCode == 86 && e.ctrlKey === true)
                            // Allow: Ctrl+X
                        || (e.keyCode == 88 && e.ctrlKey === true)
                            // Allow: home, end, left, right
                        || (e.keyCode >= 35 && e.keyCode <= 39))
                {
                    var thisVal = $(this).val();
                    if (thisVal.indexOf(".") != -1 && e.key == '.') {
                        return false;
                    }
                    $(this).removeClass('error');
                    return true;
                }
                else
                {
                    $(this).addClass('error');
                    return false;
                }
            });

            $("#" + tableID).find('.hscodes').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{url('import-permit/get-hscodes')}}",
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 4,
                response: function (event, ui) {
                    if (ui.content.length === 0) {
                        $(this).parent().find(".empty-message").text("No results found");
                        $(this).parent().find(".productNames").addClass("required error").val('');
                    } else {
                        $(this).parent().find(".empty-message").empty();
                        $(this).parent().find(".productNames").removeClass("required error");
                    }
                },
                select: function (event, data) {
                    $(this).parent().find('.productNames').val(data.item.product);
                }
            });
        }

        function removeTableRow(tableID, removeNum) {
            $('#' + tableID).find('#' + removeNum).remove();
        }

        $('#permit_type').change(function (e) {
            if (this.value == '2') { // 2 is Hand carry import permit
                $('#source_country_div').removeClass('hidden');
                $('#source_country').addClass('required');
                $('#destination_zone_div').addClass('hidden');
                $('#carrier_name_div').removeClass('hidden');
                $('#carrier_passport_div').removeClass('hidden');
                $('#flight_div').removeClass('hidden');
            }
            else if (this.value == '5') { // 5 is Intrazone import permit
                $('#source_country_div').addClass('hidden');
                $('#source_country').removeClass('required');
                $('#destination_zone_div').removeClass('hidden');
                $('#carrier_name_div').addClass('hidden');
                $('#carrier_passport_div').addClass('hidden');
                $('#flight_div').addClass('hidden');
            }
            else if (this.value == '6') { // 6 is Interzone import permit
                $('#source_country_div').addClass('hidden');
                $('#source_country').removeClass('required');
                $('#destination_zone_div').removeClass('hidden');
                $('#carrier_name_div').addClass('hidden');
                $('#carrier_passport_div').addClass('hidden');
                $('#flight_div').addClass('hidden');
            }
            else {
                $('#source_country_div').removeClass('hidden');
                $('#source_country').addClass('required');
                $('#destination_zone_div').addClass('hidden');
                $('#carrier_name_div').addClass('hidden');
                $('#carrier_passport_div').addClass('hidden');
                $('#flight_div').addClass('hidden');
            }
        });
        $('#permit_type').trigger('change');


        $(document).ready(function () {
            $(".hscodes").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{url('import-permit/get-hscodes')}}",
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 4,
                response: function (event, ui) {
                    if (ui.content.length === 0) {
                        $(this).parent().find(".empty-message").text("No results found");
                        $(this).parent().find(".productNames").addClass("required error").val('');
                    } else {
                        $(this).parent().find(".empty-message").empty();
                        $(this).parent().find(".productNames").removeClass("required error");
                    }
                },
                select: function (event, data) {
                    $(this).parent().find('.productNames').val(data.item.product);
                }
            });
        });

        <?php } ?>  /*View mode is off */

        <?php if ($viewMode == 'on') { ?>

        @if (($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)
        $(document).ready(function () {
            var today = new Date();
            var yyyy = today.getFullYear();
            var mm = today.getMonth();
            var dd = today.getDate();
            $('#user_DOB').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                minDate: '01/01/' + (yyyy - 6)
            });
        });
        @endif {{-- checking process data --}}

        $('#inputForm select').each(function (index){
            var text = $(this).find('option:selected').text();
            var id = $(this).attr("id");
            var val = $(this).val();
            $('#' + id + ' option:selected').replaceWith("<option value='" + val + "' selected>" + text + "</option>");
        });
        $("#inputForm :input[type=text]").each(function (index) {
            $(this).attr("value", $(this).val());
        });
        $("#inputForm textarea").each(function (index) {
            $(this).text($(this).val());
        });

        $("#inputForm select").css({
            "border": "none",
            "background": "#fff",
            "pointer-events": "none",
            "box-shadow": "none",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance": "none"
        });

        $("#inputForm .actions").css({"display": "none"});
        $("#inputForm .draft").css({"display": "none"});
        $("#inputForm .title ").css({"display": "none"});
        //document.getElementById("previewDiv").innerHTML = document.getElementById("projectClearanceForm").innerHTML;

        $('#inputForm #showPreview').remove();
        $('#inputForm #save_btn').remove();
        $('#inputForm #save_draft_btn').remove();
        $('#inputForm .stepHeader, #inputForm .calender-icon,#inputForm .pss-error,#inputForm .hiddenDiv, #inputForm .input-group-addon').remove();
        $('#inputForm .required-star').removeClass('required-star');
        $('#inputForm input[type=hidden], #inputForm input[type=file]').remove();
        $('#inputForm .panel-orange > .panel-heading').css('margin-bottom', '10px');
        $('#invalidInst').html('');

        $('#inputForm').find('input:not(:checked),textarea').each(function() {
            if (this.value != ''){
                var displayOp = ''; //display:block
            } else{
                var displayOp = 'display:none';
            }

            if($(this).hasClass("onlyNumber") && !$(this).hasClass("nocomma"))
            {
                var thisVal = commaSeparateNumber(this.value);
                $(this).replaceWith("<span class='onlyNumber " +this.className+
                "' style='background-color:#ddd !important;border-radius:3px;padding:6px; height:auto; margin-bottom:2px;"
                + displayOp + "'>" + thisVal + "</span>");
            }else {
                $(this).replaceWith("<span class='" +this.className+  "' style='background-color:#ddd;padding:6px; height:auto; margin-bottom:2px;"
                + displayOp + "'>" +  this.value + "</span>");
            }
        });

        $('#inputForm').find('textarea').each(function(){
            var displayOp = '';
            if (this.value != '') {
                displayOp = ''; //display:block
            } else {
                displayOp = 'display:none';
            }
            $(this).replaceWith("<span class='"+this.className+"' style='background-color:#ddd;height:auto;padding:6px;margin-bottom:2px;"
            + displayOp + "'>" + this.value + "</span>");
        });

        $('#inputForm .btn').not('.show-in-view').each(function(){
            $(this).replaceWith("");
        });

        $('#inputForm').find('input[type=radio]').each(function (){
            jQuery(this).attr('disabled', 'disabled');
        });

        $("#inputForm select").replaceWith(function (){
            var selectedText = $(this).find('option:selected').text().trim();
            var displayOp = '';
            if (selectedText != '' && selectedText != 'Select One') {
                displayOp = ''; //display:block
            } else {
                displayOp = 'display:none';
            }

            return "<span class='"+this.className+"' style='background-color:#ddd;height:auto;padding:6px;margin-bottom:2px;"
                    + displayOp + "'>" + selectedText + "</span>";
        });

        $("#inputForm select").replaceWith(function (){
            var selectedText = $(this).find('option:selected').text();
            return "<span style='background-color:#ddd;width:68%; height:auto; margin-bottom:2px;padding:6px;display:block;'>"
                    + selectedText + "</span>";
        });

        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
            }
            return val;
        }

        <?php } ?> /* viewMode is on */

        check = function (e,value){
            if (!e.target.validity.valid) {
                e.target.value = value.substring(0,value.length - 1);
                return false;
            }
            var idx = value.indexOf('.');
            if (idx >= 0) {
                if (value.length - idx > 3 ) {
                    e.target.value = value.substring(0,value.length - 1);
                    return false;
                }
            }
            return true;
        }

        function toolTipFunction() {
            $('[data-toggle="tooltip"]').tooltip();
        }

        $(document).on('click', '.download', function (e) {

            var value = $(this).attr('data');
            $('.show_' + value).show();
        });

        <?php if ($viewMode != 'on') { ?>
        $(document).ready(function(){
//            if($('html').hasClass("datepicker")) {
            $('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: (new Date()),
                minDate: '01/01/1905'
            });
//            }
        });
        <?php } ?>

    </script>

    @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(3,4,5,6,7,9))) {{--batch process start--}}
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    <script language="javascript">

        @if(in_array(Auth::user()->desk_id,array(9)) && ($alreadyExistApplicant->status_id == 30 || $alreadyExistApplicant->status_id == 31) )
        $(function () {
            $('#gpHistlist').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "iDisplayLength": 20
            });
        });
                @endif

        var numberOfCheckedBox = 0;
        var curr_app_id = '';
        function setCheckBox(){
            numberOfCheckedBox = 0;
            var flag = 1;
            var selectedWO = $("input[type=checkbox]").not(".selectall");
            selectedWO.each(function() {
                if (this.checked){
                    numberOfCheckedBox++;
                }else{
                    flag = 0;
                }
            });
            if (flag == 1){
                $("#chk_id").checked = true;
            }else {
                $("#chk_id").checked = false;
            }
            if (numberOfCheckedBox >= 1){
                $('.applicable_status').trigger('click');
            }
        } // end of setCheckBox function

        function changeStatus(check){
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            setCheckBox();
        }
        $(document).ready(function() {

            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            $("#apps_from").validate({
                errorPlacement: function() {
                    return false;
                }
            });
            $("#batch_from").validate({
                errorPlacement: function() {
                    return false;
                }
            });
            var base_checkbox = '.selectall';
            $(base_checkbox).click(function() {
                if (this.checked) {
                    $('.appCheckBox:checkbox').each(function() {
                        this.checked = true;
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    });
                } else {
                    $('.appCheckBox:checkbox').each(function() {
                        this.checked = false;
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    });
                }
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                setCheckBox();
            });
            $('.appCheckBox:checkbox').not(base_checkbox).click(function() {
                $(".selectall").prop("checked", false);
            });

            var break_for_pending_verification = 0;
            $(document).ready(function() {

                $("#status_id").trigger("click");
                var curr_app_id = $("#curr_app_id").val();
                var curr_status_id = $("#curr_status_id").val();
                $.ajaxSetup({async: false});
                var _token = $('input[name="_token"]').val();
                var delegate = '{{ @$delegated_desk }}';
                var state = false;
                $.post('/import-permit/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate,
                    _token: _token}, function(response) {

                    if (response.responseCode == 1) {
                        var option = '';
                        option += '<option selected="selected" value="">Select Below</option>';
                        $.each(response.data, function(id, value) {
                            option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                        });
                        $("#status_id").html(option);
                        $("#status_id").trigger("change");
                        $("#status_id").focus();
                    } else if (response.responseCode == 5){
                        alert('Without verification, application can not be processed');
                        break_for_pending_verification = 1;
                        option = '<option selected="selected" value="">Select Below</option>';
                        $("#status_id").html(option);
                        $("#status_id").trigger("change");
                        return false;
                    } else {
                        $('#status_id').html('Please wait');
                    }
                });
                $.ajaxSetup({async: true});
            });

            $(document).on('change', '.status_id', function() {
                var curr_app_id_for_process = $("#curr_app_id").val();
                var curr_status_id_for_process = $("#curr_status_id").val();

                var object = $(".status_id");
                var obj = $(object).parent().parent().parent();
                var id = $(object).val();
                var _token = $('input[name="_token"]').val();
                var status_from = $('#status_from').val();
                $('#sendToDeskOfficer').css('display', 'block');
                if (id == 0) {
                    obj.find('.param_id').html('<option value="">Select Below</option>');
                } else {
                    //  if(id == 5 || id == 8 || id == 9 || id == 14 || id == 2){
                    $.post('/import-permit/ajax/process', {id: id, curr_app_id: curr_app_id_for_process, status_from: curr_status_id_for_process, _token: _token}, function(response) {
                        if (response.responseCode == 1) {
                            var option = '';
                            option += '<option selected="selected" value="">Select Below</option>';
                            var countDesk = 0;
                            $.each(response.data, function(id, value) {
                                countDesk++;
                                option += '<option value="' + id + '">' + value + '</option>';
                            });
                            obj.find('#desk_id').html(option);
                            $('#desk_id').attr("disabled", false);
                            $('#remarks').attr("disabled", false);

                            if (countDesk == 0){
                                $('.dd_id').removeClass('required');
                                $('#sendToDeskOfficer').css('display', 'none');
                            }else{
                                $('.dd_id').addClass('required');
                            }

                            if (response.status_to == 5 || response.status_to == 8 || response.status_to == 16 || response.status_to == 17 || response.status_to == 19
                                    || response.status_to == 24|| response.status_to == 10 || response.status_to == 22){

                                $('#remarks').addClass('required');
                                $('#remarks').attr("disabled", false);
                            }else{
                                $('#remarks').removeClass('required');
                            }

                            if (response.file_attach == 1){
                                $('#sendToFile').css('display', 'block');
                            }else{
                                $('#sendToFile').css('display', 'none');
                            }
                        } // when response.responseCode == 1
                    });
                    //  }
                }
            });

            function resetElements(){
                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            }

            <?php if (isset($search_status_id) && $search_status_id > 0) { ?>
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: base_url + '/dashboard/search-result',
                type: 'post',
                data: {
                    _token: _token,
                    status_id:<?php echo $search_status_id; ?>,
                },
                dataType: 'json',
                success: function(response) {
                    // success
                    if (response.responseCode == 1) {
                        $('table.resultTable tbody').html(response.data);
                    } else {
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
                beforeSend: function(xhr) {
                    console.log('before send');
                },
                complete: function() {
                    //completed
                }
            });
            <?php } ?>

            $("#search_app").on('click', function() {
                $('.selectall').addClass("hidden");
                var _token = $('input[name="_token"]').val();
                var tracking_number = $('#tracking_number').val();
                var passport_number = $('#passport_number').val();
                var applicant_name = $('#applicant_name').val();
                var nationality = $('#nationality').val();
                var status_id = $('#modal_status_id').val();
                var selected = false;
                $.ajax({
                    url: base_url + '/import-permit/search-result',
                    type: 'post',
                    data: {
                        _token: _token,
                        tracking_number: tracking_number,
                        passport_number: passport_number,
                        applicant_name: applicant_name,
                        nationality: nationality,
                        status_id:status_id,
                    },
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        // success
                        if (response.responseCode == 1) {
//                            $('table.resultTable tbody').html(response.data);
                            $('#modal-close').trigger('click');
                        } else {
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    },
                    beforeSend: function(xhr) {
                        console.log('before send');
                    },
                    complete: function() {
                        //completed
                    }
                });
            });
        }); // end of document.change status_id
    </script>
    @endif

    @endsection <!--- footer-script--->

