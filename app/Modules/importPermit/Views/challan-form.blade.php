@if(($process_data->status_id == 21 || $process_data->status_id == 24) && Auth::user()->id == $alreadyExistApplicant->created_by)

                            {!! Form::open(array('url' => 'import-permit/challan-store/'.Encryption::encodeId($alreadyExistApplicant->id),'method' => 'post',
                            'files' => true, 'role'=>'form')) !!}

                                <div class="panel panel-primary">
                                <div class="panel-heading">Pay Order related information</div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="col-md-6">

                                                <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                                    {!! Form::label('Pay Order No :','Pay Order No : ',['class'=>'col-md-5 font-ok required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('challan_no', null,['class'=>'form-control bnEng required input-sm',
                                                              'placeholder'=>'e.g. 1103', 'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                                    {!! Form::label('bank_name','Bank Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::select('bank_name', $banks, '', ['class' => 'form-control input-sm required',
                                                        'placeholder' => 'Select One']) !!}
                                                        {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                                    {!! Form::label('amount','Amount :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('amount',null, ['class'=>'form-control bnEng required input-sm onlyNumber','placeholder'=>'e.g. 5000',
                                                             'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                    {!! Form::label('date','Date :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                    <div class="datepicker col-md-7  input-group date" data-date-format="yyyy-mm-dd">
                                                        {!! Form::text('date', null, ['class'=>'form-control required user_DOB', 'id' => 'user_DOB', 'placeholder'=>'Pick from datepicker']) !!}
                                                        <label for="user_DOB" class="input-group-addon user_DOB">
                                                            <span class="glyphicon glyphicon-calendar user_DOB" id="user_DOB"></span></label>
                                                    </div>
                                                    {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                                </div>
                                                <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                    {!! Form::label('branch','Branch Name :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::text('branch',null, ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12 {{$errors->has('challan_file') ? 'has-error' : ''}}">
                                                    {!! Form::label('challan_file','Pay order copy :',['class'=>'col-md-5 font-ok required-star']) !!}
                                                    <div class="col-md-7">
                                                        {!! Form::file('challan_file',null, ['class'=>'form-control bnEng required input-sm',
                                                        'data-rule-maxlength'=>'40']) !!}
                                                        {!! $errors->first('challan_file','<span class="help-block">:message</span>') !!}
                                                        <span class="text-danger" style="font-size: 9px; font-weight: bold">
                                                    [File Format: *.pdf | Maximum File size 3MB]
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                @if(ACL::getAccsessRight('projectClearance','E'))
                                                    <button type="submit" class="btn btn-primary pull-left next">
                                                        <i class="fa fa-chevron-circle-right"></i> Save</button>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                            {!! Form::close() !!}<!-- /.form end -->
                            </div> <!--End of Panel Group-->
                        @endif {{-- status_id == 21  and created by logged user --}}