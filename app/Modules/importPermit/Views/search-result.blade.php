<?php $row_sl = 0; ?>
@foreach($getList as $row)
<?php $row_sl++ ?>

    <tr>
        <td>{!! $row_sl !!}</td>
        <td>{!! $row->track_no !!}</td>        
        <td>
            @if($row->applicant_name == '')  Applicant @else{!! $row->applicant_name !!}@endif
        </td>        
        <td>
            @if($row->desk_name == '')
            Applicant
            @else
            {!! $row->desk_name !!}
            @endif
        </td>
        <td>
            @if(!empty($row->status_name))
            <span style="background-color:<?php echo $row->color; ?>;color: #fff; font-weight: bold;" class="label btn-sm">
                {!! $row->status_name !!}
            </span>
            @else
            <span style="background-color:#dd4b39;color: #fff; font-weight: bold;" class="label btn-sm">
                Draft
            </span>
            @endif {{-- checking status_name --}}
        </td>
        
        <td>{!! CommonFunction::updatedOn($row->updated_at) !!}</td>
        <td>
            <a href="{{url('import-permit/view/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" >
                <i class="fa fa-folder-open-o"></i> View</a>
            @if(!empty($row->undertaking) && !(in_array($row->status_id, array(-1,8,22))))
                <a href="{{url($row->undertaking)}}" target="_blank" class="btn btn-xs btn-warning open" >
                    <i class="fa fa-download" aria-hidden="true"></i> Undertaking</a>
            @endif
            <!-- 21 = approved and sent to custom, 28 > customs and security related status--->
            <?php if (!empty($row) && in_array($row->status_id, [21,28,29,30]) &&
            !empty($row->certificate)) { ?>
            <a class="btn btn-xs btn-info show-in-view" href="{{ url($row->certificate) }}" title="Download Certificate"
               target="_blank"> <i class="fa  fa-file-pdf-o"></i> Certificate</a>
            @if(Auth::user()->user_type == '1x101')
                <a class="btn btn-xs btn-danger show-in-view" href="/import-permit/discard-certificate/{{ Encryption::encodeId($row->record_id) }}"
                   onclick="return confirm('Are you sure??')" title="Download Certificate">
                    <i class="fa  fa-trash"></i> Discard Certificate</a>
            @endif
            <?php } ?>
        </td>
    </tr>
@endforeach



