<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><strong> Gatepass Production</strong></div>
            <div class="panel-body">

                {!! Form::open(['url' => '/import-permit/pass-history/'.Encryption::encodeId($alreadyExistApplicant->id), 'method' => 'post',
                'class' => 'form','id' => 'gp_form','role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}

                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr class="text-center">
                            <th class="text-center"  width="3%">#</th>
                            <th class="text-center">Item</th>
                            <th class="text-center">Unit</th>
                            <th class="text-center" width="15%">Remaining Quantity</th>
                            <th class="text-center" width="10%">Passed Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($gpMaterials as $mat)
                        <tr>
                            <td>{{ $i }}
                                {!! Form::hidden('ip_mat_id[]', $mat->ip_mat_id, ['class' => 'form-control input-sm']) !!}
                            </td>
                            <td>{{ $mat->material_description }}</td>
                            <td>{{ $mat->unit_name }}</td>
                            <td>{{ $mat->mat_remaining_quantity }}</td>
                            <td>
                                {!! Form::number('passed_quantity[]', null, ['class' => 'form-control input-sm required onlyNumber', 
                                'onInput'=>'return check(event,value)', 'min'=>'0', 'max'=>$mat->mat_remaining_quantity]) !!}
                                {!! $errors->first('passed_quantity','<span class="help-block">:message</span>') !!}
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>

                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                    <button type="submit" class="btn btn-success btn-sm" value="save" name="submitGp">
                        <strong> Generate Gatepass </strong>
                    </button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>