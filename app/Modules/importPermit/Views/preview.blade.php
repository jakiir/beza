@extends('layouts.front')
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Preview Form</title>
    <!--    <link  rel="stylesheet" type="text/css" href="assets/css/style.css" media="all">-->
    <link rel="stylesheet" href="assets/stylesheets/styles.css" media="all"/>
    <script src="https://code.jquery.com/jquery-1.6.2.js"></script>
    <script language="javascript"> var jq_my = jQuery.noConflict(true);</script>
</head>
<body>
<div align="right">
    <input type="button" value="&nbsp;&nbsp;&nbsp; Close &nbsp; &nbsp;&nbsp;" align="right" onClick="CloseMe()" id="closeBtn"  class="btn-submit-1" style="position: fixed;right: 0;"/>
</div>
<div id="previewDiv"></div>
<div align="center">
    <input type="button" style="font-size: 18px;"value="Go Back" id="backBtn" onclick="CloseMe()" class="btn-submit-1" />
    <input type="button"  style="font-size: 18px;"value="Submit" id="submitFromPreviewBtn" onclick="" class="btn-submit-1" />
</div>
</body>
</html>
<script language="javascript">

    jq_my(function() {
        jq_my('#submitFromPreviewBtn').click(function(e) {
            window.opener.document.getElementById("appClearenceForm").setAttribute("target", "_self");
            window.opener.jq_my("#appClearenceForm").submit();
            window.close();
        });

    });


    window.opener.$('select').each(function(index)
    {
        var text = jq_my(this).find('option:selected').text();
        var id = jq_my(this).attr("id");
        var val = jq_my(this).val();
        window.opener.jq_my('#' + id + ' option:[value="' + val + '"]').replaceWith("<option value='" + val + "' selected>" + text + "</option>");

    });

    window.opener.jq_my("#inputForm :input[type=text]").each(function(index) {
        jq_my(this).attr("value", jq_my(this).val());
    });

    window.opener.jq_my("textarea").each(function(index) {
        jq_my(this).text(jq_my(this).val());
    });

    window.opener.jq_my("select").css({
        "border" : "none",
        "background" : "#fff",
        "pointer-events" : "none",
        "box-shadow": "none",
        "-webkit-appearance" : "none",
        "-moz-appearance" : "none",
        "appearance": "none"
    });
    window.opener.jq_my("fieldset").css({"display": "block"});
    window.opener.jq_my("#full_same_as_authorized").css({"display": "none"});
    window.opener.jq_my(".actions").css({"display": "none"});
    window.opener.jq_my(".steps").css({"display": "none"});
    window.opener.jq_my(".draft").css({"display": "none"});
    window.opener.jq_my(".title ").css({"display": "none"});
    //    window.opener.jq_my("select").prop('disabled', true);
    document.getElementById("previewDiv").innerHTML = window.opener.document.getElementById("inputForm").innerHTML;
    // JavaScript Document
    function printThis(ob)
    {
//	$("#closeBtn").hide();
//	$(ob).hide();
        print();
    }
    jq_my('#showPreview').remove();
    jq_my('#save_btn').remove();
    jq_my('#save_draft_btn').remove();
    jq_my('.stepHeader,.calender-icon,.pss-error').remove();
    jq_my('.required-star').removeClass('required-star');
    jq_my('input[type=hidden]').remove();
    jq_my('.panel-orange > .panel-heading').css('margin-bottom','10px');
    jq_my('#invalidInst').html('');
    jq_my('.btn').each(function()
    {
        jq_my(this).replaceWith("-");
    });

    jq_my('#previewDiv').find('input:not([type=radio][type=hidden][type=file][name=acceptTerms]), textarea').each(function()
    {
        jq_my(this).replaceWith("<span>" + this.value + "</span>");
    });

    jq_my('#previewDiv').find('input:[type=file]').each(function()
    {
        jq_my(this).replaceWith("<span>" + this.value + "</span>");
    });

    jq_my('#acceptTerms-2').attr("onclick",'return false').prop("checked",true).css('margin-left','5px');
    jq_my('#previewDiv').find('input:[type=radio]').each(function()
    {
        jQuery(this).attr('disabled', 'disabled');
    });


    jq_my("select").replaceWith(function()
    {
        return jq_my(this).find('option:selected').text();
    });




    ///Change in opener
    window.opener.jQuery('#home').fadeOut("slow");		///home is the id of body in template page. It may be an id of div or any element
    jq_my(window).unload(function() {
        //window.opener.jq_my('#home').css({"display": "none"});
    });

    function CloseMe()
    {
        window.opener.jq_my("fieldset").css({"display": "none"});
        window.opener.jq_my(".actions").css({"display": "block"});
        window.opener.jq_my(".steps").css({"display": "block"});
        window.opener.jq_my(".draft").css({"display": "block"});
        window.opener.jq_my(".title ").css({"display": "block"});
        window.opener.jq_my("#appClearenceForm-p-3").css({"display": "block"});
        window.opener.jq_my(".last").addClass('current');
        window.opener.jq_my('#home').css({"display": "block"});
        window.close();
    }


</script>