<?php

Route::group(array('module' => 'SampleApp', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'],
    'namespace' => 'App\Modules\SampleApp\Controllers'), function() {

    // List of sample application
    
    Route::get('sample-doc/list', 'SampleAppController@index');

    /*     * ********************************End of Route group****************************** */
});
