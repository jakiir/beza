<?php

namespace App\Modules\SampleApp\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\CommonFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;

class SampleAppController extends Controller {

    public function index() {
//        if (!ACL::getAccsessRight('certificate, 'V')) {
//            die('You have no access right! Please contact with system admin if you have any query.');
//        }
        $loggedUser = CommonFunction::getUserId();
        $getList = [];
        $doc_list = DB::table('sample_doc')->get();
        return view("sampleApp::list",compact('doc_list'));

    }


    /*     * ********************************************End of Controller Class************************************************* */
}
