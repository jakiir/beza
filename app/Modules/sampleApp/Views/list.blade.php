@extends('layouts.admin')

@section('content')

   @include('partials.messages')

    <?php
//$accessMode = ACL::getAccsessRight('cer-docs');
//if (!ACL::isAllowed($accessMode, 'V')) {
//    die('You have no access right! Please contact system admin for more information');
//}
?>
<div class="col-lg-12">
    <div class="panel panel-red">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                    <i class="fa fa-list" style="padding: 10px;"></i> <strong>List of Sample Documents</strong>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th>Document Module</th>
                            <th width="15%">Document Type</th>
                            <th>Download Sample</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($doc_list as $sample_doc)
                        <tr>
                            <td>{{ $sample_doc->id }}</td>
                            <td>{{ $sample_doc->doc_module }}</td>
                            <td>{{ $sample_doc->doc_type }}</td>
                            <td>
                                @if(!empty($sample_doc->doc_path))
                                    <a target="_blank" href="{{ url($sample_doc->doc_path) }}"  title="Download PDF"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-folder-open"></i> Open</button></a>
                                    <a href="{{ url('/'.$sample_doc->doc_path) }}"  title="Download PDF" download><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-download"></i> Download</button></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
    @endsection

    @section('footer-script')
    @include('partials.datatable-scripts')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <script>
    $(function () {
        $('#list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 50
        });
    });
    </script>
    @endsection
