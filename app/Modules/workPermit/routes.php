<?php

Route::group(array('module' => 'workPermit', 'middleware' => 'auth', 'checkAdmin', 'XssProtection', 'namespace' => 'App\Modules\workPermit\Controllers'), function() {

    Route::get('/work-permit', "workPermitController@index");
    Route::get('/work-permit/list/', 'workPermitController@listOfApp');
    Route::get('/work-permit/list/{ind}/{zone}', 'workPermitController@listOfApp');
    Route::get('/work-permit/form', "workPermitController@appForm");
     Route::post('/work-permit/search-result', 'workPermitController@searchResult');

    Route::get('/work-permit/preview', 'workPermitController@preview');
    
    Route::post('/work-permit/store-app', "workPermitController@appStore");
    Route::get('/work-permit/edit-form/{id}', 'workPermitController@appFormEdit');
    Route::get('/work-permit/view/{id}', 'workPermitController@appFormView');
    Route::get('/work-permit/view-pdf/{id}','workPermitController@appDownloadPDF');

    Route::patch('/work-permit/update-batch', "workPermitController@updateBatch");
    Route::post('work-permit/challan-store/{id}', "workPermitController@challanStore");
    
    Route::get('work-permit/discard-certificate/{id}','workPermitController@discardCertificate');
    
    /*********************document upload*******************/
    Route::any('/work-permit/upload-document', 'workPermitController@uploadDocument');

    /*********************ajax function for batch processing*******************/
    Route::post('work-permit/ajax/{param}', 'workPermitController@ajaxRequest');
    
    Route::resource('work-permit', 'workPermitController');


    /********************* Certificate **************************/
    //Generate Certificate
    Route::get('work-permit/certificate-gen/{id}','workPermitController@certificate_gen');
    //Re-Generate Certificate
    Route::get('work-permit/certificate-re-gen/{id}','workPermitController@certificate_re_gen');

    //Discard Certificate
    Route::get('work-permit/discard-certificate/{id}','workPermitController@discardCertificate');


    Route::get('work-permit-vr/certificate-link','workPermitController@vrCertificateLink');
    /*     * ********************************End of Route group****************************** */
});
