<?php

namespace App\Modules\workPermit\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\apps\Models\Document;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\Apps\Models\Status;
use App\Modules\Company\Models\Company;
use App\Modules\ProcessPath\Models\ProcessHistory;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Settings\Models\HighComissions;
use App\Modules\Settings\Models\VisaCategories;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\Users;
use App\Modules\visaRecommendation\Models\VisaRecommend;
use App\Modules\workPermit\Models\Apps;
use App\Modules\workPermit\Models\docInfo;
use App\Modules\workPermit\Models\Processlist;
use App\Modules\workPermit\Models\WorkPermit;
use Carbon\Carbon;
use DB;
use Encryption;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use mPDF;
use Session;

class WorkPermitController extends Controller {

    public function __construct() {
// Globally declaring service ID
        $this->service_id = 4; // 4 is Work Permit
    }

    public function index() {
        if (!ACL::getAccsessRight('workPermit', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $userId = CommonFunction::getUserId();
        $getList['result'] = processlist::where('initiated_by', $userId)->get();
        return view("workPermit::list", ['getList' => $getList]);
    }

    public function listOfApp($zone = 0, $ind = 0) {

        if (!ACL::getAccsessRight('workPermit', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            Session::put('sess_user_id', Auth::user()->id);
            $getList = Apps::getClearanceList();
            $appStatus = Status::where('service_id', $this->service_id)->get();
            $statusList = array();
            foreach ($appStatus as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
                $statusList[$v->status_id . 'color'] = $v->color;
            }
            $statusList[-1] = "Draft";
            $statusList['-1' . 'color'] = '#AA0000';

            $desks = UserDesk::all();
            $deskList = array();
            foreach ($desks as $k => $v) {
                $deskList[$v->desk_id] = $v->desk_name;
            }

            // for advance search
            $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
            $validCompanies = Processlist::leftJoin('project_clearance as pc', 'pc.id', '=', 'process_list.record_id')
                            ->where('process_list.status_id', 23)
                            ->where('pc.status_id', 23)
                            ->orderBy('pc.applicant_name', 'ASC')
                            ->lists('pc.applicant_name', 'pc.tracking_number')->all();
            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');
            $mode = 'V';

            return view("workPermit::list", compact('status', 'deskList', 'companyList', 'statusList', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id', 'mode', 'validCompanies', 'economicZone', 'ind', 'zone'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appForm() {
        if (!ACL::getAccsessRight('workPermit', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = '';

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
                $applicant_name = $projectClearanceData->applicant_name;
                $pc_track_no = $projectClearanceData->tracking_number;
            } else {
                $economicZone = '';
                $pc_track_no = '';
            }

            $vrData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                    ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('visa_recommendation.created_by', $authUserId)
                    ->lists('process_list.track_no', 'visa_recommendation.id');

            $vrCData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                    ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('visa_recommendation.created_by', $authUserId)
                    ->lists('visa_recommendation.certificate', 'visa_recommendation.id');

            $passport_types = VisaCategories::where('is_archived', 0)->where('is_active', 1)->orderBy('description')->lists('description', 'id');
            $work_permit_types = ['New' => 'New', 'Extension' => 'Extension'];
            $high_comissions = HighComissions::select('COUNTRY_CODE', DB::raw('CONCAT(NAME, ", ", ADDRESS) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'COUNTRY_CODE');


            $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $countriesWithoutBD = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $nationality = ['' => 'Select One'] + Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso')->all();
            $nationalityWithoutBD = Countries::orderby('nationality')->where('nationality', '!=', '')->where('country_code', '!=', '001')->lists('nationality', 'iso');

            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get(); // 4 is for work permit service

            $clr_document = Document::where('app_id', $this->service_id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }

            $logged_user_info = Users::where('id', $authUserId)->first();

            $viewMode = 'off';
            $mode = 'A';
            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
            $currency = ['' => 'Select One'] + Currencies::orderby('code')->where('is_active', 1)->lists('code', 'id')->all();

            return view("workPermit::app-form", compact('work_permit_types', 'countries', 'countriesWithoutBD', 'divition_eng', 'district_eng', 'projectClearanceData', 'economicZone', 'applicant_name', 'vrData', 'vrCData', 'document', 'alreadyExistApplicant', 'logged_user_info', 'viewMode', 'clrDocuments', 'nationality', 'nationalityWithoutBD', 'passport_types', 'high_comissions', 'payment_method', 'currency', 'mode'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appStore(Request $request, WorkPermit $workPermit, Processlist $processlist) {
        $app_id = (!empty($request->get('app_id')) ? Encryption::decodeId($request->get('app_id')) : '');
        try {
            $authUserId = CommonFunction::getUserId();

            $alreadyExistApplicant = WorkPermit::where('created_by', $authUserId)->where('id', $app_id)->orderBy('id', 'ASC')->first();
            if ($alreadyExistApplicant) {
                if (!ACL::getAccsessRight('workPermit', 'E', $app_id))
                    die('You have no access right! Please contact with system admin if you have any query.');
                $workPermit = $alreadyExistApplicant;
                if ($alreadyExistApplicant->created_by != $authUserId) {
                    session()->flash('error', 'You are not authorized person to edit the application.');
                    return redirect()->back();
                }
            } else {
                if (!ACL::getAccsessRight('workPermit', 'A'))
                    die('You have no access right! Please contact with system admin if you have any query.');
            }

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();

            if (Auth::user()->user_type == '5x505' && empty($projectClearanceData) && empty($request->get('sv_draft'))) {
                Session::flash('error', "You must have an approved project clearance certificate attached to your profile!");
                return redirect()->back();
            }

            if (!empty($projectClearanceData)) {
                $economicZone = $projectClearanceData->eco_zone_id;
                $applicant_name = $projectClearanceData->applicant_name;
            } else {
                $economicZone = '';
                $applicant_name = '';
            }

            $workPermit->visa_recommend_no = $request->get('visa_recommend_no');
            $workPermit->passport_types = $request->get('passport_types');
            $workPermit->work_permit_types = $request->get('work_permit_types');

            $workPermit->applicant_name = $applicant_name;
            $workPermit->eco_zone_id = $economicZone;
            $workPermit->country = $request->get('country');
            $workPermit->division = $request->get('division');
            $workPermit->district = $request->get('district');
            $workPermit->state = $request->get('state');
            $workPermit->province = $request->get('province');
            $workPermit->road_no = $request->get('road_no');
            $workPermit->house_no = $request->get('house_no');
            $workPermit->post_code = $request->get('post_code');
            $workPermit->phone = $request->get('phone');
            $workPermit->fax = $request->get('fax');
            $workPermit->email = $request->get('email');
            $workPermit->website = $request->get('website');

            if ($request->get('same_as_authorized') != '') {
                $workPermit->same_as_authorized = $request->get('same_as_authorized');
            }
            $workPermit->correspondent_name = $request->get('correspondent_name');
            $workPermit->correspondent_nationality = $request->get('correspondent_nationality');
            $workPermit->correspondent_passport = $request->get('correspondent_passport');
            $workPermit->correspondent_country = $request->get('correspondent_country');
            $workPermit->correspondent_division = $request->get('correspondent_division');
            $workPermit->correspondent_district = $request->get('correspondent_district');
            $workPermit->correspondent_state = $request->get('correspondent_state');
            $workPermit->correspondent_province = $request->get('correspondent_province');
            $workPermit->correspondent_road_no = $request->get('correspondent_road_no');
            $workPermit->correspondent_house_no = $request->get('correspondent_house_no');
            $workPermit->correspondent_post_code = $request->get('correspondent_post_code');
            $workPermit->correspondent_phone = $request->get('correspondent_phone');
            $workPermit->correspondent_fax = $request->get('correspondent_fax');
            $workPermit->correspondent_email = $request->get('correspondent_email');
            $workPermit->correspondent_website = $request->get('correspondent_website');

            $workPermit->incumbent_national_name = $request->get('incumbent_national_name');
            $workPermit->incumbent_nationality = $request->get('incumbent_nationality');
            $workPermit->incumbent_gender = $request->get('incumbent_gender');
            $workPermit->incumbent_passport = $request->get('incumbent_passport');
            if ($request->get('incumbent_pass_expire') != '')
                $workPermit->incumbent_pass_expire = CommonFunction::changeDateFormat($request->get('incumbent_pass_expire'), true);
            if ($request->get('incumbent_pass_issue_date') != '')
                $workPermit->incumbent_pass_issue_date = CommonFunction::changeDateFormat($request->get('incumbent_pass_issue_date'), true);
            $workPermit->incumbent_pass_issue_place = $request->get('incumbent_pass_issue_place');
            $workPermit->incumbent_country = $request->get('incumbent_country');
            $workPermit->incumbent_division = $request->get('incumbent_division');
            $workPermit->incumbent_state = $request->get('incumbent_state');
            $workPermit->incumbent_district = $request->get('incumbent_district');
            $workPermit->incumbent_province = $request->get('incumbent_province');
            $workPermit->incumbent_road_no = $request->get('incumbent_road_no');
            $workPermit->incumbent_house_no = $request->get('incumbent_house_no');
            $workPermit->incumbent_post_code = $request->get('incumbent_post_code');
            $workPermit->incumbent_email = $request->get('incumbent_email');
            $workPermit->incumbent_phone = $request->get('incumbent_phone');
            $workPermit->incumbent_fax = $request->get('incumbent_fax');
            if ($request->get('incumbent_dob') != '')
                $workPermit->incumbent_dob = CommonFunction::changeDateFormat($request->get('incumbent_dob'), true);
            $workPermit->incumbent_martial_status = $request->get('incumbent_martial_status');
            $workPermit->incumbent_degree = $request->get('incumbent_degree');
            $workPermit->incumbent_institute = $request->get('incumbent_institute');
            $workPermit->incumbent_result = $request->get('incumbent_result');

            if ($request->get('incumbent_certificate')) {
                $file = $request->file('incumbent_certificate');
                $size = $file->getSize();
                $extension = $file->getClientOriginalExtension();
                $valid_formats = array("pdf");
                if (in_array($extension, $valid_formats)) {
                    if ($size < (1024 * 1024 * 3)) {
                        $original_file = $file->getClientOriginalName();
                        $file->move('uploads/', time() . $original_file);
                        $fileName = 'uploads/' . time() . $original_file;

                        $workPermit->incumbent_certificate = $fileName;
                    } else {
                        Session::flash('error_message', "File size must be less than 3 megabyte");
                        return redirect()->back();
                    }
                } else {
                    Session::flash('error_message', "File format is not valid! Please upload an pdf format");
                    return redirect()->back();
                }
            }

            $workPermit->post_name = $request->get('post_name');
            if ($request->get('arrival_in_bd') != '')
                $workPermit->arrival_in_bd = CommonFunction::changeDateFormat($request->get('arrival_in_bd'), true);
            if ($request->get('employ_start_date') != '')
                $workPermit->employ_start_date = CommonFunction::changeDateFormat($request->get('employ_start_date'), true);
            if ($request->get('employ_end_date') != '')
                $workPermit->employ_end_date = CommonFunction::changeDateFormat($request->get('employ_end_date'), true);
            $workPermit->employ_duration = $request->get('employ_duration');
            $workPermit->employ_job_desc = $request->get('employ_job_desc');
            $workPermit->employ_justification = $request->get('employ_justification');

            if ($request->get('employ_job_ad')) {
                $file = $request->file('employ_job_ad');
                $size = $file->getSize();
                $extension = $file->getClientOriginalExtension();
                $valid_formats = array("pdf");
                if (in_array($extension, $valid_formats)) {
                    if ($size < (1024 * 1024 * 3)) {
                        $original_file = $file->getClientOriginalName();
                        $file->move('uploads/', time() . $original_file);
                        $fileName = 'uploads/' . time() . $original_file;

                        $workPermit->employ_job_ad = $fileName;
                    } else {
                        Session::flash('error_message', "File size must be less than 3 megabyte");
                        return redirect()->back();
                    }
                } else {
                    Session::flash('error_message', "File format is not valid! Please upload an pdf format");
                    return redirect()->back();
                }
            }

            $workPermit->salary_pay_method = $request->get('salary_pay_method');
            $workPermit->salary_local_amount = $request->get('salary_local_amount');
            $workPermit->salary_local_currency = $request->get('salary_local_currency');
            $workPermit->salary_foreign_amount = $request->get('salary_foreign_amount');
            $workPermit->salary_foreign_currency = $request->get('salary_foreign_currency');
            $workPermit->oversea_pay_method = $request->get('oversea_pay_method');
            $workPermit->oversea_local_amount = $request->get('oversea_local_amount');
            $workPermit->oversea_local_currency = $request->get('oversea_local_currency');
            $workPermit->oversea_foreign_amount = $request->get('oversea_foreign_amount');
            $workPermit->oversea_foreign_currency = $request->get('oversea_foreign_currency');
            $workPermit->house_pay_method = $request->get('house_pay_method');
            $workPermit->house_local_amount = $request->get('house_local_amount');
            $workPermit->house_local_currency = $request->get('house_local_currency');
            $workPermit->house_foreign_amount = $request->get('house_foreign_amount');
            $workPermit->house_foreign_currency = $request->get('house_foreign_currency');
            $workPermit->conveyance_pay_method = $request->get('conveyance_pay_method');
            $workPermit->conveyance_local_amount = $request->get('conveyance_local_amount');
            $workPermit->conveyance_local_currency = $request->get('conveyance_local_currency');
            $workPermit->conveyance_foreign_amount = $request->get('conveyance_foreign_amount');
            $workPermit->conveyance_foreign_currency = $request->get('conveyance_foreign_currency');
            $workPermit->medical_pay_method = $request->get('medical_pay_method');
            $workPermit->medical_local_amount = $request->get('medical_local_amount');
            $workPermit->medical_local_currency = $request->get('medical_local_currency');
            $workPermit->medical_foreign_amount = $request->get('medical_foreign_amount');
            $workPermit->medical_foreign_currency = $request->get('medical_foreign_currency');
            $workPermit->entertain_pay_method = $request->get('entertain_pay_method');
            $workPermit->entertain_local_amount = $request->get('entertain_local_amount');
            $workPermit->entertain_local_currency = $request->get('entertain_local_currency');
            $workPermit->entertain_foreign_amount = $request->get('entertain_foreign_amount');
            $workPermit->entertain_foreign_currency = $request->get('entertain_foreign_currency');
            $workPermit->bonus_pay_method = $request->get('bonus_pay_method');
            $workPermit->bonus_local_amount = $request->get('bonus_local_amount');
            $workPermit->bonus_local_currency = $request->get('bonus_local_currency');
            $workPermit->bonus_foreign_amount = $request->get('bonus_foreign_amount');
            $workPermit->bonus_foreign_currency = $request->get('bonus_foreign_currency');
            $workPermit->fringe_benefits = $request->get('fringe_benefits');
            $workPermit->salary_remarks = $request->get('salary_remarks');

            $workPermit->local_executive = $request->get('local_executive');
            $workPermit->local_stuff = $request->get('local_stuff');
            $workPermit->local_total_no = $request->get('local_total_no');
            $workPermit->foreign_executive = $request->get('foreign_executive');
            $workPermit->foreign_stuff = $request->get('foreign_stuff');
            $workPermit->foreign_total = $request->get('foreign_total');
            $workPermit->mp_total = $request->get('mp_total');
            $workPermit->mp_ratio_local = $request->get('mp_ratio_local');
            $workPermit->mp_ratio_foreign = $request->get('mp_ratio_foreign');

            $workPermit->service_id = $this->service_id;
            
            $acceptTerms = (!empty($request->get('acceptTerms')) ? 1 : 0);
            $workPermit->acceptance_of_terms = $acceptTerms;

            $workPermit->created_by = $authUserId;
            $workPermit->save();


            /* For Tracking ID generating and update */
            if (empty($request->get('sv_draft'))) {
                /* if applicant submit the application without saving as draft or from draft submit an application track no will be generated
                  // if status id = 5 or shortfall, tracking id will not be regenerated */
                if ((!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number == '' && $alreadyExistApplicant->status_id != 5) ||
                        empty($alreadyExistApplicant)) {
                    $tracking_number = 'WP-' . date("dMY") . $this->service_id . str_pad($workPermit->id, 6, '0', STR_PAD_LEFT);
                } else {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                }
            } else {
                if (!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number != '' && $alreadyExistApplicant->status_id == 5) {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                } else {
                    $tracking_number = '';
                }
            }

            if (empty($request->get('sv_draft'))) {
                $workPermit->status_id = 1;
                $workPermit->is_draft = 0;
            } else {
                $workPermit->status_id = -1;
                $workPermit->is_draft = 1;
            }

            $workPermit->tracking_number = $tracking_number;
            $workPermit->save();

            $doc_row = docInfo::where('service_id', $this->service_id)->get(['doc_id', 'doc_name']);

            /* Start file uploading */
            if (isset($doc_row)) {
                foreach ($doc_row as $docs) {
//                if ($request->get('validate_field_' . $docs->doc_id) != '') {
                    $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ? $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                    $documnent_id = $docs->doc_id;
                    if ($request->get('document_id_' . $docs->doc_id) == '') {
                        Document::create([
                            'service_id' => $this->service_id, // 4 for Work Permit
                            'app_id' => $workPermit->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    } else {
                        $documentId = $request->get('document_id_' . $docs->doc_id);
                        Document::where('id', $documentId)->update([
                            'service_id' => $this->service_id, // 4 for Work Permit
                            'app_id' => $workPermit->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    }
//                    }
                }
            }
            /* End of file uploading */

            /* Save data to process_list table */
            $processlistExist = Processlist::where('record_id', $workPermit->id)->where('service_id', $this->service_id)->first();
            $deskId = 0;
            if (!empty($request->get('sv_draft'))) {
                $statusId = -1;
                $deskId = 0;
            } else {
                $statusId = 1;
                $deskId = 3; // 3 is RD1
            }

            if (count($processlistExist) < 1) {
                $process_list_insert = Processlist::create([
                            'track_no' => $tracking_number,
                            'reference_no' => '',
                            'company_id' => '',
                            'service_id' => $this->service_id,
                            'initiated_by' => CommonFunction::getUserId(),
                            'closed_by' => 0,
                            'status_id' => $statusId,
                            'desk_id' => $deskId,
                            'record_id' => $workPermit->id,
                            'eco_zone_id' => $economicZone,
                            'process_desc' => '',
                            'updated_by' => CommonFunction::getUserId()
                ]);
            } else {
                if ($processlistExist->status_id > -1) {
                    if (empty($request->get('sv_draft'))) {
                        $statusId = 10; // resubmitted
                    } else {
                        $statusId = $processlistExist->status_id; // if drafted, older status will remain
                    }
                }
                $processlisUpdate = array(
                    'track_no' => $tracking_number,
                    'service_id' => $this->service_id,
                    'status_id' => $statusId,
                    'desk_id' => $deskId,
                    'eco_zone_id' => $economicZone,
                );
                $processlist->update_app_for_wp($workPermit->id, $processlisUpdate);
            }

            if (empty($request->get('sv_draft'))) {
                $flassMsg = "Your application has been submitted with tracking no: <strong>" . $tracking_number . "</strong>";
            } else {
                $flassMsg = "Your application has been drafted successfully";
            }

            Session::flash('success', $flassMsg);
            $listOfApp = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
            return redirect($listOfApp);
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormEdit($id) {
        $app_id = Encryption::decodeId($id);
        if (!ACL::getAccsessRight('workPermit', 'E', $app_id)) {
            abort('400',"You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = WorkPermit::where('id', $app_id)->first();
            if ($alreadyExistApplicant) {
                $alreadyExistProcesslist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
                if ($alreadyExistProcesslist) {
                    if ($alreadyExistProcesslist->status_id != '-1' && $alreadyExistProcesslist->status_id != 5) { // -1 = draft, 5 = shortfall
                        Session::flash('error', "You have no access right! Please contact system administration for more information.");
                        $listOfApp = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
                        return redirect($listOfApp);
                    }
                }
            }
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
                $applicant_name = $projectClearanceData->applicant_name;
            } else {
                $economicZone = '';
                $applicant_name = '';
            }

            $vrData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                    ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('visa_recommendation.created_by', $authUserId)
                    ->lists('process_list.track_no', 'visa_recommendation.id');

            $vrCData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                    ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('visa_recommendation.created_by', $authUserId)
                    ->get(['visa_recommendation.certificate', 'visa_recommendation.id']);
            //dd($vrCData[0]['certificate']);

            $passport_types = VisaCategories::where('is_archived', 0)->where('is_active', 1)->orderBy('description')->lists('description', 'id');
            $work_permit_types = ['New' => 'New', 'Extension' => 'Extension'];

            $high_comissions = HighComissions::select('COUNTRY_CODE', DB::raw('CONCAT(NAME, ", ", ADDRESS) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'COUNTRY_CODE');

            $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $countriesWithoutBD = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $nationality = ['' => 'Select One'] + Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso')->all();
            $nationalityWithoutBD = Countries::orderby('nationality')->where('nationality', '!=', '')->where('country_code', '!=', '001')->lists('nationality', 'iso');


            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();

            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
            $currency = ['' => 'Select One'] + Currencies::orderby('code')->where('is_active', 1)->lists('code', 'id')->all();

            $document = docInfo::where('service_id', $this->service_id)->orderBy('doc_name')->get();

            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }

                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $clrDocuments = [];
                $processlistExist = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();

            $viewMode = 'off';
            $mode = 'E';
            return view("workPermit::app-form", compact('alreadyExistApplicant', 'passport_types', 'work_permit_types', 'high_comissions', 'payment_method', 'countries', 'countriesWithoutBD', 'divition_eng', 'district_eng', 'projectClearanceData', 'economicZone', 'vrData', 'vrCData', 'applicant_name', 'document', 'logged_user_info', 'clrDocuments', 'nationality', 'nationalityWithoutBD', 'viewMode', 'currency', 'processlistExist', 'mode'));
        } catch (Exception $e) {
            dd($e->getMessage() . '   ' . $e->getLine());
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormView($id) {
        if (!ACL::getAccsessRight('workPermit', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $app_id = Encryption::decodeId($id);
        try {
//        breadcrumb start
            $form_data = WorkPermit::where('id', $app_id)->first();
            $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
            $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = WorkPermit::where('id', $app_id)->first();

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                    ->first();

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
                $applicant_name = $projectClearanceData->applicant_name;
                $pc_track_no = $projectClearanceData->tracking_number;
            } else {
                $economicZone = '';
                $applicant_name = '';
                $pc_track_no = '';
            }

            $vrData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                    ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('visa_recommendation.created_by', $authUserId)
                    ->lists('process_list.track_no', 'visa_recommendation.id');

            $vrCData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                    ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('visa_recommendation.created_by', $authUserId)
                    ->lists('visa_recommendation.certificate', 'visa_recommendation.id');

            $passport_types = VisaCategories::where('is_archived', 0)->where('is_active', 1)->orderBy('description')->lists('description', 'id');
            $work_permit_types = ['New' => 'New', 'Extension' => 'Extension'];

            $high_comissions = HighComissions::select('COUNTRY_CODE', DB::raw('CONCAT(NAME, ", ", ADDRESS) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'COUNTRY_CODE');

            $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $countriesWithoutBD = Countries::where('country_status', 'Yes')->where('country_code', '!=', '001')->orderBy('nicename', 'asc')->lists('nicename', 'iso');
            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            $nationalityWithoutBD = Countries::orderby('nationality')->where('nationality', '!=', '')->where('country_code', '!=', '001')->lists('nationality', 'iso');

            $currency = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id')->all();
            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();

            $document = docInfo::where('service_id', $this->service_id)->orderBy('doc_name')->get();
            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $clrDocuments = [];
                $processlistExist = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $viewMode = 'on';
            $mode = 'V';

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

            return view("workPermit::app-form", compact(
                            'passport_types', 'work_permit_types', 'high_comissions', 'countries', 'countriesWithoutBD', 'divition_eng', 'district_eng', 'projectClearanceData', 'economicZone', 'applicant_name', 'vrData', 'vrCData', 'document', 'currency', 'payment_method', 'alreadyExistApplicant', 'logged_user_info', 'clrDocuments', 'nationality', 'nationalityWithoutBD', 'viewMode', 'processlistExist', 'process_history', 'form_data', 'process_data', 'statusArray', 'banks', 'mode'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appDownloadPDF($id) {
        $app_id = Encryption::decodeId($id);
//        breadcrumb start
        $form_data = WorkPermit::where('id', $app_id)->first();
        $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end
        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = WorkPermit::where('id', $app_id)->first();

            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();

            if (!empty($projectClearanceData)) {
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
                $applicant_name = $projectClearanceData->applicant_name;
                $pc_track_no = $projectClearanceData->tracking_number;
            } else {
                $economicZone = '';
                $applicant_name = '';
                $pc_track_no = '';
            }

            $vrData = VisaRecommend::leftJoin('process_list', 'process_list.record_id', '=', 'visa_recommendation.id')
                ->where('process_list.service_id', 3) // 3 is for Visa Recommendation
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('visa_recommendation.created_by', $authUserId)
                ->lists('process_list.track_no', 'visa_recommendation.id');

            $passport_types = VisaCategories::where('is_archived', 0)->where('is_active', 1)->orderBy('description')->lists('description', 'id');
            $work_permit_types = ['New' => 'New', 'Extension' => 'Extension'];

            $high_comissions = HighComissions::select('COUNTRY_CODE', DB::raw('CONCAT(NAME, ", ", ADDRESS) AS commission'))
                    ->orderBy('commission')
                    ->lists('commission', 'COUNTRY_CODE');
            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currency = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id')->all();
            $payment_method = ['Monthly' => 'Monthly', 'Yearly' => 'Yearly'];
            $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
            $divition_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 1)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $district_eng = ['' => 'Select One'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();

            $document = docInfo::where('service_id', $this->service_id)->orderBy('doc_name')->get();
            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                }
                $processlistExist = Processlist::where('record_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)->first();
            } else {
                $clrDocuments = [];
                $processlistExist = [];
            }
            $logged_user_info = Users::where('id', $authUserId)->first();
            $viewMode = 'on';
            $mode = 'V';

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));
            $content = view("workPermit::app-form-html", compact('passport_types',
                'vrData',
                'work_permit_types', 'high_comissions', 'countries', 'divition_eng',
                'district_eng', 'projectClearanceData', 'economicZone', 'applicant_name',
                'pc_track_no', 'document', 'currency', 'payment_method', 'alreadyExistApplicant',
                'logged_user_info', 'clrDocuments', 'nationality', 'viewMode', 'processlistExist',
                'process_history', 'form_data', 'process_data', 'statusArray',
                'banks', 'mode'))->render();

            $mpdf = new mPDF(
                    'utf-8', // mode - default ''
                    'A4', // format - A4, for example, default ''
                    12, // font size - default 0
                    'dejavusans', // default font family
                    10, // margin_left
                    10, // margin right
                    10, // margin top
                    10, // margin bottom
                    9, // margin header
                    9, // margin footer
                    'P'
            );
            $mpdf->Bookmark('Start of the document');
            $mpdf->useSubstitutions;
            $mpdf->SetProtection(array('print'));
            $mpdf->SetDefaultBodyCSS('color', '#000');
            $mpdf->SetTitle("BEZA");
            $mpdf->SetSubject("Subject");
            $mpdf->SetAuthor("Business Automation Limited");
            $mpdf->autoScriptToLang = true;
            $mpdf->baseScript = 1;
            $mpdf->autoVietnamese = true;
            $mpdf->autoArabic = true;

            $mpdf->setFooter('{PAGENO} / {nb}');
            $mpdf->autoLangToFont = true;
            $mpdf->SetDisplayMode('fullwidth');
            $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');

            $mpdf->setAutoTopMargin = 'stretch';
            $mpdf->setAutoBottomMargin = 'stretch';
            $mpdf->WriteHTML($stylesheet, 1);

            $mpdf->WriteHTML($content, 2);

            $mpdf->defaultfooterfontsize = 10;
            $mpdf->defaultfooterfontstyle = 'B';
            $mpdf->defaultfooterline = 0;

            $mpdf->SetCompression(true);
            $mpdf->Output($process_data->track_no . '.pdf', 'I');  // Saving pdf "F" for Save only, "I" for view only.
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function challanStore($id, Request $request) {
        $app_id = Encryption::decodeId($id);
        if (!ACL::getAccsessRight('workPermit', 'V', $app_id)) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $this->validate($request, [
            'challan_no' => 'required',
            'bank_name' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'branch' => 'required',
            'challan_file' => 'required|mimes:pdf',
        ]);

        $file = $request->file('challan_file');
        $original_file = $file->getClientOriginalName();
        $file->move('uploads/', time() . $original_file);
        $filename = 'uploads/' . time() . $original_file;

        Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->update([
            'status_id' => 26, ///This is for challan re-submitted
            'desk_id' => 6,
        ]);

        WorkPermit::find($app_id)->update([
            'challan_no' => $request->get('challan_no'),
            'status_id' => 26, ///This is for challan re-submitted
            'bank_name' => $request->get('bank_name'),
            'challan_amount' => $request->get('amount'),
            'challan_branch' => $request->get('branch'),
            'challan_date' => Carbon::createFromFormat('d-M-Y', $request->get('date'))->format('Y-m-d'),
            'challan_file' => $filename,
            'updated_by' => CommonFunction::getUserId(),
        ]);
        \Session::flash('success', "Pay order related information has been successfully updated!");
        return redirect()->back();
    }

    function ajaximage() {
        return view('workPermit::ajaximage');
    }

    public function uploadDocument() {
        return view('workPermit::ajaxUploadFile');
    }

    public function preview() {
        return view("workPermit::preview");
    }

    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');

        if ($param == 'process') {

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;

            $verifiedInfo = WorkPermit::where('id', $curr_app_id)->first();
            $sql = "SELECT DGN.desk_id, DGN.desk_name
                            FROM user_desk DGN
                            WHERE
                            find_in_set(DGN.desk_id,
                            (SELECT desk_to FROM work_permit_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            // Get all applications' id
            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition
            $statusId_wz = sprintf("%02d", $statusId);

            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {
                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM work_permit_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";

            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {

            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-status-list') {

            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');

            $verifiedInfo = WorkPermit::where('id', $curr_app_id)->first();
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM work_permit_process_path APP WHERE APP.status_from = '$statusId' $cond))
                        AND APS.service_id = $this->service_id
                        order by APS.status_name";

            $statusList = \DB::select(DB::raw($sql));

            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    public function updateBatch(Request $request, processlist $process_model) {

        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $status_id = $request->get('status_id');
        $attach_file = $request->file('attach_file');
        $service_id = $this->service_id;
        $onbehalf = $request->get('on_behalf_of');
        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }

        foreach ($apps_id as $app_id) {
            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->service_id = $this->service_id;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }

            $appInfo = Processlist::where('record_id', $app_id)->where('service_id', '=', $service_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;

            if (empty($desk_id)) {
                $whereCond = "select * from work_permit_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'
                        AND status_to REGEXP '^([0-9]*[,]+)*$status_id([,]+[,0-9]*)*$'";

                $processPath = DB::select(DB::raw($whereCond));
                if ($processPath[0]->desk_to == '0')  // Sent to Applicant
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')   // Keep in same desk
                    $desk_id = $deskFrom;
            }

            $app_data = array(
                'status_id' => $status_id,
                'remarks' => $remarks,
                'updated_at' => date('y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            );

            $info_data = array(
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8 || $status_id == 14) {
                $info_data['closed_by'] = Auth::user()->id;
            }

            Processlist::where('record_id', $app_id)
                    ->where('service_id', $this->service_id)
                    ->update($info_data);

            WorkPermit::where('id', $app_id)->update($app_data);

            WorkPermit::where('id', $app_id)->update([
                'status_id' => $status_id,
                'remarks' => $remarks]);

            if (in_array($status_id, array(5, 8, 23, 21, 22, 25, 27))) {
                $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $service_id)->first();

//            Notification
                $body_msg = '<span style="color:#000;text-align:justify;"><b>';
                if ($status_id == 21 || $status_id == 24) {
                    $smsbody = "Will be given by BEZA later";
                    $applicantMobile = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_phone', 'users');

                    $params = array([
                            'emailYes' => '0', 'emailYes' => '0',
                            'emailTemplate' => 'users::message',
                            'emailSubject' => 'Application has been accepted',
                            'emailBody' => $smsbody,
                            'emailHeader' => 'Successfully Saved the Application header',
                            'emailAdd' => 'shahin.fci@gmail.com',
                            'mobileNo' => $applicantMobile,
                            'smsYes' => '1',
                            'smsBody' => 'Your application has been submitted with tracking id: ' . $process_data->track_no .
                            ' received. Please fill up your pay order related information!',
                    ]);
                    CommonFunction::sendMessageFromSystem($params);
                    $body_msg .= 'Your application for ' . $process_data->track_no . ' has been ' .
                            CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status') .
                            ', Please give your  pay order related information!';
                } else {
                    $body_msg .= 'Your application for work permit with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' . CommonFunction::getFieldName($status_id, 'status_id', 'status_name', 'app_status') . '</b>';
                }

                $body_msg .= '</span>';
                $body_msg .= '<br/><br/><br/>Thanks<br/>';
                $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                $data = array(
                    'header' => 'Application Update',
                    'param' => $body_msg
                );
                $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');
                $certificate = '';
                $billMonth = date('Y-m');
                if ($status_id == 23) {
                    $certificate = $this->certificate_gen($app_id);   //Certificate Generation after payment accepted
                }

                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $app_id, $certificate, $status_id,$billMonth) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                            ->to($fetched_email_address)
                            ->subject('Application Update Information for work permit');
                    if ($status_id == 23) {
                        $message->attach($certificate);
                        WorkPermit::where('id', $app_id)->update(['certificate' => $certificate,'bill_month'=>$billMonth]);
                    }
                });
            }
        }

        //         for previous and present status
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        Session::flash('success', "Application status updated Previous status: $statusList[$status_from] || Present Status: $statusList[$status_id]");
        return redirect()->back();
    }

    public function discardCertificate($id) {
        $app_id = Encryption::decodeId($id);
        if (Auth::user()->user_type == '1x101') {
            $appInfo = WorkPermit::find($app_id);
            if ($appInfo->status_id == 23) {
                $appInfo->status_id = 40;
                $appInfo->certificate = '';
                $appInfo->save();

                Processlist::where(['record_id' => $app_id, 'service_id' => $this->service_id])
                        ->update(['desk_id' => 0, 'status_id' => 40]);
                Session::flash('success', 'Certificate Discard');
                return redirect()->back();
            } else {
                Session::flash('error', 'Certificate discard is not possible [WP9002]');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'You are not authorized to discard certificate [WP9001]');
            return redirect()->back();
        }
    }

    public function certificate_re_gen($id) {
        $app_id = Encryption::decodeId($id);
        $processlistExist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        $certificate = '';
        if ($status_id == 23) {
            $certificate = $this->certificate_gen($app_id);   //Certificate Generation after payment accepted
            WorkPermit::where('id', $app_id)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function certificate_gen($appID) {
        ini_set('memory_limit', '99M');

        $form_data = Processlist::leftJoin('work_permit as wp', 'wp.id', '=', 'process_list.record_id')
                ->leftJoin('project_clearance as pc', 'pc.created_by', '=', 'wp.created_by')
                ->where('pc.status_id', 23)
//            ->where('process_list.initiated_by', '=', CommonFunction::getUserId())
                ->where('process_list.service_id', '=', $this->service_id)
                ->where('wp.id', $appID)
                ->first(['process_list.*', 'wp.*', 'pc.proposed_name', 'pc.eco_zone_id', 'pc.product_name', 'pc.production_cost', 'pc.business_type', 'pc.organization_type']);
        $desk_data = Users::where('desk_id', Auth::user()->desk_id)->first();


        $desk_name = CommonFunction::getFieldName(8, 'desk_id', 'desk_name', 'user_desk');

        if (!empty($desk_data->signature)) {
            $signature = '<img src="users/signature/' . $desk_data->signature . '" width="100" height="50">';
        } else {
            $signature = '';
        }


        if (!$form_data) {
            return '';
        } else {

            $ApproveData = ProcessHistory::where('process_id', $form_data->process_id)
                            ->where('record_id', $appID)
                            ->where('status_id', 23) // 23 = approved
                            ->orderBy('p_hist_id', 'asc')->first();
            $formatted_date = '';
            if (!empty($ApproveData->updated_at)) {
                $formatted_date = date_format($ApproveData->updated_at, "d-M-Y");
            }
            $dateNow = !empty($formatted_date) ? $formatted_date : '';

            $track_no = (!empty($form_data->tracking_number) ? $form_data->tracking_number : '');
            $proposed_name = (!empty($form_data->proposed_name) ? $form_data->proposed_name : '');
            $eco_zone_id = (!empty($form_data->eco_zone_id) ? $form_data->eco_zone_id : '');
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);
            $product_name = (!empty($form_data->product_name) ? $form_data->product_name : '');
            $production_cost = (!empty($form_data->production_cost) ? $form_data->production_cost : 0 );
            $business_type = (!empty($form_data->business_type) ? $form_data->business_type : '');
            $organization_type = (!empty($form_data->organization_type) ? $form_data->organization_type : '');
        }

        $content = view('workPermit::certificate', compact('track_no', 'dateNow', 'proposed_name', 'economicZones', 'product_name', 'production_cost', 'business_type', 'organization_type', 'signature'))->render();

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');
        $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;
        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("beza_" . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->SetCompression(true);
        $mpdf->WriteHTML($content);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf
        return $pdfFilePath;
    }

    public function vrCertificateLink(Request $request) {
        $visa_recommend_id = $request->get('visa_recommend_no');
        $vr_certificate = VisaRecommend::where('id', $visa_recommend_id)->pluck('certificate');
        return json_encode(array('data' => $vr_certificate));
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', 1)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = Status::where('service_id', 1)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('projectClearance::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

        $industrial_category = $request->get('industry_cat_id');
        $economic_zone = $request->get('eco_zone_id');

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $industrial_category, $economic_zone);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;
        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('workPermit::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();

// Code to count the total number of application for different services as a whole
        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, pl.eco_zone_id, industry_cat_id
                                            from service_info si
                                            left join process_list pl on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id !='-1'
                                            left join (select industry_cat_id, created_by
                                                    from project_clearance
                                                    where industry_cat_id = '$industrial_category'
                                                     limit 1) pc
                                             on pl.initiated_by = pc.created_by
                                            group by si.id;");

        $str = "";
        foreach ($sql as $sql_data) {
            $short_name = $sql_data->short_name;
            $id = $sql_data->id;
            $industry_cat_id = $sql_data->industry_cat_id;
            $application_number = $sql_data->application_number;
            $eco_zone_id = $sql_data->eco_zone_id;
            $str.="$short_name:$application_number:$industry_cat_id:$eco_zone_id==";
        }
        $data = ['responseCode' => 1, 'data' => $contents, 'totalApps' => $str]; // pattern= Module:AppsCount:Ind:zone
        // End of code to count total number of applications

        return response()->json($data);
    }

    //*********************End of Controller Class**********************//
}
