<?php

namespace App\Modules\workPermit\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Issued extends Model {

    protected $table = 'issued_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issued_id',
        'issued_name',
        'issued_url',
        'issued_ref_no',
        'issued_type',
        'issued_status'
    ];

    /********************************End of Model Class*********************************/
}
