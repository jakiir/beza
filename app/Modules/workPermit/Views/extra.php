<?php
/**
 * Created by PhpStorm.
 * User: khaleda
 * Date: 10/19/15
 * Time: 3:32 PM
 */
?>

<div class="form-open col-md-12 " style="display: none;border: 10px solid #00a65a;">
    <br/>
    <div class="form-group col-md-8 pull-left">
        <img class="img-circle img-responsive" width="100" src="{{ asset('front-end/images/logo.gif') }}">
    </div>
    <div class="form-group col-md-4 pull-right">
        <h2><strong style="color:#19609A;">Board of Investment</strong></h2>
        <br/><strong> Prime Minister's Office</strong>
    </div>
    <div class="form-group col-md-8 pull-left">
        The Managing Director,<br/>
        <span id="ORG_NAME"></span><br/>
        <span id="ORG_HOUSE_NO"></span>,<span id="ORG_FLAT_NO"></span><br/>
        <span id="ORG_ROAD_NO"></span>,<span id="ORG_THANA"></span>,<span id="ORG_POST_OFFICE"></span><br/>
        <span id="ORG_DISTRICT"></span><br/>
        <span id="ORG_POST_CODE"></span><br/>
    </div>
    <div class="form-group col-md-12 pull-left">
        <strong>Sub: Work Permit For Foreign National</strong><br/><br/>
        Dear Sir,<br/>
        With Reference To Your Applications Received On <span id="ENTRY_DT"></span> & <span id="BANK_ISSUE_DATE"></span> On The Above Subject, This Is To Inform You That According To
        The Decision Of <span id="MEETING_AGENDA"></span>Th Inter-Ministerial Committee Meeting Held On <span id="MEETING_DATE"></span> , The Board Of Investment Of Bangladesh Has Been Pleased To Issue Work
        Permit For The Following Foreign National Employed In Your Company/office For The Period Shown Against His Name On The Terms, Conditions And
        Remuneration As Shown Below:
        <br/>
        <table class="table table-bordered">
            <tr>
                <td align="center">Name, Designation<br/>
                    Nationality And Passport No</td>
                <td align="center">Period Of Work Permit</td>
                <td colspan="3" align="center">Remuneration</td>
            </tr>
            <tr>
                <td rowspan="3" align="center" ><span id="EXP_NAME" style="text-transform:uppercase;"></span><br>
                    <span id="POST_EMP_NAME"></span><br>
                    <span id="NATIONALITY"></span><br>
                    <span id="PASSPORT_NO"></span></td>
                <td rowspan="3" align="center"><span id="PERIOD_VALIDITY"></span><br>
                    (with effect from<br>
                    <span id="PERMIT_EFCT_DATE"></span>)</td>
                <td align="center">Payment</td>
                <td align="center">Payable Locally</td>
                <td align="center">Payable Abroad</td>
            </tr>
            <tr>
                <td align="center">Daily</td>
                <td align="center">Basic salary<br>
                    BDT 150000</td>
                <td align="center">Basic salary<br>
                    BDT 1000</td>
            </tr>
            <tr>
                <td colspan="3" align="center">&nbsp;</td>
            </tr>
        </table>
        <br/>
        <ul>
            <li>The Expenditure In This Connection Would Be Borne Out Of Foreign Exchange Remittance Received From Abroad. This Condition Is Not Applicable
                In Case Of Appointment Of Foreign Nationals In Foreign Bank,insurance, Airlines, Bangladeshi Company Etc.
            </li><li> If Your Company Earns Profit Locally And Wants To Meet The Necessary Expenditure Of The Company In Bangladesh With That Profit, They Can
                Apply To The Board With Proper Justification And Sufficient Supporting Documents For Exemption Of Condition-1.
            </li><li>  Appointment Of Bangladeshi National As Per Ratio I.e. One Foreign National: Five Bangladeshi National [1:5] With Suitable And Appropriate Pay
                Structure Shall Have To Be Ensured.
            </li><li> Income Tax Payable To The Government Shall Have To Be Deducted At Source And To Be Paid As Per Provisions Of Bangladesh Government I.t
                Ordinance 1984.
            </li><li> The Foreign National(S) Is/are Required To Submit Income Tax Certificate/income Tax Exemption Certificate According To The Provision Of Section 107
                Of Income Tax Ordinance, 1984 Before The Concerned Income Tax Office At The Time Of Leaving Bangladesh As Notified In Office Memorandum File
                No.zarabo/kar-7/a:a:b:/16/2002/38 Dated 18/03/2002 Of The Nbr.
            </li><li> The Date Of Departure Of The Expatriate(S) Must Be Intimated To This Board Within 7(Seven) Days From The Date Of Expiry Of The Work Permit.
                The Company Will Also Be Held Responsible For The Illegal Stay/employment Of The Foreign National, After Expiry Of The Work Permit.
            </li><li>  Ministry Of Home Affairs Will Issue Security Clearance Of The Expatriate After Verification.
            </li><li> The Foreign National Shall Have To Be Enlisted With Bangladesh Bank Under Section 18(A)( In Case Of Branch/liaison/representative Office) Of The
                Foreign Exchange Act,1947.</li>
        </ul>
        <br/>
    </div>
    <div class="form-group col-md-9 pull-left">
        Copy forwarded for kind information & necessary action to:<br/>
        1. Bangladesh Bank, Motijheel C/A,Dhaka (Attention: The Governor).<br/>
        2. Ministry of Foreign affairs, Segunbagicha, Dhaka (Attention: Foreign Secretary).<br/>
        3. Ministry of Home Affairs, Bangladesh Secretariat, Dhaka (Attention: The Secretary).<br/>
        4. Department of Immigration and Passport, Sher-e-Bangla Nagar, Agargaon,Dhaka (Attention: Director General).<br/>
        5. Companies Circle-222, Taxes Zone-11,9,Segunbagicha, Dhaka (Attention: Deputy Comissioner of Taxes).
    </div>
    <div class="form-group col-md-3 pull-right">
        Yours Faithfully,<br/>
        ( Md. Atikul Sarker )<br/>
        Assistant Director (Commercial)<br/>
        Phone : 01712411749<br/>
        Email : Siraj@batworld.com<br/>
    </div>
</div>

