@extends('layouts.pdfGen')
@section('content')
@include('partials.messages')
<section class="content" id="workPermitSection">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;margin-bottom:10px;">
                        <img src="assets/images/logo_beza_single.png"/><br/>
                        BEZA::Bangladesh Economic Zones Authority<br/>
                        Application for Work Permit
                    </div>
                </div>
                <div class="panel panel-red" id="inputForm">
                    <div class="panel-heading">Application for Work Permit</div>
                    <div class="panel-body">
                        <section class="content-header">
                            <table width="100%">
                                <tr>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Tracking no. : </strong><span style="font-size:10px;">{{ $process_data->track_no  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;"> Date of Submission: </strong><span style="font-size:10px;"> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }}</span></td>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Current Status : </strong><span style="font-size:10px;">
                                            @if($form_data->status_id == -1) Draft
                                            @else {!! $statusArray[$form_data->status_id] !!}
                                            @endif</span>
                                    </td>
                                    <td style="padding:5px;"><strong style="font-size:10px;">Current Desk :</strong><span style="font-size:10px;">
                                            @if($process_data->desk_id != 0)
                                                {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                            @else
                                                Applicant
                                            @endif
                                        </span></td>
                                </tr>

                                <tr>
                                    <td style="padding:5px; font-size:10px">
                                        <?php if ($form_data->status_id == 23 && !empty($form_data->certificate)) { ?>
                                        <a href="{{ url($form_data->certificate) }}"
                                           title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                        <?php } ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </section>
                        <fieldset style="display: block;">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Basic Information</strong></div>
                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td width="75%" style="font-size: 10px; padding: 5px;"><strong>Please provide Visa Recommendation Reference Number</strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($vrData[$alreadyExistApplicant->visa_recommend_no]) ? $vrData[$alreadyExistApplicant->visa_recommend_no] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="75%" style="font-size: 10px; padding: 5px;"><strong>Type of Visa obtained for the Foreign Nationals</strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($passport_types[$alreadyExistApplicant->passport_types]) ?
                                                    $passport_types[$alreadyExistApplicant->passport_types] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="75%" style="font-size: 10px; padding: 5px;"><strong>Work Permit Type</strong></td>
                                            <td width="25%" style="font-size: 10px; padding: 5px;"><span>{{ (!empty($alreadyExistApplicant->work_permit_types) ?
                                                    $alreadyExistApplicant->work_permit_types : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div><!--panel-->


                            <div class="panel panel-primary">
                                <div class="panel-heading"> <strong>Particulars of Sponsors / Employers</strong></div>
                                <div class="panle-body">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>1. Name of the Applicant / Applying Firm or Company</strong></td>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>Name of Economic Zone</strong></td>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($economicZone->zone) ? $economicZone->zone : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="padding: 5px; font-size: 10px;"><strong>Full Address of Registered Head Office of Applicant / Applying Firm or Company</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($countries[$alreadyExistApplicant->country]) ? $countries[$alreadyExistApplicant->country] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Division</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($divition_eng[$alreadyExistApplicant->division]) ? $divition_eng[$alreadyExistApplicant->division] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>District</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($district_eng[$alreadyExistApplicant->district]) ? $district_eng[$alreadyExistApplicant->district] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 1</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 2</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Post Code</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Phone No</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Fax No</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Email</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Website</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <strong style="display: inline-block;width:60%;">Authorized Information</strong>
                                    <span class="text-right" id="full_same_as_authorized" style="width: 38%; display: none;">
                                        <span style="background-color:#ddd;width:100%;display:block;margin:2px;padding:6px;display:block">Yes</span>
                                        <label for="same_as_authorized" class="text-left" style="font-size:13px;">Same as Authorized Person</label>
                                    </span>
                                </div>
                                <div class="panel-body">

                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>3. Name of the Correspondent Applicant Name</strong></td>
                                            <?php $authCorrespondent_name = (!empty($logged_user_info->user_full_name) ? $logged_user_info->user_full_name : ''); ?>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_name) ? $alreadyExistApplicant->correspondent_name : $authCorrespondent_name) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Nationality</strong></td>
                                            <?php $authNationality = (!empty($nationality[$logged_user_info->nationality]) ? $nationality[$logged_user_info->nationality] : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($nationality[$alreadyExistApplicant->correspondent_nationality]) ? $nationality[$alreadyExistApplicant->correspondent_nationality] : $authNationality) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Passport</strong></td>
                                            <?php $authPassport_no = (!empty($logged_user_info->passport_no) ? $logged_user_info->passport_no : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_passport) ?
                                                            $alreadyExistApplicant->correspondent_passport : $authPassport_no) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="padding: 5px; font-size: 10px;"><strong>4. Correspondent Address & Contact Details</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country</strong></td>
                                            <?php $authCountry = (!empty($countries[$logged_user_info->country]) ? $countries[$logged_user_info->country] : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($countries[$alreadyExistApplicant->correspondent_country]) ? $countries[$alreadyExistApplicant->correspondent_country] : $authCountry) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>State</strong></td>
                                            <?php $authState = (!empty($logged_user_info->state) ? $logged_user_info->state : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_state) ? $alreadyExistApplicant->correspondent_state : $authState) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Province</strong></td>
                                            <?php $authProvince = (!empty($logged_user_info->province) ? $logged_user_info->province : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_province) ? $alreadyExistApplicant->correspondent_province : $authProvince) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 1</strong></td>
                                            <?php $authRoad_no = (!empty($logged_user_info->road_no) ? $logged_user_info->road_no : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_road_no) ? $alreadyExistApplicant->correspondent_road_no : $authRoad_no) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 2</strong></td>
                                            <?php $authHouse_no = (!empty($logged_user_info->house_no) ? $logged_user_info->house_no : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_house_no) ?
                                                        $alreadyExistApplicant->correspondent_house_no : $authHouse_no) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Post Code</strong></td>
                                            <?php $authPost_code = (!empty($logged_user_info->post_code) ? $logged_user_info->post_code : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_post_code) ?
                                                        $alreadyExistApplicant->correspondent_post_code : $authPost_code) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Phone No</strong></td>
                                            <?php $authUser_phone = (!empty($logged_user_info->user_phone) ? $logged_user_info->user_phone : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_phone) ? $alreadyExistApplicant->correspondent_phone : $authUser_phone) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Fax No</strong></td>
                                            <?php $authUser_fax = (!empty($logged_user_info->user_fax) ? $logged_user_info->user_fax : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_fax) ? $alreadyExistApplicant->correspondent_fax : $authUser_fax) }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Email</strong></td>
                                            <?php $authUser_email = (!empty($logged_user_info->user_email) ? $logged_user_info->user_email : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_email) ? $alreadyExistApplicant->correspondent_email : $authUser_email) }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Website</strong></td>
                                            <?php $authWebsite = (!empty($logged_user_info->website) ? $logged_user_info->website : 'N/A'); ?>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->correspondent_website) ?
                                                        $alreadyExistApplicant->correspondent_website : $authWebsite) }}</span></td>
                                        </tr>
                                    </table>

                                </div> <!-- / panel-body -->
                            </div> <!-- / panel -->

                        </fieldset>


                        <fieldset style="display: block;">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Particulars of Foreign Incumbent </strong></div>
                                <div class="panel-body">

                                    <table width="100%">
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Name of the foreign national</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_national_name) ?
                                                    $alreadyExistApplicant->incumbent_national_name : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Nationality</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($nationality[$alreadyExistApplicant->incumbent_nationality]) ?
                                                    $nationality[$alreadyExistApplicant->incumbent_nationality] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Gender</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_gender)?$alreadyExistApplicant->incumbent_gender:'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Passport No</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_passport) ? $alreadyExistApplicant->incumbent_passport : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Expiry Date</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_expire) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_expire, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Place of Issue</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_issue_place) ?
                                                    $alreadyExistApplicant->incumbent_pass_issue_place : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Date of Issue</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_pass_issue_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_pass_issue_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="padding: 5px; font-size: 10px;"><strong>Permanent Address</strong></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Country</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($countries[$alreadyExistApplicant->incumbent_country]) ?
                                                    $countries[$alreadyExistApplicant->incumbent_country] : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>State</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_state) ? $alreadyExistApplicant->incumbent_state : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Province</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_province) ? $alreadyExistApplicant->incumbent_province : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 1</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_road_no) ? $alreadyExistApplicant->incumbent_road_no : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Address Line 2</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_house_no) ? $alreadyExistApplicant->incumbent_house_no : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Post Code</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_post_code) ? $alreadyExistApplicant->incumbent_post_code : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Phone No</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_phone) ? $alreadyExistApplicant->incumbent_phone : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Fax No</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_fax) ? $alreadyExistApplicant->incumbent_fax : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Email</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_email) ? $alreadyExistApplicant->incumbent_email : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Date of Birth</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_dob) ?
                                                         App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->incumbent_dob, 0, 10)) : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Marital Status</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_martial_status)?$alreadyExistApplicant->incumbent_martial_status:'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table width="100%" class="table-bordered">
                                                    <tr>
                                                        <td colspan="4" style="padding: 5px; font-size: 10px;"><strong>Academic Qualification (please attach certificates)</strong></td>
                                                    </tr>
                                                    <tr class="alert-success">
                                                        <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Highest Degree</strong></td>
                                                        <td width="35%" style="padding: 5px; font-size: 10px;"><strong>College / University</strong></td>
                                                        <td width="15%" style="padding: 5px; font-size: 10px;"><strong>Result</strong></td>
                                                        <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Certificate (upload max file size 3 MB)</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_degree) ? $alreadyExistApplicant->incumbent_degree : 'N/A') }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_institute) ? $alreadyExistApplicant->incumbent_institute : 'N/A') }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->incumbent_result) ? $alreadyExistApplicant->incumbent_result : 'N/A') }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;">
                                                            <span>
                                                                <a href="{{URL::to('/'.(!empty($alreadyExistApplicant->incumbent_certificate)) ? $alreadyExistApplicant->incumbent_certificate : '')}}" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="{{ $alreadyExistApplicant->incumbent_certificate }}" /> <?php if(!empty($alreadyExistApplicant->incumbent_certificate)){ $file_name = explode('/',$alreadyExistApplicant->incumbent_certificate); echo end($file_name);} ?></a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                </div><!-- /panel-body-->
                            </div><!-- /panel-->


                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Employment Information </strong></div>
                                <div class="panel-body">
                                    <table width="100%">
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Name of the post employed for (Designation)</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->post_name) ? $alreadyExistApplicant->post_name : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Date of arrival in Bangladesh</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->arrival_in_bd) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->arrival_in_bd, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Period of Employment</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong></strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span></span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Desired effective date</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->employ_start_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->employ_start_date, 0, 10)) : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>End Date</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->employ_end_date) ?
                                                    App\Libraries\CommonFunction::changeDateFormat(substr($alreadyExistApplicant->employ_end_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Desired duration</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->employ_duration) ? $alreadyExistApplicant->employ_duration : 'N/A') }}</span></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><strong>Brief job description</strong></td>
                                            <td width="25%" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->employ_job_desc) ? $alreadyExistApplicant->employ_job_desc : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>Whether the post has been advertised in Bangladesh (please attach copy of online advertisement) :</strong></td>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;">
                                                <span>
                                                    <a href="{{URL::to('/'.(!empty($alreadyExistApplicant->employ_job_ad)) ? $alreadyExistApplicant->employ_job_ad : '')}}" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="{{ $alreadyExistApplicant->employ_job_ad }}" /> <?php if(!empty($alreadyExistApplicant->employ_job_ad)){ $file_name = explode('/',$alreadyExistApplicant->employ_job_ad); echo end($file_name);} ?></a>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>Justification for employment of foreign national</strong></td>
                                            <td colspan="2" style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->employ_justification) ? $alreadyExistApplicant->employ_justification : 'N/A') }}</span></td>
                                        </tr>
                                    </table>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong>Compensation and Benefit </strong></div>
                                <div class="panel-body">
                                    <table width="100%" class="table-bordered">
                                        <tr class="alert alert-warning">
                                            <td style="padding: 5px; font-size: 10px;"><strong>Salary Structure</strong></td>
                                            <td style="padding: 5px; font-size: 10px;" colspan="3"><strong>Payable Locally</strong></td>
                                            <td style="padding: 5px; font-size: 10px;" colspan="2"><strong>Payable Abroad</strong></td>
                                        </tr>
                                        <tr class="alert alert-warning">
                                            <td style="padding: 5px; font-size: 10px;"><span></span></td>
                                            <td style="padding: 5px; font-size: 10px;"><strong>Payment</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><strong>Amount</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><strong>Currency</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><strong>Amount</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><strong>Currency</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>a. Basic Salary / Honorarium</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->salary_pay_method) ?
                                                                $alreadyExistApplicant->salary_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->salary_local_amount) ?
                                                                $alreadyExistApplicant->salary_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->salary_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->salary_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->salary_foreign_amount) ?
                                                                $alreadyExistApplicant->salary_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->salary_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->salary_foreign_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>b. Overseas Allowance</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->oversea_pay_method) ?
                                                                $alreadyExistApplicant->oversea_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->oversea_local_amount) ?
                                                                $alreadyExistApplicant->oversea_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->oversea_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->oversea_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->oversea_foreign_amount) ?
                                                                $alreadyExistApplicant->oversea_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->oversea_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->oversea_foreign_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>c. House Rent</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->house_pay_method) ?
                                                                $alreadyExistApplicant->house_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->house_local_amount) ?
                                                                $alreadyExistApplicant->house_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->house_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->house_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->house_foreign_amount) ?
                                                                $alreadyExistApplicant->house_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->house_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->house_foreign_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>d. Conveyance</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->conveyance_pay_method) ?
                                                                $alreadyExistApplicant->conveyance_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->conveyance_local_amount) ?
                                                                $alreadyExistApplicant->conveyance_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->conveyance_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->conveyance_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->conveyance_foreign_amount) ?
                                                                $alreadyExistApplicant->conveyance_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->conveyance_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->conveyance_foreign_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>e. Medical Allowance</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->medical_pay_method) ?
                                                                $alreadyExistApplicant->medical_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->medical_local_amount) ?
                                                                $alreadyExistApplicant->medical_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->medical_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->medical_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->medical_foreign_amount) ?
                                                                $alreadyExistApplicant->medical_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->medical_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->medical_foreign_currency] : 'N/As') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>f. Entertainment Allowance</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->entertain_pay_method) ?
                                                                $alreadyExistApplicant->entertain_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->entertain_local_amount) ?
                                                                $alreadyExistApplicant->entertain_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->entertain_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->entertain_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->entertain_foreign_amount) ?
                                                                $alreadyExistApplicant->entertain_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->entertain_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->entertain_foreign_currency] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>g. Annual Bonus</strong></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->bonus_pay_method) ?
                                                                $alreadyExistApplicant->bonus_pay_method : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->bonus_local_amount) ?
                                                                $alreadyExistApplicant->bonus_local_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->bonus_local_currency]) ?
                                                                $currency[$alreadyExistApplicant->bonus_local_currency] : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->bonus_foreign_amount) ?
                                                                $alreadyExistApplicant->bonus_foreign_amount : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($currency[$alreadyExistApplicant->bonus_foreign_currency]) ?
                                                                $currency[$alreadyExistApplicant->bonus_foreign_currency] : '') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>h. Other fringe benefits (if any)</strong></td>
                                            <td style="padding: 5px; font-size: 10px;" colspan="3"><span>{{ (!empty($alreadyExistApplicant->fringe_benefits) ? $alreadyExistApplicant->fringe_benefits : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;" colspan="2"><span>Maximum 120 characters</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px; font-size: 10px;"><strong>i. Any particular comments or remarks</strong></td>
                                            <td style="padding: 5px; font-size: 10px;" colspan="3"><span>{{ (!empty($alreadyExistApplicant->salary_remarks) ? $alreadyExistApplicant->salary_remarks : 'N/A') }}</span></td>
                                            <td style="padding: 5px; font-size: 10px;" colspan="2"><span>Maximum 120 characters</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <table width="100%">
                                                    <tr class="alert alert-info">
                                                        <td colspan="9" style="padding: 5px; font-size: 10px;"><strong>Manpower of the office</strong></td>
                                                    </tr>
                                                    <tr class="alert alert-success">
                                                        <td colspan="3" style="padding: 5px; font-size: 10px;"><strong>Local (a)</strong></td>
                                                        <td colspan="3" style="padding: 5px; font-size: 10px;"><strong>Foreign (b)</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Grand Total</strong></td>
                                                        <td colspan="2" style="padding: 5px; font-size: 10px;"><strong>Ratio</strong></td>
                                                    </tr>
                                                    <tr class="alert alert-success">
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Executive</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Supporting Stuff</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Total</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Executive</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Supporting Stuff</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Total</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>(a+b)</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Local</strong></td>
                                                        <td style="padding: 5px; font-size: 10px;"><strong>Foreign</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->local_executive) ? $alreadyExistApplicant->local_executive : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->local_stuff) ? $alreadyExistApplicant->local_stuff : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->local_total_no) ? $alreadyExistApplicant->local_total_no : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->foreign_executive) ? $alreadyExistApplicant->foreign_executive : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->foreign_stuff) ? $alreadyExistApplicant->foreign_stuff : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->foreign_total) ? $alreadyExistApplicant->foreign_total : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->mp_total) ? $alreadyExistApplicant->mp_total : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->mp_ratio_local) ? $alreadyExistApplicant->mp_ratio_local : 0) }}</span></td>
                                                        <td style="padding: 5px; font-size: 10px;"><span>{{ (!empty($alreadyExistApplicant->mp_ratio_foreign) ? $alreadyExistApplicant->mp_ratio_foreign : 0) }}</span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div><!-- /panel-body-->
                            </div><!-- /panel-->

                        </fieldset>


                        <fieldset style="display: block;">
                            <div class="col-md-12" style="padding: 0px;">
                                <div class="form-group">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Required Documents for attachment</div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <table width="100%" class="table-bordered">
                                                <tr>
                                                    <td width="10%" style="padding: 5px; font-size: 10px;"><strong>No.</strong></td>
                                                    <td width="50%" style="padding: 5px; font-size: 10px;"><strong>Required Attachment</strong></td>
                                                    <td width="40%" style="padding: 5px; font-size: 10px;"><strong>Attached PDF file</strong></td>
                                                </tr>
                                                <?php $i=1; ?>
                                                @foreach($document as $row)
                                                <tr>
                                                    <td style="padding:5px;" width="10%"><span style="font-size:10px;">{{ $i }} <?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></span></td>
                                                    <td style="padding:5px;" width="45%"><span style="font-size:10px;">{{$row->doc_name }}</span></td>
                                                    <td style="padding:5px;" width="45%">
                                                        <span style="font-size:10px;">
                                                            @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                                <a href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="{{ $clrDocuments[$row->doc_id]['file'] }}" /> <?php if(!empty($clrDocuments[$row->doc_id]['file'])){ $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name);} ?></a>
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </table>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                            </div>
                            <div class="form-group" style="clear: both">

                            </div>
                        </fieldset>


                        <fieldset style="display: block;">

                        </fieldset>

                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
@endsection