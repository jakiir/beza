<table border="0">
    <tr>
        <td>
            <p>File no : {{ $track_no }}</p>
        </td>
        <td style="text-align: right;">
            <p>Date : {{ $dateNow }}</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>
                Managing Director/CEO
                <br/>{{ $proposed_name }}
                <br/>Plot no…………..
                <br/>{{ $economicZones->name }}
                <br/>{{ $economicZones->upazilla }}, {{ $economicZones->area }}.
                <br/>{{ $economicZones->district }}
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>Sub: <strong>Approval for setting up a Garments Accessories manufacturing industry in Meghna Economic Zone</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>Dear MD/CEO,<br/></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;&nbsp;With reference to your Project Proposal received on  31.08.2016 this is to inform you that the Authority
                is pleased to approve your project for setting up a Garments Accessories manufacturing industry in Meghna Economic
                Zone on terms and conditions as indicated below:-</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>01. Name of the project</p>
        </td>
        <td>
            <p>:  {{ $proposed_name }}.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>02. Products</p>
        </td>
        <td>
            <p>:  {{ $product_name }}</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>03. Cost of the project</p>
        </td>
        <td>
            <p>:  {{ $production_cost }}</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>04. Type of investment</p>
        </td>
        <td>
            <p>:  “A” (100% Foreign- China)</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>05. Type of Industry</p>
        </td>
        <td>
            <p>:  {{ $business_type }}</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>06. Annual production capacity</p>
        </td>
        <td>
            <p>:  660,000,000 Units</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>07. Employment</p>
        </td>
        <td>
            <p>:  500 persons including 10 foreign nationals  </p>
        </td>
    </tr>
    <tr>
        <td>
            <p>08. Status of the company</p>
        </td>
        <td>
            <p>:  {{ $organization_type }}</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>09. Manufacturing Process</p>
        </td>
        <td>
            <p>:  a) Zipper:  Assembling – Electroplating/ Painting – Dyeing – Finishing – Packing;<br/>
                &nbsp;&nbsp;b) Button: Moulding – Metal Shaping <br/>&nbsp;&nbsp;- Electroplating – Spray Paint – Assembling – &nbsp;&nbsp;Packing.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p>10.  Area of land/SFB to be allotted</p>
        </td>
        <td>
            <p>: {{ $economicZones->name }}
                <br/>{{ $economicZones->upazilla }}, {{ $economicZones->area }}.
                <br/>{{ $economicZones->district }}.</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>10. Machineries and equipments required to be imported for the project valued approximately at US$ 8,000,000.00 only on
                the terms and conditions acceptable to this Authority. </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>11. Machineries to be imported for the project (as per list enclosed with the Project Proposal) shall be modern and
                brand new. Prior approval of the Authority shall be required for the import/shipment of machinery from abroad.</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>12. Before procurement of machineries and building-materials from abroad you are required to submit 3 (three) sets
                of price quotations from reputed machinery suppliers or in case of procurement of machinery from manufacturer, 1 (one)
                set of price quotation along with catalogues etc. of the machinery for approval of the Authority.</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>13. The company will have to:<br/>&nbsp;&nbsp;&nbsp;
                a.	maintain the ratio of export and domestic sale within 50:50 of  entire product of its factory;<br/>&nbsp;&nbsp;&nbsp;
                b.	submit monthly reports to the Authority on the progress of implementation of the project;<br/>&nbsp;&nbsp;&nbsp;
                c.	commission the project in the allotted land space within a period of twelve months failing  which the Authority may revoke this permission;<br/>&nbsp;&nbsp;&nbsp;
                d.	furnish such other data on the project to this Authority and to any other agency as may be required;<br/>&nbsp;&nbsp;&nbsp;
                e.	obtain work permits / permission of the Authority prior to entry for any foreign nationals on employment in the project.<br/>&nbsp;&nbsp;&nbsp;
                f.	submit to this Authority, the audited Financial Statement for every financial  year within 4(four)  months from the closure  of each financial year of the company;<br/>&nbsp;&nbsp;&nbsp;
                g.	comply with the provisions of minimum wages of the workers as declared by the government from time to time and the EPZ Workers Welfare Association and Industrial Relations Act, 2010 (Act no-43 of 2010) (as amended from time to time by the  Authority) in regards to wages, employment, salary, leave, discipline, health, compensation, insurance and other benefits to the employees engaged for work in your enterprise;<br/>&nbsp;&nbsp;&nbsp;
                h.	comply with the rules pertaining to environment protection, pollution control and effluent  treatment and take necessary safety measures against possible fire hazards;<br/>&nbsp;&nbsp;&nbsp;
                i.	comply with all laws, by-laws, rules, regulations, directives of the government and of  this  Authority  which are in force or which may be issued from time to time in future;<br/>&nbsp;&nbsp;&nbsp;
                j.	comply with provisions of the “Principles and Procedures Governing Setting  up of Industries in EZ.”<br/>&nbsp;&nbsp;&nbsp;
                k.	obtain prior approval of the Authority in case the company decides to appoint a Managing  Agent  or transfer the shares of the company;<br/>&nbsp;&nbsp;&nbsp;
                l.	the company shall have to submit actual Investment, export, domestic sales, employment information to the Authority and/or with other government agency(s) in quarterly basis.<br/>&nbsp;&nbsp;&nbsp;
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>14. The construction of the factory building and other civil construction shall have to commence immediately and not later
                than 03 (three) months from the date of issuance of this letter and will have to start commercial production within 01(one) year
                from the date of signing the land lease agreement. If the company fails to start construction work of factory building within
                03(three) months, the land lease agreement may be terminated without issuance of any further notice. The building shall
                conform to the conditions and specification laid down in the Authority’s Building Construction Rules, 2016.
                Prior approval of the building plan has to be obtained from the Authority.</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>15. The company shall bear the cost of Services and Regulatory permit fees as prescribed by the Authority from time to time</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>16. No waste/old materials will be allowed to import as raw materials.</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>17. In the event of your failure to set up the unit within the stipulated time or infringement of any rules and regulations or
                violation of any of the above terms and conditions, the Authority may revoke the permission. </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>18.  This permission is not transferable.</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>19.  If you agree with the terms and conditions contained in this letter along with those contained in the BEZ Act, Rules,
                Policies, Guidelines, SROs, Circulars, Office orders etc. you are requested to proceed for signing the land lease agreement
                with the Developer/ Operator of Meghna Economic Zone with intimation to the Authority.
                Thanking and assuring you of our best co-operation at all times.
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="text-align:right;">Yours faithfully,</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p>Copy for Information to-<br/>&nbsp;&nbsp;&nbsp;
                a)	Managing Director, Meghna Economic Zone,…………………………..<br/>&nbsp;&nbsp;&nbsp;
                b)	Commissioner, Customs Bond,…………………………….<br/>&nbsp;&nbsp;&nbsp;
                c)	………………………………….<br/>&nbsp;&nbsp;&nbsp;
                d)	……………………………………<br/>&nbsp;&nbsp;&nbsp;
                e)	…………………………………..</p>
        </td>
    </tr>
</table>