{!! Form::open(['url' => '/apps/process-update', 'method' => 'patch', 'class' => 'form work_permit_form', 'role' => 'form']) !!}

<!-- text input -->
<div class="form-group col-md-3 {{$errors->has('status_id') ? 'has-error' : ''}}">
    {!! Form::label('status_id','Apply Status') !!}
    {!! Form::select('status_id', (['0' => 'Select Below'] + $statusList), null, ['class' => 'form-control']) !!}  
    {!! $errors->first('status_id','<span class="help-block">:message</span>') !!}
</div>

<!-- text input -->
<div class="form-group col-md-3 {{$errors->has('desk_id') ? 'has-error' : ''}}">
    {!! Form::label('desk_id','Send to Authorized Officer') !!}
    {!! Form::text('desk_id','',['class'=>'form-control', 'placeholder'=>'Enter reference no..']) !!}
    {!! $errors->first('desk_id','<span class="help-block">:message</span>') !!}
</div>
<!-- text input -->
<div class="form-group col-md-3 {{$errors->has('remarks') ? 'has-error' : ''}}">
    {!! Form::label('remarks','Remarks') !!}
    {!! Form::text('remarks','',['class'=>'form-control', 'placeholder'=>'Enter remarks here..']) !!}
    {!! $errors->first('remarks','<span class="help-block">:message</span>') !!}
</div>

<div class="form-group col-md-3 ">
    {!! Form::button('<i class="fa fa-save"></i> Process', array('type' => 'submit', 'value'=> 'Process', 'class' => 'btn btn-success  pull-right')) !!}
</div><!-- /.box-footer -->
{!! Form::close() !!}
