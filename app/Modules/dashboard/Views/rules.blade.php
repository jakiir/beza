@extends('layouts.admin')
@section('content')

    <div class="col-sm-12">
        @if (session('success'))
            <div class="alert alert-success">
                <strong><i class="icon-check"></i> {{session('success')}}.</strong>
            </div>
        @endif
        @if(count($duplicate) > 0)
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"> Duplicate URL:</h3>
                </div>
                <div class="panel-body">
                    <div>
                        <table  class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <th>#</th>
                            <th>Duplicate Route</th>
                            <tbody>
                            <?php $sl=0;?>
                            @foreach($duplicate as $value)
                                <tr>
                                    <td>{{++$sl}}</td>
                                    <td>{{$value}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@section('footer-script')
@endsection