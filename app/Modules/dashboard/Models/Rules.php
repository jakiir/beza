<?php namespace App\Modules\Dashboard\Models;

use App\Libraries\ReportHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Rules extends Model {

    protected $table = 'rules';
    protected $fillable = [
        'module_title','method_type','action_method','created_at','updated_at'
    ];

    
    /*********************************End of Models*******************************/
}
