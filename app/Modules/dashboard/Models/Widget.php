<?php namespace App\Modules\Dashboard\Models;

use App\Libraries\ReportHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Widget extends Model {

    protected $table = 'dashboard_object1';
    protected $fillable = [
        'db_obj_title','db_user_id','db_obj_type','db_obj_para1','db_obj_para2','db_obj_status','db_obj_sort','db_user_type'
    ];


    public function getWidget()
    {
        $widgetData = '';
        $data = Widget::where('db_obj_type','widget')
            ->where('db_user_type',Auth::user()->user_type)
            ->orWhere('db_user_type',0)
            ->orWhere('db_user_type','is null')
            ->orderBy('db_user_type','desc')
            ->first();

        if($data) {
            //user_type like {$user_type} and user_sub_type={$user_sub_type}
            $users_type = Auth::user()->user_type;
            $type = explode('x', $users_type);
            $data['user_type'] = "'"."$type[0]x" . substr($type[1], 0, 2) ."_'";
            $data['desk_id'] = Auth::user()->desk_id==''? 0 : Auth::user()->desk_id;
            $data['district'] = Auth::user()->district;

            $reportHelper = new ReportHelper();
            $sql = $reportHelper->ConvParaEx($data->db_obj_para1,$data);
            $widgetData = DB::select(DB::raw($sql));

            try
            {
                $widgetData = DB::select(DB::raw($sql));

            }catch(\Illuminate\Database\QueryException $e) {

                $widgetData = array();
//                echo "Something wrong...";
            }
        }
        return $widgetData;
    }


    public function getDashboardObject()
    {
        $data = Widget::where('db_obj_type','list')->get();
        return $data;
    }
}
