<?php

namespace App\Modules\Dashboard\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\Apps\Models\Apps;
use App\Modules\Apps\Models\Status;
use App\Modules\Company\Models\Company;
use App\Modules\Dashboard\Models\Rules;
use App\Modules\Dashboard\Models\Services;
use App\Modules\Dashboard\Models\Widget;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\Nationality;
use App\Modules\Users\Models\Users;
use App\Modules\workPermit\Models\Processlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use View;

class DashboardController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));
        ACL::db_reconnect();
    }

    public function index(Widget $dashboard) {
        $list = [];
        $log = date('H:i:s', time());
        $dbMode = Session::get('DB_MODE');
        list($type, $code) = explode('x', Auth::user()->user_type);
        /* $widgetsGroup = $dashboard->getWidget(); */
        $log .= ' - ' . date('H:i:s', time());
        $delegate_to_user_id = Auth::user()->delegate_to_user_id;
        $info = Users::leftJoin('user_desk as ud', 'ud.desk_id', '=', 'users.desk_id')
                        ->where('id', $delegate_to_user_id)->first(['user_full_name', 'user_email', 'user_phone', 'ud.desk_name']);
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        $division = AreaInfo::where('area_type', 2)->lists('area_nm', 'area_id');
        $areaList = AreaInfo::lists('area_nm', 'area_id');

//        $services = DB::table('process_list')
//            ->select(array('service_info.name', 'service_info.id', 'service_info.url', 'service_info.panel', DB::raw('COUNT(process_list.process_id) as totalApplication')))
//            ->where('service_info.is_active', '=', 1)
//            ->where('process_list.initiated_by', '=', Auth::user()->id)
//            ->leftJoin('service_info', 'service_info.id', '=', 'process_list.service_id')
//            ->groupBy('service_info.id')
//            ->orderBy('service_info.id', 'asc')
//            ->get();
        $services = DB::table('process_list')
                ->leftJoin('service_info', 'service_info.id', '=', 'process_list.service_id')
                ->groupBy('service_info.id')
                ->select(array('service_info.name', 'service_info.id', 'service_info.url', 'service_info.panel', DB::raw('COUNT(process_list.process_id) as totalApplication')))
                ->orderBy('service_info.id', 'asc')
                ->where('process_list.status_id', '!=', -1)
                ->where(function ($query1) {
                    if (Auth::user()->user_type == '5x505' || Auth::user()->user_type == '6x606') {
                        $query1->where('process_list.initiated_by', '=', Auth::user()->id);
                    } elseif (Auth::user()->user_type == '4x404') {
                        $query1->where('process_list.desk_id', '=', Auth::user()->desk_id);
                        if (Auth::user()->desk_id == 1 || Auth::user()->desk_id == 2) {
                            $query1->where('process_list.eco_zone_id', Auth::user()->eco_zone_id);
                        }
                    } else if (Auth::user()->user_type == '9x909') { // 9x909 = customs
                        if (Auth::user()->desk_id == 7) { // 7 = customs officer
                            $query1->where('process_list.desk_id', '=', Auth::user()->desk_id);
                            $query1->where('process_list.eco_zone_id', Auth::user()->eco_zone_id);
                        } else { // for 8 =customs viewer
                            $query1->whereIn('process_list.service_id', array(5,6)); // 5 = EP, 6 =IP
                            $query1->whereIn('process_list.status_id', array(21, 29, 30)); 
                                // 21 =Approved & Sent to custom, 29=Forward to another custom desk, 30 = Custom Verified
                        }
                    } else if (Auth::user()->user_type == '3x303') { // 3x303 = security
                        if (Auth::user()->desk_id == 9) { // 7 = customs officer
                            $query1->where('process_list.desk_id', '=', Auth::user()->desk_id);
                            $query1->where('process_list.eco_zone_id', Auth::user()->eco_zone_id);
                        } else { // for 10 =customs viewer
                            $query1->whereIn('process_list.service_id', array(5,6)); // 5 = EP, 6 =IP
                            $query1->whereIn('process_list.status_id', array(30,31)); //  30 = Custom Verified, 31=Issued Gatepass (Full)
                        }
                    }
                })
                ->get();

        $notice = CommonFunction::getNotice(1);
        $user_type = Auth::user()->user_type;
        $deshboardObject = [];
        if ($user_type == '1x101') {
            $deshboardObject = DB::table('dashboard_object')->where('db_obj_status', 1)->get();
        }

        return view("dashboard::index", compact('deshboardObject', 'list', 'dbMode', 'log', 'info', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'notice', 'services'));
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
//        $nationality = Nationality::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'id');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', 1)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = App\Modules\Apps\Models\Status::orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('dashboard::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function serviceWiseStatus(Request $request) {
        $id = $request->get('service_id');
        $service_wise_status = Status::where('process_id', '=', $id)->get();
        return $service_wise_status;
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $nationality = $request->get('nationality');
        $organization = $request->get('organization');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

//        dd($request->all());

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $nationality, $applicant_name, $status_id);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('dashboard::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();
        $data = ['responseCode' => 1, 'data' => $contents];
        return response()->json($data);
    }

    public function execute() {
        $controllers = [];
        $data = [];
        $duplicate = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            $action = $route->getAction();
            $method = $route->getMethods();

            if (array_key_exists('controller', $action)) {
                // You can also use explode('@', $action['controller']); here
                // to separate the class name from the method
                $controllers[] = $method[0] . "\\" . $action['controller'];
            }
        }
//        dd($controllers);
        $duCount = 0;
        foreach ($controllers as $value) {
            $data = explode("\\", $value);
            $method_type = $data[0];
            if (count($data) == 6) {
                $module_name = $data[3];
                $route_action = $data[5];
            } else {
                $module_name = $data[2];
                $route_action = $data[4];
            }

            $exitst = Rules::where('module_title', '=', $module_name)
                            ->where('method_type', '=', $method_type)
                            ->where('action_method', '=', $route_action)->count();

            if ($exitst == 0) {
                $rules = new Rules;
                $rules->module_title = $module_name;
                $rules->method_type = $method_type;
                $rules->action_method = $route_action;
                $rules->save();
            } else {
                $duplicate[] = $value;
            }
        }
        Session::flash('success', 'URL has been saved successfully!');
        return view('dashboard::rules', compact('duplicate'));
    }

    /*     * ***************************End of Controller Class******************************* */
}
