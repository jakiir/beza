<?php

Route::group(array('module' => 'exportPermit', 'middleware' => ['auth', 'checkAdmin', 'XssProtection'], 'namespace' => 'App\Modules\exportPermit\Controllers'), function() {

    //app form create
    Route::get('export-permit/search', 'exportPermitController@search');
    Route::post('export-permit/search-result', 'exportPermitController@searchResult');
    Route::post('export-permit/service-wise-result', 'exportPermitController@serviceWiseStatus');
    Route::post('export-permit/search-view/{param}', 'exportPermitController@searchView');


    Route::get('export-permit/form', 'exportPermitController@appForm');
    Route::post('export-permit/store-app', "exportPermitController@appStore");
    Route::get('export-permit/edit-form/{id}', 'exportPermitController@appFormEdit');
    Route::get('export-permit/view/{id}', 'exportPermitController@appFormView');

    Route::post('export-permit/save-as-draft', "exportPermitController@saveAsDraft");
    Route::patch('export-permit/update-batch', "exportPermitController@updateBatch");

    Route::any('export-permit/upload-document', 'exportPermitController@uploadDocument');
    Route::get('export-permit/list', 'exportPermitController@listEP');
    Route::get('export-permit/list/{ind}/{zone}', 'exportPermitController@listEP');

    Route::get('export-permit/preview', 'exportPermitController@preview');
    Route::post('export-permit/ajax/{param}', 'exportPermitController@ajaxRequest');

    Route::get('/export-permit/get-hscodes/', 'exportPermitController@getHsList');

    // PDF Generation  
    Route::get('export-permit/viewPDF/{id}', 'exportPermitController@appDownloadPDF');
    Route::get('export-permit/undertaking-gen/{id}/{type}', "ExportPermitController@undertakingGen");
    Route::get('export-permit/cer-gen/{id}', "exportPermitController@exportPermitCer");
    Route::get('export-permit/cer-re-gen/{id}', "exportPermitController@certificate_re_gen");

//Discard Certificate
    Route::get('/export-permit/discard-certificate/{id}', 'exportPermitController@discardCertificate');

// Gate pass generation
    Route::post('export-permit/pass-history/{id}', "exportPermitController@storePassHistory");
//    Route::resource('export-permit', 'exportPermitController');

    /*     * ********************************End of Route group****************************** */
});
