<?php

namespace App\Modules\exportPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class ExportMaterial extends Model {

    protected $table = 'ep_materials';
    protected $fillable = array(
        'id',
        'ip_id',
        'product_description',
        'hs_code',
        'hs_product',
        'mat_quantity',
        'mat_remaining_quantity',
        'mat_quantity_unit',
        'fob_usd',
        'fob_usd_value',
        'is_locked',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *****************************End of Model Class********************************** */
}