<?php

namespace App\Modules\exportPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class ExportLcInfo extends Model {

    protected $table = 'ep_lc_info';
    protected $fillable = array(
        'id',
        'ip_id',
        'tt_no',
        'tt_value',
        'bank_id',
        'bank_type',
        'bank_invoice_date',
        'is_locked',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *****************************End of Model Class********************************** */
}