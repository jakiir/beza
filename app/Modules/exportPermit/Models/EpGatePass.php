<?php

namespace App\Modules\exportPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class EpGatePass extends Model {

    protected $table = 'ep_gatepass';
    protected $primaryKey = 'id';
    protected $fillable = array(
        'id',
        'app_id',
        'ep_material_id',
        'gatepass_no',
        'generated_gatepass',
        'remaining_quantity',
        'passed_quantity',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }


    /*     * *****************************End of Model Class********************************** */
}
