<?php

namespace App\Modules\exportPermit\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExportPermit extends Model {

    protected $table = 'export_permit';
    protected $fillable = array(
        'id',
        'service_id',
        'tracking_number',
        'ep_number',
        'status_id',
        'bill_id',
        'bill_month',
        'payment_status_id',
        'permit_type',
        'carrier_type',
        'eco_zone_id',
        'zone_type',
        'undertaking_no',
        'undertaking_date',
        'invoice_ref_no',
        'invoice_ref_date',
        'developer_name',
        'export_port',
        'cm_select',
        'cm_value',
        'invoice_value',
        'destination_country',
        'destination_port',
        'carrier_name',
        'carrier_passport_no',
        'carrier_pass_validity',
        'destination_zone',
        'consignee_name',
        'consignee_address',
        'remark',
        'sb_gk_verification_status',
        'sb_gk_verification_remarks',
        'nsi_gk_verification_status',
        'nsi_gk_verification_remarks',
        'bank_name',
        'challan_no',
        'challan_amount',
        'challan_branch',
        'challan_date',
        'challan_file',
        'acceptance_of_terms',
        'certificate',
        'undertaking',
        'is_draft',
        'is_locked',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }


    function update_method($app_id, $data) {
        DB::table($this->table)
            ->where('id', $app_id)
            ->update($data);
    }

    /*     * *****************************End of Model Class********************************** */
}
