@extends('layouts.admin')

@section('content')
@include('partials.messages')
<?php
$accessMode = ACL::getAccsessRight('exportPermit');
if (!ACL::isAllowed($accessMode, $mode)) {
    die('You have no access right! Please contact with system admin if you have any query.');
}
if (isset($alreadyExistApplicant)) {
    $form_data = $alreadyExistApplicant;
}
?>

<style>
    .text-title{ font-size: 16px !important}
    .text-sm{ font-size: 9px !important}
    #exportPermitForm label.error {display: none !important; }
    .calender-icon{
        border: medium none; padding-top: 30px ! important;
    }
    .input-sm {
        padding: 3px 6px;
    }
    .form-group{
        margin-bottom: 2px;
    }
    @media screen and (max-width: 767px) {
        #dropdown-responsive {
            padding-left: 30px;
            width: 5%;
        }
    }
</style>

<section class="content" id="projectClearanceForm">
    <div class="col-md-12">
        <?php if ($viewMode != 'on') { ?>
            {!! Form::open(array('url' => 'export-permit/store-app','method' => 'post','id' => 'exportPermitForm','role'=>'form')) !!}
            <input type ="hidden" name="app_id" value="{{(!empty($form_data->id) ? Encryption::encodeId($form_data->id) : '')}}">
            <input type="hidden" name="selected_file" id="selected_file" />
            <input type="hidden" name="validateFieldName" id="validateFieldName" />
            <input type="hidden" name="isRequired" id="isRequired" />
        <?php } ?>
        <div class="box">
            <div class="box-body">
                @if($errors->all())
                        <div class="alert alert-danger"><ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul></div>
                @endif

                @if($viewMode == 'on')
                @if($form_data->status_id != 8)
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:6px;">
                        <a href="/export-permit/viewPDF/{{ Encryption::encodeId($form_data->id)}}" target="_blank"
                           class="btn btn-danger btn-sm pull-right">
                            <i class="fa fa-download"></i> <strong>Application Download as PDF</strong>
                        </a>
                    </div>
                </div>
                @endif

                            <!--3 = RD1, 4 = RD2, 5 = RD3, 6 = RD4, 7 = Customs Officer, 9 = security-->
                    @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(3,4,5,6,7)))
                        {!! Form::open(['url' => '/export-permit/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from',
                        'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                                @include('exportPermit::batch-process')
                                <input type="hidden" name="application[]" id="curr_app_id" value="{{$form_data->id}}">
                                <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                        {!! Form::close() !!}
               @endif {{-- checking desk id 3,4,5,6,7,9 --}}

                            @if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(9)))
                                            @if($remaining_quntities)
                                              <?php $rem=0; ?>
                                                @foreach($remaining_quntities as $remaining_quntity)
                                                    @if($remaining_quntity->mat_remaining_quantity > 0)
                                                        <?php $rem++; ?>
                                                    @endif
                                                @endforeach
                                            @endif

                                            @if($rem == 0)
                                            {!! Form::open(['url' => '/export-permit/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from',
                                            'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                                            @include('exportPermit::batch-process')
                                            <input type="hidden" name="application[]" id="curr_app_id" value="{{$alreadyExistApplicant->id}}">
                                            <input type="hidden" id="curr_status_id" value="{{$process_data->status_id}}">
                                            {!! Form::close() !!}
                                            @endif

                                @endif {{-- checking desk id 9 --}}

                      {{-- desk 9 is security --}} {{-- status 30 is custom verified, 31 is gate pass partial issue --}}
                        @if(in_array(Auth::user()->desk_id,array(9)) && ($alreadyExistApplicant->status_id == 30 || $alreadyExistApplicant->status_id == 31) )
                              @if($rem != 0)
                                    @include('exportPermit::gatepass')
                                @endif
                        @endif {{-- checking security desk --}}

                        @if(count($gpHistory) > 0)
                            @if( in_array(Auth::user()->desk_id,array(3,4,5,6,7,9,10)) || $alreadyExistApplicant->created_by == Auth::user()->id )
                                        @include('exportPermit::gatepass-history')
                            @endif {{-- checking desk id 3,4,5,6,7,9--}}
                        @endif {{-- checking gp history --}}

                @endif {{-- checking view mode on --}}


                <div class="panel panel-red"  id="inputForm">
                    <div class="panel-heading">Application for Export Permit </div>
                    <div class="panel-body" style="margin:6px;">

                        <?php if ($viewMode == 'on') { ?>
                            <section class="content-header">
                                <ol class="breadcrumb">
                                    <li><strong>Tracking no. : </strong>{{ $process_data->track_no  }}</li>
                                    <li>
                                        <strong>Date of Submission: </strong>
                                            <?php
                                                $submit_date =   !empty($submissionDate) ? \App\Libraries\CommonFunction::formateDate($submissionDate) :
                                                                        \App\Libraries\CommonFunction::formateDate($process_data->created_at) ; ?>
                                        {{ $submit_date  }}
                                    </li>
                                    <li><strong>Current Status : </strong>
                                        @if(!empty($form_data) && $form_data->status_id == '-1') Draft
                                        @else {!! $statusArray[$form_data->status_id] !!}
                                        @endif
                                    </li>
                                    <li>
                                        @if($process_data->desk_id != 0)
                                        <strong>Current Desk :</strong> {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}
                                        @else
                                        <strong>Current Desk :</strong> Applicant
                                        @endif {{-- process desk id is 0 --}}
                                    </li>
                                    @if($form_data->status_id == 8)
                                    <li>
                                        <strong>Discard Reason :</strong> {{ (!empty($form_data->remarks)?$form_data->remarks:'N/A') }}
                                    </li>
                                    @endif
                                    <li>
                                           <!-- 21 = approved and sent to custom, 28 > customs and security related status--->
                                        <?php if (!empty($form_data) && (in_array($form_data->status_id,[21,28,29,30])) &&
                                                !empty($form_data->certificate)) { ?>
                                            <a class="btn btn-xs show-in-view btn-info" href="{{ url($form_data->certificate) }}" target="_blank">
                                                <i class="fa  fa-file-pdf-o"></i><b> Download Certificate</b></a>
                                            @if(Auth::user()->user_type == '1x101')
                                            <a onclick="return confirm('Are you sure ?')" class="btn btn-xs show-in-view btn-danger"
                                               href="/export-permit/discard-certificate/{{Encryption::encodeId($form_data->id)}}">
                                                <i class="fa  fa-trash"></i><b> Discard Certificate</b></a>
                                            @endif
                                            <?php } ?>
                                         <?php if (!empty($form_data) && (in_array($form_data->status_id,[21])) && !empty($form_data->certificate)) { ?>
                                           <a href="/export-permit/cer-re-gen/{{ Encryption::encodeId($form_data->id)}}" class="btn show-in-view btn-xs btn-warning"
                                           title="Download Certificate" target="_self"> <i class="fa  fa-file-pdf-o"></i> <b>Re-generate certificate</b></a>
                                        <?php } ?>
                                    </li>
                                </ol>
                            </section>

                        <?php
                        }
                        $allRequestVal = old();
                        ?>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>1. General Information</strong></div>
                            <div class="panel-body">

                                <div class="col-md-12">
                                    @if ($viewMode == 'on' && !empty($alreadyExistApplicant->applicant_name) && $alreadyExistApplicant->applicant_name != '')
                                    <div class="form-group " style="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::label('applicant_name',' Applicant Name:' , ['class'=>'col-md-5 text-left required-star']) !!}
                                            <div class="col-md-7">
                                                <span style="background-color:#ddd;width:100%;padding:6px;display:block;display:block">{{(!empty($alreadyExistApplicant->applicant_name)?$alreadyExistApplicant->applicant_name:'N/A')}}</span>
                                            </div>
                                        </div>
                                        </div>

                                    </div>
                                    <br/>
                                    @endif
                                    <div class="form-group " style="">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('permit_type','Export Permit Type :' , ['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('permit_type') ? 'has-error': ''}}">
                                                    {!! Form::select('permit_type', $ip_permit_type, (!empty($form_data->permit_type) ? $form_data->permit_type : ''),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('carrier_type','Type of Carrier :',['class'=>'col-md-5 text-left']) !!}
                                                <div class="col-md-7 {{$errors->has('carrier_type') ? 'has-error': ''}}">
                                                    {!! Form::select('carrier_type', $carrier_type, (!empty($form_data->carrier_type) ? $form_data->carrier_type : ''),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('zone_type','Economic Zone :', ['class'=>'col-md-5 text-left required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('zone_type') ? 'has-error': ''}}">
                                                            {{ (!empty($economicZone->zone)?$economicZone->zone : 'N/A')}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('developer_name','Name of Developer :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('developer_name') ? 'has-error': ''}}">
                                                    {!! Form::text('developer_name',  (!empty($form_data->developer_name) ? $form_data->developer_name : ''),
                                                    ['class' => 'form-control textOnly input-sm required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('undertaking_no','Undertaking No. :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('undertaking_no') ? 'has-error': ''}}">
                                                    {!! Form::text('undertaking_no',  (!empty($form_data->undertaking_no) ? $form_data->undertaking_no : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('undertaking_date','Undertaking Date :',['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('undertaking_date') ? 'has-error': ''}}">
                                                    <div class="datepicker input-group date">
                                                        {!! Form::text('undertaking_date', (!empty($form_data->undertaking_date) && $form_data->undertaking_date != '0000-00-00' ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($form_data->undertaking_date, 0, 10)) : ''),
                                                        ['class' => 'form-control  input-sm required', 'placeholder' => 'dd-mm-yyyy']) !!}
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('invoice_ref_no','Invoice/Vendor Reference No. :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('invoice_ref_no') ? 'has-error': ''}}">
                                                    {!! Form::text('invoice_ref_no',  (!empty($form_data->invoice_ref_no) ? $form_data->invoice_ref_no : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('invoice_ref_date','Invoice/Vendor Reference Date :',['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('invoice_ref_date') ? 'has-error': ''}}">
                                                    <div class="datepicker input-group date">
                                                        {!! Form::text('invoice_ref_date', (!empty($form_data->invoice_ref_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($form_data->invoice_ref_date, 0, 10)) : ''),
                                                        ['class' => 'form-control  input-sm required', 'placeholder' => 'dd-mm-yyyy']) !!}
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('cm_value','CM Value :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('cm_value') ? 'has-error': ''}}">
                                                    {!! Form::select('cm_select', $currencies,  (!empty($form_data->cm_select) ? $form_data->cm_select : '107'),
                                                    ['class' => 'col-md-5 col-xs-12 col-sm-12 input-sm required', 'placeholder' => ' Select One']) !!}
                                                    {!! Form::text('cm_value',  (!empty($form_data->cm_value) ? $form_data->cm_value : ''),
                                                    ['class' => 'col-md-6 col-md-offset-1 col-xs-12 col-sm-12 input-sm required onlyNumber']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('invoice_select','Invoice Value (FoB / CIF / CFR / C&F) :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('invoice_select') ? 'has-error': ''}}">
                                                    {!! Form::select('invoice_select', $currencies,  (!empty($form_data->invoice_select) ? $form_data->invoice_select : '107'),
                                                    ['class' => 'col-md-5 col-xs-12 col-sm-12 input-sm required', 'placeholder' => ' Select One']) !!}
                                                    {!! Form::text('invoice_value',  (!empty($form_data->invoice_value) ? $form_data->invoice_value : ''),
                                                    ['class' => 'col-md-6 col-md-offset-1 col-xs-12 col-sm-12 input-sm onlyNumber required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('destination_port','Port of destination :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('destination_port') ? 'has-error': ''}}">
                                                    {!! Form::select('destination_port', $ports, (!empty($form_data->destination_port) ? $form_data->destination_port : ''),
                                                    ['class' => 'form-control input-sm required', 'placeholder' => 'Select One']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('export_port','Place of load :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('export_port') ? 'has-error': ''}}">
                                                    {!! Form::text('export_port',  (!empty($form_data->export_port) ? $form_data->export_port : ''),
                                                    ['class' => 'form-control input-sm required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('destination_country','Destination Country :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('destination_country') ? 'has-error': ''}}">
                                                    {!! Form::select('destination_country', $countries,  (!empty($form_data->destination_country) ? $form_data->destination_country : ''),
                                                    ['class' => 'form-control textOnly input-sm required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('destination_zone','Destination Zone :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('destination_zone') ? 'has-error': ''}}">
                                                    {!! Form::select('destination_zone', $countries,  (!empty($form_data->destination_zone) ? $form_data->destination_zone : ''),
                                                    ['class' => 'form-control textOnly input-sm required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('consignee_name','Consignee Name :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('consignee_name') ? 'has-error': ''}}">
                                                    {!! Form::text('consignee_name',  (!empty($form_data->consignee_name) ? $form_data->consignee_name : ''),
                                                    ['class' => 'form-control input-sm textOnly required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::label('consignee_address','Consignee Address :', ['class'=>'col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('consignee_address') ? 'has-error': ''}}">
                                                    {!! Form::textarea('consignee_address',(!empty($form_data->consignee_address) ? $form_data->consignee_address : ''),
                                                    ['class'=>'form-control input-sm required', 'rows' => 2, 'cols' => 40]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style=" ">
                                        <div class="row">
                                            <div class="col md-6">
                                                <div class="hidden col-md-6" id="carrier_name_div">
                                                    {!! Form::label('carrier_name','Name of carrier :', ['class'=>'col-md-5']) !!}
                                                    <div class="col-md-7 {{$errors->has('carrier_name') ? 'has-error': ''}}">
                                                        {!! Form::text('carrier_name', (!empty($form_data->carrier_name) ? $form_data->carrier_name : ''),
                                                        ['class' => 'form-control textOnly input-sm']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="hidden col-md-12-" id="carrier_passport_div">
                                                <div class="col-md-6">
                                                    {!! Form::label('carrier_passport_no','Passport No. :', ['class'=>'col-md-5']) !!}
                                                    <div class="col-md-7 {{$errors->has('carrier_passport_no') ? 'has-error': ''}}">
                                                        {!! Form::text('carrier_passport_no',  (!empty($form_data->carrier_passport_no) ?
                                                        $form_data->carrier_passport_no : ''), ['class' => 'col-md-12 input-sm form-control']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::label('carrier_pass_validity','Passport Validity :', ['class'=>'col-md-5']) !!}
                                                    <div class="col-md-7">
                                                        <div class="datepicker input-group date">
                                                            {!! Form::text('carrier_pass_validity', (!empty($form_data->carrier_pass_validity) && $form_data->carrier_pass_validity != '0000-00-00' ?
                                                            App\Libraries\CommonFunction::changeDateFormat(substr($form_data->carrier_pass_validity, 0, 10)) : ''),
                                                            ['class' => 'col-md-12  input-sm form-control']) !!}
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::label('remark','Remarks :', ['class'=>'text-left col-md-5 required-star']) !!}
                                                <div class="col-md-7 {{$errors->has('remark') ? 'has-error': ''}}">
                                                    {!! Form::textarea('remark',(!empty($form_data->remark) ? $form_data->remark : ''),['class'=>'form-control input-sm required',
                                                    'rows' => 2, 'cols' => 40]) !!}

                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p></p>
                                    </div>




                                </div><!--/col-md-12-->
                            </div> <!--/panel-body-->
                        </div> <!--/panel-->
                            <?php
                            if (!empty($allRequestVal)) {
                                if (!empty($allRequestVal['meterial_ids'])) {

                                    $meterials = array();
                                    $meterialsd = array();
                                    foreach ($allRequestVal['meterial_ids'] as $key => $meterial) {
                                        $meterialsd['id'] = $allRequestVal['meterial_ids'][$key];
                                        $meterialsd['product_description'] = $allRequestVal['product_description'][$key];
                                        $meterialsd['fob_usd'] = $allRequestVal['fob_usd'][$key];
                                        $meterialsd['fob_usd_value'] = $allRequestVal['fob_usd_value'][$key];
                                        $meterialsd['hs_code'] = $allRequestVal['hs_code'][$key];
                                        $meterialsd['mat_quantity'] = $allRequestVal['mat_quantity'][$key];
                                        $meterialsd['mat_quantity_unit'] = $allRequestVal['mat_quantity_unit'][$key];
                                        $meterials[] = (object) $meterialsd;
                                    }
                                }
                            }
                            ?>

                            <?php
                            if (!empty($allRequestVal)) {
                                if (!empty($allRequestVal['epLcInfo_ids'])) {

                                    $epLcInfos = array();
                                    $epLcInfosd = array();
                                    foreach ($allRequestVal['epLcInfo_ids'] as $key => $epLcInfosddd) {
                                        $epLcInfosd['id'] = $allRequestVal['epLcInfo_ids'][$key];
                                        $epLcInfosd['tt_no'] = $allRequestVal['tt_no'][$key];
                                        $epLcInfosd['tt_value'] = $allRequestVal['tt_value'][$key];
                                        $epLcInfosd['bank_id'] = $allRequestVal['bank_id'][$key];
                                        $epLcInfosd['bank_invoice_date'] = $allRequestVal['bank_invoice_date'][$key];
                                        $epLcInfosd['bank_type'] = $allRequestVal['bank_type'][$key];
                                        $epLcInfosd['expiry_date'] = $allRequestVal['expiry_date'][$key];
                                        $epLcInfos[] = (object) $epLcInfosd;
                                    }
                                }
                            }
                            ?>

                            @if(isset($meterials) && count($meterials) > 0)
                            <div id="templateImdFullPar" style="margin: 3px;">
                                <?php $inc = 0; ?>
                                @foreach($meterials as $eachmeterials)
                                    <?php $eachEpLcInfo  = $epLcInfos[$inc];?>
                                    <?php $templateImdFull = ($inc == 0 ? 'templateImdFull' : 'rowCount' . $inc); ?>
                                    <div class="panel panel-black templateImdFull" id="{{$templateImdFull}}">
                                    <div class="panel-heading"><strong><i class="fa fa-list"></i> 2. Export Details & L/C Information Group</strong>
                                        <span class="pull-right">
                                            <?php if ($inc == 0) { ?>
                                                    <a href="javascript:void(0)" onclick="addTableRow('moreInfoImd', 'templateImdFull');" class="btn btn-xs btn-info addTableRows">
                                                    <i class="fa fa-plus"></i></a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0)" onclick="removeTableRow('templateImdFullPar', 'rowCount<?php echo $inc; ?>')" class="btn btn-xs btn-info addTableRows btn-danger">
                                                    <i class="fa fa-times"></i></a>
                                            <?php } ?>
                                        </span>
                                    </div>
                                    <div class="panel-body" style="margin: 4px;">
                                        <div id="edidMoreInfoMd{{$inc}}" style="margin: 3px;">
                                            <div class="panel panel-primary templateImd" id="templateImd{{$inc}}">
                                                <div class="panel-heading">
                                                    <span class=""><strong>2.i) Export Details</strong></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-md-12">

                                                        <div class="form-group ">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="hidden" name="meterial_ids[{{$inc}}]" value="<?php echo!empty($eachmeterials->id) ? $eachmeterials->id : ''; ?>">
                                                                    {!! Form::label("product_description[$inc]",'Product description :',['class'=>'col-md-5 required-star']) !!}
                                                                    <div class="col-md-7">
                                                                        <textarea name="product_description[{{$inc}}]" id="product_description[{{$inc}}]"
                                                                                  class="form-control input-sm required ">{{ $eachmeterials->product_description }}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    {!! Form::label("fob_usd_value",'FOB / CNF / CIF / CRF / X-Factory / DDU / Replacement / Free of Cost value:',
                                                                    ['class'=>'col-md-5 required-star']) !!}
                                                                    <div class="col-md-7">
                                                                        {!! Form::select('fob_usd['.$inc.']',$currencies,(!empty($eachmeterials->fob_usd) ? $eachmeterials->fob_usd : '107'),['class'=>'input-sm col-md-5 col-sm-12 col-xs-12 required','placeholder'=>'Select One']) !!}
                                                                        {!! Form::text('fob_usd_value['.$inc.']',(!empty($eachmeterials->fob_usd_value) ? $eachmeterials->fob_usd_value : ''),['class'=>'col-md-6 col-md-offset-1 col-sm-12 col-xs-12 required onlyNumber input-sm','id'=>'fob_usd_value['.$inc.']']) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <div class="row">
                                                                        <div class="col-md-6">
                                                                            {!! Form::label("hs_code[$inc]",'HS Code :',['class'=>'col-md-5 required-star']) !!}
                                                                            <div class="col-md-7">
                                                                                <input type="text" name="hs_code[{{$inc}}]" id="hs_code[{{$inc}}]"
                                                                                       class="hscodes form-control input-sm required" value="<?php echo!empty($eachmeterials->hs_code) ? $eachmeterials->hs_code : ''; ?>"/>
                                                                                <div style="clear: both;height: 2px;"></div>
                                                                                <input type="text" name="hs_product[{{$inc}}]" id="hs_product[{{$inc}}]"
                                                                                       value="<?php echo!empty($eachmeterials->hs_product) ? $eachmeterials->hs_product : ''; ?>"
                                                                                       class="productNames form-control input-sm" placeholder="HS Product" readonly="readonly"/>
                                                                                <p class="empty-message"></p>
                                                                            </div>
                                                                        </div>
                                                                <div class="col-md-6"></div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    {!! Form::label("mat_quantity[$inc]",'Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                                    <div class="col-md-7">
                                                                        <input type="text" name="mat_quantity[{{$inc}}]" id="mat_quantity[{{$inc}}]" class="form-control input-sm required onlyNumber"
                                                                               value="{{(!empty($eachmeterials->mat_quantity) ? $eachmeterials->mat_quantity : '')}}" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    {!! Form::label("mat_quantity_unit[$inc]",'Unit of Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                                    <div class="col-md-7">
                                                                        <select name="mat_quantity_unit[{{$inc}}]" id="mat_quantity_unit[{{$inc}}]" class="form-control input-sm required">
                                                                            <option value="">Select One</option>
                                                                            @foreach($quantity_unit as $key=>$value)
                                                                            <option value="{{$key}}" {{($eachmeterials->mat_quantity_unit==$key) ? 'selected' : ''}}>{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div><p></p>
                                                        </div>

                                                    </div><!--/col-md-12-->
                                                </div> <!--/panel-body-->
                                            </div> <!--/panel-->
                                        </div>

                            @if(isset($epLcInfos) && count($epLcInfos) > 0)
                                <div id="edidMoreInfoPo{{$inc}}">
                                    <?php $templateTtPo = ($inc == 0 ? 'templateTtPo' : 'rowCount' . $inc); ?>
                                    <div class="panel panel-primary templateTtPo" id="{{$templateTtPo}}" style="margin: 3px;">
                                        <div class="panel-heading"><strong>2.ii) TT / P.O/ SC/ CM / L/C Information</strong></div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="epLcInfo_ids[{{$inc}}]" value="<?php echo!empty($eachEpLcInfo->id) ? $eachEpLcInfo->id : ''; ?>">
                                                                {!! Form::label("tt_no[$inc]",'TT / P.O/ SC/ CMT/ L/C No. :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7">
                                                                    <input type="text" name="tt_no[{{$inc}}]" id="tt_no[{{$inc}}]" class="form-control input-sm required"
                                                                           value="{{(!empty($eachEpLcInfo->tt_no) ? $eachEpLcInfo->tt_no : '')}}" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label("tt_value[$inc]",'TT / P.O/ SC/ CMT/ L/C Value :',['class'=>'col-md-5']) !!}
                                                                <div class="col-md-7">
                                                                    <input type="text" name="tt_value[{{$inc}}]" id="tt_value[{{$inc}}]" class="form-control input-sm onlyNumber"
                                                                           value="{{(!empty($eachEpLcInfo->tt_value) ? $eachEpLcInfo->tt_value : '')}}" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label("bank_id[$inc]",'Issuing Bank :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7">
                                                                    <select name="bank_id[{{$inc}}]" id="bank_id[{{$inc}}]" class="form-control input-sm required">
                                                                        <option value="">Select One</option>
                                                                        @foreach($banks as $key=>$value)
                                                                            <option value="{{$key}}" {{(!empty($eachEpLcInfo->bank_id)) && ($eachEpLcInfo->bank_id == $key) ? "selected" :""}}>
                                                                                {{$value}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label("bank_invoice_date[$inc]",'Issue Date :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7">
                                                                    <div class="datepicker input-group date col-md-12">
                                                                        <?php
                                                                        if (!empty($allRequestVal)) {
                                                                            $issueDate = $eachEpLcInfo->bank_invoice_date;
                                                                        } else {
                                                                            $issueDate = (!empty($eachEpLcInfo->bank_invoice_date) && $eachEpLcInfo->bank_invoice_date != '0000-00-00' ? App\Libraries\CommonFunction::changeDateFormat(substr($eachEpLcInfo->bank_invoice_date, 0, 10)) : '');
                                                                        }
                                                                        ?>
                                                                        <input type="text" name="bank_invoice_date[{{$inc}}]" placeholder="dd-mm-yyyy" id="bank_invoice_date[{{$inc}}]" class="form-control input-sm required"
                                                                               value="{{$issueDate}}" />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label("bank_type[$inc]",'Type :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7">
                                                                    <select name="bank_type[{{$inc}}]" id="bank_type[{{$inc}}]" class="form-control input-sm required">
                                                                        @foreach($bankTypes as $key=>$value)
                                                                            <option value="{{$key}}" {{(!empty($eachEpLcInfo->bank_type)) && ($eachEpLcInfo->bank_type == $key) ? "selected" :""}}>
                                                                                {{$value}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                {!! Form::label("expiry_date[$inc]",'Expiry Date :',['class'=>'col-md-5 required-star']) !!}
                                                                <div class="col-md-7">
                                                                    <div class="datepicker input-group date">
                                                                        <?php
                                                                        if (!empty($allRequestVal)) {
                                                                            $expiry_date = $eachEpLcInfo->expiry_date;
                                                                        } else {
                                                                            $expiry_date = (!empty($eachEpLcInfo->expiry_date) && $eachEpLcInfo->expiry_date != '0000-00-00' ? App\Libraries\CommonFunction::changeDateFormat(substr($eachEpLcInfo->expiry_date, 0, 10)) : '');
                                                                        }
                                                                        ?>
                                                                        <input type="text" name="expiry_date[{{$inc}}]" id="expiry_date[{{$inc}}]" placeholder="dd-mm-yyyy" class="form-control input-sm required"
                                                                               value="{{$expiry_date}}" />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div><p></p>
                                                    </div>
                                                </div> <!-- col-md-12 close-->
                                            </div><!--panel-body-->
                                        </div> <!--/panel-->
                                            </div>
                                        @endif
                                    </div> <!--/panel-black body close-->
                                </div> <!--/panel-black close-->
                                <?php $inc++; ?>
                            @endforeach
                        </div>
                        @else
                    <div class="panel panel-black templateImdFull" id="templateImdFull">
                            <div class="panel-heading"><strong><i class="fa fa-list"></i> 2. Export Details & L/C Information Group</strong>
                                <span class="pull-right">
                                    <a href="javascript:void(0)" onclick="addTableRow('moreInfoImd', 'templateImdFull');" class="btn btn-xs btn-info addTableRows">
                                        <i class="fa fa-plus"></i></a>
                                </span>
                            </div>
                            <div class="panel-body" style="margin: 4px;">
                                <div class="panel panel-primary templateImd" id="templateImd" style="margin: 3px;;">
                                    <div class="panel-heading">
                                        <span class=""><strong>2.i) Export Details</strong></span></div>
                                    <div class="panel-body">
                                        <div class="col-md-12">

                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="hidden" name="meterial_ids[0]" value="<?php echo!empty($eachmeterials->id) ? $eachmeterials->id : ''; ?>">
                                                        {!! Form::label('product_description[0]','Product description :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <textarea name="product_description[0]" id="product_description[0]" class="form-control input-sm required"
                                                                      value="{{(!empty($form_data->product_description) ? $form_data->product_description : '')}}"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::label('fob_usd_value','FOB / CNF / CIF / CRF / X-Factory / DDU / Replacement / Free of Cost value :',
                                                        ['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            {!! Form::select('fob_usd[]',$currencies,(!empty($form_data->fob_usd) ? $form_data->fob_usd : '107'),['class'=>'input-sm col-md-5 col-sm-12 col-xs-12 required','placeholder'=>'Select One']) !!}
                                                            {!! Form::text('fob_usd_value[0]',(!empty($form_data->fob_usd_value) ? $form_data->fob_usd_value : ''),['class'=>'col-md-6 col-md-offset-1 col-sm-12 col-xs-12 onlyNumber required input-sm','id'=>'fob_usd_value[0]']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <div class="row">
                                                            <div class="col-md-6">
                                                                {!! Form::label('hs_code[0]','HS Code :',['class'=>'hscodes col-md-5 required-star']) !!}
                                                                <div class="col-md-7 {{ $errors->has('hs_code') ? 'has-error' : '' }}">
                                                                    <input type="text" name="hs_code[0]" id="hs_code[0]" class="hscodes form-control input-sm required"/>
                                                                    <div style="clear: both;height: 2px;"></div>
                                                                    <input type="text" name="hs_product[0]" id="hs_product[0]" value=""
                                                                           class="productNames form-control input-sm" placeholder="HS product" readonly="readonly"/>
                                                                    <p class="empty-message"></p>
                                                                </div>
                                                            </div>
                                                    <div class="col-md-6"></div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        {!! Form::label('mat_quantity[0]','Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <input type="text" name="mat_quantity[0]" id="mat_quantity[0]" class="form-control input-sm required onlyNumber"
                                                                   value="{{(!empty($form_data->mat_quantity) ? $form_data->mat_quantity : '')}}" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::label('mat_quantity_unit[0]','Unit of Quantity :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <select name="mat_quantity_unit[0]" id="mat_quantity_unit[0]" class="form-control input-sm required">
                                                                <option value="">Select One</option>
                                                                @foreach($quantity_unit as $key=>$value)
                                                                <option value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div><p></p>
                                            </div>
                                        </div><!--/col-md-12-->
                                    </div> <!--/panel-body-->
                                </div> <!--/panel-->

                                <div class="panel panel-primary templateTtPo" id="templateTtPo" style="margin: 3px;">
                                    <div class="panel-heading"><strong>2.ii) TT / P.O/ SC/ CM / L/C Information </strong></div>
                                    <div class="panel-body">

                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="hidden" name="epLcInfo_ids[0]" value="<?php echo!empty($eachIpIcInfo->id) ? $eachIpIcInfo->id : ''; ?>">
                                                        {!! Form::label('tt_no[0]','TT / P.O/ SC/ CMT/ L/C No. :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <input type="text" name="tt_no[0]" id="tt_no[0]" class="form-control input-sm required"
                                                                   value="{{(!empty($form_data->tt_no) ? $form_data->tt_no : '')}}" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::label('tt_value[0]','TT / P.O/ SC/ CMT/ L/C Value :',['class'=>'col-md-5']) !!}
                                                        <div class="col-md-7">
                                                            <input type="text" name="tt_value[0]" id="tt_value[0]" class="form-control input-sm onlyNumber"
                                                                   value="{{(!empty($form_data->tt_value) ? $form_data->tt_value : '')}}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        {!! Form::label('bank_id[0]','Issuing Bank :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <select name="bank_id[0]" id="bank_id[0]" class="form-control input-sm required">
                                                                <option value="">Select One</option>
                                                                @foreach($banks as $key=>$value)
                                                                    <option value="{{$key}}" {{(!empty($form_data->bank_id)) && ($form_data->bank_id == $key) ? "selected" :""}}>
                                                                        {{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::label('bank_invoice_date[0]','Issue Date :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <div class="datepicker input-group date">
                                                                <input type="text" placeholder="dd-mm-yyyy" name="bank_invoice_date[0]" id="bank_invoice_date[0]" class="form-control input-sm required"
                                                                       value="{{(!empty($form_data->bank_invoice_date) && $form_data->bank_invoice_date != '0000-00-00' ?
                                                                                   App\Libraries\CommonFunction::changeDateFormat(substr($form_data->bank_invoice_date, 0, 10)) : '')}}" />

                                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        {!! Form::label('bank_type[0]','Type :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <select name="bank_type[0]" id="bank_type[0]" class="form-control input-sm required">
                                                                @foreach($bankTypes as $key=>$value)
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::label('expiry_date[0]','Expiry Date :',['class'=>'col-md-5 required-star']) !!}
                                                        <div class="col-md-7">
                                                            <div class="datepicker input-group date">
                                                                <input type="text" name="expiry_date[0]" id="expiry_date[0]" class="form-control input-sm required"
                                                                       placeholder="dd-mm-yyyy" value="{{(!empty($form_data->expiry_date) && $form_data->expiry_date != '0000-00-00' ?
                                                                           App\Libraries\CommonFunction::changeDateFormat(substr($form_data->expiry_date, 0, 10)) : '')}}" />

                                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div><p></p>
                                            </div>
                                        </div> <!--col-md-12 close-->
                                    </div><!--panel-body-->
                                </div> <!--/panel-->

                            </div> <!--/panel-black body close-->
                        </div> <!--/panel-black close-->
                    @endif
               <div id="moreInfoImd" class="clear"></div>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>3. Required Documents for attachment</strong></div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover ">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th colspan="6">Required Attachments</th>
                                                <th colspan="2">Attached PDF file
                                                    <span onmouseover="toolTipFunction()" data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 1MB)!">
                                                        <i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            @foreach($document as $row)
                                            <tr>
                                                <td><div align="center">{!! $i !!}<?php echo $row->doc_priority == "1" ? "<span class='required-star'></span>" : ""; ?></div></td>
                                                <td colspan="6">{!!  $row->doc_name !!}</td>
                                                <td colspan="2">
                                                    <input name="document_id_<?php echo $row->doc_id; ?>" type="hidden" value="{{(!empty($clrDocuments[$row->doc_id]['doucument_id']) ? $clrDocuments[$row->doc_id]['doucument_id'] : '')}}">
                                                    <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>"
                                                           name="doc_name_<?php echo $row->doc_id; ?>" />
                                                    <input name="file<?php echo $row->doc_id; ?>" <?php if (empty($clrDocuments[$row->doc_id]['file']) && empty($allRequestVal["file$row->doc_id"]) && $row->doc_priority == "1"){ echo "class='required'"; } ?>
                                                           id="file<?php echo $row->doc_id; ?>" type="file" size="20"
                                                           onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php echo $row->doc_id; ?>', '<?php echo $row->doc_priority; ?>')"/>

                                                    @if($row->additional_field == 1)
                                                        <table>
                                                            <tr>
                                                                <td>Other file Name : </td>
                                                                <td> <input maxlength="64" class="form-control input-sm <?php if ($row->doc_priority == "1"){ echo 'required'; } ?>"
                                                                            name="other_doc_name_<?php echo $row->doc_id; ?>" type="text" value="{{(!empty($clrDocuments[$row->doc_id]['doc_name']) ? $clrDocuments[$row->doc_id]['doc_name'] : '')}}"></td>
                                                            </tr>
                                                        </table>
                                                    @endif

                                                    @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                        <div class="save_file saved_file_{{$row->doc_id}}">
                                                            <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ?
                                                                    $clrDocuments[$row->doc_id]['file'] : ''))}}" title="{{$row->doc_name}}">
                                                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php $file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name); ?></a>

                                                            <?php if(!empty($alreadyExistApplicant) && Auth::user()->id == $alreadyExistApplicant->created_by && $viewMode != 'on') {?>
                                                            <a href="javascript:void(0)" onclick="ConfirmDeleteFile({{ $row->doc_id }})">
                                                                <span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span>
                                                            </a>
                                                            <?php } ?>
                                                        </div>
                                                    @endif


                                                    <div id="preview_<?php echo $row->doc_id; ?>">
                                                        <input type="hidden" value="<?php echo !empty($clrDocuments[$row->doc_id]['file']) ?
                                                                $clrDocuments[$row->doc_id]['file'] : ''?>" id="validate_field_<?php echo $row->doc_id; ?>"
                                                               name="validate_field_<?php echo $row->doc_id; ?>" class="<?php echo $row->doc_priority == "1" ? "required":'';  ?>"  />
                                                    </div>


                                                    @if(!empty($allRequestVal["file$row->doc_id"]))
                                                    <label id="label_file{{$row->doc_id}}"><b>File: {{$allRequestVal["file$row->doc_id"]}}</b></label>
                                                    <input type="hidden" class="required" value="{{$allRequestVal["validate_field_".$row->doc_id]}}" id="validate_field_{{$row->doc_id}}" name="validate_field_{{$row->doc_id}}">
                                                    @endif

                                                </td>
                                            </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div><!-- /.panel-body -->
                        </div>

                            <?php if ($viewMode == "on") { ?>

                            @if(in_array(Auth::user()->desk_id,array(3,4,5,6)) || Auth::user()->user_type == '1x101')
                            <div class="row">
                                <div class="col-md-12"><br/></div>
                                <div class="col-md-12" style="margin-bottom:6px;">
                                    <a href="{{ url('project-clearance/view-cer/'. Encryption::encodeId($alreadyExistApplicant->created_by))}}"
                                       target="_blank" class="btn btn-warning btn-xs pull-left show-in-view ">
                                        <i class="fa fa-download"></i> <strong>Associated Certificates of Users</strong>
                                    </a>
                                </div>
                            </div>
                            @endif {{-- checking if RD desks or system admin --}}

                                      @include('exportPermit::doc-tab')
                            <?php } ?>

                        <div class="panel panel-primary hiddenDiv">
                            <div class="panel-heading"><strong>Terms and Conditions</strong></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12" style="margin: 12px 0;">
                                        {!! Form::checkbox('acceptTerms', 'yes', (!empty($form_data->acceptTerms) && $form_data->acceptTerms == 1 ? true : false), array('class'=>'required col-md-1 col-xs-1 col-sm-1 text-left','id'=>'acceptTerms-2','style'=>'width:3%;margin-left: 2px;')) !!}
                                        <label for="acceptTerms-2" class="col-md-11 col-xs-11 col-sm-11 text-left required-star">I agree with the Terms and Conditions.</label>
                                    </div>
                                </div>
                            </div>
                            </div> <!--/ main red panel with margin-->
                        <div style="margin:6px;">
                                @if(ACL::getAccsessRight('exportPermit','A'))
                            <div class="row">
                                    <?php if ($viewMode != 'on') { ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <?php
                                        $statusId = (!empty($form_data->status_id) ? $form_data->status_id : '');
                                        if ($statusId == -1 || $statusId == '') {
                                            ?>
                                            <button type="submit" class="btn btn-primary btn-md cancel" value="draft" name="submitInsert">Save as Draft</button>
                                        <?php } else { ?>
                                            &nbsp;
                                         <?php } ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                        <button type="submit" class="btn btn-primary btn-md" value="save" name="submitInsert">Submit</button>
                                    </div>
                                        {!! Form::close() !!}<!-- /.form end -->
                                <?php } ?>
                            </div>
                                @endif
                     <?php if ($viewMode == "on" && in_array(Auth::user()->user_type, array('1x101', '2x202', '4x404', '7x707', '8x808', '9x909'))) { ?>
                        @include('exportPermit::apps_history')
                    <?php } ?>

                   </div>

                    @if($viewMode == "on" && $form_data->status_id > 20)
                                <?php if (!empty($form_data) && in_array($form_data->status_id, [21,28,29, 30,31])) { ?>
                                <?php
                                }else{
                                ?>
                        <table class="" border="1" width="100%" style="text-align: center;">
                            <tr>
                                <td style=" padding: 20px;" class="alert alert-info">
                                    The goods specified in the invoice(s) are eligible for exemption for the whole of the customs duty, sales tax etc. vide S.R.O. No. 209-LAW/2015/46/Customs dated July 1,2015 issued by the Internal Resources Division, Ministry of Finance, Dhaka
                                </td>
                            </tr>
                        </table>
                        </br>
                        <table class="" border="1" width="100%" style="text-align: center;">
                            <tr>
                                <td style=" padding: 20px;" class="alert alert-danger">
                                    <b>
                                        THIS PERMIT IS VALID FOR 30 (THIRTY) DAYS FROM THE DATE OF ISSUE
                                    </b>
                                </td>
                            </tr>
                        </table>
                        </br>
                        <table class="" border="1" width="100%" style="text-align: center;">
                            <tr>
                                <td style=" padding: 20px;"  class="alert alert-warning">
                                    <b>
                                        THIS IS COMPUTER GENERATED PERMIT. SO SIGNATURE IS NOT REQUIRED
                                    </b>
                                </td>
                            </tr>
                        </table>
                        <?php }?>
                    @endif
                    </div> <!--/ main red panel with margin-->
                </div>
            </div>
        </div>
    </div>
</div>
 </section>

@endsection
@section('footer-script')
@if(in_array(Auth::user()->desk_id,array(9)) && ($alreadyExistApplicant->status_id == 30 || $alreadyExistApplicant->status_id == 31) )
                @include('partials.datatable-scripts')
            @endif

@if($viewMode !== 'on')
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
@endif

<script type="text/javascript">

    $('#permit_type').change(function (e) {
        if (this.value == '10') { // 10 is Hand carry export permit
            $('#source_country_div').removeClass('hidden');
            $('#source_country').addClass('required');
            $('#destination_zone_div').addClass('hidden');
            $('#carrier_name_div').removeClass('hidden');
            $('#carrier_passport_div').removeClass('hidden');
            $('#flight_div').removeClass('hidden');
        }else {
            $('#source_country_div').removeClass('hidden');
            $('#source_country').addClass('required');
            $('#destination_zone_div').addClass('hidden');
            $('#carrier_name_div').addClass('hidden');
            $('#carrier_passport_div').addClass('hidden');
            $('#flight_div').addClass('hidden');
        }
    });
    $('#permit_type').trigger('change');

<?php if ($viewMode != 'on') { ?>

        function uploadDocument(targets, id, vField, isRequired){
           // alert(id)
        var inputFile = $("#" + id).val();
                if (inputFile == '') {
        $("#" + id).html('');
                document.getElementById("isRequired").value = '';
                document.getElementById("selected_file").value = '';
                document.getElementById("validateFieldName").value = '';
                document.getElementById(targets).innerHTML = '<input type="hidden" class="required" value="" id="' + vField + '" name="' + vField + '">';
                if ($('#label_' + id).length)
                    $('#label_' + id).remove();
                return false;
        }

        try {
        document.getElementById("isRequired").value = isRequired;
                document.getElementById("selected_file").value = id;
                document.getElementById("validateFieldName").value = vField;
                document.getElementById(targets).style.color = "red";
                var action = "{{url('/export-permit/upload-document')}}";
                $("#" + targets).html('Uploading....');
                var file_data = $("#" + id).prop('files')[0];
                var form_data = new FormData();
                form_data.append('selected_file', id);
                form_data.append('isRequired', isRequired);
                form_data.append('validateFieldName', vField);
                form_data.append('_token', "{{ csrf_token() }}");
                form_data.append(id, file_data);
                $.ajax({
                target: '#' + targets,
                        url: action,
                        dataType: 'text', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                        $('#' + targets).html(response);
                                var fileNameArr = inputFile.split("\\");
                                var l = fileNameArr.length;
                                if ($('#label_' + id).length)
                                $('#label_' + id).remove();

                                var doc_id = parseInt(id.substring(4));
                                var newInput = $('<label class="saved_file_'+doc_id+'" id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + ' <a href="javascript:void(0)" onclick="EmptyFile('+ doc_id +')"><span class="btn btn-xs btn-danger"><i class="fa fa-times"></i></span> </a></b></label>');
                                $("#" + id).after(newInput);
                                //check valid data
                                var validate_field = $('#' + vField).val();
                                if (validate_field == '') {
                        document.getElementById(id).value = '';
                        }
                        }
                });
        } catch (err) {
        document.getElementById(targets).innerHTML = "Sorry! Something Wrong.";
        }
        }
        $(document).ready(function () {
            $("form#exportPermitForm").validate();
        $(document).on('click', '.cancel', function (evt)
        {
        // find parent form, cancel validation and submit it
        // cancelSubmit just prevents jQuery validation from kicking in
                $("#exportPermitForm").closest('form').data("validator").cancelSubmit = true;
                $("#exportPermitForm").closest('form').submit();
                return false;
        });

        });
                //var rowCount = 0;
        function addTableRow(tableID, templateRow) {
                        //rowCount++;
                        //Direct Copy a row to many times
                        var x = document.getElementById(templateRow).cloneNode(true);
                                x.id = "";
                                x.style.display = "";
                                var table = document.getElementById(tableID);
                                var rowCount = $('.' + templateRow).length;
                                var lastTr = $('#' + tableID).find('.' + templateRow).last().attr('data-number');
                                if (lastTr != '' && typeof lastTr !== "undefined") {
                        rowCount = parseInt(lastTr) + 1;
                        }
                        //var rowCount = table.rows.length;
                        //Increment id
                        var rowCo = rowCount;
                                var idText = 'rowCount' + rowCount;
                                x.id = idText;
                                $("#" + tableID).append(x);
                                //get select box elements
                                var attrSel = $("#" + tableID).find('#' + idText).find('select');
                                for (var i = 0; i < attrSel.length; i++) {
                        var nameAtt = attrSel[i].name;
                                var idAtt = attrSel[i].id;
                                //increment all array element name
                                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                                var repTextId = idAtt.replace('[0]', '[' + rowCo + ']');
                                attrSel[i].name = repText;
                                attrSel[i].id = repTextId;
                        }
                        //get input elements
                        var attrImput = $("#" + tableID).find('#' + idText).find('input');
                                for (var i = 0; i < attrImput.length; i++) {
                        var nameAtt = attrImput[i].name;
                                var idAtt = attrImput[i].id;
                                //increment all array element name
                                var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                                var repTextId = idAtt.replace('[0]', '[' + rowCo + ']');
                                attrImput[i].name = repText;
                                attrImput[i].id = repTextId;
                        }

                        //get textarea elements
                        var attrTextarea = $("#" + tableID).find('#' + idText).find('textarea');
                        for (var i = 0; i < attrTextarea.length; i++){
                            var nameAtt = attrTextarea[i].name;
                            var idAtt = attrTextarea[i].id;
                            //increment all array element name
                            var repText = nameAtt.replace('[0]', '[' + rowCo + ']');
                            var repTextId = idAtt.replace('[0]', '[' + rowCo + ']');
                            attrTextarea[i].name = repText;
                            attrTextarea[i].id = repTextId;
                            $('#' + idText).find('.readonlyClass').prop('readonly',true);
                        }
                        attrTextarea.val(''); //value reset

                        var attrLable = $("#" + tableID).find('#' + idText).find('label');
                                for (var i = 0; i < attrLable.length; i++) {
                        var htmlFor = attrLable[i].htmlFor;
                                //increment all array element name
                                var repTextFor = htmlFor.replace('[0]', '[' + rowCo + ']');
                                attrLable[i].htmlFor = repTextFor;
                        }

                        //value reset
                        attrImput.val('');
                        //selected index reset
                        attrSel.prop('selectedIndex', 0);
                        //Class change by btn-danger to btn-primary
                        $("#" + tableID).find('#' + idText).find('.addTableRows').removeClass('btn-primary').addClass('btn-danger').attr('onclick', 'removeTableRow("' + tableID + '","' + idText + '")');
                        $("#" + tableID).find('#' + idText).find('.addTableRows > .fa').removeClass('fa-plus').addClass('fa-times');
                        $('#' + tableID).find('.' + templateRow).last().attr('data-number', rowCount);
                        $("#" + tableID).find('.datepicker').datetimepicker({
                            viewMode: 'years',
                                    format: 'DD-MMM-YYYY',
                                    maxDate: (new Date()),
                                    minDate: '01/01/1905'
                            });

                        $("#" + tableID).find('#' + idText).find('.onlyNumber').on('keydown', function (e) {
                            //period decimal
                            if ((e.which >= 48 && e.which <= 57)
                                    //numpad decimal
                                    || (e.which >= 96 && e.which <= 105)
                                    // Allow: backspace, delete, tab, escape, enter and .
                                    || $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1
                                    // Allow: Ctrl+A
                                    || (e.keyCode == 65 && e.ctrlKey === true)
                                    // Allow: Ctrl+C
                                    || (e.keyCode == 67 && e.ctrlKey === true)
                                    // Allow: Ctrl+V
                                    || (e.keyCode == 86 && e.ctrlKey === true)
                                    // Allow: Ctrl+X
                                    || (e.keyCode == 88 && e.ctrlKey === true)
                                    // Allow: home, end, left, right
                                    || (e.keyCode >= 35 && e.keyCode <= 39))
                            {
                                var thisVal = $(this).val();
                                if (thisVal.indexOf(".") != -1 && e.key == '.') {
                                    return false;
                                }
                                $(this).removeClass('error');
                                return true;
                            }
                            else
                            {
                                $(this).addClass('error');
                                return false;
                            }
                        });

            $("#" + tableID).find('.hscodes').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{url('export-permit/get-hscodes')}}",
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 4,
                response: function (event, ui) {
                    if (ui.content.length === 0) {
                         $(this).parent().find(".empty-message").text("No results found");
                         $(this).parent().find(".productNames").addClass("required error").val('');
                    } else {
                         $(this).parent().find(".empty-message").empty();
                         $(this).parent().find(".productNames").removeClass("required error");
                    }
                },
                select: function (event, data) {
                    $(this).parent().find('.productNames').val(data.item.product);
                }
            });
        }

                function removeTableRow(tableID, removeNum) {
                $('#' + tableID).find('#' + removeNum).remove();
                }



           $(document).ready(function () {
            $(".hscodes").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{url('export-permit/get-hscodes')}}",
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 4,
                response: function (event, ui) {
                    if (ui.content.length === 0) {
                         $(this).parent().find(".empty-message").text("No results found");
                         $(this).parent().find(".productNames").addClass("required error").val('');
                    } else {
                         $(this).parent().find(".empty-message").empty();
                         $(this).parent().find(".productNames").removeClass("required error");
                    }
                },
                select: function (event, data) {
                    $(this).parent().find('.productNames').val(data.item.product);
                }
            });
        });

<?php } ?>  /*View mode is off */

<?php if ($viewMode == 'on') { ?>

              @if (($process_data ->status_id == 21 || $process_data -> status_id == 24) && Auth::user() -> id == $form_data -> created_by)
                        $(document).ready(function () {
                var today = new Date();
                        var yyyy = today.getFullYear();
                        var mm = today.getMonth();
                        var dd = today.getDate();
                        $('#user_DOB').datetimepicker({
                viewMode: 'years',
                        format: 'DD-MMM-YYYY',
                        minDate: '01/01/' + (yyyy - 6)
                });
                });
                        @endif {{-- checking process data --}}

              $('#inputForm select').each(function (index){
                    var text = $(this).find('option:selected').text();
                    var id = $(this).attr("id");
                    var val = $(this).val();
                    $('#' + id + ' option:selected').replaceWith("<option value='" + val + "' selected>" + text + "</option>");
                });
                $("#inputForm :input[type=text]").each(function (index) {
                    $(this).attr("value", $(this).val());
                });
               $("#inputForm textarea").each(function (index) {
                $(this).text($(this).val());
               });

               $("#inputForm select").css({
                    "border": "none",
                    "background": "#fff",
                    "pointer-events": "none",
                    "box-shadow": "none",
                    "-webkit-appearance": "none",
                    "-moz-appearance": "none",
                    "appearance": "none"
                });

                $("#inputForm .actions").css({"display": "none"});
                $("#inputForm .draft").css({"display": "none"});
                $("#inputForm .title ").css({"display": "none"});
                //document.getElementById("previewDiv").innerHTML = document.getElementById("projectClearanceForm").innerHTML;

                $('#inputForm #showPreview').remove();
                $('#inputForm #save_btn').remove();
                $('#inputForm #save_draft_btn').remove();
                $('#inputForm .stepHeader, #inputForm .calender-icon,#inputForm .pss-error,#inputForm .hiddenDiv, #inputForm .input-group-addon').remove();
                $('#inputForm .required-star').removeClass('required-star');
                $('#inputForm input[type=hidden], #inputForm input[type=file]').remove();
                $('#inputForm .panel-orange > .panel-heading').css('margin-bottom', '10px');
                $('#invalidInst').html('');

                $('#inputForm').find('input:not(:checked),textarea').each(function() {
                    if (this.value != ''){
                        var displayOp = ''; //display:block
                    } else{
                        var displayOp = 'display:none';
                    }

                    if($(this).hasClass("onlyNumber") && !$(this).hasClass("nocomma")){
                        var thisVal = commaSeparateNumber(this.value);
                        $(this).replaceWith("<span class='onlyNumber " +this.className+
                                "' style='background-color:#ddd !important;height:auto;border-radius:3px;padding:6px;"
                                + displayOp + "'>" +thisVal + "</span>");
                    }else {
                        $(this).replaceWith("<span class='" +this.className+"' style='background-color:#ddd;padding:6px;height:auto; margin-bottom:2px;"
                                + displayOp + "'>" + this.value + "</span>");
                    }
                });

                $('#inputForm').find('textarea').each(function(){
                    var displayOp = '';
                    if (this.value != '') {
                        displayOp = ''; //display:block
                    } else {
                        displayOp = 'display:none';
                    }
                    $(this).replaceWith("<span class='"+this.className+"'style='background-color:#ddd;height:auto; margin-bottom:2px;padding:6px;"
                            + displayOp + "'>" + this.value + "</span>");
                });

                $('#inputForm .btn').not('.show-in-view').each(function(){
                    $(this).replaceWith("");
                });

                $('#inputForm').find('input[type=radio]').each(function (){
                    jQuery(this).attr('disabled', 'disabled');
                });

                $("#inputForm select").replaceWith(function (){
                    var selectedText = $(this).find('option:selected').text().trim();
                    var displayOp = '';
                    if (selectedText != '' && selectedText != 'Select One') {
                        displayOp = ''; //display:block
                    } else {
                        displayOp = 'display:none';
                    }

                    return "<span class='"+this.className+"' style='background-color:#ddd;height:auto;padding:6px;height:auto; margin-bottom:2px;"
                            + displayOp + "'>" + selectedText + "</span>";
                });

                $("#inputForm select").replaceWith(function (){
                    var selectedText = $(this).find('option:selected').text();
                    return "<span style='background-color:#ddd;width:68%;padding:6px;display:block;height:auto; margin-bottom:2px;'>"
                            + selectedText + "</span>";
                });

                function commaSeparateNumber(val){
                    while (/(\d+)(\d{3})/.test(val.toString())){
                        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                    }
                    return val;
                }

<?php } ?> /* viewMode is on */

 check = function (e,value){
     if (!e.target.validity.valid) {
       e.target.value = value.substring(0,value.length - 1);
        return false;
      }
     var idx = value.indexOf('.');
      if (idx >= 0) {
        if (value.length - idx > 3 ) {
          e.target.value = value.substring(0,value.length - 1);
          return false;
        }
      }
      return true;
    }

            function toolTipFunction() {
            $('[data-toggle="tooltip"]').tooltip();
            }

            $(document).on('click', '.download', function (e) {

            var value = $(this).attr('data');
                    $('.show_' + value).show();
            });

<?php if ($viewMode != 'on') { ?>
        $(document).ready(function(){
              $('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: (new Date()),
                minDate: '01/01/1905'
            });
        });
<?php } ?>

</script>

@if ($viewMode == 'on' && in_array(Auth::user()->desk_id,array(3,4,5,6,7,9))) {{--batch process start--}}
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
<script language="javascript">

               @if(in_array(Auth::user()->desk_id,array(9)) && ($alreadyExistApplicant->status_id == 30 || $alreadyExistApplicant->status_id == 31) )
                $(function () {
                    $('#gpHistlist').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "ordering": true,
                        "info": false,
                        "autoWidth": false,
                        "iDisplayLength": 20
                    });
                });
            @endif

                    var numberOfCheckedBox = 0;
                    var curr_app_id = '';
                    function setCheckBox() {
                    numberOfCheckedBox = 0;
                            var flag = 1;
                            var selectedWO = $("input[type=checkbox]").not(".selectall");
                            selectedWO.each(function() {
                            if (this.checked) {
                            numberOfCheckedBox++;
                            }else {
                            flag = 0;
                            }
                            });
                            if (flag == 1){
                                $("#chk_id").checked = true;
                            }else {
                                $("#chk_id").checked = false;
                            }
                        if (numberOfCheckedBox >= 1){
                            $('.applicable_status').trigger('click');
                        }
                    } // end of setCheckBox function

            function changeStatus(check) {
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    setCheckBox();
            }
            $(document).ready(function() {

            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
             $("#apps_from").validate({
            errorPlacement: function() {
            return false;
            }
            });
                    $("#batch_from").validate({
            errorPlacement: function() {
            return false;
            }
            });
                    var base_checkbox = '.selectall';
                    $(base_checkbox).click(function() {
            if (this.checked) {
            $('.appCheckBox:checkbox').each(function() {
            this.checked = true;
                    $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            });
            } else {
            $('.appCheckBox:checkbox').each(function() {
            this.checked = false;
                    $('#status_id').html('<option selected="selected" value="">Select Below</option>');
            });
            }
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    setCheckBox();
            });
                    $('.appCheckBox:checkbox').not(base_checkbox).click(function() {
            $(".selectall").prop("checked", false);
            });

                    var break_for_pending_verification = 0;
                    $(document).ready(function() {

            $("#status_id").trigger("click");
                    var curr_app_id = $("#curr_app_id").val();
                    var curr_status_id = $("#curr_status_id").val();
                    $.ajaxSetup({async: false});
                    var _token = $('input[name="_token"]').val();
                    var delegate = '{{ @$delegated_desk }}';
                    var state = false;
                    $.post('/export-permit/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate,
                        _token: _token}, function(response) {

                    if (response.responseCode == 1) {
                    var option = '';
                    option += '<option selected="selected" value="">Select Below</option>';
                    $.each(response.data, function(id, value) {
                    option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                    });
                   $("#status_id").html(option);
                            $("#status_id").trigger("change");
                            $("#status_id").focus();
                    } else if (response.responseCode == 5){
                    alert('Without verification, application can not be processed');
                            break_for_pending_verification = 1;
                            option = '<option selected="selected" value="">Select Below</option>';
                            $("#status_id").html(option);
                            $("#status_id").trigger("change");
                            return false;
                    } else {
                    $('#status_id').html('Please wait');
                    }
                    });
                    $.ajaxSetup({async: true});
            });

                    $(document).on('change', '.status_id', function() {
                    var curr_app_id_for_process = $("#curr_app_id").val();
                    var curr_status_id_for_process = $("#curr_status_id").val();
                    var object = $(".status_id");
                    var obj = $(object).parent().parent().parent();
                    var id = $(object).val();
                    var _token = $('input[name="_token"]').val();
                    var status_from = $('#status_from').val();
                    $('#sendToDeskOfficer').css('display', 'block');
                    if (id == 0) {
                        obj.find('.param_id').html('<option value="">Select Below</option>');
            } else {
                        //  if(id == 5 || id == 8 || id == 9 || id == 14 || id == 2){
                    $.post('/export-permit/ajax/process', {id: id, curr_app_id: curr_app_id_for_process, status_from: curr_status_id_for_process, _token: _token}, function(response) {
                    if (response.responseCode == 1) {
                    var option = '';
                    option += '<option selected="selected" value="">Select Below</option>';
                    var countDesk = 0;
                    $.each(response.data, function(id, value) {
                    countDesk++;
                            option += '<option value="' + id + '">' + value + '</option>';
                    });
                    obj.find('#desk_id').html(option);
                    $('#desk_id').attr("disabled", false);
                    $('#remarks').attr("disabled", false);

                    if (countDesk == 0){
                            $('.dd_id').removeClass('required');
                            $('#sendToDeskOfficer').css('display', 'none');
                    }else{
                            $('.dd_id').addClass('required');
                    }

            if (response.status_to == 5 || response.status_to == 8 || response.status_to == 16 || response.status_to == 17 || response.status_to == 19
                    || response.status_to == 24|| response.status_to == 10 || response.status_to == 22){

            $('#remarks').addClass('required');
                    $('#remarks').attr("disabled", false);
            }else{
            $('#remarks').removeClass('required');
            }

                        if (response.file_attach == 1){
                                $('#sendToFile').css('display', 'block');
                        }else{
                                 $('#sendToFile').css('display', 'none');
                        }
                 } // when response.responseCode == 1
            });
              //  }
            }
            });

                    function resetElements(){
                    $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                    }

               <?php if (isset($search_status_id) && $search_status_id > 0) { ?>
                var _token = $('input[name="_token"]').val();
                        $.ajax({
                        url: base_url + '/dashboard/search-result',
                                type: 'post',
                                data: {
                                _token: _token,
                                        status_id:<?php echo $search_status_id; ?>,
                                },
                                dataType: 'json',
                                success: function(response) {
                                // success
                                if (response.responseCode == 1) {
                                $('table.resultTable tbody').html(response.data);
                                } else {
                                }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                                },
                                beforeSend: function(xhr) {
                                console.log('before send');
                                },
                                complete: function() {
                                //completed
                                }
                        });
            <?php } ?>

            $("#search_app").on('click', function() {
            $('.selectall').addClass("hidden");
                    var _token = $('input[name="_token"]').val();
                    var tracking_number = $('#tracking_number').val();
                    var passport_number = $('#passport_number').val();
                    var applicant_name = $('#applicant_name').val();
                    var nationality = $('#nationality').val();
                    var status_id = $('#modal_status_id').val();
                    var selected = false;
                    $.ajax({
                    url: base_url + '/export-permit/search-result',
                            type: 'post',
                            data: {
                            _token: _token,
                                    tracking_number: tracking_number,
                                    passport_number: passport_number,
                                    applicant_name: applicant_name,
                                    nationality: nationality,
                                    status_id:status_id,
                            },
                            dataType: 'json',
                            success: function(response) {
                            console.log(response);
                                    // success
                                    if (response.responseCode == 1) {
//                            $('table.resultTable tbody').html(response.data);
                                    $('#modal-close').trigger('click');
                            } else {
                            }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                            },
                            beforeSend: function(xhr) {
                            console.log('before send');
                            },
                            complete: function() {
                            //completed
                            }
                    });
            });
}); // end of document.change status_id
</script>
@endif

@endsection <!--- footer-script--->

