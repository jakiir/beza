{!! Form::open(array('url' => '#','method' => 'patch', 'class' => 'form-horizontal', 'id'=>'search_form')) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel">Advance Search</h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tracking_number" class="col-md-4">Tracking No: </label>
                    <div class="col-md-7">
                        <input class="form-control" placeholder="Tracking Number" name="tracking_number" type="text" id="tracking_number"
                               maxlength="100"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="applicant_name" class="col-md-4">Applicant Name: </label>
                    <div class="col-md-8">
                        <input class="form-control" placeholder="Name" name="applicant_name" type="text" id="applicant_name"
                               maxlength="100"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nationality" class="col-md-4">Nationality: </label>
                    <div class="col-md-7">
                        {!! Form::select('nationality', $nationality, '', $attributes = array('class'=>'form-control',
                        'placeholder' => 'Select nationality', 'id'=>"nationality")) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="passport_number" class="col-md-4">Passport No: </label>
                    <div class="col-md-8">
                        <input class="form-control" placeholder="Passport Number" name="passport_number" type="text" id="passport_number"
                               maxlength="100"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="organization" class="col-md-4">Status: </label>
                    <div class="col-md-8">
                        {!! Form::select('modal_status_id', $statusList, '', array('class'=>'form-control',
                        'placeholder' => 'Select Status', 'id'=>"modal_status_id")) !!}

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="modal-close">Close</button>
    <button type="button" value="Done" class="btn btn-primary" id="search_app">search</button>
</div>
{!! Form::close() !!}

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

<script type="text/javascript">
    function openMdoal(link, div) {
        $(link).on('click', function (e) {
            e.preventDefault();
            $(div).html('<div style="text-align:center;"><img src="../img/spinner.gif"/><br/><br/><h3 class="text-primary">Loading Form...</h3></div>');
            $(div).load(
                    $(this).attr('href'),
                    function (response, status, xhr) {
                        if (status === 'error') {
                            $(div).html('<p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText + '</p>');
                        }
                        return this;
                    }
            );
        });
    }

    $(document).ready(function () {
        openMdoal('.addProjectModal', '#frmAddProject');
        var today = new Date();
        var yyyy = today.getFullYear();
        var mm = today.getMonth() + 1;
        if (mm < 10) {
            mm = '0' + mm;
        }
        var dd = today.getDate();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var minDate = yyyy + "-" + mm + "-" + dd;
        $('.datepicker').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD',
            minDate: minDate
        });
    });

    $("#search_app").on('click', function () {
        $('.selectall').addClass("hidden");
        var _token = $('input[name="_token"]').val();
        var tracking_number = $('#tracking_number').val();
        var passport_number = $('#passport_number').val();
        var applicant_name = $('#applicant_name').val();
        var organization = $('#organization').val();
        var nationality = $('#nationality').val();
        var status_id = $('#modal_status_id').val();
        var sb_status_id = $('#sb_status_id').val();
        var nsi_status_id = $('#nsi_status_id').val();

        $.ajax({
            url: base_url + '/project-clearance/search-result',
            type: 'post',
            data: {
                _token: _token,
                tracking_number: tracking_number,
                passport_number: passport_number,
                organization: organization,
                applicant_name: applicant_name,
                nationality: nationality,
                status_id: status_id,
                sb_status_id: sb_status_id,
                nsi_status_id: nsi_status_id,
            },
            dataType: 'json',
            success: function (response) {
                if (response.responseCode == 1) {  // success
                    $('table.resultTable tbody').html(response.data);
                    $('#modal-close').trigger('click');
                } else {
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            beforeSend: function (xhr) {
                console.log('before send');
            },
            complete: function () {
                //completed
            }
        });
    });

    $("#modal_service_id").on('change', function () {
        var _token = $('input[name="_token"]').val();
        var service_id = $('#modal_service_id').val();
        $.ajax({
            url: base_url + '/project-clearance/service-wise-result',
            type: 'post',
            data: {
                _token: _token,
                service_id: service_id,
            },
            dataType: 'json',
            success: function (response) {
                // success
                var option = '';
                option += '<option value="">Select Status</option>';
                $.each(response, function (id, value) {
                    option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                });
                $("#modal_status_id").html(option);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            beforeSend: function (xhr) {
                console.log('before send');
            },
            complete: function () {
                //completed
            }
        });
    });

</script>
@section('footer-script')
@include('partials.datatable-scripts')
<script>
    $(document).ready(
            function () {
                $("#search_form").validate({
                    errorPlacement: function () {
                        return false;
                    }
                });
            });
</script>
