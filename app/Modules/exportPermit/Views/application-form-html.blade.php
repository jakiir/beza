@extends('layouts.pdfGen')
@section('content')
@include('partials.messages')
<section class="content" id="projectClearanceForm">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;margin-bottom:10px;">
                        <img src="assets/images/logo_beza_single.png"/><br/>
                        BEZA::Bangladesh Economic Zones Authority<br/>
                        Application for Export Permit
                    </div>
                </div>
                <div class="panel panel-red" id="inputForm">
                    <div class="panel-heading">Application for Export Permit </div>
                    <div class="panel-body" style="margin:6px;">
                        <table width="100%">
                            <tr>
                                <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Tracking no. : </strong><span style="font-size: 10px;">{{ $process_data->track_no  }}</span></td>
                                <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Date of Submission: </strong> <span style="font-size: 10px;">{{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }}</span></td>
                                <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Status : </strong> 
                                    <span style="font-size: 10px;">
                                        @if(isset($form_data) && $form_data->status_id == -1) Draft
                                        @else {!! isset($form_data->status_id) ? $statusArray[$form_data->status_id] : ''!!}
                                        @endif</span></td>
                                @if($process_data->desk_id != 0)
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Desk :</strong> <span style="font-size: 10px;">{{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }}</span></td>
                                @else
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Current Desk :</strong> <span style="font-size: 10px;">Applicant</span></td>
                                @endif
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    <?php if ( isset($form_data->status_id) && $form_data->status_id == 23 && isset($form_data->certificate)) { ?>
                                    <a style="font-size: 10px;" href="{{ url($form_data->certificate) }}"
                                       title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                    <?php } ?>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>General Information</strong></div>
                            <div class="panel-body">

                                <div class="col-md-12">
                                    <table width="100%" cellpadding="10">
                                        <tr>
                                            <td style="padding: 5px;" width="25%"><strong style="font-size: 10px;">Applicant Name : </strong></td>
                                            <td style="padding: 5px;" width="25%"><span style="font-size: 10px;">{{ (isset($form_data->applicant_name)?$form_data->applicant_name:'N/A') }}</span></td>
                                            <td style="padding: 5px;" width="25%"><strong style="font-size: 10px;"></strong></td>
                                            <td style="padding: 5px;" width="25%"><span style="font-size: 10px;"></span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;" width="25%"><strong style="font-size: 10px;">Export Permit Type : </strong></td>
                                            <td style="padding: 5px;" width="25%"><span style="font-size: 10px;">
                                                    {{ (isset($form_data->permit_type) && $form_data->permit_type != 0)? $ip_permit_type[$form_data->permit_type] : 'N/A' }}</span></td>
                                            <td style="padding: 5px;" width="25%"><strong style="font-size: 10px;">Type of Carrier : </strong></td>
                                            <td style="padding: 5px;" width="25%"><span style="font-size: 10px;">
                                                    {{ (isset($form_data->carrier_type)  && $form_data->carrier_type != 0) ? $carrier_type[$form_data->carrier_type] : 'N/A' }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Economic Zone : </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($economicZone->zone)?$economicZone->zone : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Name of Developer : </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->developer_name) ? $form_data->developer_name : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Undertaking No.: </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->undertaking_no) ? $form_data->undertaking_no : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Undertaking Date : </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->undertaking_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($form_data->undertaking_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Invoice/Vendor Reference <br/> No. :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->invoice_ref_no) ? $form_data->invoice_ref_no : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Invoice/Vendor Reference Date :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->invoice_ref_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($form_data->invoice_ref_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">CM Value :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->cm_select)? $currencies[$form_data->cm_select]:'N/A') }}</span><br/><span style="font-size: 10px;">{{ (isset($form_data->cm_value) ? $form_data->cm_value : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Invoice Value (FoB / CIF / CFR / C&F) :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->invoice_select) ? $currencies[$form_data->invoice_select] : 'N/A') }}</span> <br/> <span style="font-size: 10px;">{{ (isset($form_data->invoice_value) ? $form_data->invoice_value : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Port of destination :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                    {{ (isset($form_data->destination_port) && $form_data->destination_port != "") ?
                                                        $ports[$form_data->destination_port] : 'N/A' }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Place of load :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                    {{ (isset($form_data->export_port) ? $form_data->export_port : 'N/A') }}</span></td>
                                        </tr>                                                     
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Destination Country :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->destination_country) ? $countries[$form_data->destination_country] : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Destination Zone :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->destination_zone) ? $countries[$form_data->destination_zone] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Consignee Name :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->consignee_name) ? $form_data->consignee_name : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Consignee Address :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->consignee_address) ? $form_data->consignee_address : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Remarks :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($form_data->remark) ? $form_data->remark : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;"></strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;"></span></td>
                                        </tr>

                                    </table>
                                </div>
                            </div> <!--/panel-body-->
                        </div> <!--/panel-->

                        @if(isset($meterials) && count($meterials) > 0)
                            <?php $inc = 0; ?>
                        <div id="templateImdFullPar" style="margin: 3px;">
                            @foreach($meterials as $eachmeterials)
                                <?php $eachEpLcInfo  = $epLcInfos[$inc];?>
                            <div class="panel panel-black templateImdFull" id="templateImdFull">
                                <div class="panel-heading"><strong><i class="fa fa-list"></i> Export Details &amp; L/C Information Group</strong>
                                        <span class="pull-right"></span>
                                </div>
                                <div class="panel-body" style="margin: 4px;">
                                    <div id="edidMoreInfoMd0" style="margin: 3px;">
                                        <div class="panel panel-primary templateImd" id="templateImd0">
                                            <div class="panel-heading">
                                                <span class=""><strong>Export Details</strong></span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <table width="100%" cellpadding="10">
                                                        <tr>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">Product description :</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($eachmeterials->product_description) ? $eachmeterials->product_description : 'N/A') }}</span></td>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">FOB / CNF / CIF / CRF / X-Factory / DDU / Replacement / Free of Cost value:</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{(isset($currencies[$eachmeterials->fob_usd]) ? $currencies[$eachmeterials->fob_usd] : 'N/A')}}</span><br/><span style="font-size: 10px;">{{(isset($eachmeterials->fob_usd_value) ? $eachmeterials->fob_usd_value : 'N/A')}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">HS Code :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{(isset($eachmeterials->hs_code) ? $eachmeterials->hs_code : '')}}</span><br><span style="font-size: 10px;">{{(isset($eachmeterials->hs_product) ? $eachmeterials->hs_product : '')}}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;"></strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Quantity :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{(isset($eachmeterials->mat_quantity) ? $eachmeterials->mat_quantity : '')}}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Unit of Quantity :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($quantity_unit[$eachmeterials->mat_quantity_unit]) ? $quantity_unit[$eachmeterials->mat_quantity_unit] : 'N/A') }}</span></td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div> <!--/panel-body-->
                                </div> <!--/panel-->
                            </div>
                                    <div id="edidMoreInfoPo0">
                                        <div class="panel panel-primary templateTtPo" id="templateTtPo" style="margin: 3px;">
                                            <div class="panel-heading"><strong>TT / P.O/ SC/ CM / L/C Information</strong></div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <table width="100%" cellpadding="10">
                                                        <tr>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">TT / P.O/ SC/ CMT/ L/C <br/> No. :</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{(isset($eachEpLcInfo->tt_no) ? $eachEpLcInfo->tt_no : 'N/A')}}</span></td>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">TT / P.O/ SC/ CMT/ L/C <br/> Value :</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{(isset($eachEpLcInfo->tt_value) ? $eachEpLcInfo->tt_value : 'N/A')}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Issuing Bank :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                                    {{ (isset($eachEpLcInfo->bank_id) && $eachEpLcInfo->bank_id != 0)? $banks[$eachEpLcInfo->bank_id]:'N/A' }}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Issue Date :</strong></td>
                                                            <?php
                                                            if (isset($allRequestVal)) {
                                                                $issueDate = $eachEpLcInfo->bank_invoice_date;
                                                            } else {
                                                                $issueDate = (isset($eachEpLcInfo->bank_invoice_date) ?
                                                                        App\Libraries\CommonFunction::changeDateFormat(substr($eachEpLcInfo->bank_invoice_date, 0, 10)) : 'N/A');
                                                            }
                                                            ?>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ $issueDate }}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Type :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (isset($eachEpLcInfo->bank_type)? $bankTypes[$eachEpLcInfo->bank_type]:'N/A') }}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Expiry Date :</strong></td>
                                                            <?php
                                                            if (isset($allRequestVal)) {
                                                                $expiry_date = $eachEpLcInfo->expiry_date;
                                                            } else {
                                                                $expiry_date = (isset($eachEpLcInfo->expiry_date) ? App\Libraries\CommonFunction::changeDateFormat(substr($eachEpLcInfo->expiry_date, 0, 10)) : 'N/A');
                                                            }
                                                            ?>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ $expiry_date }}</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div><!--panel-body-->
                                        </div> <!--/panel-->
                                    </div>
                                </div> <!--/panel-black body close-->
                            </div> <!--/panel-black close-->
                                <?php $inc++; ?>
                                @endforeach                               
                        </div>
                        @endif {{-- checking isset materials --}}





                        <div id="moreInfoImd" class="clear"></div>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>Required  Documents for attachment</strong></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <table width="100%" class="table table-bordered">
                                        <tr>
                                            <td width="10%" style="padding: 5px;"><strong style="font-size:10px;">No.</strong></td>
                                            <td width="60%" style="padding: 5px;"><strong style="font-size:10px;">Required Attachments</strong></td>
                                            <td width="30%" style="padding: 5px;"><strong style="font-size:10px;">Attached PDF file</strong></td>
                                        </tr>
                                        <?php $i = 1; ?>
                                        @foreach($document as $row)
                                        <tr>
                                            <td style="padding: 5px;" ><span style="font-size:10px;">{{ $i }}</span></td>
                                            <td style="padding: 5px;" ><span style="font-size:10px;">{{ $row->doc_name }}</span></td>
                                            <td style="padding: 5px;" >
                                                <span style="font-size:10px;">
                                                    <a target="_blank" class="documentUrl" href="{{URL::to('/uploads/'.(isset($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}"><?php if(isset($clrDocuments[$row->doc_id]['file'])){$file_name = explode('/',$clrDocuments[$row->doc_id]['file']); echo end($file_name);} ?> </a>
                                                </span>
                                            </td>
                                        </tr>
                                            <?php $i++; ?>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        @if(isset($form_data->status_id) && $form_data->status_id > 20)
                            <div style="margin-top:6px;">
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding: 20px;" class="alert alert-info">
                                            The goods specified in the invoice(s) are eligible for exemption for the whole of the customs duty, sales tax etc. vide S.R.O. No. 209-LAW/2015/46/Customs dated July 1,2015 issued by the Internal Resources Division, Ministry of Finance, Dhaka
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding: 20px;" class="alert alert-danger">
                                            <b>
                                                THIS PERMIT IS VALID FOR 30 (THIRTY) DAYS FROM THE DATE OF ISSUE
                                            </b>
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding: 20px;" class="alert alert-warning">
                                            <b>
                                                THIS IS COMPUTER GENERATED PERMIT. SO SIGNATURE IS NOT REQUIRED
                                            </b>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        @endif
                    </div> <!--/ main red panel with margin-->
                </div>

            </div>
        </div>

        <!-- /.form end -->


    </div>
</section>



@endsection <!--- footer-script--->

