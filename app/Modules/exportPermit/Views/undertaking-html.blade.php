<table style="width: 100%" align="center">
    <tr>
        <td colspan="3"><b>{{ ucwords($heading) }}</b></td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td colspan="3">Dated : {{$dateNow}}</td>
    </tr>
    <tr>
        <td colspan="3">Ref: UNDERTAKING NO : {{$track_no}}</td>
    </tr>
    @if($pdfType != 'undertaking')
    <tr>
        <td colspan="3">EP Number: {{ $epNo }}</td>
    </tr>
    @endif
    <tr>
        <td colspan="3">Managing Director,</td>
    </tr>
    <tr>
        <td colspan="3">{{$economicZones->name}}</td>
    </tr>
    <tr>
        <td colspan="3">{{$economicZones->upazilla}}, {{$economicZones->district}}.</td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td colspan="3"><strong>Subject : Undertaking and request for issuing {{ ucwords($permit_type) }} Permit</strong></td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td colspan="3">Dear Sir,</td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr>
        <td colspan="3">
            Please find enclosed herewith the invoice no: <strong>{{$invoice_no}}</strong>,
            dated: {{$invoice_date}}, of
            <strong>{{$materials}}</strong> being imported for using in our factory at
            {{$economicZones->name}}.<br/>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            In this connection we certify that the items being imported against the invoice enclosed herewith are new and having been
            procured at most competitive prices.
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Further we declare that the imported goods shall be installed or used for the production, manufacturing, processing, repair
            or re-fitting of goods within the zone for export outside the Bangladesh and shall not, in any case without the written approval
            of  the BEZA authority be sold or otherwise disposed of in any manner for consumption outside the zone.
        </td>
    </tr>
    <tr>
        <td colspan="3">
            However, if the imported goods are found to use contrary to above statement, we undertake to do the needful as may be
            directed by the authority.
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Therefore, you are requested to issue the import permit as per enclosed invoice.
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    @if($pdfType == 'approval')
    <tr>
        <td colspan="2">
            @if($pdfType == 'approval')
            <p>
                <img src="{{ $url }}" alt="QR code" />
            </p>
            <br/>
            @endif {{-- checking approval letter --}}

            Enclosed (dated) :
            @if(!empty($ep_documents))
            <table>
                @foreach($ep_documents as $key=>$doc)
                <tr>
                    <td>{{$key+1 }}.</td>
                    <td>{{$doc->doc_name }},</td>
                    <td>({{ !empty($doc->created_at) ? date( "d-M-Y", strtotime($doc->created_at) ) : 'N/A'}})</td>
                </tr>
                @endforeach
            </table>
            @endif

        </td>
        <td colspan="1" style="text-align: center; vertical-align:top;">
            Thank you<br/>
            Sincerely Yours<br/>
            @if($signature != null)
            <img src="{{$signature}}" alt="(signature)" width="150px"/>
            @endif
            <br/><b>-------------------------------</b><br/>

            @if($pdfType == 'approval')
            (<strong>{{  $approver }} </strong>)<br/>
            Joint Secretary<br/>
            Secretary, BEZA Executive Board<br/>
            Bangladesh Economic Zones Authority (BEZA) <br/>

            @else
            <b>Authorized Signature</b><br/>
            {{$applicant->user_full_name}}<br/>
            {{$company_name}}<br/>

            @endif {{-- checking approval letter --}}
        </td>
    </tr>
        @endif


    @if($pdfType == 'undertaking')
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td colspan="3" style="vertical-align:top;">
                Thank you<br/>
                Sincerely Yours<br/>
                @if($signature != null)
                    <img src="{{$signature}}" alt="(signature)" width="150px"/>
                @endif
                <br/><b>-------------------------------</b><br/>
                <b>Authorized Signature</b><br/>
                {{$applicant->user_full_name}}<br/>
                {{$company_name}}<br/>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td colspan="3">
                Enclosed (dated) :
                @if(!empty($ep_documents))
                    <table>
                        @foreach($ep_documents as $key=>$doc)
                            <tr>
                                <td>{{$key+1 }}.</td>
                                <td>{{$doc->doc_name }},</td>
                                <td>({{ !empty($doc->created_at) ? date( "d-M-Y  h:m:s a", strtotime($doc->created_at) ) : 'N/A'}})</td>
                            </tr>
                        @endforeach
                    </table>
                @endif

            </td>
        </tr>
    @endif

</table>