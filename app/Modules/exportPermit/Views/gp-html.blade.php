<table width="100%" align="center">
    <tr>
        <td colspan="2" style="text-align:center">
            <span>{{$economicZones->name}}</span><br/>
            <span>{{$economicZones->upazilla}}, {{$economicZones->district}}</span><br/>
            <span style="font-size: 14px"><strong>GATE PASS-OUT</strong></span><br/>
            <span>Export Permit ({{$permit_type}})</span><br/>
        </td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td colspan="2">SI No.- {{$track_no}}</td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td colspan="2" style="text-align:center">
            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG( $ip_no, 'C39')}}" alt="barcode" width="250"/>
        </td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td colspan="2">
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <span>Gate Pass Print 
                            <strong> Date </strong>: {{ $dateNow }}
                            <strong>Time: </strong>{{$timeNow}}
                        </span><br/><br/>
                        <strong>1. Name of Industry: </strong>{{$company_name}}
                    </td>
                </tr>
                <tr>
                    <td><strong>Permit No</strong>: {{$ip_no}}</td>
                    <td><strong>Date of Gatepass Issue</strong>: {{ $dateNow }} {{$timeNow}} </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong>2. Description of Goods - HS Code (Qty) </strong>:<br/>
                        @foreach($passedMaterials as $pass_mat)
                        {{ $pass_mat->product_description }} 
                        {{ $pass_mat->hs_code }} 
                        {{ $pass_mat->hs_product }} 
                        ({{ $pass_mat->passed_quantity }}
                        {{ $pass_mat->unit_name }}), 
                        @endforeach
                    </td>
                </tr>
<!--                <tr>
                    <td><strong>3. Category of Vehicle :</strong> Covered Van</td>
                    <td>No- 14-0267</td>
                </tr>-->
                <tr>
                    <td colspan="2"><strong>Name of Concerned Agency: </strong>self
                        <br/>Out is permitted
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align: center; vertical-align:top;">
            <img src="{{$signature}}" width="90" align="Signature">
        </td>
    </tr>
    <tr>
        <td style="text-align: center; vertical-align:top;">
            <span>--------------------------------------------------------------</span><br/>
            <span><strong>(Security Inspector/ Asst.Security Officer)</strong></span>
        </td>
        <td  style="text-align: center; vertical-align:top;">
            <span>--------------------------------------------------------------</span><br/>
            <span><strong>Officer in Duty :</strong> {{$security_name }}</span><br/>
            <span><strong>Designation:</strong> {{$designation}}</span><br/>
            <span><strong>{{$economicZones->name}} Security Gate</strong></span><br/>
        </td>
    </tr>

</table>
