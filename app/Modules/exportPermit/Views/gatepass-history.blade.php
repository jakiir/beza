<div class="row">
    <div class="col-md-12">
        <div class="panel panel-orange">
            <div class="panel-heading"><strong>Past Gatepass History</strong></div>
            <div class="panel-body">

                <table class="table table-bordered table-striped table-responsive" id="gpHistlist">
                    <thead>
                        <tr class="text-center">
                            <th class="text-center"  width="3%">#</th>
                            <th class="text-center">IP ID</th>
                            <th class="text-center">Item</th>
                            <th class="text-center">Unit</th>
                            <th class="text-center" width="7%">Original Quantity</th>
                            <th class="text-center" width="7%">Remaining Quantity</th>
                            <th class="text-center" width="7%">Passed Quantity</th>
                            <th class="text-center">GatePass No</th>
                            <th class="text-center">GatePass</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($gpHistory as $hist)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $hist->tracking_no }}</td>
                            <td>{{ $hist->product_description }}</td>
                            <td>{{ $hist->unit_name }}</td>
                            <td>{{ $hist->mat_quantity }}</td>
                            <td>{{ $hist->mat_remaining_quantity }}</td>
                            <td>{{ $hist->passed_quantity }}</td>
                            <td>{{ $hist->gatepass_no }}</td>
                            <td>
                                <a class="btn btn-xs btn-warning show-in-view" href="{{ url($hist->generated_gatepass) }}" title="View Gatepass" 
                                   target="_blank"><strong>GatePass</strong></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>.
            </div>
        </div>
    </div>
</div>