@extends('layouts.pdfGen')
@section('content')
<section class="content" id="projectClearanceForm">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="" id="inputForm" style="text-align: center;">
                    <div class="" style="background-color: red; color: whitesmoke;">
                        <strong>EXPORT PERMIT</strong>
                    </div>
                    <div class="panel-body" style="margin:6px;">

                        <section class="content-header">
                            <table width="100%">
                                <tr>
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Tracking no. : </strong><span style="font-size: 10px;">{{ $process_data->track_no  }}</span></td>
                                    <td style="padding:5px; font-size:10px"><strong style="font-size: 10px;">Date of Issue : </strong> <span style="font-size: 10px;">{{ $certificate_issue_date  }}</span></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px;">
                                        <?php if ($form_data->status_id == 23 && !empty($form_data->certificate)) { ?>
                                        <a style="font-size: 10px;" href="{{ url($form_data->certificate) }}"
                                           title="Download Approval Letter" target="_blank"> <img width="10" height="10" src="assets/images/pdf.png" alt="Download Certificate" /> <b>Download Certificate</b></a>
                                        <?php } ?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </section>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>General Information</strong></div>
                            <div class="panel-body">

                                <div class="col-md-12">
                                    <table width="100%" cellpadding="10">
                                            <tr>
                                                <td style="padding:5px;" width="25%"><strong style="font-size:10px;">Name of Company : </strong></td>
                                                <td style="padding:5px;" width="25%"><span style="font-size:10px;">{{ (!empty($projectClearanceData->proposed_name)?$projectClearanceData->proposed_name:'N/A')}}</span></td>
                                                <td style="padding:5px;" width="25%"><strong style="font-size:10px;"></strong></td>
                                                <td style="padding:5px;" width="25%"><span style="font-size:10px;"></span></td>
                                            </tr>
                                        <tr>
                                            <td style="padding: 5px;" width="25%"><strong style="font-size: 10px;">Export Permit Type : </strong></td>
                                            <td style="padding: 5px;" width="25%"><span style="font-size: 10px;">{{ (!empty($form_data->permit_type))? $ep_permit_type[$form_data->permit_type] : 'N/A' }}</span></td>
                                            <td style="padding: 5px;" width="25%"><strong style="font-size: 10px;">Type of Carrier : </strong></td>
                                            <td style="padding: 5px;" width="25%"><span style="font-size: 10px;">{{ (!empty($carrier_type[$form_data->carrier_type]) ? $carrier_type[$form_data->carrier_type] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Economic Zone : </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($zoneType[$form_data->zone_type]) ? $zoneType[$form_data->zone_type] : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Name of Developer : </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->developer_name) ? $form_data->developer_name : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Undertaking No.: </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->undertaking_no) ? $form_data->undertaking_no : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Undertaking Date : </strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->undertaking_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($form_data->undertaking_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Invoice/Vendor Reference <br/> No. :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->invoice_ref_no) ? $form_data->invoice_ref_no : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Invoice/Vendor Reference Date :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->invoice_ref_date) ?
                                                        App\Libraries\CommonFunction::changeDateFormat(substr($form_data->invoice_ref_date, 0, 10)) : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">CM Value :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->cm_select)?$currencies[$form_data->cm_select]:'N/A') }}</span><br/><span style="font-size: 10px;">{{ (!empty($form_data->cm_value) ? $form_data->cm_value : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Invoice Value (FoB / CIF / CFR / C&F) :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                    {{ (!empty($form_data->invoice_select) ? $currencies[$form_data->invoice_select] : 'N/A') }}</span> <br/> 
                                                <span style="font-size: 10px;">{{ (!empty($form_data->invoice_value) ? $form_data->invoice_value : 'N/A') }}
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Port of destination :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->destination_port) ? $ports[$form_data->destination_port] : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Place of load :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->export_port) ? $form_data->export_port : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Destination Country :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->destination_country) ? $countries[$form_data->destination_country] : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Destination Zone :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->destination_zone) ? $countries[$form_data->destination_zone] : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Consignee Name :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->consignee_name) ? $form_data->consignee_name : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Consignee Address :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->consignee_address) ? $form_data->consignee_address : 'N/A') }}</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Remarks :</strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($form_data->remark) ? $form_data->remark : 'N/A') }}</span></td>
                                            <td style="padding: 5px;"><strong style="font-size: 10px;"></strong></td>
                                            <td style="padding: 5px;"><span style="font-size: 10px;"></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div> <!--/panel-body-->
                        </div> <!--/panel-->

                        @if(isset($meterials) && count($meterials) > 0)
                        <?php $inc = 0; ?>
                        <div id="templateImdFullPar" style="margin: 3px;">
                            @foreach($meterials as $eachmeterials)
                            <?php $eachEpLcInfo = $epLcInfos[$inc]; ?>
                            <div class="panel panel-black templateImdFull" id="templateImdFull" style="">
                                <div class="panel-heading"><strong><i class="fa fa-list"></i> Export Details &amp; L/C Information Group</strong>
                                    <span class="pull-right"></span>
                                </div>
                                <div class="panel-body" style="margin: 4px;">
                                    <div id="edidMoreInfoMd0" style="margin: 3px;">
                                        <div class="panel panel-primary templateImd" id="templateImd0">
                                            <div class="panel-heading">
                                                <span class=""><strong>Export Details</strong></span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">Product description :</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($eachmeterials->product_description) ? $eachmeterials->product_description : 'N/A') }}</span></td>
                                                            <td width="25%" style="padding: 5px; text">
                                                                <strong style="font-size: 10px;">
                                                                    FOB / CNF / CIF / CRF / X-Factory / DDU / Replacement / Free of Cost value:</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">
                                                                    {{ (!empty($currencies[$eachmeterials->fob_usd]) ?  $currencies[$eachmeterials->fob_usd] : 'N/A') }} 
                                                                    {{(!empty($eachmeterials->fob_usd_value) ? $eachmeterials->fob_usd_value : 'N/A')}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">HS Code :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                                    {{(!empty($eachmeterials->hs_code) ? $eachmeterials->hs_code : '')}} <br/>
                                                                    {{(!empty($eachmeterials->hs_product) ? $eachmeterials->hs_product : '')}}
                                                                </span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;"></strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Quantity :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                                    {{(!empty($eachmeterials->mat_quantity) ? $eachmeterials->mat_quantity : '')}}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Unit of Quantity :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">
                                                                    {{ (!empty($quantity_unit[$eachmeterials->mat_quantity_unit]) ? 
                                                                        $quantity_unit[$eachmeterials->mat_quantity_unit] : 'N/A') }}</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div> <!--/panel-body-->
                                        </div> <!--/panel-->
                                    </div>

                                    <div id="edidMoreInfoPo0">
                                        <div class="panel panel-primary templateTtPo" id="templateTtPo" style="margin: 3px;">
                                            <div class="panel-heading"><strong>TT / P.O/ SC/ CM / L/C Information</strong></div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <table width="100%" cellpadding="10">
                                                        <tr>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">TT / P.O/ SC/ CMT/ L/C <br/> No. :</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{(!empty($eachEpLcInfo->tt_no) ? $eachEpLcInfo->tt_no : 'N/A')}}</span></td>
                                                            <td width="25%" style="padding: 5px;"><strong style="font-size: 10px;">TT / P.O/ SC/ CMT/ L/C <br/> Value :</strong></td>
                                                            <td width="25%" style="padding: 5px;"><span style="font-size: 10px;">{{(!empty($eachEpLcInfo->tt_value) ? $eachEpLcInfo->tt_value : 'N/A')}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Issuing Bank :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;"> {{ (!empty($eachEpLcInfo->bank_id)? $banks[$eachEpLcInfo->bank_id]:'N/A') }}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Issue Date :</strong></td>
                                                            <?php
                                                            if (!empty($allRequestVal)) {
                                                                $issueDate = $eachEpLcInfo->bank_invoice_date;
                                                            } else {
                                                                $issueDate = (!empty($eachEpLcInfo->bank_invoice_date) ? App\Libraries\CommonFunction::changeDateFormat(substr($eachEpLcInfo->bank_invoice_date, 0, 10)) : 'N/A');
                                                            }
                                                            ?>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ $issueDate }}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Type :</strong></td>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ (!empty($eachEpLcInfo->bank_type)? $bankTypes[$eachEpLcInfo->bank_type]:'N/A') }}</span></td>
                                                            <td style="padding: 5px;"><strong style="font-size: 10px;">Expiry Date :</strong></td>
                                                            <?php
                                                            if (!empty($allRequestVal)) {
                                                                $expiry_date = $eachEpLcInfo->expiry_date;
                                                            } else {
                                                                $expiry_date = (!empty($eachEpLcInfo->expiry_date) ? App\Libraries\CommonFunction::changeDateFormat(substr($eachEpLcInfo->expiry_date, 0, 10)) : 'N/A');
                                                            }
                                                            ?>
                                                            <td style="padding: 5px;"><span style="font-size: 10px;">{{ $expiry_date }}</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div><!--panel-body-->
                                        </div> <!--/panel-->
                                    </div>
                                </div> <!--/panel-black body close-->
                            </div> <!--/panel-black close-->
                            @if($inc%2 == 0)
                                <pagebreak />
                            @endif
                            <?php $inc++; ?>
                            @endforeach
                            @endif
                        </div>

                        <div id="moreInfoImd" class="clear"></div>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>Uploaded Supporting Documents</strong></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <table width="100%" class="table table-bordered">
                                        <tr>
                                            <td style="padding:5px;" width="5%"><strong style="font-size:10px;">No</strong></td>
                                            <td style="padding:5px;" width="40%"><strong style="font-size:10px;">Document Type</strong></td>
                                            <td style="padding:5px;" width="5%"><strong style="font-size:10px;"><img width="10" height="10" src="assets/images/attachment.png"/></strong></td>
                                            <td style="padding:5px;" width="5%"><strong style="font-size:10px;">No</strong></td>
                                            <td style="padding:5px;" width="40%"><strong style="font-size:10px;">Document Type</strong></td>
                                            <td style="padding:5px;" width="5%"><strong style="font-size:10px;"><img width="10" height="10" src="assets/images/attachment.png"/></strong></td>
                                        </tr>
                                        <?php $i = 1; ?>
                                        <tr>
                                            @foreach($document as $row)
                                            @if(!empty($clrDocuments[$row->doc_id]['file']))
                                            <td style="padding: 5px;" ><span style="font-size:10px;">{{ $i }}</span></td>
                                            <td style="padding: 5px;" ><span style="font-size:10px;">{{ $row->doc_name }}</span></td>
                                            <td style="padding: 5px;" >
                                                <span style="font-size:10px;">
                                                    @if(!empty($clrDocuments[$row->doc_id]['file']))
                                                    <a href="{{URL::to('/uploads/'.(!empty($clrDocuments[$row->doc_id]['file']) ? $clrDocuments[$row->doc_id]['file'] : ''))}}" target="_blank"><img width="10" height="10" src="assets/images/pdf.png" alt="{{ $clrDocuments[$row->doc_id]['file'] }}" /></a>
                                                    @endif
                                                </span>
                                            </td>
                                            <?php if ($i % 2 == 0) echo '</tr><tr>'; ?>
                                            <?php $i++; ?>
                                            @endif {{-- checking not empty --}}
                                            @endforeach
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div> <!--/ main red panel with margin-->
                </div>

            </div>
        </div>

    </div>
</section>



@endsection <!--- footer-script--->

