<?php

namespace App\Modules\exportPermit\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\Company\Models\Company;
use App\Modules\Dashboard\Models\Services;
use App\Modules\apps\Models\docInfo;
use App\Modules\apps\Models\Document;
use App\Modules\Apps\Models\Status;
use App\Modules\exportPermit\Models\Apps;
use App\Modules\exportPermit\Models\EpGatePass;
use App\Modules\exportPermit\Models\ExportLcInfo;
use App\Modules\exportPermit\Models\ExportMaterial;
use App\Modules\exportPermit\Models\ExportPermit;
use App\Modules\importPermit\Models\CarrierType;
use App\Modules\importPermit\Models\GatePassHistory;
use App\Modules\importPermit\Models\PermitType;
use App\Modules\importPermit\Models\MaterialType;
use App\Modules\ProcessPath\Models\ProcessHistory;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Settings\Models\HsCodes;
use App\Modules\Settings\Models\Ports;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Settings\Models\Units;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\Users;
use App\Modules\workPermit\Models\Processlist;
use Carbon\Carbon;
use Encryption;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Milon\Barcode\DNS1D;
use mPDF;
use Session;

class exportPermitController extends Controller {

    protected $service_id;

    public function __construct() {
        // Globally declaring service ID
        $this->service_id = 5; // 5 is Export Permit
    }

    public function listEP($ind = 0, $zone = 0) {
        if (!ACL::getAccsessRight('exportPermit', 'V')) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            Session::put('sess_user_id', Auth::user()->id);

            $getList = Apps::getClearanceList();
            $appStatus = Status::where('service_id', $this->service_id)->get();
            $statusList = array();
            foreach ($appStatus as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
                $statusList[$v->status_id . 'color'] = $v->color;
            }
            $statusList['-1'] = "Draft";
            $statusList['-1' . 'color'] = '#AA0000';

            $desks = UserDesk::all();
            $deskList = array();
            foreach ($desks as $k => $v) {
                $deskList[$v->desk_id] = $v->desk_name;
            }

            //for advance search
            $validCompanies = Processlist::leftJoin('project_clearance as pc', 'pc.id', '=', 'process_list.record_id')
                            ->where('process_list.status_id', 23)
                            ->where('pc.status_id', 23)
                            ->orderBy('pc.applicant_name', 'ASC')
                            ->lists('pc.applicant_name', 'pc.tracking_number')->all();
            $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            if (Auth::user()->user_type == '9x909') { // 9x909 = customs
                $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')
                                ->whereIn('status_id', [21, 29, 30]) // regarding customs
                                ->lists('status_name', 'status_id')->all();
            } else if (Auth::user()->user_type == '3x303') { // 3x303 = security
                $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')
                                ->whereIn('status_id', [30, 31]) // regarding security
                                ->lists('status_name', 'status_id')->all();
            } else {
                $status = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
            }
            return view("exportPermit::list", compact('status', 'deskList', 'statusList', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id', 'validCompanies', 'economicZone', 'zone', 'ind'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appForm() {
        if (!ACL::getAccsessRight('exportPermit', 'A')) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $authUserId = CommonFunction::getUserId();

            $alreadyExistApplicant = '';

            /* Start of preventing user to apply with approved project clearance */
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();
            /* end of preventing user to apply with approved project clearance */
            if (!empty($projectClearanceData)) {
                $zoneType = $projectClearanceData->zone_type;
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
            } else {
                $zoneType = '';
                $economicZone = '';
            }


            $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            $ip_permit_type = PermitType::orderBy('name')->where('type', 2)->where('status', 1)->lists('name', 'id');
            $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
            $material_type = MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id');

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
            $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');

            $ports = Ports::orderBy('name')->lists('name', 'id');

            $banks = Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id');
            $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            $clrDocuments = [];

            $viewMode = 'off';
            $mode = 'A';
            return view("exportPermit::application-form", compact('ip_permit_type', 'carrier_type', 'countries', 'zoneType', 'economicZone', 'allEcoZones', 'currencies', 'mode', 'ports', 'material_type', 'quantity_unit', 'banks', 'bankTypes', 'document', 'alreadyExistApplicant', 'clrDocuments', 'viewMode')
            );
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormEdit($id) {
        $app_id = Encryption::decodeId($id);
        if (!ACL::getAccsessRight('exportPermit', 'E', $app_id)) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $authUserId = CommonFunction::getUserId();
            $alreadyExistApplicant = ExportPermit::where('id', $app_id)->where('service_id', $this->service_id)->first();
            if ($alreadyExistApplicant) {
                $alreadyExistProcesslist = Processlist::where('track_no', $alreadyExistApplicant->tracking_number)
                                ->where('service_id', $this->service_id)->first();
                if ($alreadyExistProcesslist) {
                    if ($alreadyExistProcesslist->status_id != '-1' && $alreadyExistProcesslist->status_id != 5) {
                        Session::flash('error', "You have no access right! Please contact system administration for more information.");
                        $list = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
                        return redirect($list);
                    }
                }
            }

            /* Start of preventing user to apply with approved project clearance */
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $authUserId)
                    ->first();
            /* end of preventing user to apply with approved project clearance */
            if (!empty($projectClearanceData)) {
                $zoneType = $projectClearanceData->zone_type;
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                                ->where('id', $projectClearanceData->eco_zone_id)->first();
            } else {
                $zoneType = '';
                $economicZone = '';
            }


            $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            $form_data = ExportPermit::where('id', $app_id)->first(['status_id', 'certificate']);
            if (!count($form_data) > 0) {
                session()->flash('error', 'Application data not found.');
                return redirect()->back();
            }

            $ip_permit_type = PermitType::orderBy('name')->where('type', 2)->where('status', 1)->lists('name', 'id');
            $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
            $material_type = MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id');

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
            $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $ports = Ports::orderBy('name')->lists('name', 'id');

            $banks = Bank::orderBy('name')->where('is_active', 1)->lists('name', 'id');
            $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            if ($alreadyExistApplicant) {
                $clr_document = Document::where('app_id', $alreadyExistApplicant->id)->get();
                $meterials = ExportMaterial::where('ip_id', $alreadyExistApplicant->id)->get();
                $epLcInfos = ExportLcInfo::where('ip_id', $alreadyExistApplicant->id)->get();
                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }
            } else {
                $clrDocuments = [];
            }
            $viewMode = 'off';
            $mode = 'E';
            return view("exportPermit::application-form", compact('ip_permit_type', 'carrier_type', 'countries', 'zoneType', 'currencies', 'mode', 'economicZone', 'material_type', 'quantity_unit', 'banks', 'bankTypes', 'document', 'alreadyExistApplicant', 'meterials', 'epLcInfos', 'clrDocuments', 'viewMode', 'ports')
            );
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appFormView($id) {
        if (!ACL::getAccsessRight('exportPermit', 'V')) {
            abort('400', "You have no access right! Please contact with system admin if you have any query.");
        }
        try {
            $app_id = Encryption::decodeId($id);

            $form_data = ExportPermit::where('id', $app_id)->where('service_id', $this->service_id)->first();
            if (!count($form_data) > 0) {
                session()->flash('error', 'Application data not found.');
                return redirect()->back();
            }
            $process_data = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();

            $submitHistory = ProcessHistory::where('record_id', $app_id)->where('process_id', $process_data->process_id)
                    ->where('status_id', 1) // 1 = submitted
                    ->first();
            $submissionDate = !empty($submitHistory) ? $submitHistory->updated_at : '';

            $statusId = $process_data->status_id;
            $deskId = $process_data->desk_id;
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE APS.service_id = $this->service_id
                        AND
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM export_permit_process_path APP 
                        WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";

            $statusList2 = \DB::select(DB::raw($sql));
            $statusList = array();
            foreach ($statusList2 as $k => $v) {
                $statusList[$v->status_id] = $v->status_name;
            }

            $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
//        breadcrumb end

            $loggedUserType = CommonFunction::getUserType();
            $alreadyExistApplicant = ExportPermit::leftJoin('project_clearance', 'project_clearance.created_by', '=', 'export_permit.created_by')
                            ->Where('project_clearance.status_id', 23)
                            ->where('export_permit.id', $app_id)->first(['export_permit.*', 'project_clearance.applicant_name']);

            /* Start of preventing user to apply with approved project clearance */
            $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                    ->where('process_list.service_id', 1) // 1 is for project Clearance
                    ->where('process_list.status_id', 23) // 23 is payment accepted
                    ->where('project_clearance.created_by', $form_data->created_by)
                    ->first();


            if ($loggedUserType != '5x505' && empty($projectClearanceData)) {
                Session::flash('error', "This user doesn't have a approved project clearance certificate attached to
                 his profile. To investigate further, please contact with system admin!");
                return redirect()->back();
            }
            /* end of preventing user to apply with approved project clearance */

            $zoneType = '';
            $economicZone = '';
            if ($projectClearanceData != null) {
                $zoneType = $projectClearanceData->zone_type;
                $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))->where('id', $projectClearanceData->eco_zone_id)->first();
            }

            $allEcoZones = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                            ->orderBy('zone')->lists('zone', 'id');

            $ip_permit_type = PermitType::orderBy('name')->where('type', 2)->where('status', 1)->lists('name', 'id');
            $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
            $material_type = MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id');

            $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
            $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
            $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
            $ports = Ports::orderBy('name')->lists('name', 'id');

            $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();
            $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

            $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

            if ($form_data) {
                $applicantInfo = Users::where('id', $form_data->created_by)->first(['authorization_file']);
                $meterials = ExportMaterial::where('ip_id', $form_data->id)->get();
                $epLcInfos = ExportLcInfo::where('ip_id', $form_data->id)->get();
                $clr_document = Document::where('app_id', $form_data->id)->get();

                foreach ($clr_document as $documents) {
                    $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                    $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                    $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
                }

                $gpMaterials = ExportMaterial::leftJoin('units', 'ep_materials.mat_quantity_unit', '=', 'units.id')
                        ->where('ip_id', $form_data->id)
                        ->get([ 'ep_materials.id as ip_mat_id', 'product_description', 'mat_quantity', 'mat_remaining_quantity', 'units.name as unit_name']);
                $remaining_quntities = ExportMaterial::where('ip_id', $form_data->id)->get(['mat_remaining_quantity']);
                $gpHistory = GatePassHistory::orderBy('ep_gatepass.id', 'desc')
                        ->leftJoin('ep_gatepass', 'gatepass_history.gatepass_id', '=', 'ep_gatepass.id')
                        ->leftJoin('ep_materials', 'ep_gatepass.ep_material_id', '=', 'ep_materials.id')
                        ->leftJoin('export_permit', 'gatepass_history.app_id', '=', 'export_permit.id')
                        ->leftJoin('units', 'ep_materials.mat_quantity_unit', '=', 'units.id')
                        ->where('gatepass_history.app_id', $form_data->id)
                        ->where('gatepass_history.service_id', $this->service_id)
                        ->get(['export_permit.tracking_number as tracking_no',
                    'product_description',
                    'units.name as unit_name',
                    'mat_quantity', 'mat_remaining_quantity', 'passed_quantity',
                    'ep_gatepass.gatepass_no as gatepass_no',
                    'generated_gatepass',
                ]);
            } else {
                $applicantInfo = [];
                $meterials = [];
                $epLcInfos = [];
                $clrDocuments = [];
                $gpMaterials = [];
                $remaining_quntities = [];
                $gpHistory = [];
            }

            $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`updated_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`p_hist_id` = `pd`.`process_hist_id` 
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id` and `process_list_hist`.`process_type` = `as`.`service_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = '$this->service_id'
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.updated_at desc
                    "));

            $viewMode = 'on';
            $mode = 'V';
            return view("exportPermit::application-form", compact('ip_permit_type', 'carrier_type', 'countries', 'zoneType', 'economicZone', 'allEcoZones', 'currencies', 'alreadyExistApplicant', 'process_history', 'gpMaterials', 'remaining_quntities', 'gpHistory', 'material_type', 'quantity_unit', 'banks', 'statusArray', 'bankTypes', 'document', 'meterials', 'epLcInfos', 'applicantInfo', 'clrDocuments', 'viewMode', 'mode', 'form_data', 'process_data', 'submissionDate', 'statusId', 'deskId', 'ports'));
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function appDownloadPDF($id) {
        $app_id = Encryption::decodeId($id);

//        $form_data = ExportPermit::where('id', $app_id)->where('service_id', $this->service_id)->first();
        $form_data = ExportPermit::leftJoin('project_clearance', 'project_clearance.created_by', '=', 'export_permit.created_by')
                        ->where('project_clearance.status_id', 23)
                        ->where('export_permit.id', $app_id)->first(['export_permit.*', 'project_clearance.applicant_name']);

        if (!count($form_data) > 0) {
            session()->flash('error', 'Application data not found.');
            return redirect()->back();
        }
        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
        $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
        $statusId = $process_data->status_id;
        $deskId = $process_data->desk_id;

        $ip_permit_type = PermitType::orderBy('name')->where('type', 2)->where('status', 1)->lists('name', 'id');
        $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
        $material_type = MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id');
        $zoneType = ['' => 'Select One',
            'Private Owned Economic Site' => 'Private Owned Economic Site',
            'Government Owned Economic Site' => 'Government Owned Economic Site'];
        $alreadyExistApplicant = ExportPermit::where('id', $app_id)->first();
        $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', 1) // 1 is for project Clearance
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                ->first();
        $economicZone = '';
        if ($projectClearanceData != null) {
            $economicZone = EconomicZones::select(DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))->where('id', $projectClearanceData->eco_zone_id)->first();
        }

//        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
//            ->orderBy('zone')->lists('zone', 'id');

        $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
        $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
        $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
        $ports = Ports::orderBy('name')->lists('name', 'id');

        $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();
        $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        if ($form_data) {
            $clr_document = Document::where('app_id', $form_data->id)->get();
            $meterials = ExportMaterial::where('ip_id', $form_data->id)->get();
            $epLcInfos = ExportLcInfo::where('ip_id', $form_data->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
        } else {
            $clrDocuments = [];
            $meterials = [];
            $epLcInfos = [];
        }

        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`,
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`,
                                `process_list_hist`.`updated_by`,
                                `process_list_hist`.`status_id`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`record_id`,
                                `process_list_hist`.`created_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`record_id` = `pd`.`app_id` and `process_list_hist`.`desk_id` = `pd`.`desk_id` and `process_list_hist`.`status_id` = `pd`.`status_id`
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id`
                                where `process_list_hist`.`record_id`  = '$app_id'
                                and `process_list_hist`.`process_type` = 0
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.created_at desc
                    "));

        $viewMode = 'on';
        $mode = 'V';
        $content = view("exportPermit::application-form-html", compact('ip_permit_type', 'economicZone', 'carrier_type', 'countries', 'zoneType', 'currencies', 'process_history', 'material_type', 'quantity_unit', 'banks', 'statusArray', 'bankTypes', 'document', 'form_data', 'meterials', 'epLcInfos', 'clrDocuments', 'viewMode', 'mode', 'form_data', 'process_data', 'statusId', 'deskId', 'ports'))->render();


        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                12, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                15, // margin bottom
                10, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("BEZA");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->setFooter('{PAGENO} / {nb}');
        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->WriteHTML($content, 2);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        $mpdf->Output($process_data->track_no . '.pdf', 'I');   // Saving pdf "F" for Save only, "I" for view only.
    }

    public function appStore(Request $request, ExportPermit $exportPermit, Processlist $processlist) {

        $authUserId = CommonFunction::getUserId();

        /* Start of preventing user to apply with approved project clearance */
        $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', 1) // 1 is for project Clearance
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('project_clearance.created_by', $authUserId)
                ->first();
        /* end of preventing user to apply with approved project clearance */
        if (!empty($projectClearanceData)) {
            $ecoZoneId = $projectClearanceData->eco_zone_id;
        } else {
            $ecoZoneId = '';
        }


        if ($request->get('submitInsert') == 'save') {
            $this->validate($request, [
                'permit_type' => 'required',
                'carrier_type' => 'required',
                'developer_name' => 'required',
                'undertaking_no' => 'required',
                'undertaking_date' => 'required',
                'invoice_ref_no' => 'required',
                'invoice_ref_date' => 'required',
                'cm_select' => 'required',
                'cm_value' => 'required',
                'invoice_select' => 'required',
                'invoice_value' => 'required',
                'destination_port' => 'required',
                'export_port' => 'required',
                'destination_country' => 'required',
                'destination_zone' => 'required',
                'consignee_name' => 'required',
                'consignee_address' => 'required',
                'remark' => 'required',
                'product_description' => 'requiredarray',
                'fob_usd' => 'requiredarray',
                'fob_usd_value' => 'requiredarray',
                'hs_code' => 'requiredarray',
                'mat_quantity' => 'requiredarray',
                'mat_quantity_unit' => 'required',
                'tt_no' => 'requiredarray',
                'tt_value' => 'required',
                'bank_id' => 'requiredarray',
                'bank_invoice_date' => 'requiredarray',
                'bank_type' => 'requiredarray',
                'expiry_date' => 'requiredarray',
                'acceptTerms' => 'required',
            ]);

            $statusArr = array(8, 13, 22, '-1');
            $alreadyExistApplicant = ExportPermit::leftJoin('process_list', function($join) {
                        $join->on('process_list.record_id', '=', 'export_permit.id');
                        $join->on('process_list.service_id', '=', DB::raw($this->service_id));
                    })
                    ->where('process_list.service_id', $this->service_id)
                    ->whereNotIn('process_list.status_id', $statusArr)
                    ->where('export_permit.created_by', $authUserId)
                    ->first();
        }

        try {

            $app_id = (!empty($request->get('app_id')) ? Encryption::decodeId($request->get('app_id')) : '');
            $alreadyExistApplicant = ExportPermit::where('created_by', $authUserId)->where('id', $app_id)->orderBy('id', 'ASC')->first();
            if ($alreadyExistApplicant) {
                if (!ACL::getAccsessRight('exportPermit', 'E', $app_id))
                    abort('400', "You have no access right! Please contact with system admin if you have any query.");
                $exportPermit = $alreadyExistApplicant;
                if ($alreadyExistApplicant->created_by != $authUserId) {
                    session()->flash('error', 'You are not authorized person to edit the application.');
                    return redirect()->back();
                }
            } else {
                if (!ACL::getAccsessRight('exportPermit', 'A', $app_id))
                    abort('400', "You have no access right! Please contact with system admin if you have any query.");
            }

            DB::beginTransaction();

            $exportPermit->service_id = $this->service_id;
            $exportPermit->permit_type = $request->get('permit_type');
            $exportPermit->carrier_type = $request->get('carrier_type');
            $exportPermit->zone_type = $ecoZoneId;
            $exportPermit->eco_zone_id = $ecoZoneId;
            $exportPermit->developer_name = $request->get('developer_name');
            $exportPermit->undertaking_no = $request->get('undertaking_no');
            if ($request->get('undertaking_date') != '')
                $exportPermit->undertaking_date = CommonFunction::changeDateFormat($request->get('undertaking_date'), true);
            $exportPermit->invoice_ref_no = $request->get('invoice_ref_no');
            if ($request->get('invoice_ref_date') != '')
                $exportPermit->invoice_ref_date = CommonFunction::changeDateFormat($request->get('invoice_ref_date'), true);
            $exportPermit->cm_select = $request->get('cm_select');
            $exportPermit->cm_value = $request->get('cm_value');
            $exportPermit->invoice_select = $request->get('invoice_select');
            $exportPermit->invoice_value = $request->get('invoice_value');
            $exportPermit->destination_port = $request->get('destination_port');
            $exportPermit->carrier_name = $request->get('carrier_name');
            $exportPermit->carrier_passport_no = $request->get('carrier_passport_no');
            if ($request->get('carrier_pass_validity') != '')
                $exportPermit->carrier_pass_validity = CommonFunction::changeDateFormat($request->get('carrier_pass_validity'), true);
            $exportPermit->export_port = $request->get('export_port');
            $exportPermit->destination_country = $request->get('destination_country');
            $exportPermit->destination_zone = $request->get('destination_zone');
            $exportPermit->consignee_name = $request->get('consignee_name');
            $exportPermit->consignee_address = $request->get('consignee_address');
            $exportPermit->remark = $request->get('remark');
            $acceptTerms = (!empty($request->get('acceptTerms')) ? 1 : 0);
            $exportPermit->acceptTerms = $acceptTerms;
            $exportPermit->is_locked = 0;
            $exportPermit->is_archieved = 0;
            $exportPermit->created_by = $authUserId;
            $exportPermit->save();

            ///For Tracking ID generating and update

            if ($request->get('submitInsert') == 'save') {
                if (empty($projectClearanceData)) {
                    Session::flash('error', "You must have a approved project clearance certificate attached to your profile!");
                    return redirect()->back();
                }
            }

            if ($request->get('submitInsert') == 'save') {
                /* if applicant submit the application without saving as draft or from draft submit an application track no will be generated
                  // if status id = 5 or shortfall, tracking id will not be regenerated */
                if ((!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number == '' && $alreadyExistApplicant->status_id != 5) ||
                        empty($alreadyExistApplicant)) {
                    $tracking_number = 'EP-' . date("dMY") . $this->service_id . str_pad($exportPermit->id, 6, '0', STR_PAD_LEFT);
                } else {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                }
            } else {
                if (!empty($alreadyExistApplicant) && $alreadyExistApplicant->tracking_number != '' && $alreadyExistApplicant->status_id == 5) {
                    $tracking_number = $alreadyExistApplicant->tracking_number; // for shortfall
                } else {
                    $tracking_number = '';
                }
            }

            if ($request->get('submitInsert') == 'save') {
                $exportPermit->status_id = 1;
                $exportPermit->is_draft = 0;
            } else {
                $exportPermit->status_id = -1;
                $exportPermit->is_draft = 1;
            }

            $exportPermit->tracking_number = $tracking_number;
            $exportPermit->save();

            $data = $request->all();
            foreach ($data['product_description'] as $key => $meterial) {
                if (empty($data['meterial_ids'][$key])) {
                    $materialData = new ExportMaterial();
                } else {
                    $materialId = $data['meterial_ids'][$key];
                    $materialData = ExportMaterial::where('id', $materialId)->first();
                }
                $materialData->ip_id = $exportPermit->id;
                $materialData->product_description = $data['product_description'][$key];
                $materialData->hs_code = $data['hs_code'][$key];
                $materialData->hs_product = $data['hs_product'][$key];
                $materialData->mat_quantity = $data['mat_quantity'][$key];
                $materialData->mat_remaining_quantity = $data['mat_quantity'][$key];
                $materialData->mat_quantity_unit = $data['mat_quantity_unit'][$key];
                $materialData->fob_usd = $data['fob_usd'][$key];
                $materialData->fob_usd_value = $data['fob_usd_value'][$key];
                $materialData->is_active = 1;
                $materialData->save();
                $metIds[] = $materialData->id;
            }
            if (!empty($metIds))
                ExportMaterial::where('ip_id', $exportPermit->id)->whereNotIn('id', $metIds)->delete();
            foreach ($data['tt_no'] as $key => $TtNo) {
                if (empty($data['epLcInfo_ids'][$key])) {
                    $epLcData = new ExportLcInfo();
                } else {
                    $epLcId = $data['epLcInfo_ids'][$key];
                    $epLcData = ExportLcInfo::where('id', $epLcId)->first();
                }
                $epLcData->ip_id = $exportPermit->id;
                $epLcData->tt_no = $data['tt_no'][$key];
                $epLcData->tt_value = $data['tt_value'][$key];
                $epLcData->bank_id = $data['bank_id'][$key];
                $epLcData->bank_type = $data['bank_type'][$key];
                if (!empty($data['bank_invoice_date'][$key]))
                    $epLcData->bank_invoice_date = CommonFunction::changeDateFormat($data['bank_invoice_date'][$key], true);
                if (!empty($data['expiry_date'][$key]))
                    $epLcData->expiry_date = CommonFunction::changeDateFormat($data['expiry_date'][$key], true);
                $epLcData->is_active = 1;
                $epLcData->save();
                $epLcIds[] = $epLcData->id;
            }
            if (!empty($epLcIds))
                ExportLcInfo::where('ip_id', $exportPermit->id)->whereNotIn('id', $epLcIds)->delete();

            $doc_row = docInfo::where('service_id', $this->service_id) // 5 for export permit
                    ->get(['doc_id', 'doc_name']);

            ///Start file uploading            
            if (isset($doc_row)) {
                foreach ($doc_row as $docs) {
                    //  if ($request->get('validate_field_' . $docs->doc_id) != '') {
                    $documentName = (!empty($request->get('other_doc_name_' . $docs->doc_id)) ? $request->get('other_doc_name_' . $docs->doc_id) : $request->get('doc_name_' . $docs->doc_id));
                    $documnent_id = $docs->doc_id;
                    if ($request->get('document_id_' . $docs->doc_id) == '') {
                        Document::create([
                            'service_id' => $this->service_id, // 5 for export permit
                            'app_id' => $exportPermit->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    } else {
                        $documentId = $request->get('document_id_' . $docs->doc_id);
                        Document::where('id', $documentId)->update([
                            'service_id' => $this->service_id, // 5 for export permit
                            'app_id' => $exportPermit->id,
                            'doc_id' => $documnent_id,
                            'doc_name' => $documentName,
                            'doc_file' => $request->get('validate_field_' . $docs->doc_id)
                        ]);
                    }
                    //  }
                }
            } /* End file uploading */

            //Saving data to process_list table
            $processlistExist = Processlist::where('record_id', $exportPermit->id)->where('service_id', $this->service_id)->first();
            $deskId = 0;

            if ($request->get('submitInsert') == 'save') {
                $statusId = 1;
                $deskId = 3; // 3 is RD1
            } else {
                $statusId = -1;
                $deskId = 0;
            }

            if (count($processlistExist) < 1) {
                $process_list_insert = Processlist::create([
                            'track_no' => $tracking_number,
                            'reference_no' => '',
                            'company_id' => '',
                            'service_id' => $this->service_id,
                            'initiated_by' => CommonFunction::getUserId(),
                            'closed_by' => 0,
                            'status_id' => $statusId,
                            'desk_id' => $deskId,
                            'record_id' => $exportPermit->id,
                            'eco_zone_id' => $ecoZoneId,
                            'process_desc' => '',
                            'updated_by' => CommonFunction::getUserId()
                ]);
            } else {
                if ($processlistExist->status_id > -1) {
                    if ($request->get('submitInsert') == 'save') {
                        $statusId = 10; // resubmitted
                    } else {
                        $statusId = $processlistExist->status_id; // if drafted, older status will remain
                    }
                }
                $processlisUpdate = array(
                    'track_no' => $tracking_number,
                    'service_id' => $this->service_id,
                    'status_id' => $statusId,
                    'desk_id' => $deskId,
                );
                $processlist->update_draft_app_for_ep($exportPermit->id, $processlisUpdate);
            }

            $saved_app_id = $exportPermit->id;
            $undertaking = '';
            if ($statusId == 1) {
                $undertaking = $this->undertakingGen($saved_app_id, 'undertaking');   // Undertaking Generation after application submission
                ExportPermit::where('id', $saved_app_id)->update(['undertaking' => $undertaking]);
            }

            DB::commit();
            if ($request->get('submitInsert') == 'save') {
                Session::flash('success', "Your application has been submitted with tracking no: <strong>" . $tracking_number . "</strong>");
            } else {
                Session::flash('success', "Your application has been drafted successfully!</strong>");
            }

            $list = DB::table('service_info')->where('id', $this->service_id)->pluck('url');
            return redirect($list);
        } catch (Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }

    public function uploadDocument() {
        return View::make('exportPermit::ajaxUploadFile');
    }

    public function preview() {
        return view("exportPermit::preview");
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', $this->service_id)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = Status::where('service_id', $this->service_id)->orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('exportPermit::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

        $industrial_category = $request->get('industry_cat_id');
        $economic_zone = $request->get('eco_zone_id');

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $applicant_name, $status_id, $industrial_category, $economic_zone);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('exportPermit::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();

        // Code to count the total number of application for different services as a whole
        $sql = DB::select("SELECT COUNT(pl.process_id) as application_number, si.id, si.short_name, pl.eco_zone_id, industry_cat_id
                                            from service_info si
                                            left join process_list pl on si.id = pl.service_id and pl.eco_zone_id ='$economic_zone' and pl.status_id !='-1'
                                            left join (select industry_cat_id, created_by
                                                    from project_clearance
                                                    where industry_cat_id = '$industrial_category'
                                                     limit 1) pc
                                             on pl.initiated_by = pc.created_by
                                            group by si.id;");

        $str = "";
        foreach ($sql as $sql_data) {
            $short_name = $sql_data->short_name;
            $id = $sql_data->id;
            $industry_cat_id = $sql_data->industry_cat_id;
            $application_number = $sql_data->application_number;
            $eco_zone_id = $sql_data->eco_zone_id;
            $str.="$short_name:$application_number:$industry_cat_id:$eco_zone_id==";
        }
        $data = ['responseCode' => 1, 'data' => $contents, 'totalApps' => $str]; // pattern= Module:AppsCount:Ind:zone
        // End of code to count total number of applications

        return response()->json($data);
    }

    /**
     * @param $param request type for ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');
        $current_service_id = $request->get('current_service_id');

        $processType = 1;

        if ($param == 'process') {

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;

            $verifiedInfo = ExportPermit::where('id', $curr_app_id)->first();
            $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM export_permit_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            // Get all applications' id
            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition

            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM export_permit_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";

            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {

            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-status-list') {

            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
//                $deskId = Users::where('id', $user_id)->pluck('desk_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->where('service_id', $this->service_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');

            $verifiedInfo = ExportPermit::where('id', $curr_app_id)->first();
            $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS                        
                        WHERE find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM export_permit_process_path APP  
                        WHERE APP.status_from = '$statusId' $cond))
                        AND APS.service_id = $this->service_id
                        order by APS.status_name";

            $statusList = \DB::select(DB::raw($sql));

            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    public function updateBatch(Request $request, CommonFunction $common) {

        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $status_id = $request->get('status_id');
        $attach_file = $request->file('attach_file');
        $onbehalf = $request->get('on_behalf_of');

        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }
        foreach ($apps_id as $app_id) {

            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->service_id = $this->service_id;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }

            $appInfo = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;

            // pdf server code
            //$pdf_type = 'beza.certificate.'.env('server_type');
            //$reg_key = 'reg-key';
            //$common->certificateGenForUpdateBatch($this->service_id,$app_id,$pdf_type,$reg_key);
            // pdf server code

            if (empty($desk_id)) {
                $whereCond = "select * from export_permit_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'
                        AND status_to REGEXP '^([0-9]*[,]+)*$status_id([,]+[,0-9]*)*$'";

                $processPath = DB::select(DB::raw($whereCond));
                if ($processPath[0]->desk_to == '0')  // Sent to Applicant
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')   // Keep in same desk
                    $desk_id = $deskFrom;
            }

            if ($status_id == 21) { // 21 is Approved and sent to custom
                $currentDate = date("dmY");

                $ep_Number = 'EP' . $currentDate;
                $exportPermitData = ExportPermit::where('service_id', '=', $this->service_id)
                                ->where('ep_number', 'LIKE', $ep_Number . '%')->count();
                $ep_number = $ep_Number . str_pad($exportPermitData + 1, 3, '0', STR_PAD_LEFT);

                $app_data = array(
                    'ep_number' => $ep_number,
                    'status_id' => $status_id,
                    'remarks' => $remarks,
                    'updated_at' => date('y-m-d H:i:s'),
                    'updated_by' => Auth::user()->id
                );
            } else {
                $app_data = array(
                    'status_id' => $status_id,
                    'remarks' => $remarks,
                    'updated_at' => date('y-m-d H:i:s'),
                    'updated_by' => Auth::user()->id
                );
            }

            $info_data = array(
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8) { // 8 is discard
                $info_data['closed_by'] = Auth::user()->id;
            }

            Processlist::where('record_id', $app_id)
                    ->where('service_id', $this->service_id)
                    ->update($info_data);

            ExportPermit::where('id', $app_id)->update($app_data);

            ExportPermit::where('id', $app_id)->update([
                'status_id' => $status_id,
                'remarks' => $remarks]);

            if (in_array($status_id, array(5, 8, 21, 22))) {  // 21 = Approved & sent to customs, 22 = Rejected,
                $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
                $cur_status = Status::where('status_id', $status_id)->where('service_id', $this->service_id)->pluck('status_name');

//            Mailing
                $body_msg = '<span style="color:#000;text-align:justify;"><b>';
                $body_msg .= 'Your application for export permit with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' .
                        $cur_status . '</b>';

                $body_msg .= '</span>';
                $body_msg .= '<br/><br/><br/>Thanks<br/>';
                $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                $data = array(
                    'header' => 'Application Update',
                    'param' => $body_msg
                );

                $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');
                $certificate = '';
                if ($status_id == 21) {  // the new status 21 = Approved and sent to custom
                    $certificate = $this->exportPermitCer($app_id);
//                  $certificate = $this->undertakingGen($app_id, 'approval');   //Certificate Generation
                }
                $billMonth = date('Y-m');
                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $app_id, $certificate, $status_id, $billMonth) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                            ->to($fetched_email_address)
                            ->cc('jakir.ocpl@batworld.com')
                            ->subject('Application Update Information for export permit');
                    if ($status_id == 21) { // 21 = Approved and sent to custom
                        $message->attach($certificate);
                        ExportPermit::where('id', $app_id)->update(['certificate' => $certificate, 'bill_month' => $billMonth]);
                    }
                });
            }
        }

        //  for previous and present status
        $appStatus = Status::where('service_id', $this->service_id)->get();
        $statusList = array();
        foreach ($appStatus as $k => $v) {
            $statusList[$v->status_id] = $v->status_name;
        }

        Session::flash('success', "Application status updated: Previous status: $statusList[$status_from] || Present Status: $statusList[$status_id]");
        return redirect()->back();
    }

    public function certificate_re_gen($id) {
        $app_id = Encryption::decodeId($id);
        $processlistExist = Processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first(['process_id', 'status_id']);
        $status_id = $processlistExist->status_id;
        $certificate = '';
        if ($status_id == 21) { // 21 = approved and sent to custom
            $certificate = $this->exportPermitCer($app_id);   //Certificate Generation after payment accepted
            ExportPermit::where('id', $app_id)->update(['certificate' => $certificate]);
        }
        return redirect()->back();
    }

    public function exportPermitCer($id) {
        $app_id = $id;
        $dn1d = new DNS1D();
        $authUserId = CommonFunction::getUserId();

        $form_data = ExportPermit::where('id', $app_id)->where('service_id', $this->service_id)->first();
        if (!count($form_data) > 0) {
            session()->flash('error', 'Application data not found.');
            return redirect()->back();
        }
        $statusArray = Status::where('service_id', $this->service_id)->lists('status_name', 'status_id');
        $process_data = processlist::where('record_id', $app_id)->where('service_id', $this->service_id)->first();
        $statusId = $process_data->status_id;
        $deskId = $process_data->desk_id;
        $projectClearanceData = ProjectClearance::leftJoin('process_list', 'process_list.record_id', '=', 'project_clearance.id')
                ->where('process_list.service_id', 1) // 1 is for project Clearance
                ->where('process_list.status_id', 23) // 23 is payment accepted
                ->where('project_clearance.created_by', $form_data->created_by)
                ->first(['id', 'proposed_name', 'process_id']);

        $ep_permit_type = PermitType::orderBy('name')->where('type', 2)->where('status', 1)->lists('name', 'id');
        $permit_type = PermitType::where('id', $form_data->permit_type)->pluck('name');

        $carrier_type = CarrierType::orderBy('name')->where('status', 1)->lists('name', 'id');
        $material_type = MaterialType::orderBy('name')->where('status', 1)->lists('name', 'id');

        $zoneType = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $countries = ['' => 'Select One'] + Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('name', 'iso')->all();
        $currencies = Currencies::orderBy('code')->where('is_active', 1)->lists('code', 'id');
        $quantity_unit = Units::where('is_active', 1)->where('is_archieved', 0)->orderBy('name')->lists('name', 'id');
        $ports = Ports::orderBy('name')->lists('name', 'id');

        $banks = ['' => 'Select One'] + Bank::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();
        $bankTypes = ['' => 'Select One', '1' => 'Back to back', '2' => 'Sales Contruct', '3' => 'Defferred', '4' => 'Sight'];

        $document = docInfo::where('service_id', $this->service_id)->orderBy('order')->get();

        if ($form_data) {
            $clr_document = Document::where('app_id', $form_data->id)->get();
            $meterials = ExportMaterial::where('ip_id', $form_data->id)->get();
            $epLcInfos = ExportLcInfo::where('ip_id', $form_data->id)->get();
            foreach ($clr_document as $documents) {
                $clrDocuments[$documents->doc_id]['doucument_id'] = $documents->id;
                $clrDocuments[$documents->doc_id]['file'] = $documents->doc_file;
                $clrDocuments[$documents->doc_id]['doc_name'] = $documents->doc_name;
            }
        } else {
            $clrDocuments = [];
        }

        $certificate_issue_date = date("dS F, Y");
        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`,
                                if(`process_list_hist`.`desk_id`=0,\"Applicant\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`,
                                `process_list_hist`.`updated_by`,
                                `process_list_hist`.`status_id`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`record_id`,
                                `process_list_hist`.`created_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`record_id` = `pd`.`app_id` and `process_list_hist`.`desk_id` = `pd`.`desk_id` and `process_list_hist`.`status_id` = `pd`.`status_id`
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id`
                                where `process_list_hist`.`record_id`  = '$app_id'
                                and `process_list_hist`.`process_type` = 0
                                and `process_list_hist`.`status_id` != -1
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`
                    order by process_list_hist.created_at desc
                    "));

        $viewMode = 'on';
        $mode = 'V';
        $content = view("exportPermit::certificate", compact(
                        'ep_permit_type', 'permit_type', 'carrier_type', 'countries', 'zoneType', 'currencies', 'process_history', 'material_type', 'quantity_unit', 'banks', 'statusArray', 'bankTypes', 'document', 'form_data', 'meterials', 'epLcInfos', 'clrDocuments', 'viewMode', 'mode', 'form_data', 'process_data', 'certificate_issue_date', 'projectClearanceData', 'statusId', 'deskId', 'ports'))->render();

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                12, // font size - default 0
                'Times New Roman', // default font family
                10, // margin_left
                10, // margin right
                30, // margin top
                50, // margin bottom
                10, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->Bookmark('Start of the document');
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("BEZA");
        $mpdf->SetSubject("Subject");
        $mpdf->SetAuthor("Business Automation Limited");
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;
        $mpdf->SetHTMLHeader('<img src="assets/images/BEZA-PNG.png" alt="BEZA" width="150px"/>');

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->setWatermarkImage('assets/images/beza_watermark.png');
        $mpdf->showWatermarkImage = true;

        if (!empty($form_data->ep_number)) {
            $barcode = $dn1d->getBarcodePNG($form_data->ep_number, 'C39');
            $img = '<img src="data:image/png;base64,' . $barcode . '" alt="barcode" width="250"/>';
        } else {
            $img = '';
        }

        $mpdf->setFooter('<div style="margin-top:6px;">
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style="padding:2px;font-size:9px;" class="alert alert-info">
                                            The goods specified in the invoice(s) are eligible for exemption for the whole of the customs duty, sales tax etc. 
                                            provided S.R.O. No. 209-LAW/2015/46/Customs dated July 1,2015 issued by the Internal Resources Division, 
                                            Ministry of Finance, Dhaka
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding:2px;font-size:10px;" class="alert alert-danger">
                                            <b> THIS PERMIT IS VALID FOR 30 (THIRTY) DAYS FROM THE DATE OF ISSUE</b>
                                        </td>
                                    </tr>
                                    <tr><td style=" padding:5px; border:0;"></td></tr>
                                </table>
                                </br>
                                <table class="" border="1" width="100%" style="text-align: center;">
                                    <tr>
                                        <td style=" padding:2px;font-size:10px;" class="alert alert-warning">
                                            <b>THIS IS COMPUTER GENERATED PERMIT. SO SIGNATURE IS NOT REQUIRED</b>
                                            <br/>' . $img . '<br/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br/> {PAGENO} / {nb}');

        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->setAutoBottomMargin = 'stretch';
        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->WriteHTML($content, 2);

        $mpdf->defaultfooterfontsize = 10;
        $mpdf->defaultfooterfontstyle = 'B';
        $mpdf->defaultfooterline = 0;

        $mpdf->SetCompression(true);
        $baseURL = 'uploads/';
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Year");
        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }

        $certificateName = uniqid("beza_" . $app_id . "_", true);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
        return $pdfFilePath;
    }

    public function discardCertificate($id) {
        $app_id = Encryption::decodeId($id);
        $appInfo = ExportPermit::find($app_id);

        if (Auth::user()->user_type == '1x101') {
            if (in_array($appInfo->status_id, [21, 28, 30])) {
                $appInfo->status_id = 40;
                $appInfo->certificate = '';
                $appInfo->save();
                ProcessList::where(['record_id' => $app_id, 'service_id' => 5])
                        ->update(['desk_id' => 0, 'status_id' => 40]);

                Session::flash('success', 'Certificate cancelled');
                return redirect()->back();
            } else {
                Session::flash('error', 'Certificate discard is not possible [EP9001]');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'You are not authorized user to discard certificate[EP 9002]');
            return redirect()->back();
        }
    }

    public function storePassHistory($ip_id, Request $request) {
        $app_id = Encryption::decodeId($ip_id);

        $ipInfo = ExportPermit::where('id', $app_id)->first();

        if (empty($ipInfo)) {
            Session::flash('error', "Sufficient information couldn't be fetched for this export permit! Please contact to system admin for further investigation.");
            return redirect()->back();
        }

        $status_id = $ipInfo->status_id;

        if ($status_id == 30 || $status_id == 32) { // 30 = custom verified, 32 = Partial Gatepass
            $currentTime = time();

            $ip_mat_ids = $request->get('ip_mat_id');
            $passed_quantities = $request->get('passed_quantity');

            $i = 0;
            foreach ($ip_mat_ids as $mat_id) {
                $epMatInfo = ExportMaterial::where('id', $mat_id)->first();

                $remain_quantity = $epMatInfo->mat_remaining_quantity;
                $total_quntity = $epMatInfo->mat_quantity;

                $passed = $passed_quantities[$i];

                if ($passed > 0) {
                    $gp_no = 'GP-' . $this->service_id . str_pad($app_id, 6, '0', STR_PAD_LEFT) . $currentTime;

                    if ($total_quntity >= $remain_quantity) {
                        $new_remain = $remain_quantity - $passed; // Substracting passed quantity from remaining
                        ExportMaterial::where('id', $mat_id)->update([
                            'mat_remaining_quantity' => $new_remain,
                        ]);
                    } else {
                        Session::flash('error', "The passed quantity can not exceed the original quantity");
                        return redirect()->back();
                    }

                    $insertedGp = EpGatePass::create([
                                'app_id' => $app_id,
                                'ep_material_id' => $mat_id,
                                'gatepass_no' => $gp_no,
                                'remaining_quantity' => $remain_quantity,
                                'passed_quantity' => $passed,
                                'created_at' => date('y-m-d H:i:s'),
                                'created_by' => Auth::user()->id
                    ]);

                    if ($insertedGp) {
                        $gen_gatepass = $this->gatePassGen($app_id, $insertedGp->id);

                        EpGatePass::where('id', $insertedGp->id)->update([
                            'generated_gatepass' => $gen_gatepass,
                        ]);

                        GatePassHistory::create([
                            'app_id' => $app_id,
                            'service_id' => $this->service_id,
                            'gatepass_id' => $insertedGp->id,
                            'gatepass_no' => $gp_no,
                            'created_at' => date('y-m-d H:i:s'),
                            'created_by' => Auth::user()->id
                        ]);


                        //  Sending gatepass as attachment
                        $body_msg = '<span style="color:#000;text-align:justify;">';
                        $body_msg .= 'You can find the gatepass generated for your export permit application with Tracking Number: <b>'
                                . $ipInfo->tracking_number . '</b> in the attachment.';
                        $body_msg .= '</span>';
                        $body_msg .= '<br/><br/><span>Please print and preserve this copy for further usage.</span>';
                        $body_msg .= '<br/><br/><br/>Thanks<br/>';
                        $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

                        $data = array(
                            'header' => 'Application Update',
                            'param' => $body_msg
                        );

                        $fetched_email_address = CommonFunction::getFieldName($ipInfo->created_by, 'id', 'user_email', 'users');

                        \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $gen_gatepass ) {
                            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                                    ->to($fetched_email_address)
                                    ->cc('jakir.ocpl@batworld.com')
                                    ->subject('Gate Pass for export permit');
                            $message->attach($gen_gatepass);
                        });
                    } else {
                        Session::flash('error', "Some errors have been occurred while processing. Please try again later!");
                        return redirect()->back();
                    }
                } // if passed quantity is greater than 0
                $i++;
            }
        } else {
            Session::flash('error', "This export permit doesn't have the correct status for processing! Please contact to system admin for further investigation.");
            return redirect()->back();
        }

        if (in_array($status_id, array(5, 8, 22))) {// 22 = Rejected,
            $process_data = Processlist::where('record_id', $app_id)->where('service_id', '=', $this->service_id)->first();
            $cur_status = Status::where('status_id', $status_id)->where('service_id', $this->service_id)->pluck('status_name');

//            Notification
            $body_msg = '<span style="color:#000;text-align:justify;"><b>';
            $body_msg .= 'Your application for export permit with Tracking Number: ' . $process_data->track_no . ' is now in status: <b>' .
                    $cur_status . '</b></span>';
            $body_msg .= '<br/><br/><br/>Thanks<br/>';
            $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

            $data = array(
                'header' => 'Update of Application',
                'param' => $body_msg
            );

            $fetched_email_address = CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users');

            \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $gen_gatepass, $status_id) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->subject('Application Update Information for export permit');
                if ($status_id == 21) { // the new status 21 = Approved and sent to custom
                    $message->attach($gen_gatepass);
                }
            });
        }

        Session::flash('success', "Gatepass is generated successfully");
        return redirect()->back();
    }

    public function undertakingGen($appID, $pdfType) {
        ini_set('memory_limit', '99M');

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        if ($pdfType == 'undertaking') {
            $mpdf->SetHTMLHeader('');
        } else {
            $mpdf->SetHTMLHeader('<img src="assets/images/logo_beza.png" alt="BEZA" width="150px;">');
            $mpdf->SetWatermarkImage('assets/images/beza_watermark.png');
            $mpdf->showWatermarkImage = true;
        }

        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }

        $certificateName = uniqid("beza_" . $appID . "_", true);
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');


        $alreadyExistApplicant = Processlist::leftJoin('export_permit', 'export_permit.id', '=', 'process_list.record_id')
                ->where('process_list.service_id', $this->service_id)
                ->where('process_list.record_id', $appID)
                ->first();

        if (!$alreadyExistApplicant) {
            return '';
        } else {
            $projectClearanceInfo = ProjectClearance::where('status_id', 23) // 23 is payment acceptance
                    ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                    ->first();
            $company_name = $projectClearanceInfo->proposed_name;
            $permit_type = PermitType::where('id', $alreadyExistApplicant->permit_type)->pluck('name');

            $track_no = !empty($alreadyExistApplicant->track_no) ? $alreadyExistApplicant->track_no : '';
            $dateNow = date("dS F, Y");

            $eco_zone_id = !empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : '';
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);

            $invoice_no = !empty($alreadyExistApplicant->invoice_ref_no) ? $alreadyExistApplicant->invoice_ref_no : '';
            $invoice_date = !empty($alreadyExistApplicant->invoice_ref_date) ?
                    CommonFunction::changeDateFormat(substr($alreadyExistApplicant->invoice_ref_date, 0, 10)) : '';

            $directory = 'users/signature/';
            if ($pdfType == 'undertaking') {
                $approver = '';
                $url = '';
                $applicant = Users::where('users.id', $alreadyExistApplicant->created_by)
                        ->first(['user_full_name', 'signature']);

                $signature = (!empty($applicant->signature) && file_exists($directory . $applicant->signature)) ?
                        'users/signature/' . $applicant->signature : '';

                $heading = $permit_type . " Permit Undertaking";
            } else { // for certificate generation
                $applicant = '';

                /*                 * **Start of commenting code for Static RD3 as instructed by BEZA ***************** */
//                $approver = Users::leftJoin('user_desk', 'users.desk_id', '=', 'user_desk.desk_id')
//                        ->where('users.desk_id', 5) // 5 is RD3 approver
//                        ->first(['user_desk.desk_name as desk', 'user_full_name', 'signature']);
//             $signature = (!empty($approver->signature) && file_exists($directory . $approver->signature)) ?
//                        'users/signature/' . $approver->signature : '';
                /*                 * **End of commenting code for Static RD3 as instructed by BEZA ***************** */

                $approval_date = !empty($projectClearanceInfo->updated_at) ?
                        CommonFunction::changeDateFormat(substr($alreadyExistApplicant->updated_at, 0, 10)) : '';

                $signature = (file_exists($directory . 'rd3-sign.jpg')) ? 'users/signature/rd3-sign.jpg' : '';
                $approver = 'Mohammed Ayub';

                $heading = "Intra Zone Export Permit Forwarding Letter";

                if ($alreadyExistApplicant->status_id == 21 && !empty($alreadyExistApplicant->ep_number)) { //  21 = Approved and sent to custom
                    $epNo = $alreadyExistApplicant->ep_number;
                } else {
                    $epNo = '';
                }

                $qrCodeGenText = $alreadyExistApplicant->tracking_number . '-' . $company_name . '-' .
                        $economicZones->name . '-' . $approval_date;
                $qrcodeRule = str_replace(' ', '+', $qrCodeGenText);
                $url = "http://chart.apis.google.com/chart?chs=100x100&cht=qr&chl=$qrcodeRule&choe=ISO-8859-1";
            }

            $ep_documents = Document::where('app_id', $alreadyExistApplicant->id)->where('service_id', $this->service_id)
                    ->get(['doc_name', 'created_at']);

            $pdf_body = View::make("exportPermit::undertaking-html", compact('dateNow', 'track_no', 'economicZones', 'materials', 'permit_type', 'ep_documents', 'alreadyExistApplicant', 'invoice_no', 'invoice_date', 'signature', 'applicant', 'company_name', 'pdfType', 'heading', 'epNo', 'approver', 'applicant', 'url'))
                    ->render();

            $mpdf->SetCompression(true);
            $mpdf->WriteHTML($pdf_body);
            $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
            $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
            return $pdfFilePath;
        }
    }

    public function gatePassGen($appID, $gpId) {
        ini_set('memory_limit', '99M');

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                9, // font size - default 0
                'Times New Roman', // default font family
                17, // margin_left
                10, // margin right
                30, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );

        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $mpdf->SetHTMLHeader('');

        $mpdf->setFooter('{PAGENO} / {nb}');

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }

        $certificateName = uniqid("beza_" . $this->service_id . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');


        $alreadyExistApplicant = Processlist::leftJoin('export_permit', 'export_permit.id', '=', 'process_list.record_id')
                ->where('process_list.service_id', $this->service_id)
                ->where('process_list.record_id', $appID)
                ->first();

        if (!$alreadyExistApplicant) {
            return '';
        } else {
            $projectClearanceInfo = ProjectClearance::where('status_id', 23) // 23 is payment acceptance
                    ->where('project_clearance.created_by', $alreadyExistApplicant->created_by)
                    ->first();

            $company_name = $projectClearanceInfo->proposed_name;
            $permit_type = PermitType::where('id', $alreadyExistApplicant->permit_type)->pluck('name');

            $ip_no = !empty($alreadyExistApplicant->ep_number) ? $alreadyExistApplicant->ep_number : '';
            $track_no = !empty($alreadyExistApplicant->track_no) ? $alreadyExistApplicant->track_no : '';
            $dateNow = date("dS F, Y");
            $timeNow = date("h:i:sa");

            $eco_zone_id = !empty($alreadyExistApplicant->eco_zone_id) ? $alreadyExistApplicant->eco_zone_id : '';
            $economicZones = EconomicZones::where('id', $eco_zone_id)->first(['name', 'upazilla', 'district', 'area']);

            $passedMaterials = ExportMaterial::leftJoin('units', 'ep_materials.mat_quantity_unit', '=', 'units.id')
                    ->leftJoin('ep_gatepass', 'ep_gatepass.ep_material_id', '=', 'ep_materials.id')
                    ->where('ip_id', $appID)
                    ->where('ep_gatepass.passed_quantity', '>', 0)
                    ->where('ep_gatepass.id', $gpId)
                    ->get(['product_description', 'hs_code', 'hs_product', 'units.name as unit_name', 'passed_quantity']);

            $logged_user_id = Auth::user()->id;
            $security_user = Users::leftJoin('user_desk', 'users.desk_id', '=', 'user_desk.desk_id')
                    ->where('users.desk_id', 9) // 9 is RD3 approver
                    ->where('id', $logged_user_id)
                    ->first(['user_desk.desk_name as desk', 'user_full_name', 'designation', 'signature']);

            if (!empty($security_user)) {
                $directory = 'users/signature/';
                $signature = (!empty($security_user->signature) && file_exists($directory . $security_user->signature)) ?
                        'users/signature/' . $security_user->signature : '';
                $security_name = $security_user->user_full_name;
                $designation = $security_user->desk; // as designation field is not being in yet
            } else {
                $signature = '';
                $security_name = '';
                $designation = '';
            }

            $pdf_body = View::make("exportPermit::gp-html", compact('company_name', 'track_no', 'dateNow', 'timeNow', 'economicZones', 'permit_type', 'ip_no', 'passedMaterials', 'signature', 'security_name', 'designation'))
                    ->render();

            $mpdf->SetCompression(true);
            $mpdf->WriteHTML($pdf_body);
            $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
            $mpdf->Output($pdfFilePath, 'F'); // Saving pdf *** F for Save only, I for view only.
            return $pdfFilePath;
        }
    }

    public function getHsList(Request $request) {
        $results = HsCodes::where('hs_code', 'LIKE', '%' . $request->get('q') . '%')->get(['hs_code', 'product_name', 'id']);

        $data = array();
        foreach ($results as $key => $value) {
            $data[] = array(
                'value' => $value->hs_code,
                'product' => $value->product_name,
                'id' => $value->id);
        }

        return json_encode($data);
    }

    /*     * ********************************************End of Controller Class************************************************* */
}
