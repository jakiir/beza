@extends('layouts.admin')

@section('page_heading','<i class="fa fa-tasks"></i> Company / Organization Details')
@section('content')
<?php $accessMode=ACL::getAccsessRight('user');
if(!ACL::isAllowed($accessMode,'E')) die('no access right!');
?>
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="{{ url('users/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('company/create') }}"><i class="fa fa-object-group"></i> Organization</a></li>
    </ol>
</section>

    <section class="content">

        <!-- Default box -->
        <div class="box box-success col-md-10">
            <div class="box-header with-border">

            @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning">{{ Session::get('error') }}</div>
            @endif
            </div>

            <div class="box-body">
           
            <div class="box box-pane col-md-8 col-sm-8 col-md-offset-2">

                <br/>
                {!! Form::open(array('url' => '/company/update/'.Encryption::encodeId($company->company_id),'method' => 'patch', 'class' => 'form-horizontal',
                'id' => 'company_edit_form',   'enctype' =>'multipart/form-data', 'files' => 'true')) !!}
                <fieldset>           

                    <div class="form-group has-feedback {{ $errors->has('company_name') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left required-star">Name of the Company / Organization</label>
                        <div class="col-lg-6">                                                                     
                            {!! Form::text('company_name', $value = $company->company_name, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Enter the Name of the Company','id'=>"company_name")) !!}
                            <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
                            @if($errors->first('company_name'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_name','') }}</em>
                            </span>
                            @endif     
                        </div>
                    </div>                   

                    <?php
                    if (empty($company_types)) {
                        $company_types = array(
                            1 => 'Limited Company',
                            2 => 'Partnership',
                            3 => 'Propertieship',
                            4 => 'Government',
                            5 => 'Semi-Government',
                            6 => 'Student',
                            7 => 'Self Employed',
                        );
                    }
                    ?>

                    <div class="form-group has-feedback {{ $errors->has('company_type') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left required-star"> Type of Company</label>
                        <div class="col-lg-6">
                            {!! Form::select('company_type', $value = $company_types, $company->company_type, $attributes = array('class'=>'form-control required', 
                            'placeholder' => 'Select One', 'id'=>"company_type")) !!}
                            @if($errors->first('company_type'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_type','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="hidden form-group has-feedback {{ $errors->has('incorporation_certificate') ? 'has-error' : ''}}" id="inc_div">
                        <label  class="col-lg-6 text-left required-star">Incorporation Certificate</label>
                        <div class="col-lg-6">
                            {!! Form::file('incorporation_certificate', $value = null, $attributes = array('class'=>'form-control',
                            'id'=>"incorporation_certificate")) !!}
                            <?php if (!empty($company->incorporation_certificate)) { ?>                               
                                <a href="<?php echo url(); ?>/company/upload/<?php echo $company->incorporation_certificate; ?>" target="_blank">
                                    <?php echo $company->incorporation_certificate; ?>
                                    <img src="<?php echo url(); ?>/front-end/images/icon_download.gif" border="0" alt="download"
                                         title="<?php echo $company->incorporation_certificate;; ?>"/>            
                                </a>
                            <?php } ?>
                            <span class="glyphicon glyphicon-file form-control-feedback"></span>
                            @if($errors->first('incorporation_certificate'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('incorporation_certificate','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="hidden form-group has-feedback {{ $errors->has('partnership_deed') ? 'has-error' : ''}}" id="partner_div">
                        <label  class="col-lg-6 text-left required-star">Partnership Deed</label>
                        <div class="col-lg-6">
                            {!! Form::file('partnership_deed', $value = null, $attributes = array('class'=>'form-control',
                            'id'=>"partnership_deed")) !!}
                            <?php if (!empty($company->partnership_deed)) { ?>                               
                                <a href="<?php echo url(); ?>/company/upload/<?php echo $company->partnership_deed; ?>" target="_blank">
                                    <?php echo $company->partnership_deed; ?>
                                    <img src="<?php echo url(); ?>/front-end/images/icon_download.gif" border="0" alt="download"
                                         title="<?php echo $company->partnership_deed;; ?>"/>            
                                </a>
                            <?php } ?>
                            <span class="glyphicon glyphicon-file form-control-feedback"></span>
                            @if($errors->first('partnership_deed'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('partnership_deed','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>

                    <div class="hidden form-group has-feedback {{ $errors->has('com_trade_license') ? 'has-error' : ''}}" id="trade_div">
                        <label  class="col-lg-6 text-left required-star">Trade License </label>
                        <div class="col-lg-6">
                            {!! Form::file('com_trade_license', $value = null, $attributes = array('class'=>'form-control',
                            'id'=>"com_trade_license")) !!}
                            <?php if (!empty($company->com_trade_license)) { ?>                               
                                <a href="<?php echo url(); ?>/company/upload/<?php echo $company->com_trade_license; ?>" target="_blank">
                                    <?php echo $company->com_trade_license; ?>
                                    <img src="<?php echo url(); ?>/front-end/images/icon_download.gif" border="0" alt="download"
                                         title="<?php echo $company->com_trade_license;; ?>"/>            
                                </a>
                            <?php } ?>
                            <span class="glyphicon glyphicon-file form-control-feedback"></span>
                            @if($errors->first('com_trade_license'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('com_trade_license','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>

                    <hr/>
                    <div class="form-group has-feedback {{ $errors->has('head_of_org') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left">Head of Organization</label>
                        <div class="col-lg-6">
                            {!! Form::text('head_of_org', $value = $company->head_of_org, $attributes = array('class'=>'form-control ',
                            'placeholder'=>'Name of the Head of Organization','id'=>"head_of_org")) !!}
                            <span class="glyphicon glyphicon-header form-control-feedback"></span>
                            @if($errors->first('head_of_org'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('head_of_org','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>                                       
                    <div class="form-group has-feedback {{ $errors->has('head_contact') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left ">Contact No.</label>
                        <div class="col-lg-6">
                            {!! Form::text('head_contact', $value = $company->head_contact, $attributes = array('class'=>'form-control ',
                            'placeholder'=>'Contact of the Head of Organization','id'=>"head_contact")) !!}
                            <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
                            @if($errors->first('head_contact'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('head_contact','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('head_email') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left">Email Address</label>
                        <div class="col-lg-6">
                            {!! Form::text('head_email', $value = $company->head_email, $attributes = array('class'=>'form-control',
                            'placeholder'=>'Email of the Head of Organization','id'=>"head_email")) !!}
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if($errors->first('head_email'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('head_email','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>
                    <hr/>

                    <div class="form-group has-feedback {{ $errors->has('contact_person') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left required-star">Contact Person</label>
                        <div class="col-lg-6">
                            {!! Form::text('contact_person', $value = $company->contact_person, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Name of the Contact Person of Org.','id'=>"contact_person")) !!}
                            <span class="glyphicon glyphicon-copyright-mark form-control-feedback"></span>
                            @if($errors->first('contact_person'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('contact_person','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>                                       
                    <div class="form-group has-feedback {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left required-star">Phone No.</label>
                        <div class="col-lg-6">
                            {!! Form::text('contact_phone', $value = $company->contact_phone, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Phone No. of the Contact Person','id'=>"contact_phone")) !!}
                            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                            @if($errors->first('contact_phone'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('contact_phone','') }}</em>
                            </span>
                            @endif        
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left required-star">Email Address</label>
                        <div class="col-lg-6">
                            {!! Form::text('contact_email', $value = $company->contact_email, $attributes = array('class'=>'form-control required',
                            'placeholder'=>'Email Address of the Contact Person','id'=>"contact_email")) !!}
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if($errors->first('contact_email'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('contact_email','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr/>

                    <div class="form-group has-feedback {{ $errors->has('company_tin') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left">TIN</label>
                        <div class="col-lg-6">
                            {!! Form::text('company_tin', $value = $company->company_tin, $attributes = array('class'=>'form-control',
                            'placeholder'=>'Enter Tax Identification Number','id'=>"company_tin")) !!}
                            <span class="glyphicon glyphicon-alert form-control-feedback"></span>
                            @if($errors->first('company_tin'))
                            <span class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_tin','') }}</em>
                            </span>
                            @endif
                        </div>
                    </div>

                    <!-- <div class="form-group">
                            <div class="clearfix"><br/></div>
                            <label  class="col-lg-6 text-left">Address</label>                  
                        </div><hr/> -->

                    <fieldset>
                        <legend style="font-size: 16"><b>Address</b></legend>
                        <div class="form-group has-feedback {{ $errors->has('company_house_no') ? 'has-error' : ''}}">
                            <label  class="col-lg-6 text-left required-star">House / Flat / Plot / Holding Number</label>
                            <div class="col-lg-6">
                                {!! Form::text('company_house_no', $value = $company->company_house_no, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter the House / Plot / Holding No.','id'=>"company_house_no")) !!}
                                <span class="glyphicon glyphicon-home form-control-feedback"></span>
                                @if($errors->first('company_house_no'))
                                <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_house_no','') }}</em>
                                </span>
                                @endif        
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_street') ? 'has-error' : ''}}">
                            <label  class="col-lg-6 text-left required-star"> Street Name / Village</label>
                            <div class="col-lg-6">                                                                     
                                {!! Form::text('company_street', $value = $company->company_street, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter Street Name / No','id'=>"company_street")) !!}
                                <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                                @if($errors->first('company_street'))
                                <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_street','') }}</em>
                                </span>
                                @endif     
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_area') ? 'has-error' : ''}}">
                            <label  class="col-lg-6 text-left required-star">Area </label>
                            <div class="col-lg-6">                                                                     
                                {!! Form::text('company_area', $value = $company->company_area, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter Flat / Apartment / Floor No.','id'=>"company_area")) !!}
                                <span class="glyphicon glyphicon-tree-conifer form-control-feedback"></span>
                                @if($errors->first('company_area'))
                                <span class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_area','') }}</em>
                                </span>
                                @endif     
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_city') ? 'has-error' : ''}}">
                            <label  class="col-lg-6 text-left required-star">City </label>
                            <div class="col-lg-6">
                                {!! Form::text('company_city', $value = $company->company_city, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter the Name of the City','id'=>"company_city")) !!}
                                <span class="glyphicon glyphicon-tower form-control-feedback"></span>
                                @if($errors->first('company_city'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_city','') }}</em>
                                </span>
                                @endif        
                            </div>                      
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_zip') ? 'has-error' : ''}}">
                            <label  class="col-lg-6 text-left required-star">Zip / Post Code</label>
                            <div class="col-lg-6">
                                {!! Form::text('company_zip', $value = $company->company_zip, $attributes = array('class'=>'form-control required','
                                placeholder'=>'Enter the Zip / Post Code','id'=>"company_zip")) !!}
                                <span class="glyphicon glyphicon-tag form-control-feedback"></span>
                                @if($errors->first('company_zip'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_zip','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('company_fax') ? 'has-error' : ''}}">
                            <label  class="col-lg-6 text-left">Fax Number</label>
                            <div class="col-lg-6">
                                {!! Form::text('company_fax', $value = $company->company_fax, $attributes = array('class'=>'form-control required',
                                'placeholder'=>'Enter the fax Number (if any)','id'=>"company_fax")) !!}
                                <span class="glyphicon glyphicon-fast-forward form-control-feedback"></span>
                                @if($errors->first('company_fax'))
                                <span  class="control-label">
                                    <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_fax','') }}</em>
                                </span>
                                @endif
                            </div>
                        </div>

                    </fieldset>

                    <div class="form-group has-feedback {{ $errors->has('company_web') ? 'has-error' : ''}}">
                        <div class="clearfix"><br/></div>
                        <label  class="col-lg-6 text-left">Web Address  </label>
                        <div class="col-lg-6">
                            {!! Form::text('company_web', $value = $company->company_web, $attributes = array('class'=>'form-control',
                            'placeholder'=>"Enter the Company's Web Address",'id'=>"company_web")) !!}
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if($errors->first('company_web'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_web','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('phone_no') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left required-star">Phone No.  </label>
                        <div class="col-lg-6">
                            {!! Form::text('phone_no', $value = $company->phone_no, $attributes = array('class'=>'form-control required',
                            'placeholder'=>"Enter your Phone No.",'id'=>"phone_no")) !!}
                            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                            @if($errors->first('phone_no'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('phone_no','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('fax_no') ? 'has-error' : ''}}">
                        <label  class="col-lg-6 text-left">Fax No.</label>
                        <div class="col-lg-6">
                            {!! Form::text('fax_no', $value = $company->fax_no, $attributes = array('class'=>'form-control',
                            'placeholder'=>"Enter your Fax No.",'id'=>"fax_no")) !!}
                            <span class="glyphicon glyphicon-floppy-disk form-control-feedback"></span>
                            @if($errors->first('fax_no'))
                            <span  class="control-label">
                                <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('fax_no','') }}</em>
                            </span>
                            @endif                          
                        </div>
                    </div>

                    <div class="clearfix"><br/></div>

                    <div class="form-group">
                        <div class="col-lg-6 col-lg-offset-3">
                            <button type="submit" class="btn btn-block btn-primary"><b>Update</b></button>
                        </div>
                    </div>                    
                </fieldset>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <!--</form>-->
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@endsection <!--- content--->
