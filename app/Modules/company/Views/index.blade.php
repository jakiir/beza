@extends('layouts.admin')

@section('page_heading','List of Registered Companies')
@section('content')
<?php $accessMode=ACL::getAccsessRight('user');
if(!ACL::isAllowed($accessMode,'V')) die('no access right!');
?>

<!-- Main content -->
<section class="content">

    @if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-warning">
        {{ Session::get('error') }}				
    </div>
    @endif 

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <table id="report_list" class="table table-striped" role="grid">
                <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Company Email</th>
                        <!--<th>Status</th>-->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($getList['result'] as $row)
                    <tr>
                        <td>{!! $row->company_name !!}</td>
                        <td>{!! $row->contact_email !!}</td>
                        <!--<td>{!! $row->status==1? '<span class="label label-success">Published</span>':'<span class="label label-warning">Un-published</span>' !!}</td>-->
                        <td>
                            <i class="glyphicon glyphicon-eye-open"></i>
                            {!! link_to('company/view/'. Encryption::encodeId($row->company_id),'View',['class' => 'btn btn-primary btn-xs']) !!}
                            &nbsp;&nbsp;
                            <i class="glyphicon glyphicon-edit"></i>
                            {!! link_to('company/edit/'. Encryption::encodeId($row->company_id),'Edit',['class' => 'btn btn-info btn-xs']) !!}

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section><!-- /.content -->

@endsection

@section('footer-script')
<script>

    $(function () {
        // $("#report_list").DataTable();
        $('#report_list').DataTable({
            "paging": true,
            "lengthChange": false,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 20
        });
    });

</script>
@endsection
