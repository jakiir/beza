<meta name="csrf-token" content="{{ csrf_token() }}" />

<header>
    <div class="col-md-12 text-center">
        <img class="img-thumbnail img-responsive" src="{{ asset('front-end/images/logo.gif') }}" alt="Logo" width="15%"/>
        <h2>{!! $header !!}</h2>
    </div>
    <div class="clearfix"></div>
</header>

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="background: snow; opacity:0.7; border-radius:8px;">
            {!! $param !!}
        </div>
    </div>
</div>

