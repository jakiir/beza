@extends('layouts.admin')

@section('page_heading','<i class="fa fa-tasks"></i> Associations with the Companies')
@section('content')
<?php $accessMode=ACL::getAccsessRight('company');
if(!ACL::isAllowed($accessMode,'E')) die('no access right!');
?>
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="{{ url('users/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('company/company_associate') }}"><i class="fa fa-briefcase"></i> Organization</a></li>
    </ol>
</section>


    <section class="content">

        <!-- Default box -->
        <div class="box box-success col-md-10">
            <div class="box-header with-border">

            @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-warning">{{ Session::get('error') }}</div>
            @endif
            </div>

            <div class="box-body">
           
            <div class="box box-pane col-md-8 col-sm-8 col-md-offset-2">

                <br/>
                {!! Form::open(array('url' => '/company/get-company-name','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'associate_company_form')) !!}

                <div class="form-group has-feedback {{ $errors->has('company_id') ? 'has-error' : ''}}">
                    <label  class="col-lg-6 text-left required-star">Name of the Company / Organization</label>
                    <div class="col-lg-6">
                        <input type="text" name="company_name" id="company_name" class="form-control required"
                               placeholder="Enter the Name of the Company" 
                               value="{{$company_name}}" />
                        @if($errors->first('company_id'))
                        <span class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_id','') }}</em>
                        </span>
                        @endif     
                    </div>
                </div>


                <div class="form-group has-feedback {{ $errors->has('company_type') ? 'has-error' : ''}}">
                    <label  class="col-lg-6 text-left required-star">Organization Type</label>
                    <div class="col-lg-6">
                        {!! Form::select('company_type', $value = $company_types, $company_type, $attributes = array('class'=>'form-control required',
                        'id'=>"company_type")) !!}
                        @if($errors->first('company_type'))
                        <span class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_type','') }}</em>
                        </span>
                        @endif     
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('company_city') ? 'has-error' : ''}}">
                    <label  class="col-lg-6 text-left required-star">City of the Organization</label>
                    <div class="col-lg-6"> 
                        <input type="text" name="company_city" id="company_city" class="form-control required"
                               placeholder="Enter the Name of the Company" value="{{$company_city}}" />
                        @if($errors->first('company_city'))
                        <span class="control-label">
                            <em><i class="fa fa-times-circle-o"></i> {{ $errors->first('company_city','') }}</em>
                        </span>
                        @endif     
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-5">
                        <button type="submit" class="btn btn-lg btn-primary"><b>Search Again</b></button>
                    </div>
                </div>

                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div> <!-- End of company_details--->

            <div class="clearfix"><br/></div>

            <?php if (!empty($suggestedCompany)) { ?>
                    <table id="report_list" class="table table-striped" role="grid">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Company Address</th>                            
                                <th>Company Phone</th>                            
                                <th>Company Email</th>
                                <th>Action</th>                            
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($suggestedCompany as $row)
                            <tr>
                                {!! Form::open(array('url' => '/company/associate_company','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'associate_company_form')) !!}
                                <td>{!! $row->company_name !!}</td>
                                <td>
                                    House No:  {!! $row->company_house_no !!},                          
                                    Flat No:  {!! $row->company_flat_no !!},                          
                                    Street:  {!! $row->company_street !!},                          
                                    City: {!! $row->company_city !!}                            
                                </td>
                                <td>{!! $row->phone_no !!}</td>
                                <td>{!! $row->contact_email !!}</td>
                                <td> 
                                    <input type="hidden" name="company_id" id="company_id" value="{{ $row->company_id}}"/> 
                                    <button type="submit" class="btn btn-sm btn-success"><b>Associate</b></button>
                                </td>                          
                                {!! Form::close() !!}
                            </tr>
                            @endforeach                        
                        </tbody>
                    </table>
            <?php } else { ?>
<!--                <div class="box box-warning box-body">
                    <div class="col-md-12 text-center jumbotron">
                        <b>If you don't see your desired company in this list, then may be they are not registered in this system yet. <br/><br/>
                            Do you want to register a new company? <br/>
                            If you do, then 
                            <a href="{{ url('company/create/'.$encrypted_company_info) }}">
                                follow this link to register a new Company</a>
                        </b>
                    </div>
                </div>-->
            <?php } ?>

            <div class="clearfix"><br/><br/></div>
        </div>

        <div class="clearfix"><br/></div>

    </div>
</div>
@endsection <!--- content--->

@section('footer-script')
@endsection <!--- footer script--->