@extends('layouts.admin')

@section('page_heading','<i class="fa fa-tasks"></i> Associations with the Companies')
@section('content')
<?php $accessMode=ACL::getAccsessRight('company');
if(!ACL::isAllowed($accessMode,'E')) die('no access right!');
?>

<!-- Main content -->
<section class="content">

    @if(Session::has('success'))<div class="alert alert-success">{{ Session::get('success') }}</div>@endif
    @if(Session::has('error'))<div class="alert alert-warning"></div>@endif 

    <!-- Default box -->
    <div class="box">
        {{--<div class="box-header">--}}
            {{--<span class="col-md-11">--}}
                {{--<a href="{{ url('company/company_associate') }}">--}}
                    {{--<i class="fa fa-plus-circle"></i> --}}
                    {{--<b> Add Company / Organization </b>--}}
                {{--</a>--}}
            {{--</span>--}}
        {{--</div><!-- /.box-header -->--}}

        <div class="box-body">
            <table id="report_list" class="table table-striped" role="grid">
                <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Company Email</th>
                        <!--<th>Status</th>-->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($getList as $row)
                    <tr>
                        <td>{!! $row->company_name !!}</td>
                        <td>{!! $row->contact_email !!}</td>
                        <td>
                            <i class="glyphicon glyphicon-open"></i>
                            {!! link_to('company/view/'. Encryption::encodeId($row->company_id),'Open',['class' => 'btn btn-primary btn-xs']) !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section><!-- /.content -->

@endsection

@section('footer-script')
<script>
    $(function () {
        $('#report_list').DataTable({
            "paging": true,
            "lengthChange": false,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 20
        });
    });
</script>
@endsection
