<?php

namespace App\Modules\Company\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class CompanyType extends Model {

    protected $table = 'company_type';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'ct_id',
        'ct_name',
        'ct_status',
    ];

    /*     * ***************************************End of Model Class****************************************************** */
}
