<?php

namespace App\Modules\Company\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class CompanyAssoc extends Model {

    protected $table = 'company_assoc';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'ca_id',
        'company_id',
        'user_id',
        'ca_status'
    ];

    /*     * ***************************************End of Model Class****************************************************** */
}
