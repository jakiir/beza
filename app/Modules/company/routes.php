<?php

Route::group(array('module' => 'Company','middleware' => 'auth',  'namespace' => 'App\Modules\Company\Controllers'), function() {

        /* Routes For General Company */
    Route::get('company', 'CompanyController@index');
    Route::get('company/index', 'CompanyController@index');
    Route::get('company/create/{encrypted_company_info}', 'CompanyController@create');
    
    Route::patch('company/store', 'CompanyController@store');
    Route::patch('/company/update/{id}', "CompanyController@update");

    Route::get('/company/edit/{id}', "CompanyController@edit");
    Route::get('/company/view/{id}', "CompanyController@view");

    /* Routes For Documents uploaded for a Company */
    Route::get('company/create_doc', 'CompanyController@create_doc');

    /* Routes For Associating a User to a Company */
    Route::get('company/company_associate', 'CompanyController@company_associate');
    Route::patch('company/associate_company', 'CompanyController@associate_company');
    Route::post('company/get-company-suggestion', 'CompanyController@getCompanySuggestion');
    Route::patch('company/get-company-name', 'CompanyController@getCompanyName');

    Route::get('company/associated-company-list', 'CompanyController@associatedCompanyList');

    /*     * *************************End of Routing Group****************** */
});
