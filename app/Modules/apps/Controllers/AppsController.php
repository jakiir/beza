<?php

namespace App\Modules\Apps\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppsRequest;
use App\Libraries\CommonFunction;
use App\Modules\apps\Models\ClearanceProcessPath;
use App\Modules\Apps\Models\ClearingCertificate;
use App\Modules\Apps\Models\ProcessDoc;
use App\Modules\Apps\Models\Status;
use App\Modules\Dashboard\Models\Services;
use App\Modules\ProcessPath\Models\VisaAssistantPath;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\SbArea;
use App\Modules\Apps\Models\processVerifylist;
use App\Modules\Apps\Models\File;
use App\Modules\Users\Models\Users;
use App\Libraries\Encryption;
use App\Modules\visaAssistance\Models\VisaAssistance;
use App\Modules\workPermit\Models\workPermit;
use App\Modules\workPermit\Models\Processlist;
use App\Modules\workPermit\Models\Processpath;
use App\Modules\workPermit\Models\country;
use App\Modules\apps\Models\Document;
use Illuminate\Http\Request;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Stakeholder;
use Illuminate\Support\Facades\Auth;
use App\Modules\Apps\Models\Apps;
use DB;
use Illuminate\Support\Facades\View;
use Session;
use mPDF;

class AppsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($search_status_id = 0) {
        $search_status_id = base64_decode($search_status_id);
        Session::put('sess_user_id', Auth::user()->id);
        $getList = Apps::getAppList();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        $division = AreaInfo::where('area_type', 2)->orderBy('area_nm')->lists('area_nm', 'area_id');
        $areaList = AreaInfo::orderBy('area_nm')->lists('area_nm', 'area_id');

        $deskId = Auth::user()->desk_id;
        if ($deskId == 1) {
            $statusId = 1;
        } elseif ($deskId == 8) {
            $statusId = '2,12';
        } elseif ($deskId == 2) {
            $statusId = 6;
        } else {
            $statusId = '';
        }
        $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM app_process_path APP WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";

        $statusList2 = \DB::select(DB::raw($sql));
        $statusList = array();
        foreach ($statusList2 as $k => $v) {

            $statusList[$v->status_id] = $v->status_name;
        }
        $hqs = ['' => 'Select one'];
        return view("apps::list", compact('hqs', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id'));
    }

    public function selectApplication($id) {
        $logged_user_type = CommonFunction::getUserType();
        $servicesId = Encryption::decodeId($id);
        $selected_form_url =Services::where('id', $servicesId)->pluck('form_url');
        if ($logged_user_type == '6x606') { // for Visa Assistance Users
            $services =Services::where('id', 2)->lists('name', 'form_url');
        } else {
            $services = ['' => 'Select one'] + Services::where('is_active', 1)->where('id', '!=', 2)->orderBy('id', 'ASC')->lists('name', 'form_url')->all();
        }
        return view("apps::form-selection", compact('services','selected_form_url'));
    }

    public function selectApplicationCheck(Request $request) {
        $data = $request->all();
        $serviceId = $data['serviceId'];
        $authId = Auth::user()->id;
        $process_data = processlist::where('initiated_by', $authId)->where('service_id', $serviceId)->first(['process_list.process_id']);
        if (empty($process_data->process_id)) {
            $services = Services::where('id', $serviceId)->first(['url as formUrl']);
            $data = ['responseCode' => 1, 'url' => $services->formUrl, 'msg' => 'You are capable to apply this'];
        } else {
            $data = ['responseCode' => 0, 'url' => '', 'msg' => 'You have already applied for this application!'];
        }

        return response()->json($data);
    }

    public function listOfClearance($search_status_id = 0) {
        $search_status_id = base64_decode($search_status_id);
        Session::put('sess_user_id', Auth::user()->id);
        $getList = Apps::getClearanceList();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        $division = AreaInfo::where('area_type', 2)->orderBy('area_nm')->lists('area_nm', 'area_id');
        $areaList = AreaInfo::orderBy('area_nm')->lists('area_nm', 'area_id');

        $deskId = Auth::user()->desk_id;
        if ($deskId == 1) {
            $statusId = 1;
        } elseif ($deskId == 8) {
            $statusId = '2,12';
        } elseif ($deskId == 2) {
            $statusId = 6;
        } else {
            $statusId = '';
        }
        $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM app_process_path APP WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";

        $statusList2 = \DB::select(DB::raw($sql));
        $statusList = array();
        foreach ($statusList2 as $k => $v) {

            $statusList[$v->status_id] = $v->status_name;
        }
        $hqs = ['' => 'Select one'];
        return view("apps::list", compact('hqs', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList', 'statusList', 'search_status_id'));
    }

    public function uploadDocument() {
        return View::make('apps::ajaxUploadFile');
    }

    /* public function delegation() {
      $getList = Apps::getDelegationAppList();

      $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed',5 => 'Not Applicable'];
      $division = AreaInfo::where('area_type', 2)->orderBy('area_nm')->lists('area_nm', 'area_id');
      $areaList = AreaInfo::lists('area_nm', 'area_id');
      $userId = CommonFunction::getUserId();
      $delegated_usersArr = Users::select('desk_id', DB::raw('group_concat(desk_id) AS delegated_desk'))->where('delegate_to_user_id', $userId)->first();
      $delegated_desk = $delegated_usersArr->delegated_desk;
      $from_delegation_desk = 1;
      return view("apps::list", compact('from_delegation_desk','delegated_desk', 'getList', 'division', 'district', 'userList', 'resultList', 'areaList'));
      } */

    /**
     * @param AppsRequest $request
     * @return \Illuminate\View\View
     */
    public function store($id, Request $request, processVerifylist $process_model) {

//        $this->validate();
        $app_id = Encryption::decodeId($id);

        $unique_prefix = uniqid();

        if ($request->get('sb_status') == 1) {
            $sb_file = $request->file('sb_file');

            $verify_id = processVerifylist::where('app_id', $app_id)->where('status_id', 0)->where('sb_nsi_flag', 'sb')->limit(1)->pluck('verify_id');

            $info_data = array
                (
                'status_id' => $request->get('status_id'),
                'sb_nsi_flag' => 'sb',
                'remarks' => $request->get('sb_remarks'),
                'updated_by' => Auth::user()->id
            );


            if ($request->hasFile('sb_file')) {
//                        $file_count = count($sb_file);

                $uploadcount = 0;
                foreach ($sb_file as $file) {
                    $original_file = $file->getClientOriginalName();
                    $file->move('uploads', $unique_prefix . $original_file);
//                        $file_data['attachment'] = $unique_prefix . $original_file;
                    File::create([
                        'app_id' => $app_id,
                        'verify_id' => $verify_id,
                        'sb_nsi_flg' => 'sb',
                        'attachment' => $unique_prefix . $original_file
                    ]);
                    $uploadcount ++;
                }
            }

            $process_model->process_methods($app_id, $info_data, 'sb');
        }
        if ($request->get('nsi_status') == 1) {
            $nsi_file = $request->file('nsi_file');

            $verify_id = processVerifylist::where('app_id', $app_id)->where('status_id', 0)->where('sb_nsi_flag', 'nsi')->limit(1)->pluck('verify_id');

            $info_data = array
                (
                'status_id' => $request->get('status_id'),
                'sb_nsi_flag' => 'nsi',
                'remarks' => $request->get('nsi_remarks'),
                'updated_by' => Auth::user()->id
            );

            if ($request->hasFile('nsi_file')) {
                $uploaddcount = 0;
                foreach ($nsi_file as $nfile) {
                    $nsioriginal_file = $nfile->getClientOriginalName();
                    $nfile->move('uploads', $unique_prefix . $nsioriginal_file);
//                $info_data['attachment'] = $unique_prefix . $nsioriginal_file;
                    File::create([
                        'app_id' => $app_id,
                        'verify_id' => $verify_id,
                        'sb_nsi_flag' => 'nsi',
                        'attachment' => $unique_prefix . $nsioriginal_file
                    ]);
                    $uploaddcount ++;
                }
            }
            $process_model->process_methods($app_id, $info_data, 'nsi');
        }

        Session::flash('success', 'Successfully Updated the Information.');
        return redirect('apps/list-of-clearance/project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function view($id) {

        $app_id = Encryption::decodeId($id);

        $alreadyExistApplicant = ProjectClearance::where('id', $app_id)->first();

        $form_data = ClearingCertificate::where('id', $app_id)->first();
        $document = Document::where('id', $form_data->app_id)->get();
        $process_data = processlist::where('record_id', $app_id)->where('service_id', 1)->first();

        $process_history = DB::select(DB::raw("select `process_list_hist`.`desk_id`,`as`.`status_name`,
                                `process_list_hist`.`process_id`, 
                                if(`process_list_hist`.`desk_id`=0,\"-\",`ud`.`desk_name`) `deskname`,
                                `users`.`user_full_name`, 
                                `process_list_hist`.`updated_by`, 
                                `process_list_hist`.`status_id`, 
                                `process_list_hist`.`process_desc`,
                                `process_list_hist`.`process_desc`, 
                                `process_list_hist`.`record_id`, 
                                `process_list_hist`.`created_at` ,
                                group_concat(`pd`.`file`) as files
                                from `process_list_hist`
                                left join `user_desk` as `ud` on `process_list_hist`.`desk_id` = `ud`.`desk_id`
                                left join `users` on `process_list_hist`.`updated_by` = `users`.`id`
                                left join `process_documents` as `pd` on `process_list_hist`.`record_id` = `pd`.`app_id` and `process_list_hist`.`desk_id` = `pd`.`desk_id` and `process_list_hist`.`status_id` = `pd`.`status_id`
                                left join `app_status` as `as` on `process_list_hist`.`status_id` = `as`.`status_id`
                                where `process_list_hist`.`record_id`  = '$app_id' 
                                and `process_list_hist`.`process_type` = 0
                    group by `process_list_hist`.`record_id`,`process_list_hist`.`desk_id`, `process_list_hist`.`status_id`"));
        
        if (CommonFunction::getDeskId() == 6)
            $verify_data = processVerifylist::where('app_id', $process_data->record_id)->where('sb_nsi_flag', 'sb')->orderBy('created_at', 'desc')->limit(1)->first();

        if (CommonFunction::getDeskId() == 7)
            $verify_data = processVerifylist::where('app_id', $process_data->record_id)->where('sb_nsi_flag', 'nsi')->orderBy('created_at', 'desc')->limit(1)->first();
//         $avr_data  check for include page
        $avr_data = \DB::table('apps_verify_result')->where('app_id', $app_id)->orderBy('verify_id', 'desc')->limit(1)->first();

        $sb_verify_list = processVerifylist::where('app_id', $process_data->record_id)
                        ->where('sb_nsi_flag', 'sb')->where('status_id', '>', 0)->orderBy('created_at', 'desc')->limit(1)->get();

        $nsi_verify_list = processVerifylist::where('app_id', $process_data->record_id)
                        ->where('status_id', '!=', '-1')
                        ->where('status_id', '!=', '0')
                        ->where('sb_nsi_flag', 'nsi')->orderBy('created_at', 'desc')->limit(1)->get();

//        $nsifile_list = File::where('app_id', $process_data->record_id)->where('sb_nsi_flag', '=', 'nsi')->where('created_by', '>', Auth::user()->id)->orderBy('created_at', 'desc')->get();
//        $sbfile_list = File::where('app_id', $process_data->record_id)->where('sb_nsi_flag', '=', 'sb')->where('created_by', '>', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        if (isset($sb_verify_list[0])) {
            $sbfile_list = File::where('verify_id', $sb_verify_list[0]->verify_id)->orderBy('created_at', 'desc')->get();
        }
        if (isset($nsi_verify_list[0])) {
            $nsifile_list = File::where('verify_id', $nsi_verify_list[0]->verify_id)->orderBy('created_at', 'desc')->get();
        }
        $statusId = $process_data->status_id;
        $deskId = $process_data->desk_id;
        $statusTo = ClearanceProcessPath::select('status_to')->where('status_from', $statusId)->where('desk_from', $deskId)->get();

        $nationality = country::lists('nationality', 'country_code');

        $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE APS.service_id = 1
                        AND
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM app_process_path APP WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";
        // echo $sql;exit;
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        $statusList2 = \DB::select(DB::raw($sql));
        $statusList = array();
        foreach ($statusList2 as $k => $v) {

            $statusList[$v->status_id] = $v->status_name;
        }


        $statusArray = Status::lists('status_name', 'status_id');
        return view("apps::view", compact('app_id', 'avr_data', 'document', 'process_history', 'alreadyExistApplicant', 'resultList', 'nsifile_list', 'sbfile_list', 'form_data', 'nationality', 'statusList', 'statusId', 'process_data', 'verify_data', 'sb_verify_list', 'nsi_verify_list', 'statusArray'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    public function update($id, Request $request, processlist $process_model, workPermit $workPermit_model) {
//        dd($request->all());

        $app_id = Encryption::decodeId($id);
        $status_id = $request->get('status_id');
        $status_from = $request->get('status_from');
        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $desk_id = $request->get('desk_id');
        $process_type = 1;
        $onbehalf = $request->get('on_behalf_of');
        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }

//        dd($desk_id);

        if (empty($desk_id)) {
            $whereCond = "select * from app_process_path where status_from = '$status_from' AND desk_from = '$deskFrom' 
                        AND process_type = 1 
                        AND status_to like '%$status_id%'";
            $processPath = DB::select(DB::raw($whereCond));

            if ($processPath[0]->desk_to == '0')
                $desk_id = 0;
            if ($processPath[0]->desk_to == '-1')
                $desk_id = $deskFrom;
        }

        $app_data = array
            (
            'status_id' => $status_id,
            'remarks' => $remarks,
            'updated_at' => date('y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        $info_data = array
            (
//            'company_id' => CommonFunction::getFieldName(Auth::user()->id, 'user_id', 'company_id', 'company_assoc'),
            'desk_id' => $desk_id,
            'status_id' => $status_id,
            'process_desc' => $remarks,
            'updated_by' => Auth::user()->id
        );

        if ($status_id == 8 || $status_id == 14) {
            $info_data['closed_by'] = Auth::user()->id;
        }
        $process_model->update_app($app_id, $info_data);
        $workPermit_model->update_method($app_id, $app_data);
        workPermit::where('app_id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);

        $_data = workPermit::where('app_id', $app_id)->first();
        $process_data = Processlist::where('record_id', $app_id)->first();
        $fetched_email_address = "'" . CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users') . "'";

        $body_msg = '<span style="color:#000;text-align:justify;"><b>';
        if ($status_id == 8) {
            $body_msg .= 'Sorry!</b><br/><br/>';
            $body_msg .= 'Your application for work permit has been Discard.';
        } elseif ($status_id == 9) {
            $body_msg .= 'Congratulations!</b><br/><br/>';
            $body_msg .= 'Your application for work permit has been ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status');
        } else {
            $body_msg .= 'Congratulations!</b><br/><br/>';
            $body_msg .= 'Your application for work permit has been ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status');
        }
        $body_msg .= '</span>';
        $body_msg .= '<br/><br/><br/>Thanks<br/>';
        $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';
        $pathToFile = 'uploads';
        $display = 'clearence.pdf';
        $mime = 'application/pdf';
        $data = array(
            'header' => 'Application Update',
            'param' => $body_msg
        );

        if ($status_id == 8) {

            \Mail::send('users::message', $data, function($message) use ($fetched_email_address) {
                $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->cc('khaleda@batworld.com')
                        ->subject('Application Update Information');
            });
        }
        if ($status_id == 9) {

            $certificate = $this->certificate_gen($app_id);
            $fetched_email_address_arr = ['khaleda4sabina@gmail.com', 'khaleda@queue-pro.com'];
            workPermit::where('app_id', $app_id)->update(['certificate' => $certificate]);


            \Mail::send('users::message', $data, function($message) use ($fetched_email_address_arr, $certificate) {
                $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address_arr)
                        ->cc('khaleda@batworld.com')
                        ->attach($certificate)
                        ->subject('Application Update Information');
            });
        }

        if ($status_id == 14) {

            //$file_name = file_get_contents('http://192.168.152.156/bangla_pdf/generate.php?app_id='.$app_id);
            //Pdf generate
            $fileName = $this->pdf_gen($app_id);
//            dd($file_name);   f
            DB::table('work_permit_form')->where('app_id', $app_id)->update(
                    [
                        'certificate_file_name' => $fileName
                    ]
            );
            //Pdf generate end

            $stack_info = Stakeholder::where('process_id', 1)->get()->toArray();

            foreach ($stack_info as $value) {
                $fetched_email_address .= ',' . "" . $value['stholder_email'] . "";
            }
            $fetched_email_address_arr = explode(",", $fetched_email_address);
//          $fetched_email_address = [$fetched_email_address];

            \Mail::send('users::message', $data, function($message) use ($fetched_email_address_arr, $fileName) {
                $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address_arr)
                        ->cc('khaleda@batworld.com')
//                    ->cc(array('khaleda@batworld.com', 'kaium@batworld.com'))
                        ->attach($fileName)
                        ->subject('Application Update Information');
            });
//        \Mail::send('users::message', $data, function($message) use ($fetched_email_address) {
//            $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
//                    ->to($emails)
//                    ->cc('khaleda@batworld.com')
//                    ->attach($pathToFile, array('as' => $display, 'mime' => $mime))
//                    ->subject('Application Update Information');
//        });
        }

        Session::flash('success', 'Successfully Updated the Information.');
        return redirect('apps/list-of-clearance/project');
    }

    /**
     * @param Request $request
     * @param Processlist $process_model
     * @param ProjectClearance $ProjectClearance_model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateBatch(Request $request, processlist $process_model, VisaAssistance $visa_assistance_model,ProjectClearance $ProjectClearance_model) {
//        dd($request->all());

        $deskFrom = Auth::user()->desk_id;
        $remarks = $request->get('remarks');
        $apps_id = $request->get('application');
        $desk_id = $request->get('desk_id');
        $service_id = $request->get('service_id');
        $status_id = $request->get('status_id');
        $from_delegation_desk = $request->get('from_delegation_desk');
        $attach_file = $request->file('attach_file');
        $process_type = 1;
        $onbehalf = $request->get('on_behalf_of');
        $on_behalf_of = 0;
        if (!empty($onbehalf)) {
            $on_behalf_of = $request->get('on_behalf_of');
        }
        $increment = 0;
        foreach ($apps_id as $app_id) {
          echo $service_id[$app_id];
            if ($request->hasFile('attach_file')) {
                foreach ($attach_file as $afile) {
                    $original_file = $afile->getClientOriginalName();
                    $afile->move('uploads/', time() . $original_file);
                    $file = new ProcessDoc;
                    $file->app_id = $app_id;
                    $file->desk_id = $desk_id;
                    $file->status_id = $status_id;
                    $file->file = 'uploads/' . time() . $original_file;
                    $file->save();
                }
            }

            $appInfo = Processlist::where('record_id', $app_id)->first();
            $status_from = $appInfo->status_id;
            $deskFrom = $appInfo->desk_id;
            if ($service_id[$app_id] == 1) {
                $email_sql = "select * from app_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'                        
                        AND status_to like '%$status_id%'";
            } elseif ($service_id[$app_id] == 2) {
                $email_sql = "select * from visa_assistant_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'                        
                        AND status_to like '%$status_id%'";
            }

            $email_config = DB::select(DB::raw($email_sql));
            if (empty($desk_id)) {
                if ($service_id[$app_id] == 1) {
                    $whereCond = "select * from app_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'                        
                        AND status_to like '%$status_id%'";
                } elseif ($service_id[$app_id] == 2) {
                    $whereCond = "select * from visa_assistant_process_path where status_from = '$status_from' AND desk_from = '$deskFrom'                        
                        AND status_to like '%$status_id%'";
                }


                $processPath = DB::select(DB::raw($whereCond));
//                 dd($processPath);
                if ($processPath[0]->desk_to == '0')
                    $desk_id = 0;
                if ($processPath[0]->desk_to == '-1')
                    $desk_id = $deskFrom;
                if ($status_id == '-1')
                    $desk_id = $deskFrom;
            }
            $app_data = array
            (
                'status_id' => $status_id,
                'remarks' => $remarks,
                'updated_at' => date('y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            );

            if ($from_delegation_desk == 1) {   /// We may work next

            }
            $info_data = array
            (
                'desk_id' => $desk_id,
                'status_id' => $status_id,
                'process_desc' => $remarks,
                'updated_by' => Auth::user()->id,
                'on_behalf_of_desk' => $deskFrom
            );

            if ($status_id == 8 || $status_id == 14) {
                $info_data['closed_by'] = Auth::user()->id;
            }
            $process_model->update_app($app_id, $info_data,$service_id[$app_id]);



            if ($service_id[$app_id] == 1) {
                $ProjectClearance_model->update_method($app_id, $app_data);
                ProjectClearance::where('id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);
            } elseif ($service_id[$app_id] == 2) {
                $visa_assistance_model->update_method($app_id, $app_data);
                VisaAssistance::where('id', $app_id)->update(['status_id' => $status_id, 'remarks' => $remarks]);
            }


            $process_data = Processlist::where('record_id', $app_id)->first();


//            Notification

            $body_msg = '<span style="color:#000;text-align:justify;"><b>';

            $body_msg .= 'Your application for ' . $process_data->track_no . ' has been ' . CommonFunction::getFieldName($request->get('status_id'), 'status_id', 'status_name', 'app_status');

            $body_msg .= '</span>';
            $body_msg .= '<br/><br/><br/>Thanks<br/>';
            $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';

            $data = array(
                'header' => 'Application Update',
                'param' => $body_msg
            );
            $fetched_email_address = "'" . CommonFunction::getFieldName($process_data->initiated_by, 'id', 'user_email', 'users') . "'";
            if ($status_id == 5 || $status_id == 8 || $status_id == 23) {


                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address) {
                    $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->cc('shahin@batworld.com')
                        ->subject('Application Update Information for project clearance');
                });
            }


            //PDF
            if ($status_id == 23) {
                $certificate = $this->certificate_gen($app_id,$service_id[$app_id]);

                if ($service_id[$app_id] == 1) {
                    ProjectClearance::where('id', $app_id)->update(['certificate' => $certificate]);
                } elseif ($service_id[$app_id] == 2) {
                    VisaAssistance::where('id', $app_id)->update(['certificate' => $certificate]);
                }


                \Mail::send('users::message', $data, function ($message) use ($fetched_email_address, $certificate) {
                    $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->cc('shahin@batworld.com')
                        ->attach($certificate)
                        ->subject('Application Update Information');
                });
                $increment++;
            }
        }
        Session::flash('success', 'Successfully Updated the Information.');
        return redirect()->back();
    }

    function get_last_query() {
        $queries = DB::getQueryLog();
        $sql = end($queries);

        if (!empty($sql['bindings'])) {
            $pdo = DB::getPdo();
            foreach ($sql['bindings'] as $binding) {
                $sql['query'] = preg_replace('/\?/', $pdo->quote($binding), $sql['query'], 1);
            }
        }

        return $sql['query'];
    }

    /**
     * @param $param request type for ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxRequest($param, Request $request, workPermit $workPermitModel) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $curr_app_id = $request->get('curr_app_id');
        $current_service_id = $request->get('current_service_id');

        $processType = 1;

        if ($param == 'process') {

            $processInfo = Processlist::where('record_id', $curr_app_id)->first();

            //Set from any application desk_id and status_id. Not login user desk id
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            $deskId = $processInfo->desk_id; //Auth::user()->desk_id;
            if ($current_service_id == 1){
                $verifiedInfo = ClearingCertificate::where('id', $curr_app_id)->first();
                $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM app_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            }elseif ($current_service_id == 2){
                $verifiedInfo = VisaAssistance::where('id', $curr_app_id)->first();
                $sql = "SELECT DGN.desk_id, DGN.desk_name
                        FROM user_desk DGN
                        WHERE
                        find_in_set(DGN.desk_id,
                        (SELECT desk_to FROM visa_assistant_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
                            AND APP.status_from = '$statusFrom' AND APP.status_to REGEXP '^([0-9]*[,]+)*$statusId([,]+[,0-9]*)*$')) ";

            }


            // Get all applications' id

            // If not verified, give them a message that without verification, application can't be updated
            // adding leading zero for like condition
            $statusId_wz = sprintf("%02d", $statusId);



            //echo $sql;exit;
            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $tmpDeskId = $v->desk_id;
                $list[$tmpDeskId] = $v->desk_name; //. '( ' . $v->user_full_name . ' )';
            }
            if ($current_service_id == 1) {
                $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM app_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";
            }  elseif ($current_service_id == 2){
                $fileattach_flug = "SELECT APP.id, APP.FILE_ATTACHMENT FROM visa_assistant_process_path APP WHERE APP.desk_from LIKE '%$deskId%'
            AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' limit 1";
            }
            $fileattach_flug_data = \DB::select(DB::raw($fileattach_flug));

            $data = ['responseCode' => 1, 'data' => $list, 'status_to' => $statusId, 'status_from' => $statusFrom, 'desk_from' => $deskId,
                'file_attach' => $fileattach_flug_data[0]->FILE_ATTACHMENT];
        } elseif ($param == 'load-district') {
            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-user-list') {
            $division = $request->get('division');
            $district = $request->get('district');
            $desk_id = Auth::user()->desk_id;
            if ($desk_id == 3 || $desk_id == 9)
                $verifyDesk = 6;
            if ($desk_id == 4 || $desk_id == 10)
                $verifyDesk = 7;

            $userlist = Users::where('district', $division)->where('thana', $district)->where('desk_id', $verifyDesk)->where('id', '<>', Auth::user()->id)
                    ->get(['id', 'user_full_name']);
            $data = ['responseCode' => 1, 'data' => $userlist];
        } elseif ($param == 'load-user-list_by_team') {
            $sectionId = $request->get('sectionId');
            $teamId = $request->get('teamId');
            $desk_id = Auth::user()->desk_id;
            if ($desk_id == 3 || $desk_id == 9)
                $verifyDesk = 6;
            if ($desk_id == 4 || $desk_id == 10)
                $verifyDesk = 7;

            $userlist = Users::where('section_id', $sectionId)->where('team_id', $teamId)->where('desk_id', $verifyDesk)->where('id', '<>', Auth::user()->id)
                    ->get(['id', 'user_full_name']);
            $data = ['responseCode' => 1, 'data' => $userlist];
        }elseif ($param == 'load-status-list') {
             $current_service_id = $request->get('current_service_id');
            $statusId = $request->get('curr_status_id');
            $delegate = $request->get('delegate');
            if (empty($delegate)) {
//                $user_id = Users::where('delegate_to_user_id', Auth::user()->id)->pluck('delegate_by_user_id');
//                $deskId = Users::where('id', $user_id)->pluck('desk_id');
                $deskId = Auth::user()->desk_id;
                $cond = "AND desk_from LIKE '%$deskId%'";
            } else {
                $cond = '';
            }

            $processInfo = Processlist::where('record_id', $curr_app_id)->first();
            $statusFrom = $processInfo->status_id; //$request->get('status_from');
            if ($current_service_id == 1){
                $verifiedInfo = ClearingCertificate::where('id', $curr_app_id)->first();
                $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM app_process_path APP WHERE APP.status_from = '$statusId' $cond))
                        ";

            }elseif ($current_service_id == 2){
                $verifiedInfo = VisaAssistance::where('id', $curr_app_id)->first();
                $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM visa_assistant_process_path APP WHERE APP.status_from = '$statusId' $cond))
                        ";

            }



            $statusList = \DB::select(DB::raw($sql));


            if ($statusFrom == 9 && ($verifiedInfo->sb_gk_verification_status == 0 && $verifiedInfo->nsi_gk_verification_status == 0)) {
                $data = ['responseCode' => 5, 'data' => ''];
            } else {
                $data = ['responseCode' => 1, 'data' => $statusList];
            }
        }
        return response()->json($data);
    }

    public function ajaxRequest_back($param, Request $request) {
        $data = ['responseCode' => 0];
        $statusId = $request->get('id');
        $statusFrom = $request->get('status_from');
        $deskId = Session::get('user_type');
        $processType = 1;

        if ($param == 'process') {

            $sql2 = "SELECT DGN.desk_id, DGN.desk_name
                FROM user_desk DGN
                WHERE
                find_in_set(DGN.desk_id,
                (SELECT desk_to FROM app_process_path APP WHERE APP.desk_from LIKE '%$deskId%' AND APP.status_from = '$statusFrom' AND APP.status_to LIKE '%$statusId%' AND APP.process_type = '$processType')) ";

            $sql = "SELECT user_full_name, desk_name "
                    . "from users ui INNER JOIN "
                    . "($sql2) desg ON ui.desk_id = desg.desk_id "
                    . " WHERE ui.user_status = 'active'";
//            echo $sql;exit;
            $deskList = \DB::select(DB::raw($sql));
            $list = array();
            foreach ($deskList as $k => $v) {

                $list[$k] = $v->desk_name . '( ' . $v->user_full_name . ' )';
            }

            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'load-district') {
            $division = $request->get('division');
            $districts = AreaInfo::where('pare_id', $division)->orderBy('area_nm')->get(['area_id', 'area_nm']);
            $data = ['responseCode' => 1, 'data' => $districts];
        } elseif ($param == 'load-user-list') {
            if (Auth::user()->desk_id == 3 || Auth::user()->desk_id == 9) {
                $verifyDesk = 6;
            } elseif (Auth::user()->desk_id == 4 || Auth::user()->desk_id == 10) {
                $verifyDesk = 7;
            }

            $division = $request->get('division');
            $district = $request->get('district');
            $desk_id = Auth::user()->desk_id;
            $userList = Users::where('district', $division)
                    ->where('thana', $district)
                    ->where('desk_id', $verifyDesk)
                    ->where('id', '<>', Auth::user()->id)
                    ->get();
//          print_r($userlist);exit;
            $data = ['responseCode' => 1, 'data' => $userList];
        }
        return response()->json($data);
    }

    public function getDistrictAreaData(Request $request) {
        $id = $request->get('id');
        $list = AreaInfo::orderBy('area_nm')->where('pare_id', $id)->get();
        $data = ['responseCode' => 1, 'data' => $list];
        if ($id) {
            return response()->json($data);
        } else {
            return '';
        }
    }

    public function assignDesks(Request $request) {

        $gkDeskId = Auth::user()->desk_id;

        if ($gkDeskId == 3)
            $flag = 'sb';
        if ($gkDeskId == 4)
            $flag = 'nsi';
        if ($gkDeskId == 9)
            $flag = 'sb'; // sb dig
        if ($gkDeskId == 10)
            $flag = 'nsi'; // nsi director

        if ($flag == '')
            return redirect("apps/list-of-clearance/project");

        $division = $request->get('division');
        $district = $request->get('district');
        $user_id = $request->get('user_id');
        $status_id = $request->get('status_id');
        $gk_status = $request->get('gk_status');
        $gk_remarks = $request->get('gk_remarks');
        $app_id = $request->get('application');



        $desk_id = CommonFunction::getFieldName($user_id, 'id', 'desk_id', 'users');

        foreach ($app_id as $value) {
            $initiated_by = processlist::where('record_id', $value)->limit(1)->pluck('initiated_by');

            processVerifylist::where('app_id', $value)
                    ->where('status_id', 0)
                    ->where('sb_nsi_flag', $flag)
                    ->update(['status_id' => -1]);


            if ($request->get('submit') == 'Process') {
                if ($gkDeskId == 3 || $gkDeskId == 9) {
                    $gk_update = array(
                        'sb_gk_verification_status' => $gk_status,
                        'sb_gk_verification_remarks' => $gk_remarks,
                    );
                }
                if ($gkDeskId == 4 || $gkDeskId == 10) {
                    $gk_update = array(
                        'nsi_gk_verification_status' => $gk_status,
                        'nsi_gk_verification_remarks' => $gk_remarks,
                    );
                }
                $data = ProjectClearance::where('id', $value)->update($gk_update);

                Session::flash('success', 'Successfully completing the task.');
            } elseif ($request->get('submit') == 'Submit') {

                processVerifylist::create([
                    'app_id' => $value,
                    'desk_id' => $desk_id,
                    'user_id' => $user_id,
                    'sb_nsi_flag' => $flag,
                    'updated_by' => CommonFunction::getUserId()
                ]);
                Session::flash('success', 'Successfully Assigned the Application.');
            }

            /*
             * Email send to assigned user
             */
            $body_msg = '<span style="color:#000;text-align:justify;">';
            $body_msg .= 'You are assigned this application please check this...';
            $body_msg .= '</span>';
            $body_msg .= '<br/><br/><br/>Thanks<br/>';
            $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA)</b>';
            $data = array(
                'header' => 'Application Assigned Information',
                'param' => $body_msg
            );

            $fetched_email_address = CommonFunction::getFieldName($initiated_by, 'id', 'user_email', 'users');

            \Mail::send('users::message', $data, function($message) use ($fetched_email_address) {
                $message->from('no-reply@moha.gov.bd', 'Bangladesh Economic Zones Authority (BEZA)')
                        ->to($fetched_email_address)
                        ->cc('khaleda@batworld.com')
                        ->subject('Application Assigned Information');
            });
        }

        return redirect("apps/list-of-clearance/project");
    }

    public function certificate($id) {
        $app_id = Encryption::decodeId($id);
        return view('apps::clearence-certificate');
    }

    public function ReGenerateCertificate($id) {
//        $app_id = Encryption::decodeId($id);
//        //$file = Clearence::where('app_id', $app_id)->pluck('file_name');
//        $pdf_data = workPermit::where('app_id', $app_id)->get();
//
//        $pdf = PDF::loadView('apps::clearence-certificate', array('pdf_data' => $pdf_data))->save('uploads/app_clearence/' . $app_id . '.pdf');
//
//              Clearence::where('app_id',$app_id)->update(
//                  [
//                      'file_name' => $app_id . '.pdf',
//                      'created_by' => Auth::user()->id,
//                      'created_at' => date('y-m-d H:i:s'),
//                  ]
//              );
//        return Response::download('uploads/app_clearence/' .$file);
        $this->pdf_gen();
    }

    public function prints($id) {

        $app_id = Encryption::decodeId($id);
        $form_data = workPermit::where('app_id', $app_id)->first();
        $process_data = processlist::where('record_id', $app_id)->where('process_type', 1)->first();

        if (CommonFunction::getDeskId() == 6)
            $verify_data = processVerifylist::where('app_id', $process_data->record_id)->where('sb_nsi_flag', 'sb')->orderBy('created_at', 'desc')->limit(1)->first();
        if (CommonFunction::getDeskId() == 7)
            $verify_data = processVerifylist::where('app_id', $process_data->record_id)->where('sb_nsi_flag', 'nsi')->orderBy('created_at', 'desc')->limit(1)->first();

        $statusId = $process_data->status_id;
        $deskId = $process_data->desk_id;
        $statusTo = processpath::where('process_type', 1)->select('status_to')->where('status_from', $statusId)->where('desk_from', $deskId)->get();

        $nationality = country::lists('nationality', 'country_code');

        $sql = "SELECT APS.status_id, APS.status_name
                        FROM app_status APS
                        WHERE
                        find_in_set(APS.status_id,
                        (SELECT GROUP_CONCAT(status_to) FROM app_process_path APP WHERE APP.status_from = '$statusId' AND desk_from LIKE '%$deskId%'))
                        ";
        // echo $sql;exit;
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        $statusList2 = \DB::select(DB::raw($sql));
        $statusList = array();
        foreach ($statusList2 as $k => $v) {

            $statusList[$v->status_id] = $v->status_name;
        }

        $statusArray = Status::lists('status_name', 'status_id');

        return view('apps::print-app', compact('form_data', 'process_data'));
    }

    public function pdf_gen($appID) {
        ini_set('memory_limit', '99M');
        $form_data = workPermit::where('app_id', $appID)->first();
        $process_data = Apps::where('record_id', $appID)->first();
        $desk_data = Users::where('desk_id', 2)->first();

        $currentDate = date('j F Y', strtotime(date('y-m-d')));

        $engDATE = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');
        $bangDATE = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মারচ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', '
        বুধবার', 'বৃহস্পতিবার', 'শুক্রবার');
        $convertedDATE = str_replace($engDATE, $bangDATE, $currentDate);
        $track_no = str_replace($engDATE, $bangDATE, $process_data->track_no);

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        $desk_name = CommonFunction::getFieldName(2, 'desk_id', 'desk_name', 'user_desk');
        $nationality = CommonFunction::getFieldName($form_data->NATIONALITY, 'country_code', 'nationality', 'country_info');

        if (!empty($desk_data->signature)) {
            $signature = '<img src="users/signature/' . $desk_data->signature . '" width="100" height="50">';
        } else {
            $signature = '';
        }
//dd($form_data->sb_gk_verification_status);
        if ($form_data->sb_gk_verification_status == 3) {
            $status = ' আপত্তি (Objection)';
            $remarks = $form_data->sb_gk_verification_remarks;
        } elseif ($form_data->sb_gk_verification_status == 4) {
            $status = ' কালো তালিকাভুক্ত(Black Listed)';
            $remarks = $form_data->sb_gk_verification_remarks;
        } elseif ($form_data->sb_gk_verification_status == 2) {
            $status = ' অনাপত্তি (No Objection)';
            $remarks = '';
        } elseif ($form_data->sb_gk_verification_status == 5) {
            $status = ' প্রযোজ্য নয় (Not Applicable)';
            $remarks = $form_data->sb_gk_verification_remarks;
        }

//        dd($signature);

        $banglas = <<<HERE
            <html>
            <head>
                <style>
                   .inner table { border-collapse:none; border: 1px solid black; }
                   .inner td { border:1px solid black; }
                </style>
            </head>
<body>
<table border="0" cellpadding="10" cellspacing="1" style="font-size: 12pt;width:100%;">
                      <tbody>
                      <tr>
                        <td align="center" colspan="4">
        <style> body { font-family: solaimanlipi; }</style><img src='assets/images/logo.gif' width='75' align='center'/>
        </td></tr><tr><td colspan="4" align="center">
      গণপ্রজাতন্ত্রী   বাংলাদেশ  সরকার
        <br/>
        স্বরাষ্ট্র মন্ত্রালয়
        <br/>
        নিরাপত্তা শাখা - ২
        </td><br/><br/>
        </tr>
<tr>
        <td colspan="3">
        নম্বর   : <span style="text-align: justify;">$track_no </span>
          </td>
          <td width="20%" colspan="1" style="text-align: right;">
         তারিখ: $convertedDATE
        </td></tr>
        <tr><td colspan="4"> বিষয় :   বিদেশি   নাগরিক /  নাগরিকদের নিরাপত্তা ছারপত্র প্রদান
        </td>
        </tr>
        <tr>
          <td colspan="4">সুত্র : বিনিয়োগ বোড এর  $form_data->ENTRY_DT তারিখের  $form_data->track_no নম্বর পত্র ।</td>
        </tr>
        <tr><td colspan="4" style="text-align: justify;"><p>
          উপযক্ত  বিষয়ে সুত্রক্ত পত্রের প্রেক্ষিতে নিম্মবরনিত বিদেশী নাগরিক/ নাগরিকদের  পারশে বরনিত সংস্থা /প্রতিশঠানে চাকুরীতে নিয়োগ / নিরাপত্তার বিষয়ে  স্বরাষ্ট্র মন্ত্রনালয়ের$status $remarks  নিরদেশক্রমে জ্ঞাপন করা হল :<br/><br/><br/>
        </p>
            <table width="631" border="0" align="center" class="inner">
              <tr>
                <td>SL.</td>
                <td>Name &amp; Passport No.</td>
                <td>Nationality</td>
                <td>Organization</td>
              </tr>
              <tr>
                <td>1.</td>
                <td>$form_data->EXP_NAME <br/>(PP No. $form_data->PASSPORT_NO )</td>
                <td align="center">$nationality</td>
                <td>M/S. $form_data->ORG_NAME </td>
              </tr>
            </table>
            <p>&nbsp; </p></td><br/><br/></tr>
            <tr>
  <td align="left" style="text-align: left;" valign="top">নিরবাহি চেয়ারম্যান <br/>বিনিয়োগ বোরড,ঢাকা ।
    <br/><br/>  নম্বর   :$track_no
  <br/><br/>অনুলিপি অবগতি ও প্রয়োজনীয় বেবস্থা গ্রহণের জন্য প্রেরণ করা হল :<br/><br/>
  ১। মহাপরিচালক, এন এস  আই ,ঢাকা ।<br/>
  ২। মহাপরিচালক, বহিরাগমন  ও পাসপোরট   অধিদপ্তর  , ঢাকা ।<br/>
  ৩। অতিরিক্ত মহাপলিশ ,বিশেষ শাখা , ঢাকা ।<br/><br/>
</td>
  <td colspan="3" style="text-align: right;" valign="top">

<br/><br/>
$signature
  <br/>($desk_data->user_full_name ) <br/><br/> $desk_name <br/>  ফোন : $desk_data->user_phone
  <br/><br/><br/><br/>
   তারিখ: $convertedDATE
   <br/><br/><br/>
   $signature
   <br/>
   ($desk_data->user_full_name ) <br/><br/> $desk_name
  </td>
  </tr>
        </tbody>
        </table>
        </body>
        </html>
HERE;

//        $banglas = "\xE0\xA6\x96\xE0\xA6\xBE\xE0\xA6\xB2\xE0\xA6\xA6\xE0\xA7\x8B\x20\xE0\xA6\xAC\xE0\xA6\xBF\xE0\xA6\xA8\xE0\xA6\xA4\xE0\xA7\x87\x20\xE0\xA6\x96\xE0\xA6\xBE\xE0\xA6\xB2\xE0\xA6\x95\xE0\xA7\x87";
        $bangla = CommonFunction::convertUTF8($banglas);


        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("moha_" . $appID . "_", true);

//        $mpdf->baseScript = 1;
//        $mpdf->autoVietnamese = false;
//        $mpdf->autoArabic = false;
//        $mpdf->autoScriptToLang = false;
//        $mpdf->list_auto_mode = 'mpdf';
//        $mpdf->autoLangToFont = true;
//        $mpdf->list_indent_first_level = 0;
//        $mpdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
//        $mpdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to
//        // PDF header content
//dd($pdfFilePath);
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');

        $mpdf->SetFooter('<div style="font-size:11px;bottom: 0mm;"><barcode code="' . $process_data->track_no . '" type="CODABAR"/></div>');
        //$mpdf->defaultfooterfontsize = 10;
        //$mpdf->defaultfooterfontstyle = 'B';
        // $mpdf->defaultfooterline = 0;
        $mpdf->SetCompression(true);
        $mpdf->WriteHTML($banglas);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
//        $mpdf->Output($certificateName.'.pdf','D'); // For Download
//        $content = $mpdf->Output($pdfFilePath, 'S'); // Saving pdf to attach to email
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf makkk
        //     $mpdf->Output();
        //  exit;

        return $pdfFilePath;
    }

    public function certificate_gen($appID,$service_id) {
        ini_set('memory_limit', '99M');
        if ($service_id == 1){
            $form_data = ClearingCertificate::where('id', $appID)->first();
        }
        elseif ($service_id == 2){
            $form_data = VisaAssistance::where('id', $appID)->first();
        }
        $process_data = Apps::where('record_id', $appID)->first();
        $desk_data = Users::where('desk_id', Auth::user()->desk_id)->first();
        $nationality = CommonFunction::getFieldName($form_data->nationality, 'country_code', 'nationality', 'country_info');
        $currentDate = date('j F Y', strtotime(date('Y-m-d')));
//        $permit_effect_date = date('j F Y', strtotime($form_data->PERMIT_EFCT_DATE));
//        $after_permit_date = date('j F Y', strtotime("+1 month",strtotime($currentDate)));

        $url = url();
        $engDATE = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');
        $bangDATE = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মারচ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', '
        বুধবার', 'বৃহস্পতিবার', 'শুক্রবার');
        $convcurrentDate = str_replace($engDATE, $bangDATE, $currentDate);
//
        $track_no = str_replace($engDATE, $bangDATE, $process_data->track_no);

        $mpdf = new mPDF(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                'dejavusans', // default font family
                10, // margin_left
                10, // margin right
                10, // margin top
                10, // margin bottom
                9, // margin header
                9, // margin footer
                'P'
        );
        $mpdf->useSubstitutions;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('color', '#000');
        $mpdf->SetTitle("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetSubject("Bangladesh Economic Zones Authority (BEZA) $appID");
        $mpdf->SetAuthor("Business Automation Limited");

        // $bangla = CommonFunction::convertUTF8($banglas);

        $baseURL = "uploads/";
        $directoryName = $baseURL . date("Y/m");
        $directoryNameYear = $baseURL . date("Y");

        if (!file_exists($directoryName)) {
            $oldmask = umask(0);
            mkdir($directoryName, 0777, true);
            umask($oldmask);
            $f = fopen($directoryName . "/index.html", "w");
            fclose($f);
            if (!file_exists($directoryNameYear . "/index.html")) {
                $f = fopen($directoryNameYear . "/index.html", "w");
                fclose($f);
            }
        }
        $certificateName = uniqid("beza_" . $appID . "_", true);

        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;

        $mpdf->autoLangToFont = true;
        $mpdf->SetDisplayMode('fullwidth');

        $desk_name = CommonFunction::getFieldName(8, 'desk_id', 'desk_name', 'user_desk');
        $nationality = CommonFunction::getFieldName($form_data->NATIONALITY, 'country_code', 'nationality', 'country_info');

        if (!empty($desk_data->signature)) {
            $signature = '<img src="users/signature/' . $desk_data->signature . '" width="100" height="50">';
        } else {
            $signature = '';
        }

        $banglas = <<< HERE
<table border="0">
  <tr>
    <td width="222">&nbsp;</td>
    <td width="289" align="center"><pre>গণপ্রজাতন্ত্রী&#32;&#32;&#32;বাংলাদেশ&#32;&#32;সরকার</pre></td>
    <td width="213">&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td align="center"><pre>নিরাপত্তা&#32;শাখা&#32;-&#32;২</pre></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="left">&nbsp;<br/><br/><br/></td>
  </tr>
  <tr>
    <td colspan="2" align="left">নম্বর: $track_no </td>
    <td align="right">তারিখ: $convcurrentDate</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;<br/><br/><br/></td>
  </tr>
  <tr>
    <td colspan="3"><pre>বিষয়&#32;:&#32;&#32;&#32;বিদেশী&#32;&#32;&#32;নাগরিক&#32;/&#32;&#32;নাগরিকদের&#32;নিরাপত্তা&#32;ছারপত্র&#32;প্রদান।</pre></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;<br/><br/><br/></td>
  </tr>
  <tr>
    <td colspan="3">
    $nationality নাগরিক $form_data->EXP_NAME কে $form_data->ORG_NAME নামক কোম্পানি নিয়োগের নিমিত্তে বিনিয়োগ । বিদেশী নাগরিক/ নাগরিকদের অনুকুলে নিরাপত্তা ছারপত্র প্রদানের বিষয়ে স্বরাষ্ট্র মন্ত্রনালয়ের স্মারক  নম্বর  $track_no , তারিখঃ  $convertedpermit আনুশারে মতামত আগামী $convertedafter তারিখের মধ্যে প্রেরনের জন্য অনুরুধ করা হল ।
    <br/><br/><br/><br/><br/><br/>
</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td colspan="3" style="text-align: right;" valign="top">

<br/><br/>
$signature
  <br/>($desk_data->user_full_name ) <br/><br/> $desk_name <br/>  ফোন : $desk_data->user_phone
  <br/><br/><br/><br/>
   তারিখ: $convcurrentDate
   <br/><br/><br/>
   $signature
   <br/>
   ($desk_data->user_full_name ) <br/><br/> $desk_name
  </td>
  </tr>
</table>
HERE;
        $mpdf->SetCompression(true);
        $mpdf->WriteHTML($banglas);
        $pdfFilePath = $directoryName . "/" . $certificateName . '.pdf';
        $mpdf->Output($pdfFilePath, 'F'); // Saving pdf
        return $pdfFilePath;
    }

}
