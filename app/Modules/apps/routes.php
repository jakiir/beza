<?php

Route::group(array('module' => 'Apps', 'middleware' => 'auth', 'namespace' => 'App\Modules\Apps\Controllers'), function() {

    Route::get('apps/application-selection/{id}', 'AppsController@selectApplication');
    Route::post('apps/application-selection-check', 'AppsController@selectApplicationCheck');
    Route::get('apps/form/create', 'AppsController@appFormCreate');
    Route::post('apps/ajax/{param}', 'AppsController@ajaxRequest');
    Route::patch('apps/store/{id}', 'AppsController@store');
    Route::any('/apps/view/{id}', "AppsController@view");
    Route::patch('/apps/update/{id}', "AppsController@update");
    Route::patch('/apps/update-batch', "AppsController@updateBatch");
    Route::patch('/apps/assign-desks', "AppsController@assignDesks");
    Route::post('/apps/get-district', "AppsController@getDistrictAreaData");

    Route::get('/apps/re-certificate/{id}', "AppsController@ReGenerateCertificate");
    Route::get('/apps/certificate/{id}', "AppsController@certificate");
	
    Route::any('apps/upload-document', 'AppsController@uploadDocument');

    Route::get('apps/delegation', 'AppsController@delegation');
    Route::get('apps/pdf-gen', 'AppsController@pdf_gen');
    Route::get('apps/certificate-gen', 'AppsController@certificate_gen');
    Route::get('apps/print/{id}', 'AppsController@prints');
    Route::get('apps/{id}', 'AppsController@index');

    Route::get('apps', 'AppsController@listOfClearance');

    // for clearing certificate
    Route::get('apps/list-of-clearance/project', 'AppsController@listOfClearance');

    Route::resource('apps', 'AppsController');
    /**********************************End of Route group*******************************/
});
