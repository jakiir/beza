<?php

namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\DB;
use App\Modules\Users\Models\Users;

class Apps_back extends Model {

    protected $table = 'process_list';
    protected $fillable = array(
        'process_id',
        'track_no',
        'company_id',
        'process_type',
        'initiated_by',
        'closed_by',
        'desk_id',
        'status_id',
        'record_id',
        'process_desc',
        'updated_on',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->updated_by = 1;
        });

        static::updating(function($post) {
            $post->updated_by = 1;
        });
    }

    /**
     * @return application list
     */
    public static function getAppList2() {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        //dd($userType);
        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.initiated_by', '=', $userId)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 3) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 6) { //Special Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->join('apps_verify_result as avr', function($join) use($userId) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.user_id', '=', DB::raw("$userId"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 4) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 7) { //NSI Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->join('apps_verify_result as avr', function($join) use($userId) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.user_id', '=', DB::raw("$userId"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get([ 'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 2) { //Additional secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 6)
                            ->where('process_list.desk_id', '=', 2)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'wp.certificate', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 1) { //Administrative officer
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->where('process_list.status_id', '!=', 11)
                            ->where('process_list.desk_id', '=', 1)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 8) { //Joint secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->where('process_list.status_id', '!=', 11)
                            ->where('process_list.desk_id', '=', 8)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin / IT Help Desk
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

    public static function getDelegationAppList() {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $curr_desk_id = CommonFunction::getDeskId();
        $delegated_usersArr = Users::select(DB::raw('group_concat(desk_id) AS delegated_desk'))->where('delegate_to_user_id', $userId)->first();

        $delegated_desk = $delegated_usersArr->delegated_desk;

        if (!empty($delegated_desk))
            $desk_id = $delegated_desk;



        if ($desk_id == 3) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 4) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } else {
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
//                            ->where('process_list.status_id', '!=', 11)
                            ->whereIn('process_list.desk_id', explode(',', $delegated_desk))
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        }
    }

    public static function getSearchAppList($tracking_number, $passport_number, $nationality, $organization, $applicant_name) {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        //dd($userType);
        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.initiated_by', '=', $userId)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 3) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 6) { //Special Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->join('apps_verify_result as avr', function($join) use($userId) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.user_id', '=', DB::raw("$userId"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 4) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('apps_verify_result as avr', function($join) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', function($join) {
                                $join->on('u.id', '=', 'avr.user_id');
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                            })
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 7) { //NSI Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                                $join->on('wp.app_id', '=', 'process_list.record_id');
                                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                                $join->on('wp.status_id', '=', DB::raw("9"));
                            })
                            ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->join('apps_verify_result as avr', function($join) use($userId) {
                                $join->on('avr.app_id', '=', 'process_list.record_id');
                                $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                                $join->on('avr.status_id', '=', DB::raw("0"));
                                $join->on('avr.user_id', '=', DB::raw("$userId"));
                            })
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 9)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->orderBy('process_list.created_at', 'desc')
                            ->get([ 'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 2) { //Additional secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '=', 6)
                            ->where('process_list.desk_id', '=', 2)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 1) { //Administrative officer
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->where('process_list.status_id', '!=', 11)
                            ->where('process_list.desk_id', '=', 1)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 8) { //Joint secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->where('process_list.status_id', '!=', 11)
                            ->where('process_list.desk_id', '=', 8)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_id', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

    public function getChildList($id) {

        $this->db->select('AREA_ID, AREA_NM_BAN');

        $this->db->order_by("AREA_NM_BAN ASC");

        $this->db->where("PARE_ID", $id);

        return $this->db->get('area_info')->result_array();
    }

    public static function getSearchResults($tracking_number, $passport_number, $nationality, $organization, $applicant_name, $status_id, $sb_status_id,$nsi_status_id) {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        //dd($userType);
        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.initiated_by', '=', $userId)
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif (in_array($desk_id, array(1, 3, 4))) {

            if ($desk_id == 3 || $desk_id == 4) {
                $fields = [
                    'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status','u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status'
                ];
            }
                else{
                    $fields = [
                        'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status','u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at'
                    ];
                }

          $data =  Apps::leftJoin('work_permit_form as wp', 'process_list.record_id', '=', 'wp.app_id')
                ->leftJoin('companies as c', 'process_list.company_id', '=', 'c.company_id')
                ->leftJoin('apps_verify_result as avr', 'process_list.record_id', '=', 'avr.app_id')
                ->leftJoin('user_desk as ud', 'process_list.desk_id', '=', 'ud.desk_id')
                ->leftJoin('app_status as as1', 'process_list.status_id', '=', 'as1.status_id')
                ->leftJoin('area_info as ai', 'wp.ORG_DISTRICT', '=', 'ai.area_id')
                ->leftJoin('country_info as ci', 'wp.NATIONALITY', '=', 'ci.country_code')
                ->leftJoin('users as u','u.id', '=', 'avr.user_id')

                ->where(function($query) use ($desk_id, $nsi_status_id, $sb_status_id, $tracking_number, $passport_number, $organization, $applicant_name, $nationality, $status_id)
                {
                    if ($desk_id == 4 && $nsi_status_id!=0) {
                        $query->leftJoin('apps_verify_result as avr', 'avr.app_id', '=', 'process_list.record_id')
                            ->where('avr.sb_nsi_flag','=','nsi')
                        ->where('avr.status_id','=',$nsi_status_id);
                    }

                    if ($desk_id == 3 && $sb_status_id!=0) {
                        $query->leftJoin('apps_verify_result as avr', 'avr.app_id', '=', 'process_list.record_id')
                            ->where('avr.sb_nsi_flag','=','sb')
                            ->where('avr.status_id','=',$sb_status_id);
                    }

                       if ($tracking_number != '') {
                            $query->where('wp.track_no', '=', $tracking_number);
                        }

                        if ($passport_number != '') {
                            $query->where('wp.PASSPORT_NO', 'like','%'.$passport_number.'%' );
                        }
                        if ($organization != '') {
                            $query->where('wp.ORG_NAME', 'like', '%'.$applicant_name.'%');
                        }

                        if ($applicant_name != '') {
                            $query->where('c.company_name', 'like', '%'.$organization.'%');
                        }
                        if ($nationality != '') {
                            $query->where('ci.country_code', '=', $nationality);
                        }
                        if ($status_id != '') {
                            $query->where('process_list.status_id', '=', $status_id);
                        }
                })
                ->where('process_list.status_id','!=', DB::raw('0'))
                ->orderBy('process_list.created_at', 'desc')
                ->get($fields);
            return $data;
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                            ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                            ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                            ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                            ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                            ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                            ->where('process_list.status_id', '!=', 0)
                            ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                                return $query->where('wp.track_no', '=', $tracking_number)
                                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                                        ->orWhere('c.company_name', '=', $organization)
                                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                                        ->orWhere('ci.country_code', '=', $nationality);
                            })
                            ->groupBy('process_list.process_id')
                            ->orderBy('process_list.created_at', 'desc')
                            ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

}
