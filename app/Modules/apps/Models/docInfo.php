<?php

namespace App\Modules\apps\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Libraries\CommonFunction;

class docInfo extends Model {

    protected $table = 'doc_info';
    protected $fillable = [
        'doc_id',
        'service_id',
        'ctg_id',
        'doc_name',
        'doc_priority',
        'additional_field',
        'is_multiple',
        'order',
        'is_active',
        'is_archieved',
    ];

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->created_by = Auth::user()->id;
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *****************************End of Model Function********************************* */
}
