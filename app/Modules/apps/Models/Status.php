<?php

namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $table = 'app_status';
    protected $fillable = array(
        'status_name',
        'process_id',
        'service_id',
        'color'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->updated_by = 1;
        });

        static::updating(function($post) {
            $post->updated_by = 1;
        });
    }

    /*     * *****************************************End of Model Class**************************************************** */
}
