<?php

namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\DB;
use App\Modules\Users\Models\Users;

class Apps extends Model {

    protected $table = 'process_list';
    protected $fillable = array(
        'process_id',
        'track_no',
        'company_id',
        'record_id',
        'process_type',
        'initiated_by',
        'closed_by',
        'desk_id',
        'status_id',
        'service_id',
        'process_desc',
        'on_behalf_of_desk',
        'created_at',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->updated_by = 1;
        });

        static::updating(function($post) {
            $post->updated_by = 1;
        });
    }

    /**
     * @return application list
     */
    public static function getAppList($search_status_id = 0) {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        //dd($userType);
        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.initiated_by', '=', $userId)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 3) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
//                                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('apps_verify_result as avr', function($join) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', function($join) {
                    $join->on('u.id', '=', 'avr.user_id');
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                })
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'u.district', 'u.thana', 'u.method', 'u.section_id', 'u.team_id', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 6) { //Special Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
//                                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->join('apps_verify_result as avr', function($join) use($userId) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.user_id', '=', DB::raw("$userId"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.method', 'u.section_id', 'u.team_id', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 4) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
//                                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('apps_verify_result as avr', function($join) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', function($join) {
                    $join->on('u.id', '=', 'avr.user_id');
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                })
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'u.district', 'u.thana', 'u.method', 'u.section_id', 'u.team_id', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 7) { //NSI Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
//                                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->join('apps_verify_result as avr', function($join) use($userId) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.user_id', '=', DB::raw("$userId"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->orderBy('process_list.created_at', 'desc')
                ->get([ 'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.method', 'u.section_id', 'u.team_id', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 2) { //Additional secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 6)
                ->where('process_list.desk_id', '=', 2)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'wp.certificate', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 1) { //Administrative officer
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
                ->where('process_list.status_id', '!=', 11)
                ->where('process_list.desk_id', '=', 1)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 8) { //Joint secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
                ->where('process_list.status_id', '!=', 11)
                ->where('process_list.desk_id', '=', 8)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

    /**
     * @return clearing certificate application list
     */
    public static function getClearanceList($search_status_id = 0) {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')

                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')
                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->where('process_list.initiated_by', '=', $userId)
                ->orWhere(function ($query) use ($userId) {
                    $query->Where('process_list.status_id', '=', 21)
                        ->where('process_list.initiated_by', '=', $userId);
                })
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name',  'as1.status_id', 'process_list.created_at']);
//                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 3) { //RD1 gateway
            return $query = Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')

                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')

                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->where(function ($query) {
                    $query->where('process_list.status_id', '=', 3)
                        ->orWhere('process_list.status_id', '=', 17);
                })
                ->where('process_list.desk_id', '=', 3)
                ->groupBy('process_list.process_id')
                ->where('process_list.status_id', '!=', 0)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 6) { //RD4
            return $query = Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')
                ->where(function ($query) {
                    $query->where('process_list.status_id', '=', 21);
//                        ->orWhere('process_list.status_id', '=', 17);
                })
//                ->where('process_list.desk_id', '=', 3)
                ->groupBy('process_list.process_id')
                ->where('process_list.status_id', '!=', 0)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        }
        elseif ($desk_id == 5) { //RD3
            return $query = Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')

                ->where(function ($query) {
                    $query->where('process_list.status_id', '=', 20);
//                        ->orWhere('process_list.status_id', '=', 19)
//                        ->orWhere('process_list.status_id', '=', 16);
                })
                ->where('process_list.desk_id', '=', 5)
                ->groupBy('process_list.process_id')
                ->where('process_list.status_id', '!=', 0)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 4) { //RD2
            return $query = Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')

                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->where(function ($query) {
                    $query->where('process_list.status_id', '=', 18);
//                        ->orWhere('process_list.status_id', '=', 19)
//                        ->orWhere('process_list.status_id', '=', 16);
                })
                ->where('process_list.desk_id', '=', 4)
                ->groupBy('process_list.process_id')
                ->where('process_list.status_id', '!=', 0)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 2) { //DD2
            return $query = Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->where(function ($query) {
                    $query->where('process_list.status_id', '=', 2)
                        ->orWhere('process_list.status_id', '=', 19)
                        ->orWhere('process_list.status_id', '=', 16);
                })
                ->where('process_list.desk_id', '=', 2)
                ->where('process_list.status_id', '!=', 0)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 1) { //DD1

         return   $data = Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('project_clearance as wp', function($join) {
                    $join->on('wp.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(1));
                })
                ->leftJoin('visa_assistance as wp2', function($join) {
                    $join->on('wp2.id', '=', 'process_list.record_id');
                    $join->on('process_list.service_id', '=', DB::raw(2));
                })
//                ->leftJoin('project_clearance as wp', 'wp.id', '=', 'process_list.record_id')
                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
//                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->leftJoin('nationality as ci2', 'ci2.id', '=', 'wp2.correspondent_nationality')
                ->where('process_list.status_id', '!=', 0)
                ->where('process_list.status_id', '!=', 11)
                ->where('process_list.desk_id', '=', 1)
                ->groupBy('process_list.service_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','ci2.nationality as vanationality','process_list.service_id','si.name as siname','wp.applicant_name','wp2.applicant_name as vaname', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);

//                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.certificate', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        }  elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('project_clearance as wp', 'wp.id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('service_info as si', 'si.id', '=', 'process_list.service_id')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.nationality')
                ->where('process_list.status_id', '!=', 0)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality', 'si.name as siname','ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

    public static function getDelegationAppList() {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $curr_desk_id = CommonFunction::getDeskId();
        $delegated_usersArr = Users::select(DB::raw('group_concat(desk_id) AS delegated_desk'))->where('delegate_to_user_id', $userId)->first();



        $delegated_desk = $delegated_usersArr->delegated_desk;

        //dd($delegated_desk);
        $desk_ids = array();
        $desk_id = array('-1');
        if (!empty($delegated_desk)) {
            $desk_id = $delegated_desk;
            $desk_ids = explode(',', $desk_id);
            $desk_id_array = array_unique($desk_ids);
            if (count($desk_id_array) == 1)
                $desk_id = $desk_id_array[0];
        }



        if ($desk_id == 3 || $desk_id == 9) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('apps_verify_result as avr', function($join) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', function($join) {
                    $join->on('u.id', '=', 'avr.user_id');
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                })
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 4 || $desk_id == 10) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('apps_verify_result as avr', function($join) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', function($join) {
                    $join->on('u.id', '=', 'avr.user_id');
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                })
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } else {
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
//                            ->where('process_list.status_id', '!=', 11)
                ->whereIn('process_list.desk_id', explode(',', $delegated_desk))
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        }
    }

    public static function getSearchAppList($tracking_number, $passport_number, $nationality, $organization, $applicant_name) {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();
        //dd($userType);
        if ($userType == '5x505') { //Applicant
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.initiated_by', '=', $userId)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 3) { //Special Branch Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('apps_verify_result as avr', function($join) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', function($join) {
                    $join->on('u.id', '=', 'avr.user_id');
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                })
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 6) { //Special Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.sb_gk_verification_status', '=', DB::raw("0"));
                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->join('apps_verify_result as avr', function($join) use($userId) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'sb'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.user_id', '=', DB::raw("$userId"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 4) { //NSI Gatekeeper
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('apps_verify_result as avr', function($join) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', function($join) {
                    $join->on('u.id', '=', 'avr.user_id');
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                })
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at', 'avr.status_id as result_status']);
        } elseif ($desk_id == 7) { //NSI Branch Verifier
            return Apps::join('work_permit_form as wp', function($join) {
                $join->on('wp.app_id', '=', 'process_list.record_id');
                $join->on('wp.nsi_gk_verification_status', '=', DB::raw("0"));
                $join->on('wp.status_id', '=', DB::raw("9"));
            })
                ->leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->join('apps_verify_result as avr', function($join) use($userId) {
                    $join->on('avr.app_id', '=', 'process_list.record_id');
                    $join->on('avr.sb_nsi_flag', '=', DB::raw("'nsi'"));
                    $join->on('avr.status_id', '=', DB::raw("0"));
                    $join->on('avr.user_id', '=', DB::raw("$userId"));
                })
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('users as u', 'u.id', '=', 'avr.user_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 9)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->orderBy('process_list.created_at', 'desc')
                ->get([ 'wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'avr.status_id as result_status', 'u.district', 'u.thana', 'u.user_full_name', 'ud.desk_name', 'as1.color', 'process_list.record_id', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 2) { //Additional secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '=', 6)
                ->where('process_list.desk_id', '=', 2)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 1) { //Administrative officer
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
                ->where('process_list.status_id', '!=', 11)
                ->where('process_list.desk_id', '=', 1)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($desk_id == 8) { //Joint secretary
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
                ->where('process_list.status_id', '!=', 11)
                ->where('process_list.desk_id', '=', 8)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_id', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } elseif ($userType == '1x101' || $userType == '2x202') { // System Admin
            return Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('work_permit_form as wp', 'wp.app_id', '=', 'process_list.record_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('country_info as ci', 'ci.country_code', '=', 'wp.NATIONALITY')
                ->where('process_list.status_id', '!=', 0)
                ->Where(function($query) use ($tracking_number, $passport_number, $organization, $applicant_name, $nationality) {
                    return $query->where('wp.track_no', '=', $tracking_number)
                        ->orWhere('wp.PASSPORT_NO', '=', $passport_number)
                        ->orWhere('c.company_name', '=', $organization)
                        ->orWhere('wp.ORG_NAME', '=', $applicant_name)
                        ->orWhere('ci.country_code', '=', $nationality);
                })
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['wp.ORG_NAME', 'ci.nationality', 'ai.area_nm', 'wp.nsi_gk_verification_status', 'wp.sb_gk_verification_status', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);
        } else {
            return [];
        }
    }

    public function getChildList($id) {
        $this->db->select('AREA_ID, AREA_NM_BAN');
        $this->db->order_by("AREA_NM_BAN ASC");
        $this->db->where("PARE_ID", $id);

        return $this->db->get('area_info')->result_array();
    }

    public static function getSearchResults($tracking_number, $passport_number, $nationality, $applicant_name,$status_id) {

        $userType = CommonFunction::getUserType();
        $userId = CommonFunction::getUserId();
        $desk_id = CommonFunction::getDeskId();

        if ($userType == '5x505') { //Applicant

            return $query =  Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('project_clearance as wp', 'wp.id', '=', 'process_list.record_id')
//                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->where('process_list.initiated_by', '=', $userId)
                ->where(function ($query) use ($tracking_number) {
                    if(!empty($tracking_number)){
                        $query->where('process_list.track_no', '=', $tracking_number);
                    }
                })
                ->where(function ($query2) use ($passport_number) {
                    if(!empty($passport_number)){
                        $query2->where('wp.correspondent_passport', '=', $passport_number);
                    }
                })
                ->where(function ($query3) use ($nationality) {
                    if(!empty($nationality)){
                        $query3->where('wp.correspondent_nationality', '=', $nationality);
                    }
                })
                ->where(function ($query4) use ($applicant_name) {
                    if(!empty($applicant_name)){
                        $query4->where('wp.applicant_name', '=', $applicant_name);
                    }
                })->where(function ($query5) use ($status_id) {
                    if(!empty($status_id)){
                        $query5->where('process_list.status_id', '=', $status_id);
                    }
                })
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','wp.applicant_name','ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);

        }
        else{ // all admin
            return $query =  Apps::leftJoin('companies as c', 'c.company_id', '=', 'process_list.company_id')
                ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'process_list.desk_id')
                ->leftJoin('app_status as as1', 'as1.status_id', '=', 'process_list.status_id')
                ->leftJoin('project_clearance as wp', 'wp.id', '=', 'process_list.record_id')
//                ->leftJoin('area_info as ai', 'ai.area_id', '=', 'wp.ORG_DISTRICT')
                ->leftJoin('nationality as ci', 'ci.id', '=', 'wp.correspondent_nationality')
                ->where(function ($query) use ($tracking_number) {
                    if(!empty($tracking_number)){
                        $query->where('process_list.track_no', '=', $tracking_number);
                    }
                })
                ->where(function ($query2) use ($passport_number) {
                    if(!empty($passport_number)){
                        $query2->where('wp.correspondent_passport', '=', $passport_number);
                    }
                })
                ->where(function ($query3) use ($nationality) {
                    if(!empty($nationality)){
                        $query3->where('wp.correspondent_nationality', '=', $nationality);
                    }
                })
                ->where(function ($query4) use ($applicant_name) {
                    if(!empty($applicant_name)){
                        $query4->where('wp.applicant_name', '=', $applicant_name);
                    }
                })->where(function ($query5) use ($status_id) {
                    if(!empty($status_id)){
                        $query5->where('process_list.status_id', '=', $status_id);
                    }
                })
                ->where('process_list.status_id', '!=', 0)
                ->where('process_list.status_id', '!=', -1)
                ->where('process_list.status_id', '!=', 11)
                ->groupBy('process_list.process_id')
                ->orderBy('process_list.created_at', 'desc')
                ->get(['ci.nationality','wp.applicant_name', 'ud.desk_name', 'as1.color', 'process_list.process_id', 'process_list.record_id', 'process_list.track_no', 'c.company_name', 'as1.status_name', 'as1.status_id', 'process_list.created_at']);

        }
    }
    /*******************************************End of Model Class*****************************************************/
}
