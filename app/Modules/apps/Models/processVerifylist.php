<?php

namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Libraries\CommonFunction;
use DB;

class processVerifylist extends Model {

   protected $table = 'apps_verify_result';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'app_id',
        'status_id',
        'desk_id',
        'user_id',
        'sb_remarks',
        'sb_file',
        'nsi_remarks',
        'nsi_file',
        'sb_nsi_flag',
        'updated_by'
    ];

    function insert_method($data) {
        DB::table($this->table)
                ->insert($data);
    }

    function update_method($_id, $data) {
        DB::table($this->table)
                ->where('app_id', $_id)
                ->update($data);
    }

    function process_method($_id, $data) {
        DB::table($this->table)
                ->where('verify_id', $_id)
                ->update($data);
    }

    function process_methods($_id, $data, $flag) {
        DB::table($this->table)
                ->where('app_id', $_id)
                ->where('status_id', 0)
                ->where('sb_nsi_flag', $flag)
                ->update($data);
    }

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function($post)
        {
//            $post->created_by = Auth::user()->id;
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post)
        {
            $post->updated_by = CommonFunction::getUserId();
        });

    }


}
