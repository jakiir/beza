<?php

namespace App\Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\CommonFunction;
use Illuminate\Support\Facades\DB;
use App\Modules\Users\Models\Users;

class ClearingCertificate extends Model {

    protected $table = 'project_clearance';
    protected $fillable = array(
        'applicant_name',
        'tracking_number',
        'road_no',
        'house_no',
        'post_code',
        'phone',
        'fax',
        'email',
        'website',
        'proposed_name',
        'joint_company_address',
        'joint_com_country',
        'construction_start',
        'construction_end',
        'construction_duration',
        'acceptTerms',
        'nationality',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            $post->updated_by = 1;
        });

        static::updating(function($post) {
            $post->updated_by = 1;
        });
    }


}
