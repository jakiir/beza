<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body>
<!-- Main content -->
    <section class="invoice">
        <div class="row text-center">
          <div class="col-xs-3">
            <img src="back-end/dist/img/logo.png" alt="Logo" width="70"/>
          </div><!-- /.col -->
          <div class="col-xs-5">
            <span class="print_heading">
                    <img src="back-end/dist/img/header.png" alt="header" width="766"/>
            </span>
          </div><!-- /.col -->
            <div class="col-xs-4 pull-right">

            </div>
        </div>

        <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">

<img src="back-end/dist/img/body.png" alt="body" width="766"/>
<br/><br/>
বষিয়    বদিশেি   নাগরকি /  নাগরকিদরে নরিাপত্তা ছারপত্র প্রদান<br/>

{{--সুত্র  বিনিয়োগ বোর্ড এর ৩১।৭।২০১৩ তারিখের ২৪৮ নম্বর পত্র<br/>--}}

{{--উপযুক্ত বিষয়ে সুত্রক্ত পত্রের প্রেক্ষিতে নিম্নবর্ণিত বিদেশি নাগরিক / নাগরিকদের পার্শে  বর্ণিত সংস্থা / প্রতিষ্ঠানে চাকুরীতে নিয়োগ / নিরাপত্তার ব্যাপারে  স্বরাষ্ট্র মন্ত্রালয়এর অনাপত্তি ( No objection )<br/>      নির্দেশক্রমে  জ্ঞাপন করা হল<br/>  <br/>--}}


{{--নির্বাহী চেয়ারম্যান<br/>--}}
{{--বিনিয়োগ বোর্ড ঢাকা<br/><br/>--}}
{{--<b>অনুলিপি অবগতি ও প্রয়োজনীয় ব্যবস্থা গ্রহনের জন্য প্রেরন করা হল</b><br/>--}}
{{--মহাপরিচালক , এন এস  আই<br/>--}}
{{--মহাপরিচালক , বহিরাগমন ও  পাসপোর্ট অধিদপ্তর , ঢাকা<br/>--}}
{{--অতিরিক্ত মহাপলিশ পরিদর্শক , বিশেষ শাঁখা , ঢাকা <br/>--}}

            </div><!-- /.col -->
            <div class="col-xs-1"></div>

        </div><!-- /.row -->

        <div class="row invoice-info">
            <div class="col-sm-1  invoice-col"></div><!-- /.col -->
          <div class="col-sm-10">
              <div style="border-bottom:1px solid black;">&nbsp;</div>
              <div>
                 
<img src="back-end/dist/img/footer.png" alt="body" width="766"/>
                  </div>
              </div>
          </div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div><br />
        <div class="row invoice-info">
            <div class="col-sm-2  invoice-col"></div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div>


    </section>
</body>
</html>
