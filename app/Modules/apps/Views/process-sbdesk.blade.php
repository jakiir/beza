<div class="form-group">
    {!! Form::open(['url' => '/apps/store/'.Encryption::encodeId($form_data->id), 'method' => 'patch', 'class' => 'form process_from', 'role' => 'form', 'id' => 'process_from','enctype' =>'multipart/form-data', 'files'=>true]) !!}
    {!! Form::hidden('sb_status',1,['id'=>'sb_status']) !!}
    {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
    {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
    {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}

    <span class="form-group col-md-3"><br/>
        {!! Form::select('status_id', [2=>'No Objection',3=>'Objection', 4=>'Black Listed'], '', $attributes = array('class'=>'form-control required','placeholder' => 'Select Report Status', 'id'=>"status_id")) !!}
    </span>

    <div class="form-group col-md-3 {{$errors->has('remarks') ? 'has-error' : ''}}"><br/>
        {!! Form::file('sb_file[]', array('multiple'=>true)) !!}
         <span style="font-size: 9px; color: grey">[File Type: pdf]</span>
        <?php if (!empty($verify_data->attachment)) { ?>
            <a href="<?php echo url(); ?>/uploads/<?php echo $verify_data->attachment; ?>" target="_blank">
                <?php echo $verify_data->attachment; ?>
                <img src="<?php echo url(); ?>/front-end/images/icon_download.gif" border="0" alt="download" title="<?php echo $verify_data->attachment; ?>"/>
            </a>
        <?php } ?>
    </div>
    <div class="form-group col-md-5">
        <?php
        $remarks = "";
        if (!empty($verify_data->remarks))
            $remarks = $verify_data->remarks;
        ?>
        {!! Form::textarea('sb_remarks',$remarks,['class'=>'form-control required','id'=>'sb_remarks', 'placeholder'=>'Enter Remarks','maxlength' => 1000, 'rows' => 1, 'cols' => 50]) !!}
        <small>(Maximum length 1000)</small>
        {!! $errors->first('sb_remarks','<span class="help-block">:message</span>') !!}
    </div>
    <div class="box-footer">
        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'value'=> 'Submit', 'class' => 'btn btn-primary send')) !!}

    </div>
    {!! Form::close() !!}
    <br/>
</div>
@section('footer-script')

<script>
    $(document).ready(function() {

        var _token = $('input[name="_token"]').val();

        $("#process_from").validate({
            errorPlacement: function() {
                return false;
            }
        });
    });

</script>
@endsection