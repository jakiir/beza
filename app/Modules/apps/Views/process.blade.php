
{!! Form::open(['url' => '/apps/update/'.Encryption::encodeId($form_data->app_id), 'method' => 'patch', 'class' => 'form apps_from', 'role' => 'form']) !!}
{!! Form::hidden('status_from',$statusId,['id'=>'status_from']) !!}

    <span class="col-md-3 {{$errors->has('status_id') ? 'has-error' : ''}}">
        {!! Form::label('status_id','Apply Status ') !!}
        {!! Form::select('status_id', (['' => 'Select Below'] + $statusList), null, ['class' => 'form-control required status_id']) !!}
        {!! $errors->first('status_id','<span class="help-block">:message</span>') !!}
    </span>

    <span id="sendToDeskOfficer">
        <span class="col-md-3 {{$errors->has('desk_id') ? 'has-error' : ''}}">
            {!! Form::label('desk_id','Send to Desk') !!}
            {!! Form::select('desk_id', [''=>'Select Below'], '', ['class' => 'form-control required param_id', '']) !!}
            {!! $errors->first('desk_id','<span class="help-block">:message</span>') !!}
        </span>
    </span>



    <span class="col-md-4 {{$errors->has('remarks') ? 'has-error' : ''}}">
        {!! Form::textarea('remarks',null,['class'=>'form-control','id'=>'remarks', 'placeholder'=>'Enter Remarks','maxlength' => 254, 'rows' => 1, 'cols' => 50]) !!}
        <small>(Maximum length 254)</small>
        {!! $errors->first('remarks','<span class="help-block">:message</span>') !!}
    </span>
    <span class="">
        {!! Form::button('<i class="fa fa-save"></i> Process', array('type' => 'submit', 'value'=> 'Submit', 'class' => 'btn btn-primary send')) !!}
        <font color="#C91A1A">
        @if ($process_data->desk_id <> Auth::user()->desk_id)
        {!! Form::hidden('on_behalf_of',$process_data->desk_id) !!}
        <br />On behalf of:<br /><b>
            <?php echo CommonFunction::getFieldName($process_data->desk_id, 'desk_id', 'user_full_name', 'users') ?>
        </b>
        @endif
        </font>
    </span>

{!! Form::close() !!}

@section('footer-script')
<script>
    $(document).ready(
            function() {
                $('#desk_id').attr("disabled", true);
                $('#remarks').attr("disabled", true);
                $(".apps_from").validate({
                    errorPlacement: function() {
                        return false;
                    }
                });
            });

    var _token = $('input[name="_token"]').val();
    $(function() {

        $(".send").click(function() {
//                alert('Send Under process');
        });

        $(".apps_from").validate({
            errorPlacement: function() {
                return false;
            }
        });
    });


    $(".status_id").change(function(e) {
        var object = $(".status_id");
        var obj = $(object).parent().parent().parent();
        var id = $(object).val();

        var status_from = $('#status_from').val();
        $('#sendToDeskOfficer').css('display', 'block');
        if (id == 0) {
            obj.find('.param_id').html('<option value="">Select Below</option>');
        } /*else if (id == 9 || id == 2 || id == 8) {
         $('#sendToDeskOfficer').css('display', '');
         $('#remarks').addClass('required');
         $('#remarks').attr("disabled", false);
         }*/ else {
            $.post('/apps/ajax/process', {id: id, status_from: status_from, _token: _token}, function(response) {
                if (response.responseCode == 1) {

                    var option = '';
                    option += '<option value="">Select Below</option>';

//                            console.log(param);
                    var countDesk = 0;
                    $.each(response.data, function(id, value) {
                        countDesk++;
                        //var selected = param.indexOf(id)>-1?'selected':'';
                        option += '<option value="' + id + '">' + value + '</option>';
                    });
                    obj.find('.param_id').html(option);
                    $('#desk_id').attr("disabled", false);
                    $('#remarks').attr("disabled", false);
                    if (countDesk == 0)
                    {
                        $('#sendToDeskOfficer').css('display', 'none');
                        $('#remarks').addClass('required');
                        $('#remarks').attr("disabled", false);
                    }
                }
            });
        }
    });
    $("#status_id").on('change', function() {


        var status_id = $("#status_id").val();

        if (status_id == 12) {
            $.ajax({
                type: "POST",
                crossDomain: true,
                url: 'https://ors-boi.batworld.com/api/',
                data: {TRACK_NO: TRACK_NO},
                async: false,
                dataType: "json",
                success: function(result) {
                    if (result != '') {
                        $('.form-open').show();
                    } else {
                        $('.form-open').hide('');
                    }
                }
            });
        } else {
//            alert('Please try again');
            return false;
        }
    });



</script>
@endsection
