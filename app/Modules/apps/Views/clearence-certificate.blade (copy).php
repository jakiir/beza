@extends('print-layout')

@section('content')

<!-- Main content -->
    <section class="invoice">
        <div class="row text-center">            
          <div class="col-xs-3">
                {!! Html::image("back-end/dist/img/logo.png", "Logo",['width'=>70]) !!}     
          </div><!-- /.col -->  
          <div class="col-xs-5">
            <span class="print_heading">
                    গণপ্রজাতন্ত্রী   বাংলাদেশ  সরকার  <br/>        
 		স্বরাষ্ট্র মন্ত্রালয়   <br/>        
 		নিরাপত্তা শাখা - ২   <br/>  <br/>  <br/>           
            </span>
          </div><!-- /.col -->
            <div class="col-xs-4 pull-right">
               {!! link_to('apps/#'.Request::segment(3),'Print',['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) !!}
            </div>
        </div> 

        <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">


বিষয়    বিদেশি   নাগরিক /  নাগরিকদের নিরাপত্তা ছারপত্র প্রদান<br/>     

সুত্র  বিনিয়োগ বোর্ড এর ৩১।৭।২০১৩ তারিখের ২৪৮ নম্বর পত্র<br/>     

উপযুক্ত বিষয়ে সুত্রক্ত পত্রের প্রেক্ষিতে নিম্নবর্ণিত বিদেশি নাগরিক / নাগরিকদের পার্শে  বর্ণিত সংস্থা / প্রতিষ্ঠানে চাকুরীতে নিয়োগ / নিরাপত্তার ব্যাপারে  স্বরাষ্ট্র মন্ত্রালয়এর অনাপত্তি ( No objection )<br/>      নির্দেশক্রমে  জ্ঞাপন করা হল<br/>  <br/>     
                <table class="text-center table table-bordered table-striped  table-condensed table_report">
                    <thead class="text-center">
                    <tr>
                        <th>S.N</th>
                        <th>Name & Passport No.</th>
                        <th>Nationality</th>
			<th>Organization</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                        <tr>
				<td>1</td>
				<td>Mr. Das Sourov  (P.P.No. Z2263692)</td>
				<td>India</td>
				<td>Plus Trading Far East Ltd.</td>
                        </tr>
			<tr>
				<td>1</td>
				<td>Mr. Das Sourov  (P.P.No. Z2263692)</td>
				<td>India</td>
				<td>Plus Trading Far East Ltd.</td>
                        </tr>
                    </tbody>
                </table>
<br/><br/>
নির্বাহী চেয়ারম্যান<br/>
বিনিয়োগ বোর্ড ঢাকা<br/><br/>
<b>অনুলিপি অবগতি ও প্রয়োজনীয় ব্যবস্থা গ্রহনের জন্য প্রেরন করা হল</b><br/>
মহাপরিচালক , এন এস  আই<br/>
মহাপরিচালক , বহিরাগমন ও  পাসপোর্ট অধিদপ্তর , ঢাকা<br/>
অতিরিক্ত মহাপলিশ পরিদর্শক , বিশেষ শাঁখা , ঢাকা <br/>

            </div><!-- /.col -->
            <div class="col-xs-1"></div>

        </div><!-- /.row -->

        <div class="row invoice-info">
            <div class="col-sm-1  invoice-col"></div><!-- /.col -->
          <div class="col-sm-10">
              <div style="border-bottom:1px solid black;">&nbsp;</div>
              <div>
                  <div class="text-left"><strong>ঠিকানা</strong></div>
                  <div class="text-left">
                  স্বরাষ্ট্র মন্ত্রণালয় বাংলাদেশ সচিবালয়,<br/>
                  বাংলাদেশ সচিবালয়, ঢাকা- ১০০০। <br/>
                  ফ্যাক্সঃ সচিব এর কার্যালয় - ৯৫৭৩৭১১,ইমেইলঃ secretary@mha.gov.bd, http://mha.gov.bd                      
                  </div>
              </div>
          </div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div><br />
        <div class="row invoice-info">
            <div class="col-sm-2  invoice-col"></div><!-- /.col -->
          <div class="col-sm-1  invoice-col"></div><!-- /.col -->
        </div>        


    </section>

@endsection

