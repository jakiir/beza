@extends('print-layout')

@section('content')

                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                              <td colspan="2" align="center"><img src="{{ url('assets/images/logo_beza_single.png')  }}" class="img-responsive" alt="img" width="70" />
                                <h4> <b> গণপ্রজাতন্ত্রী   বাংলাদেশ  সরকার <br/>
                                  স্বরাষ্ট্র মন্ত্রালয় <br/>
                              নিরাপত্তা শাখা - ২ </b></h4></td>
                          </tr>
                            <tr>
                                <td>{!! Form::label('VISA_PERMIT','Received Recommendation Letter from BOI through :') !!}
                                    {{ $form_data->VISA_PERMIT }}
                                </td>
                                <td rowspan="4">@if(!empty($form_data->AUTH_IMAGE))
                                @if($form_data->external_source != 0) <img src="https://ors-boi.batworld.com/ajaximage/photos/{{ $form_data->AUTH_IMAGE  }}" class="" alt="img" id="user_pic" width="75" /> @else <img src="{{ url('uploads/'.$form_data->AUTH_IMAGE)  }}" class="img-responsive img-thumbnail" alt="img" id="user_pic" width="75" /> @endif
                              @endif </td>
                            </tr>
                            <tr>
                                <td>{!! Form::label('VISA_REF_NO','Ref No. of Visa Recommendation Letter :') !!} {!! $process_data->reference_no !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::label('DEPT_ID','Department in Boi Desk :') !!} {!! $form_data->DEPT_ID !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::label('VISA_CATEGORY','Work Permit type :') !!}
                                    @if($form_data->VISA_CATEGORY == 18)
                                    New Commercial
                                    @elseif($form_data->VISA_CATEGORY == 14)
                                    New Industrial
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                          <td  colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                          <td  colspan="4"><strong>A. PARTICULAR OF SPONSOR/EMPLOYER</strong></td>
                        </tr>
                        <tr>
                            <td  colspan="4">{!! Form::label('ORG_NAME','Name of the enterprise  (Organization / Company):') !!} {!! $form_data->ORG_NAME !!}</td>
                        </tr>
                        <tr>
                            <td colspan="4" bgcolor="#dcdcdc">{!! Form::label('Address','Address of the enterprise (Bangladesh only)') !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('ORG_HOUSE_NO','House/ Plot/ Holding /Village : ') !!} {!! $form_data->ORG_HOUSE_NO !!}</td>
                            <td>{!! Form::label('ORG_FLAT_NO','Flat/ Apartment/ Floor : ') !!} {!! $form_data->ORG_FLAT_NO !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('ORG_ROAD_NO','Road Number : ') !!} {!! $form_data->ORG_ROAD_NO !!}</td>
                            <td>{!! Form::label('ORG_POST_CODE','Post / Zip Code : ') !!} {!! $form_data->ORG_POST_CODE !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('ORG_POST_OFFICE','Post Office : ') !!} {!! $form_data->ORG_POST_OFFICE !!}</td>
                            <td>{!! Form::label('ORG_PHONE','Telephone Number : ') !!} {!! $form_data->ORG_PHONE !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('ORG_DISTRICT','City/ District : ') !!} {!! CommonFunction::getFieldName($form_data->ORG_DISTRICT,'area_id','area_nm','area_info') !!}</td>
                            <td>{!! Form::label('ORG_FAX_NO','Fax Number : ') !!} {!! $form_data->ORG_FAX_NO !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('ORG_THANA','Thana/ Upazilla : ') !!} {!! CommonFunction::getFieldName($form_data->ORG_THANA,'area_id','area_nm','area_info') !!}</td>
                            <td>{!! Form::label('ORG_EMAIL','Email : ') !!} {!! $form_data->ORG_EMAIL !!}</td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('ORG_TYPE','Type of the Organization : ') !!} {!! $form_data->ORG_TYPE !!}</td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('ORG_ACTIVITY','Nature of business : ') !!} {!! $form_data->ORG_ACTIVITY !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('AUTH_CAPITAL','Authorized capital : ') !!} {!! 'BDT '.$form_data->AUTH_CAPITAL !!}</td>
                            <td>{!! Form::label('PAID_CAPITAL','Paid up capital : ') !!} {!! 'BDT '.$form_data->PAID_CAPITAL !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('REMITTANCE','Remittance received during last 12 months : ') !!} {!! 'BDT '.$form_data->REMITTANCE !!}</td>
                            <td>
                                @if($form_data->VISA_CATEGORY == 14)
                                    {!! Form::label('INDUSTRY_TYPE','Type of Industry : ') !!}
                                    <?php $industry = array('4'=>'Industry','5'=>'Others'); echo $industry[$form_data->INDUSTRY_TYPE]; ?>
                                @endif
                           </td>
                  </tr>
                        @if($form_data->VISA_CATEGORY == 14)
                        <tr>
                            <td colspan="2">{!! Form::label('RECOMMENDATION','Recommendation of Company Boards: ') !!}{!! $form_data->RECOMMENDATION !!}</td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('COMPANY_TYPE','Whether local,foreign or joint venture company ( if joint venture, percentage of local and foreign investment is to be shown):  ') !!}{!! $form_data->COMPANY_TYPE !!}</td>
                        </tr>
                         @endif
                    </tbody>
</table>

            </div>
        </div>
    <div class="panel">
            <div class="panel-body">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                          <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="2"><strong>B. PARTICULARS OF FOREIGN INCUMBENT</strong></td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('EXP_NAME','Name of the foreign national :') !!} {!! $form_data->EXP_NAME !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('NATIONALITY','Nationality :') !!} {!! CommonFunction::getFieldName($form_data->NATIONALITY,'country_code','nationality','country_info') !!}</td>
                            <td>{!! Form::label('PASSPORT_NO','Passport Number :') !!} {!! $form_data->PASSPORT_NO !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('BANK_ISSUE_DATE','Date of Issue :') !!} {!! CommonFunction::formateDate($form_data->BANK_ISSUE_DATE) !!}</td>
                            <td>{!! Form::label('ISSUE_PLACE','Place of Issue :') !!} {!! $form_data->ISSUE_PLACE !!}</td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('EXPR_DATE','Expiry Date :') !!} {!! CommonFunction::formateDate($form_data->EXPR_DATE) !!}</td>
                        </tr>

                        <tr>
                            <td colspan="2" bgcolor="#d3d3d3">{!! Form::label('Address','Permanent address') !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('EXP_HOME_COUNTRY','Country:') !!} {!! CommonFunction::getFieldName($form_data->EXP_HOME_COUNTRY,'country_code','country_name','country_info') !!}</td>
                            <td>{!! Form::label('EXP_HOUSE_NO','House/ Plot/ Holding Number :') !!} {!! $form_data->EXP_HOUSE_NO !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('EXP_FLAT_NO','Flat/ Apartment/ Floor Number:') !!} {!! $form_data->EXP_FLAT_NO !!}</td>
                            <td>{!! Form::label('EXP_ROAD_NO','Street Name/ Street Number :') !!} {!! $form_data->EXP_ROAD_NO !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('EXP_POST_CODE','Post / Zip Code :') !!} {!! $form_data->EXP_POST_CODE !!}</td>
                            <td>{!! Form::label('EXP_POST_OFFICE','State / Province :') !!} {!! $form_data->EXP_POST_OFFICE !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('EXP_PHONE','Telephone Number :') !!} {!! $form_data->EXP_PHONE !!}</td>
                            <td>{!! Form::label('EXP_DISTRICT','City :') !!} {!! CommonFunction::getFieldName($form_data->EXP_DISTRICT,'area_id','area_nm','area_info') !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('EXP_FAX_NO','Fax Number :') !!} {!! $form_data->EXP_FAX_NO !!}</td>
                            <td>{!! Form::label('EXP_EMAIL','Email:') !!} {!! $form_data->EXP_EMAIL !!}</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('DOB','Date of birth :') !!} {!! CommonFunction::formateDate($form_data->DOB) !!}</td>
                            <td>{!! Form::label('MARITAL_STATUS','Marital status :') !!} {!! $form_data->MARITAL_STATUS !!}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
</div>
    <div class="panel">
            <div class="panel-body">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                          <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="2"><strong>C. EMPLOYMENT INFORMATION</strong></td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('POST_EMP_NAME','Name of the post employed for (Designation) : ') !!} {!! $form_data->POST_EMP_NAME !!}</td>
                            <td>{!! Form::label('ARRIVAL_DATE','Date of arrival in Bangladesh : ') !!} {!! CommonFunction::formateDate($form_data->ARRIVAL_DATE) !!}</td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('TRAVEL_VISA_CATE','Type of visa : ') !!} {!! $form_data->TRAVEL_VISA_CATE !!} (  {!! $form_data->VISA_TYPE !!} Type )</td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('EFCT_START_DATE','Desired Effective Date : ') !!} {!! CommonFunction::formateDate($form_data->EFCT_START_DATE) !!}</td>
                            <td>{!! Form::label('EFCT_END_DATE','End Date : ') !!} {!! CommonFunction::formateDate($form_data->EFCT_END_DATE) !!} </td>
                        </tr>
                        <tr>
                            <td>{!! Form::label('VISA_VALIDITY','Desired Duration : ') !!} {!! $form_data->VISA_VALIDITY !!}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">{!! Form::label('POST_EMP_JOB_DESC','Brief job description :') !!} {!! $form_data->POST_EMP_JOB_DESC !!}</td>
                        </tr>
                         <tr>
                            <td>{!! Form::label('PERIOD_VALIDITY','Approved permission period :') !!} {!! $form_data->PERIOD_VALIDITY !!}</td>
                            <td>{!! Form::label('PERMIT_EFCT_DATE','Permitted Effective Date :') !!} {!!   CommonFunction::formateDate($form_data->PERMIT_EFCT_DATE) !!}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
</div>
    <div class="panel">
            <div class="panel-body">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                          <th colspan="6">&nbsp;</th>
                        </tr>
                        <tr>
                          <th colspan="6" align="left"><strong>D. Compensation and Benefits</strong></th>
                        </tr>
                        <tr>
                            <th>Salary structure</th>
                            <th colspan="3">Payable Locally</th>
                            {{--<th colspan="2">Payable Abroad</th>--}}
                        </tr>
                         <tr align="left">
                            <td>&nbsp;</td>
                            <td><b>Payment</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Currency</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>a. Basic salary</td>
                            <td>{!! $form_data->PLOC1 !!}</td>
                            <td>{!! $form_data->BASIC_SALARY !!}</td>
                            <td>{!! $form_data->CURRENCY1 !!}</td>
                            {{--<td>{!! $form_data->ABM1 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC1 !!}</td>--}}
                        </tr>
                        <tr>
                            <td>b. Overseas allowance</td>
                            <td>{!! $form_data->PLOC2 !!}</td>
                            <td>{!! $form_data->OVERSEAS_ALLOWANCE !!}</td>
                            <td>{!! $form_data->CURRENCY2 !!}</td>
                            {{--<td>{!! $form_data->ABM2 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC2 !!}</td>--}}
                        </tr>
                        <tr>
                            <td>c. House rent/Accommodation</td>
                            <td>{!! $form_data->PLOC3 !!}</td>
                            <td>{!! $form_data->HOUSE_RENT !!}</td>
                            <td>{!! $form_data->CURRENCY3 !!}</td>
                            {{--<td>{!! $form_data->ABM3 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC3 !!}</td>--}}
                        </tr>
                        <tr>
                            <td>d. Conveyance</td>
                            <td>{!! $form_data->PLOC4 !!}</td>
                            <td>{!! $form_data->CONVEYANCE_ALLOWANCE !!}</td>
                            <td>{!! $form_data->CURRENCY4 !!}</td>
                            {{--<td>{!! $form_data->ABM4 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC4 !!}</td>--}}
                        </tr>
                        <tr>
                            <td>e. Entertainment allowance</td>
                            <td>{!! $form_data->PLOC5 !!}</td>
                            <td>{!! $form_data->ENTERTAINMENT_ALLOWANCE !!}</td>
                            <td>{!! $form_data->CURRENCY5 !!}</td>
                            {{--<td>{!! $form_data->ABM5 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC5 !!}</td>--}}
                        </tr>
                        <tr>
                            <td>f. Medical allowance</td>
                            <td>{!! $form_data->PLOC6 !!}</td>
                            <td>{!! $form_data->MEDICAL_ALLOWANCE !!}</td>
                            <td>{!! $form_data->CURRENCY6 !!}</td>
                            {{--<td>{!! $form_data->ABM6 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC6 !!}</td>--}}
                        </tr>
                        <tr>
                            <td>g. Annual bonus</td>
                            <td>{!! $form_data->PLOC7 !!}</td>
                            <td>{!! $form_data->ANNUAL_BONUS !!}</td>
                            <td>{!! $form_data->CURRENCY7 !!}</td>
                            {{--<td>{!! $form_data->ABM7 !!}</td>--}}
                            {{--<td>{!! $form_data->ABC7 !!}</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td>h. Other fringe benefit, if any</td>
                            <td colspan="5">{!! $form_data->OTHER_BENEFIT !!}</td>
                        </tr>
                        <tr>
                            <td>i. Any Particular Comments of remarks</td>
                            <td colspan="5">{!! $form_data->SALARY_REMARKS !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
</div>
    <div class="panel">
            <div class="panel-body">
                <table class="table table-striped table-condensed">
                    <tr align="center">
                      <td colspan="9" ><div align="center"></div></td>
                    </tr>
                    <tr align="center">
                      <td colspan="9" align="left" >Manpower of the office</td>
                    </tr>
                    <tr align="center">
                        <td colspan="3" ><div align="center"><strong>Local</strong> (a)</div></td>
                        <td colspan="3" bgcolor="#EAF2F1"><div align="center"><strong>Foreign</strong> (b)</div></td>
                        <td rowspan="2" ><strong>Grand Total</strong><br />
                        (a+b)</td>
                        <td colspan="2" bgcolor="#EAF2F1"><div align="center"><strong>Ratio</strong></div></td>
                    </tr>
                    <tr>
                        <td align="center" ><div align="center">Executive</div></td>
                        <td align="center" ><div align="center">Supporting Staff</div></td>
                        <td align="center" ><div align="center">Total</div></td>
                        <td align="center" bgcolor="#EAF2F1"><div align="center">Executive</div></td>
                        <td align="center" bgcolor="#EAF2F1"><div align="center">Supporting Staff</div></td>
                        <td align="center" bgcolor="#EAF2F1"><div align="center">Total</div></td>
                        <td colspan="1" align="center" bgcolor="#EAF2F1"><div align="center">Local</div></td>
                        <td align="center" bgcolor="#EAF2F1"><div align="center">Foreign</div></td>
                    </tr>
                    <tr>
                        <td >
                            <div align="center">{!! $form_data->MP_LOC_EXECUTIVE !!} </div>
                        </td>
                        <td align="center" >
                            <div align="center"> {!! $form_data->MP_LOC_STAFF !!}</div>
                        </td>
                        <td ><div align="center"> {!! $form_data->MP_LOC_TOTAL !!}</div>
                        </td>
                        <td align="center" bgcolor="#EAF2F1">
                            <div align="center">{!! $form_data->FOR_LOC_EXECUTIVE !!}</div>
                        </td>
                        <td align="center" bgcolor="#EAF2F1">
                            <div align="center">{!! $form_data->FOR_LOC_STAFF !!} </div>
                        </td>
                        <td align="center" bgcolor="#EAF2F1">
                            <div align="center">{!! $form_data->FOR_LOC_TOTAL !!}</div>
                        </td>
                        <td align="center" >
                            <div align="center">{!! $form_data->GRAND_TOTAL !!} </div>
                        </td>
                        <td align="center" bgcolor="#EAF2F1">
                            <div align="center">{!! $form_data->RAT_LOC !!} </div>
                        </td>
                        <td align="center" bgcolor="#EAF2F1">
                            <div align="center">{!! $form_data->RAT_FOR !!} </div>
                        </td>
                    </tr>
                </table>
            </div>
</div>
    <div class="panel">
                <div class="panel-body">
    			<p>
    			a. I do here by declare that the information given above is true to the best of my knowledge and I shall be liable for any false information/statement is given
    			<br/>b. I do hereby undertake full responsibility of the expatriate(s) for whom visa recommendation is sought during their stay in Bangladesh.
    			<br/>
    			Note : All documents other than original must be attested by the Managing Director/Managing partner/ Proprietor of the company/ firm
    			</p>
                    <address>
                        <strong>{!! $form_data->ORG_NAME !!} </strong>
                        <br><span>{!! $form_data->ORG_HOUSE_NO !!} </span>,<span>{!! $form_data->ORG_FLAT_NO !!} </span>
                        <br><span>{!! $form_data->ORG_ROAD_NO !!} </span>,<span>{!! $form_data->ORG_THANA !!} </span>,<span>{!! $form_data->ORG_POST_OFFICE !!} </span>,<span>{!! $form_data->ORG_DISTRICT !!} </span>
                        <br>
                        <abbr title="Phone">P:</abbr><span>{!! $form_data->ORG_PHONE !!}</span><br>
                        <abbr title="Date">Submission Date:</abbr> <span>{!! $form_data->updated_at !!}</span>
                    </address>
                    <address>
                        <strong><span>{!! $form_data->AUTH_FULL_NAME !!}</span></strong>
                        <br>
                        <a href="mailto:#">{!! $form_data->AUTH_EMAIL !!}</a>
    				</address>
    				<p>
    					<br>
    					On behalf of <br>
    					Managing Director/Managing partner/ Proprietor of the company/ firm
                    </p>
                </div>
</div>

</section>
@endsection

@section('footer-script')
<script>

    function makeView() {
        $('#app_entry_form').find('input:not([type=checkbox],[type=from], [type=hidden])').each(function () {
            $(this).replaceWith("<span><b>" + this.value + "</b></span>");
        });
    }
</script>
@endsection