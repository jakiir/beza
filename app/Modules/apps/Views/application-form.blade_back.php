@extends('layouts.admin')

@section('content')
{{--@section ('page_heading','<i class="fa fa-list"></i> Application Form')

<style>
    /*.text-title{ font-size: 16px !important}
    .text-sm{ font-size: 9px !important}
    #appClearenceForm label.error {display: none !important; }*/
</style>--}}
<section class="content">
    <div class="box">
        <div class="box-body">

            <div class="panel panel-info"  id="inputForm">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong><i class="icon-check"></i> {{session('success')}}.</strong>
                    </div>
                @endif
                <div class="panel-heading">Application for Project Clearance</div>
                <div class="panel-body">
                    {!! Form::open(array('url' => 'apps/store-app','method' => 'post','id' => 'appClearenceForm','role'=>'form')) !!}
					<input type="hidden" name="selected_file" id="selected_file" />
                    <input type="hidden" name="validateFieldName" id="validateFieldName" />
                    <input type="hidden" name="isRequired" id="isRequired" />
                    <h3>Applicant Information</h3>
                    <fieldset>
                        <legend>Applicant Information</legend>

                        <div class="form-group clearfix">
                            <div class="col-lg-6 {{$errors->has('applicant_name') ? 'has-error': ''}}">
                                {!! Form::label('applicant_name','Name of an Applicant/Applying Firm or Company ',['class'=>'text-left required-star']) !!}
                                {!! Form::text('applicant_name','', ['data-rule-maxlength'=>'64',
                                'class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('applicant_name','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6"></div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <br>
                            <div class="col-lg-12">Full Address of Registered Head Office of Applicant/Applying Firm or Company:<br><hr></div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-6 {{$errors->has('country') ? 'has-error': ''}}">
                                {!! Form::label('country','Country ',['class'=>'text-left required-star']) !!}
                                {!! Form::select('country', $countries, '001', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('country','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6 {{$errors->has('division') ? 'has-error': ''}}">
                                {!! Form::label('division','Divition/State.',['class'=>'text-left required-star']) !!}
                                {!! Form::select('division', $divition_eng, '1', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('division','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-6 {{$errors->has('district') ? 'has-error': ''}}">
                                {!! Form::label('district','District ',['class'=>'text-left required-star']) !!}
                                {!! Form::select('district', $district_eng, '2', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('district','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('road_no','Road / Street Name or Number ',['class'=>'text-left required-star']) !!}
                                {!! Form::text('road_no','', ['data-rule-maxlength'=>'80',
                                'class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-6 {{$errors->has('house_no') ? 'has-error': ''}}">
                                {!! Form::label('house_no','House / Flat / Holding Number', ['class'=>'text-left required-star']) !!}
                                {!! Form::text('house_no','', ['data-rule-maxlength'=>'80',
                                'class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('post_code','Post Code ',['class'=>'text-left']) !!}
                                {!! Form::text('post_code','', ['data-rule-maxlength'=>'20',
                                'class' => 'form-control input-sm number']) !!}
                                {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-6 {{$errors->has('phone') ? 'has-error': ''}}">
                                {!! Form::label('phone','Phone No',['class'=>'text-left required-star']) !!}
                                {!! Form::text('phone','', ['data-rule-maxlength'=>'20', 'class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('phone','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('fax','Fax No ',['class'=>'text-left']) !!}
                                {!! Form::text('fax','', ['data-rule-maxlength'=>'20',
                                'class' => 'form-control input-sm number']) !!}
                                {!! $errors->first('fax','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-6 {{$errors->has('email') ? 'has-error': ''}}">
                                {!! Form::label('email','Email',['class'=>'text-left required-star']) !!}
                                {!! Form::text('email','', ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('website','Website ',['class'=>'text-left']) !!}
                                {!! Form::text('website','', ['data-rule-maxlength'=>'100',
                                'class' => 'form-control input-sm url']) !!}
                                {!! $errors->first('website','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <p>Fields marked with stars  (<span style="color: red">*</span>) are mandatory</p>
                    </fieldset>

                    <h3>Proposed Project Information (Part A)</h3>
                    <fieldset>
                        <legend>Proposed Project Information (Part A)</legend>

                        <div class="form-group clearfix">
                            <div class="col-lg-6 {{$errors->has('zone_type') ? 'has-error': ''}}">
                                {!! Form::label('zone_type','Preferred Economic Zone where business to be set' ,
                                ['class'=>'text-left required-star']) !!}
                                {!! Form::select('zone_type', $economicZone, '', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('zone_type','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6 {{$errors->has('preferred_zone') ? 'has-error': ''}}">
                                {!! Form::label('preferred_zone','',['class'=>'text-left required-star']) !!}
                                {!! Form::select('preferred_zone', $economicZone2, '', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('preferred_zone','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <div class="col-lg-6 {{$errors->has('proposed_name') ? 'has-error': ''}}">
                                {!! Form::label('proposed_name','Proposed Project / Company Name which will carry out of the Business',
                                ['class'=>'text-left required-star font-size-12']) !!}
                                {!! Form::text('proposed_name','', ['data-rule-maxlength'=>'100',
                                'class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('proposed_name','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6 {{$errors->has('business_type') ? 'has-error': ''}}">
                                {!! Form::label('business_type','Type of Business / Industry or Services ',['class'=>'text-left required-star']) !!}
                                {!! Form::select('business_type', $businessIndustryServices, '', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('business_type','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class="form-group" style="clear:both">
                            <div class="col-lg-6 {{$errors->has('organization_type') ? 'has-error': ''}}">
                                {!! Form::label('organization_type','Type of Organizations ',['class'=>'text-left required-star']) !!}
                                {!! Form::select('organization_type', $typeofOrganizations, '', ['class' => 'form-control input-sm required','onchange'=>'in_joint_com(this.value)']) !!}
                                {!! $errors->first('organization_type','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('IfJointVenture','&nbsp;',['class'=>'text-left']) !!}
                                <div class="">If Joint Venture !</div>
                            </div>
                        </div>

                        <div class="form-group" style="clear:both">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="type_of_organizations" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                        <thead class="alert alert-success">
                                            <tr>
                                                <th>Company Name <span class="required-star"></span></th>
                                                <th>Company Address <span class="required-star"></span></th>
                                                <th>Country <span class="required-star"></span></th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {!! Form::text('joint_company[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('joint_company','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('joint_company_address[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('joint_company_address[]','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::select('joint_com_country[]', $countries,'', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('joint_com_country[]','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td><span class="add_new_span">-</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-12">Proposed Factory Construction Schedule:<hr></div>
                        </div>

                        <div class="form-group" style="clear:both">
                            <div class="col-lg-4 {{$errors->has('construction_start') ? 'has-error': ''}}">
                                <div class="construction_start input-group date col-md-12">
                                    {!! Form::label('construction_start','Start Time ',['class'=>'text-left required-star']) !!}
                                    {!! Form::text('construction_start',  '', ['class' => 'form-control  input-sm required']) !!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    {!! $errors->first('construction_start','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-4 {{$errors->has('construction_end') ? 'has-error': ''}}">
                                <div class="construction_end input-group date col-md-12">
                                    {!! Form::label('construction_end','End Time ',['class'=>'text-left required-star']) !!}
                                    {!! Form::text('construction_end',  '', ['class' => 'form-control  input-sm required']) !!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    {!! $errors->first('construction_end','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-4 {{$errors->has('construction_duration') ? 'has-error': ''}}">
                                {!! Form::label('construction_duration','Duration ',['class'=>'text-left required-star']) !!}
                                {!! Form::text('construction_duration','', ['data-rule-maxlength'=>'100',
                                'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                {!! $errors->first('construction_duration','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <br>
                            <div class="col-lg-12">Initial Investment Amount to carry out the Business:<hr></div>
                        </div>

                        <div class="form-group" style="clear:both">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                        <thead class="alert alert-info">
                                            <tr>
                                                <th>Capital Structure <span class="required-star"></span></th>
                                                <th>Foreign <span class="required-star"></span></th>
                                                <th>Local Citizen <span class="required-star"></span></th>
                                                <th>Total <span class="required-star"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>Authorized Capital</th>
                                                <td>
                                                    {!! Form::text('auth_capital_fo','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'authCapitalFo()']) !!}
                                                    {!! $errors->first('auth_capital_fo','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('auth_capital_lo','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number','onkeypress'=>'validate_number(event)','onblur'=>'authCapitalLo()']) !!}
                                                    {!! $errors->first('auth_capital_lo','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('auth_capital_to','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number','readonly'=>true, 'onkeypress'=>'validate_number(event)']) !!}
                                                    {!! $errors->first('auth_capital_to','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Paid-up Capital (%)</th>
                                                <td>
                                                    {!! Form::text('paid_capital_fo','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'paidCapitalFo()']) !!}
                                                    {!! $errors->first('paid_capital_fo','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('paid_capital_lo','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'paidCapitalLo()']) !!}
                                                    {!! $errors->first('paid_capital_lo','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('paid_capital_to','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number','readonly'=>true, 'onkeypress'=>'validate_number(event)']) !!}
                                                    {!! $errors->first('paid_capital_to','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>External Borrowing</th>
                                                <td>
                                                    {!! Form::text('ext_borrow_fo','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'extBorrowFo()']) !!}
                                                    {!! $errors->first('ext_borrow_fo','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('ext_borrow_lo','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'extBorrowLo()']) !!}
                                                    {!! $errors->first('ext_borrow_lo','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('ext_borrow_to','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required number','readonly'=>true, 'onkeypress'=>'validate_number(event)']) !!}
                                                    {!! $errors->first('ext_borrow_to','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-12">Contribution in Paid-up Capital Among Major Shareholders:<hr></div>
                        </div>

                        <div class="form-group" style="clear:both">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                        <thead class="alert alert-warning">
                                            <tr>
                                                <th>Name of Shareholder <span class="required-star"></span></th>
                                                <th>Contribution Amount  <span class="required-star"></span></th>
                                                <th>Foreign/Citizen  <span class="required-star"></span></th>
                                                <th>Share (%)  <span class="required-star"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>Authorized Capital</th>
                                                <td>
                                                    {!! Form::text('auth_cap_amount','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('auth_cap_amount','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('auth_cap_nature','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('auth_cap_nature','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('auth_cap_percentage','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('auth_cap_percentage','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Paid-up Capital (%)</th>
                                                <td>
                                                    {!! Form::text('paid_cap_amount','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('paid_cap_amount','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('paid_cap_nature','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('paid_cap_nature','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('paid_cap_percentage','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('paid_cap_percentage','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>External Borrowing (%)</th>
                                                <td>
                                                    {!! Form::text('ext_borrow_amount','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('ext_borrow_amount','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('ext_borrow_nature','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('ext_borrow_nature','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('ext_borrow_percentage','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('ext_borrow_percentage','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-lg-12">Particular's of Sponsors / Directors:<hr></div>
                        </div>

                        <div class="form-group" style="clear:both">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="directors_list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                        <thead class="alert alert-success">
                                            <tr class="text-center">
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Nationality</th>
                                                <th>Status in the proposed company</th>
                                                <th>Extent of share Holding</th>
                                                <th> # </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {!! Form::text('sponsor_name[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('sponsor_name','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('sponsor_address[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('sponsor_address','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('sponsor_nationality[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('sponsor_nationality','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('sponsor_status[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('sponsor_status','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('sponsor_share_ext[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('sponsor_share_ext','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    <a class="btn btn-xs btn-primary addSponsorRows" onclick="addSponsorRows(this.form);"><i class="fa fa-plus"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <p>Fields marked with stars  (<span style="color: red">*</span>) are mandatory</p>
                    </fieldset>

                    <h3>Proposed Project Information (Part B)</h3>
                    <fieldset>
                        <div class=" col-md-12 ">
                            {!! Form::label('products',' 1. Products:', ['class'=>'text-left text-title']) !!}
                        </div>  

                        <div class=" col-md-12 {{$errors->has('product_name') ? 'has-error': ''}}">
                            {!! Form::label('product_name','a) Name / description of the product(s):' ,
                            ['class'=>'col-md-5 text-left required-star']) !!}
                            <div class="col-md-7">
                                {!! Form::text('product_name', '', ['class' => 'form-control input-sm required']) !!}
                                {!! $errors->first('product_name','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>

                        <div class=" col-md-12 {{$errors->has('product_usage') ? 'has-error': ''}}">
                            {!! Form::label('product_usage', 'b) Usage of the product(s):' ,
                            ['class'=>'col-md-5 text-left required-star']) !!}
                            <div class="col-md-7">
                                {!! Form::text('product_usage', '', ['class' => 'form-control input-sm required required-star']) !!}
                                {!! $errors->first('product_usage','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                            </div>
                        </div>        

                        <div class=" col-md-12 ">
                            {!! Form::label('productionPrg',' 2. Production Programme:', ['class'=>'text-left text-title']) !!}
                        </div>  

                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="productionPrgTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead class="alert alert-success">
                                        <tr class="text-center">
                                            <th class="text-center"> <span class="required-star"></span>Description</th>
                                            <th class="text-center">1st Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">2nd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">3rd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">4th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">5th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center"> # </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                {!! Form::text('production_desc[]','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('production_desc','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('production_1st[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('production_1st','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('production_2nd[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('production_2nd','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('production_3rd[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('production_3rd','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('production_4th[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('production_4th','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('production_5th[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('production_5th','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                <a class="btn btn-xs btn-primary addProductionRows" onclick="addProductionRows(this.form);">
                                                    <i class="fa fa-plus"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class=" col-md-12 ">
                            {!! Form::label('proExport','3. Projection of Export:', ['class'=>'text-left text-title']) !!}
                        </div>  

                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="proExportTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead class="alert alert-info">
                                        <tr>
                                            <th class="text-center">Description <span class="required-star"></span></th>
                                            <th class="text-center">1st Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">2nd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">3rd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">4th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center">5th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                            <th class="text-center"> # </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                {!! Form::text('pro_ext_desc[]','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pro_ext_desc','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('pro_ext_1st[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pro_ext_1st','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('pro_ext_2nd[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pro_ext_2nd','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('pro_ext_3rd[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pro_ext_3rd','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('pro_ext_4th[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pro_ext_4th','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('pro_ext_5th[]','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pro_ext_5th','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                <a class="btn btn-xs btn-primary addProExportRows" onclick="addProExportRows(this.form);">
                                                    <i class="fa fa-plus"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class=" col-md-12 ">
                            {!! Form::label('manpowerReq','4. Manpower requirements:', ['class'=>'text-left text-title']) !!}
                        </div>  

                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="mpReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead class="alert alert-warning">
                                        <tr>
                                            <th>Year <span class="required-star"></span></th>
                                            <th>Job  <span class="required-star"></span></th>
                                            <th>Foreign  <span class="required-star"></span></th>
                                            <th>Local  <span class="required-star"></span></th>
                                            <th>Total  <span class="required-star"></span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                {!! Form::text('mp_year_1','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_year_1','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_job_1','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_job_1','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_foreign_1','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_foreign_1','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('mp_local_1','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_local_1','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_total_1','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_total_1','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::text('mp_year_2','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_year_2','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_job_2','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_job_2','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_foreign_2','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_foreign_2','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('mp_local_2','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_local_2','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_total_2','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_total_2','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::text('mp_year_3','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_year_3','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_job_3','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_job_3','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_foreign_3','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_foreign_3','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('mp_local_3','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_local_3','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_total_3','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_total_3','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::text('mp_year_4','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_year_4','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_job_4','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_job_4','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_foreign_4','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_foreign_4','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('mp_local_4','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_local_4','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_total_4','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_total_4','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::text('mp_year_5','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_year_5','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_job_5','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_job_5','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_foreign_5','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_foreign_5','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>{!! Form::text('mp_local_5','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_local_5','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('mp_total_5','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('mp_total_5','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class=" col-md-12 ">
                            {!! Form::label('productionSalesCost','5. Cost of Production and Sales Revenue (at 100% capacity):', ['class'=>'col-md-7 text-left text-title']) !!}
                            <div class="col-md-1 text-right">US$</div>
                            <div class="col-md-3">
                                {!! Form::text('production_cost', '', ['class' => 'form-control input-sm required required-star']) !!}
                                {!! $errors->first('production_cost','<span class="help-block">:message</span>') !!}
                                <p class="text-danger pss-error"></p>
                                <span class="text-muted text-sm"> Cost per unit / Total cost </span>
                            </div>
                        </div>  

                        <div class="col-md-12 ">
                            {!! Form::label('productionCost','5.1. Cost of Production:', ['class'=>'col-md-7 text-left']) !!}
                            <div class="col-md-12 ">
                                {!! Form::label('rawMaterials','i) Raw Materials: ', ['class'=>'col-md-4 text-left']) !!}
                                <div class="col-md-12 ">
                                    {!! Form::label('rawCostBd','a) From Bangladesh:', ['class'=>'col-md-4 text-left required-star']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('raw_cost_bd', '', ['class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('raw_cost_bd','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    {!! Form::label('rawCostOther','b) From other countries:', ['class'=>'col-md-4 text-left required-star']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('raw_cost_other', '', ['class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('raw_cost_other','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                {!! Form::label('packMaterials','ii) Packaging Materials: ', ['class'=>'col-md-4 text-left']) !!}
                                <div class="col-md-12 ">
                                    {!! Form::label('packCostBd','a) From Bangladesh:', ['class'=>'col-md-4 text-left required-star']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('pac_cost_bd', '', ['class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('pac_cost_bd','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    {!! Form::label('pacCostOther','b) From other countries:', ['class'=>'col-md-4 text-left required-star']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('pac_cost_other', '', ['class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('pac_cost_other','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>
                            </div>  
                        </div>

                        <div class=" col-md-12 ">
                            {!! Form::label('infrastructureReq','6. Required Infrastructure:', ['class'=>'text-left text-title']) !!}
                        </div> 

                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="infraReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                    <thead class="alert alert-success">
                                        <tr>
                                            <th>Infrastructure </th>
                                            <th>Initial Period (per day) <span class="required-star"></span></th>
                                            <th>Regular Operation Period (per day) <span class="required-star"></span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                {!! Form::label('land','Land (in ACR)', ['class' => 'text-left']) !!}
                                            </td>
                                            <td>{!! Form::text('land_ini','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('land_ini','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('land_reg','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('land_reg','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::label('power','Power (in KW/H)', ['class' => 'text-left']) !!}
                                            </td>
                                            <td>{!! Form::text('power_ini','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('power_ini','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('power_reg','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('power_reg','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::label('gas','GAS  (in CM)', ['class' => 'text-left']) !!}
                                            </td>
                                            <td>{!! Form::text('gas_ini','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('gas_ini','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('gas_reg','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('gas_reg','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! Form::label('water ','Water   (in CM)', ['class' => 'text-left']) !!}
                                            </td>
                                            <td>{!! Form::text('water_ini','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('water_ini','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                            <td>
                                                {!! Form::text('water_reg','', ['data-rule-maxlength'=>'100',
                                                'class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('water_reg','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
						
						<div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Required  Document s for attachment</div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover ">
                                                    <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th colspan="6">Required Attachments</th>
                                                        <th colspan="2">Attached PDF file <spna data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 1MB)!"><i class="fa fa-question-circle" aria-hidden="true"></i></spna></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php  $i = 1; ?>
                                                    @foreach($document as $row)
                                                        <tr>
                                                            <td><div align="center">{!! $i !!}<?php echo $row->doc_priority == "1" ? "<span style='color:red'>*</span>" : "";?></div></td>
                                                            <td colspan="6">{!!  $row->doc_name !!}</td>
                                                            <td colspan="2">
                                                                <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>" name="doc_name_<?php echo $row->doc_id; ?>" />
                                                                <input name="file<?php echo $row->doc_id; ?>" <?php /* echo $row->doc_priority == "1" ? "class='required'" : "";*/?> id="file<?php echo $row->doc_id; ?>" type="file" size="20" onchange="uploadDocument('preview_<?php echo $row->doc_id;  ?>', this.id, 'validate_field_<?php echo $row->doc_id;  ?>', <?php echo $row->doc_priority;  ?>)"/>
                                                                <div id="preview_<?php echo $row->doc_id;  ?>">
                                                                    <input type="hidden" <?php echo $row->doc_priority == "1" ? "class='required'" : "";?> value="" id="validate_field_<?php echo $row->doc_id; ?>" name="validate_field_<?php echo $row->doc_id; ?>" />
                                                                </div>

                                                            </td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                            </div>

                        <p>Fields marked with stars  (<span style="color: red">*</span>) are mandatory</p>

                    </fieldset>

                    <h3>Submit</h3>
                    <fieldset>
                        <legend>Terms and Conditions</legend>

                        <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms-2">
                            I agree with the Terms and Conditions.</label>
                    </fieldset>
                    {!! Form::close() !!}<!-- /.form end -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer-script')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>

<link rel="stylesheet" href="{{ url('assets/css/jquery.steps.css') }}">
{{--<script src="{{ asset("assets/scripts/modernizr-2.6.2.min.js") }}"></script>
<script src="{{ asset("assets/scripts/jquery.cookie-1.3.1.js") }}"></script>--}}
<script src="{{ asset("assets/scripts/jquery.steps.js") }}"></script>

<script type="text/javascript">
			function uploadDocument(targets, id, vField, isRequired)
			{
				var form = $("#" + id).closest("form").attr('id');
				document.getElementById("isRequired").value = isRequired;
				document.getElementById("selected_file").value = id;
				document.getElementById("validateFieldName").value = vField;
				document.getElementById(targets).style.color = "red";
				var postData = $("#" + form).serializeArray();
				var action = document.getElementById(form).action;
				document.getElementById(form).action = "{{url()}}/apps/upload-document";

				try
				{
					$("#" + id).closest('form').data("validator").cancelSubmit = true;
				} catch (err) {
				}
				$("#" + id).html('');
				$("#" + targets).html('Uploading....');
				$("#"+form).ajaxForm({
					target: '#' + targets,
					data: postData
				}).submit();
				document.getElementById(form).action = action;
				document.getElementById(form).setAttribute("target", "_self");
			}
            $(document).ready(function () {
				
				$('[data-toggle="tooltip"]').tooltip();
				
                var form = $("#appClearenceForm").show();
                form.steps({
                    headerTag: "h3",
                    bodyTag: "fieldset",
                    transitionEffect: "slideLeft",
                    onStepChanging: function (event, currentIndex, newIndex) {
                        // Allways allow previous action even if the current form is not valid!
                        if (currentIndex > newIndex)
                        {
                            return true;
                        }
                        // Forbid next action on "Warning" step if the user is to young
                        if (newIndex === 3 && Number($("#age-2").val()) < 18)
                        {
                            return false;
                        }
                        // Needed in some cases if the user went back (clean up)
                        if (currentIndex < newIndex)
                        {
                            // To remove error styles
                            form.find(".body:eq(" + newIndex + ") label.error").remove();
                            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                        }
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                    },
                    onStepChanged: function (event, currentIndex, priorIndex) {
                        // Used to skip the "Warning" step if the user is old enough.
                        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
                        {
                            form.steps("next");
                        }
                        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
                        if (currentIndex === 2 && priorIndex === 3)
                        {
                            form.steps("previous");
                        }
                    },
                    onFinishing: function (event, currentIndex) {
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function (event, currentIndex) {
                        alert("Submitted!");
                    }
                }).validate({
                    errorPlacement: function errorPlacement(error, element) {
                        element.before(error);
                    }
                });

                var today = new Date();
                var yyyy = today.getFullYear();
                var mm = today.getMonth();
                var dd = today.getDate();
                $('#construction_start, .construction_start').datetimepicker({
                    viewMode: 'years',
                    format: 'DD-MMM-YYYY',
                    minDate: '01/01/' + (yyyy - 6)
                });

                $('#construction_end, .construction_end').datetimepicker({
                    format: 'DD-MMM-YYYY',
                    useCurrent: false // Important! See issue #1075
                });
                startDateSelected = '';
                $(".construction_start").on("dp.change", function (e) {
                    $('#construction_end').data("DateTimePicker").minDate(e.date);
                    $('.construction_end').data("DateTimePicker").minDate(e.date);
                    startDateSelected = e.date;
                });

                $(".construction_end").on("dp.change", function (e) {
                    var startDate = startDateSelected;
                    var endDate = e.date;
                    if (startDate != '' && endDate != '') {
                        var days = (endDate - startDate) / 1000 / 60 / 60 / 24;
                        $('#construction_duration').val(Math.floor(days));
                    }
                });

                $("input[type=radio]").click(function () {
                    var id = $(this).attr("id");
                    var thename = $(this).attr("name");
                    $("input[name=" + thename + "]").removeAttr('checked');
                    $("#" + id).attr('checked', "checked");
                });

                $("select").change(function () {
                    var id = $(this).attr("id");
                    var val = $(this).val();
//
                    $("#" + id).find('option').removeAttr("selected");
                    $("#" + id).val(val);
                });

                $("input[type=file]").change(function () {
                    var fileName = $(this).val();
                    var id = $(this).attr("id");
                    var fileNameArr = fileName.split("\\");
                    var l = fileNameArr.length;
                    if ($('#label_' + id).length)
                        $('#label_' + id).remove();

                    var newInput = $('<br/><label id="label_' + id + '"><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                    $(this).after(newInput);
                });


                var popupWindow = null;
                $('.finish').on('click', function (e) {
                    $('#home').css({"display": "none"});
                    popupWindow = window.open('<?php echo URL::to('/preview.php'); ?>', 'Sample', '');
//	popupWindow.moveTo(0, 0);
//	popupWindow.resizeTo(screen.availWidth, screen.availHeight);
                });
            });

            var rowCount = 0;
            function addSponsorRows(frm) {
                rowCount++;
                var recRow = '<tr id="rowCount' + rowCount + '"> \n\
        <td> <input data-rule-maxlength="100" class="form-control input-sm required" name="sponsor_name[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td> <input data-rule-maxlength="100" class="form-control input-sm required" name="sponsor_address[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="sponsor_nationality[]" type="text" value="">\n\
         <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="sponsor_status[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="sponsor_share_ext[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeSponsorRow(' + rowCount + ');">\n\
        <i class="fa fa-times" aria-hidden="true"></i></a></td></tr>';
                $('#directors_list').append(recRow);
            }

            function removeSponsorRow(removeNum) {
                $('#rowCount' + removeNum).remove();
            }

            function in_joint_com(sel) {
                var thisVal = sel;
                if (thisVal !== '' && thisVal === 'Joint Venture') {
                    $('.add_new_span').html('<a class="btn btn-xs btn-primary addNewJointVe" onclick="addNewJointVe(this.form);"><i class="fa fa-plus"></i></a>');
                } else {
                    $('.add_new_span').html('-');
                    $('.otherJointRows').remove();
                }
            }
            ;

            var joinRows = 0;
            var countrySelect = '{!! Form::select('joint_com_country[]', $countries, '', ['class' => 'form-control input-sm required']) !!}';
            function addNewJointVe(frm) {
                joinRows++;
                var recJoin = '<tr class="otherJointRows" id="joinRows' + joinRows + '">\n\
<td><input data-rule-maxlength="100" class="form-control input-sm required" name="joint_company[]" type="text" value="">\n\
     <p class="text-danger pss-error"></p></td>\n\
<td> <input data-rule-maxlength="100" class="form-control input-sm required" name="joint_company_address[]" type="text" value=""> \n\
    <p class="text-danger pss-error"></p></td>\n\
<td>' + countrySelect + '<p class="text-danger pss-error"></p></td>\n\
<td><a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeJoint(' + joinRows + ');">\n\
        <i class="fa fa-times" aria-hidden="true"></i></a></td></tr>';
                $('#type_of_organizations').append(recJoin);
            }

            function removeJoint(removeNum) {
                $('#joinRows' + removeNum).remove();
            }

            //auth_capital_fo
            function authCapitalFo() {
                var get_auth_capital_fo = $("input[name='auth_capital_fo']").val();
                var auth_capital_fo = parseInt((get_auth_capital_fo != '' ? get_auth_capital_fo : 0));
                var get_auth_capital_lo = $("input[name='auth_capital_lo']").val();
                var auth_capital_local = parseInt((get_auth_capital_lo != '' ? get_auth_capital_lo : 0));
                var total_auth_capital = eval(auth_capital_fo + auth_capital_local);
                $("input[name='auth_capital_to']").val(total_auth_capital);
            }

            function authCapitalLo() {
                var get_auth_capital_lo = $("input[name='auth_capital_lo']").val();
                var auth_capital_lo = parseInt((get_auth_capital_lo != '' ? get_auth_capital_lo : 0));
                var get_auth_capital_fo = $("input[name='auth_capital_fo']").val();
                var auth_capital_fo = parseInt((get_auth_capital_fo != '' ? get_auth_capital_fo : 0));
                var total_auth_capital = eval(auth_capital_fo + auth_capital_lo);
                $("input[name='auth_capital_to']").val(total_auth_capital);
            }

            //paid_capital_fo
            function paidCapitalFo() {
                var get_paid_capital_fo = $("input[name='paid_capital_fo']").val();
                var paid_capital_fo = parseInt((get_paid_capital_fo != '' ? get_paid_capital_fo : 0));
                var get_paid_capital_lo = $("input[name='paid_capital_lo']").val();
                var paid_capital_lo = parseInt((get_paid_capital_lo != '' ? get_paid_capital_lo : 0));
                var total_paid_capital = eval(paid_capital_fo + paid_capital_lo);
                $("input[name='paid_capital_to']").val(total_paid_capital);
            }

            function paidCapitalLo() {
                var get_paid_capital_lo = $("input[name='paid_capital_lo']").val();
                var paid_capital_lo = parseInt((get_paid_capital_lo != '' ? get_paid_capital_lo : 0));
                var get_paid_capital_fo = $("input[name='paid_capital_fo']").val();
                var paid_capital_fo = parseInt((get_paid_capital_fo != '' ? get_paid_capital_fo : 0));
                var total_paid_capital = eval(paid_capital_fo + paid_capital_lo);
                $("input[name='paid_capital_to']").val(total_paid_capital);
            }
            ;

            //ext_borrow_fo
            function extBorrowFo() {
                var get_ext_borrow_fo = $("input[name='ext_borrow_fo']").val();
                var ext_borrow_fo = parseInt((get_ext_borrow_fo != '' ? get_ext_borrow_fo : 0));
                var get_ext_borrow_lo = $("input[name='ext_borrow_lo']").val();
                var ext_borrow_lo = parseInt((get_ext_borrow_lo != '' ? get_ext_borrow_lo : 0));
                var total_ext_capital = eval(ext_borrow_fo + ext_borrow_lo);
                $("input[name='ext_borrow_to']").val(total_ext_capital);
            }
            ;

            function extBorrowLo() {
                var get_ext_borrow_lo = $("input[name='ext_borrow_lo']").val();
                var ext_borrow_lo = parseInt((get_ext_borrow_lo != '' ? get_ext_borrow_lo : 0));
                var get_ext_borrow_fo = $("input[name='ext_borrow_fo']").val();
                var ext_borrow_fo = parseInt((get_ext_borrow_fo != '' ? get_ext_borrow_fo : 0));
                var total_ext_capital = eval(ext_borrow_fo + ext_borrow_lo);
                $("input[name='ext_borrow_to']").val(total_ext_capital);
            }
            ;

            function validate_number(evt) {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault)
                        theEvent.preventDefault();
                }
            }

            var rowProductionCount = 0;
            function addProductionRows(frm) {
                rowProductionCount++;
                var productionRow = '<tr id="rowProductionCount' + rowProductionCount + '"> \n\
        <td> <input data-rule-maxlength="100" class="form-control input-sm required" name="production_desc[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td> <input data-rule-maxlength="100" class="form-control input-sm required" name="production_1st[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="production_2nd[]" type="text" value="">\n\
         <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="production_3rd[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="production_4th[]" type="text" value="">\n\
         <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="production_5th[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeProductionRow(' + rowProductionCount + ');">\n\
        <i class="fa fa-times" aria-hidden="true"></i></a></td></tr>';
                $('#productionPrgTbl').append(productionRow);
            }

            function removeProductionRow(removeNum) {
                $('#rowProductionCount' + removeNum).remove();
            }

            var rowProExportCount = 0;
            function addProExportRows(frm) {
                rowProExportCount++;
                var proExportRow = '<tr id="rowProExportCount' + rowProExportCount + '"> \n\
        <td> <input data-rule-maxlength="100" class="form-control input-sm required" name="pro_ext_desc[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td> <input data-rule-maxlength="100" class="form-control input-sm required" name="pro_ext_1st[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="pro_ext_2nd[]" type="text" value="">\n\
         <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="pro_ext_3rd[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="pro_ext_4th[]" type="text" value="">\n\
         <p class="text-danger pss-error"></p></td>\n\
        <td><input data-rule-maxlength="100" class="form-control input-sm required" name="pro_ext_5th[]" type="text" value=""> \n\
        <p class="text-danger pss-error"></p></td>\n\
        <td><a href="javascript:void(0);" class="btn btn-xs btn-danger removeRow" onclick="removeProExportRow(' + rowProExportCount + ');">\n\
        <i class="fa fa-times" aria-hidden="true"></i></a></td></tr>';
                $('#proExportTbl').append(proExportRow);
            }

            function removeProExportRow(removeNum) {
                $('#rowProExportCount' + removeNum).remove();
            }

//            jQuery.extend(jQuery.validator.messages, {
//                required: "This field is required.",
//                remote: "Please fix this field.",
//                email: "Please enter a valid email address.",
//                url: "Please enter a valid URL.",
//                date: "Please enter a valid date.",
//                dateISO: "Please enter a valid date (ISO).",
//                number: "Please enter a valid number.",
//                digits: "Please enter only digits.",
//                creditcard: "Please enter a valid credit card number.",
//                equalTo: "Please enter the same value again.",
//                accept: "Please enter a value with a valid extension.",
//                maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
//                minlength: jQuery.validator.format("Please enter at least {0} characters."),
//                rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
//                range: jQuery.validator.format("Please enter a value between {0} and {1}."),
//                max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
//                min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
//            });
</script>

@endsection <!--- footer-script--->

