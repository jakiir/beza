@extends('layouts.admin')

{{--@section('page_heading','<i class="fa fa-edit"></i> '.trans('messages.app_form'))--}}

@section("content")
@include('partials.messages')
<section class="content">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="box-body">
                    <div class="col-md-8">
                        <div class="row">
                            <!--<h4>Clearence Certificate Applying Process</h4>-->
                            <div class="btn-group btn-breadcrumb">
                                <span class="btn btn-success custom-font"><b>General Information</b></span>
                                <span class="btn btn-primary custom-font"><b>Details Application Information</b></span>
                                <span class="btn btn-info custom-font"><b>Confirmation</b></span>
                            </div>
                            <hr/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="box-header with-border">
                                {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
                            </div>


                            {!! Form::open(['url' => '#', 'method' => 'get', 'class' => 'form work_permit_form', 'role' => 'form','id' => 'basic_info']) !!}

                            <div class="form-group {{$errors->has('form_id') ? 'has-error' : ''}}">
                                {!! Form::label('form_id','Application Type',['class'=>'required-star']) !!}
                                {!! Form::select('form_id', $services, $selected_form_url, ['class' => 'form-control required','onchange'=>'selectForm(this)']) !!}
                                {!! $errors->first('form_id','<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group {{$errors->has('track_no') ? 'has-error' : ''}} workPermitField" style="display: none">
                                {!! Form::label('track_no','Ref No. of Issued Work Permit (If any)') !!}
                                {!! Form::text('track_no','',['class'=>'form-control', 'placeholder'=>'Enter reference no..']) !!}
                                {!! $errors->first('track_no','<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('/dashboard') }}">{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}</a>
                                {!! Form::button('<i class="fa fa-save"></i> Next', array('type' => 'submit', 'value'=> 'Next', 'class' => 'btn btn-success  pull-right selectionNext')) !!}
                            </div><!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box box-success col-md-6 company_details" style="display: none">
                    <div class="box-body">
                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                        <p class="text-muted location">Kawranbazar, Dhaka</p>
                        <strong><i class="fa fa-file-text-o margin-r-5"></i> Contact </strong>
                        <p class="contact">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('footer-script')
<script>
    function selectForm(THIS) {
        var thisText = $(THIS).find("option:selected").text();
        var errorClass = $('.workPermitField').find('.form-control').val('');
        if (errorClass != '') {
            $('#form_id').removeClass('error');
        }
        $('.workPermitField').hide();
        if (thisText == 'Work Permit') {
            $('.workPermitField').show();
        }
    }
    $(document).ready(
            function () {
                $('.selectionNext').click(function () {
                    var base_url = '<?php echo url(); ?>'
                    var serviceId = $('#form_id').val();
                    var actionForm = "{{url('/apps/application-selection-check')}}";
                    if (serviceId != '') {
                        if (serviceId == '/#') {
                            $('#form_id').addClass('error');
                        } else {
                            window.location.href = base_url + '/' + serviceId;
                        }
                        /*$.ajax({
                         url:actionForm,
                         dataType: 'JSON',
                         type: 'post',
                         data: {
                         'serviceId' :serviceId,
                         '_token' : "{{ csrf_token() }}"
                         },
                         success: function(response){
                         console.log(response);
                         if(response.responseCode == 1){
                         if(response.url == '/#'){
                         alert('This form is disabled now!');
                         } else {
                         window.location.href = response.url;
                         }
                         } else {
                         alert(response.msg);
                         }
                         }
                         });*/
                    } else {
                        $('#form_id').addClass('error');
                    }
                    return false;

                });
            });
</script>
@endsection
