@extends('layouts.admin')

@section('content')

<style>
    .wizard {
        margin: 20px auto;
        background: #fff;
    }
    .text_box{width: 95%;margin: 4px 4px 5px 4px}

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

    .connecting-line {
        height: 2px;
        background: #e0e0e0;
        position: absolute;
        width: 80%;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: 50%;
        z-index: 1;
    }

    .wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
        color: #555555;
        cursor: default;
        border: 0;
        border-bottom-color: transparent;
    }

    span.round-tab {
        width: 70px;
        height: 70px;
        line-height: 70px;
        display: inline-block;
        border-radius: 100px;
        background: #fff;
        border: 2px solid #e0e0e0;
        z-index: 2;
        position: absolute;
        left: 0;
        text-align: center;
        font-size: 25px;
    }
    span.round-tab i{
        color:#555555;
    }
    .wizard li.active span.round-tab {
        background: #fff;
        border: 2px solid #5bc0de;

    }
    .wizard li.active span.round-tab i{
        color: #5bc0de;
    }

    span.round-tab:hover {
        color: #333;
        border: 2px solid #333;
    }

    .wizard .nav-tabs > li {
        width: 25%;
    }

    .wizard li:after {
        content: " ";
        position: absolute;
        left: 46%;
        opacity: 0;
        margin: 0 auto;
        bottom: 0px;
        border: 5px solid transparent;
        border-bottom-color: #5bc0de;
        transition: 0.1s ease-in-out;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 46%;
        opacity: 1;
        margin: 0 auto;
        bottom: 0px;
        border: 10px solid transparent;
        border-bottom-color: #5bc0de;
    }

    .wizard .nav-tabs > li a {
        width: 70px;
        height: 70px;
        margin: 20px auto;
        border-radius: 100%;
        padding: 0;
    }

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

    .wizard .tab-pane {
        position: relative;
        padding-top: 50px;
    }

    .wizard h3 {
        margin-top: 0;
    }
    .step1 .row {
        margin-bottom:10px;
    }
    .step_21 {
        border :1px solid #eee;
        border-radius:5px;
        padding:10px;
    }
    .step33 {
        border:1px solid #ccc;
        border-radius:5px;
        padding-left:10px;
        margin-bottom:10px;
    }
    .dropselectsec {
        width: 68%;
        padding: 6px 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
        color: #333;
        margin-left: 10px;
        outline: none;
        font-weight: normal;
    }
    .dropselectsec1 {
        width: 74%;
        padding: 6px 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
        color: #333;
        margin-left: 10px;
        outline: none;
        font-weight: normal;
    }
    .mar_ned {
        margin-bottom:10px;
    }
    .wdth {
        width:25%;
    }
    .birthdrop {
        padding: 6px 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
        color: #333;
        margin-left: 10px;
        width: 16%;
        outline: 0;
        font-weight: normal;
    }


    /* according menu */
    #accordion-container {
        font-size:13px
    }
    .accordion-header {
        font-size:13px;
        background:#ebebeb;
        margin:5px 0 0;
        padding:7px 20px;
        cursor:pointer;
        color:#fff;
        font-weight:400;
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border-radius:5px
    }
    .unselect_img{
        width:18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .active-header {
        -moz-border-radius:5px 5px 0 0;
        -webkit-border-radius:5px 5px 0 0;
        border-radius:5px 5px 0 0;
        background:#F53B27;
    }
    .active-header:after {
        content:"\f068";
        font-family:'FontAwesome';
        float:right;
        margin:5px;
        font-weight:400
    }
    .inactive-header {
        background:#333;
    }
    .inactive-header:after {
        content:"\f067";
        font-family:'FontAwesome';
        float:right;
        margin:4px 5px;
        font-weight:400
    }
    .accordion-content {
        display:none;
        padding:20px;
        background:#fff;
        border:1px solid #ccc;
        border-top:0;
        -moz-border-radius:0 0 5px 5px;
        -webkit-border-radius:0 0 5px 5px;
        border-radius:0 0 5px 5px
    }
    .accordion-content a{
        text-decoration:none;
        color:#333;
    }
    .accordion-content td{
        border-bottom:1px solid #dcdcdc;
    }



    @media( max-width : 585px ) {

        .wizard {
            width: 90%;
            height: auto !important;
        }

        span.round-tab {
            font-size: 16px;
            width: 50px;
            height: 50px;
            line-height: 50px;
        }

        .wizard .nav-tabs > li a {
            width: 50px;
            height: 50px;
            line-height: 50px;
        }

        .wizard li.active:after {
            content: " ";
            position: absolute;
            left: 35%;
        }
    }
</style>


<div class="container">
    <div class="row">
        <section>
            <div class="panel panel-primary">
                <div class="panel-heading">
                   <h4>Application Form</h4>
                </div>
                <div class="panel-body">
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        {!! Form::open(array('url' => 'foo/bar','class'=>'form-horizontal')) !!}
                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <div class="step1">
                                    <div class="form-group">
                                        {!! Form::label('applicant_name', '1.Name of Applicant',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::text('applicant_name','',['class' => 'form-control','placeholder'=>'Enter your applicant name']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('firm_name','Name of the firm',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::text('firm_name','',['class' => 'form-control','placeholder'=>'Enter your firm name']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('address','Address',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                {!! Form::label('district','District',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::select('district',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select District Name...']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('Police_station','Police Station',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::select('Police_station',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select Police Station ...']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('address','Address',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::textarea('address', null, ['class' => 'form-control','rows'=>'3','placeholder'=>'Enter your address']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('post_code','Post Code',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::number('post_code','',['class' => 'form-control','placeholder'=>'Enter your post code']) !!}
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('phone_number', 'Phone No.',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::number('phone_number','',['class' => 'form-control','placeholder'=>'Enter your phone number']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('fax_number', ' Fax No.',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::number('fax_number','',['class' => 'form-control','placeholder'=>'(if any)']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email', 'E-mail',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::email('email','',['class' => 'form-control','placeholder'=>'Enter your email.']) !!}
                                        </div>
                                    </div>
                                    <b>2. Applicant's official representative's to be contacted:</b><br/><br/>

                                    <div class="form-group">
                                        {!! Form::label('applicant_name_two', 'Name of Applicant',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::text('applicant_name_two','',['class' => 'form-control','placeholder'=>'Enter applicant name']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('address','Address',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                {!! Form::label('district_two','District',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::select('district_two',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select District Name...']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('Police_station_two','Police Station',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::select('Police_station_two',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select Police Station ...']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('address_two','Address',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::textarea('address_two', null, ['class' => 'form-control','rows'=>'3','placeholder'=>'Enter your address']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('post_code_two','Post Code',['class' => 'col-sm-2 control-label']) !!}
                                                <div class="col-sm-5">
                                                    {!! Form::number('post_code_two','',['class' => 'form-control','placeholder'=>'Enter your post code']) !!}
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('phone_number_two', 'Phone No.',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::number('phone_number_two','',['class' => 'form-control','placeholder'=>'Enter your phone number']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('fax_number_two', ' Fax No.',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::number('fax_number_two','',['class' => 'form-control','placeholder'=>'(if any)']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('email_two', 'E-mail',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::email('email_two','',['class' => 'form-control','placeholder'=>'Enter representative email.']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Project_name', ' Name of the proposed Project',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::text('Project_name','',['class' => 'form-control','placeholder'=>'Enter your project name']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('business_type', ' Type of Business / Industry',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::select('business_type',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select Type of Business.']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('products_type', 'Type of Products',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::select('products_type',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select Type of Products..']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('organizations_type', 'Type of Organizations',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::select('organizations_type',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select Type of Organizations..']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('zone_name', 'Name of the zone',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::select('zone_name',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select  Name of the zone..']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('particular_sponsor', 'Particular of Sponsors / Directors',['class' => 'col-sm-3 control-label']) !!}
                                        <div class="col-sm-5">
                                            {{--{!! Form::select('zone_name',['L' => 'Large', 'S' => 'Small'],NULL,['class'=>'form-control','placeholder' => 'Select  Name of the zone..']) !!}--}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <table width="98%" border="1" class="test" id="tbUser">
                                                        <tr>
                                                            <th>Name & Address</th>
                                                            <th>Nationality</th>
                                                            <th>Status in the proposed company</th>
                                                            <th>Extent of share Holding</th>
                                                            <th style="width: 100px">Action</th>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" name="address_name[]" class="form-control text_box"> </td>
                                                            <td><input type="text" name="nationaltiy[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="company_name[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="share_holding[]" class="form-control text_box"/></td>
                                                            <td><input type="button" value="Add" class="publish text_box btn btn-primary" /></td>
                                                        </tr>
                                                    </table>


                                                </div>
                                            </div>
                                        </div>
                                    </div>









                                </div>
                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                                </ul>
                            </div>


                            <div class="tab-pane" role="tabpanel" id="step2">
                                <div class="step2">


                                    <b>1. Products:</b>
                                    <div class="form-group">
                                        {!! Form::label('product_description','a) Name / description of the product(s)',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::text('product_description','',['class' => 'form-control','placeholder'=>'Enter Name / description of the product(s)']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('use_product','b) Use of the product(s)',['class' => 'col-sm-2 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! Form::text('use_product','',['class' => 'form-control','placeholder'=>'Enter Use of the product(s)']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('production_programme','2. Production Programme:',['class' => 'col-sm2 control-label']) !!}
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <table width="98%" border="1" class="add_table" id="tbsecund">
                                                        <tr>
                                                            <th>Description</th>
                                                            <th>1st year</th>
                                                            <th>2nd year</th>
                                                            <th>3rd year</th>
                                                            <th>4th year</th>
                                                            <th>5th year</th>
                                                            <th style="width: 100px">Action</th>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" name="description[]" class="form-control text_box"> </td>
                                                            <td><input type="text" name="first_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="secund_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="third_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="fourth_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="five_year[]" class="form-control text_box"/></td>
                                                            <td><input type="button" value="Add" class="add_row text_box btn btn-primary" /></td>
                                                        </tr>
                                                    </table>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Projection_export', '3. Projection of Export',['class' => 'col-sm-2 control-label']) !!}
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <table width="98%" border="1" class="export_poject" id="projection_of_export">
                                                        <tr>
                                                            <th>Description</th>
                                                            <th>1st year</th>
                                                            <th>2nd year</th>
                                                            <th>3rd year</th>
                                                            <th>4th year</th>
                                                            <th>5th year</th>
                                                            <th style="width: 100px">Action</th>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td>Qty - Value</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" name="project_export_description[]" class="form-control text_box"> </td>
                                                            <td><input type="text" name="project_export_first_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="project_export_secund_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="project_export_third_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="project_export_fourth_year[]" class="form-control text_box"/></td>
                                                            <td><input type="text" name="project_export_five_year[]" class="form-control text_box"/></td>
                                                            <td><input type="button" value="Add" class="project_export text_box btn btn-primary" /></td>
                                                        </tr>
                                                    </table>


                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                    <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                                </ul>
                            </div>










                            <div class="tab-pane" role="tabpanel" id="step3">


                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Manpower requirements</div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" style="width: 98%">
                                                        <thead>
                                                        <tr>
                                                            <th colspan="3" style="text-align: center">Local(a)</th>
                                                            <th colspan="3" style="text-align: center">Foreign(b)</th>
                                                            <th>Grand Total</th>
                                                            <th colspan="2" style="text-align: center">Ration</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>Executive</td>
                                                            <td>Supporting Staff</td>
                                                            <td>Total</td>
                                                            <td>Executive</td>
                                                            <td>Supporting Staff</td>
                                                            <td>Total</td>
                                                            <td>(a+b)</td>
                                                            <td>Local</td>
                                                            <td>Foreign</td>
                                                        </tr>
                                                        <tr>
                                                            <td>{!! Form::text('local_executive','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('local_support_staff','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('local_total','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('foreign_executive','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('foreign_support_staff','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('foreign_total','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('summation','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('ration_local','',['class' => 'form-control']) !!}</td>
                                                            <td>{!! Form::text('ration_foreign','',['class' => 'form-control']) !!}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>

                                <div class="form-group ">
                                    {!! Form::label('Project_name', ' Cost of Production and Sales Revenue (at 100% capacity)',['class' => 'col-sm-5 control-label']) !!}
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            {!! Form::label('us', ' US$',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-6">
                                                {!! Form::text('Project_name','',['class' => 'form-control']) !!}
                                                <hr>Qty:Cost per unit/Total Cost
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    {!! Form::label('production_cost', ' a) Cost of Production',['class' => 'col-sm-2 control-label']) !!}
                                    <div class="col-sm-5">
                                        {!! Form::text('Project_name','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('production_cost', ' i) Raw Materials',['class' => 'col-sm-2 control-label']) !!}
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            {!! Form::label('bangladesh', ' aa) From Bangladesh',['class' => 'col-sm-5 control-label']) !!}
                                            <div class="col-sm-5">
                                                {!! Form::text('bangladesh','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('other_county', ' bb) From other countries',['class' => 'col-sm-5 control-label']) !!}
                                            <div class="col-sm-5">
                                                {!! Form::text('other_county','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('packaging_materials', ' ii) Packaging Materials',['class' => 'col-sm-2 control-label']) !!}
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            {!! Form::label('bangladesh', ' aa) From Bangladesh',['class' => 'col-sm-5 control-label']) !!}
                                            <div class="col-sm-5">
                                                {!! Form::text('packaging_form_bangladesh','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('other_county', ' bb) From other countries',['class' => 'col-sm-5 control-label']) !!}
                                            <div class="col-sm-5">
                                                {!! Form::text('packaging_form_other_county','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('power', ' iii) Power, gas, water and other utilies',['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-5">
                                        {!! Form::text('power_utilies','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('spare_parts', ' iv) Spare Parts',['class' => 'col-sm-2 control-label']) !!}
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            {!! Form::label('local', ' aa) Local',['class' => 'col-sm-5 control-label']) !!}
                                            <div class="col-sm-5">
                                                {!! Form::text('local','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('foreign', ' bb) Foreign',['class' => 'col-sm-5 control-label']) !!}
                                            <div class="col-sm-5">
                                                {!! Form::text('foreign','',['class' => 'form-control','placeholder'=>'Enter Cost of Production']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>









                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                    <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                                    <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="complete">
                                <div class="step44">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Required  Document s for attachment</div>
                                                <!-- /.panel-heading -->
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>No.</th>
                                                                <th>Required Attachments</th>
                                                                <th>Attached PDF file (Each File Maximum size 1MB)</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Copy of permission letter for branch/liaison/representative office or Memorandum Et Articles of 1 Association and Certificate of Incorporation of the company duly signed by the shareholders in case of locally incorporated.</td>
                                                                <td>{!! Form::file('file_one','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>Board resolution regarding employment of foreign national(s) including honorarium Et other benefit to be provided</td>
                                                                <td>{!! Form::file('file_two','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>Copy of passport (whole Used part)with arrival stamp, E-type visa for employees and PI-type visa for investors each of the pages choronologically </td>
                                                                <td>{!! Form::file('file_three','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>4</td>
                                                                <td>Service contract/ agreement and appointment letter/ transfer order in case of employee</td>
                                                                <td>{!! Form::file('file_four','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>5</td>
                                                                <td>Copies of all academic qualification Et professional experience certificate for the employee</td>
                                                                <td>{!! Form::file('file_five','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>6</td>
                                                                <td>Statement of the manpower showing list of local Et expatriate personnel employed with designation, salary break-up, nationality and date of first appointment</td>
                                                                <td>{!! Form::file('file_six','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>7</td>
                                                                <td>Up-to-date income tax clearance certificate of the company</td>
                                                                <td>{!! Form::file('file_seven','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>8</td>
                                                                <td>Encashment certificate of inward remittance of minimum USS 5,0000.00 as initial establishment cost for branch/ liason/ joint-venture and 100% foreign ownership company incorporation in Bangladesh </td>
                                                                <td>{!! Form::file('file_eight','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>9</td>
                                                                <td>Visa Recommendation Letter of the Expatriate/Investors.</td>
                                                                <td>{!! Form::file('file_nine','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>10</td>
                                                                <td>Attachment of companys comments as per remarks (if any)</td>
                                                                <td>{!! Form::file('file_ten','',['class' => 'form-control']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>11</td>
                                                                <td>A. documents shall have to be attested by the Chairman/ CEO / Managing director/ Country Manager/ N.B Chief executive of the Company/ firms. </td>
                                                                <td>Document's must be submitted by an authorized person of the organization including the letter of authorization. </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.table-responsive -->
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>
                                    </div>
                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                        <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                                        <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                                    </ul>


                                </div>
                            </div>
                            <div class="clearfix"> {{--{!! Form::submit('save',['class'=>'btn btn-primary','id'=>''])!!}--}}</div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="panel-footer"></div>
            </div>

        </section>
    </div>
</div>

<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }


    //according menu

    $(document).ready(function()
    {
        //Add Inactive Class To All Accordion Headers
        $('.accordion-header').toggleClass('inactive-header');

        //Set The Accordion Content Width
        var contentwidth = $('.accordion-header').width();
        $('.accordion-content').css({});

        //Open The First Accordion Section When Page Loads
        $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
        $('.accordion-content').first().slideDown().toggleClass('open-content');

        // The Accordion Effect
        $('.accordion-header').click(function () {
            if($(this).is('.inactive-header')) {
                $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
                $(this).toggleClass('active-header').toggleClass('inactive-header');
                $(this).next().slideToggle().toggleClass('open-content');
            }

            else {
                $(this).toggleClass('active-header').toggleClass('inactive-header');
                $(this).next().slideToggle().toggleClass('open-content');
            }
        });

        return false;
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.publish').click(function() {
            $(".test").append('<tr><td><input type="text" name="address_name[]" class="form-control text_box"></td><td><input type="text" name="nationaltiy[]" class="form-control text_box"/></td><td><input type="text" name="company_name[]" class="form-control text_box"/></td><td><input type="text" name="share_holding[]" class="form-control text_box"/></td><td><button class="btnDelete text_box btn btn-primary">Delete</button></td></tr>');
        });
//field add
        $('.publish').click(function() {
            if($(".test tr").length <= 5)
            {
                $(".test tr:last-child").add();
            }
            else
            {
                alert("You cannot add field  row");
            }
        });

    });
    //
    $("#tbUser").on('click','.btnDelete',function(){
        $(this).closest('tr').remove();

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.add_row').click(function() {
            $(".add_table").append('<tr><td><input type="text" name="description[]" class="form-control text_box"></td><td><input type="text" name="first_year[]" class="form-control text_box"/></td><td><input type="text" name="secund_year[]" class="form-control text_box"/></td><td><input type="text" name="third_year[]" class="form-control text_box"/></td><td><input type="text" name="fourth_year[]" class="form-control text_box"/></td><td><input type="text" name="five_year[]" class="form-control text_box"/></td><td><button class="btnDelete text_box btn btn-primary">Delete</button></td></tr>');
        });
//field add
        $('.add_row').click(function() {
            if($(".add_table tr").length <= 5)
            {
                $(".add_table tr:last-child").add();
            }
            else
            {
                alert("You cannot add field  row");
            }
        });

    });
    //
    $("#tbsecund").on('click','.btnDelete',function(){
        $(this).closest('tr').remove();

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.project_export').click(function() {
            $(".export_poject").append('<tr><td><input type="text" name="project_export_description[]" class="form-control text_box"></td><td><input type="text" name="project_export_first_year[]" class="form-control text_box"/></td><td><input type="text" name="project_export_secund_year[]" class="form-control text_box"/></td><td><input type="text" name="project_export_third_year[]" class="form-control text_box"/></td><td><input type="text" name="project_export_fourth_year[]" class="form-control text_box"/></td><td><input type="text" name="project_export_five_year[]" class="form-control text_box"/></td><td><button class="btnDelete text_box btn btn-primary">Delete</button></td></tr>');
        });
//field add
        $('.project_export').click(function() {
            if($(".export_poject tr").length <= 5)
            {
                $(".export_poject tr:last-child").add();
            }
            else
            {
                alert("You cannot add field  row");
            }
        });

    });
    //
    $("#projection_of_export").on('click','.btnDelete',function(){
        $(this).closest('tr').remove();

    });
</script>
@endsection




