<div class="form-group">
{!! Form::open(['url' => '/apps/store/'.Encryption::encodeId($form_data->app_id), 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'apps_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
{!! Form::hidden('nsi_status',1,['id'=>'nsi_status']) !!}
{!! Form::hidden('selected_file','',['id'=>'selected_file']) !!}
{!! Form::hidden('validateFieldName','',['id'=>'validateFieldName']) !!} 
 {!! Form::hidden('isRequired','',['id'=>'isRequired']) !!}
<span class="form-group col-md-3"><br/>
    {!! Form::select('status_id', [2=>'No Objection',3=>'Objection', 4=>'Black Listed'], '', $attributes = array('class'=>'form-control required','placeholder' => 'Select Report Status', 'id'=>"status_id")) !!}
</span>
<div class="form-group col-md-3 {{$errors->has('nsi_file') ? 'has-error' : ''}}"><br/>
{!! Form::file('nsi_file[]', ['id'=>'nsi_file','multiple'=>true]) !!}
<span style="font-size: 9px; color: grey">[File Type: pdf] </span>
 <?php if (!empty($verify_data->attachment)) { ?>
        <a href="<?php echo url(); ?>/uploads/<?php echo $verify_data->attachment; ?>" target="_blank">
            <?php echo $verify_data->attachment; ?>
            <img src="<?php echo url(); ?>/front-end/images/icon_download.gif" border="0" alt="download" title="<?php echo $verify_data->attachment; ?>"/>
        </a>
 <?php } ?>
</div>


<div class="form-group col-md-4">
    <?php
    $remarks = "";
    if(!empty($verify_data->remarks))
        $remarks = $verify_data->remarks;
    ?>
    {!! Form::textarea('nsi_remarks',$remarks,['class'=>'form-control required','id'=>'nsi_remarks', 'placeholder'=>'Enter Remarks','maxlength' => 1000, 'rows' => 1, 'cols' => 50]) !!}
    <small>(Maximum length 1000)</small>

    {!! $errors->first('nsi_remarks','<span class="help-block">:message</span>') !!}
</div>
<div class="box-footer"><br/>
    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'value'=> 'Submit', 'class' => 'btn btn-primary send')) !!}
</div><br/>
{!! Form::close() !!}
</div>
@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready( function () {
        $(".apps_from").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });
</script>
@endsection
