@extends('layouts.admin')

@section('content')

@inject('SbArea', 'App\Modules\Settings\Models\SbArea')
<?php
$accessMode = ACL::getAccsessRight('project_clearance');
if (!ACL::isAllowed($accessMode, 'V'))
    die('no access right!');

$memId = CommonFunction::getUserId();
$user_type = CommonFunction::getUserType();
$user_type_name = CommonFunction::getUserTypeName();

$user_desk = CommonFunction::getUserDeskName();
?>

{{--@if(!empty($delegated_desk))
    @section ('page_heading','<i class="fa fa-list"></i> Delegate Application List')
    @else
        @section ('page_heading','<i class="fa fa-list"></i> Application List')
    @endif--}}

<section class="content" xmlns="http://www.w3.org/1999/html">

    <div class="box">
        <div class="box-body">
            @include('partials.messages')
            <?php
            $desk_id = Auth::user()->desk_id;
            ?>

            @if(in_array(Auth::user()->desk_id,array(1,2,3,4,5,6,8)))
            {!! Form::open(['url' => '/apps/update-batch', 'method' => 'patch', 'class' => 'form apps_from', 'id' => 'batch_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
            @endif
			<div class="col-lg-12">
				<div class="with-border">
					@if(in_array(Auth::user()->desk_id,array(1,2,3,4,5,6,8)))
					@include('apps::batch-process')
					@endif
				</div>
			</div>


            @if(in_array($desk_id,array(9,10)))
            {!! Form::open(array('url' => 'apps/assign-desks','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'assign_form')) !!}

            <div class="panel panel-info">
                <div class="panel-heading">Assign Verifier</div>
                <div class="panel-body">
                    <div class="form-group" id="sectionForradio">
                        <label  class="col-lg-3 text-left"> Method </label>
                        <div class="col-lg-9">
                            <label class="radio-inline">
                                <input name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked="" type="radio">District
                            </label>
                            <label class="radio-inline">
                                <input name="optionsRadiosInline" id="optionsRadiosInline2" value="option2" type="radio">Section
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="mehtodOpt1">
                        <span class="col-md-3">
                            {!! Form::select('division', $value = $division, '', $attributes = array('class'=>'form-control','placeholder' => 'Select District', 'id'=>"division")) !!}
                        </span>
                        <span class="col-md-3">
                            {!! Form::select('district', $value = [], '', $attributes = array('class'=>'form-control','placeholder' => 'Select Thana', 'id'=>"district")) !!}
                        </span>

                    </div>
                    <div class="form-group " id="mehtodOpt2" style="display: none;">

                        <span class="col-md-3">
                            {!! Form::select('hq', $value = $hqs, '', $attributes = array('class'=>'form-control','id'=>"hq")) !!}
                        </span>
                        <span class="col-md-3">
                            {!! Form::select('section', $value = [], '', $attributes = array('class'=>'form-control','id'=>"section")) !!}
                        </span>
                        <span class="col-md-3">
                            {!! Form::select('team', $value = [], '', $attributes = array('class'=>'form-control','id'=>"team")) !!}
                        </span>
                    </div>

                    <div class="form-group">
                        <span class="col-md-3">
                            {!! Form::select('user_id', $value = [], '', $attributes = array('class'=>'form-control required','placeholder' => 'Select  Field officer', 'id'=>"user_id")) !!}
                        </span>
                        <span class="col-md-3"> {!! Form::button('<i class="fa fa-save"></i> Assign Verifier', array('type' => 'submit', 'name' => 'submit', 'value'=> 'Submit', 'class' => 'btn btn-primary send')) !!}
                        </span>
                    </div>
                </div>
            </div>

            <div class="panel panel-success completing_task" >
                <div class="panel-heading">Completing Task</div>
                <div class="panel-body">
                    <span class="col-md-3">
                        {!! Form::select('gk_status', $resultList, '', $attributes = array('class'=>'form-control','placeholder' => 'Select one', 'id'=>"gk_status")) !!}
                    </span>
                    <span class="col-md-6">
                        {!! Form::textarea('gk_remarks','',['class'=>'form-control','id'=>'gk_remarks', 'placeholder'=>'Enter Remarks','maxlength' => 254, 'rows' => 2, 'cols' => 50]) !!}
                        (Maximum length 254)
                        {!! $errors->first('gk_remarks','<span class="help-block">:message</span>') !!}
                    </span>
                    <span class="col-md-3"> {!! Form::button('<i class="fa fa-save"></i> Process', array('type' => 'submit', 'name' => 'submit', 'value'=> 'Process', 'class' => 'btn btn-primary process')) !!}
                    </span>

                    </span>
                </div>
            </div>
            @endif






            @if(empty($delegated_desk))
            <div class="modal fade" id="ProjectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" id="frmAddProject"></div>
                </div>
            </div>
            @endif
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        @if($user_type == 5  || $user_type == 6)
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="btn" style="color:#ffffff">
                                    @if(!empty($delegated_desk))
                                    <i class="fa fa-list"></i> Delegate Application List
                                    @else
                                    <i class="fa fa-list"></i> Application List
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 text-right">
                                <a class="btn btn-default addProjectModal" data-toggle="modal" data-target="#ProjectModal" href="{!! url('dashboard/search') !!}"
                                   onclick="resetElements()"><i class="fa fa-search"></i> Advance Search</a>

                                <a href="{{URL::to('apps/application-selection')}}" class="">
                                    {!! Form::button('<i class="fa fa-plus"></i> New Application', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                                </a>
                            </div>
                        </div>
                        @else
                        @if(empty($delegated_desk))
                        @if(ACL::getAccsessRight('project_clearance','SER'))
                        <a class="btn btn-default addProjectModal" data-toggle="modal" data-target="#ProjectModal" href="{!! url('dashboard/search') !!}"
                           onclick="resetElements()"><i class="fa fa-search"></i> Advance Search</a>
                        @endif
                        @else
                        &nbsp;
                        @endif
                        @endif

                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table id="_list" class="table table-striped resultTable display" role="grid">
                            <thead>
                                <tr>
                                    <th>
                                        @if (in_array($desk_id,array(1,2,3,4,5,8,9,10)))
                                        {!! Form::checkbox('chk_id','chk_id','',['class'=>'selectall', 'id'=>'chk_id']) !!}
                                        @endif
                                    </th>
                                    <th>#</th>
                                    <th>Tracking Id</th>
                                    <th>Applicant & Nationality</th>

                                    <th>Serving Desk</th>

                                    <th>Status</th>
                                    <th>Modified</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $row_sl = 0; ?>
                                @foreach($getList as $row)

                                <?php $row_sl++ ?>
                                <tr>
                                    <td>
                                        @if (in_array($desk_id,array(1,2,3,4,5,6,8,9,10)))
                                        {!! Form::checkbox('application[]',$row->record_id, '',['class'=>"appCheckBox chkbox_$row_sl", 'data-sl'=>"chkbox_$row_sl",'onChange' => 'changeStatus(this.checked)']) !!}
                                        {!! Form::hidden('hdn_batch[]',$row->status_id, ['class'=>'hdnStatus','id'=>"".$row->record_id."_status"]) !!}
                                        @endif
                                    </td>
                                    <td>{!! $row_sl !!}</td>
                                    <td>{!! $row->track_no !!} - <br/>{!! $row->siname !!}</td>
                                    <td>

                                        <input type="hidden" value="{{$row->service_id}}" id="chkbox_{{$row_sl}}" name="service_id[{{$row->record_id}}]">
                                        @if($row->service_id == 1)
                                        {!! $row->applicant_name !!} - {!! $row->nationality !!}
                                        @elseif($row->service_id == 2)
                                            {!! $row->vaname !!} - {!! $row->vanationality !!}
                                        @endif
                                    </td>
                                    @if (!in_array($desk_id, array( 4, 6, 7, 9, 10)))
                                    <td>
                                        @if($row->desk_name == '') Applicant @else {!! $row->desk_name !!} @endif
                                    </td>
                                    @else
                                    <td>
                                        @if($row->desk_name == '') Applicant @else {!! $row->desk_name !!} @endif
                                    </td>
                                    @endif

                                    <td>


                                        @if(!empty($row->status_name))
                                        <span style="background-color:<?php echo $row->color; ?>;color: #fff; font-weight: bold;" class="label btn-sm">
                                            {!! $row->status_name !!}
                                        </span>
                                        @else
                                        <span style="background-color:#FFE180;color: #000; font-weight: bold;" class="label btn-sm">
                                            Draft
                                        </span>
                                        @endif


                                        @if(in_array($desk_id,array(4,9,10)))
                                        <?php
                                        $status_id = CommonFunction::getResultStatus($row->record_id, $desk_id);
                                        if ($status_id > 0)
                                            echo "<br/><small><b>" . $resultList[$status_id] . " from Verifier</b></small>";
                                        ?>
                                        @endif

                                        @if((in_array(Auth::user()->desk_id, array(1,2))) || (!empty($delegated_desk) && ($desk_id == 1 || $desk_id == 2 || $desk_id == 8)))
                                        <span class="btn-toolbar">

                                            @if(Auth::user()->user_type != 5)

                                            @if($row->nsi_gk_verification_status == 0 && ($row->status_id == 9 || $row->status_id == 3))
                                            <span class="label label-warning">NSI</span>
                                            @endif
                                            @if($row->sb_gk_verification_status == 0 && ($row->status_id == 9 || $row->status_id == 3))
                                            <span class="label label-warning">SB</span>
                                            @endif
                                            @if($row->nsi_gk_verification_status == 2 && ($row->status_id == 9 || $row->status_id == 3))
                                            <span class="label label-success">NSI</span>
                                            @endif
                                            @if($row->sb_gk_verification_status == 2 && ($row->status_id == 9 || $row->status_id == 3))
                                            <span class="label label-success">SB</span>
                                            @endif
                                            @if($row->nsi_gk_verification_status == 3 && ($row->status_id == 9 || $row->status_id == 3))
                                            <span class="label label-danger">NSI</span>
                                            @endif
                                            @if($row->sb_gk_verification_status == 3 && ($row->status_id == 9 || $row->status_id == 3))
                                            <span class="label label-danger">SB</span>
                                            @endif
                                        </span>
                                        @endif

                                        @endif

                                    </td>
                                    <td>{!! CommonFunction::updatedOn($row->created_at) !!}</td>
                                    <td>

                                        @if( $row->status_id == 0 || $row->status_id == 5 )
                                        <a href="{{url('project-clearance/edit-form/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" ><i class="fa fa-folder-open-o"></i> Edit</a>
                                        @endif
                                        @if( $row->status_id != 0)
                                        <a href="{{url('project-clearance/view/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" ><i class="fa fa-folder-open-o"></i> View</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if ($user_type == 6 || $user_type == 7)
                        {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
            @if(in_array(Auth::user()->desk_id,array(1,2,8)))
            {!! Form::close() !!}
            @endif
            </section>

            @endsection

            @section('footer-script')
            @include('partials.datatable-scripts')
            <script language="javascript">

                        var numberOfCheckedBox = 0;
                        var curr_app_id = '';
                        function setCheckBox()
                        {
                        numberOfCheckedBox = 0;
                                var flag = 1;
                                var selectedWO = $("input[type=checkbox]").not(".selectall");
                                selectedWO.each(function() {
                                if (this.checked)
                                {
                                numberOfCheckedBox++;
                                }
                                else
                                {
                                flag = 0;
                                }
                                });
                                if (flag == 1)
                        {
                        $("#chk_id").checked = true;
                        }
                        else {
                        $("#chk_id").checked = false;
                        }

                        }

                function changeStatus(check)
                {
                setCheckBox();
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                }

                $(document).ready(function() {


                $('#division').addClass("required");
                        $('#district').addClass("required");
                        $('#user_id').addClass("required");
                        $('#hq').removeClass("required");
                        $('#section').removeClass("required");
                        $('#team').removeClass("required");
                        $(".radio-inline").change(function() {
                var selectedVal = "";
                        var selected = $(".radio-inline input[type='radio']:checked");
                        if (selected.length > 0) {
                selectedVal = selected.val();
                }
                if (selectedVal == 'option2'){
                $("#mehtodOpt2").show();
                        $("#mehtodOpt1").hide();
                        $('#division').removeClass("required");
                        $('#district').removeClass("required");
                        $('#user_id').removeClass("required");
                        $('#hq').addClass("required");
                        $('#section').addClass("required");
                        $('#team').addClass("required");
                        $('#user_id').addClass("required");
                }
                else{

                $("#mehtodOpt1").show();
                        $("#mehtodOpt2").hide();
                        $('#division').addClass("required");
                        $('#district').addClass("required");
                        $('#user_id').addClass("required");
                        $('#hq').removeClass("required");
                        $('#section').removeClass("required");
                        $('#team').removeClass("required");
                }
                });
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                {{--$('#status_id').attr('disabled', 'disabled'); --}}
                {{--$('#desk_id').attr('disabled', 'disabled'); --}}
                {{--$('#remarks').attr('disabled', 'disabled'); --}}

                $('#_list').DataTable({
                "paging": true,
                        "lengthChange": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "iDisplayLength": 50
                });
                        $("#assign_form").validate({
                errorPlacement: function() {
                return false;
                }
                });
                        $("#apps_from").validate({
                errorPlacement: function() {
                return false;
                }
                });
                        $("#batch_from").validate({
                errorPlacement: function() {
                return false;
                }
                });
                        $(".send").click(function() {
                {{--$('#division').addClass("required"); --}}
                {{--$('#district').addClass("required"); --}}
                {{--$('#user_id').addClass("required"); --}}
                $('#gk_status').removeClass("required");
                        var all_app_id = [];
                        $('.appCheckBox').each(function () {
                if ($(this).is(':checked')){
                all_app_id.push($(this).val());
                }
                });
                        if (all_app_id == '')
                {
                alert("Please select any application");
                        //                         $('#status_id').attr('disabled',true);
                        return false;
                }
                });
                        $(".process").click(function() {
                $('#division').removeClass("required");
                        $('#district').removeClass("required");
                        $('#user_id').removeClass("required");
                        $('#gk_status').addClass("required");
                        if (numberOfCheckedBox == 0)
                {
                alert('Select Application');
                        return false;
                }

                });
                        $('#district').attr("disabled", true);
                        $('#division').change(function() {
                var _token = $('input[name="_token"]').val();
                        var id = $(this).val();
                        $('#district').html('<option value="">Please wait</option>');
                        $.post('/apps/get-district', {id: id, _token: _token}, function(response) {

                        if (response.responseCode == 1) {
                        var option = '<option value="">Select Thana</option>';
                                if (response.data.length >= 1) {
                        $.each(response.data, function(id, value) {
                        option += '<option value="' + value.area_id + '">' + value.area_nm + '</option>';
                        });
                        }
                        $('#district').attr("disabled", false);
                                $('#district').html(option);
                                $('#district').trigger('change');
                                $('#user_id').attr("disabled", true);
                        } else {
                        $('#user_id').attr("disabled", true);
                                $('#district').html('<option value="">Select District</option>');
                        }
                        });
                });
                        $('#user_id').attr("disabled", true);
                        $("#district").on('change', function() {
                var _token = $('input[name="_token"]').val();
                        var division = $('#division').val();
                        var district = $(this).val();

                        $.post('/apps/ajax/load-user-list', {district: district, division: division, _token: _token}, function(response) {

                        if (response.responseCode == 1) {

                        var option = '';
                                option += '<option value="">Select One</option>';
                                $.each(response.data, function(id, value) {
                                option += '<option value="' + value.id + '">' + value.user_full_name + '</option>';
                                });
                                $('#user_id').attr("disabled", false);
                                $("#user_id").html(option);
                                $("#user_id").trigger("change");
                        } else {
                        $('#user_id').attr("disabled", true);
                                $('#user_id').html('Please wait');
                        }
                        });
                });
                        var base_checkbox = '.selectall';
                        $(base_checkbox).click(function() {
                if (this.checked) {
                $('.appCheckBox:checkbox').each(function() {
                this.checked = true;
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                });
                } else {
                $('.appCheckBox:checkbox').each(function() {
                this.checked = false;
                        //                            $('#status_id').attr('disabled',false);
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                });
                }
                setCheckBox();
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                });
                        $('.appCheckBox:checkbox').not(base_checkbox).click(function() {
                $(".selectall").prop("checked", false);
                });
                        var break_for_pending_verification = 0;
                        $(".applicable_status").click(function() {

                $("#status_id").trigger("click");
                        var all_app_id = [];
                        break_for_pending_verification = 0;
                        $('.appCheckBox').each(function () {
                if ($(this).is(':checked')){
                    var thisVal = $(this).val();
                    var slid = $(this).data('sl');
                     current_service_id=$('#'+slid).val();

                all_app_id.push($(this).val());
                }
                });
                        if (all_app_id == '')
                {
                alert("Please select any application");
                        //                         $('#status_id').attr('disabled',true);
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                        return false;
                } else{
                $('#status_id').attr('disabled', false);
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                        curr_app_id = all_app_id[0];
                        var curr_status_id = $("#" + curr_app_id + "_status").val();
                        $.ajaxSetup({async: false});
                        $.each(all_app_id, function(j, i) {
                        if (break_for_pending_verification == 1)
                        {
                        return false;
                        }

                        var tmp_curr_status = $("#" + i + "_status").val();
                                if (curr_status_id != tmp_curr_status)
                        {
                        alert('Please select application of same status...');
                                $('#status_id').attr('disabled', false);
                                $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                                return false;
                        }
                        else
                        {
                        var _token = $('input[name="_token"]').val();
                                var delegate = '{{ @$delegated_desk }}';
                                var state = false;
                            if ($(this).is(':checked')){

                            }
                            console.log(current_service_id);
                                $.post('/apps/ajax/load-status-list', {curr_status_id: curr_status_id, curr_app_id: curr_app_id, delegate: delegate,current_service_id: current_service_id, _token: _token}, function(response) {

                                if (response.responseCode == 1) {

                                var option = '';
                                        option += '<option selected="selected" value="">Select Below</option>';
                                        $.each(response.data, function(id, value) {
                                        option += '<option value="' + value.status_id + '">' + value.status_name + '</option>';
                                        });
                                {{--state = !state; --}}
                                {{--$("#status_id").prop("", state ? $("option").length : 1); --}}
                                $("#status_id").html(option);
                                        $("#status_id").trigger("change");
                                        $("#status_id").focus();
                                } else if (response.responseCode == 5){
                                alert('Without verification, application can not be processed');
                                        break_for_pending_verification = 1;
                                        option = '<option selected="selected" value="">Select Below</option>';
                                        $("#status_id").html(option);
                                        $("#status_id").trigger("change");
                                        return false;
                                } else {
                                $('#status_id').html('Please wait');
                                }
                                });
                        }
                        });
                        $.ajaxSetup({async: true});
                }
                });
                        $(document).on('change', '.status_id', function() {




                var object = $(".status_id");
                        var obj = $(object).parent().parent().parent();
                        var id = $(object).val();
                        var _token = $('input[name="_token"]').val();
                        var status_from = $('#status_from').val();
                        $('#sendToDeskOfficer').css('display', 'block');
                        if (id == 0) {
                obj.find('.param_id').html('<option value="">Select Below</option>');
                } else {
                //                                        	alert(status_from);
                //                                if(id == 5 || id == 8 || id == 9 || id == 14 || id == 2){

                            console.log(current_service_id);
                $.post('/apps/ajax/process', {id: id, curr_app_id: curr_app_id,current_service_id: current_service_id, status_from: status_from, _token: _token}, function(response) {
                console.log(response);
                        if (response.responseCode == 1) {

                var option = '';
                        option += '<option selected="selected" value="">Select Below</option>';
                        var countDesk = 0;
                        $.each(response.data, function(id, value) {
                        countDesk++;
                                option += '<option value="' + id + '">' + value + '</option>';
                        });
                        obj.find('#desk_id').html(option);
                        $('#desk_id').attr("disabled", false);
                        $('#remarks').attr("disabled", false);
                        if (countDesk == 0)
                {
                $('.dd_id').removeClass('required');
                        $('#sendToDeskOfficer').css('display', 'none');
                }
                if (response.status_to == 5 || response.status_to == 8 || response.status_to == 16
                        || response.status_to == 17 || response.status_to == 19 || response.status_to == 24
                        || response.status_to == 10){

                $('#remarks').addClass('required');
                        $('#remarks').attr("disabled", false);
                }
                else{
                $('#remarks').removeClass('required');
                }


                if (response.file_attach == 1)
                {
                $('#sendToFile').css('display', 'block');
                }
                else{
                $('#sendToFile').css('display', 'none');
                }


                }
                });
                        //       }
                }


                });
                });
                        function resetElements(){
                        $('#status_id').html('<option selected="selected" value="">Select Below</option>');
                                /*                    $('#status_id').prop('selectedIndex',0);
                                 //                    $('#status_id').attr('disabled','disabled');
                                 $('#status_id').prop('selectedIndex',0);
                                 $('#desk_id').attr('disabled','disabled');
                                 $('#remarks').attr('disabled','disabled'); */
                        }

<?php if (isset($search_status_id) && $search_status_id > 0) { ?>
                    var _token = $('input[name="_token"]').val();
                            $.ajax({
                            url: base_url + '/dashboard/search-result',
                                    type: 'post',
                                    data: {
                                    _token: _token,
                                            status_id:<?php echo $search_status_id; ?>,
                                    },
                                    dataType: 'json',
                                    success: function(response) {
                                    // success
                                    if (response.responseCode == 1) {
                                    $('table.resultTable tbody').html(response.data);
                                    } else {
                                    }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                    console.log(errorThrown);
                                    },
                                    beforeSend: function(xhr) {
                                    console.log('before send');
                                    },
                                    complete: function() {
                                    //completed
                                    }
                            });
<?php } ?>

                $("#hq").change(function () {
                $(this).after('<span class="loading_data">Loading...</span>');
                        var self = $(this);
                        var divisionId = $('#hq').val();
                        $("#loaderImg").html("<img style='margin-top: -15px;' src='<?php echo url(); ?>/public/assets/images/ajax-loader.gif' alt='loading' />");
                        $.ajax({
                        type: "GET",
                                url: "<?php echo url(); ?>/settings/sb-get-section-by-hq-id",
                                data: {
                                divisionId: divisionId
                                },
                                success: function (response) {
                                var option = '<option value="">Select One</option>';
                                        if (response.responseCode == 1) {
                                $.each(response.data, function (id, value) {
                                option += '<option value="' + id + '">' + value + '</option>';
                                });
                                }
                                $("#section").html(option);
                                        self.next().hide();
                                }
                        });
                });
                        $("#section").change(function () {
                $(this).after('<span class="loading_data">Loading...</span>');
                        var self = $(this);
                        var sectionId = $('#section').val();
                        $("#loaderImg").html("<img style='margin-top: -15px;' src='<?php echo url(); ?>/public/assets/images/ajax-loader.gif' alt='loading' />");
                        $.ajax({
                        type: "GET",
                                url: "<?php echo url(); ?>/settings/sb-get-team-by-section-id",
                                data: {
                                sectionId: sectionId
                                },
                                success: function (response) {
                                var option = '<option value="">Select One</option>';
                                        if (response.responseCode == 1) {
                                $.each(response.data, function (id, value) {
                                option += '<option value="' + id + '">' + value + '</option>';
                                });
                                }
                                $("#team").html(option);
                                        self.next().hide();
                                }
                        });
                        $("#team").change(function () {
                $('#district').removeClass("required");
                        $(this).after('<span class="loading_data">Loading...</span>');
                        var self = $(this);
                        var teamId = $('#team').val();
                        var sectionId = $('#section').val();
                        var _token = $('input[name="_token"]').val();
                        $("#loaderImg").html("<img style='margin-top: -15px;' src='<?php echo url(); ?>/public/assets/images/ajax-loader.gif' alt='loading' />");
                        $.ajax({
                        type: "POST",
                                url: "<?php echo url(); ?>/apps/ajax/load-user-list_by_team",
                                data: {
                                _token :_token,
                                        teamId: teamId,
                                        sectionId: sectionId
                                },
                                success: function (response) {
                                var option = '';
                                        option += '<option value="">Select One</option>';
                                        $.each(response.data, function(id, value) {
                                        option += '<option value="' + value.id + '">' + value.user_full_name + '</option>';
                                        });
                                        $('#user_id').attr("disabled", false);
                                        $("#user_id").html(option);
                                        $("#user_id").trigger("change");
                                        self.next().hide();
                                }
                        });
                });
                });
            </script>
            @endsection
