@extends('layouts.admin')

@section('content')
@include('partials.messages')

<section class="content">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">
                                <h4><b>Reference No: {{ $process_data->track_no  }}</b></h4>
                            </div>
                            <div class="col-md-3 pull-right">
                                {{--{!! link_to('apps/re-certificate/'. Encryption::encodeId($form_data->app_id),'Re-Certificate',['class' => 'btn btn-danger btn-sm pull-right','target'=>'_blank']) !!}--}}
                                {{-- link_to('apps/print/'. Encryption::encodeId($form_data->app_id),'Print',['class' => 'btn btn-info btn-sm pull-right','target'=>'_blank']) --}}

                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="content-header">
                        <ol class="breadcrumb">
                            <li><strong> Date of Submission: </strong> {{ \App\Libraries\CommonFunction::formateDate($process_data->created_at)  }} </li>
                            <li><strong>Current Status : </strong>
                                {!! $statusArray[$form_data->status_id] !!}
                            </li>
                            <li>
                                @if($process_data->desk_id != 0) <strong>Current Desk :</strong> {{ \App\Libraries\CommonFunction::getDeskName($process_data->desk_id)  }} @endif
                            </li>
                            <li>
                                <?php if ($form_data->status_id == 14 && !empty($form_data->certificate_file_name)) { ?>
                                <a href="../../{{ $form_data->certificate_file_name }}">Download Security Clearence Certificate</a>
                                <?php } ?>
                            </li>
                        </ol>
                    </section>
                    @if((count($avr_data) == 1) && (Auth::user()->desk_id == 6) && ($form_data->status_id == 9) && ($avr_data->sb_nsi_flag == 'sb') && ($avr_data->status_id == 0) && ($avr_data->user_id == Auth::user()->id))
                        <div class="alert alert-warning"> @include('apps::process-sbdesk')  </div>
                    @endif
                    @if((count($avr_data)== 1) && (Auth::user()->desk_id == 7) && ($form_data->status_id == 9) && ($avr_data->sb_nsi_flag == 'nsi') && ($avr_data->status_id == 0) && ($avr_data->user_id == Auth::user()->id))
                        <div class="alert alert-warning"> @include('apps::process-nsidesk')  </div>
                    @endif


                    @if($process_data->status_id == 21 && Auth::user()->id == $alreadyExistApplicant->created_by)
                        {!! Form::open(array('url' => 'project-clearance/challan-store/'.Encryption::encodeId($alreadyExistApplicant->id),'method' => 'post', 'files' => true, 'role'=>'form')) !!}
                        <div class="panel panel-info">
                            <div class="panel-heading">Challan related information</div>
                            <div class="panel-body">

                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                            {!! Form::label('Challan No','Challan No : ',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('challan_no', null,['class'=>'form-control bnEng required input-sm',
                                                'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                            {!! Form::label('bank_name','Bank Name ',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('bank_name',null, ['class'=>'form-control required input-sm','placeholder'=>'Bank Name',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                            {!! Form::label('amount','Amount ',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('amount',null, ['class'=>'form-control bnEng required input-sm','placeholder'=>'5000',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                            {!! Form::label('date','Date ',['class'=>'col-md-6 font-ok required-star']) !!}
                                            <div class="datepicker col-md-6  input-group date" data-date-format="yyyy-mm-dd">

                                                {!! Form::text('date', null, ['class'=>'form-control required', 'id' => 'user_DOB']) !!}
                                                <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                            {!! Form::label('branch','Branch Name ',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('branch',null, ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 {{$errors->has('challan_file') ? 'has-error' : ''}}">
                                            {!! Form::label('challan_file','Chalan copy',['class'=>'col-md-5 font-ok required-star']) !!}
                                            <div class="col-md-7">
                                                {!! Form::file('challan_file',null, ['class'=>'form-control bnEng required input-sm',
                                                'data-rule-maxlength'=>'40']) !!}
                                                {!! $errors->first('challan_file','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary pull-left next">
                                            <i class="fa fa-chevron-circle-right"></i> Save</button>
                                    </div>
                                </div>

                            </div>

                        {!! Form::close() !!}<!-- /.form end -->
                        </div> <!--End of Panel Group-->

                    @endif {{-- status_id == 21  and created by logged user --}}

                    <div class="panel panel-info"  id="ep_form">
                        @if(!empty($alreadyExistApplicant->challan_no))
                            <div class="panel panel-info">
                                <div class="panel-heading">Challan related information</div>
                                <div class="panel-body">

                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 {{$errors->has('challan_no') ? 'has-error' : ''}}">
                                                {!! Form::label('Challan No','Challan No : ',['class'=>'col-md-5 font-ok ']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('challan_no', (!empty($alreadyExistApplicant->challan_no) ? $alreadyExistApplicant->challan_no : ''),['class'=>'form-control bnEng required input-sm',
                                                    'placeholder'=>'110', 'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('challan_no','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>



                                            <div class="form-group col-md-12 {{$errors->has('bank_name') ? 'has-error' : ''}}">
                                                {!! Form::label('bank_name','Bank Name ',['class'=>'col-md-5 font-ok']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('bank_name',(!empty($alreadyExistApplicant->bank_name) ? $alreadyExistApplicant->bank_name : ''), ['class'=>'form-control required input-sm','placeholder'=>'Bank Name',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('bank_name','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 {{$errors->has('amount') ? 'has-error' : ''}}">
                                                {!! Form::label('amount','Amount ',['class'=>'col-md-5 font-ok']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('amount',(!empty($alreadyExistApplicant->challan_amount) ? $alreadyExistApplicant->challan_amount : ''), ['class'=>'form-control bnEng required input-sm','placeholder'=>'5000',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('amount','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 {{$errors->has('date') ? 'has-error' : ''}}">
                                                {!! Form::label('date','Date ',['class'=>'col-md-5 font-ok']) !!}
                                                <div class="col-md-7">

                                                    {!! Form::text('date',(!empty($alreadyExistApplicant->challan_date) ? $alreadyExistApplicant->challan_date : ''), ['class'=>'form-control required input-sm','placeholder'=>'Date',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                {!! Form::label('branch','Branch Name ',['class'=>'col-md-5 font-ok ']) !!}
                                                <div class="col-md-7">
                                                    {!! Form::text('branch',(!empty($alreadyExistApplicant->challan_branch) ? $alreadyExistApplicant->challan_branch : ''), ['class'=>'form-control required input-sm','placeholder'=>'Branch Name',
                                                    'data-rule-maxlength'=>'40']) !!}
                                                    {!! $errors->first('branch','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12 {{$errors->has('branch') ? 'has-error' : ''}}">
                                                {!! Form::label('branch','Challan Copy ',['class'=>'col-md-5 font-ok ']) !!}
                                                <div class="col-md-7">
                                                    <a href="{{url($alreadyExistApplicant->challan_file)}}" target="_blank"> Download</a>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-left next">
                                                <i class="fa fa-chevron-circle-right"></i> Save</button>
                                        </div>

                                    </div>


                                </div>

                            {!! Form::close() !!}<!-- /.form end -->
                            </div> <!--End of Panel Group-->
                        @endif {{-- application has challan --}}

                        @if (session('success'))
                            <div class="alert alert-success">
                                <strong><i class="icon-check"></i> {{session('success')}}.</strong>
                            </div>
                        @endif
                        <div class="panel-heading">Application for Project Clearance</div>
                        <div class="panel-body">

                            <input type="hidden" name="selected_file" id="selected_file" />

                            <input type="hidden" name="validateFieldName" id="validateFieldName" />
                            <input type="hidden" name="isRequired" id="isRequired" />
                            <h3 style="visibility: hidden;height: 0;margin: 0;">Applicant Information</h3>
                            <fieldset>
                                <legend>Applicant Information</legend>

                                <div class="form-group clearfix">
                                    <div class="col-lg-6 {{$errors->has('applicant_name') ? 'has-error': ''}}">
                                        {!! Form::label('applicant_name','1. Name of an Applicant / Applying Firm or Company :',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('applicant_name',(!empty($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : ''),
                                        ['data-rule-maxlength'=>'64',
                                        'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('applicant_name','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('infrastructureReq','2. Full Address of Registered Head Office of Applicant / Applying Firm or Company:',
                                    ['class'=>'text-left text-title']) !!}
                                </div>

                                <div class="form-group" style="clear: both">

                                    <div class="col-md-6">
                                        {!! Form::label('road_no','Address Line 1 ',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('road_no',(!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : ''), ['data-rule-maxlength'=>'80',
                                        'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class="form-group" style="clear: both">
                                    <div class="col-lg-6 {{$errors->has('house_no') ? 'has-error': ''}}">
                                        {!! Form::label('house_no','Address Line 2 ', ['class'=>'text-left required-star']) !!}
                                        {!! Form::text('house_no',(!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : ''), ['data-rule-maxlength'=>'80',
                                        'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('post_code','Post Code ',['class'=>'text-left']) !!}
                                        {!! Form::text('post_code',(!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : ''), ['data-rule-maxlength'=>'20',
                                        'class' => 'form-control input-sm number']) !!}
                                        {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class="form-group" style="clear: both">
                                    <div class="col-lg-6 {{$errors->has('phone') ? 'has-error': ''}}">
                                        {!! Form::label('phone','Phone No',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('phone',(!empty($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : ''), ['data-rule-maxlength'=>'20', 'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('phone','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('fax','Fax No ',['class'=>'text-left']) !!}
                                        {!! Form::text('fax',(!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : ''), ['data-rule-maxlength'=>'20',
                                        'class' => 'form-control input-sm number']) !!}
                                        {!! $errors->first('fax','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class="form-group" style="clear: both">
                                    <div class="col-lg-6 {{$errors->has('email') ? 'has-error': ''}}">
                                        {!! Form::label('email','Email',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('email',(!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : ''), ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('website','Website ',['class'=>'text-left']) !!}
                                        {!! Form::text('website',(!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : ''), ['data-rule-maxlength'=>'100',
                                        'class' => 'form-control input-sm url']) !!}
                                        {!! $errors->first('website','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="col-lg-6 {{$errors->has('applicant_name') ? 'has-error': ''}}">
                                        {!! Form::label('applicant_name','1. Name of an Applicant / Applying Firm or Company :',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('applicant_name',(!empty($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : ''),
                                        ['data-rule-maxlength'=>'64',
                                        'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('applicant_name','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('infrastructureReq','2. Full Address of Registered Head Office of Applicant / Applying Firm or Company:',
                                    ['class'=>'text-left text-title']) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::label('road_no','Address Line 1 ',['class'=>'text-left required-star']) !!}
                                    {!! Form::text('road_no',(!empty($alreadyExistApplicant->road_no) ? $alreadyExistApplicant->road_no : ''), ['data-rule-maxlength'=>'80',
                                    'class' => 'form-control input-sm required']) !!}
                                    {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                                    <p class="text-danger pss-error"></p>
                                </div>

                                <div class="form-group" style="clear: both">
                                    <div class="col-lg-6 {{$errors->has('house_no') ? 'has-error': ''}}">
                                        {!! Form::label('house_no','Address Line 2 ', ['class'=>'text-left required-star']) !!}
                                        {!! Form::text('house_no',(!empty($alreadyExistApplicant->house_no) ? $alreadyExistApplicant->house_no : ''), ['data-rule-maxlength'=>'80',
                                        'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('post_code','Post Code ',['class'=>'text-left']) !!}
                                        {!! Form::text('post_code',(!empty($alreadyExistApplicant->post_code) ? $alreadyExistApplicant->post_code : ''), ['data-rule-maxlength'=>'20',
                                        'class' => 'form-control input-sm number']) !!}
                                        {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class="form-group" style="clear: both">
                                    <div class="col-lg-6 {{$errors->has('phone') ? 'has-error': ''}}">
                                        {!! Form::label('phone','Phone No',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('phone',(!empty($alreadyExistApplicant->phone) ? $alreadyExistApplicant->phone : ''), ['data-rule-maxlength'=>'20', 'class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('phone','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('fax','Fax No ',['class'=>'text-left']) !!}
                                        {!! Form::text('fax',(!empty($alreadyExistApplicant->fax) ? $alreadyExistApplicant->fax : ''), ['data-rule-maxlength'=>'20',
                                        'class' => 'form-control input-sm number']) !!}
                                        {!! $errors->first('fax','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class="form-group" style="clear: both">
                                    <div class="col-lg-6 {{$errors->has('email') ? 'has-error': ''}}">
                                        {!! Form::label('email','Email',['class'=>'text-left required-star']) !!}
                                        {!! Form::text('email',(!empty($alreadyExistApplicant->email) ? $alreadyExistApplicant->email : ''), ['data-rule-maxlength'=>'64','class' => 'form-control input-sm required email']) !!}
                                        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::label('website','Website ',['class'=>'text-left']) !!}
                                        {!! Form::text('website',(!empty($alreadyExistApplicant->website) ? $alreadyExistApplicant->website : ''), ['data-rule-maxlength'=>'100',
                                        'class' => 'form-control input-sm url']) !!}
                                        {!! $errors->first('website','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                            </fieldset>

                            <h3 style="visibility: hidden;height: 0;margin: 0;">Proposed Project Information (Part A)</h3>
                            <fieldset>
                                <legend>Proposed Project Information (Part A)</legend>

                                <div class="form-group clearfix">


                                    <div class="form-group clearfix">
                                        <div class="col-lg-6 {{$errors->has('proposed_name') ? 'has-error': ''}}">
                                            {!! Form::label('proposed_name','Proposed Project / Company Name which will carry out of the Business',
                                            ['class'=>'text-left required-star font-size-12']) !!}
                                            {!! Form::text('proposed_name',(!empty($alreadyExistApplicant->proposed_name) ? $alreadyExistApplicant->proposed_name : ''), ['data-rule-maxlength'=>'100',
                                            'class' => 'form-control input-sm required']) !!}
                                            {!! $errors->first('proposed_name','<span class="help-block">:message</span>') !!}
                                            <p class="text-danger pss-error"></p>
                                        </div>
                                    </div>

                                    <div class="form-group" style="clear:both">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="type_of_organizations" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                    <thead class="alert alert-success">
                                                    <tr>
                                                        <th>Company Name <span class="required-star"></span></th>
                                                        <th>Company Address <span class="required-star"></span></th>
                                                        <th>Country <span class="required-star"></span></th>
                                                        <th>#</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            {!! Form::text('joint_company[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('joint_company','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('joint_company_address[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('joint_company_address[]','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>

                                                        <td><span class="add_new_span">-</span></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" style="clear: both">
                                        <div class="col-lg-12">Proposed Factory Construction Schedule:<hr></div>
                                    </div>

                                    <div class="form-group" style="clear:both">
                                        <div class="col-lg-4 {{$errors->has('construction_start') ? 'has-error': ''}}">
                                            <div class="construction_start input-group date col-md-12">
                                                {!! Form::label('construction_start','Start Time ',['class'=>'text-lef required-start']) !!}
                                                {!! Form::text('construction_start',  (!empty($alreadyExistApplicant->construction_start) ? $alreadyExistApplicant->construction_start : ''), ['class' => 'form-control  input-sm required']) !!}
                                                {!! $errors->first('construction_start','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-4 {{$errors->has('construction_end') ? 'has-error': ''}}">
                                            <div class="construction_end input-group date col-md-12">
                                                {!! Form::label('construction_end','End Time ',['class'=>'text-left required-star']) !!}
                                                {!! Form::text('construction_end',  (!empty($alreadyExistApplicant->construction_end) ? $alreadyExistApplicant->construction_end : ''), ['class' => 'form-control  input-sm required']) !!}
                                                {!! $errors->first('construction_end','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-4 {{$errors->has('construction_duration') ? 'has-error': ''}}">
                                            {!! Form::label('construction_duration','Duration ',['class'=>'text-left required-star']) !!}
                                            {!! Form::text('construction_duration',(!empty($alreadyExistApplicant->construction_duration) ? $alreadyExistApplicant->construction_duration : ''), ['data-rule-maxlength'=>'100',
                                            'class' => 'form-control input-sm required','readonly'=>true]) !!}
                                            {!! $errors->first('construction_duration','<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group" style="clear: both">
                                        <br>
                                        <div class="col-lg-12">Initial Investment Amount to carry out the Business:<hr></div>
                                    </div>

                                    <div class="form-group" style="clear:both">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                    <thead class="alert alert-info">
                                                    <tr>
                                                        <th>Capital Structure <span class="required-star"></span></th>
                                                        <th>Foreign <span class="required-star"></span></th>
                                                        <th>Local Citizen <span class="required-star"></span></th>
                                                        <th>Total <span class="required-star"></span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <th>Authorized Capital</th>
                                                        <td>
                                                            {!! Form::text('auth_capital_fo',(!empty($alreadyExistApplicant->auth_capital_fo) ? $alreadyExistApplicant->auth_capital_fo : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'authCapitalFo()']) !!}
                                                            {!! $errors->first('auth_capital_fo','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('auth_capital_lo',(!empty($alreadyExistApplicant->auth_capital_lo) ? $alreadyExistApplicant->auth_capital_lo : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number','onkeypress'=>'validate_number(event)','onblur'=>'authCapitalLo()']) !!}
                                                            {!! $errors->first('auth_capital_lo','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('auth_capital_to',(!empty($alreadyExistApplicant->auth_capital_to) ? $alreadyExistApplicant->auth_capital_to : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number','readonly'=>true, 'onkeypress'=>'validate_number(event)']) !!}
                                                            {!! $errors->first('auth_capital_to','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Paid-up Capital (%)</th>
                                                        <td>
                                                            {!! Form::text('paid_capital_fo',(!empty($alreadyExistApplicant->paid_capital_fo) ? $alreadyExistApplicant->paid_capital_fo : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'paidCapitalFo()']) !!}
                                                            {!! $errors->first('paid_capital_fo','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('paid_capital_lo',(!empty($alreadyExistApplicant->paid_capital_lo) ? $alreadyExistApplicant->paid_capital_lo : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'paidCapitalLo()']) !!}
                                                            {!! $errors->first('paid_capital_lo','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('paid_capital_to',(!empty($alreadyExistApplicant->paid_capital_to) ? $alreadyExistApplicant->paid_capital_to : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number','readonly'=>true, 'onkeypress'=>'validate_number(event)']) !!}
                                                            {!! $errors->first('paid_capital_to','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>External Borrowing</th>
                                                        <td>
                                                            {!! Form::text('ext_borrow_fo',(!empty($alreadyExistApplicant->ext_borrow_fo) ? $alreadyExistApplicant->ext_borrow_fo : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'extBorrowFo()']) !!}
                                                            {!! $errors->first('ext_borrow_fo','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('ext_borrow_lo',(!empty($alreadyExistApplicant->ext_borrow_lo) ? $alreadyExistApplicant->ext_borrow_lo : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number', 'onkeypress'=>'validate_number(event)','onblur'=>'extBorrowLo()']) !!}
                                                            {!! $errors->first('ext_borrow_lo','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('ext_borrow_to',(!empty($alreadyExistApplicant->ext_borrow_to) ? $alreadyExistApplicant->ext_borrow_to : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required number','readonly'=>true, 'onkeypress'=>'validate_number(event)']) !!}
                                                            {!! $errors->first('ext_borrow_to','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" style="clear: both">
                                        <div class="col-lg-12">Contribution in Paid-up Capital Among Major Shareholders:<hr></div>
                                    </div>

                                    <div class="form-group" style="clear:both">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                    <thead class="alert alert-warning">
                                                    <tr>
                                                        <th>Name of Shareholder <span class="required-star"></span></th>
                                                        <th>Contribution Amount  <span class="required-star"></span></th>
                                                        <th>Foreign/Citizen  <span class="required-star"></span></th>
                                                        <th>Share (%)  <span class="required-star"></span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <th>Authorized Capital</th>
                                                        <td>
                                                            {!! Form::text('auth_cap_amount',(!empty($alreadyExistApplicant->auth_cap_amount) ? $alreadyExistApplicant->auth_cap_amount : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('auth_cap_amount','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('auth_cap_nature',(!empty($alreadyExistApplicant->auth_cap_nature) ? $alreadyExistApplicant->auth_cap_nature : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('auth_cap_nature','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('auth_cap_percentage',(!empty($alreadyExistApplicant->auth_cap_percentage) ? $alreadyExistApplicant->auth_cap_percentage : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('auth_cap_percentage','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Paid-up Capital (%)</th>
                                                        <td>
                                                            {!! Form::text('paid_cap_amount',(!empty($alreadyExistApplicant->paid_cap_amount) ? $alreadyExistApplicant->paid_cap_amount : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('paid_cap_amount','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('paid_cap_nature',(!empty($alreadyExistApplicant->paid_cap_nature) ? $alreadyExistApplicant->paid_cap_nature : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('paid_cap_nature','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('paid_cap_percentage',(!empty($alreadyExistApplicant->paid_cap_percentage) ? $alreadyExistApplicant->paid_cap_percentage : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('paid_cap_percentage','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>External Borrowing (%)</th>
                                                        <td>
                                                            {!! Form::text('ext_borrow_amount',(!empty($alreadyExistApplicant->ext_borrow_amount) ? $alreadyExistApplicant->ext_borrow_amount : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('ext_borrow_amount','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('ext_borrow_nature',(!empty($alreadyExistApplicant->ext_borrow_nature) ? $alreadyExistApplicant->ext_borrow_nature : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('ext_borrow_nature','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('ext_borrow_percentage',(!empty($alreadyExistApplicant->ext_borrow_percentage) ? $alreadyExistApplicant->ext_borrow_percentage : ''), ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('ext_borrow_percentage','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" style="clear: both">
                                        <div class="col-lg-12">Particular's of Sponsors / Directors:<hr></div>
                                    </div>

                                    <div class="form-group" style="clear:both">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="directors_list" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                                    <thead class="alert alert-success">
                                                    <tr class="text-center">
                                                        <th>Name</th>
                                                        <th>Address</th>
                                                        <th>Nationality</th>
                                                        <th>Status in the proposed company</th>
                                                        <th>Extent of share Holding</th>
                                                        <th> # </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            {!! Form::text('sponsor_name[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('sponsor_name','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('sponsor_address[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('sponsor_address','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            {!! Form::text('sponsor_nationality[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('sponsor_nationality','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('sponsor_status[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('sponsor_status','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>{!! Form::text('sponsor_share_ext[]','', ['data-rule-maxlength'=>'100',
                                                            'class' => 'form-control input-sm required']) !!}
                                                            {!! $errors->first('sponsor_share_ext','<span class="help-block">:message</span>') !!}
                                                            <p class="text-danger pss-error"></p>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-xs btn-primary addSponsorRows" onclick="addSponsorRows(this.form);"><i class="fa fa-plus"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                            </fieldset>

                            <h3 style="visibility: hidden;height: 0;margin: 0;">Proposed Project Information (Part B)</h3>
                            <fieldset>
                                <div class=" col-md-12 ">
                                    {!! Form::label('products',' 1. Products:', ['class'=>'text-left text-title']) !!}
                                </div>

                                <div class=" col-md-12 {{$errors->has('product_name') ? 'has-error': ''}}">
                                    {!! Form::label('product_name','a) Name / description of the product(s):' ,
                                    ['class'=>'col-md-5 text-left required-star']) !!}
                                    <div class="col-md-7">
                                        {!! Form::text('product_name', (!empty($alreadyExistApplicant->product_name) ? $alreadyExistApplicant->product_name : ''), ['class' => 'form-control input-sm required']) !!}
                                        {!! $errors->first('product_name','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class=" col-md-12 {{$errors->has('product_usage') ? 'has-error': ''}}">
                                    {!! Form::label('product_usage', 'b) Usage of the product(s):' ,
                                    ['class'=>'col-md-5 text-left required-star']) !!}
                                    <div class="col-md-7">
                                        {!! Form::text('product_usage', (!empty($alreadyExistApplicant->product_usage) ? $alreadyExistApplicant->product_usage : ''), ['class' => 'form-control input-sm required required-star']) !!}
                                        {!! $errors->first('product_usage','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                    </div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('productionPrg',' 2. Production Programme:', ['class'=>'text-left text-title']) !!}
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table id="productionPrgTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead class="alert alert-success">
                                            <tr class="text-center">
                                                <th class="text-center"> <span class="required-star"></span>Description</th>
                                                <th class="text-center">1st Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">2nd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">3rd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">4th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">5th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center"> # </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {!! Form::text('production_desc[]',(!empty($alreadyExistApplicant->applicant_name) ? $alreadyExistApplicant->applicant_name : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('production_desc','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('production_1st[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('production_1st','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('production_2nd[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('production_2nd','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('production_3rd[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('production_3rd','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('production_4th[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('production_4th','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('production_5th[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('production_5th','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    <a class="btn btn-xs btn-primary addProductionRows" onclick="addProductionRows(this.form);">
                                                        <i class="fa fa-plus"></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('proExport','3. Projection of Export:', ['class'=>'text-left text-title']) !!}
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table id="proExportTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead class="alert alert-info">
                                            <tr>
                                                <th class="text-center">Description <span class="required-star"></span></th>
                                                <th class="text-center">1st Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">2nd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">3rd Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">4th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center">5th Year  <span class="required-star"></span><br/>Qty - Value</th>
                                                <th class="text-center"> # </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {!! Form::text('pro_ext_desc[]','', ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('pro_ext_desc','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('pro_ext_1st[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('pro_ext_1st','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('pro_ext_2nd[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('pro_ext_2nd','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('pro_ext_3rd[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('pro_ext_3rd','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('pro_ext_4th[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('pro_ext_4th','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('pro_ext_5th[]','', ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('pro_ext_5th','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    <a class="btn btn-xs btn-primary addProExportRows" onclick="addProExportRows(this.form);">
                                                        <i class="fa fa-plus"></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('manpowerReq','4. Manpower requirements:', ['class'=>'text-left text-title']) !!}
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table id="mpReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead class="alert alert-warning">
                                            <tr>
                                                <th>Year <span class="required-star"></span></th>
                                                <th>Job  <span class="required-star"></span></th>
                                                <th>Foreign  <span class="required-star"></span></th>
                                                <th>Local  <span class="required-star"></span></th>
                                                <th>Total  <span class="required-star"></span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {!! Form::text('mp_year_1',(!empty($alreadyExistApplicant->mp_year_1) ? $alreadyExistApplicant->mp_year_1 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_year_1','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_job_1',(!empty($alreadyExistApplicant->mp_job_1) ? $alreadyExistApplicant->mp_job_1 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_job_1','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_foreign_1',(!empty($alreadyExistApplicant->mp_foreign_1) ? $alreadyExistApplicant->mp_foreign_1 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_foreign_1','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('mp_local_1',(!empty($alreadyExistApplicant->mp_local_1) ? $alreadyExistApplicant->mp_local_1 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_local_1','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_total_1',(!empty($alreadyExistApplicant->mp_total_1) ? $alreadyExistApplicant->mp_total_1 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_total_1','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::text('mp_year_2',(!empty($alreadyExistApplicant->mp_year_2) ? $alreadyExistApplicant->mp_year_2 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_year_2','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_job_2',(!empty($alreadyExistApplicant->mp_job_2) ? $alreadyExistApplicant->mp_job_2 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_job_2','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_foreign_2',(!empty($alreadyExistApplicant->mp_foreign_2) ? $alreadyExistApplicant->mp_foreign_2 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_foreign_2','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('mp_local_2',(!empty($alreadyExistApplicant->mp_local_2) ? $alreadyExistApplicant->mp_local_2 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_local_2','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_total_2',(!empty($alreadyExistApplicant->mp_total_2) ? $alreadyExistApplicant->mp_total_2 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_total_2','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::text('mp_year_3',(!empty($alreadyExistApplicant->mp_year_3) ? $alreadyExistApplicant->mp_year_3 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_year_3','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_job_3',(!empty($alreadyExistApplicant->mp_job_3) ? $alreadyExistApplicant->mp_job_3 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_job_3','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_foreign_3',(!empty($alreadyExistApplicant->mp_foreign_3) ? $alreadyExistApplicant->mp_foreign_3 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_foreign_3','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('mp_local_3',(!empty($alreadyExistApplicant->mp_local_3) ? $alreadyExistApplicant->mp_local_3 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_local_3','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_total_3',(!empty($alreadyExistApplicant->mp_total_3) ? $alreadyExistApplicant->mp_total_3 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_total_3','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::text('mp_year_4',(!empty($alreadyExistApplicant->mp_year_4) ? $alreadyExistApplicant->mp_year_4 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_year_4','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_job_4',(!empty($alreadyExistApplicant->mp_job_4) ? $alreadyExistApplicant->mp_job_4 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_job_4','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_foreign_4',(!empty($alreadyExistApplicant->mp_foreign_4) ? $alreadyExistApplicant->mp_foreign_4 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_foreign_4','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('mp_local_4',(!empty($alreadyExistApplicant->mp_local_4) ? $alreadyExistApplicant->mp_local_4 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_local_4','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_total_4',(!empty($alreadyExistApplicant->mp_total_4) ? $alreadyExistApplicant->mp_total_4 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_total_4','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::text('mp_year_5',(!empty($alreadyExistApplicant->mp_year_5) ? $alreadyExistApplicant->mp_year_5 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_year_5','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_job_5',(!empty($alreadyExistApplicant->mp_job_5) ? $alreadyExistApplicant->mp_job_5 : ''), ['data-rule-maxlength'=>'100','class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_job_5','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_foreign_5',(!empty($alreadyExistApplicant->mp_foreign_5) ? $alreadyExistApplicant->mp_foreign_5 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_foreign_5','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>{!! Form::text('mp_local_5',(!empty($alreadyExistApplicant->mp_local_5) ? $alreadyExistApplicant->mp_local_5 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_local_5','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('mp_total_5',(!empty($alreadyExistApplicant->mp_total_5) ? $alreadyExistApplicant->mp_total_5 : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('mp_total_5','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('productionSalesCost','5. Cost of Production and Sales Revenue (at 100% capacity):', ['class'=>'col-md-7 text-left text-title']) !!}
                                    <div class="col-md-1 text-right">US$</div>
                                    <div class="col-md-3">
                                        {!! Form::text('production_cost', (!empty($alreadyExistApplicant->production_cost) ? $alreadyExistApplicant->production_cost : ''), ['class' => 'form-control input-sm required required-star']) !!}
                                        {!! $errors->first('production_cost','<span class="help-block">:message</span>') !!}
                                        <p class="text-danger pss-error"></p>
                                        <span class="text-muted text-sm"> Cost per unit / Total cost </span>
                                    </div>
                                </div>

                                <div class="col-md-12 ">
                                    {!! Form::label('productionCost','5.1. Cost of Production:', ['class'=>'col-md-7 text-left']) !!}
                                    <div class="col-md-12 ">
                                        {!! Form::label('rawMaterials','i) Raw Materials: ', ['class'=>'col-md-4 text-left']) !!}
                                        <div class="col-md-12 ">
                                            {!! Form::label('rawCostBd','a) From Bangladesh:', ['class'=>'col-md-4 text-left required-star']) !!}
                                            <div class="col-md-6">
                                                {!! Form::text('raw_cost_bd', (!empty($alreadyExistApplicant->raw_cost_bd) ? $alreadyExistApplicant->raw_cost_bd : ''), ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('raw_cost_bd','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 ">
                                            {!! Form::label('rawCostOther','b) From other countries:', ['class'=>'col-md-4 text-left required-star']) !!}
                                            <div class="col-md-6">
                                                {!! Form::text('raw_cost_other', (!empty($alreadyExistApplicant->raw_cost_other) ? $alreadyExistApplicant->raw_cost_other : ''), ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('raw_cost_other','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 ">
                                        {!! Form::label('packMaterials','ii) Packaging Materials: ', ['class'=>'col-md-4 text-left']) !!}
                                        <div class="col-md-12 ">
                                            {!! Form::label('packCostBd','a) From Bangladesh:', ['class'=>'col-md-4 text-left required-star']) !!}
                                            <div class="col-md-6">
                                                {!! Form::text('pac_cost_bd', (!empty($alreadyExistApplicant->pac_cost_bd) ? $alreadyExistApplicant->pac_cost_bd : ''), ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pac_cost_bd','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 ">
                                            {!! Form::label('pacCostOther','b) From other countries:', ['class'=>'col-md-4 text-left required-star']) !!}
                                            <div class="col-md-6">
                                                {!! Form::text('pac_cost_other', (!empty($alreadyExistApplicant->pac_cost_other) ? $alreadyExistApplicant->pac_cost_other : ''), ['class' => 'form-control input-sm required']) !!}
                                                {!! $errors->first('pac_cost_other','<span class="help-block">:message</span>') !!}
                                                <p class="text-danger pss-error"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class=" col-md-12 ">
                                    {!! Form::label('infrastructureReq','6. Required Infrastructure:', ['class'=>'text-left text-title']) !!}
                                </div>

                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table id="infraReqTbl" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                            <thead class="alert alert-success">
                                            <tr>
                                                <th>Infrastructure </th>
                                                <th>Initial Period (per day) <span class="required-star"></span></th>
                                                <th>Regular Operation Period (per day) <span class="required-star"></span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {!! Form::label('land','Land (in ACR)', ['class' => 'text-left']) !!}
                                                </td>
                                                <td>{!! Form::text('land_ini',(!empty($alreadyExistApplicant->land_ini) ? $alreadyExistApplicant->land_ini : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('land_ini','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('land_reg',(!empty($alreadyExistApplicant->land_reg) ? $alreadyExistApplicant->land_reg : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('land_reg','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::label('power','Power (in KW/H)', ['class' => 'text-left']) !!}
                                                </td>
                                                <td>{!! Form::text('power_ini',(!empty($alreadyExistApplicant->power_ini) ? $alreadyExistApplicant->power_ini : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('power_ini','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('power_reg',(!empty($alreadyExistApplicant->power_reg) ? $alreadyExistApplicant->power_reg : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('power_reg','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::label('gas','GAS  (in CM)', ['class' => 'text-left']) !!}
                                                </td>
                                                <td>{!! Form::text('gas_ini',(!empty($alreadyExistApplicant->gas_ini) ? $alreadyExistApplicant->gas_ini : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('gas_ini','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('gas_reg',(!empty($alreadyExistApplicant->gas_reg) ? $alreadyExistApplicant->gas_reg : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('gas_reg','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! Form::label('water ','Water   (in CM)', ['class' => 'text-left']) !!}
                                                </td>
                                                <td>{!! Form::text('water_ini',(!empty($alreadyExistApplicant->water_ini) ? $alreadyExistApplicant->water_ini : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('water_ini','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                                <td>
                                                    {!! Form::text('water_reg',(!empty($alreadyExistApplicant->water_reg) ? $alreadyExistApplicant->water_reg : ''), ['data-rule-maxlength'=>'100',
                                                    'class' => 'form-control input-sm required']) !!}
                                                    {!! $errors->first('water_reg','<span class="help-block">:message</span>') !!}
                                                    <p class="text-danger pss-error"></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Required  Document s for attachment</div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover ">
                                                        <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th colspan="6">Required Attachments</th>
                                                            <th colspan="2">Attached PDF file <spna onmouseover="toolTipFunction()" data-toggle="tooltip" title="Attached PDF file (Each File Maximum size 1MB)!"><i class="fa fa-question-circle" aria-hidden="true"></i></spna></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i = 1; ?>
                                                        @foreach($document as $row)
                                                            <tr>
                                                                <td><div align="center">{!! $i !!}<?php echo $row->doc_priority == "1" ? "<span style='color:red'>*</span>" : ""; ?></div></td>
                                                                <td colspan="6">{!!  $row->doc_name !!}</td>
                                                                <td colspan="2">
                                                                    <input type="hidden" value="{!!  $row->doc_name !!}" id="doc_name_<?php echo $row->doc_id; ?>" name="doc_name_<?php echo $row->doc_id; ?>" />
                                                                    <input name="file<?php echo $row->doc_id; ?>" <?php /* echo $row->doc_priority == "1" ? "class='required'" : ""; */ ?> id="file<?php echo $row->doc_id; ?>" type="file" size="20" onchange="uploadDocument('preview_<?php echo $row->doc_id; ?>', this.id, 'validate_field_<?php echo $row->doc_id; ?>', <?php echo $row->doc_priority; ?>)"/>
                                                                    <div id="preview_<?php echo $row->doc_id; ?>">
                                                                        <input type="hidden" <?php echo $row->doc_priority == "1" ? "class='required'" : ""; ?> value="" id="validate_field_<?php echo $row->doc_id; ?>" name="validate_field_<?php echo $row->doc_id; ?>" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php $i++; ?>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Terms and Conditions</legend>

                                <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms-2">
                                    I agree with the Terms and Conditions.</label>

                            </fieldset>
                        {!! Form::close() !!}<!-- /.form end -->
                        </div>

                        <div class="panel panel-success">
                            <div class="panel-heading">Process path history</div>
                            <div class="panel-body">

                                <table class="table table-striped table-condensed" role="grid">
                                    <thead>
                                    <tr>
                                        <th class="text-center">On Desk</th>
                                        <th>Updated By</th>
                                        <th>Status</th>
                                        <th>Process Time</th>
                                        <th>Remark</th>
                                        <th>File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sl = 0; ?>
                                    @forelse($process_history as $history)
                                        <?php $sl++; ?>
                                        <tr>
                                            <td class="text-center">{{$history->deskname}}</td>

                                            <td>{{$history->user_full_name}}</td>

                                            <td>{{$history->status_name}}</td>

                                            <td>{{$history->created_at}}</td>

                                            <td>{{$history->process_desc}}</td>

                                            <td> @if($history->files != '')

                                                    <a class="btn btn-primary download" data="{{$sl}}"><i class="fa fa-save"></i> Download</a>

                                                @endif
                                            </td>

                                        </tr>
                                        <tr style="display: none;" class="show_{{$sl}}"><td colspan="6">
                                                <?php
                                                $file = explode(",", $history->files);
                                                $sl2 = 0;
                                                ?>
                                                @foreach($file as $value)
                                                    <a href="{{url($value)}}" target="_blank"> File {{++$sl2}}</a>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>No result found!</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div><!--/panel-body-->
                </div><!--/panel-->
            </div><!--/box-->
        </div><!--/box-body-->
        </div>
    </section>

@endsection {{-- content --}}

@section('footer-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ep_form').find('input:not([type=checkbox], [type=hidden]), button').each(function () {
                $(this).replaceWith("<span><b>" + this.value + "</b></span>");
            });
            $('#ep_form').find('textarea').each(function () {
                $(this).replaceWith("<span class=\"span3\"><b>" + this.value + "</b></span>");
            });
            $("#ep_form").find('select').replaceWith(function () {
                var selectedText = $(this).find('option:selected').text();
                var selectedTextBold = "<b>" + selectedText + "</b>";
                return selectedTextBold;
            });
            $("fieldset").css({"display": "block"});
            $("fieldset h3").remove();
            $(".addSponsorRows").remove();
            $(".addProductionRows").remove();
            $(".addProExportRows").remove();
            $(".actions").css({"display": "none"});
            $(".steps").css({"display": "none"});
            $(".draft").css({"display": "none"});
            $(".title").css({"display": "none"});

            $(document).on('click', '.download', function (e) {
                var value = $(this).attr('data');
                $('.show_' + value).show();
            });
        });
        $(function () {
            $('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: (new Date()),
                minDate: '01/01/1916'
            });
        });
    </script>
@endsection